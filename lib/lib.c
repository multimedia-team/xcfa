 /*
 * file      : lib/lib.c
 * project   : xcfa
 * with      : Gtk-2
 *
 * copyright : (C) 2003 - 2015 by Claude Bulin
 *
 * xcfa - Creation d'une base de programmation en langage C de type GNU avec les autotools
 * GNU General Public License
 *
 *  This file is part of XCFA.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * OLD ADRESS:
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * NEW ADRESS:
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define _(String) gettext (String)
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>

#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>

#include "lib.h"



OPTIONS_COMMAND_LINE OptionsCommandLine = {
	FALSE,		// gboolean	BoolVerboseMode
	FALSE,		// gboolean	BoolVersionMode
	FALSE		// gboolean	BoolHelpMode
};


/*
typedef struct {
	guint32 pixel;
	guint16 red;
	guint16 green;
	guint16 blue;
} GdkColor;
*/
GdkColor YellowColor = { 0, 63479,63479,48830 };

//
//
gchar *libutils_dup_chomp( gchar *p_str )
{
	gchar	*StrNew = g_strdup( p_str );
	gchar	*Ptr = StrNew;

	while( *Ptr ) {
		if( '\n' == *Ptr ) *Ptr = '\0';
		if( '\r' == *Ptr ) *Ptr = '\0';
		Ptr ++;
	}
	return( StrNew );
}
//
//
gchar *libutils_chomp( gchar *p_str )
{
	gchar	*Ptr = p_str;
	while( *Ptr ) {
		if( '\n' == *Ptr ) *Ptr = '\0';
		if( '\r' == *Ptr ) *Ptr = '\0';
		Ptr ++;
	}
	return( p_str );
}
//
//
gchar *libutils_get_prefix (gchar *PathNameFile)
{
	gchar	*Prefix = g_strdup (PathNameFile);
	gchar	*Ptr = NULL;

	if (NULL != (Ptr = strrchr (Prefix, '/'))) {
		*Ptr = '\0';
	}

	return (Prefix);
}
//
//
gboolean libutils_access_mode (gchar *PathNameFile)
{
	gboolean	FileAccessModeFileExist = access (PathNameFile, F_OK) == -1 ? FALSE : TRUE;
	return (FileAccessModeFileExist);
}
// CREATION D UN DOSSIER TEMPORAIRE
//
gchar *libutils_create_temporary_rep( gchar *SrcTmp, gchar *NewRepTmp )
{
	gchar	*Path = NULL;
	gint	NumPath = 0;

	// FIXED
	// Mon, 20 Jan 2014 16:33:36 +0100
	// Plusieurs instances de XCFA peuvent être activées sans collision pour les conversions et extractions
	//
	while( TRUE ) {
		Path = g_strdup_printf( "%s/%s_%d", SrcTmp, NewRepTmp, NumPath );
		if( TRUE == g_file_test( Path, G_FILE_TEST_IS_DIR ) ) {
			g_free( Path );
			Path = NULL;
		}
		else {
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print( "CREATE TEMPORARY REP:\n\t%s\n", Path );
			g_mkdir_with_parents( Path, 0700 );
			break;
		}
		NumPath ++;
	}

	return( (gchar *)Path );
}
// SUPPRESSION D UN DOSSIER TEMPORAIRE
//
gchar *libutils_remove_temporary_rep (gchar *path_tmprep)
{
	pid_t  pid;

	if (path_tmprep != NULL) {
		if ((pid = fork ()) == 0) {
			execlp (
				"rm",
				"rm",
				"-rf",
				path_tmprep,
				NULL
				);
		}
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("REMOVE TEMPORARY REP:\n\t%s\n", path_tmprep);
		g_free (path_tmprep);
		path_tmprep = NULL;
	}
	return ((gchar *)NULL);
}
// TRUE SI LE FICHIER EXISTE SINON FALSE
//
gboolean libutils_test_file_exist (gchar *File)
{
	/*struct stat status;
	gint	    ret;
	ret = stat (File, &status);
	return (ret > -1 && S_ISREG (status.st_mode));*/
	return (g_file_test (File, G_FILE_TEST_IS_REGULAR));
}
// RENVOIE LE PAHTNAME AVEC LA NOUVELLE EXTENTION
//
gchar *libutils_get_pathname_with_new_ext (gchar *PathName, gchar *NewExt)
{
	gchar	*NewPathName = g_strdup_printf ("%s0123456789", PathName);
	gchar	*Ptr = strrchr (NewPathName, '.');

	if (NULL == Ptr) {
		for (Ptr = NewPathName; *Ptr; Ptr ++);
		*Ptr ++ = '.';
		*Ptr = '\0';
	}
	else {
		Ptr ++;
	}
	strcpy (Ptr, NewExt);

	return (NewPathName);
}
// RENVOIE TRUE SI LE FICHIER EXISTE SINON FALSE
//
gboolean libutils_test_file_with_new_ext_exist (gchar *PathName, gchar *NewExt)
{
	gchar		*NewPathName = libutils_get_pathname_with_new_ext (PathName, NewExt);
	gboolean	BoolFileExist;

	BoolFileExist = libutils_test_file_exist (NewPathName);

	g_free (NewPathName);
	NewPathName = NULL;

	return (BoolFileExist);
}
// TRUE SI LE DOSSIER EXISTE SINON FALSE
//
gboolean libutils_test_dir_exist (gchar *File)
{
	/*struct stat status;
	gint	    ret;
	ret = stat (File, &status);
	return (ret > -1 && S_ISDIR (status.st_mode));*/
	return (g_file_test (File, G_FILE_TEST_IS_DIR));
}
// AFFICHE L ICONE A GAUCHE DANS LA BARRE DE TITRE
//
gchar *libutils_get_pathname_pixmaps (gchar *p_name_pixmap)
{
	gchar *LineCommand = NULL;

	LineCommand = xdg_search_data_xdg( p_name_pixmap );
	return (LineCommand);
}
// AFFICHE L ICONE A GAUCHE DANS LA BARRE DE TITRE
//
void libutils_set_default_icone_to_win (GtkWidget *win)
{
	gchar     *LineCommand = NULL;
	GError    *error = NULL;

	if( NULL != (LineCommand = xdg_search_data_xdg( "xcfa.png" ))) {
	 	gtk_window_set_icon_from_file (GTK_WINDOW(win), LineCommand, &error);
	 	if (error) {
	 		GDK_PIXBUF_ERROR;
			g_critical ("Could not load pixbuf: %s\n", error->message);
			g_error_free (error);
		}
	}
	g_free (LineCommand);
	LineCommand = NULL;
}
// Execute WHICH Name_Prg et si 'gchar **Pathname_Prg' est non null retourne le resultat
//	gchar *g_find_program_in_path( const gchar *program );
//
gboolean libutils_find_file (gchar *p_find_file)
{
	gchar      *PathName = NULL;

	if( NULL != p_find_file ) {
		if( NULL != (PathName = g_find_program_in_path( p_find_file ))) {
			g_free( PathName );	PathName = NULL;
			return( TRUE );
		}
	}

	return( FALSE );
}
/* utils_hexa_to_int : convertit en entier la chaine pointee par 'gchar *Ptr_Hexa' en hexa
*  La chaine doit commencer par 0x..
*  VALUES A = 65  et   a = 97
*/
gint libutils_hexa_to_int (gchar *Ptr_Hexa)
{
	gint     chiffre_hexa = 0;
	/*gint     cpt = 0;*/
	gint     Gint_Ret = 0;
	gboolean Bool_dans_hexa = FALSE;

	/* PRINT_FUNC_LF(); */

	if (NULL == Ptr_Hexa) return (0);
	if ('\0' == *(Ptr_Hexa) || '\0' == *(Ptr_Hexa+1) || '\0' == *(Ptr_Hexa+2) || '\0' == *(Ptr_Hexa+3))  return (0);

	if (*Ptr_Hexa == '0' && (*(Ptr_Hexa+1) == 'x' || *(Ptr_Hexa+1) == 'X')) {
		Ptr_Hexa += 2;
		Bool_dans_hexa = TRUE;
	}

	Gint_Ret = 0;
	while (Bool_dans_hexa) {
		if (g_ascii_isdigit (*Ptr_Hexa)) {
			chiffre_hexa = *Ptr_Hexa - '0';
		}
		else if (g_ascii_isxdigit (*Ptr_Hexa)) {
			if ((gint)*Ptr_Hexa >= 'a')
				chiffre_hexa = *Ptr_Hexa - 'a' + 10;
			else    chiffre_hexa = *Ptr_Hexa - 'A' + 10;
		}
		else Bool_dans_hexa = FALSE;

		if (Bool_dans_hexa) {
			Gint_Ret = 16 * Gint_Ret + chiffre_hexa;
			Ptr_Hexa ++;
		}
	}

	return (Gint_Ret);
}
// SUPPRESSION SIMPLE ALLOC POINTEE PAR LE GLIST ANSI QUE LE GLIST
//
GList *libutils_remove_glist (GList *New)
{
	GList *list = NULL;
	gchar *ptr = NULL;

	if (New != NULL) {
		list = g_list_first (New);
		while (list) {
			if ((ptr = (gchar*)list->data)) {
				g_free (ptr);
				ptr = NULL;
				list->data = NULL;
			}
			list = g_list_next(list);
		}
		g_list_free (New);
		New = NULL;
	}
	return ((GList *)NULL);
}
// SUPPRESSION SIMPLE ALLOC POINTEE PAR LE GSLIST ANSI QUE LE GSLIST
//
GSList *libutils_remove_gslist (GSList *New)
{
	GSList *gslist = NULL;

	if (NULL != (gslist = New)) {
		while (gslist) {
			if (NULL != (gchar*)gslist->data) {
				g_free (gslist->data);
				gslist->data = NULL;
			}
			gslist = g_slist_next (gslist);
		}
		g_slist_free (New);
		gslist = New = NULL;
	}
	return ((GSList *)NULL);
}
// RETOURNE LA TAILLE DU FICHIER EN OCTETS
//
size_t libutils_get_size_file (gchar *PathName)
{
	FILE   *fp;
	size_t  size = 0;

	if ((fp = fopen (PathName, "r"))) {
		fseek (fp, 0, SEEK_END);
		size = ftell (fp);
		fclose (fp);
	}
	return (size);
}
// Ecriture de datas sur disk, recuperation puis effacement du fichier
//
void libutils_add_datas_on_disk (gchar *data)
{
	FILE *fp = NULL;

	fp = fopen ("/tmp/data_xcfa.txt", "a");
	fprintf (fp, "%s", data);
	fclose (fp);
}
gchar *libutils_get_datas_on_disk (void)
{
	/*
	FILE   *fp = NULL;
	gchar  *buf = NULL;
	size_t  size;

	size = libutils_get_size_file ("/tmp/data_xcfa.txt");
	buf = (gchar *)g_malloc0 (sizeof(gchar) * (size + 10));
	if ((fp = fopen ("/tmp/data_xcfa.txt", "r")) != NULL) {
		size = fread (buf, 1, size, fp);
		fclose (fp);
	} else {
		g_free (buf);
		buf = NULL;
	}

	g_unlink ("/tmp/data_xcfa.txt");

	return ((gchar  *)buf);
	*/
	FILE   *fp = NULL;
	gchar  *buf = NULL;
	size_t  size;

	if( (size = libutils_get_size_file( "/tmp/data_xcfa.txt" )) > 10 ) {
		buf = (gchar *)g_malloc0 (sizeof(gchar) * (size + 10));
		if ((fp = fopen ("/tmp/data_xcfa.txt", "r")) != NULL) {
			size = fread (buf, 1, size, fp);
			fclose (fp);
		} else {
			g_free (buf);
			buf = NULL;
		}
	}
	g_unlink ("/tmp/data_xcfa.txt");

	return ((gchar  *)buf);
}
// TRUE SI ECRITURE OK SINON FALSE
//
gboolean libutils_test_write (gchar *path)
{
	gboolean  BoolWriteOk = FALSE;
	FILE     *fp = NULL;
	gchar    *str = NULL;

	if (path[ 0 ] == '/' && path[ 1 ] != '\0') {
		str = g_strdup_printf ("%s/xcfa_tst_write.txt", path);
		fp = fopen (str, "w");
		if (fp) {
			BoolWriteOk = TRUE;
			fclose (fp);
			g_unlink (str);
		}
		g_free (str);
		str = NULL;
	}

	return (BoolWriteOk);
}
// INIT PIXBUF
//
GdkPixbuf *libutils_init_pixbufs (gchar *NameFilePixbuf)
{
	GdkPixbuf *NewPixbuf = NULL;
	GError    *error = NULL;
	gchar     *Pathname_Pixbuf = NULL;

	Pathname_Pixbuf = libutils_get_pathname_pixmaps (NameFilePixbuf);

	NewPixbuf = gdk_pixbuf_new_from_file(Pathname_Pixbuf, &error);
 	if (error) {
 		GDK_PIXBUF_ERROR;
		g_critical ("Could not load pixbuf: %s\n", error->message);
		g_error_free (error);
		g_free (Pathname_Pixbuf);
		Pathname_Pixbuf = NULL;
		return (NULL);
	}
	g_free (Pathname_Pixbuf);
	Pathname_Pixbuf = NULL;

	return (NewPixbuf);
}
//
// typedef enum {
// 	TYPE_ALL_FILE = 0,
// 	TYPE_WAV_FILE,
// 	TYPE_MP3_OGG_FILE,
// 	TYPE_TAG_FILE
// } TYPE_FILE;
//
gint libutils_get_first_line_is_selected( GtkTreeSelection *AdrLine, GtkTreeModel *AdrTreeModel )
{
	gboolean	valid;
	GtkTreeIter	iter;
	gint		Cpt = -1;

	if (NULL != AdrLine && NULL != AdrTreeModel ) {
		valid = gtk_tree_model_get_iter_first( AdrTreeModel, &iter );
		while (valid) {
			if (TRUE == gtk_tree_selection_iter_is_selected( AdrLine, &iter) ) {
				Cpt ++;
				return( Cpt );
			}
			valid = gtk_tree_model_iter_next( AdrTreeModel, &iter );
			if( TRUE == valid ) Cpt ++;
		}
	}
	return( -1 );
}
//
//
gchar *libutils_get_name_without_ext_with_amp( gchar *p_PathNameFile )
{
	gchar *NewName = g_strnfill (strlen(p_PathNameFile) * 4, '\0');
	gchar *NewPtr = NewName;
	gchar *NameRet = NULL;
	gchar *ptr = NULL;

	// POINTEUR SUR LE DEBUT DU NOM REEL
	if (NULL != (ptr = strrchr (p_PathNameFile, '/')))
		ptr ++;
	else	ptr = p_PathNameFile;

	// COPIE DE p_PathNameFile VERS NewName
	while (*ptr) {
		if (*ptr == '&') {
			*NewPtr ++ = *ptr ++;
			*NewPtr ++ = 'a';
			*NewPtr ++ = 'm';
			*NewPtr ++ = 'p';
			*NewPtr ++ = ';';
		}
		else {
			*NewPtr ++ = *ptr ++;
		}
	}

	// DUPLIQUER NewName VERS name
	NameRet = g_strdup (NewName);
	g_free (NewName);
	NewName = NULL;

	// CACHER L EXTENTION SI ELLE EXISTE DANS name
	if (NULL != (ptr = strrchr (NameRet, '.'))) *ptr = '\0';

	return ((gchar *)NameRet);
}
//
//
gchar *libutils_get_name_without_ext( gchar *p_PathNameFile )
{
	gchar *NewName = NULL;
	gchar *Ptr = NULL;

	// POINTEUR SUR LE DEBUT DU NOM REEL
	if( NULL != (Ptr = strrchr (p_PathNameFile, '/' )))
		Ptr ++;
	else	Ptr = p_PathNameFile;

	// COPIE
	NewName = g_strdup( Ptr );

	// CACHER L EXTENTION SI ELLE EXISTE DANS NewName
	if( NULL != ( Ptr = strrchr( NewName, '.' ))) *Ptr = '\0';

	return( (gchar *)NewName );
}
//
//
gchar *libutils_string_toupper( gchar *p_str )
{
	gchar	*Str = g_strdup( p_str );
	gchar	*Ptr = Str;

	while( Ptr && *Ptr ) {
		*Ptr = g_ascii_toupper( *Ptr );
		Ptr ++;
	}

	return( Str );
}




