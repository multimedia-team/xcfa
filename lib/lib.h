 /*
 * file      : lib/lib.h
 * project   : xcfa
 * with      : Gtk-2
 *
 * copyright : (C) 2003 - 2015 by Claude Bulin
 *
 * xcfa - GTK+ implementation of the GNU shell command
 * GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * OLD ADRESS:
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * NEW ADRESS:
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef libH_h
#define libH_h 1



// /usr/include/gtk-2.0/gdk/gdkkeysyms.h
typedef struct {
	gboolean	BoolVerboseMode;
	gboolean	BoolVersionMode;
	gboolean	BoolHelpMode;
} OPTIONS_COMMAND_LINE;

extern OPTIONS_COMMAND_LINE OptionsCommandLine;

extern GdkColor YellowColor;



// FILE_IS.C
// 
gboolean	FileIs_vidm4a( gchar *namefile );
gboolean	FileIs_m4a( gchar *namefile );
gboolean	FileIs_wavpack (gchar *namefile);
gboolean	FileIs_wav (gchar *namefile);
gboolean	FileIs_shn (gchar *namefile);
gboolean	FileIs_rm (gchar *namefile);
gboolean	FileIs_ogg (gchar *namefile);
gboolean	FileIs_mpc (gchar *namefile);
gboolean	FileIs_ac3 (gchar *namefile);
gboolean	FileIs_mp3 (gchar *namefile);
gboolean	FileIs_flac (gchar *namefile);
gboolean	FileIs_ape (gchar *namefile);
gboolean	FileIs_aiff (gchar *namefile);
gboolean	FileIs_dts (gchar *namefile);
gboolean	FileIs_g_str_has_suffix (gchar *NameSong, gchar *Suffixe);
gboolean	FileIs_png( gchar *namefile );
gboolean	FileIs_jpg( gchar *namefile );
gboolean	FileIs_image( gchar *PathNameFile );

// LIB.C
// 
gchar		*libutils_dup_chomp( gchar *p_str );
gchar		*libutils_chomp( gchar *p_str );
gchar		*libutils_get_prefix (gchar *PathNameFile);
gboolean	libutils_access_mode (gchar *PathNameFile);
gboolean	libutils_test_file_exist (gchar *File);
gboolean	libutils_test_dir_exist (gchar *File);
void		libutils_set_default_icone_to_win (GtkWidget *win);
gboolean	libutils_find_file (gchar *p_find_file);
gchar		*libutils_get_pathname_pixmaps (gchar *p_name_pixmap);
gint		libutils_hexa_to_int (gchar *Ptr_Hexa);
GList		*libutils_remove_glist (GList *New);
GSList		*libutils_remove_gslist (GSList *New);
size_t		libutils_get_size_file (gchar *PathName);
void		libutils_add_datas_on_disk (gchar *data);
gchar		*libutils_get_datas_on_disk (void);
gboolean	libutils_test_write (gchar *path);
GdkPixbuf	*libutils_init_pixbufs (gchar *NameFilePixbuf);
gchar		*libutils_get_pathname_with_new_ext (gchar *PathName, gchar *NewExt);
gboolean	libutils_test_file_with_new_ext_exist (gchar *PathName, gchar *NewExt);
gint		libutils_get_first_line_is_selected( GtkTreeSelection *AdrLine, GtkTreeModel *AdrTreeModel );
gchar		*libutils_get_name_without_ext_with_amp( gchar *p_PathNameFile );
gchar		*libutils_get_name_without_ext( gchar *p_PathNameFile );
gchar		*libutils_create_temporary_rep (gchar *SrcTmp, gchar *NewRepTmp);
gchar		*libutils_remove_temporary_rep (gchar *path_tmprep);
gchar		*libutils_string_toupper( gchar *p_str );

// COMBO.C
// 
void		libcombo_remove_options (GtkComboBox *p_combo);

// UTF8.C
// 
gchar		*utf8_convert_from_utf8 (const char *string);
gchar		*utf8_convert_to_utf8 (const gchar *string);
gchar		*utf8_removediacritics( const gchar *str, gssize len );
gchar		*utf8_eperluette_name( gchar *p_name );

// XDG.C
// 
void		xdg_print_list_config_path( void );
void		xdg_print_list_data_path( void );
gchar		*xdg_search_config_xdg( void );
gchar		*xdg_search_data_xdg( gchar *NameFile );

#endif

