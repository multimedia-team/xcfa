 /*
 * file      : lib/utf8.h
 * project   : xcfa
 * with      : Gtk-2
 *
 * copyright : (C) 2003 - 2015 by Claude Bulin
 *
 * xcfa - Creation d'une base de programmation en langage C de type GNU avec les autotools
 * GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * OLD ADRESS:
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * NEW ADRESS:
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define _(String) gettext (String)
#endif

#include <gtk/gtk.h>
#include <string.h>

#include "lib.h"


gchar *utf8_convert_from_utf8 (const char *string)
{
	gchar	*ptr = NULL;
	GError	*error = NULL;
	gchar	*output = NULL;
	gsize    bytes_written;
	
	if (NULL == string) return ((gchar *)NULL);
	
	/* Suppression des ESPACES de fin de chaine	*/
	for (ptr = (gchar *)string; *ptr; ptr ++);
	ptr --;
	while (ptr >= string && *ptr == ' ') ptr --;
	ptr ++;
	*ptr = '\0';
	
	output = g_convert(string, -1, "iso-8859-1", "utf-8", NULL, &bytes_written, &error);
	if (NULL == output) {
		return ((gchar *)g_strdup(string));
	}
	else if (NULL != output) {
		/* Patch from Alexey Illarionov:
			g_convert returns null-terminated string only with one \0 at the
			end. It can cause some garbage at the end of a string for UTF-16.
			The second \0 should be set manually.
		*/
		output = g_realloc(output, bytes_written + 2);
		if (NULL != output) output[bytes_written] = output[bytes_written + 1] = 0;
	}
	
	return ((gchar *)output);
}
/* Fonctions reprises (et adaptees) depuis EasyTag v1.99.12
 *
 * Main part of code, written by:
 * Copyright (C) 1999-2001  Håvard Kvålen <havardk@xmms.org>
 *
 * Length must be passed, as the string might be Unicode, in which case we can't
 * count zeroes (see strlen call below).
 */
gchar *utf8_convert_to_utf8 (const gchar *string)
{
	gchar	*ptr = NULL;
	GError	*error = NULL;
	gchar	*output = NULL;
	gsize    bytes_written;
	
	if (!string) return ((gchar *)NULL);
	
	/* Suppression des ESPACES de fin de chaine	*/
	for (ptr = (gchar *)string; *ptr; ptr ++);
	ptr --;
	while (ptr >= string && *ptr == ' ') ptr --;
	ptr ++;
	*ptr = '\0';
	
	/* Si transformation deja faite quit */
	if (g_utf8_validate(string, -1, NULL)) return ((gchar *)g_strdup(string));
	
	output = g_convert(string, -1, "utf-8", "iso-8859-1", NULL, &bytes_written, &error);
	if (output == NULL) {
		
		gchar *escaped_str = g_strescape(string, NULL);
		
		g_warning("convert_string(): Failed conversion from charset 'iso-8859-1' to 'utf-8'. String '%s'. Errcode %d (%s).\n",
					escaped_str,
					error->code,
					error->message);
		g_free(escaped_str);
		g_error_free(error);
		/*
		Return the input string without converting it. If the string is displayed in the UI, it must be in UTF-8!
		*/
		if ((g_ascii_strcasecmp("utf-8", "UTF-8")) ||   (g_utf8_validate(string, -1, NULL)) )
		{
			return ((gchar *)g_strdup(string));
		}
	}
	else {
		/* Patch from Alexey Illarionov:
			g_convert returns null-terminated string only with one \0 at the
			end. It can cause some garbage at the end of a string for UTF-16.
			The second \0 should be set manually.
		*/
		output = g_realloc(output, bytes_written + 2);
		if (output != NULL) output[bytes_written] = output[bytes_written + 1] = 0;
	}
	
	return ((gchar *)output);
}
// 
// UNACCENT:
//	http://freetuxtv.blogspot.com/
//	http://freetuxtv.blogspot.com/2010/12/ignorer-les-diacritiques-accent-trema.html
// 
gchar *utf8_removediacritics( const gchar *str, gssize len )
{
	gchar		*szNormalizedString;
	GString		*szStringBuilder;
	gchar		*szRes = NULL;
	gunichar	c;
	gchar		*szPtr = NULL;

	if( NULL != str ) {
		szNormalizedString = g_utf8_normalize( str, len, G_NORMALIZE_NFD );

		szStringBuilder = g_string_new( "" );

		szPtr = szNormalizedString;
		while( szPtr ){
			c = g_utf8_get_char( szPtr );
			if( c != '\0' ) {
				if( !g_unichar_ismark( c )) {
					g_string_append_unichar( szStringBuilder, c );
				}
				szPtr = g_utf8_next_char( szPtr );
			}
			else {
				szPtr = NULL;
			}
		}

		szRes = g_string_free( szStringBuilder, FALSE );
		g_free( szNormalizedString );
	}

	return szRes;
}
//  &AMP
// 
gchar *utf8_eperluette_name( gchar *p_name )
{
	gchar *NewName = NULL;
	gchar *PtrNew = NULL;
	gchar *Ptr = NULL;
	
	if( NULL != p_name ) {
		NewName = g_strnfill( strlen( p_name ) * 4, '\0' );
		PtrNew = NewName;
		Ptr = p_name;
		
		while( *Ptr ) {
			if( *Ptr == '&' && *(Ptr+1) != 'a' ) {
				*PtrNew ++ = *Ptr ++;
				*PtrNew ++ = 'a';
				*PtrNew ++ = 'm';
				*PtrNew ++ = 'p';
				*PtrNew ++ = ';';
			}
			else {
				*PtrNew ++ = *Ptr ++;
			}
		}
	}
	
	return( NewName );
}




