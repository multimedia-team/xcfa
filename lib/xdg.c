 /*
 * file      : lib/xdg.h
 * project   : xcfa
 * with      : Gtk-2
 *
 * copyright : (C) 2003 - 2015 by Claude Bulin
 *
 * xcfa - Creation d'une base de programmation en langage C de type GNU avec les autotools
 * GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * OLD ADRESS:
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * NEW ADRESS:
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * !---------------------------------------------
 * ! FreeDesktop integration
 * !---------------------------------------------
 * ! http://www.freedesktop.org/wiki/
 * ! http://fr.wikipedia.org/wiki/Freedesktop.org
 * !---------------------------------------------
 *
 * Pour les applications conformes aux spécifications freedesktop.org
 * 
	XDG_CONFIG_HOME
		~/.config).
		~/.local/share
		Celui-ci permet aux applications de trouver ces dossiers. 
		Indique aux applications où placer les informations de configuration de l'utilisateur.
		En général cette variable n'est pas définie puisqu'une valeur de secours par défaut est implémentée dans les spécifications
		Si cette variable est vide ou non remplie, un  répertoire équivalent à $HOME/.config devra être utilisé.

	XDG_DATA_HOME
		~/.local/share
		Indique aux applications où placer les données de l'utilisateur.
		En général cette variable n'est pas définie puisqu'une valeur de secours par défaut est implémentée dans les spécifications.
		Si cette variable est vide ou non remplie, un  répertoire par défaut équivalent à  $HOME/.local/share devra être utilisé.

	XDG_DATA_DIRS
		/usr/local/share : /usr/share
		Une liste de dossiers séparés par deux-points (similaire à PATH) indiquant aux applications où chercher les données.
		En général cette variable n'est pas définie puisqu'une valeur de secours par défaut est implémentée dans les spécifications
		Si cette variable est vide ou non remplie, une valeur égale à  /usr/local/share/:/usr/share/ devra être utilisée

	XDG_CACHE_HOME
		~/.cache
		un emplacement utilisé par les applications pour mettre en cache les données temporaires.
		En général cette variable n'est pas définie puisqu'une valeur de secours par défaut est implémentée dans les spécifications
		Par défaut sous xcfa: /tmp

	XDG_CONFIG_DIRS
		/etc/xdg
		Une liste de dossiers séparés par deux points (similaire à PATH) indiquant aux applications où chercher les informations de configuration.
		En général cette variable n'est pas définie puisqu'une valeur de secours par défaut est implémentée dans les spécifications
		Si cette variable est vide ou non remplie, une valeur égale à /etc/xdg devra être utilisée
 *
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define _(String) gettext (String)
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lib.h"


// ------------------------------------------------------------------------
// ************************************************************************
// 
// XDG_DATA_HOME
// HOME
// XDG_DATA_DIRS
// 
// ************************************************************************
// ------------------------------------------------------------------------

// 
// 
GSList *xdg_remove_gslist( GSList *gslist )
{
	GSList *Lthemesemp = gslist;
	GSList *Lcur;

	// if( TRUE == OptionsCommandLine.BoolVerboseMode ) 
	// 	g_print ("%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__);
	for (Lcur = Lthemesemp; Lcur && Lcur->data; Lcur = Lcur->next) {
		g_free( Lcur->data );
		Lcur->data = NULL;
	}
	// if( TRUE == OptionsCommandLine.BoolVerboseMode ) 
	// 	g_print( "\n" );
	g_slist_free(Lthemesemp);
	return( (GSList *)NULL );
}
// 
// FROM GRAVEMAN
// 
// on regarde si un fichier de configuration existe 
// tel que definie par http://standards.freedesktop.org/basedir-spec/latest/
// 
// construction de la liste des PATH dans lequel on va chercher la configuration
// 
GSList *xdg_get_data_path( void )
{
	GSList	*Lnewlist = NULL;
	const	gchar *Lenv;
	gchar	*Lpath;
	gchar	**Llistpath;
	gint	i;
	// gchar	*Ptr = NULL;

	// if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
	// 	g_print( "\n" );
	// 	g_print ("%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__);
	// }
	if ((Lenv=g_getenv("XDG_DATA_HOME"))) {
		Lpath = g_strdup_printf("%s/%s", Lenv, PACKAGE);
		Lnewlist = g_slist_append(Lnewlist, Lpath);
	} else if ((Lenv=g_getenv("HOME"))) {
		Lpath = g_strdup_printf("%s/.local/share/%s", Lenv, PACKAGE);
		Lnewlist = g_slist_append(Lnewlist, Lpath);
	}

	if ((Lenv = g_getenv("XDG_DATA_DIRS"))) {
		Llistpath = g_strsplit(Lenv, ":", 0);

		for (i=0; Llistpath[i]; i++) {
			// 
			// FROM GRAVEMAN
			// 
			// 	Lcurlist->data = /home/cat/.config/graveman/graveman.conf
			// 	Lcurlist->data = /etc/xdg/graveman/graveman.conf
			// 
			// 	Lcurlist->data = /home/cat/.local/share/graveman
			// 	Lcurlist->data = /usr/share/gnome/graveman
			// 	Lcurlist->data = /usr/share/gdm//graveman
			// 	Lcurlist->data = /usr/local/share//graveman
			// 	Lcurlist->data = /usr/share//graveman
			// 	Lcurlist->data = /usr/local/share/graveman
			// 	Lcurlist->data = /usr/share/graveman
			// 	Lcurlist->data = /usr/share/graveman
			// 
			// FROM FREEDESKTOP: BEFORE
			// 
			// 	Lcur->data = /home/cat/.config/freedesktop/freedesktop.conf
			// 	Lcur->data = /etc/xdg/freedesktop/freedesktop.conf
			// 
			// 	Lcur->data = /home/cat/.local/share/freedesktop
			// 	Lcur->data = /usr/share/gnome/freedesktop
			// 	Lcur->data = /usr/share/gdm//freedesktop
			// 	Lcur->data = /usr/local/share//freedesktop
			// 	Lcur->data = /usr/share//freedesktop
			// 	Lcur->data = /usr/local/share/freedesktop
			// 	Lcur->data = /usr/share/freedesktop
			// 	Lcur->data = /usr/share/freedesktop/glade
			// 	Lcur->data = /usr/share/freedesktop
			// 	Lcur->data = /usr/share/pixmaps
			// 	Lcur->data = /usr/share/pixmaps/freedesktop
			// 
			// REMOVE END CAR:
			// 	/usr/share/gdm/
			// 	/usr/local/share/
			// 
			
			/*******************************
			
			if( NULL != (Ptr = strrchr( Llistpath[i], '/' ))) {
				*Ptr = '\0';
			}
			
			*******************************/
			
			// FROM FREEDESKTOP: AFTER
			// 
			// 	Lcur->data = /home/cat/.config/freedesktop/freedesktop.conf
			// 	Lcur->data = /etc/xdg/freedesktop/freedesktop.conf
			// 
			// 	Lcur->data = /home/cat/.local/share/freedesktop
			// 	Lcur->data = /usr/share/freedesktop
			// 	Lcur->data = /usr/share/gdm/freedesktop
			// 	Lcur->data = /usr/local/share/freedesktop
			// 	Lcur->data = /usr/share/freedesktop
			// 	Lcur->data = /usr/local/share/freedesktop
			// 	Lcur->data = /usr/share/freedesktop
			// 	Lcur->data = /usr/share/freedesktop/glade
			// 	Lcur->data = /usr/share/freedesktop
			// 	Lcur->data = /usr/share/pixmaps
			// 	Lcur->data = /usr/share/pixmaps/freedesktop
			// 
			Lpath = g_strdup_printf("%s/%s", Llistpath[i], PACKAGE);
			Lnewlist = g_slist_append(Lnewlist, Lpath);
		}

		g_strfreev(Llistpath);
	}

	Lpath = g_strdup_printf("/usr/local/share/%s", PACKAGE);
	Lnewlist = g_slist_append(Lnewlist, Lpath);
	Lpath = g_strdup_printf("/usr/share/%s", PACKAGE);
	Lnewlist = g_slist_append(Lnewlist, Lpath);
	Lpath = g_strdup_printf("/usr/share/%s/glade", PACKAGE);
	Lnewlist = g_slist_append(Lnewlist, Lpath);
	Lpath = g_strdup_printf("%s/%s", DATA_DIR, PACKAGE);
	Lnewlist = g_slist_append(Lnewlist, Lpath);
	Lpath = g_strdup("/usr/share/pixmaps");
	Lnewlist = g_slist_append(Lnewlist, Lpath);
	Lpath = g_strdup_printf("/usr/share/pixmaps/%s", PACKAGE);
	Lnewlist = g_slist_append(Lnewlist, Lpath);
	
	return Lnewlist;
}
// 
// 
gchar *xdg_search_data_xdg( gchar *NameFile )
{	
	GSList		*Lthemesemp = xdg_get_data_path();
	GSList		*Lcur;
	gchar		*Name = NULL;
	gboolean	BoolExist = FALSE;
	
	// if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
	// 	g_print( "\n" );
	// 	g_print ("%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__);
	// }
	for (Lcur = Lthemesemp; Lcur && Lcur->data; Lcur = Lcur->next) {
		Name = g_strdup_printf( "%s/%s", (gchar *)Lcur->data, NameFile );
		BoolExist = libutils_test_file_exist( Name );
		// g_print( "\t%s  NameSearch = %s\n", BoolExist ? "TRUE " : "FALSE", Name );
		if( TRUE == BoolExist ) break;
		g_free( Name );
		Name = NULL;
	}
	// if( TRUE == OptionsCommandLine.BoolVerboseMode )
	// 	g_print( "\n" );
	Lthemesemp = xdg_remove_gslist( Lthemesemp );
	return( (gchar *)Name );
}
//
//
void xdg_print_list_data_path( void )
{
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		
		GSList	*Lthemesemp = xdg_get_data_path();
		GSList *Lcur;

		g_print( "\n" );
		g_print ("%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__);
		g_print( "!---------------------------------------------\n" );
		g_print( "! FreeDesktop integration\n" );
		g_print( "!---------------------------------------------\n" );
		g_print( "! http://www.freedesktop.org/wiki/\n" );
		g_print( "! http://fr.wikipedia.org/wiki/Freedesktop.org\n" );
		g_print( "!---------------------------------------------\n" );
		for (Lcur = Lthemesemp; Lcur && Lcur->data; Lcur = Lcur->next) {
			g_print( "\tLcur->data = %s\n", (gchar *)Lcur->data );
		}
		
		Lthemesemp = xdg_remove_gslist( Lthemesemp );
	}
}

// ------------------------------------------------------------------------
// ************************************************************************
// 
// XDG_CONFIG_HOME
// HOME
// XDG_CONFIG_DIRS
// 
// ************************************************************************
// ------------------------------------------------------------------------

// 
// FROM GRAVEMAN
// 
// on regarde si un repertoire de donnée existe
// tel que definie par http://standards.freedesktop.org/basedir-spec/latest/
// on regarde de + en dernier dans le DATADIR definie a la compilation
// 
// construction de la liste des PATH dans lequel on va chercher la configuration
// 
GSList *xdg_get_config_path( void )
{
	GSList	*Lnewlist = NULL;
	const	gchar *Lenv;
	gchar	*Lpath;
	gchar	**Llistpath;
	gint	i;

	// if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
	// 	g_print( "\n" );
	// 	g_print ("%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__);
	// }
	if ((Lenv=g_getenv("XDG_CONFIG_HOME"))) {
		Lpath = g_strdup_printf("%s/%s/%s.conf", Lenv, PACKAGE, PACKAGE);
		Lnewlist = g_slist_append(Lnewlist, Lpath);
	} else if ((Lenv=g_getenv("HOME"))) {
		Lpath = g_strdup_printf("%s/.config/%s/%s.conf", Lenv, PACKAGE, PACKAGE);
		Lnewlist = g_slist_append(Lnewlist, Lpath);
	}

	if ((Lenv = g_getenv("XDG_CONFIG_DIRS"))) {
		Llistpath = g_strsplit(Lenv, ":", 0);

		for (i=0; Llistpath[i]; i++) {
			if (!*Llistpath[i]) continue; // on ignore les entrees vides
			Lpath = g_strdup_printf("%s/%s/%s.conf", Llistpath[i], PACKAGE, PACKAGE);
			Lnewlist = g_slist_append(Lnewlist, Lpath);
			// if( TRUE == OptionsCommandLine.BoolVerboseMode )
			// 	g_print( "Lpath = %s\n", Lpath );
		}

		g_strfreev(Llistpath);
	} else {
		Lpath = g_strdup_printf("/etc/xdg/%s/%s.conf", PACKAGE, PACKAGE);
		Lnewlist = g_slist_append(Lnewlist, Lpath);
	}
	
	return Lnewlist;
}
// 
// 
gchar *xdg_search_config_xdg( void )
{	
	GSList		*Lthemesemp = xdg_get_config_path();
	GSList		*Lcur;
	gchar		*Name = NULL;
	gboolean	BoolExist = FALSE;
	
	// if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
	// 	g_print( "\n" );
	// 	g_print ("%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__);
	// }
	for (Lcur = Lthemesemp; Lcur && Lcur->data; Lcur = Lcur->next) {
		Name = g_strdup( (gchar *)Lcur->data );
		BoolExist = libutils_test_file_exist( Name );
		// if( TRUE == OptionsCommandLine.BoolVerboseMode )
		// 	g_print( "\t%s  NameSearch = %s\n", BoolExist ? "TRUE " : "FALSE", Name );
		if( TRUE == BoolExist ) break;
		g_free( Name );
		Name = NULL;
	}
	// SI NameFile N'EXISTE PAS ALORS SELECTION DE LA PREMIERE ENTREE
	if( FALSE == BoolExist ) {
		Lcur = Lthemesemp;
		Name = g_strdup( (gchar *)Lcur->data );
	}
	// if( TRUE == OptionsCommandLine.BoolVerboseMode )
	// 	g_print( "\n" );
	Lthemesemp = xdg_remove_gslist( Lthemesemp );
	return( (gchar *)Name );
}
//
//
void xdg_print_list_config_path( void )
{
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		
		GSList	*Lcur;
		GSList	*Lthemesemp = xdg_get_config_path();
	
		g_print( "\n" );
		g_print( "%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__);
		g_print( "!---------------------------------------------\n" );
		g_print( "! FreeDesktop integration\n" );
		g_print( "!---------------------------------------------\n" );
		g_print( "! http://www.freedesktop.org/wiki/\n" );
		g_print( "! http://fr.wikipedia.org/wiki/Freedesktop.org\n" );
		g_print( "!---------------------------------------------\n" );
		for (Lcur = Lthemesemp; Lcur && Lcur->data; Lcur = Lcur->next) {
			g_print( "\tLcur->data = %s\n", (gchar *)Lcur->data );
		}
		
		Lthemesemp = xdg_remove_gslist( Lthemesemp );
	}
}


