 /*
 * file      : lib/file_is.c
 * project   : xcfa
 * with      : Gtk-2
 *
 * copyright : (C) 2003 - 2015 by Claude Bulin
 *
 * xcfa - Creation d'une base de programmation en langage C de type GNU avec les autotools
 * GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * OLD ADRESS:
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * NEW ADRESS:
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define _(String) gettext (String)
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>

#include <string.h>
#include <ctype.h>

#include "lib.h"


/*
SAUF
	AAC
	DTS
	M4A
	WMA

*/



// FILE IS VIDM4A ?
//
gboolean FileIs_vidm4a( gchar *namefile )
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gint         cpt;
	gboolean     bool_signature = FALSE;
	
	if( (buf = (gchar *)g_malloc0 (sizeof(gchar) * 2048)) == NULL ) return (FALSE);
	if( (fp = fopen (namefile, "rb")) != NULL ) {
		fread( buf, 1, 2000, fp );
		fclose (fp);
	}

	for (cpt = 0; cpt < 2000; cpt ++ ) {
		if( buf [ cpt +  0 ] == 0x6D &&		// m
		    buf [ cpt +  1 ] == 0x6F &&		// o
		    buf [ cpt +  2 ] == 0x6F &&		// o
		    buf [ cpt +  3 ] == 0x76 &&		// v
		    buf [ cpt +  4 ] == 0x00 &&		// \0
		    buf [ cpt +  5 ] == 0x00 &&		// \0
		    buf [ cpt +  6 ] == 0x00 &&		// \0
		    buf [ cpt +  7 ] == 0x6C &&		// l
		    buf [ cpt +  8 ] == 0x6D &&		// m
		    buf [ cpt +  9 ] == 0x76 &&		// v
		    buf [ cpt + 10 ] == 0x68 &&		// h
		    buf [ cpt + 11 ] == 0x64 ) {	// d
		    
		    bool_signature = TRUE;
		    break;
		}
	}
	g_free( buf );
	buf = NULL;
	return( bool_signature );
}

// FILE IS M4A ?
//
gboolean FileIs_m4a( gchar *namefile )
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gint         cpt;
	gboolean     bool_signature = FALSE;
	
	if( (buf = (gchar *)g_malloc0 (sizeof(gchar) * 2048)) == NULL ) return (FALSE);
	if( (fp = fopen (namefile, "rb")) != NULL ) {
		fread( buf, 1, 2000, fp );
		fclose (fp);
	}

	for (cpt = 0; cpt < 2000; cpt ++ ) {
		if( buf [ cpt + 0 ] == 'l' &&
		    buf [ cpt + 1 ] == 'i' &&
		    buf [ cpt + 2 ] == 'b' &&
		    buf [ cpt + 3 ] == 'f' &&
		    buf [ cpt + 4 ] == 'a' &&
		    buf [ cpt + 5 ] == 'a' &&
		    buf [ cpt + 6 ] == 'c' ){
		    bool_signature = TRUE;
		    break;
		}
	}
	g_free( buf );
	buf = NULL;
	return( bool_signature );
}
// FILE IS WAV ?
//
gboolean FileIs_wavpack (gchar *namefile)
{
	FILE      *fp = NULL;
	gchar      str [ 42 ] = {'\0','\0','\0','\0','\0','\0','\0','\0'};

	if ((fp = fopen (namefile, "rb")) != NULL) {
		fread (&str[0], 1, 40, fp);
		fclose (fp);
	}

	if (str [0]  == 'w' &&
	    str [1]  == 'v' &&
	    str [2]  == 'p' &&
	    str [3]  == 'k' &&
	    
	    str [34] == 'R' &&
	    str [35] == 'I' &&
	    str [36] == 'F' &&
	    str [37] == 'F') return (TRUE);

	return (FALSE);
}
// FILE IS WAV ?
//
typedef struct {			// entete fichier WAV */

	gchar          tag1[ 5 ];	// ( 4 octets) : Constante "RIFF"
	guint32        size1;		// ( 4 octets) : Taille du fichier
	gchar          tag2 [ 15 ];	// (14 octets) : Constante "WAVEfmt "
	guint16        mode;		// ( 2 octets) : Mode (1 pour mono ou 2 pour stereo)
	guint32        freq;		// ( 4 octets) : Frequence d'échantillonage (en Hertz)
	guint32        bytepersec;	// ( 4 octets) : Nombre de bits par seconde de musique
	guint16        nbrbyte;		// ( 2 octets) : Nombre d'octets par échantillon
	guint16        format;		// ( 2 octets) : Nombre de bits par donnée
	guint32        tag3;		// ( 4 octets) : Constante "data"
	guint32        size2;		// ( 4 octets) : Taille du fichier moins 116 octets

} HEADER_WAV;
gboolean FileIs_wav (gchar *namefile)
{
	FILE        *fp = NULL;
	HEADER_WAV   buf;
	
	if ((fp = fopen (namefile, "r")) == NULL) return (FALSE);
	fread(&buf.tag1, 1, 4, fp);
	buf.tag1 [ 4 ] = '\0';
	fread(&buf.tag2, 1, 4, fp);
	fread(&buf.tag2, 1, 14, fp);
	fclose (fp);
	if ((buf.tag1 [ 0 ] == 'R' &&
	     buf.tag1 [ 1 ] == 'I' &&
	     buf.tag1 [ 2 ] == 'F' &&
	     buf.tag1 [ 3 ] == 'F') &&
	    
	    (buf.tag2 [ 0 ] == 'W' &&
	     buf.tag2 [ 1 ] == 'A' &&
	     buf.tag2 [ 2 ] == 'V' &&
	     buf.tag2 [ 3 ] == 'E')) return (TRUE);

	return (FALSE);
}
// FILE IS SHN ?
//
gboolean FileIs_shn (gchar *namefile)
{
	FILE      *fp = NULL;
	gchar      str [ 8 ] = {'\0','\0','\0','\0','\0','\0','\0','\0'};

	if ((fp = fopen (namefile, "rb")) != NULL) {
		fread (&str[0], 1, 4, fp);
		fclose (fp);
	}

	if (str [0] == 'a' && str [1] == 'j' && str [2] == 'k' && str [3] == 'g') return (TRUE);

	return (FALSE);
}
// FILE IS RM ?
//
/*
$ file *.rm
	audio01.rm:   RealMedia file
	audio02.rm:   RealMedia file

	en tête fichier RM
	--- DEC ---   --- HEX ---
	[ 00 01 02 03 ] 2e 52 4d 46 = .RMF
	[ 18 19 20 21 ] 50 52 4f 50  = PROP
*/
gboolean FileIs_rm (gchar *namefile)
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gboolean     bool_signature = FALSE;
	
	/*PRINT_FUNC_LF();*/

	buf = (gchar *)g_malloc0 (sizeof(gchar) * 512);
	if ((fp = fopen (namefile, "rb")) != NULL) {
		fread (buf, 1, 500, fp);
		fclose (fp);
	
		if (buf [  0 ] == 0x2e &&	// .
		    buf [  1 ] == 0x52 &&	// R
		    buf [  2 ] == 0x4d &&	// M
		    buf [  3 ] == 0x46 &&	// F
		    
		    buf [ 18 ] == 0x50 &&	// P
		    buf [ 19 ] == 0x52 &&	// R
		    buf [ 20 ] == 0x4f &&	// O
		    buf [ 21 ] == 0x50)	{	// P
		    
			bool_signature = TRUE;
		}
	}
	g_free (buf);
	buf = NULL;
	return (bool_signature);
}
// FILE IS OGG ?
//
gboolean FileIs_ogg (gchar *namefile)
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gint         cpt;
	gboolean     bool_signature = FALSE;
	
	if ((buf = (gchar *)g_malloc0 (sizeof(gchar) * 2048)) == NULL) return (FALSE);
	if ((fp = fopen (namefile, "rb")) != NULL) {
		fread (buf, 1, 2000, fp);
		fclose (fp);
	}

	for (cpt = 0; cpt < 2000; cpt ++) {
		if (buf [ cpt + 0 ] == 'O' &&
		    buf [ cpt + 1 ] == 'g' &&
		    buf [ cpt + 2 ] == 'g' &&
		    buf [ cpt + 3 ] == 'S') {
		    bool_signature = TRUE;
		    break;
		}
	}

	if (bool_signature == TRUE) {
		bool_signature = FALSE;
		for (cpt = 0; cpt < 2000; cpt ++) {
			if (buf [ cpt +  3 ] == 'v' &&
			    buf [ cpt +  4 ] == 'o' &&
			    buf [ cpt +  5 ] == 'r' &&
			    buf [ cpt +  6 ] == 'b' &&
			    buf [ cpt +  7 ] == 'i' &&
			    buf [ cpt +  8 ] == 's'){
			    bool_signature = TRUE;
			    break;
			}
		}
	}
	g_free (buf);
	buf = NULL;
	return (bool_signature);
}
// FILE IS MPC ?
//
gboolean FileIs_mpc (gchar *namefile)
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gint         cpt;
	gboolean     bool_signature = FALSE;
	
	/*PRINT_FUNC_LF();*/

	buf = (gchar *)g_malloc0 (sizeof(gchar) * 1034);
	if((fp = fopen (namefile, "rb")) != NULL) {
		fread (buf, 1, 1024, fp);
		fclose (fp);
	}
	// 
	// MPCKSH
	// 
	for (cpt = 0; cpt < 1024; cpt ++) {
		if (
		    buf [ cpt + 0 ] == 0x4d &&	// M
		    buf [ cpt + 1 ] == 0x50 &&	// P
		    buf [ cpt + 2 ] == 0x2b	// +
		    ) {
		    bool_signature = TRUE;
		    break;
		}
	}
	if (FALSE ==  bool_signature) {
		for (cpt = 0; cpt < 1024; cpt ++) {
			if (
			    buf [ cpt + 0 ] == 0x4d &&	// M
			    buf [ cpt + 1 ] == 0x50 &&	// P
			    buf [ cpt + 2 ] == 0x43 &&	// C
			    buf [ cpt + 3 ] == 0x4b &&	// K
			    buf [ cpt + 4 ] == 0x53 &&	// S
			    buf [ cpt + 5 ] == 0x48	// H
			    ) {
			    bool_signature = TRUE;
			    break;
			}
		}
	}
	g_free( buf );
	buf = NULL;
	return (bool_signature);
}
// FILE IS AC3 ?
//
gboolean FileIs_ac3 (gchar *namefile)
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gboolean     BoolSignature = FALSE;
	
	
	/*PRINT_FUNC_LF();*/

	buf = (gchar *)g_malloc0 (sizeof(gchar) * 100);
	if((fp = fopen (namefile, "rb")) != NULL) {
		fread (buf, 1, 50, fp);
		fclose (fp);
	}
	
	if (buf[0] == 0x0b && buf[1] == 0x77) {
		BoolSignature = TRUE;
	}
	
	g_free( buf );
	buf = NULL;
	
	return (BoolSignature);
}
// FILE IS MP3 ?
//
typedef struct  {
	gchar	magic[ 3 ];
	gchar	songname[ 30 ];
	gchar	artist[ 30 ];
	gchar	album[ 30 ];
	gchar	year[ 4 ];
	gchar	note[ 28 ];
	unsigned char	nnull;
	unsigned char	track;
	unsigned char	style;
} ID3TAG;
gboolean FileIs_mp3 (gchar *namefile)
{
	FILE    *fp = NULL;
	gchar   *buf = NULL;
	ID3TAG   begin;
	ID3TAG   end;
	gboolean bool_signature = FALSE;
	gint     i;
	size_t   size;

	// PRINT_FUNC_LF();

	fp = fopen (namefile, "rb");
	if (NULL == fp) return (FALSE);

	fread ( &begin, 128, 1, fp );
	fseek ( fp, 0, SEEK_END );
	fseek ( fp, ftell( fp ) - 128, SEEK_SET );
	fread ( &end, 128, 1, fp );
	fclose (fp);
	
	// TEST SI FICHIER EST: AC3
	// OCTET 0	OCTET 1
	// 76543210	76543210
	// 00001011	01110111
	// 0b		77
	// 11		119
	if (begin.magic[0] == 0x0b && begin.magic[1] == 0x77) {
		return (FALSE);
	}
	
	if (begin.magic[0] == 'I' && begin.magic[1] == 'D' && begin.magic[2] == '3') return (TRUE);
	if (end.magic[0] == 'T' && end.magic[1] == 'A' && end.magic[2] == 'G') return (TRUE);
	
	size = libutils_get_size_file (namefile);
	buf = g_malloc0 (sizeof (gchar) * (size + 10));
	if ((fp = fopen (namefile, "rb"))) {
		fread (buf, 1, size, fp);
		fclose (fp);
		for (i = 0; i < size; i ++) {
			if (buf[i+0] == 'L' && buf[i+1] == 'A' && buf[i+2] == 'M' && buf[i+3] == 'E') {
				bool_signature = TRUE;
				break;
			}
		}
	}
	
	// 
	// http://mgc99.free.fr/InfoMP3.html
	// 
	// 
	// La partie "audio" du MP3 commence par un entête de 4 octets. Ces 4 octets donnent des informations sur le type de MP3.
	// Cet entête se trouve après les tag ID3 v2 s'il y en a. Il n'a pas de position précise dans le fichier, il faut le chercher.
	// Il commence toujours par une série de 11 bits à 1 (FF Ex xx xx ou FF Fx xx xx).
	// 1111 1111 - 111B BCCD - EEEE FFGH - IIJJ KLMM
	// BB	version de MPEG (11-v1, 10-v2, 01-reserved, 00-v2.5)
	// CC	numéro de layer (11-layer 1, 10-layer 2, 01-layer 3, 00-reserved)
	// D	protection bit (0 protection par CRC, 2 octets suive l'entête. 1 pas de protection)
	// EEEE bitrate index.
	// FF	sampling rate frequency index.
	// G	padding bit.
	// H	private bit.
	// II	chanel mode (11-single channel, 10-dual channel, 01-joint stéréo, 00-stéréo)
	// JJ	mode extention seulement si "joint stéréo"
	// K	copyright (1 copyright, 0 pas de copyright).
	// L	original (1 original, 0 copie)
	// MM	emphasis (11-CCIT J.17, 10-reserved, 01-50/15ms, 00-none)
	// 
	/*
	if (FALSE == bool_signature) {
		for (i = 0; i < size; i ++) {
			Dummy = buf[i+0] + buf[i+1];
			Dummy &= 0x011111111111;
			if (0x011111111111 == Dummy) {
				bool_signature = TRUE;
				g_print("ICI EST MP3\n");
				g_print("\tsize = %lu\n", size );
				g_print("\ti    = %d\n", i );
				break;
			}
		}
	}
	*/
	g_free( buf );
	buf = NULL;
	return (bool_signature);
}
// FILE IS FLAC ?
//
gboolean FileIs_flac (gchar *namefile)
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gint         cpt;
	gboolean     bool_signature = FALSE;
	
	/*PRINT_FUNC_LF();*/

	buf = (gchar *)g_malloc0 (sizeof(gchar) * 5124);
	if ((fp = fopen (namefile, "rb")) != NULL) {
		fread (buf, 1, 5120, fp);
		fclose (fp);
	}
	
	for (cpt = 0; cpt < 5120; cpt ++) {
		if (buf [ cpt + 0 ] == 0x66 &&	/* 'f' */
		    buf [ cpt + 1 ] == 0x4c &&	/* 'L' */
		    buf [ cpt + 2 ] == 0x61 &&	/* 'a' */
		    buf [ cpt + 3 ] == 0x43) {	/* 'C' */
		    bool_signature = TRUE;
		    break;
		}
	}
	g_free( buf );
	buf = NULL;
	return (bool_signature);
}
/*****************************************************************************************
APE header that all APE files have in common (old and new)
*****************************************************************************************
struct APE_COMMON_HEADER
{
    char cID[4];                            should equal 'MAC '
    uint16 nVersion;                        version number * 1000 (3.81 = 3810)
};

*****************************************************************************************
APE header structure for old APE files (3.97 and earlier)
*****************************************************************************************
struct APE_HEADER_OLD
{
    char cID[4];                            should equal 'MAC '
    uint16 nVersion;                        version number * 1000 (3.81 = 3810)
    uint16 nCompressionLevel;               the compression level
    uint16 nFormatFlags;                    any format flags (for future use)
    uint16 nChannels;                       the number of channels (1 or 2)
    uint32 nSampleRate;                     the sample rate (typically 44100)
    uint32 nHeaderBytes;                    the bytes after the MAC header that compose the WAV header
    uint32 nTerminatingBytes;               the bytes after that raw data (for extended info)
    uint32 nTotalFrames;                    the number of frames in the file
    uint32 nFinalFrameBlocks;               the number of samples in the final frame
};
*****************************************************************************************/
// FILE IS APE ?
//
// le prg 'file' est leurré par un tag id3 et ne reconnait qu'un mp3  !!!
// il faut lire 05 ko de datas
// bug signalé par @patachonf depuis:
// 	http://forum.ubuntu-fr.org/viewtopic.php?pid=3739121#p3739121
// [ resolu ]
gboolean FileIs_ape (gchar *namefile)
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gint         cpt;
	gboolean     bool_signature = FALSE;
	
	/*PRINT_FUNC_LF();*/

	buf = (gchar *)g_malloc0 (sizeof(gchar) * 5120);
	if ((fp = fopen (namefile, "rb")) != NULL) {
		fread (buf, 1, 5000, fp);
		fclose (fp);
	
		for (cpt = 0; cpt < 5000; cpt ++) {
			if (buf [ cpt + 0 ] == 0x4d &&	/* 'M' */
			    buf [ cpt + 1 ] == 0x41 &&	/* 'A' */
			    buf [ cpt + 2 ] == 0x43 &&	/* 'C' */
			    buf [ cpt + 3 ] == 0x20) {	/* ' ' */
			    bool_signature = TRUE;
			    break;
			}
		}
	}
	g_free( buf );
	buf = NULL;
	return (bool_signature);
}
// FILE IS AIFF ?
//
/*
POS	ASCII	HEXA
00	F		46
01	O		4f
02	R		52
03	M		4d
04	
05	
06	
07	
08	A		41
09	I		49
10	F		46
11	F		46
12	C		43
13	O		4f
14	M		4d
15	M		4d
*/
gboolean FileIs_aiff (gchar *namefile)
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gboolean     bool_signature = FALSE;
	
	/*PRINT_FUNC_LF();*/

	buf = (gchar *)g_malloc0 (sizeof(gchar) * 512);
	if ((fp = fopen (namefile, "rb")) != NULL) {
		fread (buf, 1, 500, fp);
		fclose (fp);
	
		if (buf [  0 ] == 0x46 &&	// F
		    buf [  1 ] == 0x4f &&	// O
		    buf [  2 ] == 0x52 &&	// R
		    buf [  3 ] == 0x4d &&	// M
		    
		    buf [  8 ] == 0x41 &&	// A
		    buf [  9 ] == 0x49 &&	// I
		    buf [ 10 ] == 0x46 &&	// F
		    buf [ 11 ] == 0x46 &&	// F
		    buf [ 12 ] == 0x43 &&	// C
		    buf [ 13 ] == 0x4f &&	// O
		    buf [ 14 ] == 0x4d &&	// M
		    buf [ 15 ] == 0x4d)	{	// M
		    
			bool_signature = TRUE;
		}
	}
	g_free( buf );
	buf = NULL;
	return (bool_signature);
}
// FILE IS DTS ?
//
gboolean FileIs_dts (gchar *namefile)
{
	gboolean	bool_signature = FALSE;
	gchar		Ext [ 8 ] = {'\0','\0','\0','\0','\0','\0','\0','\0',};
	gchar		*Ptr;

	/* PRINT_FUNC_LF(); */

	Ptr = strrchr (namefile, '/');
	if (Ptr) {
		Ptr = strrchr (Ptr, '.');
		if (Ptr) {
			Ptr++;
			if (*Ptr) Ext [0] = toupper (*Ptr++);
			if (*Ptr) Ext [1] = toupper (*Ptr++);
			if (*Ptr) Ext [2] = toupper (*Ptr++);
			if (*Ptr) Ext [3] = toupper (*Ptr);
			Ext [4] = '\0';
			
			if (Ext[0] == 'D' && Ext[1] == 'T' && Ext[2] == 'S' && Ext[3] == '\0') bool_signature = TRUE;
		}
	}
	return (bool_signature);
}
// FILE IS "gchar *Suffixe"
//
gboolean FileIs_g_str_has_suffix (gchar *NameSong, gchar *Suffixe)
{
	gchar		*Ptr;
	gchar		*Name = NULL;
	gchar		*Ext = NULL;
	gboolean	BoolRet = FALSE;
	
	/* PRINT_FUNC_LF(); */

	if (NameSong == NULL || Suffixe == NULL) return (FALSE);
	
	
	Ptr = strrchr (NameSong, '/');
	if (Ptr) {
		Ptr = strrchr (Ptr, '.');
		if (Ptr) {
			Name = g_strdup (NameSong);
			Ptr = strrchr (Name, '.');
			Ptr ++;
			while (*Ptr) {
				*Ptr = toupper (*Ptr);
				Ptr ++;
			}
			
			Ext = g_strdup (Suffixe);
			for (Ptr = Ext; *Ptr; Ptr ++)
				*Ptr = toupper (*Ptr);
			
			BoolRet = g_str_has_suffix (Name, Ext);
			
			g_free (Ext);
			Ext = NULL;
			 
			g_free (Name);
			Name = NULL;
		}
	}
	return (BoolRet);
}
// FILE IS PNG ?
//
gboolean FileIs_png (gchar *namefile)
{
	FILE        *fp = NULL;
	gchar       *buf = NULL;
	gint         cpt;
	gboolean     bool_signature = FALSE;
	
	/*PRINT_FUNC_LF();*/

	buf = (gchar *)g_malloc0 (sizeof(gchar) * 5124);
	if ((fp = fopen (namefile, "rb")) != NULL) {
		fread (buf, 1, 5120, fp);
		fclose (fp);
	}
	
	for (cpt = 0; cpt < 5120; cpt ++) {
		if(
			buf [ cpt + 0 ] == 0x50 &&	// 'P'
			buf [ cpt + 1 ] == 0x4E &&	// 'N'
			buf [ cpt + 2 ] == 0x47		// 'G'
		    ) {	
		    bool_signature = TRUE;
		    break;
		}
	}
	g_free( buf );
	buf = NULL;
	return (bool_signature);
}
// 
// 
gboolean FileIs_image( gchar *PathNameFile )
{
	GdkPixbuf *PixBuf = NULL;
	if( NULL != PathNameFile ) {
		if( NULL != ( PixBuf = gdk_pixbuf_new_from_file( PathNameFile, NULL ))) {
			g_object_unref( PixBuf );
			return( TRUE );
		}
	}
	return( FALSE );
}
// 
// FILE IS JPG / JPEG ?
// L'EXTENTION 'JPG' OU 'JPEG' OBLIGATOIRE
// ([ BYTE 00 ] & 0xd8ff ) == 0xd8ff
// 
gboolean FileIs_jpg( gchar *p_PathName )
{
	gchar		*PtrExtPathName = NULL;
	gchar		*StrExtPathName = NULL;
	FILE		*fp = NULL;
	guint		buf[ 4 ] = { 0,0,0,0 };
	gboolean	BoolRet = FALSE;
	
	// TEST PARAMETRE
	if( NULL != p_PathName && NULL != ( PtrExtPathName = strrchr( p_PathName, '.' ))) {
			
		PtrExtPathName ++;
		
		if( strlen( PtrExtPathName ) >= 3 ) {
			
			StrExtPathName = g_strdup( PtrExtPathName );
			PtrExtPathName = StrExtPathName;

			// EXTENTIONS TO UPPER 
			while( *PtrExtPathName ) {
				*PtrExtPathName = g_unichar_toupper( *PtrExtPathName );
				PtrExtPathName ++;
			}
			
			if( 0 == g_ascii_strncasecmp( StrExtPathName, "JPG", 3 ) || 0 == g_ascii_strncasecmp( StrExtPathName, "JPEG", 4 ) ) {
	
				if( NULL != ( fp = g_fopen( p_PathName, "rb" ))) {
					fread (buf, 1, 2, fp);
					fclose( fp );
					if( (buf [ 0 ] & 0xd8ff ) == 0xd8ff ) BoolRet = TRUE;
				}
			}
			
			g_free( StrExtPathName );	StrExtPathName = NULL;
		}
	}
	
	return( BoolRet );
}








