 /*
 * file      : lib/combo.h
 * project   : xcfa
 * with      : Gtk-2
 *
 * copyright : (C) 2003 - 2015 by Claude Bulin
 *
 * xcfa - Creation d'une base de programmation en langage C de type GNU avec les autotools
 * GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * OLD ADRESS:
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * NEW ADRESS:
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define _(String) gettext (String)
#endif

#include <gtk/gtk.h>

#include "lib.h"


// OPTIONS REMOVE COMBO
// 
void libcombo_remove_options (GtkComboBox *p_combo)
{
	gint	Index;
	while (TRUE) {
		Index = gtk_combo_box_get_active (GTK_COMBO_BOX (p_combo));		
		if (Index == -1) break;
		gtk_combo_box_text_remove (GTK_COMBO_BOX_TEXT(p_combo), Index);
		gtk_combo_box_set_active (GTK_COMBO_BOX (p_combo), 0);
	}
}

