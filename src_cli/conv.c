/*
 *  file      : src/conv.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
// #include <fcntl.h>	// PROCESSUS NON-BLOQUANT

#include "global.h"
#include "prg_init.h"
#include "tags.h"
#include "conv.h"


/*
*---------------------------------------------------------------------------
* EXTERN
*---------------------------------------------------------------------------
*/
extern int kill( pid_t pid, int sig );


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

#define CONV_MAX_CARS 1024

CONV conv;


//
// Allocate Tab Args
//
char **conv_AllocTabArgs( int *p_tab )
{
	int	len_malloc =
			50 +								// nbr elements
			C_list_length( Detail.ListFile );	// list length
	char	**PtrTab = (char **)malloc( sizeof(char **) * len_malloc );

	*p_tab = 0;
	if( Detail.Nice >= 0 && Detail.Nice <= 20) {
		PtrTab [ 0 ] = C_strdup( "nice" );
		PtrTab [ 1 ] = C_strdup( "-n" );
		PtrTab [ 2 ] = C_strdup_printf( "%d", Detail.Nice );
		*p_tab = 3;
	}
	return( (char **)PtrTab );
}

//
// Remove Tab Args
//
char **conv_RemoveTab( char **p_PtrTabArgs )
{
	int	i;

	for( i = 0; p_PtrTabArgs[ i ] != NULL; i ++ ) {
		free( p_PtrTabArgs[ i ] );
		p_PtrTabArgs[ i ] = NULL;
	}
	free( p_PtrTabArgs );
	p_PtrTabArgs = NULL;

	return( (char **)NULL );
}

char **conv_get_command_line(
				TYPE_CONV	type_conv,
				char		*src,
				char		*dest,
				TAGS		*InfosTags,
				char		*options
				)
{
	char	**PtrTabArgs = NULL;
	char	**Larrbuf = NULL;
	int	i;
	int	pos;

	PtrTabArgs = conv_AllocTabArgs( &pos );

	if( type_conv == LAME_WAV_TO_MP3 ) {
		PtrTabArgs [ pos++ ] = C_strdup( "lame" );

		Larrbuf = C_strsplit( options, " " );
		for( i=0; Larrbuf[i]; i++ ) {
			PtrTabArgs [ pos++ ] = C_strdup( Larrbuf[i] );
		}
		C_strfreev( Larrbuf );

		if( NULL != InfosTags ) {
			if( Detail.tag_album || Detail.tag_artist || Detail.tag_comment || Detail.tag_genre || Detail.tag_number || Detail.tag_title || Detail.tag_year ||
				InfosTags->Album || InfosTags->Artist || InfosTags->Comment || InfosTags->Genre || InfosTags->Number || InfosTags->Title || InfosTags->Year )
			{
				PtrTabArgs [ pos++ ] = C_strdup( "--add-id3v2" );

				if( Detail.tag_album || InfosTags->Album ) {
					PtrTabArgs [ pos++ ] = C_strdup ("--tl" );
					if( Detail.tag_album )
						PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_album );
					else if( InfosTags->Album )
						PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Album );
				}
				if( Detail.tag_artist || InfosTags->Artist ) {
					PtrTabArgs [ pos++ ] = C_strdup( "--ta" );
					if( Detail.tag_artist )
						PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_artist );
					else if( InfosTags->Artist )
						PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Artist );
				}
				if( Detail.tag_comment || InfosTags->Comment ) {
					PtrTabArgs [ pos++ ] = C_strdup( "--tc" );
					if( Detail.tag_comment )
						PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_comment );
					else if( InfosTags->Comment )
						PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Comment );
				}
				if( Detail.tag_genre || InfosTags->Genre ) {
					// int NumTagMp3 = tags_get_genre_by_value( Detail.tag_genre ? Detail.tag_genre : InfosTags->Genre );
					// if( NumTagMp3 < 0 ) NumTagMp3 = 0;
					// PtrTabArgs [ pos++ ] = C_strdup( "--tg" );
					// PtrTabArgs [ pos++ ] = C_strdup_printf( "%d", NumTagMp3 );
					PtrTabArgs [ pos++ ] = C_strdup( "--tg" );
					if( Detail.tag_genre )
						PtrTabArgs [ pos++ ] = C_strdup_printf( "%s", Detail.tag_genre );
					else if( InfosTags->Genre )
						PtrTabArgs [ pos++ ] = C_strdup_printf( "%s", InfosTags->Genre );
				}
				if( Detail.tag_number || InfosTags->Number ) {
					PtrTabArgs [ pos++ ] = C_strdup( "--tn" );
					if( Detail.tag_number )
						PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_number );
					else if( InfosTags->Number )
						PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Number );
				}
				if( Detail.tag_title || InfosTags->Title ) {
					PtrTabArgs [ pos++ ] = C_strdup( "--tt" );
					if( Detail.tag_title )
						PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_title );
					else if( InfosTags->Title )
						PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Title );
				}
				if( Detail.tag_year || InfosTags->Year ) {
					PtrTabArgs [ pos++ ] = C_strdup( "--ty" );
					if( Detail.tag_year )
						PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_year );
					else if( InfosTags->Year )
						PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Year );
				}
			}
		}
		PtrTabArgs [ pos++ ] = C_strdup( src );
		PtrTabArgs [ pos++ ] = C_strdup ("-o");
		PtrTabArgs [ pos++ ] = C_strdup( dest );
		PtrTabArgs [ pos++ ] = NULL;
	}
	else if (type_conv == OGGENC_WAV_TO_OGG || type_conv == OGGENC_FLAC_TO_OGG) {

		PtrTabArgs[ pos++ ] = C_strdup ("oggenc");
		PtrTabArgs [ pos++ ] = C_strdup( src );

		Larrbuf = C_strsplit( options, " " );
		for( i=0; Larrbuf[i]; i++ ) {
			PtrTabArgs [ pos++ ] = C_strdup( Larrbuf[i] );
		}
		C_strfreev( Larrbuf );

		if( NULL != InfosTags ) {
			if( Detail.tag_album || InfosTags->Album ) {
				PtrTabArgs [ pos++ ] = C_strdup ("-l" );
				if( Detail.tag_album )
					PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_album );
				else if( InfosTags->Album )
					PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Album );
			}
			if( Detail.tag_artist || InfosTags->Artist ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-a" );
				if( Detail.tag_artist )
					PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_artist );
				else if( InfosTags->Artist )
					PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Artist );
			}
			if( Detail.tag_comment || InfosTags->Comment ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-c" );
				if( Detail.tag_comment )
					PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_comment );
				else if( InfosTags->Comment )
					PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Comment );
			}
			if( Detail.tag_genre || InfosTags->Genre ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-G" );
				if( Detail.tag_genre )
					PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_genre );
				else if( InfosTags->Genre )
					PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Genre );
			}
			if( Detail.tag_number || InfosTags->Number ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-N" );
				if( Detail.tag_number )
					PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_number );
				else if( InfosTags->Number )
					PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Number );
			}
			if( Detail.tag_title || InfosTags->Title ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-t" );
				if( Detail.tag_title )
					PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_title );
				else if( InfosTags->Title )
					PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Title );
			}
			if( Detail.tag_year || InfosTags->Year ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--date" );
				if( Detail.tag_year )
					PtrTabArgs [ pos++ ] = C_strdup( Detail.tag_year );
				else if( InfosTags->Year )
					PtrTabArgs [ pos++ ] = C_strdup( InfosTags->Year );
			}
		}

		PtrTabArgs[ pos++ ] = C_strdup ("-o");
		PtrTabArgs [ pos++ ] = C_strdup( dest );
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (type_conv == FLAC_WAV_TO_FLAC || type_conv == FLAC_FLAC_TO_WAV) {

		PtrTabArgs[ pos++ ] = C_strdup ("flac");

		Larrbuf = C_strsplit( options, " " );
		for( i=0; Larrbuf[i]; i++ ) {
			PtrTabArgs [ pos++ ] = C_strdup( Larrbuf[i] );
		}
		C_strfreev( Larrbuf );

		if( NULL != InfosTags ) {
			if( Detail.tag_album || InfosTags->Album ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				if( Detail.tag_album )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "title=%s", Detail.tag_album );
				else if( InfosTags->Album )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "title=%s", InfosTags->Album );
			}
			if( Detail.tag_artist || InfosTags->Artist ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				if( Detail.tag_artist )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "artist=%s", Detail.tag_artist );
				else if( InfosTags->Artist )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "artist=%s", InfosTags->Artist );
			}
			if( Detail.tag_comment || InfosTags->Comment ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				if( Detail.tag_comment )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Description=%s", Detail.tag_comment );
				else if( InfosTags->Comment )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Description=%s", InfosTags->Comment );
			}
			if( Detail.tag_genre || InfosTags->Genre ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				if( Detail.tag_genre )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "genre=%s", Detail.tag_genre );
				else if( InfosTags->Genre )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "genre=%s", InfosTags->Genre );
			}
			if( Detail.tag_number || InfosTags->Number ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				if( Detail.tag_number )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "tracknumber=%s", Detail.tag_number );
				else if( InfosTags->Number )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "tracknumber=%s", InfosTags->Number );
			}
			if( Detail.tag_title || InfosTags->Title ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				if( Detail.tag_title )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "title=%s", Detail.tag_title );
				else if( InfosTags->Title )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "title=%s", InfosTags->Title );
			}
			if( Detail.tag_year || InfosTags->Year ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				if( Detail.tag_year )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "date=%s", Detail.tag_year );
				else if( InfosTags->Year )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "date=%s", InfosTags->Year );
			}
		}

		if( type_conv == FLAC_FLAC_TO_WAV ) PtrTabArgs [ pos++ ] = C_strdup ("-d");

		PtrTabArgs[ pos++ ] = C_strdup ("-f");
		PtrTabArgs [ pos++ ] = C_strdup( src );
		PtrTabArgs [ pos++ ] = C_strdup ("-o");
		PtrTabArgs [ pos++ ] = C_strdup( dest );
		PtrTabArgs [ pos++ ] = NULL;
	}
	else if (type_conv == FAAC_WAV_TO_M4A) {

		PtrTabArgs [ pos++ ] = C_strdup ("faac");

		Larrbuf = C_strsplit( options, " " );
		for( i=0; Larrbuf[i]; i++ ) {
			PtrTabArgs [ pos++ ] = C_strdup( Larrbuf[i] );
		}
		C_strfreev( Larrbuf );

		if( NULL != InfosTags ) {
			if( Detail.tag_album || InfosTags->Album ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--album" );
				if( Detail.tag_album )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_album );
				else if( InfosTags->Album )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Album );
			}
			if( Detail.tag_artist || InfosTags->Artist ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--artist" );
				if( Detail.tag_artist )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_artist );
				else if( InfosTags->Artist )
					PtrTabArgs[ pos++ ] = C_strdup(  InfosTags->Artist );
			}
			if( Detail.tag_comment || InfosTags->Comment ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--comment" );
				if( Detail.tag_comment )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_comment );
				else if( InfosTags->Comment )
					PtrTabArgs[ pos++ ] = C_strdup(  InfosTags->Comment );
			}
			if( Detail.tag_genre || InfosTags->Genre ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--genre" );
				if( Detail.tag_genre )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_genre );
				else if( InfosTags->Genre )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Genre );
			}
			if( Detail.tag_number || InfosTags->Number ) {
				PtrTabArgs [ pos++ ] = C_strdup("--track" );
				if( Detail.tag_number )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_number );
				else if( InfosTags->Number )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Number );
			}
			if( Detail.tag_title || InfosTags->Title ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--title" );
				if( Detail.tag_title )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_title );
				else if( InfosTags->Title )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Title );
			}
			if( Detail.tag_year || InfosTags->Year ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--year" );
				if( Detail.tag_year )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_year );
				else if( InfosTags->Year )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Year );
			}

		}
		PtrTabArgs [ pos++ ] = C_strdup ("-o");
		PtrTabArgs [ pos++ ] = C_strdup( dest );
		PtrTabArgs [ pos++ ] = C_strdup( src );
		PtrTabArgs [ pos++ ] = NULL;
	}
	else if (type_conv == MPPENC_WAV_TO_MPC) {

		PtrTabArgs[ pos++ ] = C_strdup (prginit_get_name (NMR_musepack_tools_mppenc));
		PtrTabArgs[ pos++ ] = C_strdup ("--verbose");
		PtrTabArgs[ pos++ ] = C_strdup ("--overwrite");

		Larrbuf = C_strsplit( options, " " );
		for( i=0; Larrbuf[i]; i++ ) {
			PtrTabArgs [ pos++ ] = C_strdup( Larrbuf[i] );
		}
		C_strfreev( Larrbuf );

		if( NULL != InfosTags ) {
			if( Detail.tag_album || InfosTags->Album ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--album" );
				if( Detail.tag_album )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_album );
				else if( InfosTags->Album )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Album );
			}
			if( Detail.tag_artist || InfosTags->Artist ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--artist" );
				if( Detail.tag_artist )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_artist );
				else if( InfosTags->Artist )
					PtrTabArgs[ pos++ ] = C_strdup(  InfosTags->Artist );
			}
			if( Detail.tag_comment || InfosTags->Comment ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--comment" );
				if( Detail.tag_comment )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_comment );
				else if( InfosTags->Comment )
					PtrTabArgs[ pos++ ] = C_strdup(  InfosTags->Comment );
			}
			if( Detail.tag_genre || InfosTags->Genre ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--genre" );
				if( Detail.tag_genre )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_genre );
				else if( InfosTags->Genre )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Genre );
			}
			if( Detail.tag_number || InfosTags->Number ) {
				PtrTabArgs [ pos++ ] = C_strdup("--track" );
				if( Detail.tag_number )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_number );
				else if( InfosTags->Number )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Number );
			}
			if( Detail.tag_title || InfosTags->Title ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--title" );
				if( Detail.tag_title )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_title );
				else if( InfosTags->Title )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Title );
			}
			if( Detail.tag_year || InfosTags->Year ) {
				PtrTabArgs [ pos++ ] = C_strdup( "--year" );
				if( Detail.tag_year )
					PtrTabArgs[ pos++ ] = C_strdup( Detail.tag_year );
				else if( InfosTags->Year )
					PtrTabArgs[ pos++ ] = C_strdup( InfosTags->Year );
			}
		}
		PtrTabArgs [ pos++ ] = C_strdup( src );
		PtrTabArgs [ pos++ ] = C_strdup( dest );
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (type_conv == MAC_WAV_TO_APE || type_conv == MAC_APE_TO_WAV) {

		PtrTabArgs[ pos++ ] = C_strdup (prginit_get_name (NMR_mac));
		PtrTabArgs [ pos++ ] = C_strdup( src );
		PtrTabArgs [ pos++ ] = C_strdup( dest );
		if( type_conv == MAC_APE_TO_WAV ) {
			PtrTabArgs[ pos++ ] = C_strdup ("-d");
		}
		else {
			Larrbuf = C_strsplit( options, " " );
			for( i=0; Larrbuf[i]; i++ ) {
				PtrTabArgs [ pos++ ] = C_strdup( Larrbuf[i] );
			}
			C_strfreev( Larrbuf );
		}
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (type_conv == WVUNPACK_WAVPACK_TO_WAV) {
		PtrTabArgs[ pos++ ] = C_strdup( "wvunpack");
		PtrTabArgs[ pos++ ] = C_strdup ("-y");
		PtrTabArgs [ pos++ ] = C_strdup( src );
		PtrTabArgs[ pos++ ] = C_strdup ("-o");
		PtrTabArgs [ pos++ ] = C_strdup( dest );
		PtrTabArgs [ pos++ ] = NULL;
	}
	else if (type_conv == WAVPACK_WAV_TO_WAVPACK) {

		PtrTabArgs[ pos++ ] = C_strdup ("wavpack");
		PtrTabArgs[ pos++ ] = C_strdup ("-y");

		Larrbuf = C_strsplit( options, " " );
		for( i=0; Larrbuf[i]; i++ ) {
			PtrTabArgs [ pos++ ] = C_strdup( Larrbuf[i] );
		}
		C_strfreev( Larrbuf );

		if( NULL != InfosTags ) {
			if( Detail.tag_album || InfosTags->Album ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-w" );
				if( Detail.tag_album )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Album=%s", Detail.tag_album );
				else if( InfosTags->Album )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Album=%s", InfosTags->Album );
			}
			if( Detail.tag_artist || InfosTags->Artist ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-w" );
				if( Detail.tag_artist )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Artist=%s", Detail.tag_artist );
				else if( InfosTags->Artist )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Artist=%s", InfosTags->Artist );
			}
			if( Detail.tag_comment || InfosTags->Comment ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-w" );
				if( Detail.tag_comment )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Comment=%s", Detail.tag_comment );
				else if( InfosTags->Comment )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Comment=%s", InfosTags->Comment );
			}
			if( Detail.tag_genre || InfosTags->Genre ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-w" );
				if( Detail.tag_genre )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Genre=%s", Detail.tag_genre );
				else if( InfosTags->Genre )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Genre=%s", InfosTags->Genre );
			}
			if( Detail.tag_number || InfosTags->Number ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-w" );
				if( Detail.tag_number )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Track=%s", Detail.tag_number );
				else if( InfosTags->Number )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Track=%s", InfosTags->Number );
			}
			if( Detail.tag_title || InfosTags->Title ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-w" );
				if( Detail.tag_title )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Title=%s", Detail.tag_title );
				else if( InfosTags->Title )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Title=%s", InfosTags->Title );
			}
			if( Detail.tag_year || InfosTags->Year ) {
				PtrTabArgs [ pos++ ] = C_strdup( "-w" );
				if( Detail.tag_year )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Year=%s", Detail.tag_year );
				else if( InfosTags->Year )
					PtrTabArgs[ pos++ ] = C_strdup_printf( "Year=%s", InfosTags->Year );
			}
		}
		PtrTabArgs [ pos++ ] = C_strdup( src );
		PtrTabArgs[ pos++ ] = C_strdup ("-o");
		PtrTabArgs [ pos++ ] = C_strdup( dest );
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (type_conv == AACPLUSENC_WAV_TO_AAC) {

		PtrTabArgs[ pos++ ] = C_strdup ("aacplusenc");
		PtrTabArgs [ pos++ ] = C_strdup( src );
		PtrTabArgs [ pos++ ] = C_strdup( dest );

		Larrbuf = C_strsplit( options, " " );
		for( i=0; Larrbuf[i]; i++ ) {
			PtrTabArgs [ pos++ ] = C_strdup( Larrbuf[i] );
		}
		C_strfreev( Larrbuf );

		PtrTabArgs[ pos++ ] = NULL;
	}

	return( (char **)PtrTabArgs );
}

/*
*---------------------------------------------------------------------------
* SIGNAL ET CONVERSIONS
*---------------------------------------------------------------------------
*/
void conv_sigchld_convert (int signum)
{
	int status;

	wait(&status);
    //  if there are still children waiting
    //  re-install the signal handler
	conv.signal_numchildren_conv --;
	if( conv.signal_numchildren_conv > 0 ) {
		// re-install the signal handler
		signal (SIGCHLD, conv_sigchld_convert);
	}
}
int conv_call_exec (char **args, pid_t *p, int p_output)
{
	char	**ptr = (char **)args;
	// int	status;

	conv.signal_numchildren_conv = 0;
	if( pipe (conv.tube_conv) != 0 ) {
		fprintf (stderr, "error: pipe\n");
		exit (1);
	}
	if( (*p = fork()) == 0 ) {
		dup2 (conv.tube_conv [ 1 ], p_output);
		close (conv.tube_conv [ 1 ]);
		execvp ((char *)*(ptr+0), ptr);
		fprintf (stderr, "error: exec");
		exit (2);
	}
	conv.signal_numchildren_conv ++;
	signal (SIGCHLD, conv_sigchld_convert);
	close (conv.tube_conv [ 1 ]);

	// LE FICHIER EST OUVERT EN MODE « NON-BLOQUANT »
	// status = fcntl( conv.tube_conv [ 0 ], F_GETFL );
	// status = fcntl( conv.tube_conv [ 0 ], F_SETFL, status | O_NONBLOCK );

	return( conv.tube_conv [ 0 ] );
}


// MPLAYER
//
C_BOOL conv_with_mplayer_ARGS (char **args, TYPE_CONV p_TypeConv)
{
	int		pos = 0;
	int		fd = -1;
	int		size;
	char	buf [ CONV_MAX_CARS + 10 ];
	C_BOOL	BoolRet = TRUE;
	C_BOOL	BoolErreurMplayer = FALSE;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO );
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

			if (strstr(buf,"Audio: no sound")) {
				BoolRet = FALSE;
				BoolErreurMplayer = TRUE;
				break;
			}
			if (strstr(buf,"relocation error")) {
				BoolRet = FALSE;
				BoolErreurMplayer = TRUE;
				break;
			}
		} while ( (buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));

		if (TRUE == BoolErreurMplayer) break;

		if (buf[pos] != '\n') {
			pos ++;
			buf[pos++] = '\n';
			buf[pos] = '\0';
		}

	} while ((size > 0));
	close(fd);

	if (TRUE == BoolErreurMplayer) {
		printf("\n");
		printf("BoolErreurMplayer = TRUE\n");
		printf("KILL Process mplayer = ");
		if ((kill (conv.code_fork_conv, SIGKILL) != 0))
			printf ("ERREUR\n");
		else	printf ("OK\n");
		printf("\n");
	}

	conv.code_fork_conv = -1;
	return (BoolRet);
}
// SOX
//
void conv_with_sox_ARGS (char **args)
{
	int		fd;
	int		size;
	int		pos = 0;
	char	buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
		buf [ 0 ] = '\0';
		buf [ 1 ] = '\0';
		buf [ 2 ] = '\0';

	} while (size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
char **conv_with_sox_get_param( char *filesrc, char *filedest, char *frequence, char *voie, char *bits )
{
	char	**PtrTabArgs = NULL;
	int		pos;

	PtrTabArgs = conv_AllocTabArgs( &pos );

	PtrTabArgs [ pos++ ] = C_strdup ("sox");
	PtrTabArgs [ pos++ ] = C_strdup ("-t");
	PtrTabArgs [ pos++ ] = C_strdup (".wav");
	PtrTabArgs [ pos++ ] = C_strdup (filesrc);
	PtrTabArgs [ pos++ ] = C_strdup ("-S");
	PtrTabArgs [ pos++ ] = C_strdup ("-r");
	PtrTabArgs [ pos++ ] = C_strdup (frequence);
	PtrTabArgs [ pos++ ] = C_strdup ("-c");
	PtrTabArgs [ pos++ ] = C_strdup (voie);

	/*
	switch (atoi (bits)) {
		case 8 :  PtrTabArgs [ pos++ ] = C_strdup ("-1"); break;
		// case 16 : PtrTabArgs [ pos++ ] = C_strdup ("-2"); break;
		case 16 : PtrTabArgs [ pos++ ] = C_strdup ("-b"); PtrTabArgs [ pos++ ] = C_strdup ("16");break;
		case 24 : PtrTabArgs [ pos++ ] = C_strdup ("-3"); break;
		case 32 : PtrTabArgs [ pos++ ] = C_strdup ("-4"); break;
		case 64 : PtrTabArgs [ pos++ ] = C_strdup ("-8"); break;
		default : PtrTabArgs [ pos++ ] = C_strdup ("-2"); break;
	}
	*/
	switch (atoi (bits)) {
		case 8 :  PtrTabArgs [ pos++ ] = C_strdup ("-b"); PtrTabArgs [ pos++ ] = C_strdup ("8");	break;
		case 16 : PtrTabArgs [ pos++ ] = C_strdup ("-b"); PtrTabArgs [ pos++ ] = C_strdup ("16");	break;
		case 24 : PtrTabArgs [ pos++ ] = C_strdup ("-b"); PtrTabArgs [ pos++ ] = C_strdup ("24");	break;
		case 32 : PtrTabArgs [ pos++ ] = C_strdup ("-b"); PtrTabArgs [ pos++ ] = C_strdup ("32");	break;
		case 64 : PtrTabArgs [ pos++ ] = C_strdup ("-b"); PtrTabArgs [ pos++ ] = C_strdup ("64");	break;
		default : PtrTabArgs [ pos++ ] = C_strdup ("-b"); PtrTabArgs [ pos++ ] = C_strdup ("16");	break;
	}

	// PtrTabArgs [ pos++ ] = C_strdup ("-o");
	PtrTabArgs [ pos++ ] = C_strdup (filedest);
	PtrTabArgs [ pos++ ] = NULL;

	return( (char **)PtrTabArgs );
}
char **conv_with_sox_float_get_param( char *filesrc, char *filedest )
{
	char	**PtrTabArgs = NULL;
	int		pos;

	PtrTabArgs = conv_AllocTabArgs( &pos );

	PtrTabArgs [ pos++ ] = C_strdup ("sox");
	PtrTabArgs [ pos++ ] = C_strdup (filesrc);
	PtrTabArgs [ pos++ ] = C_strdup ("-t");
	PtrTabArgs [ pos++ ] = C_strdup ("wavpcm");
	PtrTabArgs [ pos++ ] = C_strdup ("-S");
	//PtrTabArgs [ pos++ ] = C_strdup ("-f");
	PtrTabArgs [ pos++ ] = C_strdup ("-e");
	PtrTabArgs [ pos++ ] = C_strdup ("floating-point");
	// PtrTabArgs [ pos++ ] = C_strdup ("-o");
	PtrTabArgs [ pos++ ] = C_strdup (filedest);
	PtrTabArgs [ pos++ ] = NULL;

	return( (char **)PtrTabArgs );
}
// FLAC
//
C_BOOL conv_with_flac_ARGS (char **args)
{
	int		pos = 0;
	int		fd;
	int		size;
	char	buf [ CONV_MAX_CARS + 10 ];
	char	*Ptr = NULL;
	C_BOOL	BoolNoErrorRet = TRUE;

	fd = conv_call_exec( args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		/*
		01.flac: 27% complete
		*/
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		if ((Ptr = strstr (buf, "ERROR"))) {
			/*
			coldplay_test.flac: ERROR, MD5 signature mismatch
			coldplay_test.wav: ERROR while decoding metadata
				state = FLAC__STREAM_DECODER_END_OF_STREAM
			*/
			BoolNoErrorRet = FALSE;
			break;
		}

	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;

	return (BoolNoErrorRet);
}
// MAC
//
void conv_with_mac_ARGS (char **args)
{
	int   pos = 0;
	int   fd;
	int   size;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);
	/*
	mac ./01.wav ./02.ape -c2000
	--- Monkey's Audio Console Front End (v 3.99) (c) Matthew T. Ashland ---
	Compressing (normal)...
	Progress: 8.4% (8.6 seconds remaining, 0.8 seconds total)
	Progress: 13.3% (7.6 seconds remaining, 1.2 seconds total)
	Progress: 19.3% (6.9 seconds remaining, 1.6 seconds total)
	Progress: 24.1% (6.4 seconds remaining, 2.0 seconds total)
	Progress: 26.6% (6.1 seconds remaining, 2.2 seconds total)
	Progress: 29.0% (5.9 seconds remaining, 2.4 seconds total)
	Progress: 53.1% (3.8 seconds remaining, 4.3 seconds total)
	Progress: 79.7% (1.7 seconds remaining, 6.6 seconds total)
	Progress: 100.0% (0.0 seconds remaining, 8.2 seconds total)
	Success...
	*/
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// WAVPACK
//
void conv_with_wavpack_ARGS (char **args)
{
	int       pos = 0;
	int       fd;
	int       size;
	char      buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do
	{
		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// OGGENC
//
void conv_with_oggenc_ARGS (char **args)
{
	int   pos = 0;
	int   fd;
	int   size;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		size = -1;
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while (buf[pos] != '\b' && buf[pos] != '\r' && buf[pos] != '\n' && size > 0);
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// FAAC
//
void conv_with_faac_ARGS (char **args)
{
	int   pos = 0;
	int   fd;
	int   size;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	/*
	   53/9518  (  0%)|  108.2  |    0.1/18.0   |   12.31x | 17.9
	  250/9518  (  2%)|  117.7  |    0.5/17.5   |   12.62x | 17.1
	  450/9518  (  4%)|  122.8  |    0.8/17.9   |   12.38x | 17.0
	  650/9518  (  6%)|  124.8  |    1.2/17.9   |   12.37x | 16.6
	 2100/9518  ( 22%)|  120.5  |    3.8/17.2   |   12.89x | 13.4
	 9518/9518  (100%)|  124.0  |   17.2/17.2   |   12.86x | 0.0
	*/
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// LAME
//
void conv_with_lame_ARGS (char **args)
{
	int   pos = 0;
	int   fd;
	int   size;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// OGG123
//
void conv_with_ogg123_ARGS (char **args)
{
	int   fd;
	int   pos = 0;
	int   size;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		size = 0;
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// NORMALISE
//
void conv_with_normalise_ARGS (char **args)
{
	char      buf [ CONV_MAX_CARS + 10 ];
	int       fd;
	int       size = 0;
	int       pos = 0;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do
	{
		/*
		$ normalize-audio --peak "/home/cat/Musique/CD/PEAK_Charles Aznavour/01.wav" --
		Computing levels...
		 01.wav            100% done, ETA 00:00:00 (batch 100% done, ETA 00:00:00)
		Applying adjustment of 0,82dB to /home/cat/Musique/CD/PEAK_Charles Aznavour/01.wav...
		 01.wav            100% done, ETA 00:00:00 (batch 100% done, ETA 00:00:00)
		*/

		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);
		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
void conv_with_normalise_get_PEAK_RMS_GROUP_ARGS (char **args)
{
	char     *ptr = NULL;
	char      buf [ CONV_MAX_CARS + 10 ];
	int       fd;
	int       size;
	int       pos = 0;
	double    value = 0.0;
	double    dmin = 0.0;
	double    dmax = 0.0;
	C_BOOL	  BoolPass = TRUE;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);
	do
	{
		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos] = '\0';

		if ((ptr = strstr (&buf[0], "dBFS"))) {
			while (*ptr != ' ') ptr ++;
			while (*ptr == ' ') ptr ++;

			/*printf ("buf = %s\n", buf);*/
			value = atof(ptr);
			if (BoolPass == TRUE) {
				dmin = dmax = value;
				BoolPass = FALSE;
			}
			if (dmin > value) dmin = value;
			if (dmax < value) dmax = value;
		}

	} while ( size != 0);

	/*
	printf ("value = %f, dmin = %f,  dmax = %f\n", value, dmin, dmax);
	printf ("dmax - dmin - 0.1 = %f\n", (dmax - dmin) - 0.1);
	printf ("dmin - dmax - 0.1 = %f\n", (dmin - dmax) - 0.1);
	conv.value_PEAK_RMS_GROUP_ARGS = (dmin - dmax) - 0.1;
	conv.value_PEAK_RMS_GROUP_ARGS = dmax - dmin - 0.1;
	printf ("conv.value_PEAK_RMS_GROUP_ARGS = %f\n", conv.value_PEAK_RMS_GROUP_ARGS);
	printf ("****\n");
	*/
	printf ("\nSUR LES TRES BONS CONSEILS DE @Dzef  ;-)\n");
	printf ("\tdmin = %f\n", dmin);
	printf ("\tdmax = %f\n", dmax);
	printf ("\t0.0 - dmax(%f) - 0.1 = %f\n", dmax, 0.0 - dmax - 0.1);
	conv.value_PEAK_RMS_GROUP_ARGS = 0.0 - dmax - 0.1;
	printf ("\tconv.value_PEAK_RMS_GROUP_ARGS = %f\n\n", conv.value_PEAK_RMS_GROUP_ARGS);

	close(fd);
	conv.code_fork_conv = -1;

}
// REPLAGAIN
//
void conv_with_replaygain_ARGS (char **args)
{
	int		pos = 0;
	int		size = 0;
	char	buf[CONV_MAX_CARS + 10];
	int		fd;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do
	{
		size = -1;
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);


		} while (buf[pos] != '\b' && buf[pos] != '\r' && buf[pos] != '\n' && size > 0);
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
}
// AACPLUSENC
//
void conv_with_aacplusenc_ARGS (char **args)
{
	int		pos = 0;
	int		size = 0;
	char	buf[CONV_MAX_CARS + 10];
	int		fd;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';
	} while ( size != 0);

	conv.code_fork_conv = -1;
	close(fd);
}
// MPPENC
//
void conv_with_mppenc_ARGS (char **args)
{
	int   pos = 0;
	int   fd;
	int   size;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// MPG321
//
void conv_with_mpg321_ARGS (char **args)
{
	int   fd;
	int   pos = 0;
	int   size = -1;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// SHORTEN
//
void conv_with_shorten_ARGS (char **args)
{
	int   fd;
	int   size;
	int   pos = 0;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// FAAD
//
void conv_with_faad_ARGS (char **args)
{
	int   fd;
	int   size;
	int   pos = 0;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	/*
	39% decoding 05.m4a.
	52% decoding 05.m4a.
	60% decoding 05.m4a.
	67% decoding 05.m4a.
	77% decoding 05.m4a.
	84% decoding 05.m4a.
	*/
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

			if (strstr(buf,"Audio: no sound")) {
				break;
			}

		} while ( (buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));

		if (buf[pos] != '\n') {
			pos ++;
			buf[pos++] = '\n';
			buf[pos] = '\0';
		}
	} while ((size > 0));

	close(fd);
	conv.code_fork_conv = -1;
}
// MPPDEC
//
void conv_with_mppdec_ARGS (char **args)
{
	int   fd;
	int   size;
	int   pos = 0;
	char  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
	} while ( size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// WVUNPACK_WAVPACK_TO_WAV
//
void conv_with_wvunpack_ARGS (char **args)
{
	int       fd;
	int       size;
	int       pos = 0;
	char      buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO ); // STDERR_FILENO  STDOUT_FILENO  STDIN_FILENO
	do
	{
		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos] = '\0';
		/*
		fish@debian:~/Musique/aaa/new$ wvunpack -y -m ./20.wv

		 WVUNPACK  Hybrid Lossless Audio Decompressor  Linux Version 4.32  2006-04-05
		 Copyright (c) 1998 - 2005 Conifer Software.  All Rights Reserved.

		restoring ./20.wav,  34% done...

		original md5:  2b74aed5a5d62d1c3b0e6b96d5147cc9
		unpacked md5:  2b74aed5a5d62d1c3b0e6b96d5147cc9
		restored ./20.wav in 17.23 secs (lossless, 26.45%)
		else if ((ptr = strstr (&buf[0], "original md5:"))) {
		}
		else if ((ptr = strstr (&buf[0], "unpacked md5:"))) {
		}
		else if ((ptr = strstr (&buf[0], "restored"))) {
		}
		*/
	} while (size != 0);

	close(fd);
	conv.code_fork_conv = -1;
}
// A52DEC
//
void conv_with_a52dec_ARGS (char **args)
{
	int			pos = 0;
	int			size = 0;
	char		buf[CONV_MAX_CARS + 10];
	int			fd;
	FILE		*fp = NULL;
	char		*FileSave = C_strdup (args[ 8 ]);

	free (args[ 7 ]);	args[ 7 ] = NULL;
	free (args[ 8 ]);	args[ 8 ] = NULL;


	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);
	fp = fopen (FileSave, "w");
	do {
		pos = 0;
		do {
			/*
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			*/
			size = read(fd, &buf [ pos ], 1);
			fprintf (fp, "%c", (short)buf [ 0 ]);


		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';
	} while ( size != 0);

	fclose (fp);
	free (FileSave);	FileSave = NULL;

	conv.code_fork_conv = -1;
	close(fd);
}

// CALL CONV
//
C_BOOL conv_to_convert( char **p_TabArgs, TYPE_CONV type_conv, char *info )
{
	int		pos = 0;
	C_BOOL	RetBool = TRUE;

	if( TRUE == Detail.BoolVerboseMode ) {
		printf ("!-----------------------------------------------------------!\n");
		printf ("!  %s\n", info);
		printf ("!-----------------------------------------------------------!\n");
		printf ("!  ");
		for (pos = 0; p_TabArgs[ pos ] != NULL; pos ++)
			printf ("%s ", p_TabArgs[ pos ]);
		printf ("\n!-----------------------------------------------------------!\n");
		for (pos = 0; p_TabArgs[ pos ] != NULL; pos ++)
			printf ("p_TabArgs[ %02d ] = %s\n", pos, p_TabArgs[ pos ]);
		printf ("\n");
	}

	switch (type_conv) {
	case NONE_CONV :
		break;
	case FLAC_FLAC_TO_WAV :
	case FLAC_WAV_TO_FLAC :
		RetBool = conv_with_flac_ARGS( p_TabArgs );
		break;
	case LAME_WAV_TO_MP3 :
	case LAME_FLAC_TO_MP3 :
		conv_with_lame_ARGS( p_TabArgs );
		break;
	case OGGENC_WAV_TO_OGG :
	case OGGENC_FLAC_TO_OGG :
		conv_with_oggenc_ARGS( p_TabArgs );
		break;
	case OGG123_OGG_TO_WAV :
		conv_with_ogg123_ARGS( p_TabArgs );
		break;
	case MPG321_MP3_TO_WAV :
		conv_with_mpg321_ARGS( p_TabArgs );
		break;
	case SOX_WAV_TO_WAV :
		conv_with_sox_ARGS( p_TabArgs );
		break;
	case SHORTEN_SHN_TO_WAV :
		conv_with_shorten_ARGS( p_TabArgs );
		break;
	case FAAD_M4A_TO_WAV :
		conv_with_faad_ARGS( p_TabArgs );
		break;
	case FAAC_WAV_TO_M4A :
		conv_with_faac_ARGS( p_TabArgs );
		break;
	case MPLAYER_WMA_TO_WAV :
	case MPLAYER_RM_TO_WAV :
	case MPLAYER_DTS_TO_WAV :
	case MPLAYER_AIFF_TO_WAV :
	case MPLAYER_WAV_TO_WAV :
	case MPLAYER_AUDIO_TO_WAV :
	case MPLAYER_M4A_TO_WAV :
	case MPLAYER_OGG_TO_WAV :
	case MPLAYER_MP3_TO_WAV :
	case MPLAYER_MPC_TO_WAV :
		RetBool = conv_with_mplayer_ARGS( p_TabArgs, type_conv );
		break;
	case MPPDEC_MPC_TO_WAV :
		conv_with_mppdec_ARGS( p_TabArgs );
		break;
	case MPPENC_WAV_TO_MPC :
		conv_with_mppenc_ARGS( p_TabArgs );
		break;
	case MAC_APE_TO_WAV :
	case MAC_WAV_TO_APE :
		conv_with_mac_ARGS( p_TabArgs );
		break;
	case WAVPACK_WAV_TO_WAVPACK :
		conv_with_wavpack_ARGS( p_TabArgs );
		break;
	case WVUNPACK_WAVPACK_TO_WAV :
		conv_with_wvunpack_ARGS( p_TabArgs );
		break;
	case AACPLUSENC_WAV_TO_AAC :
		conv_with_aacplusenc_ARGS( p_TabArgs );
		break;
	case A52DEC_AC3_TO_WAV :
		conv_with_a52dec_ARGS( p_TabArgs );
		break;

	case REPLAYGAIN :
		conv_with_replaygain_ARGS( p_TabArgs );
		break;
	case NORMALISE_EXEC :
		conv_with_normalise_ARGS( p_TabArgs );
		break;
	case NORMALISE_GET_LEVEL:
		conv_with_normalise_get_PEAK_RMS_GROUP_ARGS( p_TabArgs );
		break;
	}

	return( RetBool );
}
//
// Algo retravaille depuis:
//	C EN ACTION - 2 ieme Edition - Yves Mettier
//
C_BOOL conv_copy_src_to_dest (char *filesrc, char *filedest)
{
	FILE	*fn = NULL;
	FILE	*fe = NULL;
	char	buf [ BUFSIZ + 10 ];	// 8192
	size_t	size = utils_get_size_file (filesrc);
	long	cpt;
	size_t	l_read;

	if (0 == size) {
		fprintf (stderr, "La taille de : %s est NULLE !!!\n", filesrc);
		return (FALSE);
	}
	if (NULL == (fe = fopen (filesrc, "r"))) {
		fprintf (stderr, "Echec ouverture en lecture de: %s\n", filesrc);
		return (FALSE);
	}
	if (NULL == (fn = fopen (filedest, "w"))) {
		fprintf (stderr, "Echec ouverture en ecriture de: %s\n", filedest);
		fclose (fe);
		return (FALSE);
	}

	if( TRUE == Detail.BoolVerboseMode ) {
		PRINT_FUNC_LF();
		printf ("\t%s\n\t%s\n", filesrc, filedest);
	}
	cpt = 0;
	while ( ! feof (fe)) {

		l_read = fread (buf, sizeof (*buf), BUFSIZ, fe);
		if (ferror (fe)) {
			fprintf (stderr, "Erreur de lecture depuis: %s\n", filesrc );
			fclose (fn);
			fclose (fe);
			return (FALSE);
		}
		fwrite (buf, sizeof (*buf), l_read, fn);
		if (ferror (fn)) {
			fprintf (stderr, "Erreur d'ecriture depuis: %s\n", filedest );
			fclose (fn);
			fclose (fe);
			return (FALSE);
		}
		fflush( fn );
		cpt ++;
	}
	fclose (fn);
	fclose (fe);

	return (TRUE);
}








