/*
 *  file      : src/file_is.h
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifndef file_is_h
#define file_is_h 1

#include "clib.h"

// FILE_IS.C
//
C_BOOL	FileIs_vidm4a( char *namefile );
C_BOOL	FileIs_m4a( char *namefile );
C_BOOL	FileIs_wavpack( char *namefile );
C_BOOL	FileIs_wav( char *namefile );
C_BOOL	FileIs_cue( char *namefile );
C_BOOL	FileIs_shn( char *namefile );
C_BOOL	FileIs_rm( char *namefile );
C_BOOL	FileIs_ogg( char *namefile );
C_BOOL	FileIs_mpc( char *namefile );
C_BOOL	FileIs_ac3( char *namefile );
C_BOOL	FileIs_mp3( char *namefile );
C_BOOL	FileIs_flac( char *namefile );
C_BOOL	FileIs_ape( char *namefile );
C_BOOL	FileIs_aiff( char *namefile );
C_BOOL	FileIs_dts( char *namefile );
C_BOOL	FileIs_png( char *namefile );

#endif
