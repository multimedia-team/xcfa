/*
 *  file      : src/split.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "tags.h"
#include "conv.h"


//
// Split file 'char *p_pathnamefile' from 'double p_split_begin_ss' for a length of 'int p_split_length_ss'
//
void split_wav( char *p_pathnamefile, double p_split_begin_ss, int p_split_length_ss )
{
	WAVE	WaveHeader;
	double	Percent = 0.;
	double	PercentBegin;
	double	PercentEnd;
	char	*NewName = NULL;
	char	*buffer = NULL;
	FILE	*fp = NULL;
	size_t	Ret;
	size_t	RetRead;
	size_t	WriteValue;

	if (FALSE == tagswav_read_file( p_pathnamefile, &WaveHeader )) {
		tagswav_close_file( &WaveHeader );
		printf ( "%s", BOLD_RED );
		printf("\n!--- S P L I T ----------------\n");
		printf ( "! File not found to split operation: \"%s\"", p_pathnamefile );
		printf ( "%s\n\n", RESET );
		return;
	}
	if( (p_split_begin_ss + p_split_length_ss) > (WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate) ) {
		printf ( "%s", BOLD_RED );
		printf("\n!--- S P L I T ----------------\n");
		printf ( "! The size of the '--split' and '--length' is greater than the size of the file." );
		printf ( "! %s\n", p_pathnamefile );
		printf ( "! %d secondes\n", WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate );
		printf ( "%s\n\n", RESET );
		tagswav_close_file( &WaveHeader );
		return;
	}

	PercentBegin = (((double)p_split_begin_ss * (double)WaveHeader.FMT.ByteRate) /  WaveHeader.DATA.Subchunk2Size) * 100.0;
	PercentEnd = (((double)p_split_length_ss * (double)WaveHeader.FMT.ByteRate) /  WaveHeader.DATA.Subchunk2Size) * 100.0;
	PercentEnd += PercentBegin;

	NewName = C_strdup_printf( "%s.split.wav", p_pathnamefile );

	printf("\n!--- S P L I T ---------------------------------------------!\n");
	printf( "! %s\n", p_pathnamefile );
	printf( "! %s\n", NewName );

	buffer = (char *)malloc ((sizeof(char) * BUFSIZ) + 10);

	fp = fopen( NewName, "w" );
	// ECRITURE ENTETE FICHIER WAV
	fwrite( &WaveHeader.RIFF.ChunkID, 4, 1, fp );		// RIFF
	fwrite( &WaveHeader.RIFF.ChunkSize, 4, 1, fp );		// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
	fwrite( &WaveHeader.RIFF.Format, 4, 1, fp );		// WAVE
	fwrite( &WaveHeader.FMT.Subchunk1ID, 4, 1, fp );	// 'fmt '
	fwrite( &WaveHeader.FMT.Subchunk1Size, 4, 1, fp );	// taille en octet des données à suivre
	fwrite( &WaveHeader.FMT.AudioFormat, 2, 1, fp );	// format de compression (une valeur autre que 1 indique une compression)
	fwrite( &WaveHeader.FMT.NumChannels, 2, 1, fp );	// nombre de canaux
	fwrite( &WaveHeader.FMT.SampleRate, 4, 1, fp );		// fréquence d'échantillonage (nombre d'échantillons par secondes)
	fwrite( &WaveHeader.FMT.ByteRate, 4, 1, fp );		// nombre d'octects par secondes
	fwrite( &WaveHeader.FMT.Blockalign, 2, 1, fp );		// nombre d'octects pour coder un échantillon
	fwrite( &WaveHeader.FMT.BitsPerSample, 2, 1, fp );	// nombre de bits pour coder un échantillon
	fwrite( &WaveHeader.DATA.Subchunk2ID, 4, 1, fp );	// 'data'
	fwrite( &WaveHeader.DATA.Subchunk2Size, 4, 1, fp );	//  taille des données audio( nombre total d'octets codant les données audio)

	RetRead = 0;
	WriteValue = 0;
	while(( Ret = fread (buffer,  1, BUFSIZ, WaveHeader.file )) > 0) {
		RetRead += Ret;
		Percent = (float)(RetRead / (float)WaveHeader.DATA.Subchunk2Size) * 100.0;
		if( Percent >= PercentBegin ) {
			fwrite( buffer, Ret, 1, fp );
			WriteValue += Ret;
	 	}
		if( Percent > PercentEnd ) break;
	}
	/*
	fseek( fp, 4L, SEEK_SET );
	WriteValue += 44;
	WriteValue -= 8;
	fwrite( &WriteValue, 4, 1, fp );	// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
	fseek( fp, 40L, SEEK_SET );
	WriteValue += 8;
	WriteValue -= 44;
	fwrite( &WriteValue, 4, 1, fp );	//  taille des données audio( nombre total d'octets codant les données audio )
	*/
	fseek( fp, 40L, SEEK_SET );
	fwrite( &WriteValue, 4, 1, fp );	//  taille des données audio( nombre total d'octets codant les données audio )
	fseek( fp, 4L, SEEK_SET );
	WriteValue += 44;
	WriteValue -= 8;
	fwrite( &WriteValue, 4, 1, fp );	// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent

	tagswav_close_file( &WaveHeader );
	fclose (fp );
	fp = NULL;

	conv_copy_src_to_dest( NewName, p_pathnamefile );

	remove( NewName );

	free( NewName );
	NewName = NULL;
	free( buffer );
	buffer = NULL;
}


