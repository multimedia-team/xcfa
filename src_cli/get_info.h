 /*
 *  file      : get_info.h
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */



#ifndef get_info_h
#define get_info_h 1

#include "global.h"

typedef struct {
	uint	 SecTime;
	char	*time;
	char	*size;
} SHNTAG;

SHNTAG			*GetInfo_shntool (char *file);
SHNTAG			*GetInfo_free_shntool (SHNTAG *ShnTag);
CString			*GetInfo_file (char *file);
CString			*GetInfo_checkmp3 (char *file);
CString			*GetInfo_faad (char *file);
CString			*GetInfo_ogginfo (char *file);
CString			*GetInfo_which (char *file);
CString			*GetInfo_metaflac (char *file);
char			*GetInfo_cpu (void);
void			GetInfo_cpu_print (void);
char			*GetInfo_cpu_str (void);
TYPE_FILE_IS	GetInfo_file_is (char *PathName);
long			GetInfo_level_df (void);
C_BOOL			GetInfo_level_bool_percent (void);
int				GetInfo_level_get_from( TYPE_FILE_IS TypeFileIs, char *file );
void			GetInfo_set( INFO *p_Info );
void			GetInfo_remove( INFO *p_Info );
void			GetInfo_cpu_print (void);

#endif

