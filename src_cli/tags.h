/*
 *  file      : src/tags.h
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */



#ifndef tags_h
#define tags_h 1

#include <stdio.h>

typedef enum {
	TAG_WAVE_IS_NONE = 0,
	TAG_WAVE_IS_FMT,
	TAG_WAVE_IS_BEXT
} TAG_WAV_IS_FMT_BEXT;


typedef struct {

	struct {
		char	ChunkID[4];		//(  4 octets) : contient les lettres "RIFF" pour indiquer que le fichier est codé selon la norme RIFF
		uint 	ChunkSize;		//(  4 octets) : taille du fichier entier en octets( sans compter les 8 octets de ce champ et le champ précédent CunkID
		char	Format[4];		//(  4 octets) : correspond au format du fichier donc ici, contient les lettres "WAVE" car fichier est au format wave
	} RIFF;

	TAG_WAV_IS_FMT_BEXT	TagWavIsFmtBext;

	struct {
		char	Subchunk1ID[4];	//(  4 octets) : contient les lettres "fmt " pour indiquer les données à suivre décrivent le format des données audio
		uint 	Subchunk1Size;	//(  4 octets) : taille en octet des données à suivre( qui suivent cette variable) 16 Pour un fichier PCM
		short	AudioFormat;	//(  2 octets) : format de compression( une valeur autre que 1 indique une compression)
		short	NumChannels;	//(  2 octets) : nombre de canaux
								// 	1 pour mono,
								// 	2 pour stéréo
								// 	3 pour gauche, droit et centre
								// 	4 pour face gauche, face droit, arrière gauche, arrière droit
								// 	5 pour gauche, centre, droit, surround( ambiant)
								// 	6 pour centre gauche, gauche, centre, centre droit, droit, surround( ambiant)
		uint 	SampleRate;		//(  4 octets) : fréquence d'échantillonage, ex 44100, 44800( nombre d'échantillons par secondes)
		uint 	ByteRate;		//(  4 octets) : nombre d'octects par secondes
		short	Blockalign;		//(  2 octets) : nombre d'octects pour coder un échantillon
		short	BitsPerSample;	//(  2 octets) : nombre de bits pour coder un échantillon
	} FMT;

	struct {
		char	Subchunk2ID[4];	//(  4 octets) : contient les lettres "data" pour indiquer que les données à suivre sont les données audio( les échantillons et)
		uint 	Subchunk2Size;	//(  4 octets) : taille des données audio( nombre total d'octets codant les données audio)
		uint	*data;			//(  4 octets) : données audio... les échantillons
								//     DATAS[] : [Octets du Sample 1 du Canal 1] [Octets du Sample 1 du Canal 2] [Octets du Sample 2 du Canal 1] [Octets du Sample 2 du Canal 2]
	} DATA;

	FILE		*file;			// Handle du fichier si different de NULL
} WAVE;

//
// ---------------------------------------------------------------------------
//  TAGS_WAV.C
// ---------------------------------------------------------------------------
//
INFO_WAV	*tagswav_remove_info( INFO_WAV *info );
INFO_WAV	*tagswav_get_info( INFO *p_info );
C_BOOL		tagswav_file_GetBitrate( char *namefile, int *Channels, int *Hertz, int *Bits );
C_BOOL		tagswav_read_file( char *wave_file, WAVE *WaveHeader );
void		tagswav_close_file( WAVE *WaveHeader );
void		tagswav_print_header( char *p_PathNameFile );

//
// ---------------------------------------------------------------------------
//  TAGS.C
// ---------------------------------------------------------------------------
//
typedef struct {
	int   num;
	char *name;
} STRUCT_TAGS_FILE_MP3;
//
//
extern	STRUCT_TAGS_FILE_MP3 StructTagsFileMp3 [];

TAGS		*tags_alloc( void );
TAGS		*tags_remove( TAGS *tags );
void		tags_complementation( TAGS *p_tags, char *path_name_file );
int			tags_get_genre_by_value( char *p_name );
char		*tags_get_genre_by_name( int value );
void		tags_print( char *p_namefile, TAGS *p_tags );


//
// ---------------------------------------------------------------------------
//  TAGS_FLAC.C
// ---------------------------------------------------------------------------
//
INFO_FLAC	*tagsflac_remove_info( INFO_FLAC *info );
INFO_FLAC	*tagsflac_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_MP3.C
// ---------------------------------------------------------------------------
//
INFO_MP3	*tagsmp3_remove_info( INFO_MP3 *info );
INFO_MP3	*tagsmp3_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_OGG.C
// ---------------------------------------------------------------------------
//
INFO_OGG	*tagsogg_remove_info( INFO_OGG *info );
INFO_OGG	*tagsogg_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_M4A_.C
// ---------------------------------------------------------------------------
//
INFO_M4A	*tagsm4a_remove_info( INFO_M4A *info );
INFO_M4A	*tagsm4a_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_AAC_.C
// ---------------------------------------------------------------------------
//
INFO_AAC	*tagsaac_remove_info( INFO_AAC *info );

//
// ---------------------------------------------------------------------------
//  TAGS_SHN.C
// ---------------------------------------------------------------------------
//
INFO_SHN	*tagsshn_remove_info( INFO_SHN *info );
INFO_SHN	*tagsshn_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_WMA.C
// ---------------------------------------------------------------------------
//
INFO_WMA	*tagswma_remove_info( INFO_WMA *info );
INFO_WMA	*tagswma_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_MPC.C
// ---------------------------------------------------------------------------
//
INFO_MPC	*tagsmpc_remove_info( INFO_MPC *info );
INFO_MPC	*tagsmpc_get_info( INFO *p_info );
void		tagsmpc_print_header( char *mpc_file );

//
// ---------------------------------------------------------------------------
//  TAGS_APE.C
// ---------------------------------------------------------------------------
//
INFO_APE	*tagsape_remove_info( INFO_APE *info );
INFO_APE	*tagsape_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_WAVPACK.C
// ---------------------------------------------------------------------------
//
INFO_WAVPACK	*tagswavpack_remove_info( INFO_WAVPACK *info );
INFO_WAVPACK	*tagswavpack_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_RM.C
// ---------------------------------------------------------------------------
//
INFO_RM		*tagsrm_remove_info( INFO_RM *info );
INFO_RM		*tagsrm_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_DTS.C
// ---------------------------------------------------------------------------
//
INFO_DTS	*tagsdts_remove_info( INFO_DTS *info );
INFO_DTS	*tagsdts_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_AIFF.C
// ---------------------------------------------------------------------------
//
INFO_AIFF	*tagsaiff_remove_info( INFO_AIFF *info );
INFO_AIFF	*tagsaiff_get_info( INFO *p_info );

//
// ---------------------------------------------------------------------------
//  TAGS_AC3.C
// ---------------------------------------------------------------------------
//
INFO_AC3	*tagsac3_remove_info( INFO_AC3 *info );
INFO_AC3	*tagsac3_get_info( INFO *p_info );


#endif


