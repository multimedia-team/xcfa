/*
 *  file      : src/tags_flac.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/




/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/

INFO_FLAC *tagsflac_remove_info (INFO_FLAC *info)
{
	if (info) {
		free (info->time);

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_FLAC *)NULL);
}


void tags_flac_print( INFO_FLAC *ptrinfo )
{
	printf( "time            = %s\n", ptrinfo->time );
	printf( "SecTime         = %d\n", ptrinfo->SecTime );
}


INFO_FLAC *tagsflac_get_info (INFO *p_info)
{
	TagLib_File	*file;
	TagLib_Tag	*tag;
	INFO_FLAC	*ptrinfo = NULL;
	const TagLib_AudioProperties *properties;
	int		m;
	int		s;
	int		sec;

	ptrinfo = (INFO_FLAC *)malloc (sizeof (INFO_FLAC));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc ();

	if ((file = taglib_file_new (p_info->path_name_file))) {

		taglib_set_strings_unicode(FALSE);
		tag = taglib_file_tag(file);
		properties = taglib_file_audioproperties(file);

		ptrinfo->tags->Title     = C_strdup (taglib_tag_title(tag));
		ptrinfo->tags->Artist    = C_strdup (taglib_tag_artist(tag));
		ptrinfo->tags->Album     = C_strdup (taglib_tag_album(tag));
		ptrinfo->tags->IntYear   = taglib_tag_year(tag);
		ptrinfo->tags->Year      = C_strdup_printf ("%d", ptrinfo->tags->IntYear);
		ptrinfo->tags->Comment   = C_strdup (taglib_tag_comment(tag));
		ptrinfo->tags->IntNumber = taglib_tag_track(tag);
		ptrinfo->tags->Number    = C_strdup_printf ("%d", ptrinfo->tags->IntNumber);
		ptrinfo->tags->Genre     = C_strdup (taglib_tag_genre(tag));
		ptrinfo->tags->IntGenre  = tags_get_genre_by_value (ptrinfo->tags->Genre);
		ptrinfo->SecTime         = taglib_audioproperties_length(properties);

		sec = taglib_audioproperties_length(properties);
		s = sec % 60; sec /= 60;
		m = sec % 60; sec /= 60;
		if (sec > 0) ptrinfo->time = C_strdup_printf ("%02d:%02d:%02d", sec, m, s);
		else         ptrinfo->time = C_strdup_printf ("00:%02d:%02d", m, s);

		/*
		printf("title   - \"%s\"\n", ptrinfo->tags->Title );
		printf("artist  - \"%s\"\n", ptrinfo->tags->Artist );
		printf("album   - \"%s\"\n", ptrinfo->tags->Album );
		printf("year    - \"%s\"\n", ptrinfo->tags->Year );
		printf("comment - \"%s\"\n", ptrinfo->tags->Comment );
		printf("track   - \"%i\"\n", taglib_tag_track(tag));
		printf("genre   - \"%s\"\n", ptrinfo->tags->Genre );
		printf("-- AUDIO --\n");
		printf("bitrate     - %i\n", taglib_audioproperties_bitrate(properties));
		printf("sample rate - %i\n", taglib_audioproperties_samplerate(properties));
		printf("channels    - %i\n", taglib_audioproperties_channels(properties));
		printf("length      - %i:%02i\n", minutes, seconds);
		*/

		taglib_tag_free_strings();
		taglib_file_free (file);
	}
	else {
		tags_complementation( ptrinfo->tags,  p_info->path_name_file );
	}
	return ((INFO_FLAC *)ptrinfo);
}


