/*
 *  file      : src/tags_wavpack.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>
#include "get_info.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

/*
typedef struct {
    char ckID [4];              "wvpk"
    uint32_t ckSize;            size of entire block (minus 8, of course)
    uint16_t version;           0x402 to 0x410 are currently valid for decode
    uchar track_no;             track number (0 if not used, like now)
    uchar index_no;             track sub-index (0 if not used, like now)
    uint32_t total_samples;     total samples for entire file, but this is
                                only valid if block_index == 0 and a value of
                                -1 indicates unknown length
    uint32_t block_index;       index of first sample in block relative to
                                beginning of file (normally this would start
                                at 0 for the first block)
    uint32_t block_samples;     number of samples in this block (0 = no audio)
    uint32_t flags;             various flags for id and decoding
    uint32_t crc;               crc for actual decoded data
} WavpackHeader;
*/

/*
typedef struct {
	char id [ 3 ];
	char title [ 30 ];
	char interprete [ 30 ];
	char album [ 30 ];
	char annee [ 4 ];
	char comment [ 30 ];
	char genre [ 1 ];
} TAGS_WAVPACK;
*/


/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/


INFO_WAVPACK *tagswavpack_remove_info (INFO_WAVPACK *info)
{
	if (info) {
		if (NULL != info->time)		{ free (info->time);		info->time = NULL;	}
		if (NULL != info->size)		{ free (info->size);		info->size = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_WAVPACK *)NULL);
}


void tags_wavpack_print( INFO_WAVPACK *ptrinfo )
{
	printf( "time            = %s\n", ptrinfo->time );
	printf( "SecTime         = %d\n", ptrinfo->SecTime );
	printf( "size            = %s\n", ptrinfo->size );
}


INFO_WAVPACK *tagswavpack_get_info( INFO *p_info )
{
	INFO_WAVPACK	*ptrinfo = NULL;
	SHNTAG		*ShnTag = GetInfo_shntool ( p_info->path_name_file );
	int		m;
	int		s;
	int		sec;

	ptrinfo = (INFO_WAVPACK *)malloc (sizeof (INFO_WAVPACK));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc ();

	ptrinfo->size     = C_strdup (ShnTag->size);

	ptrinfo->SecTime =
	sec = ShnTag->SecTime;
	s = sec % 60; sec /= 60;
	m = sec % 60; sec /= 60;
	if (sec > 0) ptrinfo->time = C_strdup_printf ("%02d:%02d:%02d", sec, m, s);
	else         ptrinfo->time = C_strdup_printf ("00:%02d:%02d", m, s);

	tags_complementation( ptrinfo->tags,  p_info->path_name_file );
	ShnTag = GetInfo_free_shntool (ShnTag);

	return( (INFO_WAVPACK *)ptrinfo );
}
