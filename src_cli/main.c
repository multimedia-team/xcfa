/*
 *  file      : src/main.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define _(String) gettext (String)
#endif

#include <locale.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fnmatch.h>
#include <unistd.h>

#include "global.h"
#include "get_info.h"
#include "prg_init.h"
#include "tags.h"


char *get_current_dir_name(void);	// The caller should free(3) the returned buffer.

DETAIL Detail = {
	FALSE,				// C_BOOL		BoolVerboseMode
	-1,					// int			Nice
	NULL,				// CList		*ListSearch
	NULL,				// char			*OutputDir
	{},					// C_BOOL		BoolTypeDest[ NBR_FILE_TO ]
	FALSE,				// C_BOOL		BoolRecursiveMode
	NULL,				// CList		*ListFile
	NULL,				// char			*Op_flac
	NULL,				// char			*Op_ape
	NULL,				// char			*Op_wavpack
	NULL,				// char			*Op_ogg
	NULL,				// char			*Op_m4a
	NULL,				// char			*Op_aac
	NULL,				// char			*Op_mpc
	NULL,				// char			*Op_mp3
	FALSE,				// C_BOOL		BoolExtract2Src
	FALSE,				// C_BOOL		BoolNoTag
	NULL,				// char			*tag_album
	NULL,				// char			*tag_artist
	NULL,				// char			*tag_title
	NULL,				// char			*tag_number
	NULL,				// char			*tag_genre
	NULL,				// char			*tag_year
	NULL,				// char			*tag_comment
	NULL,				// char			*tag_description

	0.0,				// double		split
	0,					// int			split_length

	0,					// int			frequency
	0,					// int			track
	0,					// int			quantification

	OP_REPLAYGAIN_NONE,	// OP_REPLAYGAIN	replaygain

						// Normalize
	FALSE,				// C_BOOL		peak
	FALSE,				// C_BOOL		peak_album
	0,					// int			mix_rms_album
	0,					// int			fix_rms

	FALSE,				// C_BOOL		bool_dest				// --dest
	FALSE,				// C_BOOL		bool_split				// --split --length
	FALSE,				// C_BOOL		bool_setting_wav		// --frequency --track --quantification
	FALSE,				// C_BOOL		bool_normalize			// --peak  --peak_album  --mix_rms_album  --fix_rms

	FALSE,				// C_BOOL		bool_info_files
	FALSE,				// C_BOOL		bool_info_tags
	FALSE,				// C_BOOL		bool_info_head

	OP_CUE_NONE			// OP_CUE		cue

};


typedef struct {
	TYPE_FILE_IS	TypeFileIs;
	C_BOOL			*BoolFound_1;
	char			*Err_1;
	C_BOOL			*BoolFound_2;
	char			*Err_2;
	C_BOOL			*BoolFound_3;
	char			*Err_3;
} TST_FROM;
TST_FROM ToWav[] = {
{ FILE_IS_FLAC,		&PrgInit.bool_flac,		"FLAC not found",					NULL, NULL, NULL, NULL },
{ FILE_IS_APE,		&PrgInit.bool_ape,		"MAC not found",					NULL, NULL, NULL, NULL },
{ FILE_IS_WAVPACK,	&PrgInit.bool_wavpack,	"WAVPACK or WVUNPACK not found",	NULL, NULL, NULL, NULL },
{ FILE_IS_OGG,		&PrgInit.bool_mplayer,	"MPLAYER not found",				&PrgInit.bool_sox,		"SOX not found",		NULL, NULL },
{ FILE_IS_M4A,		&PrgInit.bool_mplayer,	"MPLAYER not found",				NULL, NULL, NULL, NULL },
{ FILE_IS_MPC,		&PrgInit.bool_mplayer,	"MPLAYER not found",				NULL, NULL, NULL, NULL },
{ FILE_IS_MP3,		&PrgInit.bool_mplayer,	"MPLAYER not found",				&PrgInit.bool_sox,		"SOX not found",		NULL, NULL },
{ FILE_IS_WMA,		&PrgInit.bool_mplayer,	"MPLAYER not found",				NULL, NULL, NULL, NULL },
{ FILE_IS_SHN,		&PrgInit.bool_shorten,	"SHORTEN not found",				NULL, NULL, NULL, NULL },
{ FILE_IS_RM,		&PrgInit.bool_mplayer,	"MPLAYER not found",				NULL, NULL, NULL, NULL },
{ FILE_IS_DTS,		&PrgInit.bool_mplayer,	"MPLAYER not found",				NULL, NULL, NULL, NULL },
{ FILE_IS_AIFF,		&PrgInit.bool_mplayer,	"MPLAYER not found",				NULL, NULL, NULL, NULL },
{ FILE_IS_AC3,		&PrgInit.bool_a52dec,	"A52DEC not found",					NULL, NULL, NULL, NULL },
{ FILE_IS_WAV,		&PrgInit.bool_sox,		"SOX not found",					NULL, NULL, NULL, NULL },
{ FILE_IS_MP3,		&PrgInit.bool_sox,		"SOX not found",					&PrgInit.bool_lame,		"LAME not found",		NULL, NULL },
{-1}
};
TST_FROM ToOther[] = {
{ FILE_IS_FLAC,		&PrgInit.bool_sox,		"SOX not found",					&PrgInit.bool_flac,		"FLAC not found",		NULL, NULL },
{ FILE_IS_APE,		&PrgInit.bool_sox,		"SOX not found",					&PrgInit.bool_ape,		"MAC not found",		NULL, NULL },
{ FILE_IS_WAVPACK,	&PrgInit.bool_wavpack,	"WAVPACK or WVUNPACK not found",	NULL, NULL, NULL, NULL },
{ FILE_IS_OGG,		&PrgInit.bool_sox,		"SOX not found",					&PrgInit.bool_oggenc,	"OGGENC not found",		NULL, NULL },
{ FILE_IS_M4A,		&PrgInit.bool_sox,		"SOX not found",					&PrgInit.bool_faad,		"FAAD not found",		NULL, NULL },
{ FILE_IS_AAC,		&PrgInit.bool_sox,		"SOX not found",					&PrgInit.bool_mplayer,	"MPLAYER not found",	&PrgInit.bool_aacplusenc,	"AACPLUSENC not found" },
{ FILE_IS_MPC,		&PrgInit.bool_sox,		"SOX not found",					&PrgInit.bool_mplayer,	"MPLAYER not found",	&PrgInit.bool_mppenc,		"MPPENC not found" },
{ FILE_IS_MP3,		&PrgInit.bool_sox,		"SOX not found",					&PrgInit.bool_lame,		"LAME not found",		NULL, NULL },
{-1}
};
TST_FROM ToReplaygain[] = {
{ FILE_IS_FLAC,		&PrgInit.bool_flac,			"METAFLAC not found",			NULL, NULL, NULL, NULL },
{ FILE_IS_MP3,		&PrgInit.bool_mp3gain,		"MP3GAIN not found",			NULL, NULL, NULL, NULL },
{ FILE_IS_OGG,		&PrgInit.bool_vorbisgain,	"VORBISGAIN not found",			NULL, NULL, NULL, NULL },
{ FILE_IS_WAVPACK,	&PrgInit.bool_wavpack,		"WVGAIN not found",				NULL, NULL, NULL, NULL },
{-1}
};


TST_FROM ToNormalize[] = {
{ FILE_IS_WAV,		&PrgInit.bool_normalize,	"normalize not found",			NULL, NULL, NULL, NULL },
{ FILE_IS_MP3,		&PrgInit.bool_normalize,	"normalize-mp3 not found",		NULL, NULL, NULL, NULL },
{ FILE_IS_OGG,		&PrgInit.bool_normalize,	"normalize-ogg not found",		NULL, NULL, NULL, NULL },
{-1}
};



//
// Return string from 'TYPE_FILE_IS p_from'
//
char *get_type( TYPE_FILE_IS p_from )
{
	int i = p_from;
	switch( i ) {
		case FILE_IS_WAV :		return("WAV");
		case FILE_IS_FLAC :		return("FLAC");
		case FILE_IS_APE :		return("APE");
		case FILE_IS_WAVPACK :	return("WAVPACK");
		case FILE_IS_MP3 :		return("MP3");
		case FILE_IS_OGG :		return("OGG");
		case FILE_IS_MPC :		return("MPC");
		case FILE_IS_AC3 :		return("AC3");
		case FILE_IS_M4A :		return("M4A");
		case FILE_IS_VID_M4A :	return("M4A");
		case FILE_IS_WMA :		return("WMA");
		case FILE_IS_RM :		return("RM");
		case FILE_IS_DTS :		return("DTS");
		case FILE_IS_AIFF :		return("AIF");
		case FILE_IS_SHN :		return("SHN");
		case FILE_IS_AAC :		return("AAC");
	}
	return("???");
}
//
// Search in tab 'TST_FROM *p_type' if 'TYPE_FILE_IS p_from' exist.
// Return TRUE if ok else FALSE
//
C_BOOL tst_prg_extern( C_BOOL p_bool_print, TST_FROM *p_type, TYPE_FILE_IS p_from )
{
	C_BOOL	Ret = TRUE;
	TST_FROM	*type = p_type;

	while( type->TypeFileIs != -1 ) {
		if( p_from == type->TypeFileIs ) {
			if( NULL != type->BoolFound_1 && FALSE == *type->BoolFound_1 ) {
				if( TRUE == p_bool_print) printf("\t%s%s %s\n", BOLD_RED, type->Err_1, RESET );
				Ret = FALSE;
			}
			if( NULL != type->BoolFound_2 && FALSE == *type->BoolFound_2 ) {
				if( TRUE == p_bool_print) printf("\t%s%s %s\n", BOLD_RED, type->Err_2, RESET );
				Ret = FALSE;
			}
			if( NULL != type->BoolFound_3 && FALSE == *type->BoolFound_3 ) {
				if( TRUE == p_bool_print) printf("\t%s%s %s\n", BOLD_RED, type->Err_3, RESET );
				Ret = FALSE;
			}
		}
		type ++;
	}
	return( Ret );
}
//
// Returns TRUE if an external program is absent otherwise returns FALSE
//
C_BOOL get_is_err_in( TST_FROM *p_type )
{
	CList	*list = NULL;
	INFO	*Info = NULL;

	if( NULL != ( list = C_list_first( Detail.ListFile ))) {
		while( list ) {
			if( NULL != (Info = (INFO *)list->data) ) {
				if( FALSE == tst_prg_extern( FALSE, p_type, Info->type_infosong_file_is ))
					return( TRUE );
			}
			list = C_list_next( list );
		}
	}
	return( FALSE );
}

//
// Remove all memoty allocation
//
void remove_options( void )
{
	CList	*list = NULL;
	INFO	*Info = NULL;
	LIST_SEARCH	*ListSearch= NULL;

	if( NULL != ( list = C_list_first( Detail.ListSearch ))) {
		while( list ) {
			if( NULL != (ListSearch = (LIST_SEARCH *)list->data) ) {
				if( NULL != ListSearch->PathNameFile )	free( ListSearch->PathNameFile );
				if( NULL != ListSearch->Jocker )		free( ListSearch->Jocker );
				ListSearch->PathNameFile = ListSearch->Jocker = list->data = NULL;
			}
			list = C_list_next( list );
		}
		C_list_free( Detail.ListSearch );
		Detail.ListSearch = NULL;
	}

	if( Detail.OutputDir != NULL ) {
		free( Detail.OutputDir );
		Detail.OutputDir = NULL;
	}

	if( NULL != ( list = C_list_first( Detail.ListFile ))) {
		while( list ) {
			if( NULL != (Info = (INFO *)list->data) ) {
				GetInfo_remove( Info );
				if( NULL != Info->path_name_file )		free( Info->path_name_file );
				if( NULL != Info->path )				free( Info->path );
				if( NULL != Info->name_file )			free( Info->name_file );
				if( NULL != Info->path_is_normalize )	free( Info->path_is_normalize );
				Info->path_is_normalize =
					Info->path =
						Info->name_file =
							Info->path_name_file =
								list->data = NULL;
			}
			list = C_list_next( list );
		}
		C_list_free( Detail.ListFile );
		Detail.ListFile = NULL;
	}

	if( Detail.Op_flac != NULL ) {			free( Detail.Op_flac );			Detail.Op_flac = NULL;		}
	if( Detail.Op_ape != NULL ) {			free( Detail.Op_ape );			Detail.Op_ape = NULL;		}
	if( Detail.Op_wavpack != NULL ) {		free( Detail.Op_wavpack );		Detail.Op_wavpack = NULL;	}
	if( Detail.Op_ogg != NULL ) {			free( Detail.Op_ogg );			Detail.Op_ogg = NULL;		}
	if( Detail.Op_m4a != NULL ) {			free( Detail.Op_m4a );			Detail.Op_m4a = NULL;		}
	if( Detail.Op_aac != NULL ) {			free( Detail.Op_aac );			Detail.Op_aac = NULL;		}
	if( Detail.Op_mpc != NULL ) {			free( Detail.Op_mpc );			Detail.Op_mpc = NULL;		}
	if( Detail.Op_mp3  != NULL ) {			free( Detail.Op_mp3 );			Detail.Op_mp3 = NULL;		}
	if( Detail.tag_album  != NULL ) {		free( Detail.tag_album );		Detail.tag_album = NULL;	}
	if( Detail.tag_artist  != NULL ) {		free( Detail.tag_artist );		Detail.tag_artist = NULL;	}
	if( Detail.tag_title  != NULL ) {		free( Detail.tag_title );		Detail.tag_title = NULL;	}
	if( Detail.tag_number  != NULL ) {		free( Detail.tag_number );		Detail.tag_number = NULL;	}
	if( Detail.tag_genre  != NULL ) {		free( Detail.tag_genre );		Detail.tag_genre = NULL;	}
	if( Detail.tag_year  != NULL ) {		free( Detail.tag_year );		Detail.tag_year = NULL;		}
	if( Detail.tag_comment  != NULL ) {		free( Detail.tag_comment );		Detail.tag_comment = NULL;	}
	if( Detail.tag_description  != NULL ) {	free( Detail.tag_description );	Detail.tag_description = NULL;	}
}


//
// Print help and quit
//
void help( C_BOOL p_bool_halt )
{
	printf( "\n" );
	printf( "!-------------------------------------------------------------\n" );
	printf( "! %s\n", PACKAGE_STRING );
	printf( "!-------------------------------------------------------------\n" );
	printf( _("xcfa_cli is an implementation of xcfa in command line.\n"
			"xcfa_cli is an application for conversion, normalization, reconfiguring wav files and cut audio files ...\n\n"
			"What xcfa_cli can do:\n"
			"  - replaygain on files: flac, mp3, ogg, wavpack\n"
			"  - conversions:\n"
			"      - from files:\n"
			"         wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, wma, shorten, rm, dts, aif, ac3		\n"
			"      - to files:			\n"
			"        wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, aac\n"
			"  - conversion settings for file management:\n"
			"        flac, ape, wavpack, ogg, m4a, aac, mpc, mp3\n"
			"  - management tags\n"
			"  - management cue wav file\n"
			"  - manipulation of the frequency, track and bit wav files\n"
			"  - standardization on files: wav, mp3, ogg\n"
			"  - cuts (split) wav files\n"
			"  - displaying information on files\n") );

	printf( "!-------------------------------------------------------------\n" );
	GetInfo_cpu_print();
	printf( "!-------------------------------------------------------------\n" );
	prginit_print_info ();
	printf( "!-------------------------------------------------------------\n" );
	printf( "\n" );

	printf( _("  --verbose                             Verbose mode\n"
			"  -h    --help                          Print help mode and quit\n"
			"\n"

			"  -i <\"file.type\">   --input <\"file.type\">\n"
			"                                        Input name file to convert in inverted commas, by example: --input \"*.flac\"\n"
			"                                        Type input files: wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, wma, shorten, rm, dts, aif, ac3\n"
			"  -o <path_dest/>    --output <path_dest/>\n"
			"                                        Destination folder. By default in the source file folder.\n"
			"  -d <wav,flac,ape,...>    --dest <wav,flac,ape,...>\n"
			"                                        Destination file: wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, aac\n"
			"  -r    --recursion                     Recursive search\n"
			"  -e    --ext2src                       Extract in the source folder. This option is useful with '--recursion'\n"
			"  --nice <priority>                     Change the priority of running processes in the interval: 0 .. 20\n"
			"\n"

			"Management options with default parameters:\n"
			"\n"
			"  --op_flac <\"-5\">\n"
			"  --op_ape <\"c2000\">\n"
			"  --op_wavpack <\"-y -j1\">\n"
			"  --op_ogg <\"--quality=3\">\n"
			"  --op_m4a <\"-q 100\">\n"
			"  --op_aac <\"48\">\n"
			"  --op_mpc <\"--verbose --overwrite --insane\">\n"
			"  --op_mp3 <\"-h --nohist --noreplaygain -b 128\">\n"
			"\n"

			"Displays information about the files:\n"
			"\n"
			"  --info_files                          Displays time, level dBFS, frequency/track/quantification\n"
			"  --info_tags                           Displays tags\n"
			"  --info_head                           Displays head\n"
			"\n"

			"Management tags:\n"
			"\n"
			"  --no_tag                              The tags will not be carried\n"
			"  --tag_album <\"tags\">                  Tag of album\n"
			"  --tag_artist <\"tags\">                 Tag of artist\n"
			"  --tag_title <\"tags\">                  Tag of title\n"
			"  --tag_number <\"tags\">                 Tag of number\n"
			"  --tag_genre <\"tags\">                  Tag of genre\n"
			"  --tag_year <\"tags\">                   Tag of year\n"
			"  --tag_comment <\"tags\">                Tag of comment\n"
			"  --tag_description <\"tags\">            Tag of description\n"
			"\n"

			"Management cue file:\n"
			"  -c <info|extract>   --cue <info|extract>\n"
			"                                        info     Provides information on a WAV or CUE file.\n"
			"                                        extract  Extract all tracks of a WAV file.\n"
			"\n"

			"Management split:\n"
			"\n"
			"  -s <hh:mm:ss>   --split <hh:mm:ss>    Mark the beginning of the file to be cut.\n"
			"  -l <sec>   --length <sec>             Specifies the length of the file to cut with the seconds parameter.\n"
			"\n"

			"Changing the settings of a WAV file:\n"
			"\n"
			"  -f <num>   --frequency <num>          Changing the frequency:        8000, 22000, 32000, 44056, 44100, 48000, 88200, 96000 or other\n"
			"  -t <num>   --track <num>              Changing the number of tracks: 1, 2, 4, 6\n"
			"  -q <num>   --quantification <num>     Changing the quantification:   8, 16, 24, 32, 64\n"
			"\n"

			"Replaygain: dynamic modification for next files: FLAC, MP3, OGG, WAVPACK\n"
			"\n"
			"  -g <clear|album|track>   --replaygain <clear|album|track>\n"
			"                                        FLAC    [ clear | album ]\n"
			"                                        MP3     [ clear | album | track ]\n"
			"                                        OGG     [ clear | album | track ]\n"
			"                                        WAVPACK [ clear | album | track ]\n"
			"\n"

			"Normalize: static modification for next files: MP3, WAV, OGG\n"
			"   See an excellent article by @Dzef on standardization: http://ubunteros.tuxfamily.org/spip.php?article159\n"
			"\n"
			"  --peak                                Action on a single file.\n"
			"                                        Maximum volume amplification  for each file:\n"
			"                                        Increase the overall level of the signal so as to bring the level to 0 dBFS peak without changing dynamics.\n"
			"  --peak_album                          Action on a group of files.\n"
			"                                        Maximum volume boost for a group of files in accordance with the level of differences between each of them.\n"
			"                                        If the maximum level of one or more files is already at 0 dBFS, the level of all the selected files remain unchanged\n"
			"                                        after normalization. So this mode can be safely used almost systematically.\n"
			"  --mix_rms_album <dBFS>                Action on a group of files.\n"
			"                                        Adjusting the average volume for a group of files respecting average level of the differences between each of them.\n"
			"                                        The selecting a value for a file modifies the other files in the group.\n"
			"  --fix_rms <dBFS>                      Action on a single file.\n"
			"                                        Adjusting the average volume of each file..\n"
			"\n") );

	printf( _("Example conversion:\n") );
	printf( "\n" );
	printf( "   %s --input \"file.wav\" -d ogg --dest flac,mp, --output newfolder/\n", PACKAGE );
	printf( "   %s --input \"*.wav\" --dest ogg,flac,mp3 --output newfolder/ --recursive -op_mp3 \"--preset fast extreme\"\n", PACKAGE );
	printf( "   %s --input \"*.*\" -d mp3 --split 00:00:00 -l 30 -o newfolder/ --verbose --frequency 44100 --track 2 --quantification 16\n", PACKAGE );
	printf( "\n" );

	printf( _("Conversion example with two inputs:\n") );
	printf( "\n" );
	printf( "   %s --input \"file.wav\" --input \"other_file.mp3\" -d ogg --dest flac,mpc --output newfolder/\n", PACKAGE );
	printf( "\n" );

	printf( _("Example split:\n") );
	printf( "\n" );
	printf( "   %s --input \"file.wav\" --info_files\n", PACKAGE );
	printf( "   %s --input \"*.*\" -d mp3 --split 00:00:00 -l 30 -o newfolder/ --verbose\n", PACKAGE );
	printf( "   %s --input \"file.wav\" -d ogg,mpc --split 00:01:00 --length 22 --output newfolder/\n", PACKAGE );
	printf( "\n" );

	printf( _("Example cue:\n") );
	printf( "\n" );
	printf( "   %s --input \"file.cue\" --cue info\n", PACKAGE );
	printf( "   %s --input \"file.cue\" --cue extract --output newfolder/\n", PACKAGE );
	printf( "   %s --input \"file.wav\" --cue info\n", PACKAGE );
	printf( "   %s --input \"file.wav\" --cue extract --output newfolder/\n", PACKAGE );
	printf( "\n" );

	printf( _("Example setting wav:\n") );
	printf( "\n" );
	printf( "   %s -i \"file.wav\" -d wav -o newfolder/ --frequency 96000  --track 6 --quantification 32 --verbose\n", PACKAGE );
	printf( "\n" );

	printf( _("Example replaygain:\n") );
	printf( "\n" );
	printf( "   %s -i \"*.*\" --replaygain clear\n", PACKAGE );
	printf( "   %s -i \"*.*\" --replaygain album\n", PACKAGE );
	printf( "\n" );

	printf( _("Example normalize:\n") );
	printf( "\n" );
	printf( "   %s -i \"*.*\" --info_files\n", PACKAGE );
	printf( "   %s -i \"*.*\" --peak_album\n", PACKAGE );
	printf( "   %s -i \"*.*\" --peak\n", PACKAGE );
	printf( "   %s -i \"*.*\" --mix_rms_album -10\n", PACKAGE );
	printf( "   %s -i \"*.*\" --fix_rms -4\n", PACKAGE );
	printf( "\n" );

	printf( _("Example info:\n") );
	printf( "\n" );
	printf( "   %s -i \"*.*\" --info_files --info_tags --info_head\n", PACKAGE );
	printf( "\n" );

	printf( "!-------------------------------------------------------------\n" );
	printf( "! See [ $ man xcfa_cli ]\n" );
	printf( "!-------------------------------------------------------------\n" );
	printf( "\n" );

	printf( "\n\n" );
	remove_options();
	if( TRUE == p_bool_halt) exit( EXIT_SUCCESS );
}


//
// Return type file from 'char *p_type_dest'
//
int ret_type_dest( char *p_type_dest )
{
	if( strstr( p_type_dest, "flac" ))			return( FILE_IS_FLAC );
	else if( strstr( p_type_dest, "ape" ))		return( FILE_IS_APE );
	else if( strstr( p_type_dest, "wavpack" ))	return( FILE_IS_WAVPACK );
	else if( strstr( p_type_dest, "wav" ))		return( FILE_IS_WAV );
	else if( strstr( p_type_dest, "mp3" ))		return( FILE_IS_MP3 );
	else if( strstr( p_type_dest, "ogg" ))		return( FILE_IS_OGG );
	else if( strstr( p_type_dest, "mpc" ))		return( FILE_IS_MPC );
	else if( strstr( p_type_dest, "ac3" ))		return( FILE_IS_AC3 );
	else if( strstr( p_type_dest, "m4a" ))		return( FILE_IS_M4A );
	else if( strstr( p_type_dest, "wma" ))		return( FILE_IS_WMA );
	else if( strstr( p_type_dest, "rm" ))		return( FILE_IS_RM );
	else if( strstr( p_type_dest, "dts" ))		return( FILE_IS_DTS );
	else if( strstr( p_type_dest, "aif" ))		return( FILE_IS_AIFF );
	else if( strstr( p_type_dest, "shn" ))		return( FILE_IS_SHN );
	else if( strstr( p_type_dest, "aac" ))		return( FILE_IS_AAC );
	return FILE_IS_NONE;
}


//
// Analyse options from command line
//
void analyse_command_line( int argc, char *argv[] )
{
	int c;

	while (1) {
		static struct option long_options[] =
        {
			{ "verbose",		no_argument,		&Detail.BoolVerboseMode,	1 },

			{ "help",			no_argument,		0,							'h' },
			{ "input",			required_argument,	0,							'i' },
			{ "output",			required_argument,	0,							'o' },
			{ "dest",			required_argument,	0,							'd' },
			{ "recursion",		no_argument,		0,							'r' },
			{ "ext2src",		no_argument,		0,							'e' },

			{ "op_flac",		required_argument,	0,							1 },
			{ "op_ape",			required_argument,	0,							2 },
			{ "op_wavpack",		required_argument,	0,							3 },
			{ "op_ogg",			required_argument,	0,							4 },
			{ "op_m4a",			required_argument,	0,							5 },
			{ "op_aac",			required_argument,	0,							6 },
			{ "op_mpc",			required_argument,	0,							7 },
			{ "op_mp3",			required_argument,	0,							8 },
			{ "no_tag",			no_argument,		0,							9 },
			{ "tag_album",		required_argument,	0,							10 },
			{ "tag_artist",		required_argument,	0,							11 },
			{ "tag_title",		required_argument,	0,							12 },
			{ "tag_number",		required_argument,	0,							13 },
			{ "tag_genre",		required_argument,	0,							14 },
			{ "tag_year",		required_argument,	0,							15 },
			{ "tag_comment",	required_argument,	0,							16 },
			{ "tag_description",required_argument,	0,							17 },

			{ "split",			required_argument,	0,							's' },
			{ "length",			required_argument,	0,							'l' },

			{ "frequency",		required_argument,	0,							'f' },
			{ "track",			required_argument,	0,							't' },
			{ "quantification",	required_argument,	0,							'q' },

			{ "replaygain",		required_argument,	0,							'g' },

			{ "peak",			no_argument,		&Detail.bool_peak,			1 },
			{ "peak_album",		no_argument,		&Detail.bool_peak_album,	1 },
			{ "mix_rms_album",	required_argument,	0,							18 },
			{ "fix_rms",		required_argument,	0,							19 },

			{ "nice",			required_argument,	0,							20 },

			{ "info_files",		no_argument,		&Detail.bool_info_files,	1 },
			{ "info_tags",		no_argument,		&Detail.bool_info_tags,		1 },
			{ "info_head",		no_argument,		&Detail.bool_info_head,		1 },

			{ "cue",			required_argument,	0,							'c' },

			{0, 0, 0, 0}
		};
		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long( argc, argv, "hi:o:d:res:l:f:t:q:g:c:", long_options, &option_index );

		/* Detect the end of the options. */
		if( c == -1 ) break;

		switch( c ) {
		case 0:
			/* If this option set a flag, do nothing else now. */
			if( long_options[option_index].flag != 0 ) break;
			if( optarg ) printf(" with arg %s\n", optarg );
			break;

		// --op_flac	1
		case 1 :
			if( NULL == Detail.Op_flac ) Detail.Op_flac = C_strdup( optarg );
			break;
		// --op_ape		2
		case 2 :
			if( NULL == Detail.Op_ape ) Detail.Op_ape = C_strdup( optarg );
			break;
		// --op_wavpack	3
		case 3 :
			if( NULL == Detail.Op_wavpack ) Detail.Op_wavpack = C_strdup( optarg );
			break;
		// --op_ogg		4
		case 4 :
			if( NULL == Detail.Op_ogg ) Detail.Op_ogg = C_strdup( optarg );
			break;
		// --op_m4a		5
		case 5 :
			if( NULL == Detail.Op_m4a ) Detail.Op_m4a = C_strdup( optarg );
			break;
		// --op_aac		6
		case 6 :
			if( NULL == Detail.Op_aac ) Detail.Op_aac = C_strdup( optarg );
			break;
		// --op_mpc		7
		case 7 :
			if( NULL == Detail.Op_mpc ) Detail.Op_mpc = C_strdup( optarg );
			break;
		// --op_mp3		8
		case 8 :
			if( NULL == Detail.Op_mp3 ) Detail.Op_mp3 = C_strdup( optarg );
			break;

		// --no_tag		9
		case 9 :
			Detail.BoolNoTag = TRUE;
			break;

		// --tag_album			10
		case 10 :
			if( NULL == Detail.tag_album )
				Detail.tag_album = C_strdup( optarg );
			break;
		// --tag_artist			11
		case 11 :
			if( NULL == Detail.tag_artist )
				Detail.tag_artist = C_strdup( optarg );
			break;
		// --tag_title			12
		case 12 :
			if( NULL == Detail.tag_title )
				Detail.tag_title = C_strdup( optarg );
			break;
		// --tag_number			13
		case 13 :
			if( NULL == Detail.tag_number )
				Detail.tag_number = C_strdup( optarg );
			break;
		// --tag_genre			14
		case 14 :
			if( NULL == Detail.tag_genre )
				Detail.tag_genre = C_strdup( optarg );
			break;
		// --tag_year			15
		case 15 :
			if( NULL == Detail.tag_year )
				Detail.tag_year = C_strdup( optarg );
			break;
		// --tag_comment		16
		case 16 :
			if( NULL == Detail.tag_comment )
				Detail.tag_comment = C_strdup( optarg );
			break;
		// --tag_description	17
		case 17 :
			if( NULL == Detail.tag_description )
				Detail.tag_description = C_strdup( optarg );
			break;

		// --help -h
		case 'h' :
			help( TRUE );
			break;

		// --input -i
		case 'i' :
			{
			/*
			--input     -i          Input name file to convert in inverted commas: --input "*.flac"
			L'option de la commande [ -i, --input ] doit imperativement etre entouree de guillemets:
				$ xcfa_cli -i "*.wav"
				$ xcfa_cli --input  "*.wav"  --input "*.ogg"
			*/
			LIST_SEARCH *ListSearch = (LIST_SEARCH *)malloc (sizeof (LIST_SEARCH));
			if( *optarg == '/' ) {
				char	*Ptr = NULL;
				ListSearch->PathNameFile = C_strdup( optarg );
				if( NULL != (Ptr = strrchr( ListSearch->PathNameFile, '/' ))) {
					Ptr ++;
					ListSearch->Jocker = C_strdup( Ptr );
					*Ptr = '\0';
				}
			}
			else {
				char	*Ptr = NULL;
				char	*str_current_dir = get_current_dir_name();
				ListSearch->PathNameFile = C_strdup_printf( "%s/%s", str_current_dir, optarg );
				free( str_current_dir );
				str_current_dir = NULL;
				if( NULL != (Ptr = strrchr( ListSearch->PathNameFile, '/' ))) {
					Ptr ++;
					ListSearch->Jocker = C_strdup( Ptr );
					*Ptr = '\0';
				}
			}
			Detail.ListSearch = C_list_append( Detail.ListSearch, ListSearch );
			}
			break;

		// --output -o
		case 'o' :
			{
			char *Ptr = NULL;
			Detail.OutputDir = C_strdup_printf( "%s/", optarg );
			if( NULL != ( Ptr = strstr( Detail.OutputDir, "//" ))) {
				Ptr ++;
				*Ptr = '\0';
			}
			}
			break;

		// --dest -d
		case 'd' :
			{
			char *p = NULL;
			p = strtok( optarg, "," );
			while( p != NULL ) {
				int Dest = ret_type_dest( p );
				if( FILE_IS_NONE != Dest ) {
					Detail.BoolTypeDest[ Dest ] = TRUE;
					Detail.bool_dest = TRUE;
				}
				else {
					printf( "%s", BOLD_RED );
					printf(
					"!-------------------------------------------------------------\n"
					"! Unknown option for: --dest %s%s%s\n"
					"!-------------------------------------------------------------\n"
					"  -d <wav,flac,ape,...>    --dest <wav,flac,ape,...>\n"
					"                                        Destination file: wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, aac\n",
					BOLD_YELLOW,
					p,
					BOLD_RED
					);
					printf( "%s", RESET );
					exit( EXIT_SUCCESS );
				}
				p = strtok(NULL, ",");
			}
			}
			break;

		// --recursion -r
		case 'r' :
			Detail.BoolRecursiveMode = TRUE;
			break;

		// --ext2src -e
		case 'e' :
			Detail.BoolExtract2Src = TRUE;
			break;

		// --split -s  [ hh:mm:ss ]
		case 's' :
			if( 0.0 == Detail.split) {
				int	cpt = 0;
				char *p = NULL;
				p = strtok( optarg, ":" );
				while( p != NULL ) {
					if( 0 == cpt )
						Detail.split += (double)atoi( p ) * 3600;
					else if( 1 == cpt )
						Detail.split += (double)atoi( p ) * 60;
					else if( 2 == cpt )
						Detail.split += (double)atoi( p );
					else if( 3 == cpt )
						Detail.split += (double)atoi( p ) / 100.0;
					cpt ++;
					p = strtok(NULL, ":");
				}
				if( cpt < 3 || cpt > 3 ) {
					printf( "%s", BOLD_RED );
					printf(
					"!-------------------------------------------------------------\n"
					"! Error option for: --split %shh:mm:ss%s\n"
					"!-------------------------------------------------------------\n"
					"Management split:\n"
					"\n"
					"  -s <hh:mm:ss>   --split <hh:mm:ss>    Mark the beginning of the file to be cut.\n"
					"  -l <sec>   --length <sec>             Specifies the length of the file to cut with the seconds parameter.\n"
					"\n",
					BOLD_YELLOW,
					BOLD_RED
					);
					printf( "%s", RESET );
					exit( EXIT_SUCCESS );
				}
				Detail.bool_split = TRUE;
			}
			break;

		// --length -l
		case 'l' :
			Detail.split_length = atoi( optarg );
			Detail.bool_split = TRUE;
			break;

		// --frequency  -f
		case 'f' :
			Detail.frequency = atoi( optarg );
			Detail.bool_setting_wav = TRUE;
			break;
		// --track      -t
		case 't' :
			Detail.track = atoi( optarg );
			Detail.bool_setting_wav = TRUE;
			break;
		// --quantification    -q
		case 'q' :
			Detail.quantification = atoi( optarg );
			Detail.bool_setting_wav = TRUE;
			break;

		// --replaygain  -g
		case 'g' :
			if( strcmp( optarg, "clear" ) == 0 )
				Detail.replaygain =  OP_REPLAYGAIN_CLEAR;
			else if( strcmp( optarg, "album" ) == 0 )
				Detail.replaygain =  OP_REPLAYGAIN_ALBUM;
			else if( strcmp( optarg, "track" ) == 0 )
				Detail.replaygain =  OP_REPLAYGAIN_TRACK;
			else {
				printf( "%s", BOLD_RED );
				printf(
					"!-------------------------------------------------------------\n"
					"! Unknown option for: --replaygain %s%s%s\n"
					"!-------------------------------------------------------------\n"
					"Replaygain: dynamic modification for next files: FLAC, MP3, OGG, WAVPACK\n"
					"\n"
					"  -g <clear|album|track>   --replaygain <clear|album|track>\n"
					"                                        FLAC    [ clear | album ]\n"
					"                                        MP3     [ clear | album | track ]\n"
					"                                        OGG     [ clear | album | track ]\n"
					"                                        WAVPACK [ clear | album | track ]\n"
					"\n",
					BOLD_YELLOW,
					optarg,
					BOLD_RED
					);
				printf( "%s", RESET );
				exit( EXIT_SUCCESS );
			}
			break;

		// --peak					1
			// Detail.bool_peak
		// --peak_album				18
			// Detail.bool_peak_album
		// --mix_rms_album			19
		case 18:
			Detail.mix_rms_album = atoi( optarg );
			break;
		// --fix_rms				20
		case 19:
			Detail.fix_rms = atoi( optarg );
			break;

		// --nice
		case 20 :
			Detail.Nice = atoi( optarg );
			break;

		// --cue < info | extract >
		//
		case 'c' :
			if( strcmp( optarg, "info" ) == 0 )
				Detail.cue = OP_CUE_INFO;
			else if( strcmp( optarg, "extract" ) == 0 )
				Detail.cue = OP_CUE_EXTRACT;
			else {
				printf( "%s", BOLD_RED );
				printf(
					"!-------------------------------------------------------------\n"
					"! Unknown option for: --cue %s%s%s\n"
					"!-------------------------------------------------------------\n"
					"Management cue file:\n"
					"  -c <info|extract>   --cue <info|extract>\n"
					"                                        info     Provides information on a WAV or CUE file.\n"
					"                                        extract  Extract all tracks of a WAV file.\n"
					"\n",
					BOLD_YELLOW,
					optarg,
					BOLD_RED
					);
				printf( "%s", RESET );
				exit( EXIT_SUCCESS );
			}
			break;

		case '?':
			/* getopt_long already printed an error message. */
			help( FALSE );
			printf( "%s\n\n", BOLD_RED );
			printf("!------------------------------\n");
			printf("! Invalid option\n" );
			printf("!------------------------------\n");
			printf( "%s\n\n", RESET );
			exit( EXIT_SUCCESS );
			break;

		default:
			help( TRUE );
			abort ();
		}
	}

	/* Print any remaining command line arguments (not options). */
	if (optind < argc) {
		help( FALSE );
		printf( "%s", BOLD_RED );
//		printf ("non-option ARGV-elements: \n");
//		while (optind < argc)
//			printf ("    %s\n", argv[optind++] );
//		printf( "\n" );
		printf( "!-------------------------------------------------------------\n" );
		printf( "! You must provide an entry with the parameter '--input'\n" );
		printf( "! --input     -i          Input name file to convert in inverted commas, by example: --input %s\"*.flac\"%s\n", RESET, BOLD_RED );
		printf( "!                         Type input files: wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, wma, shorten, rm, dts, aif, ac3\n" );
		printf( "! See [ $ xcfa_cli --help ]\n" );
		printf( "! See [ $ man xcfa_cli ]\n" );
		printf( "!-------------------------------------------------------------\n" );
		printf( "\n" );
		printf( "%s", RESET );
		exit( EXIT_SUCCESS );
	}

	if( argc == 1 ) help( TRUE );

	// DEFAULT OPTIONS CONVERT
	if( Detail.Op_flac == NULL)		Detail.Op_flac		= C_strdup( "-5" );
	if( Detail.Op_ape == NULL)		Detail.Op_ape		= C_strdup( "-c2000" );
	if( Detail.Op_wavpack == NULL)	Detail.Op_wavpack	= C_strdup( "-y -j1" );
	if( Detail.Op_ogg == NULL)		Detail.Op_ogg		= C_strdup( "--quality=3" );
	if( Detail.Op_m4a == NULL)		Detail.Op_m4a		= C_strdup( "-q 100" );
	if( Detail.Op_aac == NULL)		Detail.Op_aac		= C_strdup( "48" );
	if( Detail.Op_mpc == NULL)		Detail.Op_mpc		= C_strdup( "--verbose --overwrite --insane" );
	if( Detail.Op_mp3 == NULL)		Detail.Op_mp3		= C_strdup( "-h --nohist --noreplaygain -b 128" );

	if( NULL == Detail.OutputDir ) {
		char	*str_current_dir = get_current_dir_name();
		Detail.OutputDir = C_strdup_printf( "%s/", str_current_dir );
		free( str_current_dir );
		str_current_dir = NULL;
	}

	if( TRUE == Detail.BoolNoTag  ) {
		if( Detail.tag_album  != NULL ) {
			free( Detail.tag_album );
			Detail.tag_album = NULL;
		}
		if( Detail.tag_artist  != NULL ) {
			free( Detail.tag_artist );
			Detail.tag_artist = NULL;
		}
		if( Detail.tag_title  != NULL ) {
			free( Detail.tag_title );
			Detail.tag_title = NULL;
		}
		if( Detail.tag_number  != NULL ) {
			free( Detail.tag_number );
			Detail.tag_number = NULL;
		}
		if( Detail.tag_genre  != NULL ) {
			free( Detail.tag_genre );
			Detail.tag_genre = NULL;
		}
		if( Detail.tag_year  != NULL ) {
			free( Detail.tag_year );
			Detail.tag_year = NULL;
		}
		if( Detail.tag_comment  != NULL ) {
			free( Detail.tag_comment );
			Detail.tag_comment = NULL;
		}
		if( Detail.tag_description  != NULL ) {
			free( Detail.tag_description );
			Detail.tag_description = NULL;
		}
	}
}


//
// From 'char *Directory' search 'char *jocker'.
// Recursive search is applied if Detail.BoolRecursiveMode is TRUE
//
void fileanalyze_recherche_recursive( char *Directory, char *jocker )
{
	DIR    *dp;
	struct  dirent *entry;
	struct  stat statbuf;
	char	*Ptr = NULL;
	TYPE_FILE_IS	TypeFileRet = FILE_IS_NONE;

	if( NULL == ( dp = opendir( Directory ))) {
		if( TRUE == C_file_test( Directory ) && utils_get_size_file( Directory ) > 100 ) {
			if( !fnmatch( jocker, Directory, 0 )) {
				// Accepted file:
				// wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, wma, shorten, rm, dts, aif, ac3
				TypeFileRet = GetInfo_file_is( Directory );
				if( TypeFileRet == FILE_IS_WAV || TypeFileRet == FILE_IS_FLAC || TypeFileRet == FILE_IS_WAVPACK ||
					TypeFileRet == FILE_IS_OGG || TypeFileRet == FILE_IS_M4A || TypeFileRet == FILE_IS_VID_M4A ||
					TypeFileRet == FILE_IS_MPC || TypeFileRet == FILE_IS_MP3 || TypeFileRet == FILE_IS_WMA ||
					TypeFileRet == FILE_IS_SHN || TypeFileRet == FILE_IS_RM || TypeFileRet == FILE_IS_DTS ||
					TypeFileRet == FILE_IS_AIFF || TypeFileRet == FILE_IS_AC3 || TypeFileRet == FILE_IS_VID_M4A ||
					TypeFileRet == FILE_IS_CUE ) {

					INFO *Info = (INFO *)malloc (sizeof (INFO));
					Info->path_name_file        = C_strdup( Directory );
					Info->path                  = C_strdup( Info->path_name_file );
					if( NULL != (Ptr = strrchr( Info->path, '/' ))) {
						Ptr ++;
					}
					else {
						Ptr = Info->path;
					}
					Info->name_file             = C_strdup( Ptr );
					Info->type_infosong_file_is = TypeFileRet;
					Info->info                  = NULL;
					Info->path_is_normalize     = NULL;
					Info->tmp_wav               = NULL;
					Info->tmp_sox               = NULL;
					Info->tmp_sox_24            = NULL;
					Info->tmp_flac              = NULL;
					Info->tmp_ape               = NULL;
					Info->tmp_wavpack           = NULL;
					Info->tmp_ogg               = NULL;
					Info->tmp_m4a               = NULL;
					Info->tmp_aac               = NULL;
					Info->tmp_mpc               = NULL;
					Info->tmp_mp3               = NULL;
					GetInfo_set( Info );
					Detail.ListFile = C_list_append( Detail.ListFile, Info );
				}
			}
		}
		return;
	}
	chdir (Directory);
	while ((entry = readdir (dp)) != NULL) {
		lstat (entry->d_name, &statbuf);
		if (S_ISDIR (statbuf.st_mode)) {
			if (strcmp (".", entry->d_name) == 0 || strcmp ("..", entry->d_name) == 0) continue;
			if( Detail.BoolRecursiveMode == TRUE )
				fileanalyze_recherche_recursive (entry->d_name, jocker );
		}
		else {
			if( !fnmatch( jocker, entry->d_name, 0 )) {
				char	*str_current_dir = get_current_dir_name();

				INFO *Info = (INFO *)malloc (sizeof (INFO));
				Info->path_name_file  = C_strdup_printf( "%s/%s", str_current_dir, entry->d_name );
				// Accepted file:
				// wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, wma, shorten, rm, dts, aif, ac3
				TypeFileRet = GetInfo_file_is( Info->path_name_file );
				if( TypeFileRet == FILE_IS_WAV || TypeFileRet == FILE_IS_FLAC || TypeFileRet == FILE_IS_WAVPACK ||
					TypeFileRet == FILE_IS_OGG || TypeFileRet == FILE_IS_M4A || TypeFileRet == FILE_IS_VID_M4A ||
					TypeFileRet == FILE_IS_MPC || TypeFileRet == FILE_IS_MP3 || TypeFileRet == FILE_IS_WMA ||
					TypeFileRet == FILE_IS_SHN || TypeFileRet == FILE_IS_RM || TypeFileRet == FILE_IS_DTS ||
					TypeFileRet == FILE_IS_AIFF || TypeFileRet == FILE_IS_AC3 || TypeFileRet == FILE_IS_VID_M4A ||
					TypeFileRet == FILE_IS_CUE ) {

					Info->path                  = C_strdup( Info->path_name_file );
					if( NULL != (Ptr = strrchr( Info->path, '/' ))) {
						Ptr ++;
						Info->name_file          = C_strdup( Ptr );
						*Ptr = '\0';
					}
					else {
						Ptr = Info->path;
						Info->name_file          = C_strdup( Ptr );
					}
					Info->type_infosong_file_is = TypeFileRet;
					Info->info                  = NULL;
					Info->path_is_normalize     = NULL;
					Info->tmp_wav               = NULL;
					Info->tmp_sox               = NULL;
					Info->tmp_sox_24            = NULL;
					Info->tmp_flac              = NULL;
					Info->tmp_ape               = NULL;
					Info->tmp_wavpack           = NULL;
					Info->tmp_ogg               = NULL;
					Info->tmp_m4a               = NULL;
					Info->tmp_aac               = NULL;
					Info->tmp_mpc               = NULL;
					Info->tmp_mp3               = NULL;
					GetInfo_set( Info );
					Detail.ListFile = C_list_append( Detail.ListFile, Info );
				}
				else {
					free( Info->path_name_file );
					Info->path_name_file = NULL;
					free( Info );
					Info = NULL;
				}
				free( str_current_dir );
				str_current_dir = NULL;
			}
		}
	}
	chdir ("..");
	closedir (dp);
}


//
// Print all option from command line.
// If necessary applications of convertion are present then return TRUE else FALSE.
//
C_BOOL test_and_print_options( void )
{
	C_BOOL	BoolRet = TRUE;
	CList	*list = NULL;
	INFO	*Info = NULL;
	int		i;

	if( NULL ==  Detail.ListFile ) {
		printf( "%s", BOLD_RED );
		printf( "!-------------------------------------------------------------\n" );
		printf( "! You must provide an entry with the parameter '--input'\n" );
		printf( "! --input     -i          Input name file to convert in inverted commas, by exemple: --input %s\"*.flac\"%s\n", RESET, BOLD_RED );
		printf( "!                         Type input files: wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, wma, shorten, rm, dts, aif, ac3\n" );
		printf( "! See [ $ xcfa_cli --help ]\n" );
		printf( "! See [ $ man xcfa_cli ]\n" );
		printf( "!-------------------------------------------------------------\n" );
		printf( "\n" );
		printf( "%s", RESET );
		return( FALSE );
	}

	if( TRUE == Detail.bool_info_files || TRUE == Detail.bool_info_tags || TRUE == Detail.bool_info_head ) {
		if( NULL != ( list = C_list_first( Detail.ListFile ))) {
			while( list ) {
				if( NULL != (Info = (INFO *)list->data) ) {

					printf("\n%s\n", Info->path_name_file );

					if( TRUE == Detail.bool_info_files ) {
						printf( "\t!-------------------------------------------------------------\n" );
						printf( "\t!  --info_files\n" );
						printf( "\t!-------------------------------------------------------------\n" );
						if( FILE_IS_FLAC == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_FLAC *)Info->info)->time );
						}
						else if( FILE_IS_WAV == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_WAV *)Info->info)->time );
							printf("\tLevelDbfs = %d dBFS\n", ((INFO_WAV *)Info->info)->LevelDbfs );
							printf("\tfrequency = %s  track = %s   quantification = %s\n",
									((INFO_WAV *)Info->info)->hertz,
									((INFO_WAV *)Info->info)->voie,
									((INFO_WAV *)Info->info)->bits
									);
						}
						else if( FILE_IS_MP3 == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_MP3 *)Info->info)->time );
							printf("\tLevelDbfs = %d dBFS\n", ((INFO_MP3 *)Info->info)->LevelDbfs );
						}
						else if( FILE_IS_OGG == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_OGG *)Info->info)->time );
							printf("\tLevelDbfs = %d dBFS\n", ((INFO_OGG *)Info->info)->LevelDbfs );
						}
						else if( FILE_IS_M4A == Info->type_infosong_file_is ||
								 FILE_IS_VID_M4A == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_M4A *)Info->info)->time );
							printf("\tfrequency = %s\n", ((INFO_M4A *)Info->info)->hertz );
						}
						else if( FILE_IS_AAC == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_AAC *)Info->info)->time );
							printf("\tfrequency = %s\n", ((INFO_AAC *)Info->info)->hertz );
						}
						else if( FILE_IS_SHN == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_SHN *)Info->info)->time );
						}
						else if( FILE_IS_APE == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_APE *)Info->info)->time );
						}
						else if( FILE_IS_WAVPACK == Info->type_infosong_file_is ) {
							printf("\ttime      = %s\n", ((INFO_WAVPACK *)Info->info)->time );
						}
						else {
							printf("\tNone for %s\n", get_type( Info->type_infosong_file_is ));
						}
					}

					if( TRUE == Detail.bool_info_tags ) {
						printf( "\t!-------------------------------------------------------------\n" );
						printf( "\t!  --info_tags\n" );
						printf( "\t!-------------------------------------------------------------\n" );
						if( FILE_IS_FLAC == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_FLAC *)Info->info)->tags );
						}
						else if( FILE_IS_WAV == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_WAV *)Info->info)->tags );
						}
						else if( FILE_IS_MP3 == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_MP3 *)Info->info)->tags );
						}
						else if( FILE_IS_OGG == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_OGG *)Info->info)->tags );
						}
						else if( FILE_IS_M4A == Info->type_infosong_file_is ||
								 FILE_IS_VID_M4A == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_M4A *)Info->info)->tags );
						}
						else if( FILE_IS_AAC == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_AAC *)Info->info)->tags );
						}
						else if( FILE_IS_SHN == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_SHN *)Info->info)->tags );
						}
						else if( FILE_IS_WMA == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_WMA *)Info->info)->tags );
						}
						else if( FILE_IS_RM == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_RM *)Info->info)->tags );
						}
						else if( FILE_IS_DTS == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_DTS *)Info->info)->tags );
						}
						else if( FILE_IS_AIFF == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_AIFF *)Info->info)->tags );
						}
						else if( FILE_IS_MPC == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_MPC *)Info->info)->tags );
						}
						else if( FILE_IS_APE == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_APE *)Info->info)->tags );
						}
						else if( FILE_IS_WAVPACK == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_WAVPACK *)Info->info)->tags );
						}
						else if( FILE_IS_AC3 == Info->type_infosong_file_is ) {
							tags_print( Info->path_name_file, ((INFO_AC3 *)Info->info)->tags );
						}
						else {
							printf("\tNone for %s\n", get_type( Info->type_infosong_file_is ));
						}
					}

					if( TRUE == Detail.bool_info_head ) {
						printf( "\t!-------------------------------------------------------------\n" );
						printf( "\t!  --info_head\n" );
						printf( "\t!-------------------------------------------------------------\n" );
						if( FILE_IS_WAV == Info->type_infosong_file_is ) {
							tagswav_print_header( Info->path_name_file );
						}
						else if( FILE_IS_MPC == Info->type_infosong_file_is ) {
							tagsmpc_print_header( Info->path_name_file );
						}
						else {
							printf("\tNone for %s\n", get_type( Info->type_infosong_file_is ));
						}
					}
				}
				list = C_list_next( list );
			}
		}
		printf( "\n" );
		return( FALSE );
	}

	// Test the presence of external programs for: REPLAYGAIN
	if( OP_REPLAYGAIN_NONE !=  Detail.replaygain ) {
		if( TRUE == get_is_err_in( ToReplaygain )) {
			printf( "!-------------------------------------------------------------\n" );
			printf( "! Error to REPLAYGAIN:\n");
			printf( "!-------------------------------------------------------------\n" );
			if( NULL != ( list = C_list_first( Detail.ListFile ))) {
				while( list ) {
					if( NULL != (Info = (INFO *)list->data) ) {
						i = Info->type_infosong_file_is;
						switch( i ) {
						case FILE_IS_FLAC:
						case FILE_IS_MP3:
						case FILE_IS_OGG:
						case FILE_IS_WAVPACK:
							printf("%s\n", Info->path_name_file );
							if( FALSE == tst_prg_extern( TRUE, ToReplaygain, Info->type_infosong_file_is ))
								BoolRet = FALSE;
						}
					}
					list = C_list_next( list );
				}
			}
			printf("\n");
			BoolRet = FALSE;
		}
	}

	// Test the presence of external programs for: NORMALIZE
	if(	FALSE != Detail.bool_peak || FALSE != Detail.bool_peak_album || 0 != Detail.mix_rms_album || 0 != Detail.fix_rms ) {
		if( TRUE == get_is_err_in( ToNormalize )) {
			printf( "!-------------------------------------------------------------\n" );
			printf( "! Error to NORMALIZE:\n");
			printf( "!-------------------------------------------------------------\n" );
			if( NULL != ( list = C_list_first( Detail.ListFile ))) {
				while( list ) {
					if( NULL != (Info = (INFO *)list->data) ) {
						i = Info->type_infosong_file_is;
						switch( i ) {
						case FILE_IS_WAV:
						case FILE_IS_MP3:
						case FILE_IS_OGG:
							printf("%s\n", Info->path_name_file );
							if( FALSE == tst_prg_extern( TRUE, ToNormalize, Info->type_infosong_file_is ))
								BoolRet = FALSE;
						}
					}
					list = C_list_next( list );
				}
			}
			printf("\n");
			BoolRet = FALSE;
		}
	}

	// Test the presence of external programs for: CONVERSION
	if( TRUE == Detail.bool_dest || TRUE == Detail.bool_split || TRUE == Detail.bool_setting_wav ) {
		if( TRUE == get_is_err_in( ToWav )) {
			printf( "!-------------------------------------------------------------\n" );
			printf( "! Error to CONV:\n");
			printf( "!-------------------------------------------------------------\n" );
			if( NULL != ( list = C_list_first( Detail.ListFile ))) {
				while( list ) {
					if( NULL != (Info = (INFO *)list->data) ) {
						printf("%s\n", Info->path_name_file );
						printf("\t*TO WAV\n");
						if( FALSE == tst_prg_extern( TRUE, ToWav, Info->type_infosong_file_is ))
							BoolRet = FALSE;
					}
					list = C_list_next( list );
				}
			}
			for( i = 0; i < NBR_FILE_TO; i ++ )
				if( TRUE == Detail.BoolTypeDest[ i ] ) {
					printf("\tTO %s\n", get_type( i ));
					if( FALSE == tst_prg_extern( TRUE, ToOther, i ))
						BoolRet = FALSE;
				}
			printf("\n");
			BoolRet = FALSE;
		}
	}
	if( FALSE == BoolRet )
		prginit_print_info();

	return( BoolRet );
}


/* int main (int argc, char *argv[], char **envp)
 *
 * argc  designe le nombre darguments transmis au moment du lancement de lexecutable
 * argv  designe le vecteur contenant les differents arguments
 * envp  designe le vecteur contenant les informations sur lenvironnement
 */
int main( int argc, char *argv[], char **envp )
{
	CList	*list = NULL;
	LIST_SEARCH *ListSearch = NULL;
#ifdef ENABLE_NLS
	setlocale( LC_ALL, "" );
	bindtextdomain( GETTEXT_PACKAGE, LOCALE_DIR );
	bind_textdomain_codeset( GETTEXT_PACKAGE, "UTF-8" );
	textdomain( GETTEXT_PACKAGE );
#endif
	prginit_scan();
	analyse_command_line( argc, &argv[0] );
	if( NULL != ( list = C_list_first( Detail.ListSearch )))
		while( list ) {
			if( NULL != (ListSearch = (LIST_SEARCH *)list->data) )
				fileanalyze_recherche_recursive( ListSearch->PathNameFile, ListSearch->Jocker );
			list = C_list_next( list );
		}

	Detail.bool_normalize =	TRUE == Detail.bool_peak || TRUE == Detail.bool_peak_album || 0 != Detail.mix_rms_album || 0 != Detail.fix_rms;
	if( TRUE == test_and_print_options() ) {

		if( OP_REPLAYGAIN_CLEAR == Detail.replaygain )
			replaygain_set( Detail.replaygain );

		if( OP_REPLAYGAIN_ALBUM == Detail.replaygain || OP_REPLAYGAIN_TRACK == Detail.replaygain )
			replaygain_set( Detail.replaygain );

		if( TRUE == Detail.bool_split && FALSE == Detail.bool_dest ) {
			printf( "%s", BOLD_RED );
			printf(	"!-------------------------------------------------------------\n"
					"! Option %s--dest%s not found\n"
					"!-------------------------------------------------------------\n"
					"  -d <wav,flac,ape,...>    --dest <wav,flac,ape,...>\n"
					"                                        Destination file: wav, flac, ape, wavpack, ogg, m4a, mpc, mp3, aac\n"
					"\n",
					BOLD_YELLOW,
					BOLD_RED
					);
			printf( "%s", RESET );

		}
		if( TRUE == Detail.bool_dest ) {
			file_conv();
		}
		else if( TRUE == Detail.bool_setting_wav ) {
			loop_wav_change_setting( Detail.frequency, Detail.track, Detail.quantification );
		}
		if( TRUE == Detail.bool_normalize ) {
			normalize_set( Detail.bool_peak, Detail.bool_peak_album, Detail.mix_rms_album, Detail.fix_rms );
		}

		// OP_CUE_INFO
		// OP_CUE_EXTRACT
		if( OP_CUE_NONE != Detail.cue )
			cue_set( Detail.cue );

	}

	remove_options();
	printf( "\n" );

	exit( EXIT_SUCCESS );
}
















