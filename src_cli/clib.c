/*
 *  file      : src/clib.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 *
 * ---
 * This code is to prevent the use of the glibc library.
 * Ce code est destiné à éviter l'utilisation de la librairie glibc.
 * ---
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>

#include "clib.h"



CList *C_list_first( CList *p_clist  )
{
	while( p_clist && p_clist->previous != NULL )
			p_clist = p_clist->previous;
	return( (CList *)p_clist );
}

CList *C_list_last( CList *p_clist )
{
	while( p_clist && p_clist->next != NULL )
			p_clist = p_clist->next;
	return( (CList *)p_clist );
}

CList *C_list_previous( CList *p_clist )
{
	return( (CList *)p_clist->previous );
}

CList *C_list_next( CList *p_clist )
{
	return( (CList *)p_clist->next );
}

CList *C_list_append( CList *p_clist, void *p_alloc )
{
	CList	*new_clist = NULL;
	CList	*list = NULL;
	new_clist = (CList *)malloc( sizeof( CList ) );
	new_clist->previous = NULL;
	new_clist->next = NULL;
	new_clist->data = p_alloc;
	if( NULL != (list = C_list_last( p_clist ))) {
		list->next = new_clist;
		new_clist->previous = list;
	}
	return( (CList *)new_clist );
}

CList *C_list_free( CList *p_clist )
{
	CList	*list = NULL;
	if( NULL != (list = C_list_first( p_clist )))
		while( list ) {
			list->data = NULL;
			list = C_list_next( list );
		}
	free( p_clist );
	p_clist = NULL;
	return( (CList *)NULL );
}

CString *C_string_free( CString *p_cstring )
{
	if( p_cstring ) {
		if( p_cstring->str ) free( p_cstring->str );
		free( p_cstring );
		p_cstring = NULL;
	}
	return( (CString *)NULL );
}

CString *C_string_new( void )
{
	CString *s = (CString *)malloc( sizeof( CString ));
	s->str = NULL;
	s->len = 0;
	return( (CString *)s );
}

CString *C_string_append_printf( CString *p_cstring, const char *fmt, ... )
{
	int n;
	int size = 100;     // Guess we need no more than 100 bytes
	char *p, *np;
	va_list ap;

	p = malloc( size );
	if( p == NULL )
		return NULL;

	while( 1 ) {
		// Try to print in the allocated space
		va_start( ap, fmt );
		n = vsnprintf( p, size, fmt, ap );
		va_end( ap );
		// Check error code
		if( n < 0 ) {
			free( p );
			return NULL;
		}
		// If that worked, return the string
		if( n < size ) {
			char *s = C_strdup_printf( "%s%s", p_cstring->str ? p_cstring->str : "", p );
			free( p );
			if( p_cstring->str ) free( p_cstring->str );
			p_cstring->str = s;
			p_cstring->len = strlen( s );
			return( p_cstring );
		}
		// Else try again with more space
		size = n + 1;       // Precisely what is needed
		np = realloc( p, size );
		if( np == NULL ) {
			free( p );
			return NULL;
		} else {
			p = np;
		}
	}
}

char *C_strdup( char *p_str )
{
	int len = ( sizeof( char *) * strlen( p_str ) ) + 2;
	char *new_alloc = (char *)malloc( len );
	strcpy( new_alloc, p_str );
	return( (char *)new_alloc );
}

char *C_strdup_printf( const char *fmt, ... )
{
	int n;
	int size = 100;     // Guess we need no more than 100 bytes
	char *p, *np;
	va_list ap;

	p = malloc( size );
	if( p == NULL )
		return NULL;

	while( 1 ) {
		// Try to print in the allocated space
		va_start( ap, fmt );
		n = vsnprintf( p, size, fmt, ap );
		va_end( ap );
		// Check error code
		if( n < 0 ) {
			free( p );
			return NULL;
		}
		// If that worked, return the string
		if( n < size )
			return p;
		// Else try again with more space
		size = n + 1;       // Precisely what is needed
		np = realloc( p, size );
		if( np == NULL ) {
			free( p );
			return NULL;
		} else {
			p = np;
		}
	}
}

C_BOOL C_dir_test( char *p_dir )
{
	struct stat status;
	int	    ret;
	ret = stat( p_dir, &status );
	return( ret > -1 && S_ISDIR( status.st_mode ) ? TRUE : FALSE );
}

C_BOOL C_file_test( char *p_file )
{
	struct stat status;
	int	    ret;
	ret = stat( p_file, &status );
	return( ret > -1 && S_ISREG( status.st_mode ) ? TRUE : FALSE );
}

C_BOOL C_mkdir_with_parents( char *p_path )
{
	char *LineCommand = NULL;
	if( p_path != NULL ) {
		if( C_dir_test( p_path ) == FALSE ) {
			LineCommand = C_strdup_printf( "mkdir -p \"%s\"", p_path );
			system( LineCommand );
			free( LineCommand );
			LineCommand = NULL;
			return( TRUE );
		}
	}
	return( FALSE );
}

void C_rmdir( char *p_path )
{
	pid_t  pid;
	if( p_path != NULL ) {
		if( (pid = fork()) == 0 ) {
			execlp (
				"rm",
				"rm",
				"-rf",
				p_path,
				NULL
				);
		}
	}
}

char **C_strsplit( const char *string, const char *delimiter )
{
	char **Buf = NULL;
	char *p = NULL;
	int n;

	for( n = 0, p = (char *)string; *p; p ++ )
		if( *p == *delimiter ) n ++;
	if( NULL == (Buf = (char **)malloc( sizeof( char ** )  * ( n + 4 ) ))) return( (char **)NULL );
	n = 0;
	p = strtok( (char *)string, delimiter );
	while( p != NULL ) {
		Buf[ n ++ ] = C_strdup( p );
		p = strtok( NULL, delimiter );
	}
	Buf[ n  ] = NULL;
	return( (char **)Buf );
}

char **C_strfreev( char **p_Larrbuf )
{
	int n;
	for( n = 0; p_Larrbuf[ n ]; n ++ )
		if(  p_Larrbuf[ n ] ) {
			free( p_Larrbuf[ n ] );
			p_Larrbuf[ n ] = NULL;
		}
	free( p_Larrbuf );
	p_Larrbuf = NULL;
	return( (char **)NULL );

}

int C_list_length( CList *p_clist )
{
	CList	*list = NULL;
	int		cpt = 0;
	if( NULL != (list = C_list_first( p_clist )))
		while( list ) {
			cpt ++;
			list = C_list_next( list );
		}
	return( (int)cpt );
}

char *C_path_get_basename( char *p_path )
{
	char *ptr = NULL;
	if( NULL != ( ptr = strrchr( p_path, '/' ))) {
		ptr ++;
		return( C_strdup( ptr ));
	}
	return( (char *)NULL );
}

char *C_ascii_strdown( char *p_str )
{
	char *str = C_strdup( p_str );
	char *ptr = NULL;
	for( ptr = str; *ptr; ptr ++ )
		*ptr = tolower( *ptr );
	return( (char *)str );
}

char *C_ascii_strup( char *p_str )
{
	char *str = C_strdup( p_str );
	char *ptr = NULL;
	for( ptr = str; *ptr; ptr ++ )
		*ptr = toupper( *ptr );
	return( (char *)str );
}

C_BOOL C_find_program_in_path( char *p_find )
{
	char *path[] = { "/bin/", "/usr/bin/", "/usr/sbin/" , "/usr/local/bin/" };
	char *p = NULL;
	C_BOOL bool_find = FALSE;
	int i;
	if( p_find )
		for( i = 0; i < 4; i ++ ) {
			p = C_strdup_printf( "%s%s", path[ i ], p_find );
			bool_find = C_file_test( p );
			free( p );
			p = NULL;
			if( TRUE == bool_find ) return( TRUE );
		}
	return( FALSE );
}


