/*
 *  file      : src/tags_wav.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "tags.h"
#include "get_info.h"


//
//
void tagswav_close_file( WAVE *WaveHeader )
{
	if (NULL != WaveHeader->file) {
		fclose(WaveHeader->file);
		WaveHeader->file = NULL;
	}
}
//
//
void tagswav_init (WAVE *WaveHeader)
{
	WaveHeader->RIFF.ChunkID [ 0 ]     = '\0';
	WaveHeader->RIFF.ChunkSize         = 0;
	WaveHeader->RIFF.Format [ 0 ]      = '\0';
	WaveHeader->FMT.Subchunk1ID [ 0 ]  = '\0';
	WaveHeader->FMT.Subchunk1Size      = 0;
	WaveHeader->FMT.AudioFormat        = 0;
	WaveHeader->FMT.NumChannels        = 0;
	WaveHeader->FMT.SampleRate         = 0;
	WaveHeader->FMT.ByteRate           = 0;
	WaveHeader->FMT.Blockalign         = 0;
	WaveHeader->FMT.BitsPerSample      = 0;
	WaveHeader->DATA.Subchunk2ID [ 0 ] = '\0';
	WaveHeader->DATA.Subchunk2Size     = 0;
	WaveHeader->DATA.data              = NULL;
	WaveHeader->file                   = NULL;
	WaveHeader->TagWavIsFmtBext        = TAG_WAVE_IS_NONE;
}
//
//
void tagswav_print_header( char *p_PathNameFile )
{
	WAVE	WaveHeader;
	if (TRUE == tagswav_read_file( p_PathNameFile, &WaveHeader )) {
		//printf ("\n" );
		//printf ("\t%s\n", p_PathNameFile );
		printf ("\tChunkID        = %c%c%c%c\n", WaveHeader.RIFF.ChunkID [ 0 ], WaveHeader.RIFF.ChunkID [ 1 ], WaveHeader.RIFF.ChunkID [ 2 ], WaveHeader.RIFF.ChunkID [ 3 ] );
		printf ("\tChunkSize      = %d\n", WaveHeader.RIFF.ChunkSize);
		printf ("\tFormat         = %c%c%c%c\n", WaveHeader.RIFF.Format [ 0 ], WaveHeader.RIFF.Format [ 1 ], WaveHeader.RIFF.Format [ 2 ], WaveHeader.RIFF.Format [ 3 ] );
		printf( "\t%s\n", (WaveHeader.TagWavIsFmtBext == TAG_WAVE_IS_FMT) ? "[ 'fmt ' ]" : "[ 'bext' | 'qlty' | 'levl' | 'link' | 'axml' ]" );
		printf ("\tSubchunk1ID    = %c%c%c%c\n", WaveHeader.FMT.Subchunk1ID [ 0 ], WaveHeader.FMT.Subchunk1ID [ 1 ], WaveHeader.FMT.Subchunk1ID [ 2 ], WaveHeader.FMT.Subchunk1ID [ 3 ] );
		printf ("\tSubchunk1Size  = %d\n", WaveHeader.FMT.Subchunk1Size);
		printf ("\tAudioFormat    = %d\n", WaveHeader.FMT.AudioFormat);
		printf ("\tNumChannels    = %d\n", WaveHeader.FMT.NumChannels);
		printf ("\tSampleRate     = %d\n", WaveHeader.FMT.SampleRate);
		printf ("\tByteRate       = %d\n", WaveHeader.FMT.ByteRate);
		printf ("\tBlockalign     = %d\n", WaveHeader.FMT.Blockalign);
		printf ("\tBitsPerSample  = %d\n", WaveHeader.FMT.BitsPerSample);
		printf ("\tSubchunk2ID    = %c%c%c%c\n", WaveHeader.DATA.Subchunk2ID [ 0 ], WaveHeader.DATA.Subchunk2ID [ 1 ], WaveHeader.DATA.Subchunk2ID [ 2 ], WaveHeader.DATA.Subchunk2ID [ 3 ] );
		printf ("\tSubchunk2Size  = %d\n", WaveHeader.DATA.Subchunk2Size);
		tagswav_close_file( &WaveHeader );
	}
}
//
//
#define BLOCK_SIZE	2352
//
//
int tagswav_get_pos( char *p_PathNameFile, char p_1, char p_2, char p_3, char p_4 )
{
	FILE		*file = NULL;
 	unsigned char 	devbuf[ BLOCK_SIZE +10 ];
 	int		n_read;
	int		tt_read = 0;
	int		cpt;

	if( NULL != (file = fopen( p_PathNameFile, "rb" ))) {

		do {
			// LECTURE D UN BLOCK
			n_read = fread( devbuf, BLOCK_SIZE, 1, file );

			// RECHERCHE DE: data
			for( cpt = 0; cpt < BLOCK_SIZE; cpt ++ ) {
				if( devbuf [ cpt +0 ] == p_1 && devbuf [ cpt +1 ] == p_2 && devbuf [ cpt +2 ] == p_3 && devbuf [ cpt +3 ] == p_4 ) {
					return( tt_read + cpt );
				}
			}

			// VARIABLE TOTAL OCTECTS LU MOINS 8 AU CAS OU data SOIS COUPE EN FIN DE BLOCK
			tt_read += BLOCK_SIZE;
			tt_read -= 8;

			// POINTEUR AU DEBUT DU FICHIER
			rewind( file );

			// POINTEUR DE LECTURE SUR tt_read
			fseek( file, tt_read,  SEEK_CUR );

		} while( n_read > 0 );

		fclose( file );
	}

	return( -1 );
}
//
// MODIF DU 02 MAI 2013 A LA DEMANDE DE CHRISTOPHE MARILLAT POUR TENIR COMPTE DE : Broadcast Wave Format
// VERSION DE TEST:
//	xcfa-4.3.4~beta0
//	xcfa-4.3.4~beta1
//  xcfa_cli
/*

broadcast_audio_extension typedef struct {
  DWORD  ckID;                   		 // (broadcastextension)ckID=bext.
  DWORD  ckSize;                 		 // size of extension chunk
  BYTE   ckData[ckSize];         		 // data of the chunk
}


Quality_chunk typedef struct {
  DWORD          ckID;           		// (quality_chunk) ckID='qlty'
  DWORD          ckSize;         		// size of quality chunk
  BYTE           ckData[ckSize]; 		// data of the chunk
}


typedef struct peak_envelope
{
   CHAR           ckID[4];				// {'l','e','v','l'}
   DWORD          ckSize;				// size of chunk
   DWORD          dwVersion;			// version information
   DWORD          dwFormat;				// format of a peak point
										// 1 = unsigned char
										// 2 = unsigned short
   DWORD          dwPointsPerValue;		// 1 = only positive peak point
										// 2 = positive AND negative peak point
   DWORD          dwBlockSize;			// frames per value
   DWORD          dwPeakChannels;		// number of channels
   DWORD          dwNumPeakFrames;		// number of peak frames
   DWORD          dwPosPeakOfPeaks;		// audio sample frame index
										// or 0xFFFFFFFF if unknown
   DWORD          dwOffsetToPeaks;		// should usually be equal to the size of this header, but could also be higher
   CHAR           strTimestamp[28];		// ASCII: time stamp of the peak data
   CHAR           reserved[60];			// reserved set to 0x00
   CHAR           peak_envelope_data[]	// the peak point data
}
levl_chunk;


typedef struct link
{
    CHAR          CkID[4];				// 'link'
    DWORD         CkSize;				// size of chunk
    CHAR          XmlData[ ];			// text data in XML
}
link_chunk;


typedef struct axml
{
    CHAR        ckID[4];				// {'a','x','m','l'}
    DWORD       ckSize;					// size of chunk
    CHAR        xmlData[ ];				// text data in XML
}
axml_chunk;

*/
C_BOOL tagswav_read_file (char *wave_file, WAVE *WaveHeader)
{
	int	seek_fmt = -1;
	int	seek_data = -1;
 	int	n_read;

	seek_fmt  = tagswav_get_pos( wave_file, 'f', 'm', 't', ' ' );
	seek_data = tagswav_get_pos( wave_file, 'd', 'a', 't', 'a' );
	if( -1 == seek_fmt || -1 == seek_data ) return( FALSE );

	if( NULL == ( WaveHeader->file = fopen( wave_file, "rb" ))) return (FALSE);

	// LECTURE ENTETE
	n_read = fread( &WaveHeader->RIFF.ChunkID, 4, 1, WaveHeader->file );		// RIFF
	n_read = fread( &WaveHeader->RIFF.ChunkSize, 4, 1, WaveHeader->file );		// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
	n_read = fread( &WaveHeader->RIFF.Format, 4, 1, WaveHeader->file );			// WAVE
	n_read = fread( &WaveHeader->FMT.Subchunk1ID, 4, 1, WaveHeader->file );		// 'fmt ' | 'bext'

	if( 0 == strncmp( WaveHeader->FMT.Subchunk1ID, "fmt ", 4 )) {
		WaveHeader->TagWavIsFmtBext = TAG_WAVE_IS_FMT;
	}
	else {
		WaveHeader->TagWavIsFmtBext = TAG_WAVE_IS_BEXT;
		n_read = fseek( WaveHeader->file, seek_fmt + 4, SEEK_SET );
	}

	// FMT
	n_read = fread( &WaveHeader->FMT.Subchunk1Size, 4, 1, WaveHeader->file );	// taille en octet des données à suivre
	n_read = fread( &WaveHeader->FMT.AudioFormat, 2, 1, WaveHeader->file );		// format de compression (une valeur autre que 1 indique une compression)
	n_read = fread( &WaveHeader->FMT.NumChannels, 2, 1, WaveHeader->file );		// nombre de canaux
	n_read = fread( &WaveHeader->FMT.SampleRate, 4, 1, WaveHeader->file );		// fréquence d'échantillonage (nombre d'échantillons par secondes)
	n_read = fread( &WaveHeader->FMT.ByteRate, 4, 1, WaveHeader->file );		// nombre d'octects par secondes
	n_read = fread( &WaveHeader->FMT.Blockalign, 2, 1, WaveHeader->file );		// nombre d'octects pour coder un échantillon
	n_read = fread( &WaveHeader->FMT.BitsPerSample, 2, 1, WaveHeader->file );	// nombre de bits pour coder un échantillon

	// DATA
	n_read = fseek( WaveHeader->file, seek_data, SEEK_SET );
	n_read = fread( &WaveHeader->DATA.Subchunk2ID, 4, 1, WaveHeader->file );
	n_read = fread( &WaveHeader->DATA.Subchunk2Size, 4, 1, WaveHeader->file );

	if( n_read );
	// 	tagswav_print_header( wave_file,  WaveHeader );

	return( TRUE );

}
//
//
INFO_WAV *tagswav_remove_info (INFO_WAV *info)
{
	if (info) {
		if (NULL != info->time)		{ free (info->time);		info->time = NULL;	}
		if (NULL != info->hertz)	{ free (info->hertz);		info->hertz = NULL;	}
		if (NULL != info->voie)		{ free (info->voie);		info->voie = NULL;	}
		if (NULL != info->bits)		{ free (info->bits);		info->bits = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_WAV *)NULL);
}
//
//
C_BOOL tagswav_file_GetBitrate (char *namefile, int *Channels, int *Hertz, int *Bits)
{
	WAVE	WaveHeader;

	if (TRUE == tagswav_read_file (namefile, &WaveHeader)) {
		tagswav_close_file( &WaveHeader );
		if( NULL != Channels )	*Channels = WaveHeader.FMT.NumChannels;
		if( NULL != Hertz )		*Hertz    = WaveHeader.FMT.SampleRate;
		if( NULL != Bits )		*Bits     = WaveHeader.FMT.BitsPerSample;

		return ((WaveHeader.FMT.NumChannels != 2 || WaveHeader.FMT.SampleRate != 44100 || WaveHeader.FMT.BitsPerSample != 16) ? TRUE : FALSE);
	}
	return (FALSE);
}


void tags_wav_print( INFO_WAV *ptrinfo )
{
	printf( "\ttime        = %s\n", ptrinfo->time );
	printf( "\tSecTime     = %d\n", ptrinfo->SecTime );
	printf( "\thertz       = %s\n", ptrinfo->hertz );
	printf( "\tvoie        = %s\n", ptrinfo->voie );
	printf( "\tbits        = %s\n", ptrinfo->bits );
	printf( "\tLevelDbfs   = %d\n", ptrinfo->LevelDbfs );
}


//
//
INFO_WAV *tagswav_get_info (INFO *p_info)
{
	WAVE		WaveHeader;
	INFO_WAV	*ptrinfo = NULL;
	int		m;
	int		s;
	int		sec;
	C_BOOL	BoolReadWavFile;

	// PRINT_FUNC_LF();

	ptrinfo = (INFO_WAV *)malloc (sizeof (INFO_WAV));
	if (NULL == ptrinfo) {
		printf ("!---------------------------------------\n");
		PRINT_FUNC_LF();
		printf ("!---------------------------------------\n");
		printf ("! PTRINFO EST NULL  :(\n");
		printf ("!---------------------------------------\n");
		return (NULL);
	}

	BoolReadWavFile = tagswav_read_file (p_info->path_name_file, &WaveHeader);
	tagswav_close_file( &WaveHeader );

	if (FALSE == BoolReadWavFile) {
		free (ptrinfo);
		PRINT("EXIT");
		ptrinfo = NULL;
		return (NULL);
	}
	if (WaveHeader.DATA.Subchunk2Size == 0 || WaveHeader.FMT.ByteRate == 0) {
		free (ptrinfo);
		ptrinfo = NULL;
		printf ("! ---\n");
		printf ("! MAUVAIS ENTETE DU FICHIER: %s\n", p_info->path_name_file);
		printf ("! ---\n\n");
		return (NULL);
	}

	// calcul de la duree
	sec = WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate;
	ptrinfo->SecTime              = sec;

	s = sec % 60; sec /= 60;
	m = sec % 60; sec /= 60;
	if (sec > 0) ptrinfo->time = C_strdup_printf ("%02d:%02d:%02d", sec, m, s);
	else         ptrinfo->time = C_strdup_printf ("00:%02d:%02d", m, s);

	// 'fmt ', 'bext', 'qlty', 'levl', 'link','axml'
	ptrinfo->BoolBwf = (WaveHeader.TagWavIsFmtBext == TAG_WAVE_IS_FMT) ? FALSE : TRUE;

	// mode
	ptrinfo->voie    = C_strdup_printf ("%d", WaveHeader.FMT.NumChannels);

	// freq
	ptrinfo->hertz    = C_strdup_printf ("%d", WaveHeader.FMT.SampleRate);

	// format
	ptrinfo->bits    = C_strdup_printf ("%d", WaveHeader.FMT.BitsPerSample);

	ptrinfo->tags = (TAGS *)tags_alloc ();
	tags_complementation( ptrinfo->tags,  p_info->path_name_file );

	ptrinfo->LevelDbfs = GetInfo_level_get_from( FILE_IS_WAV, p_info->path_name_file );

	return( (INFO_WAV *)ptrinfo );
}







