/*
 *  file      : src/tags_shn.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>
#include "get_info.h"





/*
*---------------------------------------------------------------------------
* REMOVE HEADER
*---------------------------------------------------------------------------
*/
INFO_SHN *tagsshn_remove_info (INFO_SHN *info)
{
	if (info) {
		if (NULL != info->time)		{ free (info->time); info->time = NULL;	}
		if (NULL != info->size)		{ free (info->size);info->size = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_SHN *)NULL);
}


void tags_shn_print( INFO_SHN *ptrinfo )
{
	printf( "time            = %s\n", ptrinfo->time );
	printf( "SecTime         = %d\n", ptrinfo->SecTime );
	printf( "size            = %s\n", ptrinfo->size );
}


/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/
INFO_SHN *tagsshn_get_info( INFO *p_info )
{
	INFO_SHN	*ptrinfo = NULL;
	SHNTAG		*ShnTag = GetInfo_shntool ( p_info->path_name_file );

	ptrinfo = (INFO_SHN *)malloc (sizeof (INFO_SHN));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc ();

	ptrinfo->SecTime  = ShnTag->SecTime;
	ptrinfo->time     = C_strdup (ShnTag->time);
	ptrinfo->size     = C_strdup (ShnTag->size);

	tags_complementation( ptrinfo->tags,  p_info->path_name_file );

	ShnTag = GetInfo_free_shntool (ShnTag);

	return ((INFO_SHN *)ptrinfo);
}
