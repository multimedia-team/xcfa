/*
 *  file      : src/wav.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "conv.h"
#include "tags.h"



//
// Changing the settings of a WAV file
// --frequency      -f       Changing the frequency:        8000, 22000, 32000, 44056, 44100, 48000, 88200, 96000 or other
// --track          -t       Changing the number of tracks: 1, 2, 4, 6
// --quantification -q       Changing the quantification:   8, 16, 24, 32, 64
//
void wav_change_setting( char *p_pathnamefile, int p_frequency, int p_track, int p_quantification )
{
	char	**PtrTabArgs = NULL;
	int		pos;
	int		Channels;
	int		Hertz;
	int		Bits;
	char	*NewName = C_strdup_printf( "%s.setting.wav", p_pathnamefile );

	printf("\n!--- S E T T I N G    P A R A M E T E R S    W A V ---------!\n");
	if( p_frequency <= 0 && p_frequency > 96000 ) p_frequency = 44100;
	if( p_track != 1 && p_track != 2 &&  p_track != 4 &&  p_track != 6  ) p_track = 2;
	if( p_quantification != 8 && p_quantification != 16 &&  p_quantification != 24 &&  p_quantification != 32 &&  p_quantification != 64 ) p_quantification = 16;

	PtrTabArgs = conv_AllocTabArgs( &pos );

	PtrTabArgs [ pos++ ] = C_strdup( "mplayer" );
	PtrTabArgs [ pos++ ] = C_strdup( "-nojoystick" );
	PtrTabArgs [ pos++ ] = C_strdup( "-nolirc" );
	PtrTabArgs [ pos++ ] = C_strdup( p_pathnamefile );
	PtrTabArgs [ pos++ ] = C_strdup( "-ao" );
	PtrTabArgs [ pos++ ] = C_strdup( "pcm" );
	PtrTabArgs [ pos++ ] = C_strdup( "-ao" );
	PtrTabArgs [ pos++ ] = C_strdup_printf( "pcm:file=%s", NewName );
	PtrTabArgs [ pos++ ] = C_strdup( "-af" );
	PtrTabArgs [ pos++ ] = C_strdup_printf( "channels=%d", p_track );
	PtrTabArgs [ pos++ ] = C_strdup( "-srate" );
	PtrTabArgs [ pos++ ] = C_strdup_printf( "%d", p_frequency );
	PtrTabArgs [ pos++ ] = NULL;
	conv_to_convert( PtrTabArgs, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV" );
	PtrTabArgs = conv_RemoveTab( PtrTabArgs );

	conv_copy_src_to_dest( NewName, p_pathnamefile );
	remove( NewName );

	tagswav_file_GetBitrate( p_pathnamefile, &Channels, &Hertz, &Bits );

	if( Bits != p_quantification ) {
		fileconv_change_quantification_with_sox( TRUE, p_pathnamefile, NewName, p_frequency, p_track, p_quantification );
		remove( NewName );
	}

	free( NewName );
	NewName = NULL;

}

void loop_wav_change_setting( int p_frequency, int p_track, int p_quantification )
{
	CList	*list = NULL;
	INFO	*Info = NULL;

	if( NULL != ( list = C_list_first( Detail.ListFile ))) {
		while( list ) {
			if( NULL != (Info = (INFO *)list->data) ) {
				if( Info->type_infosong_file_is == FILE_IS_WAV ) {

					wav_change_setting( Info->path_name_file, p_frequency, p_track, p_quantification );
				}
			}
			list = C_list_next( list );
		}
	}
}












