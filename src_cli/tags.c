/*
 *  file      : src/tags.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>



//
//
TAGS *tags_alloc( void )
{
	TAGS *new_tag = (TAGS *)malloc (sizeof (TAGS));
	new_tag->Album       = NULL;
	new_tag->Artist      = NULL;
	new_tag->Title       = NULL;
	new_tag->Number      = NULL;
	new_tag->Genre       = NULL;
	new_tag->Year        = NULL;
	new_tag->Comment     = NULL;
	new_tag->Description = NULL;
	new_tag->IntNumber   = 1;
	new_tag->IntGenre    = 1;
	new_tag->IntYear     = 1962;
	return( (TAGS *)new_tag );
}
//
//
TAGS *tags_remove (TAGS *p_tags)
{
	if (NULL != p_tags) {
		if( NULL != p_tags->Album ) 		{	free( p_tags->Album );		p_tags->Album = NULL;		}
		if( NULL != p_tags->Artist )		{ 	free( p_tags->Artist );		p_tags->Artist = NULL;		}
		if( NULL != p_tags->Title )			{	free( p_tags->Title );		p_tags->Title = NULL;		}
		if( NULL != p_tags->Number )		{	free( p_tags->Number );		p_tags->Number = NULL;		}
		if( NULL != p_tags->Genre )			{ 	free( p_tags->Genre );		p_tags->Genre = NULL;		}
		if( NULL != p_tags->Year )			{ 	free( p_tags->Year );			p_tags->Year = NULL;		}
		if( NULL != p_tags->Comment )		{ 	free( p_tags->Comment );		p_tags->Comment = NULL;		}
		if( NULL != p_tags->Description )	{ 	free( p_tags->Description );	p_tags->Description = NULL;	}
		free( p_tags );
		p_tags = NULL;
	}
	return( (TAGS *)NULL );
}


//
//
void tags_complementation( TAGS *p_tags, char *path_name_file )
{
	char *Ptr = NULL;

	// Album
	// Artist
	if( p_tags->Artist == NULL ) {
		if( NULL != (Ptr = strrchr( path_name_file, '/' ))) {
			Ptr ++;
			p_tags->Artist = C_strdup( Ptr );
		}
		else {
			p_tags->Artist = C_strdup( path_name_file );
		}
		if( NULL != (Ptr = strrchr( p_tags->Artist, '.' ))) *Ptr = '\0';
	}
	// Title
	if( p_tags->Title == NULL ) {
		if( NULL != (Ptr = strrchr( path_name_file, '/' ))) {
			Ptr ++;
			p_tags->Title = C_strdup( Ptr );
		}
		else {
			p_tags->Title = C_strdup( path_name_file );
		}
		if( NULL != (Ptr = strrchr( p_tags->Title, '.' ))) *Ptr = '\0';
	}
	// Number
	// IntNumber
	if( NULL == p_tags->Number ) {
		p_tags->Number      = C_strdup( "1" );
		p_tags->IntNumber   = 1;
	} else {
		p_tags->IntNumber   = atoi( p_tags->Number );
	}
	// Genre
	// IntGenre
	if( NULL == p_tags->Genre ) {
		p_tags->Genre      = C_strdup( "1" );
		p_tags->IntGenre   = 1;
	} else {
		p_tags->IntGenre   = atoi( p_tags->Genre );
	}
	// Year
	// IntYear
	if( NULL == p_tags->Year ) {
		p_tags->Year        = C_strdup( "1962" );
		p_tags->IntYear     = 1962;
	} else {
		p_tags->IntYear     = atoi( p_tags->Year );
	}
	// Comment
	// Description
}


STRUCT_TAGS_FILE_MP3 StructTagsFileMp3 [] = {

{123, "A Cappella"},
{ 74, "Acid Jazz"},
{ 73, "Acid Punk"},
{ 34, "Acid"},
{ 99, "Acoustic"},
{ 40, "Alt. Rock"},
{ 20, "Alternative"},
{ 26, "Ambient"},
{145, "Anime"},
{ 90, "Avantgarde"},
{116, "Ballad"},
{ 41, "Bass"},
{135, "Beat"},
{ 85, "Bebob"},
{ 96, "Big Band"},
{138, "Black Metal"},
{ 89, "Bluegrass"},
{  0, "Blues"},
{107, "Booty Bass"},
{132, "BritPop"},
{ 65, "Cabaret"},
{ 88, "Celtic"},
{104, "Chamber Music"},
{102, "Chanson"},
{ 97, "Chorus"},
{136, "Christian Gangsta Rap"},
{ 61, "Christian Rap"},
{141, "Christian Rock"},
{  1, "Classic Rock"},
{ 32, "Classical"},
{128, "Club-House"},
{112, "Club"},
{ 57, "Comedy"},
{140, "Contemporary Christian"},
{  2, "Country"},
{139, "Crossover"},
{ 58, "Cult"},
{125, "Dance Hall"},
{  3, "Dance"},
{ 50, "Darkwave"},
{ 22, "Death Metal"},
{  4, "Disco"},
{ 55, "Dream"},
{127, "Drum & Bass"},
{122, "Drum Solo"},
{120, "Duet"},
{ 98, "Easy Listening"},
{ 52, "Electronic"},
{ 48, "Ethnic"},
{124, "Euro-House"},
{ 25, "Euro-Techno"},
{ 54, "Eurodance"},
{ 84, "Fast-Fusion"},
{ 81, "Folk/Rock"},
{115, "Folklore"},
{ 80, "Folk"},
{119, "Freestyle"},
{  5, "Funk"},
{ 30, "Fusion"},
{ 36, "Game"},
{ 59, "Gangsta Rap"},
{126, "Goa"},
{ 38, "Gospel"},
{ 91, "Gothic Rock"},
{ 49, "Gothic"},
{  6, "Grunge"},
{ 79, "Hard Rock"},
{129, "Hardcore"},
{137, "Heavy Metal"},
{  7, "Hip-Hop"},
{ 35, "House"},
{100, "Humour"},
{131, "Indie"},
{ 19, "Industrial"},
{ 46, "Instrumental Pop"},
{ 47, "Instrumental Rock"},
{ 33, "Instrumental"},
{146, "JPop"},
{ 29, "Jazz+Funk"},
{  8, "Jazz"},
{ 63, "Jungle"},
{ 86, "Latin"},
{ 71, "Lo-Fi"},
{ 45, "Meditative"},
{142, "Merengue"},
{  9, "Metal"},
{148, "Misc"},
{ 77, "Musical"},
{ 82, "National Folk"},
{ 64, "Native American"},
{133, "Negerpunk"},
{ 10, "New Age"},
{ 10, "NewAge"},
{ 66, "New Wave"},
{ 39, "Noise"},
{ 11, "Oldies"},
{103, "Opera"},
{ 12, "Other"},
{ 75, "Polka"},
{134, "Polsk Punk"},
{ 53, "Pop-Folk"},
{ 62, "Pop/Funk"},
{ 13, "Pop"},
{109, "Porn Groove"},
{117, "Power Ballad"},
{ 23, "Pranks"},
{108, "Primus"},
{ 92, "Progressive Rock"},
{ 93, "Psychedelic Rock"},
{ 67, "Psychedelic"},
{121, "Punk Rock"},
{ 43, "Punk"},
{ 14, "R&B"},
{ 15, "Rap"},
{ 68, "Rave"},
{ 16, "Reggae"},
{ 76, "Retro"},
{ 87, "Revival"},
{118, "Rhythmic Soul"},
{ 78, "Rock & Roll"},
{ 17, "Rock"},
{143, "Salsa"},
{114, "Samba"},
{110, "Satire"},
{ 69, "Showtunes"},
{ 21, "Ska"},
{111, "Slow Jam"},
{ 95, "Slow Rock"},
{105, "Sonata"},
{ 42, "Soul"},
{ 37, "Sound Clip"},
{ 24, "Soundtrack"},
{ 56, "Southern Rock"},
{ 44, "Space"},
{101, "Speech"},
{ 83, "Swing"},
{ 94, "Symphonic Rock"},
{106, "Symphony"},
{147, "Synthpop"},
{113, "Tango"},
{ 51, "Techno-Industrial"},
{ 18, "Techno"},
{130, "Terror"},
{144, "Thrash Metal"},
{ 60, "Top 40"},
{ 70, "Trailer"},
{ 31, "Trance"},
{ 72, "Tribal"},
{ 27, "Trip-Hop"},
{ 28, "Vocal"},
{102, "Chanson française"},
{-1,  NULL}
};
//
//
int tags_get_genre_by_value( char *p_name )
{
	int		i;
	int		RetNum = 1;
	char	*NameSrc = C_ascii_strdown( p_name );
	char	*NameStruct = NULL;

	for( i=0; StructTagsFileMp3[ i ].num != -1; i++ ) {
		NameStruct = C_ascii_strdown( StructTagsFileMp3[ i ].name );
		if( strlen( NameSrc ) == strlen( NameStruct ) && 0 == strncmp( NameSrc, NameStruct, strlen( NameSrc ))) {
			RetNum = StructTagsFileMp3[ i ].num;
			break;
		}
		free( NameStruct );	NameStruct = NULL;
	}
	if( NULL != NameSrc )	 { free( NameSrc );		NameSrc = NULL;		}
	if( NULL != NameStruct ) { free( NameStruct );	NameStruct = NULL;	}
	return( RetNum );
}
//
//
char *tags_get_genre_by_name( int value )
{
	int i;

	if( value < 0 || value > 148 ) return ((char *)NULL );
	for( i=0; StructTagsFileMp3[ i ].num != value; i++ );
	return( (char *)StructTagsFileMp3[ i ].name );
}


//
//
void tags_print( char *p_namefile, TAGS *p_tags )
{
	printf("\tAlbum        = %s\n", p_tags->Album ? p_tags->Album : "" );
	printf("\tArtist       = %s\n", p_tags->Artist ? p_tags->Artist : "" );
	printf("\tTitle        = %s\n", p_tags->Title ? p_tags->Title : "" );
	printf("\tNumber       = %s\n", p_tags->Number ? p_tags->Number : "" );
	printf("\tIntNumber    = %d\n", p_tags->IntNumber );
	printf("\tGenre        = %s\n", p_tags->Genre ? p_tags->Genre : "" );
	printf("\tIntGenre     = %d\n", p_tags->IntGenre );
	printf("\tYear         = %s\n", p_tags->Year ? p_tags->Year : "" );
	printf("\tIntYear      = %d\n", p_tags->IntYear );
	printf("\tComment      = %s\n", p_tags->Comment ? p_tags->Comment : "" );
	printf("\tDescription  = %s\n", p_tags->Description ? p_tags->Description : "" );
}

