/*
 *  file      : src/tags_dts.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>



/*
*---------------------------------------------------------------------------
* FILE IS DTS ?
*---------------------------------------------------------------------------
*/
/*
$ file *
	audio.dts:    data
*/

/*
*---------------------------------------------------------------------------
* REMOVE HEADER
*---------------------------------------------------------------------------
*/
INFO_DTS *tagsdts_remove_info (INFO_DTS *info)
{
	if (info) {
		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_DTS *)NULL);
}

/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/
INFO_DTS *tagsdts_get_info( INFO *p_info )
{
	INFO_DTS     *ptrinfo = NULL;

	ptrinfo = (INFO_DTS *)malloc (sizeof (INFO_DTS));
	if (ptrinfo == NULL) return (NULL);

	ptrinfo->tags = (TAGS *)tags_alloc ();
	tags_complementation( ptrinfo->tags,  p_info->path_name_file );

	return ((INFO_DTS *)ptrinfo);
}











