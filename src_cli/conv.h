/*
 *  file      : src/conv.h
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */



#ifndef conv_h
#define conv_h 1

#include <sys/types.h>
#include <signal.h>


typedef enum {						// Les types de conversions possibles
	NONE_CONV = 0,					//
	FLAC_FLAC_TO_WAV,				//
	FLAC_WAV_TO_FLAC,				//
	LAME_WAV_TO_MP3,				//
	LAME_FLAC_TO_MP3,				//
	OGGENC_WAV_TO_OGG,				//
	OGGENC_FLAC_TO_OGG,				//
	OGG123_OGG_TO_WAV,				//
	MPG321_MP3_TO_WAV,				//
	SOX_WAV_TO_WAV,					//
	SHORTEN_SHN_TO_WAV,				//
	FAAD_M4A_TO_WAV,				//
	FAAC_WAV_TO_M4A,				//
	MPLAYER_WAV_TO_WAV,				//
	MPLAYER_WMA_TO_WAV,				//
	MPLAYER_AUDIO_TO_WAV,			//
	MPLAYER_M4A_TO_WAV,				//
	MPLAYER_OGG_TO_WAV,				//
	MPLAYER_MP3_TO_WAV,				//
	MPLAYER_MPC_TO_WAV,				//
	MPLAYER_RM_TO_WAV,				//
	MPLAYER_DTS_TO_WAV,				//
	MPLAYER_AIFF_TO_WAV,			//
	MPPDEC_MPC_TO_WAV,				//
	MPPENC_WAV_TO_MPC,				//
	MAC_APE_TO_WAV,					//
	MAC_WAV_TO_APE,					//
	WAVPACK_WAV_TO_WAVPACK,			//
	WVUNPACK_WAVPACK_TO_WAV,		//
	AACPLUSENC_WAV_TO_AAC,			//
	A52DEC_AC3_TO_WAV,				//
	REPLAYGAIN,						//
	NORMALISE_EXEC,					//
	NORMALISE_GET_LEVEL				//

} TYPE_CONV;

typedef struct {
	char		*TmpRep;							// Le repertoire temporaire d'extractions/conversions
	pid_t		code_fork_conv;						// Num Code Fork
	int			signal_numchildren_conv;			// the signal handler
	int			tube_conv [ 2 ];					// for pipe
	double		value_PEAK_RMS_GROUP_ARGS;			//
} CONV;

extern CONV conv;

C_BOOL		conv_to_convert( char **p_TabArgs, TYPE_CONV type_conv, char *info );
C_BOOL  	conv_copy_src_to_dest( char *filesrc, char *filedest );
char 		**conv_with_sox_get_param( char *filesrc, char *filedest, char *frequence, char *voie, char *bits );
char		**conv_with_sox_float_get_param( char *filesrc, char *filedest );
char		**conv_get_command_line(
				TYPE_CONV	type_conv,
				char		*src,
				char		*dest,
				TAGS		*InfosTags,
				char		*options
				);
char		**conv_RemoveTab( char **p_PtrTabArgs );
char		**conv_AllocTabArgs( int *p_tab );

#endif


