/*
 *  file      : src/replaygain.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
// #include <fcntl.h>	// PROCESSUS NON-BLOQUANT

#include "global.h"
#include "conv.h"



//
// Renvoie une liste de pointeur sur les fichier de type 'TYPE_FILE_IS p_type_infosong_file_is'
// depuis la liste choisie par l'utilisateur ou renvoie NULL
//
CList *replaygain_get_list( TYPE_FILE_IS p_type_infosong_file_is )
{
	CList		*list = NULL;
	INFO		*Info = NULL;
	CList		*list_replaygain = NULL;

	if( NULL != ( list = C_list_first( Detail.ListFile ))) {
		while( list ) {
			if( NULL != (Info = (INFO *)list->data) ) {
				if( Info->type_infosong_file_is == p_type_infosong_file_is )
					list_replaygain = C_list_append( list_replaygain, Info->path_name_file );
			}
			list = C_list_next( list );
		}
	}
	return( list_replaygain );
}
//
// Annule le pointeur de structure et detruit la liste 'CList *p_list'
// Renvoie NULL
//
CList *replaygain_remove_glist( CList *p_list )
{
	CList *list = NULL;

	if( p_list != NULL ) {
		list = C_list_first( p_list );
		while( list ) {
			list->data = NULL;
			list = C_list_next( list );
		}
		C_list_free( p_list );
		p_list = NULL;
	}
	return( (CList *)NULL );
}
//
// FILE_IS_FLAC, FILE_IS_MP3, FILE_IS_OGG, FILE_IS_WAVPACK
//
C_BOOL replaygain_verify_list( void )
{
	CList		*list = NULL;
	INFO		*Info = NULL;

	if( NULL != ( list = C_list_first( Detail.ListFile ))) {
		while( list ) {
			if( NULL != (Info = (INFO *)list->data) ) {
				if( Info->type_infosong_file_is == FILE_IS_FLAC ||
					Info->type_infosong_file_is == FILE_IS_MP3 ||
					Info->type_infosong_file_is == FILE_IS_OGG ||
					Info->type_infosong_file_is == FILE_IS_WAVPACK )
					return( TRUE );
			}
			list = C_list_next( list );
		}
	}
	return( FALSE );
}


/*
 Verify if exist file type:
    FILE_IS_FLAC
    FILE_IS_MP3
    FILE_IS_OGG
    FILE_IS_WAVPACK
 and assign type 'OP_REPLAYGAIN TypeReplaygain'


 ! Replaygain: dynamic modification for next files: FLAC, MP3, OGG, WAVPACK
 !
 ! -g <clear|album|track>   --replaygain <clear|album|track>
 !                                      FLAC    [ clear | album ]
 !                                      MP3     [ clear | album | track ]
 !                                      OGG     [ clear | album | track ]
 !                                      WAVPACK [ clear | album | track ]
*/
void replaygain_set( OP_REPLAYGAIN TypeReplaygain )
{
	CList	*list_replaygain = NULL;
	CList	*list = NULL;
	int		pos;
	char	**PtrTabArgs = NULL;
	int		tab_file [ 4 ] = { FILE_IS_FLAC, FILE_IS_MP3, FILE_IS_OGG, FILE_IS_WAVPACK };
	int		cpt;
	C_BOOL	bool_replaygain = FALSE;
	char	StrReplaygainDetail[ 40 ];

	if( FALSE == replaygain_verify_list() ) {
		printf( "%s", BOLD_RED );
		printf("\n!--- R E P L A Y G A I N -----------------------------------!\n");
		printf( "! \n");
		printf( "! File: FLAC or MP3 or OGG or WAVPACK not found\n" );
		printf( "! \n");
		printf( "!------------------------------------------------------------!\n" );
		printf( "\n" );
		printf( "%s", RESET );
		return;
	}

	for( cpt = 0; cpt < 4; cpt ++ ) {
		if( NULL != (list_replaygain = replaygain_get_list( tab_file [ cpt ] ))) {

			bool_replaygain = FALSE;
			PtrTabArgs = conv_AllocTabArgs( &pos );

			if( FILE_IS_FLAC == tab_file [ cpt ] && OP_REPLAYGAIN_TRACK != TypeReplaygain ) {

				bool_replaygain = TRUE;
				PtrTabArgs [ pos++ ] = C_strdup( "metaflac" );
				if(  OP_REPLAYGAIN_CLEAR == TypeReplaygain )
					PtrTabArgs [ pos++ ] = C_strdup( "--remove-replay-gain" );
				else if( OP_REPLAYGAIN_ALBUM == TypeReplaygain )
					PtrTabArgs [ pos++ ] = C_strdup( "--add-replay-gain" );

			}
			else if( FILE_IS_MP3 == tab_file [ cpt ] ) {

				bool_replaygain = TRUE;
				PtrTabArgs [ pos++ ] = C_strdup( "mp3gain" );
				if(  OP_REPLAYGAIN_CLEAR == TypeReplaygain ) {
					PtrTabArgs [ pos++ ] = C_strdup( "-s" );
					PtrTabArgs [ pos++ ] = C_strdup( "d" );
				}
				else if( OP_REPLAYGAIN_ALBUM == TypeReplaygain ) {
					PtrTabArgs [ pos++ ] = C_strdup( "-a" );
					PtrTabArgs [ pos++ ] = C_strdup( "-p" );
					PtrTabArgs [ pos++ ] = C_strdup( "-c" );
					PtrTabArgs [ pos++ ] = C_strdup( "-f" );
					PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				}
				else if( OP_REPLAYGAIN_TRACK == TypeReplaygain ) {
					PtrTabArgs [ pos++ ] = C_strdup( "-r" );
					PtrTabArgs [ pos++ ] = C_strdup( "-p" );
					PtrTabArgs [ pos++ ] = C_strdup( "-c" );
					PtrTabArgs [ pos++ ] = C_strdup( "-f" );
					PtrTabArgs [ pos++ ] = C_strdup( "-T" );
				}

			}
			else if( FILE_IS_OGG == tab_file [ cpt ] ) {

				bool_replaygain = TRUE;
				PtrTabArgs [ pos++ ] = C_strdup( "vorbisgain" );
				if(  OP_REPLAYGAIN_CLEAR == TypeReplaygain )
					PtrTabArgs [ pos++ ] = C_strdup ("-c" );
				else if( OP_REPLAYGAIN_ALBUM == TypeReplaygain )
					PtrTabArgs [ pos++ ] = C_strdup ("-a" );
				else if( OP_REPLAYGAIN_TRACK == TypeReplaygain );
					// None op

			}
			else if( FILE_IS_WAVPACK == tab_file [ cpt ] ) {

				bool_replaygain = TRUE;
				PtrTabArgs [ pos++ ] = C_strdup ("wvgain" );
				if(  OP_REPLAYGAIN_CLEAR == TypeReplaygain )
					PtrTabArgs [ pos++ ] = C_strdup( "-c" );
				else if( OP_REPLAYGAIN_ALBUM == TypeReplaygain )
					PtrTabArgs [ pos++ ] = C_strdup( "-a" );
				else if( OP_REPLAYGAIN_TRACK == TypeReplaygain );
					// None op

			}

			if( TRUE == bool_replaygain ) {
				list = C_list_first( list_replaygain );
				while( list ) {
					if( NULL != list->data )
						PtrTabArgs [ pos++ ] = C_strdup( list->data );
					list = C_list_next( list );
				}
				PtrTabArgs [ pos++ ] = NULL;

				if(  OP_REPLAYGAIN_CLEAR == TypeReplaygain )
					strcpy( StrReplaygainDetail, "REPLAYGAIN  clear" );
				else if( OP_REPLAYGAIN_ALBUM == TypeReplaygain )
					strcpy( StrReplaygainDetail, "REPLAYGAIN  album" );
				else if( OP_REPLAYGAIN_TRACK == TypeReplaygain )
					strcpy( StrReplaygainDetail, "REPLAYGAIN  track" );

				conv_to_convert( PtrTabArgs, REPLAYGAIN, StrReplaygainDetail );
			}
			PtrTabArgs = conv_RemoveTab( PtrTabArgs );
			list_replaygain = replaygain_remove_glist( list_replaygain );
		}
	}
}

