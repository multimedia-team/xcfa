/*
 *  file      : src/tags_m4a.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "get_info.h"
#include "tags.h"
#include <taglib/tag_c.h>


/*
*---------------------------------------------------------------------------
* REMOVE
*---------------------------------------------------------------------------
*/
INFO_M4A *tagsm4a_remove_info (INFO_M4A *info)
{
	if (info) {
		if (NULL != info->time)		{ free (info->time); info->time = NULL;	}
		if (NULL != info->hertz)	{ free (info->hertz);info->hertz = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_M4A *)NULL);
}


void tags_m4a_print( INFO_M4A *ptrinfo )
{
	printf( "time        = %s\n", ptrinfo->time );
	printf( "SecTime     = %d\n", ptrinfo->SecTime );
	printf( "hertz       = %s\n", ptrinfo->hertz );
}


/*
*---------------------------------------------------------------------------
* GET INFO
*---------------------------------------------------------------------------
*/
INFO_M4A *tagsm4a_get_info( INFO *p_info )
{
	char		**Larrbuf = NULL;
	CString		*gstr = NULL;
	int		cpt;
	char		*ptr = NULL;
	INFO_M4A	*ptrinfo = NULL;
	int		m;
	int		s;
	int		sec;

	gstr = GetInfo_faad ( p_info->path_name_file );
	Larrbuf = C_strsplit( gstr->str, "\n" );

	ptrinfo = (INFO_M4A *)malloc (sizeof (INFO_M4A));
	if (ptrinfo == NULL) {
		C_strfreev( Larrbuf );
		C_string_free( gstr );
		return (NULL);
	}
	ptrinfo->tags = (TAGS *)tags_alloc ();

	for (cpt=0; Larrbuf[cpt]; cpt++) {

		// LC AAC	181.287 secs, 2 ch, 44100 Hz
		// LC AAC	7169.049 secs, 2 ch, 44100 Hz
		if ((ptr = strstr (Larrbuf[cpt], "LC AAC"))) {

			// --> 181.287 secs, 2 ch, 44100 Hz
			// --> 7169.049 secs, 2 ch, 44100 Hz
			ptr = strstr (ptr, " secs,");
			ptr --;
			while (*ptr != ' ' && *ptr != '\t') ptr --;
			ptr ++;

			sec = atoi (ptr);
			s = sec % 60; sec /= 60;
			m = sec % 60; sec /= 60;
			if (sec > 0) ptrinfo->time = C_strdup_printf ("%02d:%02d:%02d", sec, m, s);
			else         ptrinfo->time = C_strdup_printf ("00:%02d:%02d", m, s);

			ptrinfo->SecTime = sec;

			/* --> 44100 Hz */
			ptr = strstr (ptr, " Hz");
			ptr --;
			while (*ptr != ' ' && *ptr != '\t') ptr --;
			ptr ++;
			ptrinfo->hertz = C_strdup_printf ("%d", atoi(ptr));
		}
		else if ((ptr = strstr (Larrbuf[cpt], "artist:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Artist = C_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "title:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Title = C_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "album:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Album = C_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "date:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Year = C_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "genre:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Genre = C_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "comment:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Comment = C_strdup (ptr);
			}
		}
	}

	C_strfreev( Larrbuf );
	C_string_free( gstr );

	tags_complementation( ptrinfo->tags,  p_info->path_name_file );

	return ((INFO_M4A *)ptrinfo);
}

