/*
 *  file      : src/cue.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define _(String) gettext (String)
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "global.h"
#include "tags.h"
#include "conv.h"



typedef struct {
	int		num_track;
	char	title[ 256 ];
	int		begin;
	int		end;
	int		length;
} TRACK;
CList	*ListTrack = NULL;


//
//
C_BOOL cue_read_wav_file( char *p_path_name_file )
{
#define BLOCK_SIZE	2352
	typedef struct {
		int		begin;
		int		end;
	} BLANK;
	WAVE	WaveHeader;
	int		ChannelNext;
	int		Ret;
	int		tmp = 0;
	int		points = 0;
	unsigned char	devbuf[ BLOCK_SIZE +10 ];
	int		Indice;
	double	min, max;
	int		point_begin, point_end;
	BLANK	*blank = NULL;
	CList	*ListBlank = NULL;
	CList	*list_b = NULL;
	BLANK	*blank_next = NULL;
	TRACK	*Track = NULL;
	int		num_track = 1;
	double	PercentBegin;
	double	PercentEnd;
	int		TimeSongSec;
	int		max_points;

	if( FALSE == tagswav_read_file( p_path_name_file, &WaveHeader )) {
		printf("Mauvais format de fichier: %s\n", p_path_name_file );
		tagswav_close_file( &WaveHeader );
		return( FALSE );
	}

	switch( WaveHeader.FMT.BitsPerSample ) {
		case 8 :
		case 16 :
		case 24 :
		case 32 :
			break;
		default :
			tagswav_close_file( &WaveHeader );
			printf("Seuls ces formats de bitrates : 8 16 24 et 32 sont pris en charge pour la lecture\n"
					"Format de bitrate [ %d ] non prit en charge\n",
					WaveHeader.FMT.BitsPerSample
				);
		return( FALSE );
	}

	points = 0;
	point_begin = point_end = -1;
	ChannelNext = (WaveHeader.FMT.NumChannels -1) * (WaveHeader.FMT.BitsPerSample / 8);

	// LECTURE DES DATAS
	while( (Ret = fread( devbuf, 1, BLOCK_SIZE, WaveHeader.file )) > 0 ) {

		min = max = 0.0;

		for (Indice = 0; Indice < Ret; Indice++) {

			if (WaveHeader.FMT.BitsPerSample == 8) {

				tmp = devbuf [ Indice ];
				tmp -= 128;
			}
			else if (WaveHeader.FMT.BitsPerSample == 16) {

				tmp = (char)devbuf [ Indice +1 ] << 8 | (char)devbuf [ Indice ];
				Indice ++;
			}
			else if (WaveHeader.FMT.BitsPerSample == 24) {

				tmp  = (char)devbuf [ Indice +0 ] >> 16;
				tmp += (char)devbuf [ Indice +1 ] >> 8;
				tmp += (char)devbuf [ Indice +2 ];

				Indice += 2;
			}
			else if (WaveHeader.FMT.BitsPerSample == 32) {

				tmp  = (char)devbuf [ Indice +0 ] >> 24;
				tmp += (char)devbuf [ Indice +1 ] >> 16;
				tmp += (char)devbuf [ Indice +2 ] >> 8;
				tmp += (char)devbuf [ Indice +3 ];

				Indice += 2;
			}

			if (tmp > max)			max = tmp;
			else if (tmp <= min)	min = tmp;

			// skip over any extra channels
			Indice += ChannelNext;
		}

		// ~-50 dB calculation :
		// ---------------------
		// From @Dzef:
		// 	Sinon, Claude, pour le soucis de not' Christophe-coupeur-d'album-en-rondelles-sans-cue,
		//  la parade existe en partie dans le cas où les plages sont séparées par des "blancs"
		//  (ce qui n'est pas le cas des albums "live" par exemple) : il suffit de détecter le niveau
		// 	audio et en dessous d'un seuil fixé (typiquement < ~-50 dB), hop, 1 plage !
		// PS:
		// 	Et chose fut faite: MERCI Xavier  :-)
		if( min >= -1. && max <= 1. ) {

			if( point_begin == -1 )
					point_begin = point_end = points;
			else	point_end = points;

		} else {
			if( NULL == blank && point_begin != 0 ) {
				blank = (BLANK *)malloc( sizeof(BLANK) );
				blank->begin = 0;
				blank->end   = 2;
				ListBlank = C_list_append( ListBlank, blank );
			}
			// threshold below ~-50 dB
			// seuil en dessous ~-50 dB
			if( point_end - point_begin >= 10 )  {
				blank = (BLANK *)malloc( sizeof(BLANK) );
				blank->begin = point_begin;
				blank->end   = point_end;
				ListBlank = C_list_append( ListBlank, blank );
			}
			point_begin = point_end = -1;
		}
		points ++;
	}
	// The end of file in list
	if( point_end != points ) {
		blank = (BLANK *)malloc( sizeof(BLANK) );
		blank->begin = point_begin != -1 ? point_begin : points;
		blank->end   = points;
		ListBlank = C_list_append( ListBlank, blank );
	}
	// if only one track
	if( 1 == C_list_length( ListBlank )) {
		blank = (BLANK *)malloc( sizeof(BLANK) );
		blank->begin = points;
		blank->end   = points;
		ListBlank = C_list_append( ListBlank, blank );
	}

	TimeSongSec = WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate;
	max_points = points;

	if( NULL != ( list_b = C_list_first( ListBlank ))) {
		while( list_b ) {
			if( NULL != ( blank = (BLANK *)list_b->data )) {
				if( NULL != list_b->next && NULL != (blank_next =(BLANK *)list_b->next->data )) {

					if( 1 == num_track )
							PercentBegin = ((double)blank->begin / (double)max_points) * 100.0;
					else	PercentBegin = ((double)blank->end / (double)max_points) * 100.0;
					PercentEnd   = ((double)(blank_next->end - 5) / (double)max_points) * 100.0;

					Track = (TRACK *)malloc( sizeof(TRACK) );
					Track->num_track = num_track;
					sprintf( Track->title, "title_%d", num_track );
					Track->begin     = (int) (((double)TimeSongSec * (double)PercentBegin) / 100.0);
					Track->end       = (int) (((double)TimeSongSec * (double)PercentEnd) / 100.0);
					Track->length    = Track->end - Track->begin;
					ListTrack = C_list_append( ListTrack, Track );

					num_track ++;
				}
			}
			list_b = C_list_next( list_b );
			Indice ++;
		}
	}

	// Clear list: ListBlank
	if( NULL != (list_b = C_list_first( ListBlank ))) {
		while( list_b ) {
			free( list_b->data );
			list_b->data = NULL;
			list_b = C_list_next( list_b );
		}
		list_b = ListBlank = C_list_free( ListBlank );
	}

	tagswav_close_file( &WaveHeader );

	return( TRUE );
}

//
//
void cue_put_infos_wav_file( char *p_path_name_file )
{
	CList	*list = NULL;
	TRACK	*Track = NULL;
	char	StrTimeBegin[ 10 ];
	char	StrTimeEnd[ 10 ];

	if( NULL != ( list = C_list_first( ListTrack ))) {
		printf("\n" );
		printf( "!-------------------------------------------------------------\n" );
		printf( "! %s\n", p_path_name_file );
		printf( "!-------------------------------------------------------------\n" );
		printf( "! Track  Begin       End         Length    Title\n" );
		printf( "!-------------------------------------------------------------\n" );
		while( list ) {
			if( NULL != ( Track = list->data )) {
				sprintf( StrTimeBegin, "%02d:%02d:%02d",
						(Track->begin / 60) / 60,
						(Track->begin / 60) % 60,
						Track->begin % 60
						);
				sprintf( StrTimeEnd, "%02d:%02d:%02d",
						(Track->end / 60) / 60,
						(Track->end / 60) % 60,
						Track->end % 60
						);

				printf(" [ %2d ]  %s    %s  %5d       %s\n",
						Track->num_track,
						StrTimeBegin,
						StrTimeEnd,
						Track->length,
						Track->title
						);
			}
			list = C_list_next( list );
		}
	}
}


//
// Split file 'char *p_pathnamefile' from 'int p_split_begin_ss' for a length of 'int p_split_length_ss'
//
void cue_split_wav( char *p_from_wav, char *p_to_wav, int p_split_begin_ss, int p_split_length_ss )
{
	WAVE	WaveHeader;
	double	Percent = 0.;
	double	PercentBegin;
	double	PercentEnd;
	char	*buffer = NULL;
	FILE	*fp = NULL;
	size_t	Ret;
	size_t	RetRead;
	size_t	WriteValue;

	printf ( "%s", BOLD_RED );
	printf("		p_from_wav        = %s\n", p_from_wav );
	printf("		p_to_wav          = %s\n", p_to_wav );
	printf("		p_split_begin_ss  = %d\n", p_split_begin_ss );
	printf("		p_split_length_ss = %d\n", p_split_length_ss );
	printf ( "%s", RESET );

	if (FALSE == tagswav_read_file( p_from_wav, &WaveHeader )) {
		tagswav_close_file( &WaveHeader );
		printf ( "%s", BOLD_RED );
		printf("\n!--- S P L I T ----------------\n");
		printf ( "! File not found to split operation: \"%s\"", p_from_wav );
		printf ( "%s\n\n", RESET );
		return;
	}
	if( (p_split_begin_ss + p_split_length_ss) > (WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate) ) {
		printf ( "%s", BOLD_RED );
		printf("\n!--- S P L I T ----------------\n");
		printf ( "! The size of the '--split' and '--length' is greater than the size of the file." );
		printf ( "! %s\n", p_from_wav );
		printf ( "! %d secondes\n", WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate );
		printf ( "%s\n\n", RESET );
		tagswav_close_file( &WaveHeader );
		return;
	}

	PercentBegin = (((double)p_split_begin_ss * (double)WaveHeader.FMT.ByteRate) /  WaveHeader.DATA.Subchunk2Size) * 100.0;
	PercentEnd = (((double)p_split_length_ss * (double)WaveHeader.FMT.ByteRate) /  WaveHeader.DATA.Subchunk2Size) * 100.0;
	PercentEnd += PercentBegin;

	printf("\n!--- S P L I T ---------------------------------------------!\n");
	printf( "! %s\n", p_from_wav );
	printf( "! %s\n", p_to_wav );

	buffer = (char *)malloc ((sizeof(char) * BUFSIZ) + 10);

	fp = fopen( p_to_wav, "w" );
	// ECRITURE ENTETE FICHIER WAV
	fwrite( &WaveHeader.RIFF.ChunkID, 4, 1, fp );		// RIFF
	fwrite( &WaveHeader.RIFF.ChunkSize, 4, 1, fp );		// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
	fwrite( &WaveHeader.RIFF.Format, 4, 1, fp );		// WAVE
	fwrite( &WaveHeader.FMT.Subchunk1ID, 4, 1, fp );	// 'fmt '
	fwrite( &WaveHeader.FMT.Subchunk1Size, 4, 1, fp );	// taille en octet des données à suivre
	fwrite( &WaveHeader.FMT.AudioFormat, 2, 1, fp );	// format de compression (une valeur autre que 1 indique une compression)
	fwrite( &WaveHeader.FMT.NumChannels, 2, 1, fp );	// nombre de canaux
	fwrite( &WaveHeader.FMT.SampleRate, 4, 1, fp );		// fréquence d'échantillonage (nombre d'échantillons par secondes)
	fwrite( &WaveHeader.FMT.ByteRate, 4, 1, fp );		// nombre d'octects par secondes
	fwrite( &WaveHeader.FMT.Blockalign, 2, 1, fp );		// nombre d'octects pour coder un échantillon
	fwrite( &WaveHeader.FMT.BitsPerSample, 2, 1, fp );	// nombre de bits pour coder un échantillon
	fwrite( &WaveHeader.DATA.Subchunk2ID, 4, 1, fp );	// 'data'
	fwrite( &WaveHeader.DATA.Subchunk2Size, 4, 1, fp );	//  taille des données audio( nombre total d'octets codant les données audio)

	RetRead = 0;
	WriteValue = 0;
	while(( Ret = fread (buffer,  1, BUFSIZ, WaveHeader.file )) > 0) {
		RetRead += Ret;
		Percent = (float)(RetRead / (float)WaveHeader.DATA.Subchunk2Size) * 100.0;
		if( Percent >= PercentBegin ) {
			fwrite( buffer, Ret, 1, fp );
			WriteValue += Ret;
	 	}
		if( Percent > PercentEnd ) break;
	}
	/*
	fseek( fp, 4L, SEEK_SET );
	WriteValue += 44;
	WriteValue -= 8;
	fwrite( &WriteValue, 4, 1, fp );	// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
	fseek( fp, 40L, SEEK_SET );
	WriteValue += 8;
	WriteValue -= 44;
	fwrite( &WriteValue, 4, 1, fp );	//  taille des données audio( nombre total d'octets codant les données audio )
	*/
	fseek( fp, 40L, SEEK_SET );
	fwrite( &WriteValue, 4, 1, fp );	//  taille des données audio( nombre total d'octets codant les données audio )
	fseek( fp, 4L, SEEK_SET );
	WriteValue += 44;
	WriteValue -= 8;
	fwrite( &WriteValue, 4, 1, fp );	// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent

	tagswav_close_file( &WaveHeader );
	fclose (fp );
	fp = NULL;

	free( buffer );
	buffer = NULL;
}
void cue_extract_wav_file( char *p_OutputDir, char *p_path_name_file )
{
	CList	*list = NULL;
	TRACK	*Track = NULL;
	char	*to_wav = NULL;

	if( NULL != ( list = C_list_first( ListTrack ))) {

		// MAKE DIRECTORY DESTINATION IF NOT EXIST
		if( FALSE == C_dir_test( p_OutputDir ))
			C_mkdir_with_parents( p_OutputDir );

		while( list ) {
			if( NULL != ( Track = list->data )) {
				to_wav = C_strdup_printf( "%s%s.wav", p_OutputDir, Track->title );
				cue_split_wav( p_path_name_file, to_wav, Track->begin, Track->length );
				free( to_wav );
				to_wav = NULL;
			}
			list = C_list_next( list );
		}
	}
}

//
//
float cue_get_time_in_sec( char *p_str )
{
	char		*Ptr = NULL;
	uint		 Min, Sec, Hundr;
	float		 SecondesTempsActuel = 0;

	// MINUTES
	if ((Ptr = strchr (p_str, ':')) != NULL) {
		while (*Ptr != ' ') Ptr --;
		if (*Ptr == ' ') Ptr ++;
		Min = atoi (Ptr);

		// SECONDES
		if ((Ptr = strchr (p_str, ':')) != NULL) {
			Ptr ++;
			Sec = atoi (Ptr);

			// CENTIEMES
			if ((Ptr = strrchr (p_str, ':')) != NULL) {
				Ptr ++;
				Hundr = atoi (Ptr);

				// Temps Total en secondes
				SecondesTempsActuel = ((float)Min * 60.0) + (float)Sec + ((float)Hundr / 100.0);
			}
		}
	}

	return (SecondesTempsActuel);
}

//
//
C_BOOL cue_read_cue_file( char *p_path_name_file_cue, char **p_path_name_file_wav )
{
	FILE	*fp;
#define MAX_CARS_SPLIT_CUE_FILE 255
	char	buf   [ MAX_CARS_SPLIT_CUE_FILE + 4 ];
	char	*StrNameSong = NULL;
	char	*PathNameSong = NULL;
	char	*Ptr = NULL;
	char	*Path = NULL;
	WAVE	WaveHeader;
	int		TimeSongSec;

	// Cherche le nom de fichier WAV associé au fichier CUE pour: PathNameSong
	fp = fopen( p_path_name_file_cue, "r" );
	while( NULL != fgets( buf, MAX_CARS_SPLIT_CUE_FILE, fp )) {

		// Si debut de ligne ok
		if( NULL != ( Ptr = strstr( buf, "FILE \"" ))) {

			// StrNameSong contiendra le nom du fichier delimite par les signes "
			Ptr = strchr( buf, '"' );
			Ptr ++;
			StrNameSong = C_strdup( Ptr );
			if( NULL == ( Ptr = strrchr( StrNameSong, '"' ))) {
				fclose( fp );
				free( StrNameSong );
				StrNameSong = NULL;
				return( FALSE );
			}
			*Ptr = '\0';

			// Path contiendra le chemin
			Path = C_strdup( p_path_name_file_cue );
			if( NULL != ( Ptr = strrchr( Path, '/' ))) {
				*Ptr = '\0';
			}

			// PathNameSong contiendra: chemin + nom du fichier musical
			PathNameSong = C_strdup_printf( "%s/%s", Path, StrNameSong );
			free( Path );
			Path = NULL;
			free( StrNameSong );
			StrNameSong = NULL;
			break;
		}
	}
	fclose( fp );

	if( FALSE == C_file_test( PathNameSong )) {
		printf("[ %s ] n'existe pas dans le dossier contenant [ %s ] ! \n", PathNameSong, p_path_name_file_cue );
		free( PathNameSong );
		PathNameSong = NULL;
		return( FALSE );
	}

	tagswav_read_file( PathNameSong, &WaveHeader );
	TimeSongSec = WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate;
	tagswav_close_file( &WaveHeader );

	*p_path_name_file_wav = PathNameSong;

	// LECTURE DES INFOS DU FICHIER
	//
	if( NULL != ( fp = fopen( p_path_name_file_cue, "r" )) ) {

		int		num_track = -1;
		int		index_begin = -1;
		int		index_end = -1;
		char	title[ 100 ];
		TRACK	*Track = NULL;

		while( NULL != ( fgets( buf, MAX_CARS_SPLIT_CUE_FILE, fp ))) {

			if( NULL != ( Ptr = strrchr( buf, '\n' ))) *Ptr = '\0';

			// TRACK
			if( NULL != ( Ptr = strstr( buf, "TRACK " ))) {
				num_track = atoi( Ptr +6 );
			}

			// TITRE
			else if( strstr( buf, "TITLE " )) {
				if( num_track > -1 ) {
					if( NULL != ( Ptr = strchr( buf, '"' ))) {
						Ptr ++;
						strcpy( title, Ptr );
						if( NULL != ( Ptr = strrchr( title, '"' ))) {
							*Ptr = '\0';
						}
					}
				}
			}

			// INDEX DE DEBUT
			else if( strstr( buf, "INDEX 01" )) {
				if( num_track > 1 ) {
					if( index_end == -1 )
						index_end = (int)cue_get_time_in_sec( buf );

					Track->end       = index_end;
					Track->length    = Track->end - Track->begin;
				}

				Track = (TRACK *)malloc( sizeof(TRACK) );
				Track->num_track = num_track;
				strcpy( Track->title, title );
				Track->begin     = index_begin;
				Track->end       = index_end;
				Track->length    = Track->end - Track->begin;
				ListTrack = C_list_append( ListTrack, Track );

				index_begin = index_end = -1;
 				index_begin = (int)cue_get_time_in_sec( buf );

				Track->begin     = index_begin;
			}

			// INDEX DE FIN
			else if( index_begin != -1 && strstr( buf, "INDEX 00" )) {
				index_end = (int)cue_get_time_in_sec( buf );
			}
		}
		fclose( fp );

		if( index_end == -1 ) {
			Track->end       = TimeSongSec;
		}
	}
	return( TRUE );
}

//
//
void cue_set( OP_CUE p_op_cue )
{
	CList	*list = NULL;
	CList	*list_b = NULL;
	INFO	*Info = NULL;
	char	*OutputDir = NULL;
	char	*path_name_file_wav = NULL;

	if( NULL != ( list = C_list_first( Detail.ListFile ))) {

		while( list ) {
			if( NULL != (Info = (INFO *)list->data) ) {

				OutputDir = utils_get_dir_dest( Info );

				if( p_op_cue == OP_CUE_INFO ) {
					if( FILE_IS_WAV == Info->type_infosong_file_is ) {
						if( TRUE == cue_read_wav_file( Info->path_name_file ))
							cue_put_infos_wav_file( Info->path_name_file  );
					}
					else if( FILE_IS_CUE == Info->type_infosong_file_is ) {
						if( TRUE == cue_read_cue_file( Info->path_name_file, &path_name_file_wav ))
							cue_put_infos_wav_file( Info->path_name_file  );
					}
				}
				else if( p_op_cue == OP_CUE_EXTRACT ) {
					if( FILE_IS_WAV == Info->type_infosong_file_is ) {
						if( TRUE == cue_read_wav_file( Info->path_name_file ))
							cue_extract_wav_file( OutputDir, Info->path_name_file );
					}
					else if( FILE_IS_CUE == Info->type_infosong_file_is ) {
						if( TRUE == cue_read_cue_file( Info->path_name_file, &path_name_file_wav ))
							cue_extract_wav_file( OutputDir, path_name_file_wav );
					}
				}

				if( NULL != OutputDir ) {
					free( OutputDir );
					OutputDir = NULL;
				}
			}

			// Clear list: ListTrack
			if( NULL != (list_b = C_list_first( ListTrack ))) {
				while( list_b ) {
					free( list_b->data );
					list_b->data = NULL;
					list_b = C_list_next( list_b );
				}
				list_b = ListTrack = C_list_free( ListTrack );
			}

			list = C_list_next( list );
		}
	}
}






