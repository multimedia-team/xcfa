/*
 *  file      : src/global.h
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifndef global_h
#define global_h 1

#include "clib.h"

typedef struct {
	char		Machine [ 100 ];
	int			NbCpu;
	int			TypeCpu;
	C_BOOL		BoolCpuIs64Bits;
} HOST_CONF;

extern HOST_CONF HostConf;

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define gettext_noop(x) (x)
	#define _(String) gettext (String)
#endif


// Terminal color codes:
//		printf( "\t%sNew text to see%s\n", BOLD_YELLOW, RESET );
#define RESET			"\033[0m"			// RESET
#define BLACK			"\033[30m"			// Black
#define RED				"\033[31m"			// Red
#define GREEN			"\033[32m"			// Green
#define YELLOW			"\033[33m"			// Yellow
#define BLUE			"\033[34m"			// Blue
#define MAGENTA			"\033[35m"			// Magenta
#define CYAN			"\033[36m"			// Cyan
#define WHITE			"\033[37m"			// White
#define BOLD_BLACK		"\033[1m\033[30m"	// Bold Black
#define BOLD_RED		"\033[1m\033[31m"	// Bold Red
#define BOLD_GREEN		"\033[1m\033[32m"	// Bold Green
#define BOLD_YELLOW		"\033[1m\033[33m"	// Bold Yellow
#define BOLD_BLUE		"\033[1m\033[34m"	// Bold Blue
#define BOLD_MAGENTA	"\033[1m\033[35m"	// Bold Magenta
#define BOLD_CYAN		"\033[1m\033[36m"	// Bold Cyan
#define BOLD_WHITE		"\033[1m\033[37m"	// Bold White


//
// ---------------------------------------------------------------------------
//  DEBUG
// ---------------------------------------------------------------------------
//
#define PRINT_FUNC_LF() printf( "%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__ )
#define PRINT(str) printf( "%s :: %s(line = %d)\n\t%s\n", __FILE__, __FUNCTION__, __LINE__, str )

typedef struct {
	char			*Album;					// %b
	char			*Artist;				// %a
	char			*Title;					// %d
	char			*Number;				// %c
	int				 IntNumber;				//
	char			*Genre;					// %g
	int			 	IntGenre;				//
	char			*Year;					// %e
	int				 IntYear;				//
	char			*Comment;				//
	char			*Description;			//
} TAGS;

//
// ---------------------------------------------------------------------------
//  I N F O _
// ---------------------------------------------------------------------------
//

typedef struct {
	char			*time;					// Temps d'ecoute
	int			 	SecTime;				// Temps d'ecoute en secondes
	char			*hertz;					//
	TAGS			*tags;					// Tags
} INFO_M4A;

typedef struct {
	char			*time;					// Temps d'ecoute
	int				 SecTime;				// Temps d'ecoute en secondes
	char			*hertz;					//
	TAGS			*tags;					// Tags
} INFO_AAC;

typedef struct {
	C_BOOL		 	BoolBwf;				// 'fmt ', 'bext', 'qlty', 'levl', 'link','axml'
	char			*time;					// Temps d'ecoute
	int			 	SecTime;				// Temps d'ecoute en secondes
	char			*hertz;					// Taux d'échantillonnage en hertz( qualité du son)
	char			*voie;					// 1, 2, 4, 6
	char			*bits;					// 8, 16, ,24, 32 ou 64
	TAGS			*tags;					// Tags
	int				 LevelDbfs;				//
} INFO_WAV;

typedef enum {								// type:     bitrate:
	NONE_MPEG = -1,
	MPEG_1 = 0,								// MPV_1     [ 32 40 48 56 64 80 96 112 128 160 192 224 256 320 ]
	MPEG_2,									// MPV_2     [  8 16 24 32 40 48 56  64  80  96 112 128 144 160 ]
	MPEG_25									// MPV_25    [  8 16 24 32 40 48 56  64  80  96 112 128 144 160 ]
} MPEG_IS;

typedef struct {
	char			*bitrate;				//
	char			*time;					// Temps d'ecoute
	int			 	SecTime;				// Temps d'ecoute en secondes
	MPEG_IS			 mpeg_is;				//
	char			*size;					//
	TAGS			*tags;					// Tags
	int				LevelDbfs;				//
} INFO_MP3;

typedef struct {
	char			*Channels;				//
	char			*Rate;					//
	char			*Nominal_bitrate;		//
	char			*time;					// Temps d'ecoute
	int				SecTime;				// Temps d'ecoute en secondes
	char			*size;					//
	TAGS			*tags;					// Tags
	int			 	LevelDbfs;				//
} INFO_OGG;

typedef struct {
	char			*time;					// Temps d'ecoute
	int			 	SecTime;				// Temps d'ecoute en secondes
	TAGS			*tags;					// Tags
} INFO_FLAC;

typedef struct {
	TAGS			*tags;					// Tags
} INFO_WMA;

typedef struct {
	TAGS			*tags;					// Tags
} INFO_RM;

typedef struct {
	TAGS			*tags;					// Tags
} INFO_DTS;

typedef struct {
	TAGS			*tags;					// Tags
} INFO_AIFF;

typedef struct {
	char			*namefile;				// Nom complet du fichier
	TAGS			*tags;					// Tags
} INFO_MPC;

typedef struct {
	TAGS			*tags;					// Tags
	char			*time;					// Temps d'ecoute
	int			 	SecTime;				// Temps d'ecoute en secondes
	char			*size;					//
} INFO_SHN;

typedef struct {
	TAGS			*tags;					// Tags
	char			*time;					// Temps d'ecoute
	int			 	SecTime;				// Temps d'ecoute en secondes
	char			*size;					//
} INFO_APE;

typedef struct {
	TAGS			*tags;					// Tags
	char			*time;					// Temps d'ecoute
	int			 	SecTime;				// Temps d'ecoute en secondes
	char			*size;					//
} INFO_WAVPACK;

typedef struct {
	TAGS			*tags;					// Tags
} INFO_AC3;


typedef enum {
	FILE_IS_NONE = 0,
	FILE_IS_FLAC,
	FILE_IS_WAV,
	FILE_IS_CUE,
	// FILE_IS_BFW,	// BROADCAST WAVE FORMAT
	FILE_IS_MP3,
	FILE_IS_OGG,
	/* Les fichiers au format AAC, portant l'extension
		.mp4( pour MPEG-4),
	   	.m4a( pour MPEG-4 Audio)
	   	.m4p( pour MPEG-4 Protégé),
	   sont globalement plus petits que les fichiers au format MP3. */
	FILE_IS_M4A,
	FILE_IS_VID_M4A,
	FILE_IS_AAC,
	FILE_IS_SHN,
	FILE_IS_WMA,
	FILE_IS_RM,
	FILE_IS_DTS,
	FILE_IS_AIFF,
	FILE_IS_MPC,
	FILE_IS_APE,
	FILE_IS_WAVPACK,
	FILE_IS_WAVPACK_MD5,
	FILE_IS_AC3,
	NBR_FILE_TO
} TYPE_FILE_IS;

typedef struct {
	char			*path_name_file;
	char			*path;
	char			*name_file;
	TYPE_FILE_IS	type_infosong_file_is;
	void			*info;					// INFO_xx
	char			*path_is_normalize;
	char			*tmp_wav;				// pathnames temporaires
	char			*tmp_mp3;
	char			*tmp_ogg;
	char			*tmp_sox;
	char			*tmp_sox_24;
	char			*tmp_flac;
	char			*tmp_ape;
	char			*tmp_wavpack;
	char			*tmp_m4a;
	char			*tmp_aac;
	char			*tmp_mpc;
} INFO;


typedef struct {							// Liste de recherche recursive si plusieurs fois --input sur ligne de commande
	char		*PathNameFile;
	char		*Jocker;
} LIST_SEARCH;

typedef enum {
	OP_REPLAYGAIN_NONE = 0,
	OP_REPLAYGAIN_CLEAR,
	OP_REPLAYGAIN_ALBUM,
	OP_REPLAYGAIN_TRACK
} OP_REPLAYGAIN;

typedef enum {
	OP_NORMALISE_NONE = 0,
	OP_NORMALISE_PEAK_ALBUM,					// NORM_PEAK_ALBUM
	OP_NORMALISE_PEAK,							// NORM_PEAK
	OP_NORMALISE_RMS_MIX_ALBUM,					// ( scan) NORM_RMS_MIX_ALBUM
	OP_NORMALISE_RMS_FIX						// ( scan) NORM_RMS_FIX
} OP_NORMALISE;

// info | extract
typedef enum {
	OP_CUE_NONE = 0,
	OP_CUE_INFO,
	OP_CUE_EXTRACT
} OP_CUE;

typedef struct {
	int				BoolVerboseMode;
	int				Nice;
	CList			*ListSearch;				// struct LIST_SEARCH
	char			*OutputDir;
	C_BOOL			BoolTypeDest[ NBR_FILE_TO ];
	C_BOOL			BoolRecursiveMode;
	CList			*ListFile;					// struct  INFO
	char			*Op_flac;					// Options tags
	char			*Op_ape;
	char			*Op_wavpack;
	char			*Op_ogg;
	char			*Op_m4a;
	char			*Op_aac;
	char			*Op_mpc;
	char			*Op_mp3;
	C_BOOL			BoolExtract2Src;
	C_BOOL			BoolNoTag;					// tags
	char			*tag_album;
	char			*tag_artist;
	char			*tag_title;
	char			*tag_number;
	char			*tag_genre;
	char			*tag_year;
	char			*tag_comment;
	char			*tag_description;
	double			split;
	int				split_length;
	int				frequency;					// Setting wav
	int				track;
	int				quantification;
	OP_REPLAYGAIN	replaygain;
	int				bool_peak;					// Normalize
	int				bool_peak_album;
	int				mix_rms_album;
	int				fix_rms;
	C_BOOL			bool_dest;					// --dest
	C_BOOL			bool_split;					// --split --length
	C_BOOL			bool_setting_wav;			// --frequency --track --quantification
	C_BOOL			bool_normalize;				// --peak  --peak_album  --mix_rms_album  --fix_rms

	int				bool_info_files;			//
	int				bool_info_tags;				//
	int				bool_info_head;				//

	OP_CUE			cue;

} DETAIL;

extern DETAIL Detail;


//
// ---------------------------------------------------------------------------
//  FILE_CONV.C
// ---------------------------------------------------------------------------
//
void file_conv( void );
C_BOOL fileconv_change_quantification_with_sox(
						C_BOOL pbool_copy_dest_to_src,
						char *p_src,
						char *p_dest,
						int p_Hertz,
						int p_Channels,
						int p_Bits
						);

//
// ---------------------------------------------------------------------------
//  UTILS.C
// ---------------------------------------------------------------------------
//
size_t	utils_get_size_file( char *PathName );
char *utils_create_rep( char *path_name_rep );
char *utils_remove_temporary_rep( char *path_tmprep );
char *utils_get_dir_dest( INFO *p_Info );

//
// ---------------------------------------------------------------------------
//  SPLIT.C
// ---------------------------------------------------------------------------
//
void split_wav( char *p_pathnamefile, double p_split_begin_ss, int p_split_length_ss );

//
// ---------------------------------------------------------------------------
//  WAV.C
// ---------------------------------------------------------------------------
//
void wav_change_setting( char *p_pathnamefile, int p_frequency, int p_track, int p_quantification );
void loop_wav_change_setting( int p_frequency, int p_track, int p_quantification );

//
// ---------------------------------------------------------------------------
//  REPLAYGAIN.C
// ---------------------------------------------------------------------------
//
void replaygain_set( OP_REPLAYGAIN TypeReplaygain );

//
// ---------------------------------------------------------------------------
//  NORMALIZE.C
// ---------------------------------------------------------------------------
//
void normalize_set( C_BOOL p_bool_peak, C_BOOL p_bool_peak_album, int p_mix_rms_album, int p_fix_rms );

//
// ---------------------------------------------------------------------------
//  CUE.C
// ---------------------------------------------------------------------------
//
void cue_set( OP_CUE p_op_cue );


#endif
