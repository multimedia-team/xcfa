/*
 *  file      : src/normalize.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
// #include <fcntl.h>	// PROCESSUS NON-BLOQUANT

#include "global.h"
#include "conv.h"


// p_type_infosong_file_is = FILE_IS_MP3 | FILE_IS_WAV | FILE_IS_OGG
//
CList *normalize_get_list( TYPE_FILE_IS p_type_infosong_file_is )
{
	CList	*list = NULL;
	INFO	*Info = NULL;
	CList	*list_normalize = NULL;

	if( NULL != ( list = C_list_first( Detail.ListFile ))) {
		while( list ) {
			if( NULL != (Info = (INFO *)list->data) ) {
				if( Info->type_infosong_file_is == p_type_infosong_file_is )
					list_normalize = C_list_append( list_normalize, Info->path_is_normalize ? Info->path_is_normalize : Info->path_name_file );
			}
			list = C_list_next( list );
		}
	}
	return( list_normalize );
}
CList *normalize_remove_glist( CList *p_list_normalize )
{
	CList *list = NULL;

	if( p_list_normalize != NULL ) {
		list = C_list_first( p_list_normalize );
		while( list ) {
			list->data = NULL;
			list = C_list_next( list );
		}
		C_list_free( p_list_normalize );
		p_list_normalize = NULL;
	}
	return( (CList *)NULL );
}

/*
! Normalize: static modification for next files: MP3, WAV, OGG
! See an excellent article by @Dzef on standardization: http://ubunteros.tuxfamily.org/spip.php?article159
!
! --peak                    Action on a single file.
!                           Increase the overall level of the signal so as to bring the level to 0 dBFS peak without changing dynamics.
! --peak_album              Action on a group of files.
!                           If the maximum level of one or more files is already at 0 dBFS, the level of all the selected files remain unchanged
!                           after normalization. That is why this mode can be safely used almost systematically.
! --mix_rms_album <dBFS>    Action on a group of files.
!                           The selecting a value for a file modifies the other files in the group.
! --fix_rms <dBFS>          Action on a single file.
!                           The selecting a value for a file.
*/
void normalize_set( C_BOOL p_bool_peak, C_BOOL p_bool_peak_album, int p_mix_rms_album, int p_fix_rms )
{
	int		pos;
	char	**PtrTabArgs = NULL;
	CList	*list_normalize = NULL;
	CList	*list = NULL;
	INFO	*Info = NULL;
	int	tab_file [ 3 ] = { FILE_IS_MP3, FILE_IS_WAV, FILE_IS_OGG };
	int	cpt;

	if( TRUE == p_bool_peak ) {
		if( NULL != ( list = C_list_first( Detail.ListFile ))) {
			while( list ) {
				if( NULL != (Info = (INFO *)list->data) ) {
					if( Info->type_infosong_file_is == FILE_IS_MP3 ||
						Info->type_infosong_file_is == FILE_IS_WAV ||
						Info->type_infosong_file_is == FILE_IS_OGG ) {

						PtrTabArgs = conv_AllocTabArgs( &pos );

						if( Info->type_infosong_file_is == FILE_IS_MP3 )
							PtrTabArgs [ pos++ ] = C_strdup( "normalize-mp3" );
						else if( Info->type_infosong_file_is == FILE_IS_WAV )
							PtrTabArgs [ pos++ ] = C_strdup( "normalize" );
						else if( Info->type_infosong_file_is == FILE_IS_OGG )
							PtrTabArgs [ pos++ ] = C_strdup( "normalize-ogg" );

						PtrTabArgs [ pos++ ] = C_strdup( "--peak" );
						PtrTabArgs [ pos++ ] = C_strdup( Info->path_is_normalize ? Info->path_is_normalize : Info->path_name_file );
						PtrTabArgs [ pos++ ] = C_strdup( "--" );
						PtrTabArgs [ pos++ ] = NULL;
						conv_to_convert( PtrTabArgs, NORMALISE_EXEC, "NORMALISE_EXEC  peak" );
						PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
				}
				list = C_list_next( list );
			}
		}
	}
	else if( TRUE == p_bool_peak_album ) {
		for( cpt = 0; cpt < 3; cpt ++ ) {
			if( NULL != (list_normalize = normalize_get_list( tab_file [ cpt ] ))) {

				PtrTabArgs = conv_AllocTabArgs( &pos );

				if( FILE_IS_MP3 == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize-mp3" );
				else if( FILE_IS_WAV == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize" );
				else if( FILE_IS_OGG == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize-ogg" );

				PtrTabArgs [ pos++ ] = C_strdup( "-n" );
				list = C_list_first( list_normalize );
				while( list ) {
					if( NULL != list->data )
						PtrTabArgs [ pos++ ] = C_strdup( list->data );
					list = C_list_next( list );
				}
				PtrTabArgs [ pos++ ] = NULL;
				// GET LEVEL to get value 'conv.value_PEAK_RMS_GROUP_ARGS'
				conv_to_convert( PtrTabArgs, NORMALISE_GET_LEVEL, "NORMALISE_GET_LEVEL" );
				PtrTabArgs = conv_RemoveTab( PtrTabArgs );

				// EXEC

				PtrTabArgs = conv_AllocTabArgs( &pos );

				if( FILE_IS_MP3 == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize-mp3" );
				else if( FILE_IS_WAV == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize" );
				else if( FILE_IS_OGG == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize-ogg" );

				PtrTabArgs [ pos++ ] = C_strdup_printf( "%d,0000dB", (int)conv.value_PEAK_RMS_GROUP_ARGS );
				list = C_list_first( list_normalize );
				while( list ) {
					if( NULL != list->data )
						PtrTabArgs [ pos++ ] = C_strdup( list->data );
					list = C_list_next( list );
				}
				PtrTabArgs [ pos++ ] = NULL;
				conv_to_convert( PtrTabArgs, NORMALISE_EXEC, "NORMALISE_EXEC peak_albu" );
				PtrTabArgs = conv_RemoveTab( PtrTabArgs );
				list_normalize = normalize_remove_glist( list_normalize );
			}
		}
	}
	else if( p_mix_rms_album < 0 ) {
		for( cpt = 0; cpt < 3; cpt ++ ) {
			if( NULL != (list_normalize = normalize_get_list( tab_file [ cpt ] ))) {

				PtrTabArgs = conv_AllocTabArgs( &pos );

				if( FILE_IS_MP3 == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize-mp3" );
				else if( FILE_IS_WAV == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize" );
				else if( FILE_IS_OGG == tab_file [ cpt ] )
					PtrTabArgs [ pos++ ] = C_strdup( "normalize-ogg" );

				PtrTabArgs [ pos++ ] = C_strdup( "-a" );
				PtrTabArgs [ pos++ ] = C_strdup_printf( "%d,0000dB", p_mix_rms_album );
				PtrTabArgs [ pos++ ] = C_strdup( "--batch" );
				list = C_list_first( list_normalize );
				while( list ) {
					if( NULL != list->data )
						PtrTabArgs [ pos++ ] = C_strdup( list->data );
					list = C_list_next( list );
				}
				PtrTabArgs [ pos++ ] = NULL;
				conv_to_convert( PtrTabArgs, NORMALISE_EXEC, "NORMALISE_EXEC mix_rms_album" );
				PtrTabArgs = conv_RemoveTab( PtrTabArgs );
				list_normalize = normalize_remove_glist( list_normalize );
			}
		}
	}
	else if( p_fix_rms < 0 ) {
		if( NULL != ( list = C_list_first( Detail.ListFile ))) {
			while( list ) {
				if( NULL != (Info = (INFO *)list->data) ) {
					if( Info->type_infosong_file_is == FILE_IS_MP3 ||
						Info->type_infosong_file_is == FILE_IS_WAV ||
						Info->type_infosong_file_is == FILE_IS_OGG ) {

						PtrTabArgs = conv_AllocTabArgs( &pos );

						if( Info->type_infosong_file_is == FILE_IS_MP3 )
							PtrTabArgs [ pos++ ] = C_strdup( "normalize-mp3" );
						else if( Info->type_infosong_file_is == FILE_IS_WAV )
							PtrTabArgs [ pos++ ] = C_strdup( "normalize" );
						else if( Info->type_infosong_file_is == FILE_IS_OGG )
							PtrTabArgs [ pos++ ] = C_strdup( "normalize-ogg" );

						PtrTabArgs [ pos++ ] = C_strdup( "-a" );
						PtrTabArgs [ pos++ ] = C_strdup_printf( "%d,0000dB", p_fix_rms );
						PtrTabArgs [ pos++ ] = C_strdup( Info->path_is_normalize ? Info->path_is_normalize : Info->path_name_file );
						PtrTabArgs [ pos++ ] = NULL;
						conv_to_convert( PtrTabArgs, NORMALISE_EXEC, "NORMALISE_EXEC fix_rms" );
						PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
				}
				list = C_list_next( list );
			}
		}
	}
}








