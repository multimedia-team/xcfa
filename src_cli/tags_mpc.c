/*
 *  file      : src/tags_mpc.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <taglib/tag_c.h>

#include "global.h"
#include "tags.h"



/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

typedef struct {
	unsigned is_4d : 8;
	unsigned is_50 : 8;
	unsigned is_2b : 8;
	unsigned is_StreamMajorVersion : 4;
	unsigned is_StreamMinorVersion : 4;
} MPC_BIT_0;

typedef struct {
	unsigned is_FrameCount : 32;
} MPC_BIT_1;

typedef struct {
	unsigned is_MaxLevel : 16;
	unsigned is_SampleFreq : 2;
	unsigned is_Link : 2;
	unsigned is_Profile : 4;
	unsigned is_MaxBand : 6;
	unsigned is_MidSideStereo : 1;
	unsigned is_IntensityStereo : 1;
} MPC_BIT_2;

typedef struct {
	unsigned is_TitlePeak : 16;
	unsigned is_TitleGain : 16;
} MPC_BIT_3;

typedef struct {
	unsigned is_AlbumPeak : 16;
	unsigned is_AlbumGain : 16;
} MPC_BIT_4;

typedef struct {
	unsigned is_unusued : 19;
	unsigned is_Safely : 1;
	unsigned is_LastFrameLength : 11;
	unsigned is_TrueGapless : 1;
} MPC_BIT_5;

typedef struct {
	unsigned is_unusued : 24;
	unsigned is_EncoderVersion : 8;
} MPC_BIT_6;

typedef struct {
	MPC_BIT_0 bit_0;
	MPC_BIT_1 bit_1;
	MPC_BIT_2 bit_2;
	MPC_BIT_3 bit_3;
	MPC_BIT_4 bit_4;
	MPC_BIT_5 bit_5;
	MPC_BIT_6 bit_6;
} MPC_HEADER;


/*
*---------------------------------------------------------------------------
* FILE IS MPC ?
*---------------------------------------------------------------------------
*/

void tagsmpc_print_header( char *mpc_file )
{
	FILE	*fp = NULL;
	MPC_HEADER	fm;

	if( NULL == ( fp = fopen( mpc_file, "rb" ))) return;
	fread( &fm, sizeof(MPC_HEADER), 1, fp );
	fclose( fp );

	// printf ("\n");
	// printf ("%s\n", mpc_file);
	// printf ("\n");
	printf ("\t-- BIT 0 -----------------------------------------\n");
	printf ("\tis_4d                  %4d  -  0x%x\n", fm.bit_0.is_4d, fm.bit_0.is_4d);
	printf ("\tis_50                  %4d  -  0x%x\n", fm.bit_0.is_50, fm.bit_0.is_50);
	printf ("\tis_2b                  %4d  -  0x%x\n", fm.bit_0.is_2b, fm.bit_0.is_2b);
	printf ("\tis_StreamMajorVersion  %4d  -  0x%x\n", fm.bit_0.is_StreamMajorVersion, fm.bit_0.is_StreamMajorVersion);
	printf ("\tis_StreamMinorVersion  %4d  -  0x%x\n", fm.bit_0.is_StreamMinorVersion, fm.bit_0.is_StreamMinorVersion);
	printf ("\t-- BIT 1 -----------------------------------------\n");
	printf ("\tis_FrameCount          %4d  -  0x%x\n", fm.bit_1.is_FrameCount, fm.bit_1.is_FrameCount);
	printf ("\t-- BIT 2 -----------------------------------------\n");
	printf ("\tis_MaxLevel            %4d  -  0x%x\n", fm.bit_2.is_MaxLevel, fm.bit_2.is_MaxLevel);
	printf ("\tis_SampleFreq          %4d  -  0x%x\n", fm.bit_2.is_SampleFreq, fm.bit_2.is_SampleFreq);
	switch(fm.bit_2.is_SampleFreq) {
	case 0 : printf ("\t\t44100 Hz\n"); break;
	case 1 : printf ("\t\t48000 Hz\n"); break;
	case 2 : printf ("\t\t37800 Hz\n"); break;
	case 3 : printf ("\t\t32000 Hz\n"); break;
	}
	printf ("\tis_Link                %4d  -  0x%x\n", fm.bit_2.is_Link, fm.bit_2.is_Link);
	switch(fm.bit_2.is_Link) {
	case 0 : printf ("\t\tTitle start or ends with a very low level\n"); break;
	case 1 : printf ("\t\tTitle ends loudly\n"); break;
	case 2 : printf ("\t\tTitle start loudly\n"); break;
	case 3 : printf ("\t\tTitle start loudly and ends loudly\n"); break;
	}
	printf ("\tis_Profile             %4d  -  0x%x\n", fm.bit_2.is_Profile, fm.bit_2.is_Profile);
	switch (fm.bit_2.is_Profile) {
	case 0  : printf ("\t\tno profile\n"); break;
	case 1  : printf ("\t\tUnstable/Experimental\n"); break;
	case 2  : printf ("\t\tunused\n"); break;
	case 3  : printf ("\t\tunused\n"); break;
	case 4  : printf ("\t\tunused\n"); break;
	case 5  : printf ("\t\tbelow telephone : q=0.0\n"); break;
	case 6  : printf ("\t\tbelow telephone : q=1.0\n"); break;
	case 7  : printf ("\t\ttelephone : q=2.0\n"); break;
	case 8  : printf ("\t\tThumb : q=3.0\n"); break;
	case 9  : printf ("\t\tRadio : q=4.0\n"); break;
	case 10 : printf ("\t\tStandard : q=5.0\n"); break;
	case 11 : printf ("\t\tXtreme : q=6.0\n"); break;
	case 12 : printf ("\t\tInsane : q=7.0\n"); break;
	case 13 : printf ("\t\tBrainDead : q=8.0\n"); break;
	case 14 : printf ("\t\tabove BrainDead : q=9.0\n"); break;
	case 15 : printf ("\t\tabove BrainDead : q=10.0\n"); break;
	}
	printf ("\tis_MaxBand             %4d  -  0x%x\n", fm.bit_2.is_MaxBand, fm.bit_2.is_MaxBand);
	printf ("\tis_MidSideStereo       %4d  -  0x%x\n", fm.bit_2.is_MidSideStereo, fm.bit_2.is_MidSideStereo);
	printf ("\tis_IntensityStereo     %4d  -  0x%x\n", fm.bit_2.is_IntensityStereo, fm.bit_2.is_IntensityStereo);
	printf ("\t-- BIT 3 -----------------------------------------\n");
	printf ("\tis_TitlePeak           %4d  -  0x%x\n", fm.bit_3.is_TitlePeak, fm.bit_3.is_TitlePeak);
	printf ("\tis_TitleGain           %4d  -  0x%x\n", fm.bit_3.is_TitleGain, fm.bit_3.is_TitleGain);
	printf ("\t-- BIT 4 -----------------------------------------\n");
	printf ("\tis_AlbumPeak           %4d  -  0x%x\n", fm.bit_4.is_AlbumPeak, fm.bit_4.is_AlbumPeak);
	printf ("\tis_AlbumGain           %4d  -  0x%x\n", fm.bit_4.is_AlbumGain, fm.bit_4.is_AlbumGain);
	printf ("\t-- BIT 5 -----------------------------------------\n");
	printf ("\tis_unusued             %4d  -  0x%x\n", fm.bit_5.is_unusued, fm.bit_5.is_unusued);
	printf ("\tis_Safely              %4d  -  0x%x\n", fm.bit_5.is_Safely, fm.bit_5.is_Safely);
	printf ("\tis_LastFrameLength     %4d  -  0x%x\n", fm.bit_5.is_LastFrameLength, fm.bit_5.is_LastFrameLength);
	printf ("\tis_TrueGapless         %4d  -  0x%x\n", fm.bit_5.is_TrueGapless, fm.bit_5.is_TrueGapless);
	printf ("\t-- BIT 6 -----------------------------------------\n");
	printf ("\tis_EncoderVersion      %4d  -  0x%x\n", fm.bit_6.is_EncoderVersion, fm.bit_6.is_EncoderVersion);
}


/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/

INFO_MPC *tagsmpc_remove_info (INFO_MPC *info)
{
	if (info) {

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_MPC *)NULL);
}

INFO_MPC *tagsmpc_get_info( INFO *p_info )
{
	INFO_MPC     *ptrinfo = NULL;
	TagLib_File  *file;
	TagLib_Tag   *tag;

	ptrinfo = (INFO_MPC *)malloc (sizeof (INFO_MPC));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc ();

	if ((file = taglib_file_new ( p_info->path_name_file ))) {

		taglib_set_strings_unicode(FALSE);
		tag = taglib_file_tag(file);

		ptrinfo->tags->Title     = C_strdup (taglib_tag_title(tag));
		ptrinfo->tags->Artist    = C_strdup (taglib_tag_artist(tag));
		ptrinfo->tags->Album     = C_strdup (taglib_tag_album(tag));
		ptrinfo->tags->IntYear   = taglib_tag_year(tag);
		ptrinfo->tags->Year      = C_strdup_printf ("%d", ptrinfo->tags->IntYear);
		ptrinfo->tags->Comment   = C_strdup (taglib_tag_comment(tag));
		ptrinfo->tags->IntNumber = taglib_tag_track(tag);
		ptrinfo->tags->Number    = C_strdup_printf ("%d", ptrinfo->tags->IntNumber);
		ptrinfo->tags->Genre     = C_strdup (taglib_tag_genre(tag));
		ptrinfo->tags->IntGenre  = tags_get_genre_by_value (ptrinfo->tags->Genre);
		taglib_tag_free_strings();
		taglib_file_free (file);
	}
	else {
		tags_complementation( ptrinfo->tags,  p_info->path_name_file );
	}

	return ((INFO_MPC *)ptrinfo);
}


