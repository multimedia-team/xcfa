/*
 *  file      : src/utils.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "global.h"


// Renvoie la taille d'un fichier en octets
// Returns the size of a file in bytes
size_t utils_get_size_file( char *PathName )
{
	FILE   *fp;
	size_t  size = 0;

	if ((fp = fopen( PathName, "r" ))) {
		fseek( fp, 0, SEEK_END );
		size = ftell( fp );
		fclose( fp );
	}
	return( size );
}
// Création d'un dossier temporaire
// Create a temporary folder
// FIXED
// Mon, 20 Jan 2014 16:33:36 +0100
// Plusieurs instances de XCFA peuvent être activées sans collision
// Multiple instances of XCFA can be activated without collision
char *utils_create_rep( char *path_name_rep )
{
	char	*PathTest = NULL;
	char	*Path = NULL;
	char	*Ptr = NULL;
	int		NumPath = 0;

	Path = C_strdup( path_name_rep );
	if( NULL != (Ptr = strrchr( Path, '/' )))
		if( *(Ptr + 1) == '\0' )
			*Ptr = '\0';
	PathTest = C_strdup( Path );

	while( TRUE ) {
		if( TRUE == C_dir_test( PathTest ) ) {
			free( PathTest );
			PathTest = NULL;
			PathTest = C_strdup_printf( "%s_%d/", Path, NumPath );
			NumPath ++;
		}
		else {
			C_mkdir_with_parents( PathTest );
			free( Path );
			Path = NULL;
			break;
		}
	}
	return( (char *)PathTest );
}
// Suppression d'un dossier temporaire
// Delete a temporary folder
char *utils_remove_temporary_rep( char *path_tmprep )
{
	if( NULL != path_tmprep ) {
		C_rmdir( path_tmprep );
		free( path_tmprep );
		path_tmprep = NULL;
	}
	return ((char *)NULL);
}
// Renvoie le chemin de destination
// Returns the destination path
char *utils_get_dir_dest( INFO *p_Info )
{
	char	*OutputDir = NULL;

	if( TRUE == Detail.BoolExtract2Src ) {
		OutputDir = C_strdup( p_Info->path );
	}
	else {
		if( *Detail.OutputDir == '/' ) {
			OutputDir = C_strdup( Detail.OutputDir );
		}
		else {
			OutputDir = C_strdup_printf( "%s%s", p_Info->path, Detail.OutputDir );
		}
	}
	return( OutputDir );
}