/*
 *  file      : src/get_info.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/utsname.h>

#include "global.h"
#include "tags.h"
#include "get_info.h"
#include "file_is.h"
#include "prg_init.h"

#define GET_MAX_CARS 1024

typedef struct {
	pid_t		code_fork;				// Num Code Fork
	int			signal_numchildren;		// the signal handler
	int			tube [ 2 ];				// for pipe
} VAR_GET;

VAR_GET VarGet;

/*
*---------------------------------------------------------------------------
* SIGNAL
*---------------------------------------------------------------------------
*/
void GetInfo_sigchld (int signum)
{
	int status;

	wait(&status);
    // if there are still children waiting
    // re-install the signal handler
	VarGet.signal_numchildren --;
	if (VarGet.signal_numchildren > 0) {
		// re-install the signal handler
		signal (SIGCHLD, GetInfo_sigchld);
	}
}
//
//
int GetInfo_exec_with_output (char **args, pid_t *p, int Streams)
{
	VarGet.signal_numchildren = 0;

	if (pipe (VarGet.tube) != 0) {
		fprintf (stderr, "error: pipe\n");
		exit (1);
	}
	if ((*p = fork()) == 0) {
		dup2 (VarGet.tube [ 1 ], Streams);	// STDOUT_FILENO STDERR_FILENO
		close (VarGet.tube [ 1 ]);
		execvp (args[0], args);
		fprintf (stderr, "error: exec");
		exit (2);
	}
	VarGet.signal_numchildren ++;
	signal (SIGCHLD, GetInfo_sigchld);
	close (VarGet.tube [ 1 ]);
	return (VarGet.tube [ 0 ]);
}


//
//
CString *GetInfo_file (char *file)
{
	int     fd;
	int     pos = 0;
	char   *args [ 10 ];
	char    buf [ GET_MAX_CARS + 10 ];
	int     size = 0;
	CString *gstr = C_string_new();

	args[pos++] = "file";
	args[pos++] = "-b";
	args[pos++] = "-z";
	args[pos++] = "-n";
	args[pos++] = file;
	args[pos++] = NULL;

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		/*
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			C_string_append_printf (gstr, "%s", buf);
		}
		*/
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';
		C_string_append_printf (gstr, "%s", buf);

	} while (size != 0);

	close(fd);
	return (gstr);
}


//
// RECONNAISSANCE ENTETE FICHIER:
//	FILE_IS_NONE = 0,
//	FILE_IS_FLAC,
//	FILE_IS_WAV,
//	FILE_IS_CUE,
//  FILE_IS_BFW,	// BROADCAST WAVE FORMAT
//	FILE_IS_MP3,
//	FILE_IS_OGG,
//	/* Les fichiers au format AAC, portant l'extension
//		.mp4( pour MPEG-4),
//	   	.m4a( pour MPEG-4 Audio)
//	   	.m4p( pour MPEG-4 Protégé),
//	   sont globalement plus petits que les fichiers au format MP3. */
//	FILE_IS_M4A,
//	FILE_IS_VID_M4A,
//	FILE_IS_AAC,
//	FILE_IS_SHN,
//	FILE_IS_WMA,
//	FILE_IS_RM,
//	FILE_IS_DTS,
//	FILE_IS_AIFF,
//	FILE_IS_MPC,
//	FILE_IS_APE,
//	FILE_IS_WAVPACK,
//	FILE_IS_WAVPACK_MD5,
//	FILE_IS_AC3,
//
TYPE_FILE_IS GetInfo_file_is( char *PathName )
{
	CString			*gstr = NULL;
	TYPE_FILE_IS	TypeFileRet = FILE_IS_NONE;

	gstr = GetInfo_file (PathName);
	// printf( "gstr =\n%s\n", gstr->str );
	// ABANDON SI LA COMMANDE file NE TROUVE PAS LE TYPE DE FICHIER
	if( 0 == strcmp( gstr->str, "data" )) {
		TypeFileRet = FILE_IS_NONE;
		PRINT("ERROR: LA COMMANDE file NE TROUVE RIEN :/");
	}
	// UNE PETITE CHANCE SE PRESENTE ICI POUR AAC
	else {
		// FILE_IS_AAC
		// 19 Waka Waka (This Time for Africa).m4a:          ISO Media, MPEG v4 system, iTunes AAC-LC
		// 4 Non Blondes - Whats Going On.m4a:               ISO Media, MPEG v4 system, version 2
		if( TRUE == FileIs_vidm4a( PathName )) {
			TypeFileRet = FILE_IS_VID_M4A;
		}
		else if( strstr (gstr->str, "AAC" ) || strstr (gstr->str, "MPEG ADTS, AAC," )) {
			TypeFileRet = FILE_IS_AAC;
		}
		// FILE_IS_WMA
		else if (strstr (gstr->str, "Microsoft ASF")) {
			TypeFileRet = FILE_IS_WMA;
		}
		// FILE_IS_VID_M4A
		// WCC_RADIO_EP042.m4a:			ISO Media, MPEG v4 system, iTunes AAC-LC
		//
		// FILE_IS_M4A
		// 21 The Academy.m4a:			ISO Media, MPEG v4 system, version 2
		// 4 Non Blondes - Whats Going On.m4a:	ISO Media, MPEG v4 system, version 2
		// audio.m4a:				ISO Media, MPEG v4 system, version 2
		//
		// FILE_IS_AAC
		// 01_King_Porter_Stomp.aac:		MPEG ADTS, AAC, v4 LC, 22.05 kHz, stereo
		// aif.aac:				MPEG ADTS, AAC, v4 LC, 22.05 kHz, stereo
		// audio.aac:				MPEG ADTS, AAC, v2 LC, 48 kHz, stereo
	}
	if( FILE_IS_VID_M4A == TypeFileRet || FILE_IS_AAC == TypeFileRet || FILE_IS_WMA == TypeFileRet ) {
		C_string_free( gstr );
		gstr = NULL;
		return( TypeFileRet );
	}

	// FILE_IS_VID_M4A
	if( TRUE == FileIs_vidm4a( PathName )) {
		TypeFileRet = FILE_IS_VID_M4A;
	}
	// FILE_IS_M4A
	else if( TRUE == FileIs_m4a( PathName )) {
		TypeFileRet = FILE_IS_M4A;
	}
	// FILE_IS_WAVPACK
	// FILE_IS_WAVPACK_MD5
	else if( TRUE == FileIs_wavpack( PathName )) {
		TypeFileRet = FILE_IS_WAVPACK;
	}
	// FILE_IS_WAV
	else if( TRUE == FileIs_wav( PathName )) {
		TypeFileRet = FILE_IS_WAV;
	}
	// FILE_IS_CUE
	else if( TRUE == FileIs_cue( PathName )) {
		TypeFileRet = FILE_IS_CUE;
	}
	// FILE_IS_SHN
	else if( TRUE == FileIs_shn( PathName )) {
		TypeFileRet = FILE_IS_SHN;
	}
	// FILE_IS_RM
	else if( TRUE == FileIs_rm( PathName )) {
		TypeFileRet = FILE_IS_RM;
	}
	// FILE_IS_OGG
	else if( TRUE == FileIs_ogg( PathName )) {
		TypeFileRet = FILE_IS_OGG;
	}
	// FILE_IS_MPC
	else if( TRUE == FileIs_mpc( PathName )) {
		TypeFileRet = FILE_IS_MPC;
	}
	// FILE_IS_AC3
	else if( TRUE == FileIs_ac3( PathName )) {
		TypeFileRet = FILE_IS_AC3;
	}
	// FILE_IS_FLAC
	else if( TRUE == FileIs_flac( PathName )) {
		TypeFileRet = FILE_IS_FLAC;
	}
	// FILE_IS_APE
	else if( TRUE == FileIs_ape( PathName )) {
		TypeFileRet = FILE_IS_APE;
	}
	// FILE_IS_DTS
	else if( TRUE == FileIs_dts( PathName )) {
		TypeFileRet = FILE_IS_DTS;
	}
	// FILE_IS_AIFF
	else if( TRUE == FileIs_aiff( PathName )) {
		TypeFileRet = FILE_IS_AIFF;
	}
	// FILE_IS_MP3
	else if( TRUE == FileIs_mp3( PathName )) {
		TypeFileRet = FILE_IS_MP3;
	}
	// FILE_IS_MP3
	// Without tag id3 :/
	else if( strstr (gstr->str, "MPEG ADTS, layer III" )) {
		TypeFileRet = FILE_IS_MP3;
	}

	C_string_free( gstr );
	gstr = NULL;

	return( TypeFileRet );
}

//
//
void GetInfo_set( INFO *p_Info )
{
	if ( p_Info->type_infosong_file_is == FILE_IS_FLAC )        p_Info->info = (INFO_FLAC *)tagsflac_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_WAV )     p_Info->info = (INFO_WAV *)tagswav_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_MP3 )     p_Info->info = tagsmp3_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_OGG )     p_Info->info = tagsogg_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_SHN )     p_Info->info = tagsshn_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_WMA )     p_Info->info = tagswma_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_RM )      p_Info->info = tagsrm_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_DTS )     p_Info->info = tagsdts_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_AIFF )    p_Info->info = tagsaiff_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_M4A )     p_Info->info = tagsm4a_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_VID_M4A ) p_Info->info = tagsm4a_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_MPC )     p_Info->info = tagsmpc_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_APE )     p_Info->info = tagsape_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_WAVPACK ) p_Info->info = tagswavpack_get_info( p_Info );
	else if( p_Info->type_infosong_file_is == FILE_IS_AC3 )     p_Info->info = tagsac3_get_info( p_Info );
}

//
//
void GetInfo_remove( INFO *p_Info )
{
	switch( p_Info->type_infosong_file_is ) {

	case FILE_IS_NONE :
	case FILE_IS_CUE :
		break;
	case FILE_IS_FLAC :
		{
		INFO_FLAC *info = (INFO_FLAC *)p_Info->info;
		info = (INFO_FLAC *)tagsflac_remove_info( info );
		}
		break;
	case FILE_IS_WAV :
		{
		INFO_WAV *info = (INFO_WAV *)p_Info->info;
		info = (INFO_WAV *)tagswav_remove_info( info );
		}
		break;
	case FILE_IS_MP3 :
		{
		INFO_MP3 *info = (INFO_MP3 *)p_Info->info;
		info = (INFO_MP3 *)tagsmp3_remove_info( info );
		}
		break;
	case FILE_IS_OGG :
		{
		INFO_OGG *info = (INFO_OGG *)p_Info->info;
		info = (INFO_OGG *)tagsogg_remove_info( info );
		}
		break;
	case FILE_IS_M4A :
	case FILE_IS_VID_M4A :
		{
		INFO_M4A *info = (INFO_M4A *)p_Info->info;
		info = (INFO_M4A *)tagsm4a_remove_info( info );
		}
		break;
	case FILE_IS_AAC :
		{
		INFO_AAC *info = (INFO_AAC *)p_Info->info;
		info = (INFO_AAC *)tagsaac_remove_info( info );
		}
		break;
	case FILE_IS_SHN :
		{
		INFO_SHN *info = (INFO_SHN *)p_Info->info;
		info = (INFO_SHN *)tagsshn_remove_info( info );
		}
		break;
	case FILE_IS_WMA :
		{
		INFO_WMA *info = (INFO_WMA *)p_Info->info;
		info = (INFO_WMA *)tagswma_remove_info( info );
		}
		break;
	case FILE_IS_RM :
		{
		INFO_RM *info = (INFO_RM *)p_Info->info;
		info = (INFO_RM *)tagsrm_remove_info( info );
		}
		break;
	case FILE_IS_DTS :
		{
		INFO_DTS *info = (INFO_DTS *)p_Info->info;
		info = (INFO_DTS *)tagsdts_remove_info( info );
		}
		break;
	case FILE_IS_AIFF :
		{
		INFO_AIFF *info = (INFO_AIFF *)p_Info->info;
		info = (INFO_AIFF *)tagsaiff_remove_info( info );
		}
		break;
	case FILE_IS_MPC :
		{
		INFO_MPC *info = (INFO_MPC *)p_Info->info;
		info = (INFO_MPC *)tagsmpc_remove_info( info );
		}
		break;
	case FILE_IS_APE :
		{
		INFO_APE *info = (INFO_APE *)p_Info->info;
		info = (INFO_APE *)tagsape_remove_info( info );
		}
		break;
	case FILE_IS_WAVPACK :
	case FILE_IS_WAVPACK_MD5 :
		{
		INFO_WAVPACK *info = (INFO_WAVPACK *)p_Info->info;
		info = (INFO_WAVPACK *)tagswavpack_remove_info( info );
		}
		break;
	case FILE_IS_AC3 :
		{
		INFO_AC3 *info = (INFO_AC3 *)p_Info->info;
		info = (INFO_AC3 *)tagsac3_remove_info( info );
		}
		break;
	case NBR_FILE_TO:
		break;
	}
}

//
//
CString *GetInfo_checkmp3 (char *file)
{
	int     fd;
	int     pos = 0;
	char   *args [ 10 ];
	char    buf [ GET_MAX_CARS + 10 ];
	int     size = 0;
	CString *gstr = NULL;


	if( FALSE == PrgInit.bool_checkmp3 ) {
		PRINT_FUNC_LF();
		printf ("\tPrgInit.bool_checkmp3 = %s\n", PrgInit.bool_checkmp3 ? "TRUE" : "FALSE");
		printf ("\tIL FAUT INSTALLER checkmp3 OU mp3check SUIVANT VOTRE DISTRIBUTION !\n\n");
		return( NULL );
	}

	gstr = C_string_new();

	// args[pos++] = PrgInit.name_checkmp3;
	args[pos++] = prginit_get_name (NMR_checkmp3);
	args[pos++] = "-a";
	args[pos++] = "-v";

	// TODO: @Tetsumaki
	// http://forum.ubuntu-fr.org/viewtopic.php?pid=3889239#p3889239
	// OPTION -i n'existe pas sous ARCHLINUX !!!
	// args[pos++] = "-i";

	args[pos++] = file;
	args[pos++] = NULL;

	/*
	printf ("!-----------------------------------!\n");
	printf ("!           C H E C K M P 3         !\n");
	printf ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		printf ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	printf ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			C_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);
	return (gstr);
}

//
//
SHNTAG *GetInfo_free_shntool (SHNTAG *ShnTag)
{
	if (NULL != ShnTag) {
		if (NULL != ShnTag->time)	{ free (ShnTag->time);	ShnTag->time = NULL; }
		if (NULL != ShnTag->size)	{ free (ShnTag->size);	ShnTag->size = NULL; }

		free (ShnTag);
		ShnTag = NULL;
	}
	return ((SHNTAG *)NULL);
}
//
//
SHNTAG *GetInfo_shntool (char *file)
{
	int     fd;
	int     pos = 0;
	char   *args [ 10 ];
	char    buf [ GET_MAX_CARS + 10 ];
	int     size = 0;
	CString *gstr = C_string_new();
	SHNTAG	*ShnTag = NULL;
	char	*Ptr = NULL;

	int	 Min;
	int	 Sec;
	int	 Hundr;

	args[pos++] = "shntool";
	args[pos++] = "info";
	args[pos++] = file;
	args[pos++] = NULL;
	/*
	printf ("!-----------------------------------!\n");
	printf ("!          S H N T O O L            !\n");
	printf ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		printf ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	printf ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			C_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);

	ShnTag = malloc (sizeof (SHNTAG));

	if ((Ptr = strstr (gstr->str, "Length:")) != NULL) {
		Ptr = strchr (Ptr, ':');
		Ptr ++;
		while (*Ptr == ' ') Ptr ++;

		Min = atoi (Ptr);

		Ptr = strchr (Ptr, ':');
		Ptr ++;
		Sec = atoi (Ptr);

		Ptr = strchr (Ptr, '.');
		Ptr ++;
		Hundr = atoi (Ptr);

		ShnTag->SecTime = (double)(Min * 60) + (double)(Sec) + (double)(Hundr / 10.0);

		ShnTag->time = C_strdup_printf ("%02d:%02d", Min, Sec);

		if ((Ptr = strchr (ShnTag->time, '.')) != NULL) {
			*Ptr = '\0';
		}
	}

	ShnTag->size = C_strdup_printf ("%d Ko", (int)utils_get_size_file (file) / 1024);

	C_string_free( gstr );

	return ((SHNTAG *)ShnTag);
}

//
//
CString *GetInfo_faad (char *file)
{
	int     fd;
	int     pos = 0;
	char   *args [ 10 ];
	char    buf [ GET_MAX_CARS + 10 ];
	int     size = 0;
	CString *gstr = C_string_new();

	args[pos++] = "faad";
	args[pos++] = "-i";
	args[pos++] = file;
	args[pos++] = NULL;

	/*
	printf ("!-----------------------------------!\n");
	printf ("!             F A A D               !\n");
	printf ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		printf ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	printf ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		/*
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			C_string_append_printf (gstr, "%s", buf);
		}
		*/
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';
		C_string_append_printf (gstr, "%s", buf);

	} while (size != 0);

	close(fd);
	return (gstr);
}


//
//
char *GetInfo_cpu (void)
{
	char	 buf [ GET_MAX_CARS + 10 ];
	FILE	*fp;
	char	*Ptr = NULL;

	fp = fopen ("/proc/cpuinfo", "r");
	while (fgets (buf,  GET_MAX_CARS, fp) != NULL) {
		// printf("BUF = %s", buf);
		Ptr = buf;
		if (*(Ptr + 0) == 'm' &&
		    *(Ptr + 1) == 'o' &&
		    *(Ptr + 2) == 'd' &&
		    *(Ptr + 3) == 'e' &&
		    *(Ptr + 4) == 'l' &&
		    *(Ptr + 5) == ' ' &&
		    *(Ptr + 6) == 'n' &&
		    *(Ptr + 7) == 'a' &&
		    *(Ptr + 8) == 'm' &&
		    *(Ptr + 9) == 'e'

		) {
			// printf("---------BUF = %s", buf);

			Ptr = buf;
			while (*Ptr ++)
				if (*Ptr == '\n') *Ptr = '\0';
			for ( Ptr = buf; *Ptr != ':'; Ptr ++ );
			Ptr ++;
			while (*Ptr == ' ') Ptr ++;
			// printf("---------BUF = %s", Ptr);

			fclose (fp);
			return (C_strdup (Ptr));
		}
	}
	fclose (fp);
	return (NULL);
}
//
//
void GetInfo_cpu_print (void)
{
	struct		utsname utsname;
	char		*ptruname = NULL;
	char		*ModelName = NULL;
	HOST_CONF	 HostConf;

	ModelName = GetInfo_cpu ();
	HostConf.NbCpu   = sysconf(_SC_NPROCESSORS_ONLN);
	// printf ("_SC_NPROCESSORS_CONF : Le nombre de processeurs configure                           = %d\n", (int)sysconf(_SC_NPROCESSORS_CONF));
	// printf ("_SC_NPROCESSORS_ONLN : Le nombre de processeurs actuellement en ligne (disponibles) = %d\n", (int)sysconf(_SC_NPROCESSORS_ONLN));
	HostConf.TypeCpu = 8 * sizeof(void*);
	if (uname (&utsname) == 0) {

		strcpy (HostConf.Machine, utsname.machine);
		if (strcmp (HostConf.Machine, "x86_64") == 0)
			HostConf.BoolCpuIs64Bits = TRUE;
		else	HostConf.BoolCpuIs64Bits = FALSE;

		ptruname = C_strdup_printf (
				"! sysname    %s\n"
				"! nodename   %s\n"
				"! release    %s\n"
				"! version    %s\n"
				"! machine    %s\n"
				"!            %d CPU (%d bits)\n"
				"!            %s\n",
			utsname.sysname, utsname.nodename, utsname.release, utsname.version, utsname.machine,
			HostConf.NbCpu, HostConf.TypeCpu,
			ModelName);
		printf("%s",ptruname);
	}
	free (ModelName);
	ModelName = NULL;
	free (ptruname);
	ptruname = NULL;
}

//
//
int GetInfo_level_get_from( TYPE_FILE_IS TypeFileIs, char *file )
{
	int   pos = 0;
	char *args [ 20 ];
	int   fd;
	int   size = -1;
	char  buf [ 1024 + 10 ];
	int   arrondit = -1;

	if( FALSE == PrgInit.bool_normalize ) return( arrondit );

	if( TypeFileIs == FILE_IS_WAV ) {
		args[pos++] = prginit_get_name( NMR_normalize );
		args[pos++] = "-q";
		args[pos++] = "-n";
		args[pos++] = file;
		args[pos++] = NULL;
	}
	else if( TypeFileIs == FILE_IS_MP3 ) {
		args[pos++] = "normalize-mp3";
		args[pos++] = "-q";
		args[pos++] = "-n";
		args[pos++] = file;
		args[pos++] = NULL;
	}
	else if( TypeFileIs == FILE_IS_OGG ) {
		args[pos++] = "normalize-ogg";
		args[pos++] = "-q";
		args[pos++] = "-n";
		args[pos++] = file;
		args[pos++] = NULL;
	}

	fd = GetInfo_exec_with_output( args, &VarGet.code_fork, STDOUT_FILENO );
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= 1024) {
				PRINT_FUNC_LF();
				printf ("pos(%d) >= 1024\n", pos);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';
		if (strstr (buf, "dBFS")) {
			arrondit = (int)roundf(atof(buf));
		}
	} while (size != 0);

	close(fd);
	VarGet.code_fork = -1;
	return( arrondit );
}
