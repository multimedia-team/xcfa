/*
 *  file      : src/prg_init.h
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */



#ifndef prg_init_h
#define prg_init_h 1


typedef enum {
	NMR_a52dec = 0,						//
	NMR_aacplusenc,						//
	NMR_checkmp3,						//
	NMR_faac,							//
	NMR_faad,							//
	NMR_flac,							//
	NMR_lame,							//
	NMR_mac,							//
	NMR_musepack_tools_mppdec,			//
	NMR_musepack_tools_mppenc,			//
	NMR_mplayer,						//
	NMR_mp3gain,						//
	NMR_normalize,						//
	NMR_libnotify_bin,					//
	NMR_shorten,						//
	NMR_shntool,						//
	NMR_sox,							//
	NMR_vorbis_tools_oggenc,			//
	NMR_vorbisgain,						//
	NMR_wavpack,						//
	NMR_MAX_TABLEAU						//
} TYPE_PROGINIT;

typedef struct {
	char		*Name1;					// NAME1
	char		*Name2;					// NAME2
	char		*Name3;					// NAME3
	C_BOOL		*BoolFound;				// FOUND
	char		*PtrName;				// PTR ON Name 1 | 2 | 3
	char		*Description;			// DESCRIPTION TREE-VIEW
} TABLEAU_PRG_EXTERN;

typedef struct {
	C_BOOL	bool_a52dec;			//
	C_BOOL	bool_notify_send;		//
	C_BOOL	bool_ape;				//
	C_BOOL	bool_mpc123_mppdec;		//
	C_BOOL	bool_mppenc;			//
	C_BOOL	bool_cdda2wav;			//
	C_BOOL	bool_lame;				//
	C_BOOL	bool_oggenc;			//
	C_BOOL	bool_sox;				//
	C_BOOL	bool_normalize;			//
	C_BOOL	bool_checkmp3;			//
	C_BOOL	bool_faad;				//
	C_BOOL	bool_faac;				//
	C_BOOL	bool_mplayer;			//
	C_BOOL	bool_shorten;			//
	C_BOOL	bool_vorbisgain;		//
	C_BOOL	bool_flac;				//
	C_BOOL	bool_mp3gain;			//
	C_BOOL	bool_wavpack;			//
	C_BOOL	bool_shntool;			//
	C_BOOL	bool_aacplusenc;		//
} PRGINIT;

extern PRGINIT PrgInit;

char		*prginit_get_name (TYPE_PROGINIT TypeEnum);
void		prginit_scan( void );
void		prginit_print_info( void );

#endif

