/*
 *  file      : src/file_conv.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */

#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "global.h"
#include "conv.h"
#include "tags.h"



//
// Change quantification with SOX program
//
C_BOOL fileconv_change_quantification_with_sox(
						C_BOOL pbool_copy_dest_to_src,
						char *p_src,
						char *p_dest,
						int p_Hertz,
						int p_Channels,
						int p_Bits
						)
{
	char	StrHertz[ 10 ];
	char	StrChannels[ 10 ];
	char	StrBits[ 10 ];
	char	**PtrTabArgs = NULL;

	sprintf( &StrHertz[0], "%d", (int)p_Hertz );
	sprintf( &StrChannels[0], "%d", (int)p_Channels );
	sprintf( &StrBits[0], "%d", (int)p_Bits );

	PtrTabArgs = conv_with_sox_get_param( p_src, p_dest, StrHertz, StrChannels, StrBits );
	conv_to_convert( PtrTabArgs, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
	PtrTabArgs = conv_RemoveTab( PtrTabArgs );
	if( TRUE == pbool_copy_dest_to_src )
		conv_copy_src_to_dest( p_dest, p_src );

	return( C_file_test( p_src ));
}


//
// Conversion and | or split
//
// FROM:
//		FLAC | APE | WAVPACK | OGG | M4A | MPC | MP3 | WMA | SHORTEN | RM | DTS | AIF | AC3
// TO:
//		WAV
// TO:
//		FLAC | APE | WAVPACK | OGG | M4A | AAC | MPC | MP3
//
void file_conv( void )
{
	CList	*list = NULL;
	INFO	*Info = NULL;
	int		Channels;
	int		Hertz;
	int		Bits;
	int		NewBits;
	int		pos;
	char	**PtrTabArgs = NULL;
	char	*OutputDir = NULL;
	char	*basename = NULL;
	char	*Ptr = NULL;
	int	i;
	TAGS	*info_tags = NULL;

	if( NULL != ( list = C_list_first( Detail.ListFile ))) {

		// TEMPORARY FOLDER CONVERSION
		conv.TmpRep  = utils_create_rep( "/tmp/xcfa_cli_conv/" );

		while( list ) {
			if( NULL != (Info = (INFO *)list->data) ) {

				OutputDir = utils_get_dir_dest( Info );

				// MAKE DIRECTORY DESTINATION IF NOT EXIST
				if( FALSE == C_dir_test( OutputDir ))
					C_mkdir_with_parents( OutputDir );

				Info->tmp_wav = C_strdup_printf( "%s/TmpRep.wav", conv.TmpRep );
				Info->tmp_sox = C_strdup_printf( "%s/TmpRep.sox.wav", conv.TmpRep );

				basename = C_path_get_basename( Info->path_name_file );
				if( NULL != (Ptr = strchr( basename, '.' ))) *Ptr = '\0';

				// ------------------------------------------------------------------------
				//
				// FROM:
				//		FLAC | APE | WAVPACK | OGG | M4A | FILE_IS_VID_M4A | MPC | MP3 | WMA | SHORTEN | RM | DTS | AIF | AC3
				// TO:
				//		WAV
				//
				// ------------------------------------------------------------------------

				switch( Info->type_infosong_file_is ) {

				case FILE_IS_WAV :
					{
					INFO_WAV	*info_wav = (INFO_WAV *)Info->info;
					info_tags = info_wav->tags;

					if( TRUE == info_wav->BoolBwf ) {
						PtrTabArgs = conv_with_sox_float_get_param(
								Info->path_name_file,
								Info->tmp_wav
								);
						conv_to_convert( PtrTabArgs, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV" );
						PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
					else {
						conv_copy_src_to_dest( Info->path_name_file, Info->tmp_wav );
					}
					}
					break;

				case FILE_IS_FLAC :
					{
					INFO_FLAC	*info_flac = (INFO_FLAC *)Info->info;
					info_tags = info_flac->tags;

					PtrTabArgs = conv_get_command_line(
						FLAC_FLAC_TO_WAV,
						Info->path_name_file,
						Info->tmp_wav,
						NULL,
						Detail.Op_flac
						);
					conv_to_convert( PtrTabArgs, FLAC_FLAC_TO_WAV, "FLAC_FLAC_TO_WAV");
					PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
					break;

				case FILE_IS_APE :
					{
					INFO_APE	*info_ape = (INFO_APE *)Info->info;
					info_tags = info_ape->tags;

					PtrTabArgs = conv_get_command_line(
						MAC_APE_TO_WAV,
						Info->path_name_file,
						Info->tmp_wav,
						NULL,
						Detail.Op_ape
						);
					conv_to_convert( PtrTabArgs, MAC_APE_TO_WAV, "MAC_APE_TO_WAV");
					PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
					break;

				case FILE_IS_WAVPACK :
					{
					INFO_WAVPACK	*info_wavpack = (INFO_WAVPACK *)Info->info;
					info_tags = info_wavpack->tags;

					PtrTabArgs = conv_get_command_line(
						WVUNPACK_WAVPACK_TO_WAV,
						Info->path_name_file,
						Info->tmp_wav,
						NULL,
						Detail.Op_wavpack
						);
					conv_to_convert( PtrTabArgs, WVUNPACK_WAVPACK_TO_WAV, "WVUNPACK_WAVPACK_TO_WAV");
					PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
					break;

				case FILE_IS_MP3 :
				case FILE_IS_OGG :
					{
					if( FILE_IS_MP3 == Info->type_infosong_file_is ) {
						INFO_MP3	*info_mp3 = (INFO_MP3 *)Info->info;
						info_tags = info_mp3->tags;
					}
					else if( FILE_IS_OGG == Info->type_infosong_file_is ) {
						INFO_OGG	*info_ogg = (INFO_OGG *)Info->info;
						info_tags = info_ogg->tags;
					}

					PtrTabArgs = conv_AllocTabArgs( &pos );
					PtrTabArgs[ pos++ ] = C_strdup ("mplayer");
					PtrTabArgs[ pos++ ] = C_strdup ("-nojoystick");
					PtrTabArgs[ pos++ ] = C_strdup ("-nolirc");
					PtrTabArgs[ pos++ ] = C_strdup ("-ao");
					PtrTabArgs[ pos++ ] = C_strdup ("pcm");
					PtrTabArgs[ pos++ ] = C_strdup( Info->path_name_file );
					PtrTabArgs[ pos++ ] = C_strdup ("-ao");
					PtrTabArgs[ pos++ ] = C_strdup_printf ("pcm:file=%s", Info->tmp_wav );
					PtrTabArgs[ pos ] = NULL;
					if( FILE_IS_OGG == Info->type_infosong_file_is ) {
						conv_to_convert( PtrTabArgs, MPLAYER_OGG_TO_WAV, "MPLAYER_OGG_TO_WAV");
						PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
					else {
						conv_to_convert( PtrTabArgs, MPLAYER_MP3_TO_WAV, "MPLAYER_MP3_TO_WAV");
						PtrTabArgs = conv_RemoveTab( PtrTabArgs );

						// Mon, 06 Jan 2014 08:33:58 +0100
						// Isaak Babaev
						// Must be 16 bits
						Channels = 0;
						Hertz = 0;
						Bits = 0;
						tagswav_file_GetBitrate( Info->tmp_wav, &Channels, &Hertz, &Bits );
						if( Bits != 16 ) {
							Bits = 16;
							fileconv_change_quantification_with_sox( TRUE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, Bits );
						}
					}
					}
					break;

				case FILE_IS_MPC :
					{
					INFO_MPC	*info_mpc = (INFO_MPC *)Info->info;
					info_tags = info_mpc->tags;

					PtrTabArgs = conv_AllocTabArgs( &pos );
					PtrTabArgs [ pos++ ] = C_strdup ("mplayer");
					PtrTabArgs [ pos++ ] = C_strdup ("-nojoystick");
					PtrTabArgs [ pos++ ] = C_strdup ("-nolirc");
					PtrTabArgs [ pos++ ] = C_strdup( Info->path_name_file );
					PtrTabArgs [ pos++ ] = C_strdup ("-ao");
					PtrTabArgs [ pos++ ] = C_strdup ("pcm");
					PtrTabArgs [ pos++ ] = C_strdup ("-ao");
					PtrTabArgs [ pos++ ] = C_strdup_printf ("pcm:file=%s", Info->tmp_wav );
					PtrTabArgs [ pos++ ] = C_strdup ("-af");
					PtrTabArgs [ pos++ ] = C_strdup ("channels=2");
					PtrTabArgs [ pos++ ] = C_strdup ("-srate");
					PtrTabArgs [ pos++ ] = C_strdup ("44100");
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, MPLAYER_MPC_TO_WAV, "MPLAYER_MPC_TO_WAV");
					PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
					break;

				case FILE_IS_AC3 :
					{
					INFO_AC3	*info_ac3 = (INFO_AC3 *)Info->info;
					info_tags = info_ac3->tags;

					PtrTabArgs = conv_AllocTabArgs( &pos );
					PtrTabArgs [ pos++ ] = C_strdup ("a52dec");
					PtrTabArgs [ pos++ ] = C_strdup( Info->path_name_file );
					PtrTabArgs [ pos++ ] = C_strdup ("-o");
					PtrTabArgs [ pos++ ] = C_strdup ("wav");
					PtrTabArgs [ pos++ ] = C_strdup (">");
					PtrTabArgs [ pos++ ] = C_strdup( Info->tmp_wav );
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, A52DEC_AC3_TO_WAV, "A52DEC_AC3_TO_WAV" );
					PtrTabArgs = conv_RemoveTab( PtrTabArgs );

					tagswav_file_GetBitrate( Info->tmp_wav, &Channels, &Hertz, &Bits );
					fileconv_change_quantification_with_sox( TRUE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, Bits );
					}
					break;

				case FILE_IS_M4A :
				case FILE_IS_VID_M4A :
					{
					INFO_M4A	*info_m4a = (INFO_M4A *)Info->info;
					info_tags = info_m4a->tags;

					PtrTabArgs = conv_AllocTabArgs( &pos );
					PtrTabArgs [ pos++ ] = C_strdup ("mplayer");
					PtrTabArgs [ pos++ ] = C_strdup ("-nojoystick");
					PtrTabArgs [ pos++ ] = C_strdup ("-nolirc");
					if( FILE_IS_VID_M4A == Info->type_infosong_file_is ) {
						PtrTabArgs [ pos++ ] = C_strdup ("-novideo");
						PtrTabArgs [ pos++ ] = C_strdup ("-vo");
						PtrTabArgs [ pos++ ] = C_strdup ("null");
					}
					PtrTabArgs [ pos++ ] = C_strdup ("-ao");
					PtrTabArgs [ pos++ ] = C_strdup ("pcm");
					PtrTabArgs [ pos++ ] = C_strdup( Info->path_name_file );
					PtrTabArgs [ pos++ ] = C_strdup ("-ao");
					PtrTabArgs [ pos++ ] = C_strdup_printf ("pcm:file=%s", Info->tmp_wav );
					if( FILE_IS_VID_M4A == Info->type_infosong_file_is ) {
						PtrTabArgs [ pos++ ] = C_strdup ("-srate");
						PtrTabArgs [ pos++ ] = C_strdup ("44100");
					}
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, MPLAYER_M4A_TO_WAV, "MPLAYER_M4A_TO_WAV");
					PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					}
					break;

				case FILE_IS_WMA :
				case FILE_IS_RM :
				case FILE_IS_DTS :
				case FILE_IS_AIFF :
					if( FILE_IS_WMA == Info->type_infosong_file_is ) {
						INFO_WMA	*info_wma = (INFO_WMA *)Info->info;
						info_tags = info_wma->tags;
					}
					else if( FILE_IS_RM == Info->type_infosong_file_is ) {
						INFO_RM	*info_rm = (INFO_RM *)Info->info;
						info_tags = info_rm->tags;
					}
					else if( FILE_IS_DTS == Info->type_infosong_file_is ) {
						INFO_DTS	*info_dts = (INFO_DTS *)Info->info;
						info_tags = info_dts->tags;
					}
					else if( FILE_IS_AIFF == Info->type_infosong_file_is ) {
						INFO_AIFF	*info_aif = (INFO_AIFF *)Info->info;
						info_tags = info_aif->tags;
					}

					PtrTabArgs = conv_AllocTabArgs( &pos );
					PtrTabArgs [ pos++ ] = C_strdup ("mplayer");
					PtrTabArgs [ pos++ ] = C_strdup ("-nojoystick");
					PtrTabArgs [ pos++ ] = C_strdup ("-nolirc");
					PtrTabArgs [ pos++ ] = C_strdup( Info->path_name_file );
					PtrTabArgs [ pos++ ] = C_strdup ("-ao");
					PtrTabArgs [ pos++ ] = C_strdup ("pcm");
					PtrTabArgs [ pos++ ] = C_strdup ("-ao");
					PtrTabArgs [ pos++ ] = C_strdup_printf ("pcm:file=%s", Info->tmp_wav );
					PtrTabArgs [ pos++ ] = C_strdup ("-af");
					PtrTabArgs [ pos++ ] = C_strdup ("channels=2");
					PtrTabArgs [ pos++ ] = C_strdup ("-srate");
					PtrTabArgs [ pos++ ] = C_strdup ("44100");
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, MPLAYER_WMA_TO_WAV, "MPLAYER_WMA_TO_WAV");
					PtrTabArgs = conv_RemoveTab( PtrTabArgs );
					break;

				case FILE_IS_SHN :
					{
					INFO_SHN	*info_shn = (INFO_SHN *)Info->info;
					info_tags = info_shn->tags;

					PtrTabArgs = conv_AllocTabArgs( &pos );
					PtrTabArgs [ pos++ ] = C_strdup ("shorten");
					PtrTabArgs [ pos++ ] = C_strdup ("-x");
					PtrTabArgs [ pos++ ] = C_strdup ("-b");
					PtrTabArgs [ pos++ ] = C_strdup ("256");
					PtrTabArgs [ pos++ ] = C_strdup ("-c");
					PtrTabArgs [ pos++ ] = C_strdup ("2");
					PtrTabArgs [ pos++ ] = C_strdup( Info->path_name_file );
					PtrTabArgs [ pos++ ] = C_strdup( Info->tmp_wav );
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, SHORTEN_SHN_TO_WAV, "SHORTEN_SHN_TO_WAV");
					}
					break;

				case FILE_IS_NONE :
				case FILE_IS_AAC :
				case FILE_IS_WAVPACK_MD5 :
				case FILE_IS_CUE :
				case NBR_FILE_TO:
					break;
				}

				// ------------------------------------------------------------------------
				//
				// S P L I T
				//		WAV to WAV
				//
				// ------------------------------------------------------------------------
				if( 0 != Detail.split_length )
					split_wav( Info->tmp_wav, Detail.split, Detail.split_length );

				// ------------------------------------------------------------------------
				//
				// S E T T I N G    WAV
				//
				// ------------------------------------------------------------------------
				if( 0 != Detail.frequency || 0 != Detail.track || 0 != Detail.quantification )
					wav_change_setting( Info->tmp_wav, Detail.frequency, Detail.track, Detail.quantification );

				tagswav_file_GetBitrate( Info->tmp_wav, &Channels, &Hertz, &Bits );
				NewBits = Bits;

				// ------------------------------------------------------------------------
				//
				// FROM:
				//		WAV
				// TO:
				//		FLAC | APE | WAVPACK | OGG | M4A | FILE_IS_VID_M4A | AAC | MPC | MP3
				//
				// ------------------------------------------------------------------------

				Info->tmp_flac    = C_strdup_printf( "%s%s.flac", OutputDir, basename );
				Info->tmp_ape     = C_strdup_printf( "%s%s.ape", OutputDir, basename );
				Info->tmp_wavpack = C_strdup_printf( "%s%s.wv", OutputDir, basename );
				Info->tmp_sox_24  = C_strdup_printf( "%s/TmpRep_24.sox.wav", conv.TmpRep );
				Info->tmp_ogg     = C_strdup_printf( "%s%s.ogg", OutputDir, basename );
				Info->tmp_m4a     = C_strdup_printf( "%s%s.m4a", OutputDir, basename );
				Info->tmp_aac     = C_strdup_printf( "%s%s.aac", OutputDir, basename );
				Info->tmp_mpc     = C_strdup_printf( "%s%s.mpc", OutputDir, basename );
				Info->tmp_mp3     = C_strdup_printf( "%s%s.mp3", OutputDir, basename );

				for( i = 0; i < NBR_FILE_TO; i ++ ) {
					if( TRUE == Detail.BoolTypeDest[ i ] ) {
						switch( i ) {
							case FILE_IS_FLAC:
								if( Bits == 32 )
									fileconv_change_quantification_with_sox( FALSE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, 24 );
								PtrTabArgs = conv_get_command_line(
									FLAC_WAV_TO_FLAC,
									( Bits == 32 ) ? Info->tmp_sox : Info->tmp_wav,
									Info->tmp_flac,
									NULL,
									Detail.Op_flac
									);
								conv_to_convert( PtrTabArgs, FLAC_WAV_TO_FLAC, "FLAC_WAV_TO_FLAC");
								PtrTabArgs = conv_RemoveTab( PtrTabArgs );
								break;

							case FILE_IS_APE:
								// if( Bits != 16 );
									fileconv_change_quantification_with_sox( FALSE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, 16 );
								PtrTabArgs = conv_get_command_line(
									MAC_WAV_TO_APE,
									// ( Bits != 16 ) ? Info->tmp_sox : Info->tmp_wav,
									Info->tmp_sox,
									Info->tmp_ape,
									NULL,
									Detail.Op_ape
									);
								conv_to_convert( PtrTabArgs, MAC_WAV_TO_APE, "MAC_WAV_TO_APE");
								PtrTabArgs = conv_RemoveTab( PtrTabArgs );
								break;

							case FILE_IS_WAVPACK:
								PtrTabArgs = conv_get_command_line(
									WAVPACK_WAV_TO_WAVPACK,
									Info->tmp_wav,
									Info->tmp_wavpack,
									(Detail.BoolNoTag == TRUE) ? NULL : info_tags,
									Detail.Op_wavpack
									);
								conv_to_convert( PtrTabArgs, WAVPACK_WAV_TO_WAVPACK, "WAVPACK_WAV_TO_WAVPACK");
								PtrTabArgs = conv_RemoveTab( PtrTabArgs );
								break;

							case FILE_IS_WAV:
								{
								char *dest_wav = C_strdup_printf( "%s%s.wav", OutputDir, basename );
								conv_copy_src_to_dest( Info->tmp_wav, dest_wav );

								if( TRUE == Detail.bool_normalize )
									Info->path_is_normalize = C_strdup( dest_wav );

								free( dest_wav );
								dest_wav = NULL;
								}
								break;

							case FILE_IS_OGG:
								if( Bits != 24 && Bits != 16 && Bits != 8 ) {
									if (Bits > 24)						NewBits = 24;
									else if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 24 : 16;
									else if (Bits > 8 && Bits < 16)		NewBits = (Bits - 8) < 3 ? 16 : 8;
									else if (Bits < 8)					NewBits = 8;
									Bits = NewBits;
									fileconv_change_quantification_with_sox( FALSE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, 24 );
								}
								if( Bits == 24 ) {
									PtrTabArgs = conv_with_sox_float_get_param(
												( Bits != 24 && Bits != 16 && Bits != 8 ) ? Info->tmp_sox : Info->tmp_wav,
												Info->tmp_sox_24
												);
									conv_to_convert( PtrTabArgs, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
									PtrTabArgs = conv_RemoveTab( PtrTabArgs );
									conv_copy_src_to_dest( Info->tmp_sox_24, Info->tmp_sox );
								}

								PtrTabArgs = conv_get_command_line(
									OGGENC_WAV_TO_OGG,
									( Bits != 24 && Bits != 16 && Bits != 8 ) || ( Bits == 24 ) ? Info->tmp_sox : Info->tmp_wav,
									Info->tmp_ogg,
									(Detail.BoolNoTag == TRUE) ? NULL : info_tags,
									Detail.Op_ogg
									);
								conv_to_convert( PtrTabArgs, OGGENC_WAV_TO_OGG, "OGGENC_WAV_TO_OGG");
								PtrTabArgs = conv_RemoveTab( PtrTabArgs );

								if( TRUE == Detail.bool_normalize )
									Info->path_is_normalize = C_strdup( Info->tmp_ogg );
								break;

							case FILE_IS_M4A:
								if( Bits != 64 && Bits != 32 && Bits != 24 && Bits != 16 && Bits != 8 ) {
									if (Bits > 64)						NewBits = 64;
									else if (Bits > 32 && Bits < 64)	NewBits = 32;
									else if (Bits > 24 && Bits < 32)	NewBits = 24;
									else if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 24 : 16;
									else if (Bits > 8 && Bits < 16)		NewBits = (Bits - 8) < 3 ? 16 : 8;
									else if (Bits < 8)					NewBits = 8;
									NewBits = Bits;
									fileconv_change_quantification_with_sox( FALSE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, NewBits );
								}
								else {
									conv_copy_src_to_dest( Info->tmp_wav, Info->tmp_sox );
								}
								if( Bits == 32 || Bits == 24 ) {
									PtrTabArgs = conv_with_sox_float_get_param(
												Info->tmp_sox,
												Info->tmp_sox_24
												);
									conv_to_convert( PtrTabArgs, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
									PtrTabArgs = conv_RemoveTab( PtrTabArgs );
									conv_copy_src_to_dest( Info->tmp_sox_24, Info->tmp_sox );
								}
								PtrTabArgs = conv_get_command_line(
									FAAC_WAV_TO_M4A,
									Info->tmp_sox,
									Info->tmp_m4a,
									(Detail.BoolNoTag == TRUE) ? NULL : info_tags,
									Detail.Op_m4a
									);
								conv_to_convert( PtrTabArgs, FAAC_WAV_TO_M4A, "FAAC_WAV_TO_M4A");
								PtrTabArgs = conv_RemoveTab( PtrTabArgs );
								break;

							case FILE_IS_AAC:
								if( Bits != 64 && Bits != 32 && Bits != 24 && Bits != 16 && Bits != 8 ) {
									if (Bits > 64)						NewBits = 64;
									else if (Bits > 32 && Bits < 64)	NewBits = 32;
									else if (Bits > 24 && Bits < 32)	NewBits = 24;
									else if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 24 : 16;
									else if (Bits > 8 && Bits < 16)		NewBits = (Bits - 8) < 3 ? 16 : 8;
									else if (Bits < 8)					NewBits = 8;
									NewBits = Bits;
									fileconv_change_quantification_with_sox( FALSE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, NewBits );
								}
								else {
									conv_copy_src_to_dest( Info->tmp_wav, Info->tmp_sox );
								}
								if( Bits == 24 ) {
									PtrTabArgs = conv_with_sox_float_get_param(
											Info->tmp_sox,
											Info->tmp_sox_24
											);
									conv_to_convert( PtrTabArgs, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
									PtrTabArgs = conv_RemoveTab( PtrTabArgs );
									conv_copy_src_to_dest( Info->tmp_sox_24, Info->tmp_sox);
								}
								if( Hertz < 44100 ) {
									PtrTabArgs = conv_AllocTabArgs( &pos );
									PtrTabArgs [ pos++ ] = C_strdup ("mplayer");
									PtrTabArgs [ pos++ ] = C_strdup ("-nojoystick");
									PtrTabArgs [ pos++ ] = C_strdup ("-nolirc");
									PtrTabArgs [ pos++ ] = C_strdup ( Info->tmp_sox );
									PtrTabArgs [ pos++ ] = C_strdup ("-ao");
									PtrTabArgs [ pos++ ] = C_strdup ("pcm");
									PtrTabArgs [ pos++ ] = C_strdup ("-ao");
									PtrTabArgs [ pos++ ] = C_strdup_printf( "pcm:file=%s", Info->tmp_sox_24 );
									PtrTabArgs [ pos++ ] = C_strdup ("-srate");
									PtrTabArgs [ pos++ ] = C_strdup ("44100");
									PtrTabArgs [ pos++ ] = NULL;
									conv_to_convert( PtrTabArgs, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV");
									PtrTabArgs = conv_RemoveTab( PtrTabArgs );
									conv_copy_src_to_dest( Info->tmp_sox_24, Info->tmp_sox );
								}
								PtrTabArgs = conv_get_command_line(
									AACPLUSENC_WAV_TO_AAC,
									Info->tmp_sox,
									Info->tmp_aac,
									NULL,
									Detail.Op_aac
									);
								conv_to_convert( PtrTabArgs, AACPLUSENC_WAV_TO_AAC, "AACPLUSENC_WAV_TO_AAC");
								PtrTabArgs = conv_RemoveTab( PtrTabArgs );
								break;

							case FILE_IS_MPC:
								if( Bits != 64 || Bits != 32 || Bits != 24 || Bits != 16 || Bits != 8 ) {
									if (Bits > 64)						NewBits = 64;
									else if (Bits > 32 && Bits < 64)	NewBits = 32;
									else if (Bits > 24 && Bits < 32)	NewBits = 24;
									else if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 24 : 16;
									else if (Bits > 8 && Bits < 16)		NewBits = (Bits - 8) < 3 ? 16 : 8;
									else if (Bits < 8)					NewBits = 8;
									NewBits = Bits = 16;
									fileconv_change_quantification_with_sox( FALSE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, NewBits );
								}
								else {
									conv_copy_src_to_dest( Info->tmp_wav, Info->tmp_sox );
								}
								// SET WITH FLOAT FORMAT IF Bits == 24 || 32
								if (Bits == 32 || Bits == 24) {
									PtrTabArgs = conv_with_sox_float_get_param(
											Info->tmp_sox,
											Info->tmp_sox_24
											);
									conv_to_convert( PtrTabArgs, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
									PtrTabArgs = conv_RemoveTab( PtrTabArgs );
									conv_copy_src_to_dest( Info->tmp_sox_24, Info->tmp_sox );
								}
								if( Hertz < 44100 ) {
									PtrTabArgs = conv_AllocTabArgs( &pos );
									PtrTabArgs [ pos++ ] = C_strdup ("mplayer");
									PtrTabArgs [ pos++ ] = C_strdup ("-nojoystick");
									PtrTabArgs [ pos++ ] = C_strdup ("-nolirc");
									PtrTabArgs [ pos++ ] = C_strdup (Info->tmp_sox);
									PtrTabArgs [ pos++ ] = C_strdup ("-ao");
									PtrTabArgs [ pos++ ] = C_strdup ("pcm");
									PtrTabArgs [ pos++ ] = C_strdup ("-ao");
									PtrTabArgs [ pos++ ] = C_strdup_printf ("pcm:file=%s", Info->tmp_sox_24);
									PtrTabArgs [ pos++ ] = C_strdup ("-srate");
									PtrTabArgs [ pos++ ] = C_strdup ("44100");
									PtrTabArgs [ pos++ ] = NULL;
									conv_to_convert( PtrTabArgs, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV");
									PtrTabArgs = conv_RemoveTab( PtrTabArgs );
									conv_copy_src_to_dest( Info->tmp_sox_24, Info->tmp_sox );
								}
								PtrTabArgs = conv_get_command_line(
									MPPENC_WAV_TO_MPC,
									Info->tmp_sox,
									Info->tmp_mpc,
									(Detail.BoolNoTag == TRUE) ? NULL : info_tags,
									Detail.Op_mpc
									);
								conv_to_convert( PtrTabArgs, MPPENC_WAV_TO_MPC, "MPPENC_WAV_TO_MPC");
								PtrTabArgs = conv_RemoveTab( PtrTabArgs );
								break;

							case FILE_IS_MP3:
								if( Info->type_infosong_file_is == FILE_IS_VID_M4A ||
									Info->type_infosong_file_is == FILE_IS_M4A ) {
									NewBits = 16;
									fileconv_change_quantification_with_sox( FALSE, Info->tmp_wav, Info->tmp_sox, 44100, 2, 16 );
								}
								else if( Hertz == 48000 || Bits == 32 ) {
									PtrTabArgs = conv_with_sox_float_get_param(
											Info->tmp_wav,
											Info->tmp_sox
											);
									conv_to_convert( PtrTabArgs, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
									PtrTabArgs = conv_RemoveTab( PtrTabArgs );
								} else  {
									// CORRECTION
									// http://forum.ubuntu-fr.org/viewtopic.php?pid=3271586#p3271586
									// joelab07
									if (Bits < 8)				NewBits = 8;
									if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 16 : 24;
									if (Bits > 24 && Bits < 32)	NewBits = (Bits - 24) < 3 ? 24 : 32;
									NewBits = Bits;
									fileconv_change_quantification_with_sox( FALSE, Info->tmp_wav, Info->tmp_sox, Hertz, Channels, NewBits );
								}
								PtrTabArgs = conv_get_command_line(
									LAME_WAV_TO_MP3,
									Info->tmp_sox,
									Info->tmp_mp3,
									(Detail.BoolNoTag == TRUE) ? NULL : info_tags,
									Detail.Op_mp3
									);
								conv_to_convert( PtrTabArgs, LAME_WAV_TO_MP3, "LAME_WAV_TO_MP3");
								PtrTabArgs = conv_RemoveTab( PtrTabArgs );

								if( TRUE == Detail.bool_normalize )
									Info->path_is_normalize = C_strdup( Info->tmp_mp3 );
								break;
						}
					}
				}

				if( NULL != OutputDir ) {
					free( OutputDir );
					OutputDir = NULL;
				}
			}
			list = C_list_next( list );

			if( NULL != Info->tmp_wav ) {		free( Info->tmp_wav );		Info->tmp_wav = NULL;		}
			if( NULL != Info->tmp_sox ) {		free( Info->tmp_sox );		Info->tmp_sox  = NULL;		}
			if( NULL != Info->tmp_flac ) {		free( Info->tmp_flac );		Info->tmp_flac = NULL;		}
			if( NULL != Info->tmp_ape ) {		free( Info->tmp_ape );		Info->tmp_ape = NULL;		}
			if( NULL != Info->tmp_wavpack ) {	free( Info->tmp_wavpack );	Info->tmp_wavpack = NULL;	}
			if( NULL != Info->tmp_sox_24 ) {	free( Info->tmp_sox_24 );	Info->tmp_sox_24 = NULL;	}
			if( NULL != Info->tmp_ogg ) {		free( Info->tmp_ogg );		Info->tmp_ogg = NULL;		}
			if( NULL != Info->tmp_m4a ) {		free( Info->tmp_m4a );		Info->tmp_m4a = NULL;		}
			if( NULL != Info->tmp_aac ) {		free( Info->tmp_aac );		Info->tmp_aac = NULL;		}
			if( NULL != Info->tmp_mpc ) {		free( Info->tmp_mpc );		Info->tmp_mpc = NULL;		}
			if( NULL != Info->tmp_mp3 ) {		free( Info->tmp_mp3 );		Info->tmp_mp3 = NULL;		}
			if( NULL != basename ) {			free( basename );			basename = NULL;			}
			if( NULL != OutputDir ) {			free( OutputDir );			OutputDir = NULL;			}
		}
		conv.TmpRep  = utils_remove_temporary_rep( conv.TmpRep );
	}
}


