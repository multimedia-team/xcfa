/*
 *  file      : src/tags_ape.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "get_info.h"
#include "tags.h"
#include <taglib/tag_c.h>


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

/*****************************************************************************************
APE header that all APE files have in common (old and new)
*****************************************************************************************
struct APE_COMMON_HEADER
{
    char cID[4];                            should equal 'MAC '
    uint16 nVersion;                        version number * 1000 (3.81 = 3810)
};

*****************************************************************************************
APE header structure for old APE files (3.97 and earlier)
*****************************************************************************************
struct APE_HEADER_OLD
{
    char cID[4];                            should equal 'MAC '
    uint16 nVersion;                        version number * 1000 (3.81 = 3810)
    uint16 nCompressionLevel;               the compression level
    uint16 nFormatFlags;                    any format flags (for future use)
    uint16 nChannels;                       the number of channels (1 or 2)
    uint32 nSampleRate;                     the sample rate (typically 44100)
    uint32 nHeaderBytes;                    the bytes after the MAC header that compose the WAV header
    uint32 nTerminatingBytes;               the bytes after that raw data (for extended info)
    uint32 nTotalFrames;                    the number of frames in the file
    uint32 nFinalFrameBlocks;               the number of samples in the final frame
};
*****************************************************************************************/



/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/

INFO_APE *tagsape_remove_info (INFO_APE *info)
{
	if (info) {
		if (NULL != info->time)		{ free (info->time); info->time = NULL;	}
		if (NULL != info->size)		{ free (info->size);info->size = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_APE *)NULL);
}


void tags_ape_print( INFO_APE *ptrinfo )
{
	printf( "time            = %s\n", ptrinfo->time );
	printf( "SecTime         = %d\n", ptrinfo->SecTime );
	printf( "size            = %s\n", ptrinfo->size );
}


INFO_APE *tagsape_get_info( INFO *p_info )
{
	INFO_APE	*ptrinfo = NULL;
	SHNTAG		*ShnTag = GetInfo_shntool ( p_info->path_name_file );
	int		m;
	int		s;
	int		sec;

	ptrinfo = (INFO_APE *)malloc (sizeof (INFO_APE));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc ();

	ptrinfo->size     = C_strdup (ShnTag->size);
	ptrinfo->SecTime =
	sec = ShnTag->SecTime;
	s = sec % 60; sec /= 60;
	m = sec % 60; sec /= 60;
	if (sec > 0) ptrinfo->time = C_strdup_printf ("%02d:%02d:%02d", sec, m, s);
	else         ptrinfo->time = C_strdup_printf ("00:%02d:%02d", m, s);

	tags_complementation( ptrinfo->tags,  p_info->path_name_file );

	ShnTag = GetInfo_free_shntool (ShnTag);

	return ((INFO_APE *)ptrinfo);
}
