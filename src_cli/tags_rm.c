/*
 *  file      : src/tags_rm.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>




/*
*---------------------------------------------------------------------------
* REMOVE HEADER
*---------------------------------------------------------------------------
*/
INFO_RM *tagsrm_remove_info (INFO_RM *info)
{
	if (info) {

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}
	return ((INFO_RM *)NULL);
}

/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/
INFO_RM *tagsrm_get_info( INFO *p_info )
{
	INFO_RM     *ptrinfo = NULL;

	ptrinfo = (INFO_RM *)malloc (sizeof (INFO_RM));
	if (ptrinfo == NULL) return (NULL);

	ptrinfo->tags = (TAGS *)tags_alloc ();
	tags_complementation( ptrinfo->tags,  p_info->path_name_file );

	return ((INFO_RM *)ptrinfo);
}









