/*
 *  file      : src/prg_init.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <string.h>
#include <stdio.h>

#include "global.h"
#include "prg_init.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/



PRGINIT PrgInit;


TABLEAU_PRG_EXTERN TableauPrgExtern [ NMR_MAX_TABLEAU ] = {

{
"a52dec",								// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_a52dec,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Decode ATSC A/52 audio streams")
},
{
"aacplusenc",							// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_aacplusenc,				// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" AAC+ encoder")
},
{
"mp3check",								// NAME1
"checkmp3",								// NAME2
"mp3_check",							// NAME3
&PrgInit.bool_checkmp3,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Cherche des renseignements sur les formats mp3")
},
{
"faac",									// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_faac,						// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Audio Codeur freeware")
},
{
"faad",									// NAME1
"faad2",								// NAME2
"",										// NAME3
&PrgInit.bool_faad,						// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" MPEG-4 AAC decodeur")
},
{
"flac",									// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_flac,						// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Conversion wav : flac")
},
{
"lame",									// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_lame,						// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Conversion wav : mp3")
},
// ADD
{
"mac",									// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_ape,						// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Monkey's Audio Console Front End : APE")
},
// ADD TO amd64 and i386
{
"mpcdec",								// NAME1
"mppdec",								// NAME2
"mpc123",								// NAME3
&PrgInit.bool_mpc123_mppdec,			// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" MusePack commandline utilities")
},
{
"mpcenc",								// NAME1
"mppenc",								// NAME2
"",										// NAME3
&PrgInit.bool_mppenc,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" MusePack commandline utilities")
},
{
"mplayer",								// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_mplayer,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Lecteur et extracteur")
},
{
"mp3gain",								// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_mp3gain,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Replaygain pour les fichiers mp3")
},
{
"normalize-audio",						// NAME1
"normalize",							// NAME2
"",										// NAME3
&PrgInit.bool_normalize,				// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Normaliseur de fichier wav")
},
{
"libnotify-bin",						// NAME1
"notify-send",							// NAME2
"",										// NAME3
&PrgInit.bool_notify_send,				// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" A program to send desktop notifications")
},
{
"shorten",								// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_shorten,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Forte compression au format wave")
},
{
"shntool",								// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_shntool,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Decoupage de fichiers sans decodage")
},
{
"sox",									// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_sox,						// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Transformation universelle de fichiers son")
},
{
"oggenc",								// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_oggenc,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Conversion wav : ogg")
},
{
"vorbisgain",							// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_vorbisgain,				// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" Replaygain pour les fichiers ogg")
},
{
"wavpack",								// NAME1
"",										// NAME2
"",										// NAME3
&PrgInit.bool_wavpack,					// FOUND
NULL,									// PTR ON Name 1 | 2 | 3
gettext_noop(" WAVPACK  Hybrid Lossless Audio Compressor")
}
};



// GET INFOS FROM STRUCT
//
char *prginit_get_name (TYPE_PROGINIT TypeEnum)
{
	return ((char *)TableauPrgExtern [ TypeEnum ] . PtrName);
}
//
//
char *prginit_get_Description (TYPE_PROGINIT TypeEnum)
{
	return (gettext((char *)TableauPrgExtern [ TypeEnum ] . Description));
}
//
//
C_BOOL prginit_elem_is_present (TYPE_PROGINIT TypeEnum)
{
	return ((C_BOOL)*TableauPrgExtern [ TypeEnum ] . BoolFound);
}
//
//
void prginit_print_info (void)
{
	int	index;

	// printf ("!------------------------------\n");
	printf ("! INSTALL      NAME             DESCRIPTION\n");
	printf( "!-------------------------------------------------------------\n" );
	for (index = 0; index < NMR_MAX_TABLEAU; index ++)
		printf(
			"!%s  %s   %-12s    %s%s\n",
			(*TableauPrgExtern [ index ] . BoolFound == TRUE) ?	RESET : BOLD_RED,
			(*TableauPrgExtern [ index ] . BoolFound == TRUE) ? "Yes      " : " NO  --> ",
			TableauPrgExtern [ index ] . PtrName,
			TableauPrgExtern [ index ] . Description,
			RESET
				);
	// printf ("!------------------------------\n\n");
}
//
//
void prginit_scan (void)
{
	int	index;

	for (index = 0; index < NMR_MAX_TABLEAU; index ++) {

		// INIT VAR

		*TableauPrgExtern [ index ] . BoolFound = FALSE;
		TableauPrgExtern [ index ] . PtrName    = NULL;

		// CHERCHE LE NOM APPROPRIE SUIVANT LA DISTRIBUTION UTILISEE

		*TableauPrgExtern [ index ] . BoolFound = C_find_program_in_path (TableauPrgExtern [ index ] . Name1);
		TableauPrgExtern [ index ] . PtrName = TableauPrgExtern [ index ] .Name1;

		if (*TableauPrgExtern [ index ] . BoolFound == FALSE) {
			if (*TableauPrgExtern [ index ] . Name2 != '\0') {
				if ((*TableauPrgExtern [ index ] . BoolFound = C_find_program_in_path (TableauPrgExtern [ index ] . Name2)) == TRUE) {
					TableauPrgExtern [ index ] . PtrName = TableauPrgExtern [ index ] .Name2;
				}
			}
		}

		if (*TableauPrgExtern [ index ] . BoolFound == FALSE) {
			if (*TableauPrgExtern [ index ] . Name3 != '\0') {
				if ((*TableauPrgExtern [ index ] . BoolFound = C_find_program_in_path (TableauPrgExtern [ index ] . Name3)) == TRUE) {
					TableauPrgExtern [ index ] . PtrName = TableauPrgExtern [ index ] .Name3;
				}
			}
		}
	}
}


