/*
 *  file      : src/clib.h
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 *
 * ---
 * This code is to prevent the use of the glibc library.
 * Ce code est destiné à éviter l'utilisation de la librairie glibc.
 * ---
 */


#ifndef clib_h
#define clib_h 1

// definition d'un nouveau type
typedef enum { FALSE=0, TRUE } C_BOOL;

typedef struct {
	char	*str;
	int		len;
} CString;

typedef struct _clist CList;
struct _clist {
	CList	*previous;
	CList	*next;
	void	*data;
};

CList	*C_list_first( CList *p_clist  );
CList	*C_list_last( CList *p_clist );
CList	*C_list_previous( CList *p_clist );
CList	*C_list_next( CList *p_clist );
CList	*C_list_append( CList *p_clist, void *p_alloc );
CList	*C_list_free( CList *p_clist );
int		C_list_length( CList *p_clist );

char	*C_strdup( char *p_str );
char	*C_strdup_printf( const char *fmt, ... );

CString	*C_string_new( void );
CString	*C_string_free( CString *p_cstring );
CString	*C_string_append_printf( CString *p_cstring, const char *fmt, ... );

C_BOOL	C_dir_test( char *p_dir );
C_BOOL	C_file_test( char *p_file );

C_BOOL	C_mkdir_with_parents( char *p_path );
void	C_rmdir( char *p_path );

char	**C_strsplit( const char *string, const char *delimiter );
char	**C_strfreev( char **p_Larrbuf );

char	*C_path_get_basename( char *p_path );

char	*C_ascii_strdown( char *p_str );
char	*C_ascii_strup( char *p_str );

C_BOOL	C_find_program_in_path( char *p_find );

#endif
