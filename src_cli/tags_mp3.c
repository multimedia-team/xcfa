/*
 *  file      : src/tags_mp3.c
 *  project   : xcfa_cli
 *  copyright : (C) 2014 by BULIN Claude
 *
 *  This file is part of xcfa_cli project
 *
 *  xcfa_cli is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 *
 *  xcfa_cli is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <taglib/tag_c.h>

#include "global.h"
#include "tags.h"
#include "get_info.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

typedef struct  {
	char	magic[ 3 ];
	char	songname[ 30 ];
	char	artist[ 30 ];
	char	album[ 30 ];
	char	year[ 4 ];
	char	note[ 28 ];
	unsigned char	nnull;
	unsigned char	track;
	unsigned char	style;
} ID3TAG;


/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/
INFO_MP3 *tagsmp3_remove_info (INFO_MP3 *info)
{
	if (NULL != info) {
		if (NULL != info->bitrate)	{ free (info->bitrate);	info->bitrate = NULL;	}
		if (NULL != info->time)		{ free (info->time);		info->time = NULL;	}
		if (NULL != info->size)		{ free (info->size);		info->size = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		free (info);
		info = NULL;
	}

	return ((INFO_MP3 *)NULL);
}
/*
MPEG-1   layer III sample frequencies (kHz):  32  48  44.1
bitrates (kbit/s): 32 40 48 56 64 80 96 112 128 160 192 224 256 320

MPEG-2   layer III sample frequencies (kHz):  16  24  22.05
bitrates (kbit/s):  8 16 24 32 40 48 56 64 80 96 112 128 144 160

MPEG-2.5 layer III sample frequencies (kHz):   8  12  11.025
bitrates (kbit/s):  8 16 24 32 40 48 56 64 80 96 112 128 144 160
-1
0
1
2
*/
int tagsmp3_type_mpeg (char *namefile)
{
	char        *Lout = NULL;
	char        *ptr = NULL;
#define MAX_STR 256
	static char  str [ MAX_STR + 4 ];
	int          cpt = 0;
	char        *term[] = {"MPV_1", "MPV_2", "MPV_25"};
	int          i;
	CString      	*gstr = NULL;
	int          Ret = NONE_MPEG;	// NONE_MPEG = -1

	if (NULL == (gstr = GetInfo_checkmp3 (namefile))) {
		return (-1);
	}

	Lout = gstr->str;
	for (i = 0; i < 3; i ++) {
		if ((ptr = strstr (Lout, term[ i ]))) {

			/* Init la chaine de stockage */
			for (cpt=0; cpt < MAX_STR; cpt++) str [ cpt ] = '\0';

			/* Passe l'intro */
			while (*ptr != ' ') ptr ++;
			while (*ptr == ' ') ptr ++;

			/* Copie */
			cpt = 0;
			while (*ptr != '\n') {
				if (cpt > MAX_STR) break;
				str [ cpt ++ ] = *ptr ++;
			}
			str [ cpt ] = '\0';

			switch ( i ) {
			case 0 :
				// printf ("----------------------------MPV_1  str=%s\n", str);
				if (str[0] == '1') Ret = MPEG_1;	// MPEG_1 = 0
				break;
			case 1 :
				// printf ("----------------------------MPV_2  str=%s\n", str);
				if (str[0] == '1') Ret = MPEG_2;	// MPEG_2 = 1
				break;
			case 2 :
				// printf ("----------------------------MPV_25 str=%s\n", str);
				if (str[0] == '1') Ret =  MPEG_25;	// MPEG_25 = 2
				break;
			}
			if (Ret > -1) break;
		}
	}
	gstr = C_string_free( gstr );
	Lout = NULL;

	return (Ret);
}


void tags_mp3_print( INFO_MP3 *ptrinfo )
{
	printf( "\ttime        = %s\n", ptrinfo->time );
	printf( "\tbitrate     = %s\n", ptrinfo->bitrate );
	printf( "\tmpeg_is     = %d\n", ptrinfo->mpeg_is );
	printf( "\tsize        = %s\n", ptrinfo->size );
	printf( "\tLevelDbfs   = %d\n", ptrinfo->LevelDbfs );
}


INFO_MP3 *tagsmp3_get_info( INFO *p_info )
{
	INFO_MP3     *ptrinfo = NULL;
	TagLib_File  *file;
	TagLib_Tag   *tag;
	const TagLib_AudioProperties *properties;
	int		m;
	int		s;
	int		sec;

	ptrinfo = (INFO_MP3 *)malloc (sizeof (INFO_MP3));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc ();

	if ((file = taglib_file_new (p_info->path_name_file))) {
		taglib_set_strings_unicode(FALSE);
		tag = taglib_file_tag(file);
		properties = taglib_file_audioproperties(file);

		ptrinfo->tags->Title     = C_strdup (taglib_tag_title(tag));
		ptrinfo->tags->Artist    = C_strdup (taglib_tag_artist(tag));
		ptrinfo->tags->Album     = C_strdup (taglib_tag_album(tag));
		ptrinfo->tags->IntYear   = taglib_tag_year(tag);
		ptrinfo->tags->Year      = C_strdup_printf ("%d", ptrinfo->tags->IntYear);
		ptrinfo->tags->Comment   = C_strdup (taglib_tag_comment(tag));
		ptrinfo->tags->IntNumber = taglib_tag_track(tag);
		ptrinfo->tags->Number    = C_strdup_printf ("%d", ptrinfo->tags->IntNumber);
		ptrinfo->tags->Genre     = C_strdup (taglib_tag_genre(tag));
		ptrinfo->tags->IntGenre  = tags_get_genre_by_value (ptrinfo->tags->Genre);
		ptrinfo->SecTime = sec   = taglib_audioproperties_length(properties);
		s = sec % 60; sec /= 60;
		m = sec % 60; sec /= 60;
		 if (sec > 0) ptrinfo->time = C_strdup_printf ("%02d:%02d:%02d", sec, m, s);
		 else         ptrinfo->time = C_strdup_printf ("00:%02d:%02d", m, s);

		ptrinfo->bitrate  = C_strdup_printf ("%d", taglib_audioproperties_bitrate(properties));
		ptrinfo->mpeg_is  = tagsmp3_type_mpeg ( p_info->path_name_file );
		ptrinfo->size     = C_strdup_printf ("%d Ko", (int)utils_get_size_file ( p_info->path_name_file ) / 1024);

		taglib_tag_free_strings();
		taglib_file_free (file);
	}
	else {
		tags_complementation( ptrinfo->tags,  p_info->path_name_file );
	}
	// ptrinfo->LevelDbfs = GetInfo_level_get_from( FILE_IS_MP3, p_info->path_name_file );

	return ((INFO_MP3 *)ptrinfo);
}






