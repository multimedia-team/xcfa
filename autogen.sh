#!/bin/sh

(aclocal --version) < /dev/null > /dev/null 2>&1 || {
  echo
  echo "** Error **: You must have aclocal installed."
  echo
  exit 1
}

(autoheader --version) < /dev/null > /dev/null 2>&1 || {
  echo
  echo "** Error **: You must have autoheader installed."
  echo
  exit 1
}

(autoconf --version) < /dev/null > /dev/null 2>&1 || {
  echo
  echo "** Error **: You must have autoconf installed."
  echo
  exit 1
}

(automake --version) < /dev/null > /dev/null 2>&1 || {
  echo
  echo "** Error **: You must have aclocal installed."
  echo
  exit 1
}

echo aclocal
aclocal
# autoreconf --install
# AM_GNU_GETTEXT_VERSION
echo autoheader
autoheader
echo autoconf
autoconf
echo automake -a -c
automake -a -c
echo automake --add-missing
automake --add-missing
echo automake
automake
echo ./configure --prefix=/usr
./configure --prefix=/usr
echo 
exit 0
