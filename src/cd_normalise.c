 /*
 *  file      : cd_normalise.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cd_audio.h"
#include "configuser.h"
#include "cd_normalise.h"



typedef struct {
	GList	*ListPeakGroup;			// Pointeur sur AUDIO
	gint	MaxElements;			// 
	gint	NumExtract;				// 
	gchar	*TmpRep;				// Le dossier temporaire
} VAR_CD_NORMALISE;

VAR_CD_NORMALISE VarCdNormalise = {
		NULL,						// GList	*ListPeakGroup
		0,							// gint		MaxElements
		0,							// gint		NumExtract
		NULL						// gchar	*TmpRep
		};



// 
// PEAK GROUP
// 
void CdNormalise_set_list_PeakGroup (CD_AUDIO *Audio)
{
	VAR_CD_NORMALISE_ELEMENT	*VarCDNormaliseElement = NULL;
	
	if (VarCdNormalise.TmpRep == NULL) {
		VarCdNormalise.TmpRep = libutils_create_temporary_rep (Config.PathnameTMP, PATH_TMP_XCFA_CD_NORMALISE_PEAK_ALBUM );
	}
	
	VarCDNormaliseElement = (VAR_CD_NORMALISE_ELEMENT *)g_malloc0 (sizeof (VAR_CD_NORMALISE_ELEMENT));
	VarCDNormaliseElement->PathNameSrc  = g_strdup_printf ("%s/%d.wav", VarCdNormalise.TmpRep, VarCdNormalise.MaxElements);
	VarCDNormaliseElement->PathNameDest = g_strdup (Audio->PathName_Dest_Wav);
	VarCDNormaliseElement->Audio        = Audio;
	VarCdNormalise.ListPeakGroup = g_list_append (VarCdNormalise.ListPeakGroup, VarCDNormaliseElement);
	VarCdNormalise.MaxElements ++;
}
// 
// 
void CdNormalise_add_PeakGroup (void)
{
	VarCdNormalise.NumExtract ++;
}
// 
// 
void CdNormalise_set_list_collectif_remove (void)
{
	GList         			*List = NULL;
	VAR_CD_NORMALISE_ELEMENT	*VarCDNormaliseElement = NULL;

	List = g_list_first (VarCdNormalise.ListPeakGroup);
	while (List) {
		if ((VarCDNormaliseElement = (VAR_CD_NORMALISE_ELEMENT *)List->data) != NULL) {
			g_free (VarCDNormaliseElement->PathNameSrc);
			VarCDNormaliseElement->PathNameSrc = NULL;
			g_free (VarCDNormaliseElement->PathNameDest);
			VarCDNormaliseElement->PathNameDest = NULL;
		}
		List = g_list_next(List);
	}
	g_list_free (VarCdNormalise.ListPeakGroup);
	VarCdNormalise.ListPeakGroup = NULL;
	
	VarCdNormalise.MaxElements = 0;
	VarCdNormalise.NumExtract = 0;
	
	if (VarCdNormalise.TmpRep != NULL) {
		VarCdNormalise.TmpRep = libutils_remove_temporary_rep (VarCdNormalise.TmpRep);
	}
}
// 
// 
gboolean CdNormalise_list_PeakGroup_is_ready (void)
{
	if (VarCdNormalise.MaxElements == VarCdNormalise.NumExtract) return (TRUE);
	return (FALSE);
}
// 
// 
gboolean CdNormalise_get_is_list_PeakGroup (void)
{
	return (VarCdNormalise.ListPeakGroup == NULL ? FALSE : TRUE);
}
// 
// 
GList *CdNormalise_get_list_PeakGroup (void)
{
	return ((GList *)VarCdNormalise.ListPeakGroup);
}
// 
// 
void CdNormalise_set_normalise_ok (void)
{
	GList				*List = NULL;
	VAR_CD_NORMALISE_ELEMENT	*VarCDNormaliseElement = NULL;
	
	List = g_list_first (VarCdNormalise.ListPeakGroup);
	while (List) {
		if (NULL != (VarCDNormaliseElement = (VAR_CD_NORMALISE_ELEMENT *)List->data)) {
			VarCDNormaliseElement->Audio->EtatNormalise = FALSE;
			VarCDNormaliseElement->Audio->EtatPeak = CD_NORM_PEAK_NONE;
		}
		List = g_list_next(List);
	}
	// gtk_combo_box_set_active (GTK_COMBO_BOX (var_cd.Adr_combobox_normalise_cd), 0);
}







