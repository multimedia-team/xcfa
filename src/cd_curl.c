 /*
 *  file      : cd_curl.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <curl/curl.h>
#include <curl/easy.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cd_audio.h"
#include "get_info.h"
#include "cd_cue.h"
#include "configuser.h"
#include "poche.h"
#include "cd_curl.h"


/*
	-> ABANDON DES LIBRAIRIES
		CDDB et CDIO
	
	-> AU PROFIT DE:
	
	-- Utilisation de:
	
	$ cd-discid
	6f11b918 24 150 20835 32512 47955 62530 74420 89332 100575 113692 126522 133245 145330 159460 170647 186450 199462 219450 233727 247745 262270 276072 288807 310705 325995 4539
	
	pour la valeur DiscId ( par exemple 6f11b918 )
	et la chaine complete a passer en parametre pour CURL:

	! http://freedb.org/~cddb/cddb.cgi?cmd=cddb+query+6f11b918+24+150+20835+32512+47955+62530+74420+89332+100575+113692+126522+133245+145330+159460+170647+186450+199462+219450+233727+247745+262270+276072+288807+310705+325995+4539&hello=private+free.the.cddb+xcfa+4.0.4~beta7&proto=6

	-- recuperation ( si disponibles ! ) des datas 210 pour le menu pop-up qui permettra un choix des autres bases:
	
	Result = 
	210 Found exact matches, list follows (until terminating `.')
	jazz 6f11b918 Frank Sinatra / My Way - The Best Of Frank Sinatra Disc 1
	misc 6f11b918 Frank Sinatra / My Way - The Best Of Frank Sinatra (CD 1)
	rock 6f11b918 Frank Sinatra / My Way The Best Of Frank Sinatra
	blues 6f11b918 Frank Sinatra / My Way The Best Of Frank Sinatra
	classical 6f11b918 Frank Sinatra / My Way The Best Of Frank Sinatra
	.
	
	-- recup du complement d'informations de la base avec:
	
	! http://freedb.org/~cddb/cddb.cgi?cmd=cddb+read+jazz+6f11b918&hello=private+free.the.cddb+xcfa+4.0.4~beta7&proto=6
	
	-- Utilisation de:
	
	CDPARANOIA
		pour le module cd_cue.c
	
	
cdparanoia III release 10.2 (September 11, 2008)
Table of contents (audio tracks only):
track        length               begin        copy pre ch
===========================================================
  1.    20685 [04:35.60]        0 [00:00.00]    no   no  2
  2.    11677 [02:35.52]    20685 [04:35.60]    no   no  2
  3.    15443 [03:25.68]    32362 [07:11.37]    no   no  2
  4.    14575 [03:14.25]    47805 [10:37.30]    no   no  2
  5.    11890 [02:38.40]    62380 [13:51.55]    no   no  2
  6.    14912 [03:18.62]    74270 [16:30.20]    no   no  2
  7.    11243 [02:29.68]    89182 [19:49.07]    no   no  2
  8.    13117 [02:54.67]   100425 [22:19.00]    no   no  2
  9.    12830 [02:51.05]   113542 [25:13.67]    no   no  2
 10.     6723 [01:29.48]   126372 [28:04.72]    no   no  2
 11.    12085 [02:41.10]   133095 [29:34.45]    no   no  2
 12.    14130 [03:08.30]   145180 [32:15.55]    no   no  2
 13.    11187 [02:29.12]   159310 [35:24.10]    no   no  2
 14.    15803 [03:30.53]   170497 [37:53.22]    no   no  2
 15.    13012 [02:53.37]   186300 [41:24.00]    no   no  2
 16.    19988 [04:26.38]   199312 [44:17.37]    no   no  2
 17.    14277 [03:10.27]   219300 [48:44.00]    no   no  2
 18.    14018 [03:06.68]   233577 [51:54.27]    no   no  2
 19.    14525 [03:13.50]   247595 [55:01.20]    no   no  2
 20.    13802 [03:04.02]   262120 [58:14.70]    no   no  2
 21.    12735 [02:49.60]   275922 [61:18.72]    no   no  2
 22.    21898 [04:51.73]   288657 [64:08.57]    no   no  2
 23.    15290 [03:23.65]   310555 [69:00.55]    no   no  2
 24.    14482 [03:13.07]   325845 [72:24.45]    no   no  2
TOTAL  340327 [75:37.52]    (audio only)
	
  ------------------ B A S E ----------------------------       ---------------- E X T R A C T I O N -----------------
   1. length   20685 [04:35:60]  begin       0 [00:00:00]	cdparanoia [.0]-[.20684] -d /dev/sr1 -O 0 "1.wav"
   2. length   11677 [02:35:52]  begin   20685 [04:35:60]	cdparanoia [.20685]-[.11676] -d /dev/sr1 -O 0 "2.wav"
   3. length   15443 [03:25:68]  begin   32362 [07:11:37]	cdparanoia [.32362]-[.15442] -d /dev/sr1 -O 0 "3.wav"
   4. length   14575 [03:14:25]  begin   47805 [10:37:30]	cdparanoia [.47805]-[.14574] -d /dev/sr1 -O 0 "4.wav"
   5. length   11890 [02:38:40]  begin   62380 [13:51:55]	cdparanoia [.62380]-[.11889] -d /dev/sr1 -O 0 "5.wav"
   6. length   14912 [03:18:62]  begin   74270 [16:30:20]	cdparanoia [.74270]-[.14911] -d /dev/sr1 -O 0 "6.wav"
   7. length   11243 [02:29:68]  begin   89182 [19:49:07]	cdparanoia [.89182]-[.11242] -d /dev/sr1 -O 0 "7.wav"
   8. length   13117 [02:54:67]  begin  100425 [22:19:00]	cdparanoia [.100425]-[.13116] -d /dev/sr1 -O 0 "8.wav"
   9. length   12830 [02:51:05]  begin  113542 [25:13:67]	cdparanoia [.113542]-[.12829] -d /dev/sr1 -O 0 "9.wav"
  10. length    6723 [01:29:48]  begin  126372 [28:04:72]	cdparanoia [.126372]-[.6722] -d /dev/sr1 -O 0 "10.wav"
  11. length   12085 [02:41:10]  begin  133095 [29:34:45]	cdparanoia [.133095]-[.12084] -d /dev/sr1 -O 0 "11.wav"
  12. length   14130 [03:08:30]  begin  145180 [32:15:55]	cdparanoia [.145180]-[.14129] -d /dev/sr1 -O 0 "12.wav"
  13. length   11187 [02:29:12]  begin  159310 [35:24:10]	cdparanoia [.159310]-[.11186] -d /dev/sr1 -O 0 "13.wav"
  14. length   15803 [03:30:53]  begin  170497 [37:53:22]	cdparanoia [.170497]-[.15802] -d /dev/sr1 -O 0 "14.wav"
  15. length   13012 [02:53:37]  begin  186300 [41:24:00]	cdparanoia [.186300]-[.13011] -d /dev/sr1 -O 0 "15.wav"
  16. length   19988 [04:26:38]  begin  199312 [44:17:37]	cdparanoia [.199312]-[.19987] -d /dev/sr1 -O 0 "16.wav"
  17. length   14277 [03:10:27]  begin  219300 [48:44:00]	cdparanoia [.219300]-[.14276] -d /dev/sr1 -O 0 "17.wav"
  18. length   14018 [03:06:68]  begin  233577 [51:54:27]	cdparanoia [.233577]-[.14017] -d /dev/sr1 -O 0 "18.wav"
  19. length   14525 [03:13:50]  begin  247595 [55:01:20]	cdparanoia [.247595]-[.14524] -d /dev/sr1 -O 0 "19.wav"
  20. length   13802 [03:04:02]  begin  262120 [58:14:70]	cdparanoia [.262120]-[.13801] -d /dev/sr1 -O 0 "20.wav"
  21. length   12735 [02:49:60]  begin  275922 [61:18:72]	cdparanoia [.275922]-[.12734] -d /dev/sr1 -O 0 "21.wav"
  22. length   21898 [04:51:73]  begin  288657 [64:08:57]	cdparanoia [.288657]-[.21897] -d /dev/sr1 -O 0 "22.wav"
  23. length   15290 [03:23:65]  begin  310555 [69:00:55]	cdparanoia [.310555]-[.15289] -d /dev/sr1 -O 0 "23.wav"
  24. length   14482 [03:13:07]  begin  325845 [72:24:45]	cdparanoia [.325845]-[.14481] -d /dev/sr1 -O 0 "24.wav"
TOTAL  340327 [75:37:52]  

*/

typedef struct {
	gchar		*NameGenre;			// jazz || rock || ...
	gint		NumCdDiscId;		// NUM
	gchar		*StrCdDiscId;		// STR
} VAR_LIST_POPUP;

typedef struct {
	gchar		*Title;				// Titre d'une piste
	gchar		StrTime[ 10 ];		// Temps ( duree ) d'une piste mm:ss
	gint		NumSector;			// Numero du secteur corespondant a la piste
} LIST_SECTOR;


typedef struct {
	gchar		*StrDiscID;			// STR
	GString		*GStrCdDiscID;		// STRING
	gint		LenDisc;			// EN SECONDES
	gint		TotalTracks;		// TOTAL TRACKS
	GList		*ListDiscId;		// VAR_LIST_POPUP
	LIST_SECTOR	*ListSector;		// CDPARANOIA
	
	gint		NumForceFromPopUp;	// Pour le reaffichage du popup
	gchar		*CallStrDiscID;		// 
	gchar		*CallStrGenre;		// 
} VAR_SECTOR;

VAR_SECTOR	VarSector;

typedef enum {						// DO_CURL_QUERY CdCurl_discdb_do_query( void )
	DO_CURL_QUERY_FALSE = 0,		// NO QUERY
	DO_CURL_QUERY_TRUE,				// QUERY IS OK
	DO_CURL_QUERY_REPEAT			// 211 Found inexact matches
} DO_CURL_QUERY;




// 
// 
void CdCurl_init_entete_cd (void)
{
	if( NULL != EnteteCD.TitleCD ) {
		g_free( EnteteCD.TitleCD );
		EnteteCD.TitleCD = NULL;
	}
	if( NULL != EnteteCD.Title ) {
		g_free( EnteteCD.Title );
		EnteteCD.Title = NULL;
	}
	if( NULL != EnteteCD.Artiste ) {
		g_free( EnteteCD.Artiste );
		EnteteCD.Artiste = NULL;
	}
	if( NULL != EnteteCD.StrDureeCd ) {
		g_free( EnteteCD.StrDureeCd );
		EnteteCD.StrDureeCd = NULL;
	}
	if( NULL != EnteteCD.StrGenre ) {
		g_free( EnteteCD.StrGenre );
		EnteteCD.StrGenre = NULL;
	}
	if( NULL != EnteteCD.StrYear ) {
		g_free( EnteteCD.StrYear );
		EnteteCD.StrYear = NULL;
	}
	
	cdaudio_deallocate_glist();
	
	if( NULL != EnteteCD.Message ) {
		g_free( EnteteCD.Message );
		EnteteCD.Message = NULL;
	}
}
//
//
void CdCurl_remove( void )
{
	if( NULL != VarSector.StrDiscID ) {
		g_free( VarSector.StrDiscID );
		VarSector.StrDiscID = NULL;
	}
	if( NULL != VarSector.GStrCdDiscID ) {
		g_string_free( VarSector.GStrCdDiscID, TRUE );
		VarSector.GStrCdDiscID = NULL;
	}
	// REMOVE PART THREE
	if( NULL != VarSector.ListSector ) {
		gint	Track;
		
		for( Track = 0; Track < VarSector.TotalTracks; Track ++ ) {
			if( NULL != VarSector.ListSector[ Track ].Title ) {
				g_free( VarSector.ListSector[ Track ].Title ); 
				VarSector.ListSector[ Track ].Title = NULL;	
			}
		}
		g_free( VarSector.ListSector );
		VarSector.ListSector = NULL;
	}
}
//
//
void CdCurl_remove_struct( void )
{
	if( NULL != VarSector.ListDiscId ) {
		GList		*List = NULL;
		VAR_LIST_POPUP	*PtrVarList = NULL;
		
		List = g_list_first( VarSector.ListDiscId );
		while ( List ) {
			if( NULL != ( PtrVarList = (VAR_LIST_POPUP *)List->data )) {
				if( NULL != PtrVarList->NameGenre ) {
					g_free( PtrVarList->NameGenre );
					PtrVarList->NameGenre = NULL;
				}
				if( NULL != PtrVarList->StrCdDiscId ) {
					g_free( PtrVarList->StrCdDiscId );
					PtrVarList->StrCdDiscId = NULL;
				}
				g_free( PtrVarList );
				PtrVarList = List->data = NULL;
			}
			List = g_list_next( List );
		}
		g_list_free( VarSector.ListDiscId );
		VarSector.ListDiscId = NULL;
	}
}
//
//
void CdCurl_remove_struct_all( void )
{
	CdCurl_remove();
	CdCurl_remove_struct();
	if( NULL != VarSector.CallStrDiscID ) {
		g_free( VarSector.CallStrDiscID );
		VarSector.CallStrDiscID = NULL;
	}
	if( NULL != VarSector.CallStrGenre ) {
		g_free( VarSector.CallStrGenre );
		VarSector.CallStrGenre = NULL;
	}
}
//
//
gboolean CdCurl_test_access_web (void)
{
	if (libutils_find_file ("wget") == FALSE) {
		g_print ("WGET absent !\n");
		return (FALSE);
	}
	return (GetInfo_wget ());
}
//
//
gchar *CdCurl_get_title_cd( void )
{
	return( view.TitleCD );
}
// 
// 
void CdCurl_set_list_discid( void )
{
	if( NULL != VarSector.ListDiscId ) {
		GList		*List = NULL;
		VAR_LIST_POPUP	*PtrVarList = NULL;
		gint		NbrOptions = 0;

		libcombo_remove_options( GTK_COMBO_BOX(var_cd.Adr_combobox_discid_cd));
		gtk_combo_box_set_active( GTK_COMBO_BOX(var_cd.Adr_combobox_discid_cd), 0 );

		List = g_list_first( VarSector.ListDiscId );
		while ( List ) {
			if( NULL != ( PtrVarList = (VAR_LIST_POPUP *)List->data )) {
				gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_cd.Adr_combobox_discid_cd), PtrVarList->NameGenre );
				NbrOptions ++;
			}
			List = g_list_next( List );
		}
		gtk_combo_box_set_active( GTK_COMBO_BOX(var_cd.Adr_combobox_discid_cd), VarSector.NumForceFromPopUp > -1 ? VarSector.NumForceFromPopUp : 0 );
		// gtk_widget_show( GTK_WIDGET (GLADE_GET_OBJECT("frame_discid")));
		if( NbrOptions > 1 )
			gtk_widget_show( GTK_WIDGET (GLADE_GET_OBJECT("frame_discid")));
		else	gtk_widget_hide( GTK_WIDGET (GLADE_GET_OBJECT("frame_discid")));
		
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print( "CdCurl_set_list_discid()\n" );
			g_print( "\tNbrOptions = %d [ %s ]\n", NbrOptions,  NbrOptions > 1 ? "gtk_widget_show" : "gtk_widget_hide" );
		}
		
		return;
	}
	gtk_widget_hide( GTK_WIDGET (GLADE_GET_OBJECT("frame_discid")));
}
//
//
void CdCurl_set_call( gint p_num_from_popup, gchar *p_CallStrDiscID, gchar *p_CallStrGenre )
{
	VarSector.NumForceFromPopUp = p_num_from_popup;
	// CLEAR
	if( NULL != VarSector.CallStrDiscID ) {
		g_free( VarSector.CallStrDiscID );
		VarSector.CallStrDiscID = NULL;
	}
	if( NULL != VarSector.CallStrGenre ) {
		g_free( VarSector.CallStrGenre  );
		VarSector.CallStrGenre  = NULL;
	}
	// SET
	if( p_num_from_popup > -1 ) {
	
		GList		*List = NULL;
		VAR_LIST_POPUP	*PtrVarList = NULL;
		
		if( NULL != ( List = g_list_nth( VarSector.ListDiscId, p_num_from_popup ))) {
			if( NULL != ( PtrVarList = (VAR_LIST_POPUP *)List->data )) {
				if( NULL != PtrVarList->StrCdDiscId ) {
					VarSector.CallStrDiscID = g_strdup( PtrVarList->StrCdDiscId );
					// DEBUG: TO SEE OTHER DsicId
					// VarSector.CallStrDiscID = g_strdup( "c60a1a0f" ); 
					
					VarSector.CallStrGenre = g_strdup( PtrVarList->NameGenre );
				}
			}
		}
	}
	if( NULL != p_CallStrDiscID && NULL != p_CallStrGenre ) {
		VarSector.CallStrDiscID = g_strdup( p_CallStrDiscID );
		VarSector.CallStrGenre  = g_strdup( p_CallStrGenre );
	}
}
//
//
gchar *CdCurl_get_str_genre( void )
{
	if( NULL == VarSector.CallStrGenre ) {
	
		GList		*List = NULL;
		VAR_LIST_POPUP	*PtrVarList = NULL;
		
		List = g_list_first( VarSector.ListDiscId );
		while ( List ) {
			if( NULL != ( PtrVarList = (VAR_LIST_POPUP *)List->data )) {
				if( NULL != PtrVarList->StrCdDiscId ) {
					VarSector.CallStrGenre = g_strdup( PtrVarList->NameGenre );
					break;
				}
			}
			List = g_list_next( List );
		}
	}
	return( VarSector.CallStrGenre );
}
//
//
gchar *CdCurl_get_str_discid( void )
{
	if( NULL == VarSector.CallStrDiscID ) {
	
		GList		*List = NULL;
		VAR_LIST_POPUP	*PtrVarList = NULL;
		
		List = g_list_first( VarSector.ListDiscId );
		while ( List ) {
			if( NULL != ( PtrVarList = (VAR_LIST_POPUP *)List->data )) {
				if( NULL != PtrVarList->StrCdDiscId ) {
					VarSector.CallStrDiscID = g_strdup( PtrVarList->StrCdDiscId );
					break;
				}
			}
			List = g_list_next( List );
		}
	}
	if( NULL == VarSector.CallStrDiscID ) {
		VarSector.CallStrDiscID = g_strdup( VarSector.StrDiscID );
	}
	return( VarSector.CallStrDiscID );
}
//
//
void CdCurl_print_glist( void )
{
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		
		GList		*List = NULL;
		VAR_LIST_POPUP	*PtrVarList = NULL;
	
		PRINT_FUNC_LF();

		List = g_list_first( VarSector.ListDiscId );
		while( List ) {
			if( NULL != ( PtrVarList = (VAR_LIST_POPUP *)List->data )) {
				g_print( "%10x", PtrVarList->NumCdDiscId );
				g_print( "\t%s", PtrVarList->StrCdDiscId );
				g_print( "\t%s\n", PtrVarList->NameGenre );
			}
			List = g_list_next( List );
		}
	}
}
//
//
VAR_LIST_POPUP *CdCurl_alloc_list( gchar *p_str_discid, gchar *p_name_genre  )
{
	VAR_LIST_POPUP	*PtrVarList = NULL;
	GList		*List = NULL;
	gint		NumDiscID = strtoul( p_str_discid, NULL, 16 );
	
	// CHERCHE SI DOUBLON AVEC LE DISC_ID
	List = g_list_first( VarSector.ListDiscId );
	while( List ) {
		if( NULL != ( PtrVarList = (VAR_LIST_POPUP *)List->data )) {
			if( NumDiscID == PtrVarList->NumCdDiscId ) {
				// return( (VAR_LIST_POPUP *)NULL );
			}
		}
		List = g_list_next( List );
	}
	// ALLOCATION
	PtrVarList = (VAR_LIST_POPUP *)g_malloc0( sizeof( VAR_LIST_POPUP ));
	PtrVarList->NameGenre    = g_strdup( p_name_genre );
	PtrVarList->NumCdDiscId  = NumDiscID;
	PtrVarList->StrCdDiscId  = g_strdup( p_str_discid );
	VarSector.ListDiscId = g_list_append (VarSector.ListDiscId, PtrVarList );
	
	return( (VAR_LIST_POPUP *)PtrVarList );
}
//
//
GString *CdCurl_make_line_request_for_query( gchar *p_StrDiscID, gchar *p_GStrCdDiscID )
{
	GString	*Gstr = g_string_new( NULL );
	g_string_append_printf(
			Gstr,
			// http://freedb.org/~cddb/cddb.cgi?cmd=cddb+query+6f11b918+24+150+20835+32512+47955+62530+74420+89332+100575+113692+126522+133245+145330+159460+170647+186450+199462+219450+233727+247745+262270+276072+288807+310705+325995+4539&hello=private+free.the.cddb+xcfa+4.0.4~beta7&proto=6
			"http://freedb.org/~cddb/cddb.cgi?cmd=cddb+query+%s+%s&hello=private+free.the.cddb+%s+%s&proto=6",
			p_StrDiscID,
			p_GStrCdDiscID,
			PACKAGE,
			VERSION
			);
	return( Gstr );
}
//
//
GString *CdCurl_make_line_request_for_read( gchar *p_MusicType, gchar *p_StrDiscID )
{
	GString	*Gstr = g_string_new( NULL );
	g_string_append_printf(
			Gstr,
			// "http://freedb.org/~cddb/cddb.cgi?cmd=cddb+read+jazz+6f11b918&hello=private+free.the.cddb+xcfa+4.0.4~beta7&proto=6",
			"http://freedb.org/~cddb/cddb.cgi?cmd=cddb+read+%s+%s&hello=private+free.the.cddb+%s+%s&proto=6",
			p_MusicType,
			p_StrDiscID,
			PACKAGE,
			VERSION
			);
	return( Gstr );
}
// 
// CALL WITH:
// 	CdCurl_make_line_request_for_query(...)
//	OR
// 	CdCurl_make_line_request_for_read(...)
// 
gchar *CdCurl_disc_db_make_request( gchar *p_Cmd )
{
	gchar			*DatasBuffer = NULL;
	CURL			*curl_handle = NULL;
	struct   curl_slist	*headers = NULL;
	FILE               	*outfile = NULL;
	gint			success = 1;
	glong			response = -1;
	glong			filesize;
	gchar			user_agent[ 300 ];
	
	curl_global_init( CURL_GLOBAL_ALL );
	curl_handle = curl_easy_init();
	curl_easy_setopt( curl_handle, CURLOPT_TIMEOUT, 4 );
	if( NULL != curl_handle ) {
		if( SERVER_PROXY_PARAM == Config.ServeurCddb ) {
			gchar	*StrNamePort = g_strdup_printf( "%s:%s", Config.entry_proxy_server, Config.entry_proxy_port );
			curl_easy_setopt( curl_handle, CURLOPT_PROXY, StrNamePort );
			g_free( StrNamePort );
			StrNamePort = NULL;
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print( "\n" );
				g_print( "!-------------------------------------------------\n" );
				g_print( "! AT THE REQUEST OF @Gounlaf:\n");
				g_print( "!    Gestion du proxy\n" );
				g_print( "!    Management PROXY\n" );
				g_print( "!-------------------------------------------------\n" );
				g_print( "!\tConfig.entry_proxy_server = %s\n", Config.entry_proxy_server ); 
				g_print( "!\tConfig.entry_proxy_port   = %s\n", Config.entry_proxy_port ); 
				g_print( "!-------------------------------------------------\n\n" );
			}
		}
		curl_easy_setopt( curl_handle, CURLOPT_URL, p_Cmd );
		strcpy( user_agent, PACKAGE_STRING );
		headers = curl_slist_append( headers, user_agent );
		curl_easy_setopt( curl_handle, CURLOPT_HTTPHEADER, headers );
		outfile = tmpfile();
		if( outfile ) {
			curl_easy_setopt( curl_handle, CURLOPT_FILE, outfile );
			curl_easy_setopt( curl_handle, CURLOPT_NOSIGNAL, 1 );
			success = curl_easy_perform( curl_handle );
			curl_easy_getinfo( curl_handle, CURLINFO_RESPONSE_CODE, &response );
			if( 0 == success ) {
				filesize = ftell( outfile );
				rewind( outfile );
				DatasBuffer = (gchar *)g_malloc0( filesize +10 );
				fread( DatasBuffer, filesize, 1, outfile );
				DatasBuffer[ filesize +0 ] = '\0';
				DatasBuffer[ filesize +1 ] = '\0';
				DatasBuffer[ filesize +2 ] = '\0';
			}
			fclose( outfile );
		}
		curl_slist_free_all( headers );
		headers = NULL;
		curl_easy_cleanup( curl_handle );
	}
	curl_global_cleanup();
	
	if( NULL != DatasBuffer && NULL != strstr( DatasBuffer, "500 Internal Server Error" )) {
		g_free( DatasBuffer );
		DatasBuffer = NULL;
		PRINT("DatasBuffer -> 500 Internal Server Error" );
	}
	return( DatasBuffer );
}
// 
// 	DO_CURL_QUERY_FALSE = 0,	// NO QUERY
// 	DO_CURL_QUERY_TRUE,		// QUERY IS OK
// 	DO_CURL_QUERY_REPEAT		// 211 Found inexact matches
// 
DO_CURL_QUERY CdCurl_discdb_do_query( void )
{
	GString		*Gstr        = CdCurl_make_line_request_for_query( VarSector.StrDiscID, VarSector.GStrCdDiscID->str );
	gchar		*DatasBuffer = CdCurl_disc_db_make_request( Gstr->str );
	gchar		**Larrbuf = NULL;
	gint		Cpt;
	gint		CodeRetData;
	gboolean	BoolCodeRet = DO_CURL_QUERY_TRUE;
	
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print( "\n" );
		g_print( "GSTR_QUERY = %s\n", Gstr->str );
	}
	g_string_free( Gstr, TRUE );
	Gstr = NULL;
	
	CdCurl_remove_struct();

	if( NULL == DatasBuffer ) {
		return( DO_CURL_QUERY_FALSE );
	}
	Larrbuf = g_strsplit( DatasBuffer, "\n", 0 );
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print( "\nQUERY\n" );
		for( Cpt = 0; Larrbuf[ Cpt ]; Cpt ++ ) {
			g_print( "%s\n", Larrbuf[ Cpt ] );
		}
		g_print( "\n" );
	}
	// TEST DU CODE DE RETOUR
	// ----------------------
	CodeRetData = atoi( DatasBuffer );
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print( "CodeRetData = %d\n", CodeRetData );
	switch( CodeRetData ) {
	case 200 :
		// 200 blues dc0c720f Various / L'âme du blues (CD1)
		// g_print( "strtok( Larrbuf[ 0 ], \" \" ) = %s\n", strtok( Larrbuf[ 0 ], " " ));
		strtok( Larrbuf[ 0 ], " " );
		CdCurl_alloc_list( strtok( NULL, " " ), strtok( NULL, " " ));
		BoolCodeRet = DO_CURL_QUERY_TRUE;
		break;
	case 202 :
		// 202 No match found
		BoolCodeRet = DO_CURL_QUERY_FALSE;
		break;
	case 210 :
		// 210 Found exact matches, list follows (until terminating `.')
		// jazz 6f11b918 Frank Sinatra / My Way - The Best Of Frank Sinatra Disc 1
		// misc 6f11b918 Frank Sinatra / My Way - The Best Of Frank Sinatra (CD 1)
		// rock 6f11b918 Frank Sinatra / My Way The Best Of Frank Sinatra
		// blues 6f11b918 Frank Sinatra / My Way The Best Of Frank Sinatra
		// classical 6f11b918 Frank Sinatra / My Way The Best Of Frank Sinatra
		// .
		for( Cpt = 1; Larrbuf[ Cpt ]; Cpt ++ ) {
			if( '.' == *Larrbuf[ Cpt ] ) break;
			CdCurl_alloc_list( strtok( NULL, " " ), strtok( Larrbuf[ Cpt ], " " ));
		}
		BoolCodeRet = DO_CURL_QUERY_TRUE;
		break;
	case 211 :
		// 211 Found inexact matches, list follows (until terminating `.')
		// classical 3710de15 Enrico Caruso / The Legendary Enrico Caruso 21 Favorite Arias
		CdCurl_set_call( -1, strtok( NULL, " " ), strtok( Larrbuf[ 1 ], " " ));
		g_free( VarSector.StrDiscID );
		VarSector.StrDiscID = NULL;
		VarSector.StrDiscID = g_strdup( VarSector.CallStrDiscID );
		BoolCodeRet = DO_CURL_QUERY_REPEAT;
		break;
	default :
		BoolCodeRet = DO_CURL_QUERY_FALSE;
		break;
	}

	g_strfreev( Larrbuf );
	g_free( DatasBuffer );
	DatasBuffer = NULL;
	
	CdCurl_print_glist();
	
	return( BoolCodeRet );
}
//
//
void CdCurl_set_datas_buffer( GString *gstr_buffer_title,
			      GString *gstr_buffer_title_time,
			      GString *gstr_buffer_artist_title_time )
{
	view.Buffer_none = g_strdup( " " );
	
	view.Buffer_title = g_strdup( gstr_buffer_title->str );
	g_string_free( gstr_buffer_title, TRUE );
	
	view.Buffer_title_time = g_strdup( gstr_buffer_title_time->str );
	g_string_free( gstr_buffer_title_time, TRUE );
	
	view.Buffer_artist_title_time = g_strdup( gstr_buffer_artist_title_time->str );
	g_string_free( gstr_buffer_artist_title_time, TRUE );
}
//
//
gint CdCurl_get_numtrack_next( gchar *StrTitle )
{
	if( ! strncasecmp( StrTitle, "TTITLE", 6 )) {
		
		StrTitle += 6;
		return( atoi( StrTitle ));
	}
	
	return( -1 );
}
// 
//
void CdCurl_discdb_do_read( DO_CURL_QUERY p_DoCurlQuery )
{
	GString		*Gstr = NULL;
	CD_AUDIO	*Audio = NULL;
	gchar		*DatasBuffer = NULL;
	gchar		**Larrbuf = NULL;
	gint		Cpt;
	gint		Track = 0;
	GString 	*gstr_buffer_title             = g_string_new (NULL);
	GString 	*gstr_buffer_title_time        = g_string_new (NULL);
	GString 	*gstr_buffer_artist_title_time = g_string_new (NULL);
	gchar	*PtrNameTrack = NULL;
	
	EnteteCD.StrDureeCd  = g_strdup_printf( "%02d:%02d", 
			BaseIoctl.Datas[ BaseIoctl.TotalTracks ].begin_min,
			BaseIoctl.Datas[ BaseIoctl.TotalTracks ].begin_sec
						);
	EnteteCD.TotalTracks = VarSector.TotalTracks;
	
	if( DO_CURL_QUERY_TRUE == p_DoCurlQuery ) {
		Gstr        = CdCurl_make_line_request_for_read( CdCurl_get_str_genre(), CdCurl_get_str_discid() );
		DatasBuffer = CdCurl_disc_db_make_request( Gstr->str );
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print( "\n" );
			g_print( "GSTR_READ = %s\n", Gstr->str );
		}
		g_string_free( Gstr, TRUE );
		Gstr = NULL;
	}	
	
	if( NULL == DatasBuffer ) {
		EnteteCD.Message = g_strdup( _("A web connection is missing") );
		// MINIMUM D'INFORMATION ET RETOUR
		// SET Track
		for( Track = 0; Track < VarSector.TotalTracks; Track ++ ) {
			
			Audio = ( CD_AUDIO * )g_malloc0( sizeof( CD_AUDIO ));
			Audio->EtatPlay                 = CD_ETAT_PLAY_ATTENTE;
			Audio->Bool_Delette_Flac	= FALSE;
			Audio->Bool_Delette_Wav		= FALSE;
			Audio->Bool_Delette_Mp3		= FALSE;
			Audio->Bool_Delette_Ogg		= FALSE;
			Audio->Bool_Delette_M4a		= FALSE;
			Audio->Bool_Delette_Mpc		= FALSE;
			Audio->Bool_Delette_Ape		= FALSE;
			Audio->Bool_Delette_WavPack	= FALSE;
			Audio->EtatSelection_Flac	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Wav	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Mp3	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Ogg	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_M4a	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Mpc	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Ape	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_WavPack	= CD_ETAT_ATTENTE;
			Audio->Etat_Flac		= ETAT_CONV_CD_NONE;
			Audio->Etat_Wav			= ETAT_CONV_CD_NONE;
			Audio->Etat_Mp3			= ETAT_CONV_CD_NONE;
			Audio->Etat_Ogg			= ETAT_CONV_CD_NONE;
			Audio->Etat_m4a			= ETAT_CONV_CD_NONE;
			Audio->Etat_Mpc			= ETAT_CONV_CD_NONE;
			Audio->Etat_Ape			= ETAT_CONV_CD_NONE;
			Audio->Etat_WavPack		= ETAT_CONV_CD_NONE;
			Audio->EtatNormalise		= CD_NORM_PEAK_NONE;
			Audio->PathName_Dest_Flac	= NULL;
			Audio->PathName_Dest_Wav	= NULL;
			Audio->PathName_Dest_Mp3	= NULL;
			Audio->PathName_Dest_Ogg	= NULL;
			Audio->PathName_Dest_M4a	= NULL;
			Audio->PathName_Dest_Mpc	= NULL;
			Audio->PathName_Dest_Ape	= NULL;
			Audio->PathName_Dest_WavPack	= NULL;
			Audio->Num_Track		= Track + 1;
			Audio->Str_Track		= g_strdup_printf ( "%02d", Track + 1 );
			Audio->Duree			= g_strdup( VarSector.ListSector[ Track ].StrTime );
			Audio->NameSong			= g_strdup_printf ("Track_%02d", Track + 1);
			VarSector.ListSector[ Track ].Title = g_strdup( Audio->NameSong	);
		
			Audio->tags = ( TAGS * )tags_alloc( TRUE );
			Audio->tags->Album		= g_strdup ("Album");
			Audio->tags->Artist		= g_strdup ("Artist");
			Audio->tags->Title		= g_strdup_printf ("Track_%02d", Track +1);
			Audio->tags->Number		= g_strdup_printf ("%d", Track +1);
			Audio->tags->IntNumber		= Track +1;
			Audio->tags->Genre		= g_strdup ("A Cappella");
			Audio->tags->IntGenre		= 123;
			Audio->tags->Year		= g_strdup ("1962");
			Audio->tags->IntYear		= 1962;
			// Audio->tags->Comment		= g_strdup ("By Xcfa");
			// Audio->tags->Description	= g_strdup ("By Xcfa");
			Audio->tags->Comment		= g_strdup( Config.StringCommentCD );
			Audio->tags->Description	= g_strdup( Config.StringCommentCD );

			EnteteCD.GList_Audio_cd = g_list_append( EnteteCD.GList_Audio_cd, (CD_AUDIO *)Audio );
			
			// TITRE
			g_string_append_printf( gstr_buffer_title, "%s\n", Audio->NameSong	);
			// TITRE - TEMPS
			g_string_append_printf( gstr_buffer_title_time, "%s  ][  ", Audio->NameSong	);
			g_string_append_printf( gstr_buffer_title_time, "%s\n", Audio->Duree );
			// ARTISTE - TITRE - TEMPS
			g_string_append_printf( gstr_buffer_artist_title_time, "%s  -  ", Audio->tags->Artist );
			g_string_append_printf( gstr_buffer_artist_title_time, "%s  ][  ", Audio->NameSong	);
			g_string_append_printf( gstr_buffer_artist_title_time, "%s\n", Audio->Duree );
			
			view.TitleCD = g_strdup( "Track" );
		}
		CdCurl_set_datas_buffer( gstr_buffer_title, gstr_buffer_title_time, gstr_buffer_artist_title_time );
		
		return;
	}

	Larrbuf = g_strsplit( DatasBuffer, "\n", 0 );
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print( "\nREAD\n" );
	
	// SUPPRESSION DES CARACTERES INDELICATS
	for( Cpt = 0; Larrbuf[ Cpt ]; Cpt ++ ) {
		
		if( ! strncasecmp( Larrbuf[ Cpt ], "TTITLE", 6 )) {
			// 
			// BUG FIND BY @TICROB: SOLVED
			// DEL CAR '/' IN STRING
			//
			for( PtrNameTrack = Larrbuf[ Cpt ]; *PtrNameTrack; PtrNameTrack ++ ) {
				if( *PtrNameTrack == '/' ) *PtrNameTrack = '-';
				if( *PtrNameTrack == '\n' ) *PtrNameTrack = '\0';
				if( *PtrNameTrack == '\r' ) *PtrNameTrack = '\0';
			}
		}
	}
	
	for( Cpt = 0; Larrbuf[ Cpt ]; Cpt ++ ) {
				
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print( "%02d  %s\n", Cpt, Larrbuf[ Cpt ] );
		
		if( ! strncasecmp( Larrbuf[ Cpt ], "DTITLE", 6 )) {

			gchar	*Ptr = NULL;
			
			// DTITLE=Frank Sinatra / My Way - The Best Of Frank Sinatra Disc 1
			if( NULL != ( Ptr = strrchr( Larrbuf[ Cpt ], '/' ))) {
				EnteteCD.TitleCD = libutils_dup_chomp( Larrbuf[ Cpt ] +7 );
				Ptr = strrchr( EnteteCD.TitleCD, '/' );
				*Ptr = '-';
				EnteteCD.Artiste = libutils_dup_chomp( Larrbuf[ Cpt ] +7 );
				Ptr = strrchr( EnteteCD.Artiste, '/' );
				Ptr --;
				*Ptr = '\0';
			}
			else {
				EnteteCD.TitleCD = libutils_dup_chomp( Larrbuf[ Cpt ] +7 );
				EnteteCD.Artiste = libutils_dup_chomp( Larrbuf[ Cpt ] +7 );
			}
		}
		else if( ! strncasecmp( Larrbuf[ Cpt ], "DYEAR", 5 )) {
			EnteteCD.NumYear = atoi( Larrbuf[ Cpt ] +6 );
			EnteteCD.StrYear = libutils_dup_chomp( Larrbuf[ Cpt ] +6 );
		}
		else if( ! strncasecmp( Larrbuf[ Cpt ], "DGENRE", 6 )) {
			// 
			// EnteteCD.StrGenre = libutils_dup_chomp( Larrbuf[ Cpt ] +7 );
			// EnteteCD.NumGenre = tags_get_genre_by_value( EnteteCD.StrGenre );
			// 
			// DGENRE n'indiqque pas obligatoirement le meme type que le retour de QUERY !!!
			// 
			EnteteCD.StrGenre = g_strdup( VarSector.CallStrGenre );
			EnteteCD.NumGenre = tags_get_genre_by_value(VarSector.CallStrGenre );
		}
		else if( ! strncasecmp( Larrbuf[ Cpt ], "TTITLE", 6 ) && Track < VarSector.TotalTracks ) {
						
			gint	NumTrack;
			GString	*gstr = g_string_new (NULL);
			
			
			// TTITLE0=My Way
			// TTITLE1=Strangers In The Night
			// TTITLE2=Theme From New York New York
			// TTITLE3=I Get A Kick Out Of You
			
			// 
			// UNE LIGNE PEUT ETRE SCINDEE SUR PLUSIEURS LIGNES:
			// -------------------------------------------------
			// TTITLE3=Paul Buckmaster / Spider Research / Introduccion (We Did It) / The Prop
			// TTITLE3=osition
			// TTITLE8=Paul Buckmaster / Introduccion (Escape To Nowhere) / Scanner Room / Cap
			// TTITLE8=ture And Sedation
						
			// STOCKER LE PREMIER TITRE
			PtrNameTrack = strchr( Larrbuf[ Cpt ], '=' );
			PtrNameTrack ++;
			g_string_append_printf( gstr, "%s", PtrNameTrack );
			
			// NUMERO DU TTITLE 'xx'
			NumTrack = CdCurl_get_numtrack_next( Larrbuf[ Cpt ] );
			
			// NOUVEL ALGO: BUG TICROB SOLVED
			while( NumTrack == CdCurl_get_numtrack_next( Larrbuf[ Cpt +1 ] )) {
							
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print( "%02d  %s\n", Cpt, Larrbuf[ Cpt +1 ] );
				
				PtrNameTrack = strchr( Larrbuf[ Cpt +1 ], '=' );
				PtrNameTrack ++;
				g_string_append_printf( gstr, "%s", PtrNameTrack );
				Cpt ++;
			}
			
			PtrNameTrack = gstr->str;
			
			Audio = ( CD_AUDIO * )g_malloc0( sizeof( CD_AUDIO ));
			Audio->EtatPlay                 = CD_ETAT_PLAY_ATTENTE;
			Audio->Bool_Delette_Flac	= FALSE;
			Audio->Bool_Delette_Wav		= FALSE;
			Audio->Bool_Delette_Mp3		= FALSE;
			Audio->Bool_Delette_Ogg		= FALSE;
			Audio->Bool_Delette_M4a		= FALSE;
			Audio->Bool_Delette_Mpc		= FALSE;
			Audio->Bool_Delette_Ape		= FALSE;
			Audio->Bool_Delette_WavPack	= FALSE;
			Audio->EtatSelection_Flac	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Wav	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Mp3	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Ogg	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_M4a	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Mpc	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_Ape	= CD_ETAT_ATTENTE;
			Audio->EtatSelection_WavPack	= CD_ETAT_ATTENTE;
			Audio->Etat_Flac		= ETAT_CONV_CD_NONE;
			Audio->Etat_Wav			= ETAT_CONV_CD_NONE;
			Audio->Etat_Mp3			= ETAT_CONV_CD_NONE;
			Audio->Etat_Ogg			= ETAT_CONV_CD_NONE;
			Audio->Etat_m4a			= ETAT_CONV_CD_NONE;
			Audio->Etat_Mpc			= ETAT_CONV_CD_NONE;
			Audio->Etat_Ape			= ETAT_CONV_CD_NONE;
			Audio->Etat_WavPack		= ETAT_CONV_CD_NONE;
			Audio->EtatNormalise		= CD_NORM_PEAK_NONE;
			Audio->PathName_Dest_Flac	= NULL;
			Audio->PathName_Dest_Wav	= NULL;
			Audio->PathName_Dest_Mp3	= NULL;
			Audio->PathName_Dest_Ogg	= NULL;
			Audio->PathName_Dest_M4a	= NULL;
			Audio->PathName_Dest_Mpc	= NULL;
			Audio->PathName_Dest_Ape	= NULL;
			Audio->PathName_Dest_WavPack	= NULL;
			Audio->Num_Track		= Track + 1;
			Audio->Str_Track		= g_strdup_printf ( "%02d", Track + 1 );
			Audio->Duree			= g_strdup( VarSector.ListSector[ Track ].StrTime );
			Audio->NameSong			= libutils_dup_chomp( PtrNameTrack );

			VarSector.ListSector[ Track ].Title = g_strdup( Audio->NameSong	);
		
			Audio->tags = ( TAGS * )tags_alloc( TRUE );
		
			// JEROME JOLIMONT
			// 
			// Une fois la recherche effectuée, il y a dans le champ "Album", la chaîne de
			// caractères correspondant à la concaténation des chaînes "Artist", " - " et
			// "Album". Ca ne me paraît pas normal. C'est voulu ?
			// 
			// SOLVED:
			// Audio->tags->Album		= g_strdup( EnteteCD.TitleCD );
			// 
			if( NULL != ( PtrNameTrack = strchr( EnteteCD.TitleCD, '-' ))) {
				PtrNameTrack ++;
				while( ' ' == *PtrNameTrack ) PtrNameTrack++;
				Audio->tags->Album = g_strdup( PtrNameTrack );
			}
			else {
				Audio->tags->Album		= g_strdup( EnteteCD.TitleCD );
			}
			
			Audio->tags->Artist		= g_strdup( EnteteCD.Artiste );
			Audio->tags->Title		= g_strdup( Audio->NameSong	 );
			Audio->tags->Year		= g_strdup( EnteteCD.StrYear );
			Audio->tags->IntYear		= EnteteCD.NumYear;
			Audio->tags->IntGenre		= EnteteCD.NumGenre;
			Audio->tags->Genre		= g_strdup( EnteteCD.StrGenre );
			Audio->tags->Number		= g_strdup_printf ("%d", Track +1);
			Audio->tags->IntNumber		= Track +1;
			// Audio->tags->Comment		= g_strdup ("By Xcfa");
			// Audio->tags->Description	= g_strdup( "By Xcfa" );
			Audio->tags->Comment		= g_strdup( Config.StringCommentCD );
			Audio->tags->Description	= g_strdup( Config.StringCommentCD );

			EnteteCD.GList_Audio_cd = g_list_append( EnteteCD.GList_Audio_cd, (CD_AUDIO *)Audio );
			
			// TITRE
			g_string_append_printf( gstr_buffer_title, "%s\n", Audio->NameSong	);
			// TITRE - TEMPS
			g_string_append_printf( gstr_buffer_title_time, "%s  ][  ", Audio->NameSong	);
			g_string_append_printf( gstr_buffer_title_time, "%s\n", Audio->Duree );
			// ARTISTE - TITRE - TEMPS
			g_string_append_printf( gstr_buffer_artist_title_time, "%s  -  ", EnteteCD.Artiste );
			g_string_append_printf( gstr_buffer_artist_title_time, "%s  ][  ", Audio->NameSong	);
			g_string_append_printf( gstr_buffer_artist_title_time, "%s\n", Audio->Duree );

			view.TitleCD = g_strdup_printf( "%s", EnteteCD.TitleCD );
			
			Track ++;
			
			g_string_free (gstr, TRUE);
			gstr = NULL;
		}
	}
	CdCurl_set_datas_buffer( gstr_buffer_title, gstr_buffer_title_time, gstr_buffer_artist_title_time );
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print( "\n" );
	g_strfreev( Larrbuf );
	g_free( DatasBuffer );
	DatasBuffer = NULL;
}
//
//
gint CdCurl_cdparanoia_get_total_tracks( gchar *buffer )
{
	gchar  **Larrbuf = NULL;
	gint     Track;
	gint     TotalTracks = -1;
	gchar    *ptr;

	Larrbuf = g_strsplit( buffer, "\n", 0 );

	for( Track = 0; Larrbuf[ Track ]; Track++ ) {
		if( strstr (Larrbuf[ Track ], "TOTAL" )) {
			Track --;
			break;
		}
	}
	if( NULL != ( ptr = Larrbuf[ Track ] )) {
		while( *ptr == ' ' ) ptr ++;
		TotalTracks = atoi( ptr );
	}
	g_strfreev( Larrbuf );
	
	return( TotalTracks );
}
//
//
void CdCurl_cdparanoia_get_sectors ( gchar *buffer )
{
	gchar	**Larrbuf = NULL;
	gint	piste;
	gchar	*ptr = NULL;
	gint	Track = 0;
	
	if( NULL == VarSector.ListSector ) {
		VarSector.ListSector = (LIST_SECTOR *)g_malloc0( sizeof(LIST_SECTOR) * ( VarSector.TotalTracks + 4 ));
	}
	
	Larrbuf = g_strsplit( buffer, "\n", 0 );
	// GET SECTOR
	for( piste = 0; Larrbuf[ piste ]; piste ++ ) {
		if( strstr( Larrbuf[ piste ], "TOTAL" )) {
			ptr = Larrbuf[ piste ];
			while( *ptr && *ptr != ' ' ) ptr ++;
			while( *ptr && *ptr == ' ' ) ptr ++;
			if( *ptr ) {
				VarSector.ListSector[ Track ].NumSector = atoi( ptr );
			}
			break;
		}
		if( NULL != ( ptr = strchr( Larrbuf[ piste ], ']' ))) {
			ptr ++;
			while( *ptr && *ptr == ' ' ) ptr ++;
			if( *ptr )
				VarSector.ListSector[ Track ].NumSector = atoi( ptr );
			Track ++;
		}
	}
	// GET TIME
	Track = 0;
	for( piste = 0; Larrbuf[ piste ]; piste ++ ) {
		if( NULL != ( ptr = strchr( Larrbuf[ piste ], '[' ))) {
			//  1.    20685 [04:35.60]        0 [00:00.00]    no   no  2
			//  2.    11677 [02:35.52]    20685 [04:35.60]    no   no  2
			ptr ++;
			strncpy( VarSector.ListSector[ Track ].StrTime, ptr, 5 );
			Track ++;
		}
	}
	g_strfreev( Larrbuf );
}
//
//
void CdCurl_set_datas_for_cdcue( void )
{
	gint	Track;
	gint	TempDiv;
	gint	TempMod;
	
	cdcue_alloc_base_ioctl( VarSector.TotalTracks );
	for( Track = 0; Track < VarSector.TotalTracks; Track++ ) {
		BaseIoctl.Datas[ Track ].length = -1;
		BaseIoctl.Datas[ Track ].begin  = -1;
	}

	for( Track = 0; Track < VarSector.TotalTracks; Track++ ) {
		BaseIoctl.Datas[ Track ].length = VarSector.ListSector[ Track +1 ].NumSector - VarSector.ListSector[ Track ].NumSector;
		TempDiv = BaseIoctl.Datas[ Track ].length / 75;
		TempMod = BaseIoctl.Datas[ Track ].length % 75;
		BaseIoctl.Datas[ Track ].length_min  = TempDiv / 60;
		BaseIoctl.Datas[ Track ].length_sec  = TempDiv % 60;
		BaseIoctl.Datas[ Track ].length_cent = TempMod;
	}
	BaseIoctl.Datas[ 0 ].begin = VarSector.ListSector[ 0 ].NumSector % 75;

	for( Track = 0; Track < VarSector.TotalTracks; Track++ ) {

		BaseIoctl.Datas[ Track +1 ].begin = BaseIoctl.Datas[ Track ].length + BaseIoctl.Datas[ Track ].begin;
		TempDiv = BaseIoctl.Datas[ Track ].begin / 75;
		TempMod = BaseIoctl.Datas[ Track ].begin % 75;
		BaseIoctl.Datas[ Track ].begin_min  = TempDiv / 60;
		BaseIoctl.Datas[ Track ].begin_sec  = TempDiv % 60;
		BaseIoctl.Datas[ Track ].begin_cent = TempMod;
	}
	BaseIoctl.Datas[ VarSector.TotalTracks ].begin  = BaseIoctl.Datas[ Track ].length + BaseIoctl.Datas[ Track ].begin;
	BaseIoctl.Datas[ VarSector.TotalTracks ].begin -= BaseIoctl.Datas[ 0 ].begin;
	TempDiv = BaseIoctl.Datas[ VarSector.TotalTracks ].begin / 75;
	TempMod = BaseIoctl.Datas[ VarSector.TotalTracks ].begin % 75;
	BaseIoctl.Datas[ VarSector.TotalTracks ].begin_min  = TempDiv / 60;
	BaseIoctl.Datas[ VarSector.TotalTracks ].begin_sec  = TempDiv % 60;
	BaseIoctl.Datas[ VarSector.TotalTracks ].begin_cent = TempMod;
	
	cdcue_make_cue();

	/*
	-- void CdCurl_set_datas_for_cdcue_two( void )
	
	BaseIoctl.Performer = g_strdup( EnteteCD . TitleCD );
	BaseIoctl.Title     = g_strdup( EnteteCD . TitleCD );
	BaseIoctl.File      = g_strdup_printf ("%s.wav", EnteteCD . TitleCD);
	
	g_print( "BaseIoctl.Performer = %s\n", BaseIoctl.Performer );
	g_print( "BaseIoctl.Title     = %s\n", BaseIoctl.Title );
	g_print( "BaseIoctl.File      = %s\n", BaseIoctl.File );

	for( Track = 0; Track < VarSector.TotalTracks; Track++ ) {
		BaseIoctl.Cue [ Track ].Performer = g_strdup( EnteteCD . TitleCD );
		BaseIoctl.Cue [ Track ].Title     = g_strdup( VarSector.ListSector[ Track ].Title );
	}

	cdcue_print_base_ioctl();
	*/
}
//
//
void CdCurl_set_datas_for_cdcue_two( void )
{
	gint	Track;
	gchar	*PtrNameTrack = NULL;
	
	PRINT("");
	
	BaseIoctl.Performer = g_strdup( EnteteCD . TitleCD );
	BaseIoctl.Title     = g_strdup( EnteteCD . TitleCD );
	BaseIoctl.File      = g_strdup_printf ("%s.wav", EnteteCD . TitleCD);
	
	for( Track = 0; Track < VarSector.TotalTracks; Track++ ) {
		// 
		// BUG FIND BY @TICROB: SOLVED
		// TEST ALLOC OF: 
		// 	BaseIoctl.Cue [ Track ].Performer
		//	BaseIoctl.Cue [ Track ].Title
		// 
		if( NULL != BaseIoctl.Cue [ Track ].Performer ) {
			g_free( BaseIoctl.Cue [ Track ].Performer );
			BaseIoctl.Cue [ Track ].Performer = NULL;
		}
		if( NULL != BaseIoctl.Cue [ Track ].Title ) {
			g_free( BaseIoctl.Cue [ Track ].Title );
			BaseIoctl.Cue [ Track ].Title = NULL;
		}
		BaseIoctl.Cue [ Track ].Performer = g_strdup( EnteteCD . TitleCD );
		BaseIoctl.Cue [ Track ].Title     = g_strdup( VarSector.ListSector[ Track ].Title );

		while( NULL != ( PtrNameTrack = strchr( BaseIoctl.Cue [ Track ].Title, '/' ))) {
			*PtrNameTrack = '-';
		}
	}
	
	cdcue_print_base_ioctl();
}
//
//
gboolean CdCurl_get_info_cd( void )
{
	gchar		*StrCdDiscId = NULL;
	gchar		**Larrbuf = NULL;
	gint		Cpt;
	gchar		*Ptr = NULL;
	GString		*gstr = NULL;
	gboolean	BoolRet = FALSE;
	
	CdCurl_init_entete_cd();
	CdCurl_remove();
	poche_remove_view();
	StrCdDiscId = GetInfo_cd_discid( EnteteCD.NameCD_Device );
	if( NULL == StrCdDiscId ) {
		CdCurl_remove_struct();
		BoolRet = FALSE;
		EnteteCD.Message = g_strdup( _("Couldn't find CD") );
	}
	else if( NULL != StrCdDiscId ) {
		
		BoolRet = TRUE;
		
		Larrbuf = g_strsplit( StrCdDiscId, " ", 0 );
		g_free( StrCdDiscId );
		StrCdDiscId = NULL;
		
		VarSector.StrDiscID    = g_strdup( Larrbuf[ 0 ] );
		VarSector.GStrCdDiscID = g_string_new( NULL );
		for( Cpt = 1; Larrbuf[ Cpt ]; Cpt ++ ) {
			g_string_append_printf( VarSector.GStrCdDiscID, "%s", Larrbuf[ Cpt ] );
			if( NULL != Larrbuf[ Cpt +1 ] )
				g_string_append_printf( VarSector.GStrCdDiscID, "+" );
		}
		VarSector.LenDisc      = atoi( Larrbuf[ Cpt -1 ] );
		g_strfreev( Larrbuf );
		if( NULL != ( Ptr = strchr( VarSector.GStrCdDiscID->str, '\n' ))) *Ptr = '\0';
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print( "StrDiscID    = %s\n", VarSector.StrDiscID );
			g_print( "GStrCdDiscID = %s\n", VarSector.GStrCdDiscID->str );
			g_print( "LenDisc      = %d: ", VarSector.LenDisc );
			g_print( "\n" );
		}
		// GET REAL TRACKS AND GET REAL SECTORS FROM CDPARANOIA
		gstr = GetInfo_cdparanoia (EnteteCD.NameCD_Device);
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print("gstr->str=\n%s", gstr->str);
		if( ( VarSector.TotalTracks = CdCurl_cdparanoia_get_total_tracks( gstr->str ) ) > -1 ) {
			CdCurl_cdparanoia_get_sectors( gstr->str );
		}
		g_string_free (gstr, TRUE);
		gstr = NULL;
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print( "total_tracks with cdparanoia = %d\n", VarSector.TotalTracks );
		
		if( VarSector.TotalTracks > -1 ) {
			
			// SET DATAS FOR CD_CUE
			CdCurl_set_datas_for_cdcue();
		
			switch( CdCurl_discdb_do_query() ) {
			case DO_CURL_QUERY_FALSE :
				CdCurl_discdb_do_read( DO_CURL_QUERY_FALSE );
				break;
			case DO_CURL_QUERY_TRUE :
				CdCurl_discdb_do_read( DO_CURL_QUERY_TRUE );
				break;
			case DO_CURL_QUERY_REPEAT :
				if( DO_CURL_QUERY_TRUE == CdCurl_discdb_do_query()) {
					CdCurl_discdb_do_read( DO_CURL_QUERY_TRUE );
				}
				break;
			}
			
			// SET DATAS FOR CD_CUE
			CdCurl_set_datas_for_cdcue_two();
		}
		else {
			CdCurl_remove_struct();
			BoolRet = FALSE;
			EnteteCD.Message = g_strdup( _("No Audio CD") );
		}
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print( "\n" );
	}
	return( BoolRet );
}

















