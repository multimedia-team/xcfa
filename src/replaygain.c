 /*
 *  file      : replaygain.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "file.h"
#include "options.h"
#include "win_scan.h"
#include "notify_send.h"
#include "replaygain.h"



// 
// ---------------------------------------------------------------------------
//  VARIABLES
// ---------------------------------------------------------------------------
// 
typedef struct {
	gboolean	bool_etat;			// 
	gchar		MessUser[ 100 ];		// 
	gint		NbrElementsInList;		// 
	gint		ElementActif;			// 
	double		total_percent;			// 
	pthread_t	nmr_tid;			// 
	guint		handler_timeout;		// 
	gboolean	bool_thread_end;		// 
	gboolean	bool_compteur;			// 
	gint		pass_conv;			// 
	gboolean	bool_update;			// 
	TYPE_REPLAYGAIN	type_replaygain;		// 
} RPG;

RPG rpg;

typedef enum {
	MP3GAIN = 0,					// 
	METAFLAC,					// 
	WVGAIN,						// 
	VORBISGAIN					// 
} RPG_TYPE;



// 
// ---------------------------------------------------------------------------
//  GET LIST OF TO SET:
// ---------------------------------------------------------------------------
// 	TYPE
// 		MP3GAIN
// 		METAFLAC
// 		WVGAIN
// 		VORBISGAIN
// ---------------------------------------------------------------------------
// 	ACTION
// 		EFFACER
// 		ALBUM
// 		PISTE
// ---------------------------------------------------------------------------
// 
void replaygain_set_list( RPG_TYPE rpg_type, ETAT_REPLAYGAIN rpg_action)
{
	GList            *list = NULL;
	GList            *ListProg = NULL;
	GList            *ListEtat = NULL;
	DETAIL           *detail = NULL;
	gchar            *Ptr = NULL;
	ETAT_REPLAYGAIN  *PtrReplayGain = NULL;
	gboolean          BoolList = FALSE;
	gint		  pos;
	gchar		**PtrTabArgs = NULL;
	
	if( TRUE == rpg.bool_thread_end) return;
	
	rpg.MessUser [ 0 ] = '\0';
	
	// Entete global
	PtrTabArgs = filelc_AllocTabArgs();
	pos = 3;
	
	// Entetes particuliers
	switch (rpg_type ) {
	case MP3GAIN :
		PtrTabArgs [ pos++ ] = g_strdup ("mp3gain" );
		strcat( rpg.MessUser, "MP3, mp3gain"  );
		switch (rpg_action ) {
		case RPG_NONE :
		case RPG_ATTENTE :
			break;
		case RPG_EFFACER :
			PtrTabArgs [ pos++ ] = g_strdup ("-s" );
			PtrTabArgs [ pos++ ] = g_strdup ("d" );
			strcat( rpg.MessUser, ", EFFACER"  );
			break;
		case RPG_ALBUM :
			PtrTabArgs [ pos++ ] = g_strdup ("-a" );
			PtrTabArgs [ pos++ ] = g_strdup ("-p" );
			PtrTabArgs [ pos++ ] = g_strdup ("-c" );
			PtrTabArgs [ pos++ ] = g_strdup ("-f" );
			PtrTabArgs [ pos++ ] = g_strdup ("-T" );
			strcat( rpg.MessUser, ", ALBUM"  );
			break;
		case RPG_PISTE :
			PtrTabArgs [ pos++ ] = g_strdup ("-r" );
			PtrTabArgs [ pos++ ] = g_strdup ("-p" );
			PtrTabArgs [ pos++ ] = g_strdup ("-c" );
			PtrTabArgs [ pos++ ] = g_strdup ("-f" );
			PtrTabArgs [ pos++ ] = g_strdup ("-T" );
			strcat( rpg.MessUser, ", PISTE"  );
			break;
		}
		break;
	case METAFLAC :
		PtrTabArgs [ pos++ ] = g_strdup ("metaflac" );
		strcat( rpg.MessUser, "FLAC, metaflac"  );
		switch (rpg_action ) {
		case RPG_NONE :
		case RPG_ATTENTE :
			break;
		case RPG_EFFACER :
			PtrTabArgs [ pos++ ] = g_strdup ("--remove-replay-gain" );
			strcat( rpg.MessUser, ", EFFACER"  );
			break;
		case RPG_ALBUM :
			PtrTabArgs [ pos++ ] = g_strdup ("--add-replay-gain" );
			strcat( rpg.MessUser, ", ALBUM"  );
			break;
		case RPG_PISTE :
			/* NONE */
			break;
		}
		break;
	case WVGAIN :
		PtrTabArgs [ pos++ ] = g_strdup ("wvgain" );
		strcat( rpg.MessUser, "WAVPACK, wvgain"  );
		switch (rpg_action ) {
		case RPG_NONE :
		case RPG_ATTENTE :
			break;
		case RPG_EFFACER :
			PtrTabArgs [ pos++ ] = g_strdup ("-c" );
			strcat( rpg.MessUser, ", EFFACER"  );
			break;
		case RPG_ALBUM :
			PtrTabArgs [ pos++ ] = g_strdup ("-a" );
			strcat( rpg.MessUser, ", ALBUM"  );
			break;
		case RPG_PISTE :
			strcat( rpg.MessUser, ", PISTE"  );
			break;
		}
		break;
	case VORBISGAIN :
		PtrTabArgs [ pos++ ] = g_strdup ("vorbisgain" );
		strcat( rpg.MessUser, "OGG, vorbisgain"  );
		switch (rpg_action ) {
		case RPG_NONE :
		case RPG_ATTENTE :
			break;
		case RPG_EFFACER :
			PtrTabArgs [ pos++ ] = g_strdup ("-c" );
			strcat( rpg.MessUser, ", EFFACER"  );
			break;
		case RPG_ALBUM :
			PtrTabArgs [ pos++ ] = g_strdup ("-a" );
			strcat( rpg.MessUser, ", ALBUM"  );
			break;
		case RPG_PISTE :
			strcat( rpg.MessUser, ", PISTE"  );
			break;
		}
		break;
	}

	// -- RECHERCHE DES NOMS DE FICHIERS QUI SERONT AJOUTES EN FIN DE LISTE
	ListProg = g_list_first (entetefile );
	while (ListProg ) {
		if( (detail = (DETAIL *)ListProg->data) ) {
			
			// TEST D ENTREE
			if( detail->type_infosong_file_is != FILE_IS_OGG &&
			    detail->type_infosong_file_is != FILE_IS_MP3 &&
			    detail->type_infosong_file_is != FILE_IS_FLAC &&
			    detail->type_infosong_file_is != FILE_IS_WAVPACK ) {
			    
				ListProg = g_list_next (ListProg );
				continue;
			}
			
			if( NULL != detail && detail->Etat_ReplayGain == rpg_action ) {
				// VORBISGAIN
				if( detail->type_infosong_file_is == FILE_IS_OGG && rpg_type == VORBISGAIN ) {
					
					BoolList = TRUE;
					
					PtrTabArgs [ pos++ ] = g_strdup (detail->namefile );
					
					// Get pointer Etat Flag
					ListEtat = g_list_append (ListEtat, &detail->Etat_ReplayGain );
					
					if( detail->EtatTrash == FILE_TRASH_OK) detail->EtatTrash = FILE_TRASH_VERIF_OK;
					
					rpg.ElementActif ++;
				}
				// MP3GAIN
				else if( detail->type_infosong_file_is == FILE_IS_MP3 && rpg_type == MP3GAIN ) {
				
					BoolList = TRUE;
					PtrTabArgs [ pos++ ] = g_strdup (detail->namefile );
					
					// Get pointer Etat Flag
					ListEtat = g_list_append (ListEtat, &detail->Etat_ReplayGain );
					
					if( detail->EtatTrash == FILE_TRASH_OK) detail->EtatTrash = FILE_TRASH_VERIF_OK;
					
					rpg.ElementActif ++;
				}
				// METAFLAC
				else if( detail->type_infosong_file_is == FILE_IS_FLAC && rpg_type == METAFLAC ) {
					// PISTE -> NONE
					BoolList = TRUE;
					PtrTabArgs [ pos++ ] = g_strdup (detail->namefile );
					
					// Get pointer Etat Flag
					ListEtat = g_list_append (ListEtat, &detail->Etat_ReplayGain );
					
					if( detail->EtatTrash == FILE_TRASH_OK) detail->EtatTrash = FILE_TRASH_VERIF_OK;
					
					rpg.ElementActif ++;
				}
				// WVGAIN
				else if( detail->type_infosong_file_is == FILE_IS_WAVPACK && rpg_type == WVGAIN ) {
					BoolList = TRUE;
					PtrTabArgs [ pos++ ] = g_strdup (detail->namefile );
					
					// Get pointer Etat Flag
					ListEtat = g_list_append (ListEtat, &detail->Etat_ReplayGain );
					
					if( detail->EtatTrash == FILE_TRASH_OK) detail->EtatTrash = FILE_TRASH_VERIF_OK;
					
					rpg.ElementActif ++;
				}
			}
		}
		ListProg = g_list_next (ListProg );
	}

	// Test application de ReplayGain
	if( TRUE == BoolList ) {

		// Appel au ReplayGain
		rpg.bool_compteur = TRUE;
		rpg.bool_etat = TRUE;
		PtrTabArgs [ pos++ ] = NULL;
		conv_to_convert( PtrTabArgs, FALSE, REPLAYGAIN, rpg.MessUser );
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
		
		rpg.bool_compteur = FALSE;

		// Set update
		ListProg = g_list_first (ListEtat );
		while (ListProg ) {
			if( NULL != (PtrReplayGain = (ETAT_REPLAYGAIN *)ListProg->data) ) {
				*PtrReplayGain = RPG_ATTENTE;
			}
			ListProg->data = NULL;
			ListProg = g_list_next (ListProg );
		}
		g_list_free (ListEtat );
		ListEtat = NULL;
		rpg.bool_update = TRUE;
	}
	
	// suppression des elements de la liste
	ListProg = g_list_first (list );
	while (ListProg ) {
		if( NULL != (Ptr = (gchar *)ListProg->data) ) {
			g_free (Ptr );
			Ptr = NULL;
		}
		ListProg->data = NULL;
		ListProg = g_list_next (ListProg );
	}
	g_list_free (list );
	list = NULL;
}

// 
// ---------------------------------------------------------------------------
//  TIMEOUT & THREAD
// ---------------------------------------------------------------------------
// 
static gint replaygain_timeout (gpointer data)
{
	if( TRUE == conv.bool_percent_conv || TRUE == rpg.bool_compteur  ) {
		gchar *str = NULL;
    		gchar *spinner="|/-\\";
		gchar  foo [ 2 ];
		
		foo [ 0 ] = spinner[rpg.pass_conv++%4];
		foo [ 1 ] = '\0';
		str = g_strdup_printf ( "%s   Please wait   %s", foo, foo  );
		WindScan_set_label_bar( str  );
		g_free (str );
		str = NULL;
		
		// rpg.total_percent = (double)rpg.ElementActif / (double)rpg.NbrElementsInList;
		WindScan_set_pulse( );
		conv.bool_percent_conv = FALSE;
	}

	if( TRUE == rpg.bool_update ) {
		rpg.bool_update = FALSE;
		file_pixbuf_update_glist ( );
		file_set_flag_buttons ( );
		return (TRUE );
	}
	
	if( TRUE == rpg.bool_etat ) {
		rpg.bool_etat = FALSE;
	}
	
	if( TRUE == rpg.bool_thread_end ) {
		
		if( rpg.type_replaygain == _CLEAR_REPLAYGAIN_ ) {
			fileaction_set_end (TYPE_REPLAYGAINCLEAR );
		} else if( rpg.type_replaygain ==  _APPLY_REPLAYGAIN_ ) {
			fileaction_set_end (TYPE_REPLAYGAINAPPLY );
		}
		g_source_remove (rpg.handler_timeout );
	}
	
	return (TRUE );
}
// 
// 
static void replaygain_thread (void *arg)
{
	rpg.bool_thread_end = FALSE;
	
	if( rpg.type_replaygain == _CLEAR_REPLAYGAIN_ ) {
		replaygain_set_list( MP3GAIN, RPG_EFFACER );
		replaygain_set_list( METAFLAC, RPG_EFFACER );
		replaygain_set_list( WVGAIN, RPG_EFFACER );
		replaygain_set_list( VORBISGAIN, RPG_EFFACER );
	}
	else if( rpg.type_replaygain ==  _APPLY_REPLAYGAIN_ ) {
		replaygain_set_list( MP3GAIN, RPG_PISTE );
		replaygain_set_list( MP3GAIN, RPG_ALBUM );
		replaygain_set_list( METAFLAC, RPG_ALBUM );
		replaygain_set_list( WVGAIN, RPG_PISTE );
		replaygain_set_list( WVGAIN, RPG_ALBUM );
		replaygain_set_list( VORBISGAIN, RPG_PISTE );
		replaygain_set_list( VORBISGAIN, RPG_ALBUM );
	}
	
	rpg.bool_thread_end = TRUE;
	pthread_exit(0 );
}

// 
// ---------------------------------------------------------------------------
//  GET LIST REPLAYGAIN -- CLEAR & APPLY
// ---------------------------------------------------------------------------
// 	
void replaygain_get_list_Clear (void)
{
	GList   *list = NULL;
	DETAIL  *detail = NULL;

	// PRINT_FUNC_LF( );

	list = g_list_first (entetefile );
	while (list ) {
		if( NULL != (detail = (DETAIL *)list->data) ) {
			if( detail->type_infosong_file_is == FILE_IS_OGG ||
				detail->type_infosong_file_is == FILE_IS_MP3 ||
				detail->type_infosong_file_is == FILE_IS_FLAC ||
				detail->type_infosong_file_is == FILE_IS_WAVPACK ) {
				
				if( detail->Etat_ReplayGain == RPG_EFFACER ) {
					rpg.NbrElementsInList ++;
				}
			}
		}
		list = g_list_next( list );
	}
}
// 
// 
void replaygain_get_list_Apply( void )
{
	GList   *list = NULL;
	DETAIL  *detail = NULL;

	// PRINT_FUNC_LF( );

	list = g_list_first( entetefile );
	while( list ) {
		if( NULL != (detail = (DETAIL *)list->data )) {
			if( detail->type_infosong_file_is == FILE_IS_OGG ||
				detail->type_infosong_file_is == FILE_IS_MP3 ||
				detail->type_infosong_file_is == FILE_IS_FLAC ||
				detail->type_infosong_file_is == FILE_IS_WAVPACK ) {
				if( detail->Etat_ReplayGain != RPG_EFFACER && detail->Etat_ReplayGain != RPG_ATTENTE ) {
					rpg.NbrElementsInList ++;
				}
			}
		}
		list = g_list_next( list );
	}
}

// 
// ---------------------------------------------------------------------------
//  ACTION
// ---------------------------------------------------------------------------
// 
void replaygain_action( TYPE_REPLAYGAIN type_replaygain)
{
	// PRINT_FUNC_LF( );
	
	rpg.bool_thread_end = FALSE;
	rpg.bool_etat = FALSE;
	rpg.NbrElementsInList = 0;
	rpg.ElementActif = 0;
	rpg.bool_compteur = FALSE;
	rpg.pass_conv = -1;
	rpg.bool_update = FALSE;
	rpg.type_replaygain = type_replaygain;
	
	if( type_replaygain == _CLEAR_REPLAYGAIN_  ) {
		replaygain_get_list_Clear();
	}
	else if( type_replaygain ==  _APPLY_REPLAYGAIN_  ) {
		replaygain_get_list_Apply();
	}

	conv_reset_struct( WindScan_close_request );
	// WindScan_open( "ReplayGain", WINDSCAN_PULSE );
	wind_scan_init(
		WindMain,
		"ReplayGain",
		WINDSCAN_PULSE 
		);
	WindScan_set_label( "<b><i>ReplayGain ...</i></b>" );
	rpg.handler_timeout = g_timeout_add( 100, replaygain_timeout, 0 );
	pthread_create( &rpg.nmr_tid, NULL ,(void *)replaygain_thread, (void *)NULL );
}




