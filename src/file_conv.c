 /*
 *  file      : file_conv.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gprintf.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "options.h"
#include "conv.h"
#include "tags.h"
#include "configuser.h"
#include "prg_init.h"
#include "notify_send.h"
#include "tags.h"
#include "win_scan.h"
#include "win_info.h"
#include "file.h"



typedef struct {
	gboolean	BoolResultMplayerIsOk;
	gboolean	BoolResultSoxIsOk;
	gboolean	bool_thread_conv_one;
	gboolean	bool_thread_conv_two;
	gboolean	bool_thread_conv_three;
} VAR_FILE_CONV;

VAR_FILE_CONV VarFileConv = {
			FALSE,
			FALSE,
			FALSE,
			FALSE,
			FALSE
			};

pthread_mutex_t		mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t		synchro = PTHREAD_COND_INITIALIZER;



//
//
void fileconv_infos (DETAIL *detail)
{
	if( FALSE == OptionsCommandLine.BoolVerboseMode ) return;

	g_print ("\nCONVERSION(S) DEPUIS L'ONGLET 'FICHIERS'\n");
	if (detail->type_infosong_file_is == FILE_IS_WMA) {
		g_print ("\tFrom FILE_IS_WMA\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_RM) {
		g_print ("\tFrom FILE_IS_RM\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_DTS) {
		g_print ("\tFrom FILE_IS_DTS\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_AIFF) {
		g_print ("\tFrom FILE_IS_AIFF\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_M4A) {
		g_print ("\tFrom FILE_IS_M4A\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_VID_M4A) {
		g_print ("\tFrom FILE_IS_VID_M4A\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_SHN) {
		g_print ("\tFrom FILE_IS_SHN\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_WAV) {
		g_print ("\tFrom FILE_IS_WAV\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_FLAC) {
		g_print ("\tFrom FILE_IS_FLAC\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_MP3) {
		g_print ("\tFrom FILE_IS_MP3\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_OGG) {
		g_print ("\tFrom FILE_IS_OGG\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_MPC) {
		g_print ("\tFrom FILE_IS_MPC\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_APE) {
		g_print ("\tFrom FILE_IS_APE\n");
	}
	if (detail->type_infosong_file_is == FILE_IS_WAVPACK) {
		g_print ("\tFrom FILE_IS_WAVPACK\n");
	}
	if (detail->EtatSelection_Wav > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo Wav\n");
	if (detail->EtatSelection_Mp3 > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo Mp3%s\n", detail->EtatSelection_Mp3 >= ETAT_SELECT_EXPERT ? "............WithLineExpert" :"");
	if (detail->EtatSelection_Ogg > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo Ogg%s\n", detail->EtatSelection_Ogg >= ETAT_SELECT_EXPERT ? "............WithLineExpert" :"");
	if (detail->EtatSelection_Flac > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo Flac%s\n", detail->EtatSelection_Flac >= ETAT_SELECT_EXPERT ? "...........WithLineExpert" :"");
	if (detail->EtatSelection_M4a > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo M4a%s\n", detail->EtatSelection_M4a >= ETAT_SELECT_EXPERT ? "............WithLineExpert" :"");
	if (detail->EtatSelection_Aac > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo Aac%s\n", detail->EtatSelection_Aac >= ETAT_SELECT_EXPERT ? "............WithLineExpert" :"");
	if (detail->EtatSelection_Mpc > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo Mpc%s\n", detail->EtatSelection_Mpc >= ETAT_SELECT_EXPERT ? "............WithLineExpert" :"");
	if (detail->EtatSelection_Ape > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo Ape%s\n", detail->EtatSelection_Ape >= ETAT_SELECT_EXPERT ? "............WithLineExpert" :"");
	if (detail->EtatSelection_WavPack > ETAT_ATTENTE_EXIST)	g_print ("\t\tTo WavPack%s\n", detail->EtatSelection_WavPack >= ETAT_SELECT_EXPERT ? "........WithLineExpert" :"");
	g_print ("\n");
}
//
//
void fileconv_copy_src_to_dest( TYPE_FILE_IS p_TypeFileIs, DETAIL *detail, CONV_FIC *PConv )
{
	if( TRUE == conv.bool_stop) return;

	if( p_TypeFileIs == FILE_IS_WAV && detail->EtatSelection_Wav > ETAT_ATTENTE_EXIST && libutils_test_file_exist (PConv->tmp_wav)) {
		conv_copy_src_to_dest( PConv->tmp_wav, PConv->dest_wav);
		detail->EtatSelection_Wav = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	else if( p_TypeFileIs == FILE_IS_FLAC && detail->EtatSelection_Flac > ETAT_ATTENTE_EXIST && libutils_test_file_exist(PConv->tmp_flac) ) {
		conv_copy_src_to_dest( PConv->tmp_flac, PConv->dest_flac);
		detail->EtatSelection_Flac = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	else if( p_TypeFileIs == FILE_IS_APE && detail->EtatSelection_Ape > ETAT_ATTENTE_EXIST && libutils_test_file_exist (PConv->tmp_ape)) {
		conv_copy_src_to_dest( PConv->tmp_ape, PConv->dest_ape);
		detail->EtatSelection_Ape = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	else if( p_TypeFileIs == FILE_IS_WAVPACK && detail->EtatSelection_WavPack > ETAT_ATTENTE_EXIST && libutils_test_file_exist (PConv->tmp_wavpack)) {
		conv_copy_src_to_dest( PConv->tmp_wavpack, PConv->dest_wavpack);

		if (*optionsWavpack_get_wavpack_correction_file () != '\0') {
			conv_copy_src_to_dest( PConv->tmp_wavpack_md5, PConv->dest_wavpack_md5);
		}
		detail->EtatSelection_WavPack = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	else if( p_TypeFileIs == FILE_IS_OGG && detail->EtatSelection_Ogg > ETAT_ATTENTE_EXIST && libutils_test_file_exist (PConv->tmp_ogg)) {
		conv_copy_src_to_dest( PConv->tmp_ogg, PConv->dest_ogg);
		detail->EtatSelection_Ogg = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	else if( p_TypeFileIs == FILE_IS_M4A && detail->EtatSelection_M4a > ETAT_ATTENTE_EXIST && libutils_test_file_exist (PConv->tmp_m4a)) {
		conv_copy_src_to_dest( PConv->tmp_m4a, PConv->dest_m4a);
		detail->EtatSelection_M4a = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	else if( p_TypeFileIs == FILE_IS_AAC && detail->EtatSelection_Aac > ETAT_ATTENTE_EXIST && libutils_test_file_exist (PConv->tmp_aac)) {
		conv_copy_src_to_dest( PConv->tmp_aac, PConv->dest_aac);
		detail->EtatSelection_Aac = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	else if( p_TypeFileIs == FILE_IS_MPC && detail->EtatSelection_Mpc > ETAT_ATTENTE_EXIST && libutils_test_file_exist (PConv->tmp_mpc)) {
		conv_copy_src_to_dest( PConv->tmp_mpc, PConv->dest_mpc);
		detail->EtatSelection_Mpc = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	else if( p_TypeFileIs == FILE_IS_MP3 && detail->EtatSelection_Mp3 > ETAT_ATTENTE_EXIST && libutils_test_file_exist (PConv->tmp_mp3)) {
		conv_copy_src_to_dest( PConv->tmp_mp3, PConv->dest_mp3);
		detail->EtatSelection_Mp3 = ETAT_ATTENTE_EXIST;
		conv.Bool_MAJ_select = TRUE;
	}
	if( detail->EtatTrash == FILE_TRASH_OK ) detail->EtatTrash = FILE_TRASH_VERIF_OK;
}
//
// PConv->tmp_wav TO PConv->tmp_sox
//
gboolean fileconv_change_quantification_with_sox( CONV_FIC *PConv, gint p_Hertz, gint p_Channels, gint p_Bits )
{
	gchar	StrHertz[ 10 ];
	gchar	StrChannels[ 10 ];
	gchar	StrBits[ 10 ];
	gchar	**PtrTabArgs = NULL;

	g_sprintf( &StrHertz[0], "%d", (gint)p_Hertz );
	g_sprintf( &StrChannels[0], "%d", (gint)p_Channels );
	g_sprintf( &StrBits[0], "%d", (gint)p_Bits );

pthread_mutex_lock( &mutex );
	PtrTabArgs = conv_with_sox_get_param( PConv->tmp_wav, PConv->tmp_sox, StrHertz, StrChannels, StrBits );
pthread_mutex_unlock( &mutex );
	conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
	PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

	return( libutils_test_file_exist( PConv->tmp_sox ));
}
//
//
// http://www.bien-programmer.fr/pthreads.htm
//
//
static void fileconv_thread_one (void *arg)
{
	GList		*ListGlobal = NULL;
	DETAIL		*detail = NULL;
	CONV_FIC	*PConv = NULL;
	PARAM_FILELC	param_filelc;
	gint		pos;
	TAGS		*InfosTags = NULL;
	gint		Channels;
	gint		Hertz;
	gint		Bits;
	gint		NewBits;

	VarFileConv.bool_thread_conv_one = TRUE;

	PRINT("DEBUT THREAD FILECONV ONE");
	ListGlobal = g_list_first (entetefile);
	while (FALSE == conv.bool_stop && ListGlobal) {
		if (NULL != (detail = (DETAIL *)ListGlobal->data) && NULL != (PConv = (CONV_FIC *)detail->PConv)) {

			// ------------------------------------------------------------------------
			//
			// FROM:
			//	FLAC | APE | WAVPACK | OGG | M4A | FILE_IS_VID_M4A | MPC | MP3 | WMA | SHORTEN | RM | DTS | AIF | AC3
			// TO:
			//	WAV
			//
			// ------------------------------------------------------------------------

			if( FALSE == conv.bool_stop ) {

				fileconv_infos (detail);

				conv.Bool_MAJ_select = FALSE;

				// COPIE DANS LE DOSSIER TEMPORAIRE
				if( NULL == detail->NameFileCopyFromNormalizate ) {
					conv_copy_src_to_dest( detail->namefile, PConv->tmp_wav );
				}

				switch( detail->type_infosong_file_is ) {
				case FILE_IS_NONE :
				// case FILE_IS_VID_M4A :
				case FILE_IS_WAVPACK_MD5 :
				case FILE_TO_NORMALISE :
				case FILE_TO_NORMALISE_COLLECTIF :
				case FILE_TO_REPLAYGAIN :
				case FILE_IS_AAC :
				break;

				case FILE_IS_WAV :
				{
					INFO_WAV *info = (INFO_WAV *)detail->info;
					InfosTags = info->tags;
					gchar	**PtrTabArgs = NULL;

					if( TRUE == info->BoolBwf ) {
						PtrTabArgs = conv_with_sox_float_get_param(
								PConv->tmp_wav,
								PConv->tmp_sox
								);
						conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
						PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
						conv_copy_src_to_dest( PConv->tmp_sox, PConv->tmp_wav );
					}
					conv.Bool_MAJ_select = TRUE;
					break;
				}
				case FILE_IS_FLAC :
				{
					INFO_FLAC *info = (INFO_FLAC *)detail->info;
					InfosTags = info->tags;
					gchar	**PtrTabArgs = NULL;

					// COPIE DANS LE DOSSIER TEMPORAIRE
					conv_copy_src_to_dest( detail->namefile, PConv->tmp_flac);

					param_filelc.type_conv            = FLAC_FLAC_TO_WAV;
					param_filelc.With_CommandLineUser = detail->EtatSelection_Flac >= ETAT_SELECT_EXPERT;
					param_filelc.filesrc              = PConv->tmp_flac;
					param_filelc.filedest             = PConv->tmp_wav;
					param_filelc.tags                 = InfosTags;
					param_filelc.cdrom                = NULL;
					param_filelc.num_track            = NULL;
					param_filelc.BoolSetBitrate       = FALSE;
					param_filelc.PtrStrBitrate        = NULL;
			pthread_mutex_lock( &mutex );
					PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
					conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, FLAC_FLAC_TO_WAV, "FLAC_FLAC_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					conv.Bool_MAJ_select = TRUE;
					break;
				}
				case FILE_IS_APE :
				{
					INFO_APE *info = (INFO_APE *)detail->info;
					InfosTags = info->tags;
					gchar	**PtrTabArgs = NULL;

					// COPIE DANS LE DOSSIER TEMPORAIRE
					conv_copy_src_to_dest( detail->namefile, PConv->tmp_ape);

					param_filelc.type_conv            = MAC_APE_TO_WAV;
					param_filelc.With_CommandLineUser = detail->EtatSelection_Ape >= ETAT_SELECT_EXPERT;
					param_filelc.filesrc              = PConv->tmp_ape;
					param_filelc.filedest             = PConv->tmp_wav;
					param_filelc.tags                 = NULL;
					param_filelc.cdrom                = NULL;
					param_filelc.num_track            = NULL;
					param_filelc.BoolSetBitrate       = FALSE;
					param_filelc.PtrStrBitrate        = NULL;
			pthread_mutex_lock( &mutex );
					PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
					conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, MAC_APE_TO_WAV, "MAC_APE_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					conv.Bool_MAJ_select = TRUE;
					break;
				}
				case FILE_IS_WAVPACK :
				{
					INFO_WAVPACK *info = (INFO_WAVPACK *)detail->info;
					InfosTags = info->tags;
					gchar	**PtrTabArgs = NULL;

					// COPIE DANS LE DOSSIER TEMPORAIRE
					conv_copy_src_to_dest( detail->namefile, PConv->tmp_wavpack);

					// COPIE DANS LE DOSSIER TEMPORAIRE
					conv_copy_src_to_dest( PConv->dest_wavpack_md5, PConv->tmp_wavpack_md5);

					PtrTabArgs = filelc_AllocTabArgs();

					pos = 3;
					PtrTabArgs[ pos++ ] = g_strdup ("wvunpack");
					PtrTabArgs[ pos++ ] = g_strdup ("-y");
					if (*optionsWavpack_get_wavpack_signature_md5 () != '\0') {
						PtrTabArgs[ pos++ ] = g_strdup (optionsWavpack_get_wavpack_signature_md5 ());
					}
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_wavpack);
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_wav);
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, FALSE, WVUNPACK_WAVPACK_TO_WAV, "WVUNPACK_WAVPACK_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					conv.Bool_MAJ_select = TRUE;
					break;
				}
				case FILE_IS_OGG :
				case FILE_IS_MP3 :
				{
					gchar	**PtrTabArgs = NULL;

					PtrTabArgs = filelc_AllocTabArgs();

					if( FILE_IS_OGG == detail->type_infosong_file_is ) {
						INFO_OGG *info = (INFO_OGG *)detail->info;
						InfosTags = info->tags;
					} else if( FILE_IS_MP3 == detail->type_infosong_file_is ) {
						INFO_MP3 *info = (INFO_MP3 *)detail->info;
						InfosTags = info->tags;
					}

					// COPIE DANS LE DOSSIER TEMPORAIRE
					if (FALSE == conv.bool_stop && NULL == detail->NameFileCopyFromNormalizate) {

						if( FILE_IS_OGG == detail->type_infosong_file_is ) {
							conv_copy_src_to_dest( detail->namefile, PConv->tmp_ogg);
						} else if( FILE_IS_MP3 == detail->type_infosong_file_is ) {
							conv_copy_src_to_dest( detail->namefile, PConv->tmp_mp3);
						}
					}

					pos = 3;
					PtrTabArgs[ pos++ ] = g_strdup ("mplayer");
					PtrTabArgs[ pos++ ] = g_strdup ("-nojoystick");
					PtrTabArgs[ pos++ ] = g_strdup ("-nolirc");
					PtrTabArgs[ pos++ ] = g_strdup ("-ao");
					PtrTabArgs[ pos++ ] = g_strdup ("pcm");
					if( FILE_IS_OGG == detail->type_infosong_file_is ) {
						PtrTabArgs[ pos++ ] = g_strdup (PConv->tmp_ogg);
					} else if( FILE_IS_MP3 == detail->type_infosong_file_is ) {
						PtrTabArgs[ pos++ ] = g_strdup (PConv->tmp_mp3);
					}
					PtrTabArgs[ pos++ ] = g_strdup ("-ao");
					PtrTabArgs[ pos++ ] = g_strdup_printf ("pcm:file=%s", PConv->tmp_wav);
					PtrTabArgs[ pos++ ] = NULL;
					if( FILE_IS_OGG == detail->type_infosong_file_is ) {
						conv_to_convert( PtrTabArgs, FALSE, MPLAYER_OGG_TO_WAV, "MPLAYER_OGG_TO_WAV");
					}
					else {
						conv_to_convert( PtrTabArgs, FALSE, MPLAYER_MP3_TO_WAV, "MPLAYER_MP3_TO_WAV");

						// Mon, 06 Jan 2014 08:33:58 +0100
						// Isaak Babaev
						// Must be 16 bits
						tagswav_file_GetBitrate( PConv->tmp_wav, &Channels, &Hertz, &Bits );
						if( Bits != 16 ) {
							Bits = 16;
							fileconv_change_quantification_with_sox( PConv, Hertz, Channels, Bits );
							conv_copy_src_to_dest( PConv->tmp_sox, PConv->tmp_wav );
						}
					}
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					conv.Bool_MAJ_select = TRUE;
					break;
				}
				case FILE_IS_MPC :
				{
					gchar	**PtrTabArgs = NULL;
					gchar	*Ptr = NULL;
					gint	BoolApplyConvMPC = 0;
					INFO_MPC *info = (INFO_MPC *)detail->info;
					InfosTags = info->tags;

					conv_copy_src_to_dest( detail->namefile, PConv->tmp_mpc );

					PtrTabArgs = filelc_AllocTabArgs();
					pos = 3;
					PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
					PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
					PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_mpc);
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup ("pcm");
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", PConv->tmp_wav);
					PtrTabArgs [ pos++ ] = g_strdup ("-af");
					PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
					PtrTabArgs [ pos++ ] = g_strdup ("-srate");
					PtrTabArgs [ pos++ ] = g_strdup ("44100");
					PtrTabArgs [ pos++ ] = NULL;
					VarFileConv.BoolResultMplayerIsOk = conv_to_convert( PtrTabArgs, FALSE, MPLAYER_MPC_TO_WAV, "MPLAYER_MPC_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					if( FALSE == libutils_test_file_exist( PConv->tmp_wav )) {
						PtrTabArgs = filelc_AllocTabArgs();
						pos = 3;
						Ptr = PtrTabArgs [ pos++ ] = g_strdup( prginit_get_name( NMR_musepack_tools_mppdec ));
						if( strstr( Ptr, "mpc123" ) != NULL ) BoolApplyConvMPC = 1;
						if( strstr( Ptr, "mppdec" ) != NULL ) BoolApplyConvMPC = 1;
						if( strstr( Ptr, "mpcdec" ) != NULL ) BoolApplyConvMPC = 2;

						// mpc123 | mppdec
						if( 1 == BoolApplyConvMPC ) {
							PtrTabArgs [ pos++ ] = g_strdup ("--wav");
						}
						if( HostConf.BoolCpuIs64Bits == TRUE ) {
							if( 1 == BoolApplyConvMPC ) {
								PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_wav);
								PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_mpc);
							}
							else if( 2 == BoolApplyConvMPC ) {
								PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_mpc);
								PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_wav);
							}
						}
						else {
							PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_mpc);
							PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_wav);
						}
						PtrTabArgs[ pos++ ] = NULL;
						conv_to_convert( PtrTabArgs, FALSE, MPPDEC_MPC_TO_WAV, "MPPDEC_MPC_TO_WAV" );
						PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
						conv.Bool_MAJ_select = TRUE;
					}
					conv.Bool_MAJ_select = TRUE;
					break;
				}
				case FILE_IS_AC3 :
				{
					gchar	**PtrTabArgs = NULL;
					INFO_AC3*info = (INFO_AC3 *)detail->info;
					InfosTags = info->tags;

					conv_copy_src_to_dest( detail->namefile, PConv->tmp_ac3 );
					//
					// TODO DEBIAN/ liba52-0.7.4-dev
					//
					// a52dec ./audio.ac3 -o wav > audio.wav
					//
					PtrTabArgs = filelc_AllocTabArgs();
					pos = 3;
					PtrTabArgs [ pos++ ] = g_strdup ("a52dec");
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_ac3);
					PtrTabArgs [ pos++ ] = g_strdup ("-o");
					PtrTabArgs [ pos++ ] = g_strdup ("wav");
					PtrTabArgs [ pos++ ] = g_strdup (">");
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_wav);
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, FALSE, A52DEC_AC3_TO_WAV, "A52DEC_AC3_TO_WAV" );
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					tagswav_file_GetBitrate( PConv->tmp_wav, &Channels, &Hertz, &Bits );
					fileconv_change_quantification_with_sox( PConv, Hertz, Channels, Bits );
					conv_copy_src_to_dest( PConv->tmp_sox, PConv->tmp_wav );
					conv.Bool_MAJ_select = TRUE;
					break;
				}

				case FILE_IS_VID_M4A :
				case FILE_IS_M4A :
				{
					INFO_M4A *info = (INFO_M4A *)detail->info;
					InfosTags = info->tags;
					gchar	**PtrTabArgs = NULL;

					// COPIE DANS LE DOSSIER TEMPORAIRE
					conv_copy_src_to_dest( detail->namefile, PConv->tmp_m4a);

					// CONVERSION WAV
					PtrTabArgs = filelc_AllocTabArgs();
					pos = 3;
					PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
					PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
					PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
					if( FILE_IS_VID_M4A == detail->type_infosong_file_is ) {
						// mplayer -nojoystick -nolirc -novideo -vo null ./WCC_RADIO_EP042.m4a -ao pcm -ao pcm:file=dest_novdeo_1.wav -srate 44100
						PtrTabArgs [ pos++ ] = g_strdup ("-novideo");
						PtrTabArgs [ pos++ ] = g_strdup ("-vo");
						PtrTabArgs [ pos++ ] = g_strdup ("null");
					}
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup ("pcm");
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_m4a);
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", PConv->tmp_wav);
					if( FILE_IS_VID_M4A == detail->type_infosong_file_is ) {
						// mplayer -nojoystick -nolirc -novideo -vo null ./WCC_RADIO_EP042.m4a -ao pcm -ao pcm:file=dest_novdeo_1.wav -srate 44100
						PtrTabArgs [ pos++ ] = g_strdup ("-srate");
						PtrTabArgs [ pos++ ] = g_strdup ("44100");
					}
					PtrTabArgs [ pos++ ] = NULL;
					VarFileConv.BoolResultMplayerIsOk = conv_to_convert( PtrTabArgs, FALSE, MPLAYER_M4A_TO_WAV, "MPLAYER_M4A_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					conv.Bool_MAJ_select = TRUE;
					break;
				}
				case FILE_IS_WMA :
				case FILE_IS_RM :
				case FILE_IS_DTS :
				case FILE_IS_AIFF :
				{
					gchar	**PtrTabArgs = NULL;

					if( FILE_IS_WMA == detail->type_infosong_file_is ) {
						INFO_WMA *info = (INFO_WMA *)detail->info;
						InfosTags = info->tags;
						conv_copy_src_to_dest( detail->namefile, PConv->tmp_wma );
					} else if( FILE_IS_RM == detail->type_infosong_file_is ) {
						INFO_RM *info = (INFO_RM *)detail->info;
						InfosTags = info->tags;
						conv_copy_src_to_dest( detail->namefile, PConv->tmp_rm );
					} else if( FILE_IS_DTS == detail->type_infosong_file_is ) {
						INFO_DTS *info = (INFO_DTS *)detail->info;
						InfosTags = info->tags;
						conv_copy_src_to_dest( detail->namefile, PConv->tmp_dts );
					} else if( FILE_IS_AIFF == detail->type_infosong_file_is ) {
						INFO_AIFF *info = (INFO_AIFF *)detail->info;
						InfosTags = info->tags;
						conv_copy_src_to_dest( detail->namefile, PConv->tmp_aiff );
					}

					// CONVERSION WAV
					PtrTabArgs = filelc_AllocTabArgs();
					pos = 3;
					PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
					PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
					PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
					if( FILE_IS_WMA == detail->type_infosong_file_is ) {
						PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_wma);
					} else if( FILE_IS_RM == detail->type_infosong_file_is ) {
						PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_rm);
					} else if( FILE_IS_DTS == detail->type_infosong_file_is ) {
						PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_dts);
					} else if( FILE_IS_AIFF == detail->type_infosong_file_is ) {
						PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_aiff );
					}
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup ("pcm");
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", PConv->tmp_wav);
					PtrTabArgs [ pos++ ] = g_strdup ("-af");
					PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
					PtrTabArgs [ pos++ ] = g_strdup ("-srate");
					PtrTabArgs [ pos++ ] = g_strdup ("44100");
					PtrTabArgs [ pos++ ] = NULL;
					VarFileConv.BoolResultMplayerIsOk = conv_to_convert( PtrTabArgs, FALSE, MPLAYER_WMA_TO_WAV, "MPLAYER_WMA_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					conv.Bool_MAJ_select = TRUE;
					break;
				}
				case FILE_IS_SHN :
				{
					INFO_SHN *info = (INFO_SHN *)detail->info;
					InfosTags = info->tags;
					gchar	**PtrTabArgs = NULL;

					// COPIE DANS LE DOSSIER TEMPORAIRE
					conv_copy_src_to_dest( detail->namefile, PConv->tmp_shn);

					// CONVERSION WAV
					PtrTabArgs = filelc_AllocTabArgs();
					pos = 3;
					PtrTabArgs [ pos++ ] = g_strdup ("shorten");
					PtrTabArgs [ pos++ ] = g_strdup ("-x");
					PtrTabArgs [ pos++ ] = g_strdup ("-b");
					PtrTabArgs [ pos++ ] = g_strdup ("256");
					PtrTabArgs [ pos++ ] = g_strdup ("-c");
					PtrTabArgs [ pos++ ] = g_strdup ("2");
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_shn);
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_wav);
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, FALSE, SHORTEN_SHN_TO_WAV, "SHORTEN_SHN_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					conv.Bool_MAJ_select = TRUE;
					break;
				}

				} // swicth

				if( TRUE == conv.Bool_MAJ_select ) fileconv_copy_src_to_dest( FILE_IS_WAV, detail, PConv );
			}

			tagswav_file_GetBitrate( PConv->tmp_wav, &Channels, &Hertz, &Bits );
			NewBits = Bits;

			// ------------------------------------------------------------------------
			//
			// FROM:
			//	WAV
			// TO:
			//	FLAC | APE | WAVPACK | OGG | M4A | FILE_IS_VID_M4A | AAC | MPC | MP3
			//
			// ------------------------------------------------------------------------

			if( FALSE == conv.bool_stop &&  detail->EtatSelection_Flac > ETAT_ATTENTE_EXIST ) {

				gchar	**PtrTabArgs = NULL;

				// REAJUSTE LA QUANTIFICATION
				if( Bits == 32 ) {

					VarFileConv.BoolResultSoxIsOk = fileconv_change_quantification_with_sox( PConv, Hertz, Channels, 24 );
				}

				param_filelc.type_conv            = FLAC_WAV_TO_FLAC;
				param_filelc.With_CommandLineUser = detail->EtatSelection_Flac >= ETAT_SELECT_EXPERT;
				param_filelc.filesrc              = ( Bits == 32 ) ? PConv->tmp_sox : PConv->tmp_wav;
				param_filelc.filedest             = PConv->tmp_flac;
				param_filelc.tags                 = NULL;
				param_filelc.cdrom                = NULL;
				param_filelc.num_track            = NULL;
				param_filelc.PtrStrBitrate        = NULL;
			pthread_mutex_lock( &mutex );
				PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, FLAC_WAV_TO_FLAC, "FLAC_WAV_TO_FLAC");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				conv.Bool_MAJ_select = TRUE;
				fileconv_copy_src_to_dest( FILE_IS_FLAC, detail, PConv );
			}

			if( FALSE == conv.bool_stop &&  detail->EtatSelection_Ape > ETAT_ATTENTE_EXIST ) {

				gchar	**PtrTabArgs = NULL;

				// REAJUSTE LA QUANTIFICATION
				if( Bits >= 24 ) {

					VarFileConv.BoolResultSoxIsOk = fileconv_change_quantification_with_sox( PConv, Hertz, Channels, 16 );
				}

				param_filelc.type_conv            = MAC_WAV_TO_APE;
				param_filelc.With_CommandLineUser = detail->EtatSelection_Ape >= ETAT_SELECT_EXPERT;
				param_filelc.filesrc              = ( Bits >= 24 ) ? PConv->tmp_sox : PConv->tmp_wav;
				param_filelc.filedest             = PConv->tmp_ape;
				param_filelc.tags                 = NULL;
				param_filelc.cdrom                = NULL;
				param_filelc.num_track            = NULL;
				param_filelc.PtrStrBitrate        = NULL;
			pthread_mutex_lock( &mutex );
				PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, MAC_WAV_TO_APE, "MAC_WAV_TO_APE");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				conv.Bool_MAJ_select = TRUE;
				fileconv_copy_src_to_dest( FILE_IS_APE, detail, PConv );
			}

			if( FALSE == conv.bool_stop &&  detail->EtatSelection_WavPack > ETAT_ATTENTE_EXIST ) {

				gchar	**PtrTabArgs = NULL;

				param_filelc.type_conv             = WAVPACK_WAV_TO_WAVPACK;
				param_filelc.With_CommandLineUser  = detail->EtatSelection_WavPack >= ETAT_SELECT_EXPERT;
				param_filelc.filesrc               = PConv->tmp_wav;
				param_filelc.filedest              = PConv->tmp_wavpack;
				param_filelc.tags                  = InfosTags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.PtrStrBitrate         = NULL;
			pthread_mutex_lock( &mutex );
				PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, WAVPACK_WAV_TO_WAVPACK, "WAVPACK_WAV_TO_WAVPACK");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				conv.Bool_MAJ_select = TRUE;
				fileconv_copy_src_to_dest( FILE_IS_WAVPACK, detail, PConv );
			}

			if( FALSE == conv.bool_stop &&  detail->EtatSelection_Ogg > ETAT_ATTENTE_EXIST ) {

				gchar	**PtrTabArgs = NULL;

				// REAJUSTE LA QUANTIFICATION
				if (TRUE == PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_OGG ]) {

					if (Bits > 24)				NewBits = 24;
					else if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 24 : 16;
					else if (Bits > 8 && Bits < 16)		NewBits = (Bits - 8) < 3 ? 16 : 8;
					else if (Bits < 8)			NewBits = 8;

					Bits = NewBits;

					VarFileConv.BoolResultSoxIsOk = fileconv_change_quantification_with_sox( PConv, Hertz, Channels, NewBits );
				}

				if (Bits == 24) {

					PtrTabArgs = conv_with_sox_float_get_param(
								(TRUE == PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_OGG ]) ?  PConv->tmp_sox : PConv->tmp_wav,
								PConv->tmp_sox_24
								);

					conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					conv_copy_src_to_dest( PConv->tmp_sox_24, PConv->tmp_sox);
				}

				param_filelc.type_conv             = OGGENC_WAV_TO_OGG;
				param_filelc.With_CommandLineUser  = detail->EtatSelection_Ogg >= ETAT_SELECT_EXPERT;

				if (TRUE == PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_OGG ] || Bits == 24) {
					param_filelc.filesrc               = PConv->tmp_sox;
				}
				else {
					param_filelc.filesrc               = PConv->tmp_wav;
				}

				param_filelc.filedest              = PConv->tmp_ogg;
				param_filelc.tags                  = InfosTags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = options_get_params (OGGENC_WAV_TO_OGG);
			pthread_mutex_lock( &mutex );
				PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, OGGENC_WAV_TO_OGG, "OGGENC_WAV_TO_OGG");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				conv.Bool_MAJ_select = TRUE;
				fileconv_copy_src_to_dest( FILE_IS_OGG, detail, PConv );
			}
			if( FALSE == conv.bool_stop &&  detail->EtatSelection_M4a > ETAT_ATTENTE_EXIST ) {

				gchar	**PtrTabArgs = NULL;

				// REAJUSTE LA QUANTIFICATION
				if (TRUE == PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_M4A ]) {

					if (Bits > 64)				NewBits = 64;
					else if (Bits > 32 && Bits < 64)	NewBits = 32;
					else if (Bits > 24 && Bits < 32)	NewBits = 24;
					else if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 24 : 16;
					else if (Bits > 8 && Bits < 16)		NewBits = (Bits - 8) < 3 ? 16 : 8;
					else if (Bits < 8)			NewBits = 8;

					NewBits = Bits;

					VarFileConv.BoolResultSoxIsOk = fileconv_change_quantification_with_sox( PConv, Hertz, Channels, NewBits );
				}
				else {
					conv_copy_src_to_dest( PConv->tmp_wav, PConv->tmp_sox);
				}

				if (Bits == 32 || Bits == 24) {

					PtrTabArgs = conv_with_sox_float_get_param(
								PConv->tmp_sox,
								PConv->tmp_sox_24
								);
					conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					conv_copy_src_to_dest( PConv->tmp_sox_24, PConv->tmp_sox);
				}

				param_filelc.type_conv            = FAAC_WAV_TO_M4A;
				param_filelc.With_CommandLineUser = detail->EtatSelection_M4a >= ETAT_SELECT_EXPERT;
				param_filelc.filesrc              = PConv->tmp_sox;
				param_filelc.filedest             = PConv->tmp_m4a;
				param_filelc.tags                 = InfosTags;
				param_filelc.cdrom                = NULL;
				param_filelc.num_track            = NULL;
			pthread_mutex_lock( &mutex );
				PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, FAAC_WAV_TO_M4A, "FAAC_WAV_TO_M4A");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				conv.Bool_MAJ_select = TRUE;
				fileconv_copy_src_to_dest( FILE_IS_M4A, detail, PConv );
			}

			if( FALSE == conv.bool_stop &&  detail->EtatSelection_Aac > ETAT_ATTENTE_EXIST ) {

				gchar	**PtrTabArgs = NULL;

				// REAJUSTE LA QUANTIFICATION
				if (TRUE == PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_AAC ]) {

					if (Bits > 64)				NewBits = 64;
					else if (Bits > 32 && Bits < 64)	NewBits = 32;
					else if (Bits > 24 && Bits < 32)	NewBits = 24;
					else if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 24 : 16;
					else if (Bits > 8 && Bits < 16)		NewBits = (Bits - 8) < 3 ? 16 : 8;
					else if (Bits < 8)			NewBits = 8;

					NewBits = Bits;

					VarFileConv.BoolResultSoxIsOk = fileconv_change_quantification_with_sox( PConv, Hertz, Channels, NewBits );
				}
				else {
					conv_copy_src_to_dest( PConv->tmp_wav, PConv->tmp_sox);
				}

				if (Bits == 24) {

					PtrTabArgs = conv_with_sox_float_get_param(
							PConv->tmp_sox,
							PConv->tmp_sox_24
							);
					conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					conv_copy_src_to_dest( PConv->tmp_sox_24, PConv->tmp_sox);
				}

				if (Hertz < 44100) {
					PtrTabArgs = filelc_AllocTabArgs();
					pos = 3;
					PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
					PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
					PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_sox);
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup ("pcm");
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s",PConv->tmp_sox_24);
					PtrTabArgs [ pos++ ] = g_strdup ("-srate");
					PtrTabArgs [ pos++ ] = g_strdup ("44100");
					PtrTabArgs [ pos++ ] = NULL;
					VarFileConv.BoolResultMplayerIsOk = conv_to_convert( PtrTabArgs, FALSE, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					VarFileConv.BoolResultMplayerIsOk = libutils_test_file_exist( PConv->tmp_sox_24 );
					conv_copy_src_to_dest( PConv->tmp_sox_24, PConv->tmp_sox);
				}

				param_filelc.type_conv            = AACPLUSENC_WAV_TO_AAC;
				param_filelc.With_CommandLineUser = detail->EtatSelection_Aac >= ETAT_SELECT_EXPERT;
				param_filelc.filesrc              = PConv->tmp_sox;
				param_filelc.filedest             = PConv->tmp_aac;
				param_filelc.tags                 = NULL;
				param_filelc.cdrom                = NULL;
				param_filelc.num_track            = NULL;
				param_filelc.PtrStrBitrate        = NULL;
			pthread_mutex_lock( &mutex );
				PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, AACPLUSENC_WAV_TO_AAC, "AACPLUSENC_WAV_TO_AAC");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				conv.Bool_MAJ_select = TRUE;
				fileconv_copy_src_to_dest( FILE_IS_AAC, detail, PConv );
			}

			if( FALSE == conv.bool_stop &&  detail->EtatSelection_Mpc > ETAT_ATTENTE_EXIST ) {

				gchar	**PtrTabArgs = NULL;

				// REAJUSTE LA QUANTIFICATION
				if( Bits != 64 || Bits != 32 || Bits != 24 || Bits != 16 || Bits != 8 ) {

					if (Bits > 64)				NewBits = 64;
					else if (Bits > 32 && Bits < 64)	NewBits = 32;
					else if (Bits > 24 && Bits < 32)	NewBits = 24;
					else if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 24 : 16;
					else if (Bits > 8 && Bits < 16)		NewBits = (Bits - 8) < 3 ? 16 : 8;
					else if (Bits < 8)			NewBits = 8;

					NewBits = Bits;

					VarFileConv.BoolResultSoxIsOk = fileconv_change_quantification_with_sox( PConv, Hertz, Channels, NewBits );
				}
				else {
					conv_copy_src_to_dest( PConv->tmp_wav, PConv->tmp_sox);
				}

				// SET WITH FLOAT FORMAT IF Bits == 24 || 32
				if (Bits == 32 || Bits == 24) {

					PtrTabArgs = conv_with_sox_float_get_param(
							PConv->tmp_sox,
							PConv->tmp_sox_24
							);
					conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					conv_copy_src_to_dest( PConv->tmp_sox_24, PConv->tmp_sox);
				}

				if (Hertz < 44100) {
					PtrTabArgs = filelc_AllocTabArgs();
					pos = 3;
					PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
					PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
					PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
					PtrTabArgs [ pos++ ] = g_strdup (PConv->tmp_sox);
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup ("pcm");
					PtrTabArgs [ pos++ ] = g_strdup ("-ao");
					PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", PConv->tmp_sox_24);
					PtrTabArgs [ pos++ ] = g_strdup ("-srate");
					PtrTabArgs [ pos++ ] = g_strdup ("44100");
					PtrTabArgs [ pos++ ] = NULL;
					VarFileConv.BoolResultMplayerIsOk = conv_to_convert( PtrTabArgs, FALSE, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					VarFileConv.BoolResultMplayerIsOk = libutils_test_file_exist( PConv->tmp_sox_24 );
					conv_copy_src_to_dest( PConv->tmp_sox_24, PConv->tmp_sox);
				}

				param_filelc.type_conv            = MPPENC_WAV_TO_MPC;
				param_filelc.With_CommandLineUser = detail->EtatSelection_Mpc >= ETAT_SELECT_EXPERT;
				param_filelc.filesrc              = PConv->tmp_sox;
				param_filelc.filedest             = PConv->tmp_mpc;
				param_filelc.tags                 = InfosTags;
				param_filelc.cdrom                = NULL;
				param_filelc.num_track            = NULL;
				param_filelc.PtrStrBitrate        = NULL;
			pthread_mutex_lock( &mutex );
				PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, MPPENC_WAV_TO_MPC, "MPPENC_WAV_TO_MPC");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				conv.Bool_MAJ_select = TRUE;
				fileconv_copy_src_to_dest( FILE_IS_MPC, detail, PConv );
			}

			if( FALSE == conv.bool_stop &&  detail->EtatSelection_Mp3 > ETAT_ATTENTE_EXIST ) {

				gchar	**PtrTabArgs = NULL;

				// REAJUSTE LA QUANTIFICATION
				if( detail->type_infosong_file_is == FILE_IS_VID_M4A ||
					detail->type_infosong_file_is == FILE_IS_M4A ) {

					NewBits = 16;
			// pthread_mutex_lock( &mutex );
					VarFileConv.BoolResultSoxIsOk = fileconv_change_quantification_with_sox( PConv, 44100, 2, 16 );
			// pthread_mutex_unlock( &mutex );
				}
				else if( Hertz == 48000 || Bits == 32 ) {
					PtrTabArgs = conv_with_sox_float_get_param(
							PConv->tmp_wav,
							PConv->tmp_sox
							);

					conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				} else  {

					// CORRECTION
					// http://forum.ubuntu-fr.org/viewtopic.php?pid=3271586#p3271586
					// joelab07

					if (Bits < 8)			NewBits = 8;
					if (Bits > 16 && Bits < 24)	NewBits = (Bits - 16) < 3 ? 16 : 24;
					if (Bits > 24 && Bits < 32)	NewBits = (Bits - 24) < 3 ? 24 : 32;

					NewBits = Bits;
					VarFileConv.BoolResultSoxIsOk = fileconv_change_quantification_with_sox( PConv, Hertz, Channels, NewBits );
				}

				param_filelc.type_conv            = LAME_WAV_TO_MP3;
				param_filelc.With_CommandLineUser = detail->EtatSelection_Mp3 >= ETAT_SELECT_EXPERT;
				param_filelc.filesrc              = PConv->tmp_sox;
				param_filelc.filedest             = PConv->tmp_mp3;
				param_filelc.tags                 = InfosTags;
				param_filelc.cdrom                = NULL;
				param_filelc.num_track            = NULL;
				param_filelc.BoolSetBitrate       = FALSE;
				param_filelc.PtrStrBitrate        = options_get_params (LAME_WAV_TO_MP3);
			pthread_mutex_lock( &mutex );
				PtrTabArgs = filelc_get_command_line (&param_filelc);
			pthread_mutex_unlock( &mutex );
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, LAME_WAV_TO_MP3, "LAME_WAV_TO_MP3");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				conv.Bool_MAJ_select = TRUE;
				fileconv_copy_src_to_dest( FILE_IS_MP3, detail, PConv );
			}

			// COPIE DOSSIER TEMPORAIRE VERS DESTINATION
			// fileconv_copy_src_to_dest( detail, PConv );

			g_unlink (PConv->tmp_flac);
			g_unlink (PConv->tmp_wav);
			g_unlink (PConv->tmp_sox);
			g_unlink (PConv->tmp_sox_24);
			g_unlink (PConv->tmp_mp3);
			g_unlink (PConv->tmp_ogg);
			g_unlink (PConv->tmp_m4a);
			g_unlink (PConv->tmp_aac);
			g_unlink (PConv->tmp_shn);
			g_unlink (PConv->tmp_wma);
			g_unlink (PConv->tmp_rm);
			g_unlink (PConv->tmp_dts);
			g_unlink (PConv->tmp_mpc);
			g_unlink (PConv->tmp_ape);
			g_unlink (PConv->tmp_wavpack);
			g_unlink (PConv->tmp_wavpack_md5);
			g_unlink (PConv->tmp_ac3);
		}

		if (TRUE == conv.bool_stop)
			break;
		else
			ListGlobal = g_list_next(ListGlobal);
	}

	// close(conv.tube_conv [ 0 ]);

	VarFileConv.bool_thread_conv_one = FALSE;

	PRINT("FIN THREAD FILECONV ONE");
	pthread_exit(0);
}
//
//
static gint fileconv_timeout (gpointer data)
{
	if (TRUE == conv.BoolIsa52dec) {
		WindScan_set_pulse ();
	}
	if (TRUE == conv.BoolIsa52dec || TRUE == conv.BoolIsExtract || TRUE == conv.BoolIsConvert || TRUE == conv.BoolIsCopy || TRUE == conv.BoolIsNormalise || TRUE == conv.BoolIsReplaygain) {
		gchar	Str [ 200 ];

		Str [ 0 ] = '\0';

		if (TRUE == conv.BoolIsExtract) {
			strcat (Str, "<b><i>Extraction</i></b> ");
		}
		if (TRUE == conv.BoolIsConvert) {
			strcat (Str, "<b><i>Conversion</i></b> ");
		}
		if (TRUE == conv.BoolIsCopy) {
			strcat (Str, "<b><i>Copie</i></b> ");
		}
		if (TRUE == conv.BoolIsNormalise) {
			strcat (Str, "<b><i>Normalise</i></b> ");
		}
		if (TRUE == conv.BoolIsReplaygain) {
			strcat (Str, "<b><i>Replaygain</i></b>");
		}

		WindScan_set_label (Str);
	}
	if (TRUE == conv.bool_percent_conv) {

		gchar	*Str = NULL;

		// DEBUG
		if( conv.total_percent > 1.0 ) conv.total_percent = 1.0;
		Str = g_strdup_printf ("%d%%", (int)(conv.total_percent * 100));
		WindScan_set_progress (Str, conv.total_percent);
		g_free (Str);
		Str = NULL;
		conv.bool_percent_conv = FALSE;
	}
	if (TRUE == conv.Bool_MAJ_select) {
		conv.Bool_MAJ_select = FALSE;
		file_pixbuf_update_glist ();
	}
	else if( FALSE == VarFileConv.bool_thread_conv_one && FALSE == VarFileConv.bool_thread_conv_two && FALSE == VarFileConv.bool_thread_conv_three ) {

		DETAIL		*detail = NULL;
		CONV_FIC	*PConv = NULL;
		GList		*list = NULL;

		pthread_mutex_destroy( &mutex );

		// SUPPRESSION DE LA STRUCTURE DE CONVERSIONS
		list = g_list_first (entetefile);
		while (list) {
			if (NULL != (detail = (DETAIL *)list->data)) {
				if (NULL != (PConv = (CONV_FIC *)detail->PConv)) {

					g_free (detail->NameFileCopyFromNormalizate);		detail->NameFileCopyFromNormalizate = NULL;

					g_free (PConv->dest_flac);		PConv->dest_flac = NULL;
					g_free (PConv->dest_wav);		PConv->dest_wav = NULL;
					g_free (PConv->dest_mp3);		PConv->dest_mp3 = NULL;
					g_free (PConv->dest_ogg);		PConv->dest_ogg = NULL;
					g_free (PConv->dest_m4a);		PConv->dest_m4a = NULL;
					g_free (PConv->dest_aac);		PConv->dest_aac = NULL;
					g_free (PConv->dest_mpc);		PConv->dest_mpc = NULL;
					g_free (PConv->dest_ape);		PConv->dest_ape = NULL;
					g_free (PConv->dest_wavpack);		PConv->dest_wavpack = NULL;
					g_free (PConv->dest_wavpack_md5);	PConv->dest_wavpack_md5 = NULL;
					g_free (PConv->tmp_flac);		PConv->tmp_flac = NULL;
					g_free (PConv->tmp_wav);		PConv->tmp_wav = NULL;
					g_free (PConv->tmp_mp3);		PConv->tmp_mp3 = NULL;
					g_free (PConv->tmp_ogg);		PConv->tmp_ogg = NULL;
					g_free (PConv->tmp_m4a);		PConv->tmp_m4a = NULL;
					g_free (PConv->tmp_aac);		PConv->tmp_aac = NULL;
					g_free (PConv->tmp_shn);		PConv->tmp_shn = NULL;
					g_free (PConv->tmp_wma);		PConv->tmp_wma = NULL;
					g_free (PConv->tmp_rm);			PConv->tmp_rm = NULL;
					g_free (PConv->tmp_dts);		PConv->tmp_dts = NULL;
					g_free (PConv->tmp_mpc);		PConv->tmp_mpc = NULL;
					g_free (PConv->tmp_ape);		PConv->tmp_ape = NULL;
					g_free (PConv->tmp_wavpack);		PConv->tmp_wavpack = NULL;
					g_free (PConv->tmp_wavpack_md5);	PConv->tmp_wavpack_md5 = NULL;
					g_free (PConv->tmp_sox);		PConv->tmp_sox = NULL;
					g_free (PConv->tmp_sox_24);		PConv->tmp_sox_24 = NULL;
					g_free (PConv->tmp_ac3);		PConv->tmp_ac3 = NULL;

					g_free (PConv);
					PConv = detail->PConv = NULL;
				}
			}
			list = g_list_next(list);
		}

		// DELETTE TEMPORAY REP
		if (NULL != conv.TmpRep)  {
			conv.TmpRep  = libutils_remove_temporary_rep (conv.TmpRep);
		}

		// EST FERMEE DEPUIS: fileaction_ ...
		// WindScan_close ();

		PRINT("FIN TIMEOUT FILECONV");
		g_source_remove (conv.handler_timeout_conv);
		file_pixbuf_update_glist ();
		fileaction_set_end (TYPE_CONVERSION);

		if( FALSE == VarFileConv.BoolResultMplayerIsOk ) {
			wind_info_init (
				WindMain,
				_("Error since MPLAYER !"),
				_("You can enable XCFA command line with:         "),
				  "\n",
				_("$ xcfa -verbose"),
				  "\n",
				_("to see the type of error returned by mplayer."),
			  	"");
			VarFileConv.BoolResultMplayerIsOk = FALSE;
		}
		else if( FALSE == VarFileConv.BoolResultSoxIsOk ) {
			wind_info_init (
				WindMain,
				_("Error since SOX !"),
				_("You can enable XCFA command line with:         "),
				  "\n",
				_("$ xcfa -verbose"),
				  "\n",
				_("to see the type of error returned by sox."),
			  	"");
			VarFileConv.BoolResultSoxIsOk = FALSE;
		}
	}
	return (TRUE);
}
//
//
void fileconv_get_total_convert (void)
{
	GList		*ListGlobal = NULL;
	DETAIL		*detail = NULL;
	CONV_FIC	*PConv = NULL;
	gint		Channels;
	gint		Hertz;
	gint		Bits;

	ListGlobal = g_list_first (entetefile);
	while (ListGlobal) {
		if (NULL != (detail = (DETAIL *)ListGlobal->data) && NULL != (PConv = (CONV_FIC *)detail->PConv)) {

			// ------------------------------------------------------------------------
			//
			// FROM:
			//	FLAC | APE | WAVPACK | OGG | M4A | MPC | MP3 | WMA | SHORTEN | RM | DTS | AIF | AC3
			// TO:
			//	WAV
			//
			// ------------------------------------------------------------------------

			if (NULL == detail->NameFileCopyFromNormalizate) {
				conv.total_convert ++;
			}

			switch( detail->type_infosong_file_is ) {
			case FILE_IS_NONE :
			// case FILE_IS_VID_M4A :
			case FILE_IS_WAVPACK_MD5 :
			case FILE_TO_NORMALISE :
			case FILE_TO_NORMALISE_COLLECTIF :
			case FILE_TO_REPLAYGAIN :
			case FILE_IS_AAC :
			break;

			case FILE_IS_WAV :
			{
				INFO_WAV *info = (INFO_WAV *)detail->info;

				if( TRUE == info->BoolBwf ) {
					conv.total_convert ++;
					conv.total_convert ++;

				}
				break;
			}
			case FILE_IS_FLAC :
			{
				conv.total_convert ++;
				conv.total_convert ++;
				break;
			}
			case FILE_IS_APE :
			{
				conv.total_convert ++;
				conv.total_convert ++;
				break;
			}
			case FILE_IS_WAVPACK :
			{
				conv.total_convert ++;
				conv.total_convert ++;
				conv.total_convert ++;
				break;
			}
			case FILE_IS_OGG :
			case FILE_IS_MPC :
			case FILE_IS_MP3 :
			case FILE_IS_AC3 :
			{
				if (FALSE == conv.bool_stop && NULL == detail->NameFileCopyFromNormalizate) {
					conv.total_convert ++;
				}
				conv.total_convert ++;
				break;
			}
			case FILE_IS_M4A :
			case FILE_IS_VID_M4A :
			{
				conv.total_convert ++;
				conv.total_convert ++;
				break;
			}
			case FILE_IS_WMA :
			case FILE_IS_RM :
			case FILE_IS_DTS :
			case FILE_IS_AIFF :
			{
				conv.total_convert ++;
				conv.total_convert ++;
				break;
			}
			case FILE_IS_SHN :
			{
				conv.total_convert ++;
				conv.total_convert ++;
				break;
			}

			conv.total_convert ++;

			} // swicth

			// ------------------------------------------------------------------------
			//
			// FROM:
			//	WAV
			// TO:
			//	FLAC | APE | WAVPACK | OGG | M4A | AAC | MPC | MP3
			//
			// ------------------------------------------------------------------------

			if( detail->type_infosong_file_is == FILE_IS_WAV ) {
				tagswav_file_GetBitrate( detail->namefile, &Channels, &Hertz, &Bits );
			}
			else {
				Channels = 2;
				Hertz    = 44100;
				Bits     = 32;
			}

			if( detail->EtatSelection_Flac > ETAT_ATTENTE_EXIST ) {

				if( Bits == 32 ) {
					conv.total_convert ++;
				}
				conv.total_convert ++;
				conv.total_convert ++;
			}

			if( detail->EtatSelection_Ape > ETAT_ATTENTE_EXIST ) {

				if( Bits >= 24 ) {
					conv.total_convert ++;
				}
				conv.total_convert ++;
				conv.total_convert ++;
			}

			if( detail->EtatSelection_WavPack > ETAT_ATTENTE_EXIST ) {

				conv.total_convert ++;
				conv.total_convert ++;
			}

			if( detail->EtatSelection_Ogg > ETAT_ATTENTE_EXIST ) {

				if (TRUE == PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_OGG ]) {
					conv.total_convert ++;
				}
				if (Bits == 24) {
					conv.total_convert ++;
					conv.total_convert ++;
				}
				conv.total_convert ++;
				conv.total_convert ++;
			}

			if( detail->EtatSelection_M4a > ETAT_ATTENTE_EXIST ) {

				conv.total_convert ++;
				if (Bits == 32 || Bits == 24) {
					conv.total_convert ++;
					conv.total_convert ++;
				}
				conv.total_convert ++;
				conv.total_convert ++;
			}

			if( detail->EtatSelection_Aac > ETAT_ATTENTE_EXIST ) {

				conv.total_convert ++;
				if (Bits == 24) {
					conv.total_convert ++;
					conv.total_convert ++;
				}

				if (Hertz < 44100) {
					conv.total_convert ++;
					conv.total_convert ++;
				}
				conv.total_convert ++;
				conv.total_convert ++;
			}

			if( detail->EtatSelection_Mpc > ETAT_ATTENTE_EXIST ) {

				conv.total_convert ++;
				if (Bits == 32 || Bits == 24) {
					conv.total_convert ++;
					conv.total_convert ++;
				}
				if (Hertz < 44100) {
					conv.total_convert ++;
					conv.total_convert ++;
				}
				conv.total_convert ++;
				conv.total_convert ++;
			}

			if( detail->EtatSelection_Mp3 > ETAT_ATTENTE_EXIST ) {

				conv.total_convert ++;
				conv.total_convert ++;
				conv.total_convert ++;
			}
		}
		ListGlobal = g_list_next(ListGlobal);
	}
}
// PREPARE LA LISTE POUR LES CONVERSIONS
//
void fileconv_set_flags_before_conversion_file (void)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	CONV_FIC	*PConv = NULL;
	gint		CptEtat;
	gint		NUm = 1;

	VarFileConv.BoolResultMplayerIsOk = TRUE;
	VarFileConv.BoolResultSoxIsOk = TRUE;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {

			// SI AUCUNE CONVERSION DANS CETTE LIGNE, CONTINUE
			if (detail->EtatSelection_Wav <= ETAT_ATTENTE_EXIST &&
				detail->EtatSelection_Flac <= ETAT_ATTENTE_EXIST &&
				detail->EtatSelection_Ape <= ETAT_ATTENTE_EXIST &&
				detail->EtatSelection_WavPack <= ETAT_ATTENTE_EXIST &&
				detail->EtatSelection_Ogg <= ETAT_ATTENTE_EXIST &&
				detail->EtatSelection_M4a <= ETAT_ATTENTE_EXIST &&
				detail->EtatSelection_Aac <= ETAT_ATTENTE_EXIST &&
				detail->EtatSelection_Mpc <= ETAT_ATTENTE_EXIST &&
				detail->EtatSelection_Mp3 <= ETAT_ATTENTE_EXIST) {

				list = g_list_next(list);
				continue;
			}
			// CREATION STRUCTURE
			detail->PConv = (CONV_FIC *)g_malloc0 (sizeof (CONV_FIC));
			PConv = detail->PConv;


			// Create tmp rep
			if( NULL == conv.TmpRep )
				conv.TmpRep  = libutils_create_temporary_rep (Config.PathnameTMP, PATH_TMP_XCFA_AUDIOFILE);

			// INIT ETAT
			for (CptEtat = 0; CptEtat < ETAT_BOOL_TOTAL; CptEtat ++) {
				PConv->BoolEtatConv [ CptEtat ] = FALSE;
			}

			// PREPARATION DES PATHNAMES DE DESTINATION
			PConv->dest_flac        = file_get_pathname_dest (detail, "flac");
			PConv->dest_wav         = file_get_pathname_dest (detail, "wav");
			PConv->dest_mp3         = file_get_pathname_dest (detail, "mp3");
			PConv->dest_ogg         = file_get_pathname_dest (detail, "ogg");
			PConv->dest_m4a         = file_get_pathname_dest (detail, "m4a");
			PConv->dest_aac         = file_get_pathname_dest (detail, "aac");
			PConv->dest_mpc         = file_get_pathname_dest (detail, "mpc");
			PConv->dest_ape         = file_get_pathname_dest (detail, "ape");
			PConv->dest_wavpack     = file_get_pathname_dest (detail, "wv");
			PConv->dest_wavpack_md5 = file_get_pathname_dest (detail, "wvc");

			// PREPARATION DES PATHNAMES TEMPORAIRES
			PConv->tmp_flac         = g_strdup_printf ("%s/tmp_%d.flac",    conv.TmpRep, NUm );
			if (NULL != detail->NameFileCopyFromNormalizate)
				PConv->tmp_wav = g_strdup (detail->NameFileCopyFromNormalizate);
			else	PConv->tmp_wav = g_strdup_printf ("%s/tmp_%d.wav", conv.TmpRep, NUm );
			PConv->tmp_sox         = g_strdup_printf ("%s/tmp_sox_%d.wav", conv.TmpRep, NUm );
			PConv->tmp_sox_24      = g_strdup_printf ("%s/tmp_sox_24_%d.wav", conv.TmpRep, NUm );
			PConv->tmp_mp3         = g_strdup_printf ("%s/tmp_%d.mp3",     conv.TmpRep, NUm );
			PConv->tmp_ogg         = g_strdup_printf ("%s/tmp_%d.ogg",     conv.TmpRep, NUm );
			PConv->tmp_m4a         = g_strdup_printf ("%s/tmp_%d.m4a",     conv.TmpRep, NUm );
			PConv->tmp_aac         = g_strdup_printf ("%s/tmp_%d.aac",     conv.TmpRep, NUm );
			PConv->tmp_shn         = g_strdup_printf ("%s/tmp_%d.shn",     conv.TmpRep, NUm );
			PConv->tmp_wma         = g_strdup_printf ("%s/tmp_%d.wma",     conv.TmpRep, NUm );
			PConv->tmp_rm          = g_strdup_printf ("%s/tmp_%d.rm",      conv.TmpRep, NUm );
			PConv->tmp_dts         = g_strdup_printf ("%s/tmp_%d.dts",     conv.TmpRep, NUm );
			PConv->tmp_aiff        = g_strdup_printf ("%s/tmp_%d.aiff",    conv.TmpRep, NUm );
			PConv->tmp_mpc         = g_strdup_printf ("%s/tmp_%d.mpc",     conv.TmpRep, NUm );
			PConv->tmp_ape         = g_strdup_printf ("%s/tmp_%d.ape",     conv.TmpRep, NUm );
			PConv->tmp_wavpack     = g_strdup_printf ("%s/tmp_%d.wv",      conv.TmpRep, NUm );
			PConv->tmp_wavpack_md5 = g_strdup_printf ("%s/tmp_%d.wvc",     conv.TmpRep, NUm );
			PConv->tmp_ac3         = g_strdup_printf ("%s/tmp_%d.ac3",     conv.TmpRep, NUm );

			NUm ++;

			if( detail->type_infosong_file_is == FILE_IS_WAV ) {

				gint		 Channels;
				gint		 Hertz;
				gint		 Bits;

				tagswav_file_GetBitrate( detail->namefile, &Channels, &Hertz, &Bits );

				PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_OGG ] =
					detail->EtatSelection_Ogg > ETAT_ATTENTE_EXIST &&
					(Bits != 24 && Bits != 16 && Bits != 8);

				PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_M4A ] =
					detail->EtatSelection_M4a > ETAT_ATTENTE_EXIST &&
					(Bits != 64 && Bits != 32 && Bits != 24 && Bits != 16 && Bits != 8);

				PConv->BoolEtatConv [ ETAT_FROM_WAV_TO_AAC ] =
					detail->EtatSelection_Aac > ETAT_ATTENTE_EXIST &&
					(Bits != 64 && Bits != 32 && Bits != 24 && Bits != 16 && Bits != 8);
			}
		}
		list = g_list_next(list);
	}

	fileconv_get_total_convert();
	/*
	g_print("\n");
	g_print("conv.total_convert      = %d\n", conv.total_convert);
	g_print("conv.total_percent      = %f\n", conv.total_percent);
	g_print("conv.rip_completed      = %d\n", conv.rip_completed);
	g_print("conv.encode_completed   = %d\n", conv.encode_completed);
	g_print("conv.extract_percent    = %f\n", conv.extract_percent);
	g_print("conv.conversion_percent = %f\n", conv.conversion_percent);
	g_print("conv.total_rip          = %d\n", conv.total_rip);
	g_print("\n");
	*/
}

//
//
void fileconv_action (void)
{
	pthread_t  nmr_tid;

	// WindScan_open ("Conversions files", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Conversions files",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Conversions files ...</i></b>");

	conv_reset_struct (WindScan_close_request);

	fileconv_set_flags_before_conversion_file ();

	VarFileConv.bool_thread_conv_one = TRUE;
 	VarFileConv.bool_thread_conv_two = FALSE;
 	VarFileConv.bool_thread_conv_three = FALSE;

	pthread_mutex_init( &mutex , NULL ) ;

	VarFileConv.bool_thread_conv_one = TRUE;
  	pthread_create (&nmr_tid, NULL ,(void *)fileconv_thread_one, (void *)NULL);

	PRINT("DEBUT TIMEOUT FILECONV");
	conv.handler_timeout_conv = g_timeout_add( 100, fileconv_timeout, 0 );
}




