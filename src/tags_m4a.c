 /*
 *  file    : tags_m4a.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "get_info.h"
#include "tags.h"
#include <taglib/tag_c.h>


/*
*---------------------------------------------------------------------------
* REMOVE
*---------------------------------------------------------------------------
*/
INFO_M4A *tagsm4a_remove_info (INFO_M4A *info)
{
	if (info) {
		if (NULL != info->time)		{ g_free (info->time); info->time = NULL;	}
		if (NULL != info->hertz)	{ g_free (info->hertz);info->hertz = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}
	return ((INFO_M4A *)NULL);
}

/*
*---------------------------------------------------------------------------
* GET INFO
*---------------------------------------------------------------------------
*/
INFO_M4A *tagsm4a_get_info (DETAIL *detail)
{
	gchar		**Larrbuf = NULL;
	GString		*gstr = NULL;
	gint		cpt;
	gchar		*ptr = NULL;
	INFO_M4A	*ptrinfo = NULL;
	gint		m;
	gint		s;
	gint		sec;

	gstr = GetInfo_faad (detail->namefile);
	Larrbuf = g_strsplit (gstr->str, "\n", 0);

	ptrinfo = (INFO_M4A *)g_malloc0 (sizeof (INFO_M4A));
	if (ptrinfo == NULL) {
		g_strfreev(Larrbuf);
		g_string_free (gstr, TRUE);
		return (NULL);
	}
	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);

	for (cpt=0; Larrbuf[cpt]; cpt++) {

		// LC AAC	181.287 secs, 2 ch, 44100 Hz
		// LC AAC	7169.049 secs, 2 ch, 44100 Hz
		if ((ptr = strstr (Larrbuf[cpt], "LC AAC"))) {

			// --> 181.287 secs, 2 ch, 44100 Hz
			// --> 7169.049 secs, 2 ch, 44100 Hz
			ptr = strstr (ptr, " secs,");
			ptr --;
			while (*ptr != ' ' && *ptr != '\t') ptr --;
			ptr ++;
			
			sec = atoi (ptr);
			s = sec % 60; sec /= 60;
			m = sec % 60; sec /= 60;
			if (sec > 0) ptrinfo->time = g_strdup_printf ("%02d:%02d:%02d", sec, m, s);
			else         ptrinfo->time = g_strdup_printf ("%02d:%02d", m, s);

			ptrinfo->SecTime = sec;

			/* --> 44100 Hz */
			ptr = strstr (ptr, " Hz");
			ptr --;
			while (*ptr != ' ' && *ptr != '\t') ptr --;
			ptr ++;
			ptrinfo->hertz = g_strdup_printf ("%d", atoi(ptr));
		}
		else if ((ptr = strstr (Larrbuf[cpt], "artist:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Artist = g_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "title:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Title = g_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "album:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Album = g_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "date:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Year = g_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "genre:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Genre = g_strdup (ptr);
			}
		}
		else if ((ptr = strstr (Larrbuf[cpt], "comment:"))) {
			if ((ptr = strchr (ptr, ' '))) {
				ptr ++;
				ptrinfo->tags->Comment = g_strdup (ptr);
			}
		}

	}

	g_strfreev(Larrbuf);
	g_string_free (gstr, TRUE);

	tags_set (detail->namefile, ptrinfo->tags);

	return ((INFO_M4A *)ptrinfo);
}

