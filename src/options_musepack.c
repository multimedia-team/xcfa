 /*
 *  file      : options_musepack.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "options.h"



static 	gchar *str_val_preset[] = {
		"--telephone",
		"--thumb",
		"--radio",
		"--standard",
		"--xtreme",
		"--insane",
		"--braindead"
	};
gchar *optionsMusepack_get_quality_mppenc (void)
{
	return ( (gchar *)str_val_preset[ gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Mppenc)) ]);
}



void on_combobox_mppenc_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_Mppenc = GTK_COMBO_BOX (widget);

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("telephone: lowest quality              :   32 ..  48 kbit/s"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("thumb      : low quality/internet     :   58 ..  86 kbit/s"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("radio        : medium (MP3) quality : 112 .. 152 kbit/s"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("standard  : high quality (dflt)         : 142 .. 184 kbit/s"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("xtreme     : extreme high quality   : 168 .. 212 kbit/s"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("insane      : extreme high quality   : 232 .. 268 kbit/s"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("braindead : extreme high quality   : 232 .. 278 kbit/s"));
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.QualityMppenc);
}
void on_combobox_mppenc_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_Mppenc) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Mppenc))) >= 0)
			Config.QualityMppenc = ind;
		
		OptionsInternal_set_datas_interne (COLOR_MUSEPACK_QUALITE, var_options.Adr_label_musepack_mpc, MPPENC_WAV_TO_MPC);
	}
}












