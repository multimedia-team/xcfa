 /*
 *  file      : options_oggenc.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "options.h"




static 	gchar *val_bitrate_ogg[] = {
	"--bitrate=45",
	"--bitrate=64",
	"--bitrate=80",
	"--bitrate=96",
	"--bitrate=112",
	"--bitrate=128",
	"--bitrate=160",
	"--bitrate=192",
	"--bitrate=224",
	"--bitrate=256",
	"--bitrate=320",
	"--quality=-1",
	"--quality=0",
	"--quality=1",
	"--quality=2",
	"--quality=3",
	"--quality=4",
	"--quality=5",
	"--quality=6",
	"--quality=7",
	"--quality=8",
	"--quality=9",
	"--quality=10"
	};


// 
// 
gchar *optionsOggenc_get_param( void )
{
	gchar StrOggenc[ 1000 ];
	
	strcpy (StrOggenc, val_bitrate_ogg [ Config.BitrateOggenc ]);
	
	if (Config.BitrateOggenc < 11 && Config.ManagedOggenc == 0) {
		if (*StrOggenc != '\0') strcat (StrOggenc, " ");
		strcat (StrOggenc, "--managed");
	}
	if (Config.BitrateOggenc < 9 && Config.DownmixOggenc == 0) {
		if (*StrOggenc != '\0') strcat (StrOggenc, " ");
		strcat (StrOggenc, "--downmix");
	}
	
	return ((gchar *)strdup (StrOggenc));
}
// 
// 
gchar *optionsOggenc_get_val_bitrate_oggenc (void)
{
	return (val_bitrate_ogg [ gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_bitrate)) ]);
}
// 
// 
gboolean optionsOggenc_get_bool_managed_oggenc (void)
{
	if( gtk_widget_is_sensitive(GTK_WIDGET (GLADE_GET_OBJECT("eventbox_combobox_oggenc_managed"))) == TRUE) {
		if (gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_managed)) == 0) return (TRUE);
	}
	return (FALSE);
}
// 
// 
gboolean optionsOggenc_get_bool_downmix_oggenc (void)
{
	if (gtk_widget_is_sensitive(GTK_WIDGET (GLADE_GET_OBJECT("eventbox_combobox_oggenc_downmix"))) == TRUE) {
		if (gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_downmix)) == 0) return (TRUE);
	}
	return (FALSE);
}
// 
// 
void on_combobox_oggenc_bitrate_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_Oggenc_bitrate = GTK_COMBO_BOX (widget);

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), " 45  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), " 64  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), " 80  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), " 96  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "112  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "128  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "160  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "192  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "224  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "256  kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "320  kbit/s");

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality -1   (Poor quality)"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  0"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  1"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  2"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  3"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  4"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  5"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  6"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  7"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  8"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  9"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Quality  10  (Best quality)"));

	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.BitrateOggenc);
}
// 
// 
void on_combobox_oggenc_bitrate_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_Oggenc_bitrate) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_bitrate))) >= 0)
			Config.BitrateOggenc = ind;
		
		gtk_widget_set_sensitive (
			GTK_WIDGET (GLADE_GET_OBJECT("eventbox_combobox_oggenc_managed")),
			gtk_combo_box_get_active (var_options.Adr_Widget_Oggenc_bitrate) < 11 ? TRUE : FALSE);

		gtk_widget_set_sensitive (
			GTK_WIDGET (GLADE_GET_OBJECT("eventbox_combobox_oggenc_downmix")),
			gtk_combo_box_get_active (var_options.Adr_Widget_Oggenc_bitrate) < 9 ? TRUE : FALSE);
		
		OptionsInternal_set_datas_interne (COLOR_OGGENC_DEBIT, var_options.Adr_label_oggenc_ogg, OGGENC_WAV_TO_OGG);
	}
}
// 
// 
void on_combobox_oggenc_managed_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Yes"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("No"));

	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.ManagedOggenc);

	var_options.Adr_Widget_Oggenc_managed = GTK_COMBO_BOX (widget);
}
void on_combobox_oggenc_managed_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_Oggenc_managed) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_managed))) >= 0)
			Config.ManagedOggenc = ind;
		OptionsInternal_set_datas_interne (COLOR_OGGENC_MANAGED, var_options.Adr_label_oggenc_ogg, OGGENC_WAV_TO_OGG);
	}
}
// 
// 
void on_combobox_oggenc_downmix_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Force mono"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Stereo"));

	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.DownmixOggenc);

	var_options.Adr_Widget_Oggenc_downmix = GTK_COMBO_BOX (widget);
}
void on_combobox_oggenc_downmix_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_Oggenc_downmix) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_downmix))) >= 0)
			Config.DownmixOggenc = ind;
		OptionsInternal_set_datas_interne (COLOR_OGGENC_DOWNMIX, var_options.Adr_label_oggenc_ogg, OGGENC_WAV_TO_OGG);
	}
}







