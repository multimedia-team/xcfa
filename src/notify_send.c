 /*
 *  file      : notify_send.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "prg_init.h"
#include "configuser.h"
#include "play_file.h"
#include "notify_send.h"



typedef struct {
	pthread_t	 nmr_tid;
	gchar		*Title;
	gchar		*Str;
} VAR_NOTIFYSEND ;

VAR_NOTIFYSEND NotifySend;



// 
// 
void NotifySend_call (void)
{
	pid_t  pid;

	if ((pid = fork ()) == 0) {
		execlp ("notify-send",
			"notify-send",
			NotifySend.Title,
			NotifySend.Str,
			NULL);
		_exit (0);
	}
}
// 
// 
static void NotifySend_thread (void *arg)
{
	NotifySend_call ();
	pthread_exit(0);
}
// 
// 
void NotifySend_remove (void)
{
	if( NotifySend.Title != NULL ) {
		g_free( NotifySend.Title );
		NotifySend.Title = NULL;
	}
	if( NotifySend.Str != NULL ) {
		g_free( NotifySend.Str );
		NotifySend.Str = NULL;
	}
}
// 
// 
void NotifySend_msg( gchar *Title, gchar *Str, gboolean p_BoolStopByUser )
{
	// EN FIN DE CONVERSION:
	// 	#include "play_file.h"
	// 	void PlayFile_play (gchar *PathNameFile)
	// 
	if( TRUE == Config.BoolCheckbuttonEndOfConvert && FALSE == p_BoolStopByUser ) {
		gchar	*PathNameFileMusic = g_strdup_printf( "%s/%s", Config.PathMusicFileEndOfConvert, Config.FileMusicFileEndOfConvert );
		PlayFile_play( PathNameFileMusic );
		g_free( PathNameFileMusic );
		PathNameFileMusic = NULL;
	}
	
	NotifySend_remove ();
	if (FALSE == PrgInit.bool_notify_send) {
		// SUPPRESSION DE L'ALERTE INDIQUANT L'ABSENCE DU PAQUET: LIBNOTIFY-BIN
		// wind_info_init (
		// 	WindMain,
		// 	_("Package NOT FOUND !"),
		// 	_("(Package libnotify-bin): notify-send NOT EXIST"),
		//   	"");
		return;
	}
	NotifySend.Title = g_strdup_printf ("%s", Title);
	NotifySend.Str   = g_strdup_printf ("%s", Str);
	pthread_create (&NotifySend.nmr_tid, NULL ,(void *)NotifySend_thread, (void *)NULL);

}


