 /*
 *  file      : popup.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif


#include "global.h"
#include "poche.h"
#include "cd_audio.h"
#include "dvd.h"
#include "split.h"
#include "popup.h"



typedef struct {
	DETAIL      *detail;
	CD_AUDIO    *Audio;
	TYPE_FILE_IS type_file_is;
} VAR_POPUP;

VAR_POPUP VarPopup;

typedef struct {
	gboolean   Show;
	gint       Type;
	gint       Val;
	gint       Call;
} MENU_VAL;

typedef struct {
	GtkWidget *Widget;
	gchar     *Title;
	MENU_VAL   Op;
} POP_MENU;



#define CASE_POP_MENU_FICHERS 0
#define MAX_POP_MENU_FICHERS 9
POP_MENU PopMenuFichiers [ MAX_POP_MENU_FICHERS ] = {
/*
 WIDGET	TITLE							OP
								SHOW		TYPE	VAL	CALL
*/
{NULL,	gettext_noop("   Deselection globale"),			{TRUE,		0,	0,	CASE_POP_MENU_FICHERS}},
{NULL,	gettext_noop("   Deselection verticale"),		{TRUE,		0,	0,	CASE_POP_MENU_FICHERS +1}},
{NULL,	gettext_noop("   Deselection horizontale"),		{TRUE,		0,	0,	CASE_POP_MENU_FICHERS +2}},
{NULL,	gettext_noop("   ---"),					{FALSE,		-1,	0,	-1}},
{NULL,	gettext_noop("   Selection verticale"),			{TRUE,		0,	0,	CASE_POP_MENU_FICHERS +3}},
{NULL,	gettext_noop("   Selection Expert verticale"),		{TRUE,		0,	0,	CASE_POP_MENU_FICHERS +4}},
{NULL,	gettext_noop("   ---"),					{FALSE,		-1,	0,	-1}},
{NULL,	gettext_noop("   Selection horizontale"),		{TRUE,		0,	0,	CASE_POP_MENU_FICHERS +5}},
{NULL,	gettext_noop("   Selection Expert horizontale  "),	{TRUE,		0,	0,	CASE_POP_MENU_FICHERS +6}}
};


#define CASE_POP_MENU_CD 10
#define MAX_POP_MENU_CD 9
POP_MENU PopMenuCD [ MAX_POP_MENU_CD ] = {
/*
 WIDGET	TITLE							OP
								SHOW		TYPE	VAL	CALL
*/
{NULL,	gettext_noop("   Deselection globale"),			{TRUE,		0,	0,	CASE_POP_MENU_CD}},
{NULL,	gettext_noop("   Deselection verticale"),		{TRUE,		0,	0,	CASE_POP_MENU_CD +1}},
{NULL,	gettext_noop("   Deselection horizontale"),		{TRUE,		0,	0,	CASE_POP_MENU_CD +2}},
{NULL,	gettext_noop("   ---"),					{FALSE,		-1,	0,	-1}},
{NULL,	gettext_noop("   Selection verticale"),			{TRUE,		0,	0,	CASE_POP_MENU_CD +3}},
{NULL,	gettext_noop("   Selection Expert verticale"),		{TRUE,		0,	0,	CASE_POP_MENU_CD +4}},
{NULL,	gettext_noop("   ---"),					{FALSE,		-1,	0,	-1}},
{NULL,	gettext_noop("   Selection horizontale"),		{TRUE,		0,	0,	CASE_POP_MENU_CD +5}},
{NULL,	gettext_noop("   Selection Expert horizontale  "),	{TRUE,		0,	0,	CASE_POP_MENU_CD +6}}
};


#define CASE_POP_MENU_NORMALISE_CD 200
#define MAX_POP_MENU_NORMALISE_CD 3
POP_MENU PopMenuNormaliseCD [ MAX_POP_MENU_NORMALISE_CD ] = {
/*
 WIDGET	TITLE							OP
								SHOW		TYPE	VAL	CALL
*/
{NULL,	gettext_noop("   Deselection verticale    "),		{TRUE,		0,	0,	CASE_POP_MENU_NORMALISE_CD +0}},
{NULL,	gettext_noop("   ---"),					{FALSE,		-1,	0,	-1}},
{NULL,	gettext_noop("   Selection verticale  "),		{TRUE,		0,	0,	CASE_POP_MENU_NORMALISE_CD +1}}
};


#define CASE_POP_MENU_NORMALISE_DVD 300
#define MAX_POP_MENU_NORMALISE_DVD 3
POP_MENU PopMenuNormaliseDVD [ MAX_POP_MENU_NORMALISE_DVD ] = {
/*
 WIDGET	TITLE							OP
								SHOW		TYPE	VAL	CALL
*/
{NULL,	gettext_noop("   Deselection verticale    "),		{TRUE,		0,	0,	CASE_POP_MENU_NORMALISE_DVD +0}},
{NULL,	gettext_noop("   ---"),					{FALSE,		-1,	0,	-1}},
{NULL,	gettext_noop("   Selection verticale  "),		{TRUE,		0,	0,	CASE_POP_MENU_NORMALISE_DVD +1}}
};


#define CASE_POP_MENU_REPLAYGAIN 30
#define MAX_POP_MENU_REPLAYGAIN 6
POP_MENU PopMenuReplayGain[ MAX_POP_MENU_REPLAYGAIN ] = {
/*
 WIDGET	TITLE							OP
								SHOW		TYPE	VAL	CALL
*/
{NULL,	gettext_noop("---SELECTION VERTICALE-----"),		{FALSE,		-2,	0,	-1}},
{NULL,	gettext_noop("   Deselection verticale   "),		{TRUE,		0,	0,	CASE_POP_MENU_REPLAYGAIN +0}},
{NULL,	gettext_noop("   ---"),					{FALSE,		-1,	0,	-1}},
{NULL,	gettext_noop("   Selection PISTE         "),		{TRUE,		0,	0,	CASE_POP_MENU_REPLAYGAIN +1}},
{NULL,	gettext_noop("   Selection ALBUM         "),		{TRUE,		0,	0,	CASE_POP_MENU_REPLAYGAIN +2}},
{NULL,	gettext_noop("   Selection NETTOYER      "),		{TRUE,		0,	0,	CASE_POP_MENU_REPLAYGAIN +3}}
};


#define CASE_POP_MENU_TRASH 40
#define MAX_POP_MENU_TRASH 3
POP_MENU PopMenuTrash[ MAX_POP_MENU_TRASH ] = {
/*
 WIDGET	TITLE							OP
								SHOW		TYPE	VAL	CALL
*/
{NULL,	gettext_noop("---MENU TRASH--------------"),		{FALSE,		-2,	0,	-1}},
{NULL,	gettext_noop("   Deselection verticale"),		{TRUE,		0,	0,	CASE_POP_MENU_TRASH}},
{NULL,	gettext_noop("   Selection verticale   "),		{TRUE,		0,	0,	CASE_POP_MENU_TRASH +1}}
};










// 
// 
void popup_callback( GtkMenuItem *menuitem, gpointer user_data )
{
	MENU_VAL *Op =( MENU_VAL *)user_data;
	
	if( -1 == Op->Call) return;
	
	// g_print("Op->Call = %d\n",Op->Call );
	
	switch( Op->Call ) {
	
	// Deselection globale	
	case CASE_POP_MENU_FICHERS :
		file_from_popup( FILE_CONV_DESELECT_ALL, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Deselection verticale	
	case CASE_POP_MENU_FICHERS +1 :
		file_from_popup( FILE_CONV_DESELECT_V, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Deselection horizontale	
	case CASE_POP_MENU_FICHERS +2 :
		file_from_popup( FILE_CONV_DESELECT_H, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Selection verticale	
	case CASE_POP_MENU_FICHERS +3 : 
		file_from_popup( FILE_CONV_SELECT_V, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Selection Expert verticale
	case CASE_POP_MENU_FICHERS +4 :
		file_from_popup( FILE_CONV_SELECT_EXPERT_V, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Selection horizontale
	case CASE_POP_MENU_FICHERS +5 :
		file_from_popup( FILE_CONV_SELECT_H, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Selection Expert horizontale
	case CASE_POP_MENU_FICHERS +6 :
		file_from_popup( FILE_CONV_SELECT_EXPERT_H, VarPopup.detail, VarPopup.type_file_is );
		break;
	

	// Deselection globale
	case CASE_POP_MENU_CD +0 :
		cdaudio_from_popup( CD_CONV_DESELECT_ALL, VarPopup.Audio, VarPopup.type_file_is );
		break;
	// Deselection verticale
	case CASE_POP_MENU_CD +1 :
		cdaudio_from_popup( CD_CONV_DESELECT_V, VarPopup.Audio, VarPopup.type_file_is );
		break;
	// Deselection horizontale
	case CASE_POP_MENU_CD +2 :
		cdaudio_from_popup( CD_CONV_DESELECT_H, VarPopup.Audio, VarPopup.type_file_is );
		break;
	// Selection verticale
	case CASE_POP_MENU_CD +3 :
		cdaudio_from_popup( CD_CONV_SELECT_V, VarPopup.Audio, VarPopup.type_file_is );
		break;
	// Selection Expert verticale
	case CASE_POP_MENU_CD +4 :
		cdaudio_from_popup( CD_CONV_SELECT_EXPERT_V, VarPopup.Audio, VarPopup.type_file_is );
		break;
	// Selection horizontale
	case CASE_POP_MENU_CD +5 :
		cdaudio_from_popup( CD_CONV_SELECT_H, VarPopup.Audio, VarPopup.type_file_is );
		break;
	// Selection Expert horizontale
	case CASE_POP_MENU_CD +6 :
		cdaudio_from_popup( CD_CONV_SELECT_EXPERT_H, VarPopup.Audio, VarPopup.type_file_is );
		break;
	

	// Deselection verticale
	case CASE_POP_MENU_NORMALISE_CD +0:
		cdaudio_from_popup( CD_REPLAYGAIN_SELECT_V, VarPopup.Audio, VarPopup.type_file_is );
		break;
	// Selection verticale
	case CASE_POP_MENU_NORMALISE_CD +1:
		cdaudio_from_popup( CD_REPLAYGAIN_DESELECT_V, VarPopup.Audio, VarPopup.type_file_is );
		break;


	// Deselection verticale
	case CASE_POP_MENU_NORMALISE_DVD +0:
		dvd_from_popup( CD_NORMALISE_SELECT_V , FALSE );
		break;
	// Selection verticale
	case CASE_POP_MENU_NORMALISE_DVD +1:
		dvd_from_popup( CD_NORMALISE_DESELECT_V, TRUE );
		break;
	
	
	// Deselection verticale
	case CASE_POP_MENU_REPLAYGAIN +0 :
		file_from_popup( FILE_REPLAYGAIN_DESELECT_V, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Selection PISTE
	case CASE_POP_MENU_REPLAYGAIN +1 :
		file_from_popup( FILE_REPLAYGAIN_SELECT_PISTE, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Selection ALBUM
	case CASE_POP_MENU_REPLAYGAIN +2 :
		file_from_popup( FILE_REPLAYGAIN_SELECT_ALBUM, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Selection NETTOYER
	case CASE_POP_MENU_REPLAYGAIN +3 :
		file_from_popup( FILE_REPLAYGAIN_SELECT_NETTOYER, VarPopup.detail, VarPopup.type_file_is );
		break;
	

	// Deselection verticale
	case CASE_POP_MENU_TRASH :
		file_from_popup( FILE_TRASH_DESELECT_V, VarPopup.detail, VarPopup.type_file_is );
		break;
	// Selection verticale
	case CASE_POP_MENU_TRASH +1 :
		file_from_popup( FILE_TRASH_SELECT_V, VarPopup.detail, VarPopup.type_file_is );
		break;
	}
}
// 
// 
void popup_make_popup( POP_MENU *p_popup, gint nbr_op )
{
	GtkWidget *menu = NULL;
	GtkWidget *SubMenu = NULL;
	gint       cpt = 0;
	gint       val = 0;
	
	
	if( VarPopup.detail != NULL ) {
		if( VarPopup.detail->type_infosong_file_is == FILE_IS_WAV ) {
			INFO_WAV *info =( INFO_WAV *)VarPopup.detail->info;
			val = info->LevelDbfs.level;
		}
		else if( VarPopup.detail->type_infosong_file_is == FILE_IS_OGG ) {
			INFO_OGG *info =( INFO_OGG *)VarPopup.detail->info;
			val = info->LevelDbfs.level;
		}
		else if( VarPopup.detail->type_infosong_file_is == FILE_IS_MP3 ) {
			INFO_MP3 *info =( INFO_MP3 *)VarPopup.detail->info;
			val = info->LevelDbfs.level;
		}
		val = val;
	}
	
	menu = gtk_menu_new(  );	
	// gtk_widget_set_extension_events( menu, GDK_EXTENSION_EVENTS_ALL );

	for( cpt = 0; cpt < nbr_op; cpt ++ ) {
		if( p_popup[ cpt ].Op.Type > -1 ) {
			p_popup[ cpt ].Widget = gtk_menu_item_new_with_mnemonic( gettext(p_popup[ cpt ].Title) );
			gtk_widget_show( p_popup[ cpt ].Widget );
			gtk_container_add( GTK_CONTAINER( menu), p_popup[ cpt ].Widget );
		}
		else if( p_popup[ cpt ].Op.Type == -1 ) {
			p_popup[ cpt ].Widget = gtk_separator_menu_item_new(  );
			gtk_widget_show( p_popup[ cpt ].Widget );
			gtk_container_add( GTK_CONTAINER( menu), p_popup[ cpt ].Widget );
		}
		else if( p_popup[ cpt ].Op.Type == -2 ) {
			p_popup[ cpt ].Widget = gtk_menu_item_new_with_mnemonic( gettext(p_popup[ cpt ].Title) );
			gtk_widget_show( p_popup[ cpt ].Widget );
			gtk_container_add( GTK_CONTAINER( menu), p_popup[ cpt ].Widget );
		}
		else if( p_popup[ cpt ].Op.Type == -3 ) {
			p_popup[ cpt ].Widget = gtk_menu_item_new_with_mnemonic( gettext(p_popup[ cpt ].Title) );
			gtk_widget_show( p_popup[ cpt ].Widget );
			gtk_container_add( GTK_CONTAINER( menu), p_popup[ cpt ].Widget );
			
			SubMenu = gtk_menu_new(  );
			gtk_menu_item_set_submenu( GTK_MENU_ITEM( p_popup[ cpt ].Widget), SubMenu );
		}
		else if( p_popup[ cpt ].Op.Type == -10 ) {
			p_popup[ cpt ].Widget =  gtk_menu_item_new_with_mnemonic( gettext(p_popup[ cpt ].Title) );
			gtk_widget_show( p_popup[ cpt ].Widget );
			gtk_container_add( GTK_CONTAINER( SubMenu), p_popup[ cpt ].Widget );
		}
		
		gtk_widget_set_sensitive( p_popup[ cpt ].Widget, p_popup[ cpt ].Op.Show );
	}

	for( cpt = 0; cpt < nbr_op; cpt ++ ) {
		g_signal_connect( (gpointer) p_popup[ cpt ].Widget, "activate",
					G_CALLBACK( popup_callback),
					(gpointer)&p_popup[ cpt ].Op );
	}

	GLADE_HOOKUP_OBJECT_NO_REF( menu, menu, "menu" );
	for( cpt = 0; cpt < nbr_op; cpt ++ ) {
		GLADE_HOOKUP_OBJECT( menu, p_popup[ cpt ].Widget, p_popup[ cpt ].Title );
	}

	gtk_menu_popup( GTK_MENU( menu), NULL, NULL, NULL, NULL, 3, 0 );	
}

// 
// 
// POPUP CD 
// 
// 
void popup_cd( CD_AUDIO *Audio, TYPE_FILE_IS TypeFileIs )
{
	VarPopup.detail       = (DETAIL *)NULL;
	VarPopup.type_file_is = TypeFileIs;
	VarPopup.Audio        = (CD_AUDIO *)Audio;
	popup_make_popup( PopMenuCD, MAX_POP_MENU_CD );
}
// 
// 
// POPUP FILE 
// 
// 
void popup_file( DETAIL *detail, TYPE_FILE_IS TypeFileIs )
{
	VarPopup.Audio        = (CD_AUDIO *)NULL;
	VarPopup.detail       = (DETAIL *)detail;
	VarPopup.type_file_is = TypeFileIs;
	popup_make_popup( PopMenuFichiers, MAX_POP_MENU_FICHERS );
}
// 
// 
// POPUP FILE REPLAYGAIN 
// 
// 
void popup_file_ReplayGain( DETAIL *detail )
{
	VarPopup.detail                = (DETAIL *)detail;
	VarPopup.Audio                 = (CD_AUDIO *)NULL;
	PopMenuReplayGain[ 3 ].Op.Show = TRUE;
	if( VarPopup.detail->type_infosong_file_is == FILE_IS_FLAC ) {
		PopMenuReplayGain[ 3 ].Op.Show = FALSE;
	}
	popup_make_popup( PopMenuReplayGain, MAX_POP_MENU_REPLAYGAIN );
}
// 
// 
// POPUP NORMALISE DVD 
// 
// 
void popup_normalise_dvd( void )
{
	VarPopup.detail = (DETAIL *)NULL;
	VarPopup.Audio  = (CD_AUDIO *)NULL;
	popup_make_popup( PopMenuNormaliseDVD, MAX_POP_MENU_NORMALISE_DVD );
}
// 
// 
// POPUP NORMALISE CD 
// 
// 
void popup_normalise_cd( void )
{
	VarPopup.detail = (DETAIL *)NULL;
	VarPopup.Audio  = (CD_AUDIO *)NULL;
	popup_make_popup( PopMenuNormaliseCD, MAX_POP_MENU_NORMALISE_CD );
}
// 
// 
// POPUP TRASH 
// 
// 
void popup_trash( void )
{
	VarPopup.detail = (DETAIL *)NULL;
	VarPopup.Audio  = (CD_AUDIO *)NULL;
	popup_make_popup( PopMenuTrash, MAX_POP_MENU_TRASH );
}



// 
//
// POPUP FILE WAV PISTE
// 
// 
void on_CellTrack1_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_TRACK_CELL, detail, -2, 1, -2 );
}
void on_CellTrack2_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_TRACK_CELL, detail, -2, 2, -2 );
}
void on_CellTrack4_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_TRACK_CELL, detail, -2, 4, -2 );
}
void on_CellTrack6_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_TRACK_CELL, detail, -2, 6, -2 );
}
void on_CellTrackOriginal_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_TRACK_CELL, detail, -2, -1, -2 );
}
void popup_file_wav_piste( DETAIL *detail)
{
	GtkWidget *Menu;
	GtkWidget *CellTrack1;
	GtkWidget *CellTrack2;
	GtkWidget *CellTrack4;
	GtkWidget *CellTrack6;
	GtkWidget *CellSeparate;
	GtkWidget *CellTrackOriginal;

	
	Menu = gtk_menu_new(  );
	
	CellTrack1 = gtk_menu_item_new_with_mnemonic( "1  track" );
	gtk_widget_show( CellTrack1 );
	gtk_container_add( GTK_CONTAINER( Menu), CellTrack1  );
	CellTrack2 = gtk_menu_item_new_with_mnemonic( "2  track" );
	gtk_widget_show( CellTrack2 );
	gtk_container_add( GTK_CONTAINER( Menu), CellTrack2  );
	CellTrack4 = gtk_menu_item_new_with_mnemonic( "4  track" );
	gtk_widget_show( CellTrack4 );
	gtk_container_add( GTK_CONTAINER( Menu), CellTrack4  );
	CellTrack6 = gtk_menu_item_new_with_mnemonic( "6  track" );
	gtk_widget_show( CellTrack6 );
	gtk_container_add( GTK_CONTAINER( Menu), CellTrack6  );
	CellSeparate = gtk_separator_menu_item_new(  );
	gtk_widget_show( CellSeparate );
	gtk_container_add( GTK_CONTAINER( Menu), CellSeparate );
	CellTrackOriginal = gtk_menu_item_new_with_mnemonic( _("Original value") );
	gtk_widget_show( CellTrackOriginal );
	gtk_container_add( GTK_CONTAINER( Menu), CellTrackOriginal  );
	
	g_signal_connect(( gpointer) CellTrack1,        "activate", G_CALLBACK( on_CellTrack1_activate),        detail  );
	g_signal_connect(( gpointer) CellTrack2,        "activate", G_CALLBACK( on_CellTrack2_activate),        detail  );
	g_signal_connect(( gpointer) CellTrack4,        "activate", G_CALLBACK( on_CellTrack4_activate),        detail  );
	g_signal_connect(( gpointer) CellTrack6,        "activate", G_CALLBACK( on_CellTrack6_activate),        detail  );
	g_signal_connect(( gpointer) CellTrackOriginal, "activate", G_CALLBACK( on_CellTrackOriginal_activate), detail  );
	
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu,       "Menu" );
	GLADE_HOOKUP_OBJECT( Menu, CellTrack1,        "CellTrack1" );
	GLADE_HOOKUP_OBJECT( Menu, CellTrack2,        "CellTrack2" );
	GLADE_HOOKUP_OBJECT( Menu, CellTrack4,        "CellTrack4" );
	GLADE_HOOKUP_OBJECT( Menu, CellTrack6,        "CellTrack6" );
	GLADE_HOOKUP_OBJECT( Menu, CellSeparate,      "CellSeparate" );
	GLADE_HOOKUP_OBJECT( Menu, CellTrackOriginal, "CellTrackOriginal" );

	gtk_menu_popup( GTK_MENU( Menu), NULL, NULL, NULL, NULL, 3, 0 );
}
// 
//
// POPUP FILE WAV FREQUENCE
// 
// 
void on_CellHertz8000_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, 8000, -2, -2 );
}
void on_CellHertz22000_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, 22000, -2, -2 );
}
void on_CellHertz32000_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, 32000, -2, -2 );
}
void on_CellHertz44056_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, 44056, -2, -2 );
}
void on_CellHertz44100_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, 44100, -2, -2 );
}
void on_CellHertz48000_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, 48000, -2, -2 );
}
void on_CellHertz88200_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, 88200, -2, -2 );
}
void on_CellHertz96000_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, 96000, -2, -2 );
}
void on_CellHertzOriginal_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_FREQUENCY_CELL_HERTZ, detail, -1, -2, -2 );
}
void popup_file_wav_frequence( DETAIL *detail)
{
	GtkWidget *Menu;
	GtkWidget *CellHertz8000;
	GtkWidget *CellHertz22000;
	GtkWidget *CellHertz32000;
	GtkWidget *CellHertz44056;
	GtkWidget *CellHertz44100;
	GtkWidget *CellHertz48000;
	GtkWidget *CellHertz88200;
	GtkWidget *CellHertz96000;
	GtkWidget *CellSeparate;
	GtkWidget *CellHertzOriginal;
		
	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
		

	CellHertz8000 = gtk_menu_item_new_with_mnemonic( " 8000  Hertz" );
	gtk_widget_show( CellHertz8000 );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertz8000  );
	CellHertz22000 = gtk_menu_item_new_with_mnemonic( "22000  Hertz" );
	gtk_widget_show( CellHertz22000 );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertz22000  );
	CellHertz32000 = gtk_menu_item_new_with_mnemonic( "32000  Hertz" );
	gtk_widget_show( CellHertz32000 );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertz32000  );
	CellHertz44056 = gtk_menu_item_new_with_mnemonic( "44056  Hertz" );
	gtk_widget_show( CellHertz44056 );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertz44056  );
	CellHertz44100 = gtk_menu_item_new_with_mnemonic( "44100  Hertz" );
	gtk_widget_show( CellHertz44100 );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertz44100  );
	CellHertz48000 = gtk_menu_item_new_with_mnemonic( "48000  Hertz" );
	gtk_widget_show( CellHertz48000 );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertz48000  );
	CellHertz88200 = gtk_menu_item_new_with_mnemonic( "88200  Hertz" );
	gtk_widget_show( CellHertz88200 );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertz88200  );
	CellHertz96000 = gtk_menu_item_new_with_mnemonic( "96000  Hertz" );
	gtk_widget_show( CellHertz96000 );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertz96000  );
	CellSeparate = gtk_separator_menu_item_new(  );
	gtk_widget_show( CellSeparate );
	gtk_container_add( GTK_CONTAINER( Menu), CellSeparate );
	CellHertzOriginal = gtk_menu_item_new_with_mnemonic( _("Original value") );
	gtk_widget_show( CellHertzOriginal );
	gtk_container_add( GTK_CONTAINER( Menu), CellHertzOriginal  );
	
	g_signal_connect(( gpointer) CellHertz8000,     "activate", G_CALLBACK( on_CellHertz8000_activate),     detail  );
	g_signal_connect(( gpointer) CellHertz22000,    "activate", G_CALLBACK( on_CellHertz22000_activate),    detail  );
	g_signal_connect(( gpointer) CellHertz32000,    "activate", G_CALLBACK( on_CellHertz32000_activate),    detail  );
	g_signal_connect(( gpointer) CellHertz44056,    "activate", G_CALLBACK( on_CellHertz44056_activate),    detail  );
	g_signal_connect(( gpointer) CellHertz44100,    "activate", G_CALLBACK( on_CellHertz44100_activate),    detail  );
	g_signal_connect(( gpointer) CellHertz48000,    "activate", G_CALLBACK( on_CellHertz48000_activate),    detail  );
	g_signal_connect(( gpointer) CellHertz88200,    "activate", G_CALLBACK( on_CellHertz88200_activate),    detail  );
	g_signal_connect(( gpointer) CellHertz96000,    "activate", G_CALLBACK( on_CellHertz96000_activate),    detail  );
	g_signal_connect(( gpointer) CellHertzOriginal, "activate", G_CALLBACK( on_CellHertzOriginal_activate), detail  );

	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu,  "Menu" );
	GLADE_HOOKUP_OBJECT( Menu, Menu,       		"Menu" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertz8000,     "CellHertz8000" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertz22000,    "CellHertz22000" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertz32000,    "CellHertz32000" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertz44056,    "CellHertz44056" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertz44100,    "CellHertz44100" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertz48000,    "CellHertz48000" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertz88200,    "CellHertz88200" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertz96000,    "CellHertz96000" );
	GLADE_HOOKUP_OBJECT( Menu, CellSeparate,      "CellSeparate" );
	GLADE_HOOKUP_OBJECT( Menu, CellHertzOriginal, "CellHertzOriginal" );

	gtk_menu_popup( GTK_MENU( Menu), NULL, NULL, NULL, NULL, 3, 0 );
}
// 
//
// POPUP FILE WAV QUANTIFICATION
// 
//
void on_CellWav8_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_QUANTIFICATION_CELL, detail, -2, -2, 8 );
}
void on_CellWav16_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_QUANTIFICATION_CELL, detail, -2, -2, 16 );
}
void on_CellWav24_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_QUANTIFICATION_CELL, detail, -2, -2, 24 );
}
void on_CellWav32_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_QUANTIFICATION_CELL, detail, -2, -2, 32 );
}
void on_CellWav64_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_QUANTIFICATION_CELL, detail, -2, -2, 64 );
}
void on_CellOriginal_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	DETAIL *detail = ( DETAIL *)user_data;
	FileWav_from_popup( FILEWAV_QUANTIFICATION_CELL, detail, -2, -2, -1 );
}
void popup_file_wav_quantification( DETAIL *detail)
{
	GtkWidget *Menu;
	GtkWidget *CellWav8;
	GtkWidget *CellWav16;
	GtkWidget *CellWav24;
	GtkWidget *CellWav32;
	GtkWidget *CellWav64;
	GtkWidget *CellSeparate;
	GtkWidget *CellOriginal;
	
	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
		
	CellWav8 = gtk_menu_item_new_with_mnemonic( " 8  bits" );
	gtk_widget_show( CellWav8 );
	gtk_container_add( GTK_CONTAINER( Menu), CellWav8  );
	CellWav16 = gtk_menu_item_new_with_mnemonic( "16  bits" );
	gtk_widget_show( CellWav16 );
	gtk_container_add( GTK_CONTAINER( Menu), CellWav16  );
	CellWav24 = gtk_menu_item_new_with_mnemonic( "24  bits" );
	gtk_widget_show( CellWav24 );
	gtk_container_add( GTK_CONTAINER( Menu), CellWav24  );
	CellWav32 = gtk_menu_item_new_with_mnemonic( "32  bits" );
	gtk_widget_show( CellWav32 );
	gtk_container_add( GTK_CONTAINER( Menu), CellWav32  );
	CellWav64 = gtk_menu_item_new_with_mnemonic( "64  bits" );
	gtk_widget_show( CellWav64 );
	gtk_container_add( GTK_CONTAINER( Menu), CellWav64  );
	CellSeparate = gtk_separator_menu_item_new(  );
	gtk_widget_show( CellSeparate );
	gtk_container_add( GTK_CONTAINER( Menu), CellSeparate );
	CellOriginal = gtk_menu_item_new_with_mnemonic( _("Original value") );
	gtk_widget_show( CellOriginal );
	gtk_container_add( GTK_CONTAINER( Menu), CellOriginal  );
	
	g_signal_connect(( gpointer) CellWav8,     "activate", G_CALLBACK( on_CellWav8_activate),     detail  );
	g_signal_connect(( gpointer) CellWav16,    "activate", G_CALLBACK( on_CellWav16_activate),    detail  );
	g_signal_connect(( gpointer) CellWav24,    "activate", G_CALLBACK( on_CellWav24_activate),    detail  );
	g_signal_connect(( gpointer) CellWav32,    "activate", G_CALLBACK( on_CellWav32_activate),    detail  );
	g_signal_connect(( gpointer) CellWav64,    "activate", G_CALLBACK( on_CellWav64_activate),    detail  );
	g_signal_connect(( gpointer) CellOriginal, "activate", G_CALLBACK( on_CellOriginal_activate), detail  );
	
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu,  "Menu" );
	GLADE_HOOKUP_OBJECT( Menu, CellWav8,     "CellWav8" );
	GLADE_HOOKUP_OBJECT( Menu, CellWav16,    "CellWav16" );
	GLADE_HOOKUP_OBJECT( Menu, CellWav24,    "CellWav24" );
	GLADE_HOOKUP_OBJECT( Menu, CellWav32,    "CellWav32" );
	GLADE_HOOKUP_OBJECT( Menu, CellWav64,    "CellWav64" );
	GLADE_HOOKUP_OBJECT( Menu, CellWav64,    "CellSeparate" );
	GLADE_HOOKUP_OBJECT( Menu, CellOriginal, "CellOriginal" );
	
	gtk_menu_popup( GTK_MENU( Menu), NULL, NULL, NULL, NULL, 3, 0 );
}
// 
//
// POPUP SPLIT
// 
//
void on_SplitNext_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	split_from_popup( 0 );
}
void on_SplitPrevious_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	split_from_popup( 1 );
}
void on_SplitRemove_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	split_from_popup( 2 );
}
void popup_file_Split( void )
{
	GtkWidget *Menu;
	GtkWidget *SplitNext;
	GtkWidget *SplitPrevious;
	GtkWidget *Separate;
	GtkWidget *SplitRemove;
	
	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
	
	SplitNext = gtk_menu_item_new_with_mnemonic( _("Next selector") );
	gtk_widget_show( SplitNext );
	gtk_container_add( GTK_CONTAINER( Menu), SplitNext );

	SplitPrevious = gtk_menu_item_new_with_mnemonic( _("Previous selector") );
	gtk_widget_show( SplitPrevious );
	gtk_container_add( GTK_CONTAINER( Menu), SplitPrevious );
	
	Separate = gtk_separator_menu_item_new(  );
	gtk_widget_show( Separate );
	gtk_container_add( GTK_CONTAINER( Menu), Separate );
		
	SplitRemove = gtk_menu_item_new_with_mnemonic( _("Delete the active range") );
	gtk_widget_show( SplitRemove );
	gtk_container_add( GTK_CONTAINER( Menu), SplitRemove );	
	
	g_signal_connect(( gpointer) SplitNext,      "activate", G_CALLBACK( on_SplitNext_activate),     NULL  );
	g_signal_connect(( gpointer) SplitPrevious,  "activate", G_CALLBACK( on_SplitPrevious_activate), NULL  );
	g_signal_connect(( gpointer) SplitRemove,    "activate", G_CALLBACK( on_SplitRemove_activate),   NULL  );
	
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu,     "Menu" );
	GLADE_HOOKUP_OBJECT( Menu, SplitNext,       "SplitNext" );
	GLADE_HOOKUP_OBJECT( Menu, SplitPrevious,   "SplitPrevious" );
	GLADE_HOOKUP_OBJECT( Menu, SplitRemove,     "SplitRemove" );
	gtk_menu_popup( GTK_MENU( Menu), NULL, NULL, NULL, NULL, 3, 0 );
}
// 
//
// POPUP FLIP
// 
// 
void on_FlipHorizontal_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	IMAGE *Image = ( IMAGE *)user_data;
	
	Image->BoolFlipHorizontal =(  TRUE == Image->BoolFlipHorizontal ) ? FALSE : TRUE;
	gtk_widget_queue_draw( view.AdrDrawingarea  );
}
void on_FlipVertical_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	IMAGE *Image = ( IMAGE *)user_data;
	
	Image->BoolFlipVertical =(  TRUE == Image->BoolFlipVertical ) ? FALSE : TRUE;
	gtk_widget_queue_draw( view.AdrDrawingarea  );
}
void on_RemoveImage_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	// IMAGE *Image =( IMAGE *)user_data;
	poche_remove_image( );
}
void on_ImageFisrt_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	IMAGE *Image = ( IMAGE *)user_data;
	poche_set_selected_first_image( Image  );
}
void on_ImageUp_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	IMAGE *Image = ( IMAGE *)user_data;
	poche_set_selected_up_image( Image  );
}
void on_ImageDown_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	IMAGE *Image = ( IMAGE *)user_data;
	poche_set_selected_down_image( Image  );
}
void on_ImageLast_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	IMAGE *Image = ( IMAGE *)user_data;
	poche_set_selected_last_image( Image  );
}
void popup_flip( IMAGE *p_Image )
{
	GtkWidget *Menu;
	GtkWidget *FlipHorizontal;
	GtkWidget *FlipVertical;
	GtkWidget *Separate;
	GtkWidget *RemoveImage;
	GtkWidget *ImageFisrt;
	GtkWidget *ImageUp;
	GtkWidget *ImageDown;
	GtkWidget *ImageLast;

	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
	
	FlipHorizontal = gtk_menu_item_new_with_mnemonic( _("Flip Horizontal") );
	gtk_widget_show( FlipHorizontal );
	gtk_container_add( GTK_CONTAINER( Menu), FlipHorizontal );
	
	FlipVertical = gtk_menu_item_new_with_mnemonic( _("Flip Vertical") );
	gtk_widget_show( FlipVertical );
	gtk_container_add( GTK_CONTAINER( Menu), FlipVertical );
	
	Separate = gtk_separator_menu_item_new(  );
	gtk_widget_show( Separate );
	gtk_container_add( GTK_CONTAINER( Menu), Separate );
		
	ImageFisrt = gtk_menu_item_new_with_mnemonic( _("Foreground image") );
	gtk_widget_show( ImageFisrt );
	gtk_container_add( GTK_CONTAINER( Menu), ImageFisrt );	
	
	ImageUp = gtk_menu_item_new_with_mnemonic( _("Image above") );
	gtk_widget_show( ImageUp );
	gtk_container_add( GTK_CONTAINER( Menu), ImageUp );	

	ImageDown = gtk_menu_item_new_with_mnemonic( _("Image below") );
	gtk_widget_show( ImageDown );
	gtk_container_add( GTK_CONTAINER( Menu), ImageDown );	
	
	ImageLast = gtk_menu_item_new_with_mnemonic( _("Image to the background") );
	gtk_widget_show( ImageLast );
	gtk_container_add( GTK_CONTAINER( Menu), ImageLast );	
	
	Separate = gtk_separator_menu_item_new(  );
	gtk_widget_show( Separate );
	gtk_container_add( GTK_CONTAINER( Menu), Separate );
		
	RemoveImage = gtk_menu_item_new_with_mnemonic( _("Clear image") );
	gtk_widget_show( RemoveImage );
	gtk_container_add( GTK_CONTAINER( Menu), RemoveImage );	

	g_signal_connect( (gpointer) FlipHorizontal,   "activate", G_CALLBACK( on_FlipHorizontal_activate),   p_Image );
	g_signal_connect( (gpointer) FlipVertical,     "activate", G_CALLBACK( on_FlipVertical_activate),     p_Image );
	g_signal_connect( (gpointer) ImageFisrt,       "activate", G_CALLBACK( on_ImageFisrt_activate),       p_Image );
	g_signal_connect( (gpointer) ImageUp,          "activate", G_CALLBACK( on_ImageUp_activate),          p_Image );
	g_signal_connect( (gpointer) ImageDown,        "activate", G_CALLBACK( on_ImageDown_activate),        p_Image );
	g_signal_connect( (gpointer) ImageLast,        "activate", G_CALLBACK( on_ImageLast_activate),        p_Image );
	g_signal_connect( (gpointer) RemoveImage,      "activate", G_CALLBACK( on_RemoveImage_activate),      p_Image );
	
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu,    "Menu" );
	GLADE_HOOKUP_OBJECT( Menu, FlipHorizontal, "FlipHorizontal" );
	GLADE_HOOKUP_OBJECT( Menu, FlipVertical,   "FlipVertical" );
	GLADE_HOOKUP_OBJECT( Menu, RemoveImage,    "RemoveImage" );
	gtk_menu_popup( GTK_MENU( Menu), NULL, NULL, NULL, NULL, 3, 0 );
}
// 
//
// POPUP REMOVE IMAGE IN VIEWPORT
// 
// 
void on_viewport_remove_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	GLIST_POCHETTE *gl = ( GLIST_POCHETTE *)user_data;

	gl->BoolStructRemove = TRUE;
	pochedir_destroy_image( gl  );
}
void popup_viewport( GLIST_POCHETTE *gl )
{
	GtkWidget *Menu;
	GtkWidget *RemoveImage;
	
	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
	
	RemoveImage = gtk_menu_item_new_with_mnemonic( _("Remove image from the list") );
	gtk_widget_show( RemoveImage );
	gtk_container_add( GTK_CONTAINER( Menu), RemoveImage );
	
	g_signal_connect(( gpointer) RemoveImage, "activate", G_CALLBACK( on_viewport_remove_activate), gl  );
	
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu, "Menu" );
	GLADE_HOOKUP_OBJECT( Menu, RemoveImage, "RemoveImage" );
	
	gtk_menu_popup( GTK_MENU( Menu), NULL, NULL, NULL, NULL, 3, 0 );
}
// 
//
// POPUP TAG CD
// 
// 
/*
typedef struct {
	gboolean	BoolFromPopup;
	gint		num;
	gchar		*name;
} CD_POPUP_GENRE;
*/
CD_POPUP_GENRE CdPopupGenre = { FALSE, -1, NULL };

void on_menu_popup_num_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	gint	Choice = GPOINTER_TO_INT(user_data);
	
	CdPopupGenre.num           = Choice;
	CdPopupGenre.BoolFromPopup = TRUE;
	
	PRINT_FUNC_LF();
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print( "\tPtrEntry = %s\n", CdPopupGenre.name );
		g_print( "\tNumEntry = %d\n", Choice);
	}

	gtk_entry_set_text( GTK_ENTRY(GLADE_GET_OBJECT("entry_tag_genre")), CdPopupGenre.name );
	cd_expander_set_genre( CdPopupGenre.num, CdPopupGenre.name );
	
	CdPopupGenre.BoolFromPopup = FALSE;
}
void on_menu_popup_txt_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	gchar	*Ptr = (gchar *)user_data;
	
	if( NULL != CdPopupGenre.name ) {
		g_free( CdPopupGenre.name );
		CdPopupGenre.name = NULL;
	}
	CdPopupGenre.name = g_strdup( Ptr );
}
void popup_menu_cd( void )
{
	GtkWidget	*Menu;
	GtkWidget	*RemoveImage;
	gint		Choice;
	
	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
	
	for( Choice = 0; -1 != StructTagsFileMp3[ Choice ].num; Choice ++ ) {
		
		// StructTagsFileMp3[ i ].name
		// StructTagsFileMp3[ i ].num
		
		RemoveImage = gtk_menu_item_new_with_mnemonic( StructTagsFileMp3[ Choice ].name );
		
		gtk_widget_show( RemoveImage );
		gtk_container_add( GTK_CONTAINER( Menu), RemoveImage );
		
		g_signal_connect(( gpointer) RemoveImage, "activate", G_CALLBACK(on_menu_popup_txt_activate), (gpointer)StructTagsFileMp3[ Choice ].name );
		g_signal_connect(( gpointer) RemoveImage, "activate", G_CALLBACK(on_menu_popup_num_activate), GINT_TO_POINTER(StructTagsFileMp3[ Choice ].num));
		
		GLADE_HOOKUP_OBJECT( Menu, RemoveImage, "RemoveImage" );
	}
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu, "Menu" );

	gtk_menu_popup( GTK_MENU( Menu), NULL, NULL, NULL, NULL, 3, 0 );
}


// 
//
// POPUP TAG FILE
// 
// 
/*
typedef struct {
	gboolean	BoolFromPopup;
	gint		num;
	gchar		*name;
} FILE_POPUP_GENRE;
*/
FILE_POPUP_GENRE FilePopupGenre = { FALSE, -1, NULL };

void on_menu_popup_num_file_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	gint	Choice = GPOINTER_TO_INT(user_data);
	
	FilePopupGenre.num           = Choice;
	FilePopupGenre.BoolFromPopup = TRUE;
	
	PRINT_FUNC_LF();
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print( "\tPtrEntry = %s\n", FilePopupGenre.name );
		g_print( "\tNumEntry = %d\n", Choice);
	}

	gtk_entry_set_text( GTK_ENTRY(GLADE_GET_OBJECT("entry_tag_genre_file")), FilePopupGenre.name );
	// cd_expander_set_genre( CdPopupGenre.num, CdPopupGenre.name );
	
	FileTags_changed_all_tags( 0 ); // #define	TAG_GENRE	0
	
	FilePopupGenre.BoolFromPopup = FALSE;
}
void on_menu_popup_txt_file_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	gchar	*Ptr = (gchar *)user_data;
	
	if( NULL != FilePopupGenre.name ) {
		g_free( FilePopupGenre.name );
		FilePopupGenre.name = NULL;
	}
	FilePopupGenre.name = g_strdup( Ptr );
}
void popup_menu_file( void )
{
	GtkWidget	*Menu;
	GtkWidget	*RemoveImage;
	gint		Choice;
	
	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
	
	for( Choice = 0; -1 != StructTagsFileMp3[ Choice ].num; Choice ++ ) {
		
		// StructTagsFileMp3[ i ].name
		// StructTagsFileMp3[ i ].num
		
	// ********* gtk_combo_box_set_wrap_width (GTK_COMBO_BOX (widget), 4);
		RemoveImage = gtk_menu_item_new_with_mnemonic( StructTagsFileMp3[ Choice ].name );
		
		gtk_widget_show( RemoveImage );
		gtk_container_add( GTK_CONTAINER( Menu), RemoveImage );
		
		g_signal_connect(( gpointer) RemoveImage, "activate", G_CALLBACK(on_menu_popup_txt_file_activate), (gpointer)StructTagsFileMp3[ Choice ].name );
		g_signal_connect(( gpointer) RemoveImage, "activate", G_CALLBACK(on_menu_popup_num_file_activate), GINT_TO_POINTER(StructTagsFileMp3[ Choice ].num));
		
		GLADE_HOOKUP_OBJECT( Menu, RemoveImage, "RemoveImage" );
	}
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu, "Menu" );

	gtk_menu_popup( GTK_MENU( Menu), NULL, NULL, NULL, NULL, 3, 0 );
}




// 
// POPUP MP3 FILES
// 
/*
	TYPE:	ABR
	DEBIT:	32 40 48 56 64 80 96 112 128 160 192 224 320
	MODE:	Defaut / Stereo / Join Stereo / Forced Join Stereo / Duo Channels / Mono

	TYPE:	CBR
	DEBIT:	32 40 48 56 64 80 96 112 128 160 192 224 320
	MODE:	Defaut / Stereo / Join Stereo / Forced Join Stereo / Duo Channels / Mono

	TYPE:	VBR
	DEBIT:	preset medium / preset standard / preset extreme / preset fast standard / preset fast extreme / V0 V1 V2 V3 V4 V5 V6 V7 V8 V9
	MODE:	Defaut / Stereo / Join Stereo / Forced Join Stereo / Duo Channels / Mono

	TYPE:	VBR-NEW
	DEBIT:	NONE / preset medium / preset standard / preset extreme / preset fast standard / preset fast extreme / V0 V1 V2 V3 V4 V5 V6 V7 V8 V9
	MODE:	Defaut / Stereo / Join Stereo / Forced Join Stereo / Duo Channels / Mono
*/
DETAIL *Detail_VarOptionsFileOggMp3 = NULL;

gchar *VarOptionsFileMp3[] = {
	// ABR
	// 0 .. 13
	" 32", " 40", " 48", " 56", " 64", " 80", " 96", "112", "128", "160", "192", "224", "256", "320",
	// CBR
	// 14 .. 28
	" 32", " 40", " 48", " 56", " 64", " 80", " 96", "112", "128", "160", "192", "224", "256", "320", "preset insane",
	// VBR
	// 29 .. 43
	"preset medium", "preset standard", "preset extreme", "preset fast standard", "preset fast extreme", gettext_noop("V0 [ best quality ]"), "V1", "V2", "V3", gettext_noop("V4 [ default ]"), "V5", "V6", "V7", "V8", gettext_noop("V9 [ Lower quality ]"),
	// VBR-NEW
	// 44 .. 59
	"NONE", "preset medium", "preset standard", "preset extreme", "preset fast standard", "preset fast extreme", "V0", "V1", "V2", "V3", gettext_noop("V4 [ default ]"), "V5", "V6", "V7", "V8", "V9",
	// MODE
	// 60 .. 65
	"Default", "Stereo", "Join Stereo", "Forced Join Stereo", "Duo Channels", "Mono"
};
// 
// 
void on_type_file_mp3_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	gint	Choice = GPOINTER_TO_INT(user_data);
	gint	Debit  = Detail_VarOptionsFileOggMp3->Mp3_Debit;
	gint	Mode   = Detail_VarOptionsFileOggMp3->Mp3_Mode;
	
	if( -1 == Choice ) {
		// g_print( "REMOVE\n");
		Debit = Detail_VarOptionsFileOggMp3->Mp3_Debit = -1;	// SET DEBIT
		Mode  = Detail_VarOptionsFileOggMp3->Mp3_Mode  = -1;	// SET MODE
		FileMp3Ogg_update_newbitrate( FILE_IS_MP3, Debit, Mode, -1 );
		return;
	}
	
	// MP3: DEBIT MODE
	if( Choice <= 59 )
		Debit = Detail_VarOptionsFileOggMp3->Mp3_Debit = Choice;	// SET DEBIT
	else	Mode  = Detail_VarOptionsFileOggMp3->Mp3_Mode  = Choice;	// SET MODE
	
	// SELECTIONS DES DEUX OPTIONS OBLIGATOIRE
	if( -1 == Detail_VarOptionsFileOggMp3->Mp3_Debit )	Debit = Detail_VarOptionsFileOggMp3->Mp3_Debit = 0;
	if( -1 == Detail_VarOptionsFileOggMp3->Mp3_Mode )	Mode  = Detail_VarOptionsFileOggMp3->Mp3_Mode  = 60;
	
	Detail_VarOptionsFileOggMp3->BoolChanged = ( -1 != Detail_VarOptionsFileOggMp3->Mp3_Debit || -1 != Detail_VarOptionsFileOggMp3->Mp3_Mode );
		
	FileMp3Ogg_update_newbitrate( FILE_IS_MP3, Debit, Mode, -1 );
}
// 
// 
void popup_file_mp3_type( DETAIL *detail, gint p_debit, gint p_mode )
{
	GtkWidget	*Menu;
	GtkWidget	*Type;
	GtkWidget	*SubMenu;
	GtkWidget	*Debit;
	GtkWidget	*Separate;
	gint		Cpt;
	gchar		*Str = NULL;
	
	Detail_VarOptionsFileOggMp3 = detail;
	
	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
	
	// ABR
	Type = gtk_menu_item_new_with_mnemonic( "Abr" );
	gtk_widget_show( Type );
	gtk_container_add( GTK_CONTAINER( Menu), Type );
		
		// SUBMENU ABR
		SubMenu = gtk_menu_new();
		gtk_menu_item_set_submenu( GTK_MENU_ITEM(Type), SubMenu );
		for( Cpt = 0; Cpt <= 13; Cpt ++ ) {
			if( p_debit == Cpt )
				Str = g_strdup_printf( ">%s", VarOptionsFileMp3[ Cpt ] );
			else	Str = g_strdup_printf( "  %s", VarOptionsFileMp3[ Cpt ] );
			Debit = gtk_menu_item_new_with_mnemonic( Str );
			g_free( Str );
			Str = NULL;
			gtk_widget_show( Debit );
			gtk_container_add( GTK_CONTAINER(SubMenu), Debit  );
			g_signal_connect( (gpointer)Debit, "activate", G_CALLBACK(on_type_file_mp3_activate), GINT_TO_POINTER(Cpt));
		}
	
	// CBR
	Type = gtk_menu_item_new_with_mnemonic( "Cbr" );
	gtk_widget_show( Type );
	gtk_container_add( GTK_CONTAINER( Menu), Type );
		
		// SUBMENU CBR
		SubMenu = gtk_menu_new();
		gtk_menu_item_set_submenu( GTK_MENU_ITEM(Type), SubMenu );
		for( Cpt = 14; Cpt <= 28; Cpt ++ ) {
			if( p_debit == Cpt )
				Str = g_strdup_printf( ">%s", VarOptionsFileMp3[ Cpt ] );
			else	Str = g_strdup_printf( "  %s", VarOptionsFileMp3[ Cpt ] );
			Debit = gtk_menu_item_new_with_mnemonic( Str );
			g_free( Str );
			Str = NULL;
			gtk_widget_show( Debit );
			gtk_container_add( GTK_CONTAINER(SubMenu), Debit  );
			g_signal_connect( (gpointer)Debit, "activate", G_CALLBACK(on_type_file_mp3_activate), GINT_TO_POINTER(Cpt));
		}

	// VBR
	Type = gtk_menu_item_new_with_mnemonic( "Vbr" );
	gtk_widget_show( Type );
	gtk_container_add( GTK_CONTAINER( Menu), Type );	
		
		// SUBMENU VBR
		SubMenu = gtk_menu_new();
		gtk_menu_item_set_submenu( GTK_MENU_ITEM(Type), SubMenu );
		for( Cpt = 29; Cpt <= 43; Cpt ++ ) {
			if( p_debit == Cpt )
				Str = g_strdup_printf( ">%s", VarOptionsFileMp3[ Cpt ] );
			else	Str = g_strdup_printf( "  %s", VarOptionsFileMp3[ Cpt ] );
			Debit = gtk_menu_item_new_with_mnemonic( Str );
			g_free( Str );
			Str = NULL;
			gtk_widget_show( Debit );
			gtk_container_add( GTK_CONTAINER(SubMenu), Debit  );
			g_signal_connect( (gpointer)Debit, "activate", G_CALLBACK(on_type_file_mp3_activate), GINT_TO_POINTER(Cpt));
		}
	
	// VBR-NEW
	Type = gtk_menu_item_new_with_mnemonic( "Vbr New" );
	gtk_widget_show( Type );
	gtk_container_add( GTK_CONTAINER( Menu), Type );
		
		// SUBMENU VBR-NEW
		SubMenu = gtk_menu_new();
		gtk_menu_item_set_submenu( GTK_MENU_ITEM(Type), SubMenu );
		for( Cpt = 44; Cpt <= 59; Cpt ++ ) {
			if( p_debit == Cpt )
				Str = g_strdup_printf( ">%s", VarOptionsFileMp3[ Cpt ] );
			else	Str = g_strdup_printf( "  %s", VarOptionsFileMp3[ Cpt ] );
			Debit = gtk_menu_item_new_with_mnemonic( Str );
			g_free( Str );
			Str = NULL;
			gtk_widget_show( Debit );
			gtk_container_add( GTK_CONTAINER(SubMenu), Debit  );
			g_signal_connect( (gpointer)Debit, "activate", G_CALLBACK(on_type_file_mp3_activate), GINT_TO_POINTER(Cpt));
		}
	
	// MODE
	Type = gtk_menu_item_new_with_mnemonic( "Mode" );
	gtk_widget_show( Type );
	gtk_container_add( GTK_CONTAINER( Menu), Type );
		
		// SUBMENU MODE
		SubMenu = gtk_menu_new();
		gtk_menu_item_set_submenu( GTK_MENU_ITEM(Type), SubMenu );
		for( Cpt = 60; Cpt <= 65; Cpt ++ ) {
			if( p_mode == Cpt )
				Str = g_strdup_printf( "> %s", VarOptionsFileMp3[ Cpt ] );
			else	Str = g_strdup_printf( "  %s", VarOptionsFileMp3[ Cpt ] );
			Debit = gtk_menu_item_new_with_mnemonic( Str );
			g_free( Str );
			Str = NULL;
			gtk_widget_show( Debit );
			gtk_container_add( GTK_CONTAINER(SubMenu), Debit  );
			g_signal_connect( (gpointer)Debit, "activate", G_CALLBACK(on_type_file_mp3_activate), GINT_TO_POINTER(Cpt));
		}
		
	// REMOVE ENTRY
	if( p_debit > -1 ) {
		Separate = gtk_separator_menu_item_new(  );
		gtk_widget_show( Separate );
		gtk_container_add( GTK_CONTAINER( Menu), Separate );
		Separate = gtk_menu_item_new_with_mnemonic( "Effacer la saisie" );
		gtk_widget_show( Separate );
		gtk_container_add( GTK_CONTAINER(Menu), Separate );	
		g_signal_connect( (gpointer)Separate, "activate", G_CALLBACK(on_type_file_mp3_activate), GINT_TO_POINTER(-1));
	}
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu, "Menu" );
	
	gtk_menu_popup( GTK_MENU(Menu), NULL, NULL, NULL, NULL, 3, 0 );
}



// 
// POPUP OGG FILES
// 
/*
DEBIT:		45  kbit/s / 64  kbit/s / 80  kbit/s / 96  kbit/s / 112  kbit/s / 128  kbit/s / 160  kbit/s / 192  kbit/s / 224  kbit/s / 256  kbit/s / 320  kbit/s / 
		Qualite -1 -Moins bonne qualite / Qualite  0 / Qualite  1 / Qualite  2 / Qualite  3 -Defaut / Qualite  4 / Qualite  5 / Qualite  6 / Qualite  7 / Qualite  8 / Qualite  9 / Qualite  10 -Meilleure qualite
MANAGED:	Oui / Non
DOWNMIX:	Mono force / Stereo
*/
gchar *VarOptionsFileOgg[] = {
	// 0 .. 22
	" 45  kbit/s", " 64  kbit/s", " 80  kbit/s", " 96  kbit/s", "112  kbit/s", "128  kbit/s", "160  kbit/", "192  kbit/s", "224  kbit/s", "256  kbit/s", "320  kbit/s",
	gettext_noop("Quality -1 [ Lower quality ]"), gettext_noop("Quality  0"), gettext_noop("Quality  1"), gettext_noop("Quality  2"),
	gettext_noop("Quality  3 [ Defaut ]"), gettext_noop("Quality  4"), gettext_noop("Quality  5"), gettext_noop("Quality  6"), gettext_noop("Quality  7"),
	gettext_noop("Quality  8"), gettext_noop("Quality  9"), gettext_noop("Quality  10 [ Best Quality ]"),
	// 23 .. 24
	gettext_noop("Yes"), gettext_noop("No"),
	// 25 .. 26
	"Mono force", "Stereo"
};

// 
// 
void on_type_file_ogg_activate( GtkMenuItem *menuitem, gpointer user_data )
{
	gint	Choice  = GPOINTER_TO_INT(user_data);
	gint	Debit   = Detail_VarOptionsFileOggMp3->Ogg_Debit;
	gint	Managed = Detail_VarOptionsFileOggMp3->Ogg_Managed;
	gint	Downmix = Detail_VarOptionsFileOggMp3->Ogg_Downmix;
	
	if( -1 == Choice ) {
		// g_print( "REMOVE\n");
		Debit   = Detail_VarOptionsFileOggMp3->Ogg_Debit   = -1;	// SET DEBIT
		Managed = Detail_VarOptionsFileOggMp3->Ogg_Managed = -1;	// SET MANAGED
		Downmix = Detail_VarOptionsFileOggMp3->Ogg_Downmix = -1;	// SET DOWNMIX
		FileMp3Ogg_update_newbitrate( FILE_IS_OGG, Debit, Managed, Downmix );
		return;
	}
	
	// OGG: DEBIT MANAGED DOWNMIX
	if( Choice <= 22 )
		Debit   = Detail_VarOptionsFileOggMp3->Ogg_Debit = Choice;	// SET DEBIT
	else if( Choice <= 24)
		Managed = Detail_VarOptionsFileOggMp3->Ogg_Managed = Choice;	// SET MANAGED
	else
		Downmix = Detail_VarOptionsFileOggMp3->Ogg_Downmix = Choice;	// SET DOWNMIX
	
	// SELECTIONS DES DEUX OPTIONS OBLIGATOIRE
	if( -1 == Detail_VarOptionsFileOggMp3->Ogg_Debit )	Debit   = Detail_VarOptionsFileOggMp3->Ogg_Debit   = 0;
	if( -1 == Detail_VarOptionsFileOggMp3->Ogg_Managed )	Managed = Detail_VarOptionsFileOggMp3->Ogg_Managed = 23;
	if( -1 == Detail_VarOptionsFileOggMp3->Ogg_Downmix )	Downmix = Detail_VarOptionsFileOggMp3->Ogg_Downmix = 25;
	
	if( Detail_VarOptionsFileOggMp3->Ogg_Debit >= 11 )	Managed = Detail_VarOptionsFileOggMp3->Ogg_Managed = -1;
	if( Detail_VarOptionsFileOggMp3->Ogg_Debit >= 9 )	Downmix = Detail_VarOptionsFileOggMp3->Ogg_Downmix = -1;
	
	Detail_VarOptionsFileOggMp3->BoolChanged = ( -1 != Detail_VarOptionsFileOggMp3->Ogg_Debit || -1 != Detail_VarOptionsFileOggMp3->Ogg_Managed || -1 != Detail_VarOptionsFileOggMp3->Ogg_Downmix );
	
	FileMp3Ogg_update_newbitrate( FILE_IS_OGG, Debit, Managed, Downmix );
}
// 
// 
void popup_file_ogg_type( DETAIL *detail, gint p_debit, gint p_managed, gint p_downmix )
{
	GtkWidget	*Menu;
	GtkWidget	*Debit;
	GtkWidget	*SubMenu;
	GtkWidget	*Separate;
	gint		Cpt;
	gchar		*Str = NULL;
	
	Detail_VarOptionsFileOggMp3 = detail;
	
	Menu = gtk_menu_new(  );
	// gtk_widget_set_extension_events( Menu, GDK_EXTENSION_EVENTS_ALL );
	
	// DEBIT
	Debit = gtk_menu_item_new_with_mnemonic( "Debit" );
	gtk_widget_show( Debit );
	gtk_container_add( GTK_CONTAINER( Menu), Debit );
		
		// SUBMENU DEBIT
		SubMenu = gtk_menu_new();
		gtk_menu_item_set_submenu( GTK_MENU_ITEM(Debit), SubMenu );
		for( Cpt = 0; Cpt <= 22; Cpt ++ ) {
			if( p_debit == Cpt )
				Str = g_strdup_printf( ">%s", VarOptionsFileOgg[ Cpt ] );
			else	Str = g_strdup_printf( "  %s", VarOptionsFileOgg[ Cpt ] );
			Debit = gtk_menu_item_new_with_mnemonic( Str );
			g_free( Str );
			Str = NULL;
			gtk_widget_show( Debit );
			gtk_container_add( GTK_CONTAINER(SubMenu), Debit  );
			g_signal_connect( (gpointer)Debit, "activate", G_CALLBACK(on_type_file_ogg_activate), GINT_TO_POINTER(Cpt));
		}
	
	// MANAGED
	if( p_debit < 11 ) {
		Debit = gtk_menu_item_new_with_mnemonic( "Managed" );
		gtk_widget_show( Debit );
		gtk_container_add( GTK_CONTAINER( Menu), Debit );
		
			// SUBMENU MANAGED
			SubMenu = gtk_menu_new();
			gtk_menu_item_set_submenu( GTK_MENU_ITEM(Debit), SubMenu );
			for( Cpt = 23; Cpt <= 24; Cpt ++ ) {
				if( p_managed == Cpt )
					Str = g_strdup_printf( ">%s", VarOptionsFileOgg[ Cpt ] );
				else	Str = g_strdup_printf( "  %s", VarOptionsFileOgg[ Cpt ] );
				Debit = gtk_menu_item_new_with_mnemonic( Str );
				g_free( Str );
				Str = NULL;
				gtk_widget_show( Debit );
				gtk_container_add( GTK_CONTAINER(SubMenu), Debit  );
				g_signal_connect( (gpointer)Debit, "activate", G_CALLBACK(on_type_file_ogg_activate), GINT_TO_POINTER(Cpt));
			}
	}
	
	// DOWNMIX
	if( p_debit < 9 ) {
		Debit = gtk_menu_item_new_with_mnemonic( "Downmix" );
		gtk_widget_show( Debit );
		gtk_container_add( GTK_CONTAINER( Menu), Debit );
		
			// SUBMENU DOWNMIX
			SubMenu = gtk_menu_new();
			gtk_menu_item_set_submenu( GTK_MENU_ITEM(Debit), SubMenu );
			for( Cpt = 25; Cpt <= 26; Cpt ++ ) {
				if( p_downmix == Cpt )
					Str = g_strdup_printf( ">%s", VarOptionsFileOgg[ Cpt ] );
				else	Str = g_strdup_printf( "  %s", VarOptionsFileOgg[ Cpt ] );
				Debit = gtk_menu_item_new_with_mnemonic( Str );
				g_free( Str );
				Str = NULL;
				gtk_widget_show( Debit );
				gtk_container_add( GTK_CONTAINER(SubMenu), Debit  );
				g_signal_connect( (gpointer)Debit, "activate", G_CALLBACK(on_type_file_ogg_activate), GINT_TO_POINTER(Cpt));
			}
	}
	
	// REMOVE ENTRY
	if( p_debit > -1 ) {
		Separate = gtk_separator_menu_item_new(  );
		gtk_widget_show( Separate );
		gtk_container_add( GTK_CONTAINER( Menu), Separate );
		Separate = gtk_menu_item_new_with_mnemonic( "Effacer la saisie" );
		gtk_widget_show( Separate );
		gtk_container_add( GTK_CONTAINER(Menu), Separate );	
		g_signal_connect( (gpointer)Separate, "activate", G_CALLBACK(on_type_file_ogg_activate), GINT_TO_POINTER(-1));
	}
	GLADE_HOOKUP_OBJECT_NO_REF( Menu, Menu, "Menu" );
	
	gtk_menu_popup( GTK_MENU(Menu), NULL, NULL, NULL, NULL, 3, 0 );
}
// 
// 
gchar *popup_get_param_ogg( gint p_debit, gint p_managed, gint p_downmix )
{
	gchar *val_bitrate_ogg[] = {
		"--bitrate=45",
		"--bitrate=64",
		"--bitrate=80",
		"--bitrate=96",
		"--bitrate=112",
		"--bitrate=128",
		"--bitrate=160",
		"--bitrate=192",
		"--bitrate=224",
		"--bitrate=256",
		"--bitrate=320",
		"--quality=-1",
		"--quality=0",
		"--quality=1",
		"--quality=2",
		"--quality=3",
		"--quality=4",
		"--quality=5",
		"--quality=6",
		"--quality=7",
		"--quality=8",
		"--quality=9",
		"--quality=10"
		};
	gchar	*StrRet = NULL;
	
	if( p_debit > -1 ) {
		StrRet = g_strdup_printf( "%s%s%s",
				val_bitrate_ogg[ p_debit ], 
				( p_debit < 11 && p_managed == 23 ) ? " --managed" : "",
				( p_debit < 9 && p_downmix == 25 ) ? " --downmix" : ""
				);
	}
	return( (gchar *)StrRet );
}
// 
// 
gchar *popup_get_param_mp3( gint p_debit, gint p_mode )
{
	static 	gchar *str_param_mp3 [] = {
		// ABR
		// 0 .. 13
		"-b 32",
		"-b 40",
		"-b 48",
		"-b 56",
		"-b 64",
		"-b 80",
		"-b 96",
		"-b 112",
		"-b 128",
		"-b 160",
		"-b 192",
		"-b 224",
		"-b 256",
		"-b 320",
		// CBR
		// 14 .. 28
		"-b 32",
		"-b 40",
		"-b 48",
		"-b 56",
		"-b 64",
		"-b 80",
		"-b 96",
		"-b 112",
		"-b 128",
		"-b 160",
		"-b 192",
		"-b 224",
		"-b 256",
		"-b 320",
		"--preset insane",
		// VBR
		// 29 .. 43
		"--preset medium",
		"--preset standard",
		"--preset extreme",
		"--preset fast standard",
		"--preset fast extreme",
		"-V0",
		"-V1",
		"-V2",
		"-V3",
		"-V4",
		"-V5",
		"-V6",
		"-V7",
		"-V8",
		"-V9",
		// VBR-NEW
		// 44 .. 59
		"--vbr-new",
		"--vbr-new --preset medium",
		"--vbr-new --preset standard",
		"--vbr-new --preset extreme",
		"--vbr-new --preset fast standard",
		"--vbr-new --preset fast extreme",
		"--vbr-new -V0",
		"--vbr-new -V1",
		"--vbr-new -V2",
		"--vbr-new -V3",
		"--vbr-new -V4",
		"--vbr-new -V5",
		"--vbr-new -V6",
		"--vbr-new -V7",
		"--vbr-new -V8",
		"--vbr-new -V9",
		// MODE
		// 60 .. 65
		"",		// Defaut
		"-m s",		// Stereo
		"-m j",		// Join Stereo
		"-m f",		// Forced Join Stereo
		"-m d",		// Duo Channels
		"-m m"		// Mono
		};
	gchar	*StrRet = NULL;
	
	if( p_debit > -1) {
		StrRet = g_strdup_printf( "%s%s%s",
				str_param_mp3[ p_debit ],
				( p_mode == 60 ) ? "" : " ",
				( p_mode > 60 ) ? str_param_mp3[ p_mode ] : ""
				);
	}
	return( (gchar *)StrRet );
}








