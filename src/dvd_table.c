 /*
 *  file      : dvd_table.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "dvd.h"


/*
!-- Menu --!
! ORIGINAL !
! VERS 2CH !
! VERS 6CH !
!----------!

>Question pour Maître Xavier ou Shankarius:
>
>Si l'on trouve un fichier un fichier 4 pistes sans pouvoir
>l'analyser, doit on en déduire qu'il s'agit de:
>quadraphonique ou surround 4.0 ?
>
>Claude

xavier a écrit :
> Hi Claude,
> Sur un DVD, un fichier 4 pistes sera (presque) toujours un Dolby
> Surround 4.0...Question de monopole ! Dolby est à l'industrie
> cinématographique ce que Micro$oft est à l'industrie informatique. En
> même temps c'est très, très, très rare de trouver du 4.0 sur un DVD
> (j'en ai jamais vu...). En général ce sont des données de matriçage
> qui sont inclues dans le Dolby 5.1 et qui permettent d'aiguiller la
> voie centrale et la voie sub vers les enceintes principales, pour ceux
> qui ne disposent ni de sub ni de voie centrale.

----------------------------------------------------------------------------------------------------------------------------
mono		stéréo		quadraphonique		surround 4.0		surround 5.0		surround 5.1
----------------------------------------------------------------------------------------------------------------------------
0. centre	0. gauche	0. devant gauche	0. devant gauche	0. devant gauche	0. devant gauche
		1. droite	1. devant droite	1. devant droite	1. devant droite	1. devant droite
				2. arrière gauche	2. arrière centre	2. arrière gauche	2. arrière gauche
				3. arrière droite	3. devant centre	3. arrière droite	3. arrière droite
										4. devant centre	4. devant centre
													5. caisson de basse
----------------------------------------------------------------------------------------------------------------------------

Il suffit juste de placer dans les case vides:
 - 0 pour laisser dans l'état
Ou
 - >0 .. 512 : pour valider le mixage (généralement 0.5 ou 1)
*/


typedef struct {
	gchar        *pan1;
	gchar        *pan2;
	gchar        *pan3;
	gchar        *pan4;
	gchar        *pan5;
	gchar        *pan6;
} AF_PAN;


AF_PAN af_pan [ 6 ] =
{
/*
	tab [ 0 ] [ 0 .. 5 ]
	-channels 1
		pan1 .. pan6
*/
{
"1",
"1:1",
"1:1:1",
"1:1:1:1",
"1:1:1:1:1",
"1:1:1:1:1:1"},
/*
	tab [ 1 ] [ 0 .. 5 ]
	-channels 2
		pan1 .. pan6
*/
{
"1:1",
"1:0:0:1",
"1:0:1:0:1:1",
"1:0:1:0:0:1:0:1",
"1:0:1:1:0:0:1:0:1:1",
"1:0:1:0:1:1:0:1:0:1:1:1"},
/*
	tab [ 2 ] [ 0 .. 5 ]
	-channels 3
		pan1 .. pan6
*/
{
"0.5:0.5:0.5",
"1:0:0:1:1:1",
"1:0:0.5:0:1:0.5:1:1:1",
"1:0:0:0:0:1:0:0:1:1:1:1",
"1:0:1:0:1:0:1:0:1:1:1:1:1:1:1",
"1:0:1:0:1:1:0:1:0:1:1:1:0:1:1:1:1:1"},
/*
	tab [ 3 ] [ 0 .. 5 ]
	-channels 4
		pan1 .. pan6
*/
{
"1:1:1:1",
"1:0:0:1:1:0:0:1",
"1:0:0:0:1:0:0:0:1:0:0:1",
"1:0:0:0:0:1:0:0:0:0:1:0:0:0:0:1",
"1:0:1:0:0:0:1:0:1:0:0:0:0:1:0:0:0:0:0:1",
"1:0:1:0:0:0:0:1:0:1:0:0:0:0:0:0:1:1:0:0:0:0:1:1"},
/*
	tab [ 4 ] [ 0 .. 5 ]
	-channels 5
		pan1 .. pan6
*/
{
"1:1:1:1:1",
"1:0:0:1:1:0:0:1:1:1",
"1:0:0:0:1:0:1:0:0:0:1:0:0:0:1",
"1:0:0:0:0:1:0:0:0:0:1:0:0:0:1:0:0:0:0:1",
"1:0:0:0:0:0:1:0:0:0:0:0:1:0:0:0:0:0:1:0:0:0:0:0:1",
"1:0:0:0:0:0:0:1:0:0:0:0:0:0:1:0:0:0:0:0:0:1:0:0:0:0:0:0:1:1"},
/*
@Dzef:
En fait, pour transformer le 6 ch en stéréo, le mélange doit se faire comme ça :
0. devant gauche	vers	0. gauche
1. devant droite	vers	1. droite
2. arrière gauche	vers	0. gauche
3. arrière droite	vers	1. droite
4. devant centre	vers	0. gauche ET 1. droite
5. caisson de basse	vers	0. gauche ET 1. droite

	tab [ 5 ] [ 0 .. 5 ]
	-channels 6
		pan1 .. pan6
*/
{
"1:1:1:1:1:1",
"1:0:0:1:1:0:0:1:1:1:1:1",
"1:0:0:0:1:0:1:0:0:0:1:0:0:0:1:0:0:1",
"1:0:0:0:0:1:0:0:0:0:1:0:0:0:1:0:0:0:0:1:0:0:1:0",
"1:0:0:0:0:0:1:0:0:0:0:0:1:0:0:0:0:0:1:0:0:0:0:0:1:0:0:0:0:1",
"1:0:0:0:0:0:0:1:0:0:0:0:0:0:1:0:0:0:0:0:0:1:0:0:0:0:0:0:1:0:0:0:0:0:0:1"}
};




void dvdtable_set_data_glist (GList *p_list, gint num, gchar *new_data)
{
	p_list = g_list_nth (p_list, num);
	if (p_list->data) {
		gchar *ptr = (gchar *)p_list->data;
		g_free (ptr);
		ptr = NULL;
		p_list->data = NULL;
		p_list->data = g_strdup (new_data);
	}
}

gchar *dvdtable_get (gint channel, gint pan)
{
	gchar   **Larrbuf = NULL;
	gchar    *ptr = NULL;
	gint      cpt;
	GList    *list = NULL;
	GList    *Nlist = NULL;
	gchar    *str = NULL;
	GString  *gstr = NULL;
	gint      cpt_sub;
	gint      cpt_ambiance;
	
	/* Pointeur sur la chaine */
	
	channel --;
	switch (pan) {
	case 1 : ptr = af_pan [ channel ].pan1; break;
	case 2 : ptr = af_pan [ channel ].pan2; break;
	case 3 : ptr = af_pan [ channel ].pan3; break;
	case 4 : ptr = af_pan [ channel ].pan4; break;
	case 5 : ptr = af_pan [ channel ].pan5; break;
	case 6 : ptr = af_pan [ channel ].pan6; break;
	}
	channel ++;
	
	/* -- DECOUPE POUR GLIST */
	
	Larrbuf = g_strsplit (ptr, ":", 0);
	for (cpt=0; Larrbuf[cpt]; cpt++) {
		list = g_list_append (list, g_strdup (Larrbuf[cpt]));
	}
	g_strfreev(Larrbuf);

	/* -- MENU VALIDE POUR channel > 2 VERS 2 */
	if (pan == 2 && channel >= 4) {

		cpt_sub = gtk_combo_box_get_active (GTK_COMBO_BOX (var_dvd.Adr_combobox_sub_dvd));
		cpt_ambiance = gtk_combo_box_get_active (GTK_COMBO_BOX (var_dvd.Adr_combobox_ambiance_dvd));
		
		if (channel == 4) {
			if (cpt_ambiance == 0) {
				dvdtable_set_data_glist (list, 4, "1");
				dvdtable_set_data_glist (list, 5, "1");
			}
			else if (cpt_ambiance == 1) {
				dvdtable_set_data_glist (list, 4, "0.7");
				dvdtable_set_data_glist (list, 5, "0.7");
			}
			else if (cpt_ambiance == 2) {
				dvdtable_set_data_glist (list, 4, "0.5");
				dvdtable_set_data_glist (list, 5, "0.5");
			}
		}
		else if (channel == 5) {
			if (cpt_ambiance == 0) {
				dvdtable_set_data_glist (list, 4, "1");
				dvdtable_set_data_glist (list, 7, "1");
			}
			else if (cpt_ambiance == 1) {
				dvdtable_set_data_glist (list, 4, "0.7");
				dvdtable_set_data_glist (list, 7, "0.7");
			}
			else if (cpt_ambiance == 2) {
				dvdtable_set_data_glist (list, 4, "0.5");
				dvdtable_set_data_glist (list, 7, "0.5");
			}
		}	
		else if (channel == 6) {
			if (cpt_ambiance == 0) {
				dvdtable_set_data_glist (list, 4, "1");
				dvdtable_set_data_glist (list, 7, "1");
			}
			else if (cpt_ambiance == 1) {
				dvdtable_set_data_glist (list, 4, "0.7");
				dvdtable_set_data_glist (list, 7, "0.7");
			}
			else if (cpt_ambiance == 2) {
				dvdtable_set_data_glist (list, 4, "0.5");
				dvdtable_set_data_glist (list, 7, "0.5");
			}
			
			if (cpt_sub == 0) {
				dvdtable_set_data_glist (list, 10, "1");
				dvdtable_set_data_glist (list, 11, "1");
			}
			else if (cpt_sub == 1) {
				dvdtable_set_data_glist (list, 10, "0.7");
				dvdtable_set_data_glist (list, 11, "0.7");
			}
			else if (cpt_sub == 2) {
				dvdtable_set_data_glist (list, 10, "0.5");
				dvdtable_set_data_glist (list, 11, "0.5");
			}
		}
	}
	
	/* -- INSERTION DES DATAS DU GLIST DANS UNE CHAINE GString */
	
	gstr = g_string_new (NULL);
	
	Nlist = g_list_first (list);
	while (Nlist) {
		if ((ptr = (gchar *)Nlist->data)) {
			g_string_append_printf (gstr, "%s:", ptr);
		}
		Nlist = g_list_next (Nlist);
	}
	
	/* -- SUPPRIMER LE : DE FIN */
	
	ptr = strrchr (gstr->str, ':');
	*ptr = '\0';
	
	/* -- TRANFERT POUR LE RETOUR */
	
	str = g_strdup (gstr->str);
	g_string_free (gstr, TRUE);
	
	/* -- DESTRUCTION DU GLIST */
	
	cpt = 0;
	Nlist = g_list_first (list);
	while (Nlist) {
		if ((ptr = (gchar *)Nlist->data)) {
			g_free (ptr);
			ptr = NULL;
			Nlist->data = NULL;
		}
		Nlist = g_list_next (Nlist);
	}
	g_list_free (list);
	list = NULL;
	
	/* -- ET RETOUR */
	
	return (str);
}












