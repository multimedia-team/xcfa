 /*
 *  file    : tags_mpc.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

typedef struct {
	unsigned is_4d : 8;
	unsigned is_50 : 8;
	unsigned is_2b : 8;
	unsigned is_StreamMajorVersion : 4;
	unsigned is_StreamMinorVersion : 4;
} MPC_BIT_0;

typedef struct {
	unsigned is_FrameCount : 32;
} MPC_BIT_1;

typedef struct {
	unsigned is_MaxLevel : 16;
	unsigned is_SampleFreq : 2;
	unsigned is_Link : 2;
	unsigned is_Profile : 4;
	unsigned is_MaxBand : 6;
	unsigned is_MidSideStereo : 1;
	unsigned is_IntensityStereo : 1;
} MPC_BIT_2;

typedef struct {
	unsigned is_TitlePeak : 16;
	unsigned is_TitleGain : 16;
} MPC_BIT_3;

typedef struct {
	unsigned is_AlbumPeak : 16;
	unsigned is_AlbumGain : 16;
} MPC_BIT_4;

typedef struct {
	unsigned is_unusued : 19;
	unsigned is_Safely : 1;
	unsigned is_LastFrameLength : 11;
	unsigned is_TrueGapless : 1;
} MPC_BIT_5;

typedef struct {
	unsigned is_unusued : 24;
	unsigned is_EncoderVersion : 8;
} MPC_BIT_6;

typedef struct {
	MPC_BIT_0 bit_0;
	MPC_BIT_1 bit_1;
	MPC_BIT_2 bit_2;
	MPC_BIT_3 bit_3;
	MPC_BIT_4 bit_4;
	MPC_BIT_5 bit_5;
	MPC_BIT_6 bit_6;
} MPC_HEADER;


/*
*---------------------------------------------------------------------------
* FILE IS MPC ?
*---------------------------------------------------------------------------
*/

void tagsmpc_print_entete_mpc (gchar *namefile, MPC_HEADER *fm)
{
	/*PRINT_FUNC_LF();*/

	g_print ("\n");
	g_print ("%s\n", namefile);
	g_print ("\n");
	g_print ("-- BIT 0 -----------------------------------------\n");
	g_print ("\tis_4d                  %4d  -  0x%x\n", fm->bit_0.is_4d, fm->bit_0.is_4d);
	g_print ("\tis_50                  %4d  -  0x%x\n", fm->bit_0.is_50, fm->bit_0.is_50);
	g_print ("\tis_2b                  %4d  -  0x%x\n", fm->bit_0.is_2b, fm->bit_0.is_2b);
	g_print ("\tis_StreamMajorVersion  %4d  -  0x%x\n", fm->bit_0.is_StreamMajorVersion, fm->bit_0.is_StreamMajorVersion);
	g_print ("\tis_StreamMinorVersion  %4d  -  0x%x\n", fm->bit_0.is_StreamMinorVersion, fm->bit_0.is_StreamMinorVersion);
	g_print ("-- BIT 1 -----------------------------------------\n");
	g_print ("\tis_FrameCount          %4d  -  0x%x\n", fm->bit_1.is_FrameCount, fm->bit_1.is_FrameCount);
	g_print ("-- BIT 2 -----------------------------------------\n");
	g_print ("\tis_MaxLevel            %4d  -  0x%x\n", fm->bit_2.is_MaxLevel, fm->bit_2.is_MaxLevel);
	g_print ("\tis_SampleFreq          %4d  -  0x%x\n", fm->bit_2.is_SampleFreq, fm->bit_2.is_SampleFreq);
	switch(fm->bit_2.is_SampleFreq) {
	case 0 : g_print ("\t\t44100 Hz\n"); break;
	case 1 : g_print ("\t\t48000 Hz\n"); break;
	case 2 : g_print ("\t\t37800 Hz\n"); break;
	case 3 : g_print ("\t\t32000 Hz\n"); break;
	}
	g_print ("\tis_Link                %4d  -  0x%x\n", fm->bit_2.is_Link, fm->bit_2.is_Link);
	switch(fm->bit_2.is_Link) {
	case 0 : g_print ("\t\tTitle start or ends with a very low level\n"); break;
	case 1 : g_print ("\t\tTitle ends loudly\n"); break;
	case 2 : g_print ("\t\tTitle start loudly\n"); break;
	case 3 : g_print ("\t\tTitle start loudly and ends loudly\n"); break;
	}
	g_print ("\tis_Profile             %4d  -  0x%x\n", fm->bit_2.is_Profile, fm->bit_2.is_Profile);
	switch (fm->bit_2.is_Profile) {
	case 0  : g_print ("\t\tno profile\n"); break;
	case 1  : g_print ("\t\tUnstable/Experimental\n"); break;
	case 2  : g_print ("\t\tunused\n"); break;
	case 3  : g_print ("\t\tunused\n"); break;
	case 4  : g_print ("\t\tunused\n"); break;
	case 5  : g_print ("\t\tbelow telephone : q=0.0\n"); break;
	case 6  : g_print ("\t\tbelow telephone : q=1.0\n"); break;
	case 7  : g_print ("\t\ttelephone : q=2.0\n"); break;
	case 8  : g_print ("\t\tThumb : q=3.0\n"); break;
	case 9  : g_print ("\t\tRadio : q=4.0\n"); break;
	case 10 : g_print ("\t\tStandard : q=5.0\n"); break;
	case 11 : g_print ("\t\tXtreme : q=6.0\n"); break;
	case 12 : g_print ("\t\tInsane : q=7.0\n"); break;
	case 13 : g_print ("\t\tBrainDead : q=8.0\n"); break;
	case 14 : g_print ("\t\tabove BrainDead : q=9.0\n"); break;
	case 15 : g_print ("\t\tabove BrainDead : q=10.0\n"); break;
	}
	g_print ("\tis_MaxBand             %4d  -  0x%x\n", fm->bit_2.is_MaxBand, fm->bit_2.is_MaxBand);
	g_print ("\tis_MidSideStereo       %4d  -  0x%x\n", fm->bit_2.is_MidSideStereo, fm->bit_2.is_MidSideStereo);
	g_print ("\tis_IntensityStereo     %4d  -  0x%x\n", fm->bit_2.is_IntensityStereo, fm->bit_2.is_IntensityStereo);
	g_print ("-- BIT 3 -----------------------------------------\n");
	g_print ("\tis_TitlePeak           %4d  -  0x%x\n", fm->bit_3.is_TitlePeak, fm->bit_3.is_TitlePeak);
	g_print ("\tis_TitleGain           %4d  -  0x%x\n", fm->bit_3.is_TitleGain, fm->bit_3.is_TitleGain);
	g_print ("-- BIT 4 -----------------------------------------\n");
	g_print ("\tis_AlbumPeak           %4d  -  0x%x\n", fm->bit_4.is_AlbumPeak, fm->bit_4.is_AlbumPeak);
	g_print ("\tis_AlbumGain           %4d  -  0x%x\n", fm->bit_4.is_AlbumGain, fm->bit_4.is_AlbumGain);
	g_print ("-- BIT 5 -----------------------------------------\n");
	g_print ("\tis_unusued             %4d  -  0x%x\n", fm->bit_5.is_unusued, fm->bit_5.is_unusued);
	g_print ("\tis_Safely              %4d  -  0x%x\n", fm->bit_5.is_Safely, fm->bit_5.is_Safely);
	g_print ("\tis_LastFrameLength     %4d  -  0x%x\n", fm->bit_5.is_LastFrameLength, fm->bit_5.is_LastFrameLength);
	g_print ("\tis_TrueGapless         %4d  -  0x%x\n", fm->bit_5.is_TrueGapless, fm->bit_5.is_TrueGapless);
	g_print ("-- BIT 6 -----------------------------------------\n");
	g_print ("\tis_EncoderVersion      %4d  -  0x%x\n", fm->bit_6.is_EncoderVersion, fm->bit_6.is_EncoderVersion);
	g_print ("\n");
}


/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/

INFO_MPC *tagsmpc_remove_info (INFO_MPC *info)
{
	if (info) {

		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}
	return ((INFO_MPC *)NULL);
}

INFO_MPC *tagsmpc_get_info (DETAIL *detail)
{
	INFO_MPC     *ptrinfo = NULL;
	TagLib_File  *file;
	TagLib_Tag   *tag;

	ptrinfo = (INFO_MPC *)g_malloc0 (sizeof (INFO_MPC));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);

	if ((file = taglib_file_new (detail->namefile))) {

		taglib_set_strings_unicode(FALSE);
		tag = taglib_file_tag(file);

		ptrinfo->tags->Title     = g_strdup (taglib_tag_title(tag));
		ptrinfo->tags->Artist    = g_strdup (taglib_tag_artist(tag));
		ptrinfo->tags->Album     = g_strdup (taglib_tag_album(tag));
		ptrinfo->tags->IntYear   = taglib_tag_year(tag);
		ptrinfo->tags->Year      = g_strdup_printf ("%d", ptrinfo->tags->IntYear);
		ptrinfo->tags->Comment   = g_strdup (taglib_tag_comment(tag));
		ptrinfo->tags->IntNumber = taglib_tag_track(tag);
		ptrinfo->tags->Number    = g_strdup_printf ("%d", ptrinfo->tags->IntNumber);
		ptrinfo->tags->Genre     = g_strdup (taglib_tag_genre(tag));
		ptrinfo->tags->IntGenre  = tags_get_genre_by_value (ptrinfo->tags->Genre);
		/*
		printf("title   - \"%s\"\n", taglib_tag_title(tag));
		printf("artist  - \"%s\"\n", taglib_tag_artist(tag));
		printf("album   - \"%s\"\n", taglib_tag_album(tag));
		printf("year    - \"%i\"\n", taglib_tag_year(tag));
		printf("comment - \"%s\"\n", taglib_tag_comment(tag));
		printf("track   - \"%i\"\n", taglib_tag_track(tag));
		printf("genre   - \"%s\"\n", taglib_tag_genre(tag));
		*/
		taglib_tag_free_strings();
		taglib_file_free (file);
	}
	else {
		ptrinfo->tags = (TAGS *)tags_alloc (FALSE);
		tags_set (detail->namefile, ptrinfo->tags);
	}

	return (ptrinfo);
}


