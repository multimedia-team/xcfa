 /*
 *  file      : file_mp3ogg.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "dragNdrop.h"
#include "fileselect.h"
#include "tags.h"
#include "file.h"
#include "statusbar.h"
#include "popup.h"



/*
typedef struct {
	GtkWidget		*Adr_scroll;				// Adresse
	GtkListStore		*Adr_List_Store;			// Adresse
	GtkTreeModel		*Adr_Tree_Model;			// Adresse
	GtkTreeSelection	*Adr_Line_Selected;			// Adresse
	GtkWidget		*Adr_TreeView;				// Adresse
	GtkWidget		*Adr_Entry_Dest;			// Adresse
	GtkWidget		*Adr_Button_Dest;			// Adresse
	GtkComboBox		*Adr_combobox_DestFile;			// Adresse
	GdkPixbuf		*Pixbuf_NotInstall;			// not_install.png
	GtkCellRenderer		*Renderer;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggType;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggBitrate;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggNewBitrate;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggSize;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggTime;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggName;
	gboolean		BoolPopUpIsSelected;			// 
} VAR_FILE_MP3OGG;
*/
VAR_FILE_MP3OGG var_file_mp3ogg = {
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
};

enum
{
	COLUMN_FILEMP3OGG_TYPE,
	COLUMN_FILEMP3OGG_BITRATE,
	COLUMN_FILEMP3OGG_NEWBITRATE,
	COLUMN_FILEMP3OGG_SIZE,
	COLUMN_FILEMP3OGG_TIME,
	COLUMN_FILEMP3OGG_NAME,
	COLUMN_FILEMP3OGG_POINTER_STRUCT,

	COLUMN_FILE_NUM
};
enum
{
	NUM_TREE_FILEMP3OGG_Type = 0,
	NUM_TREE_FILEMP3OGG_Bitrate,
	NUM_TREE_FILEMP3OGG_NOUVEAUBITRATE,
	NUM_TREE_FILEMP3OGG_Size,
	NUM_TREE_FILEMP3OGG_Time,
	NUM_TREE_FILEMP3OGG_Nom,

	NUM_TREE_FILEMP3OGG_ALL_COLUMN
};



/*
// 
//  IDEE POUR AFFICHAGE AVEC PLUS DE DETAILS POUR L'UTILISATEUR
// 
	gchar *str_abr [] = {
	"ABR: -b 32",
	"ABR: -b 40",
	"ABR: -b 48",
	"ABR: -b 56",
	"ABR: -b 64",
	"ABR: -b 80",
	"ABR: -b 96",
	"ABR: -b 112",
	"ABR: -b 128",
	"ABR: -b 160",
	"ABR: -b 192",
	"ABR: -b 224",
	"ABR: -b 256",
	"ABR: -b 320"
	};
	gchar *str_cbr [] = {
	"CBR: -b 32",
	"CBR: -b 40",
	"CBR: -b 48",
	"CBR: -b 56",
	"CBR: -b 64",
	"CBR: -b 80",
	"CBR: -b 96",
	"CBR: -b 112",
	"CBR: -b 128",
	"CBR: -b 160",
	"CBR: -b 192",
	"CBR: -b 224",
	"CBR: -b 256",
	"CBR: -b 320",
	"CBR: --preset insane"
	};
	gchar *str_vbr [] = {
	"VBR: --preset medium",
	"VBR: --preset standard",
	"VBR: --preset extreme",
	"VBR: --preset fast standard",
	"VBR: --preset fast extreme",
	"VBR: -V0",
	"VBR: -V1",
	"VBR: -V2",
	"VBR: -V3",
	"VBR: -V4",
	"VBR: -V5",
	"VBR: -V6",
	"VBR: -V7",
	"VBR: -V8",
	"VBR: -V9"
	};
	gchar *str_vbr_new [] = {
	"VBR NEW",
	"VBR NEW: --preset medium",
	"VBR NEW: --preset standard",
	"VBR NEW: --preset extreme",
	"VBR NEW: --preset fast standard",
	"VBR NEW: --preset fast extreme",
	"VBR NEW: -V0",
	"VBR NEW: -V1",
	"VBR NEW: -V2",
	"VBR NEW: -V3",
	"VBR NEW: -V4",
	"VBR NEW: -V5",
	"VBR NEW: -V6",
	"VBR NEW: -V7",
	"VBR NEW: -V8",
	"VBR NEW: -V9"
	};



	gchar *val[] = {
	"--bitrate=45",
	"--bitrate=64",
	"--bitrate=80",
	"--bitrate=96",
	"--bitrate=112",
	"--bitrate=128",
	"--bitrate=160",
	"--bitrate=192",
	"--bitrate=224",
	"--bitrate=256",
	"--bitrate=320",
	"--quality=-1",
	"--quality=0",
	"--quality=1",
	"--quality=2",
	"--quality=3",
	"--quality=4",
	"--quality=5",
	"--quality=6",
	"--quality=7",
	"--quality=8",
	"--quality=9",
	"--quality=10"
	};
*/


// MISE A JOUR DES PARAMETRES EN TENANT COMPTE DU FLAG: detail->BoolChanged == TRUE
// 
void FileMp3Ogg_change_parameters (void)
{
	GtkTreeIter	iter;
	DETAIL		*detail = NULL;
	gboolean	valid;
	gchar		*PtrBitrate = NULL;

	valid = gtk_tree_model_get_iter_first (var_file_mp3ogg.Adr_Tree_Model, &iter);
	while (valid) {
	
		PtrBitrate = NULL;

		gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
		if (NULL != detail && TRUE == detail->BoolChanged) {
			if (FILE_IS_OGG == detail->type_infosong_file_is) {
				
				INFO_OGG *info = (INFO_OGG *)detail->info;
				
				detail->info = (INFO_OGG *)tagsogg_remove_info (info);
				info = (INFO_OGG *)detail->info;
				detail->info = (INFO_OGG *)tagsogg_get_info (detail);
				info = (INFO_OGG *)detail->info;
				PtrBitrate = info->Nominal_bitrate;
			}
			else if (FILE_IS_MP3 == detail->type_infosong_file_is) {
				
				INFO_MP3 *info = (INFO_MP3 *)detail->info;
				
				detail->info = (INFO_MP3 *)tagsmp3_remove_info (info);
				info = (INFO_MP3 *)detail->info;
				detail->info = (INFO_MP3 *)tagsmp3_get_info (detail);
				info = (INFO_MP3 *)detail->info;
				PtrBitrate = info->bitrate;
			}
			if (NULL != PtrBitrate) {
				gtk_list_store_set (var_file_mp3ogg.Adr_List_Store, &iter, COLUMN_FILEMP3OGG_BITRATE, PtrBitrate, -1);
				gtk_list_store_set (var_file_mp3ogg.Adr_List_Store, &iter, COLUMN_FILEMP3OGG_NEWBITRATE, "", -1);
				gtk_list_store_set (var_file_mp3ogg.Adr_List_Store, &iter, COLUMN_FILEMP3OGG_SIZE, file_get_size (detail), -1);
				gtk_list_store_set (var_file_mp3ogg.Adr_List_Store, &iter, COLUMN_FILEMP3OGG_TIME, file_get_time (detail), -1);
								
				detail->Mp3_Debit =
				detail->Mp3_Mode =
				detail->Ogg_Debit =
				detail->Ogg_Managed =
				detail->Ogg_Downmix = -1;
				detail->BoolChanged = FALSE;
			}
			// BUG SOLVED: 24 03 2012
			// valid = gtk_tree_model_iter_next (var_file_mp3ogg.Adr_Tree_Model, &iter);
		}
		valid = gtk_tree_model_iter_next (var_file_mp3ogg.Adr_Tree_Model, &iter);
	}
}
// 
// 
gboolean FileMp3Ogg_is_appliqued (void)
{
	GList		*List = NULL;
	DETAIL		*detail = NULL;

	List = g_list_first (entetefile);
	while (List) {
		if( NULL != (detail = (DETAIL *)List->data)) {
			if( TRUE == detail->BoolChanged ) return( TRUE );
		}
		List = g_list_next (List);
	}
	return (FALSE);
}
// 
// 
gboolean FileMp3Ogg_is_removed (void)
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*list = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_mp3ogg.Adr_TreeView));
	list = gtk_tree_selection_get_selected_rows (var_file_mp3ogg.Adr_Line_Selected, &model);
	list = g_list_first (list);
	while (list) {
		if (NULL != (path = list->data)) {
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
			if( NULL != detail &&  TRUE == detail->BoolChanged ) return (TRUE);
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
// 
// 
gboolean FileMp3Ogg_is_selected_FILE (TYPE_FILE_IS TypeFileIs)
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*list = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_mp3ogg.Adr_TreeView));
	list = gtk_tree_selection_get_selected_rows (var_file_mp3ogg.Adr_Line_Selected, &model);
	list = g_list_first (list);
	while (list) {
		if (NULL != (path = list->data)) {
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
			if (NULL != detail &&  TypeFileIs == detail->type_infosong_file_is) return (TRUE);
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
// ACTIVE OU DESACTIVE LES ACTIONS
// 
void FileMp3Ogg_set_flag_buttons (void)
{
	gboolean      BoolButtonsAudio [ 3 ] = { FALSE, FALSE, FALSE };

	// APPLIQUE
	BoolButtonsAudio [ 0 ] = FileMp3Ogg_is_appliqued();
	// EFFACE
	BoolButtonsAudio [ 1 ] = FileMp3Ogg_is_removed ();
	// REMOVE FILE
	BoolButtonsAudio [ 2 ] = FileMp3Ogg_is_selected_FILE (FILE_IS_MP3) | FileMp3Ogg_is_selected_FILE (FILE_IS_OGG);	
	// ACTION
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_applique_file_mp3ogg")),			BoolButtonsAudio [ 0 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_efface_bitrate_mp3ogg")),		BoolButtonsAudio [ 1 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_file")),				BoolButtonsAudio [ 2 ]);
}
// MARQUER LES LIGNES POUR LA DESTRUCTION
// 
gboolean FileMp3Ogg_del_file_clicked (void)
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*BeginList = NULL;
	GList		*list = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;
	gboolean	 BoolPrint = FALSE;

	// RECUP. LIGNES EN SELECTION POUR DESTRUCTION
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_mp3ogg.Adr_TreeView));
	if ((BeginList = gtk_tree_selection_get_selected_rows (var_file_mp3ogg.Adr_Line_Selected, &model))) {
		BoolPrint = TRUE;
		list = g_list_first (BeginList);
		while (list) {
			if ((path = list->data)) {
				gtk_tree_model_get_iter (model, &iter, path);				
				gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
				// MARQUER LA LIGNE DU GLIST A DETRUIRE AVANT LE REAFFICHAGE
				if (NULL != detail) detail->BoolRemove = TRUE;
			}
			list = g_list_next (list);
		}
		// gtk_tree_selection_unselect_all (var_file_mp3ogg.Adr_Line_Selected);
	}
	return (BoolPrint);
}
// UPDATE NEW BITRATE
// 
void FileMp3Ogg_update_newbitrate( TYPE_FILE_IS p_TypeFileIs, gint p_debit, gint p_mode_managed, gint p_downmix )
{
	GtkTreeModel		*model = NULL;
	GList			*list = NULL;
	GtkTreePath		*path;
	DETAIL			*detail = NULL;
	GtkTreeIter		 iter;
	gchar			*Str = NULL;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_mp3ogg.Adr_TreeView));
	list = g_list_first (gtk_tree_selection_get_selected_rows (var_file_mp3ogg.Adr_Line_Selected, &model));
	while (list) {
		if (NULL != (path = list->data)) {
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
			
			if (NULL == detail) {
				list = g_list_next(list);
				continue;
			}
			// FILE_IS_MP3
			if (FILE_IS_MP3 == p_TypeFileIs && FILE_IS_MP3 == detail->type_infosong_file_is) {
				
				// Str = popup_get_param_mp3( detail->Mp3_Debit, detail->Mp3_Mode );
				detail->Mp3_Debit = p_debit;
				detail->Mp3_Mode  = p_mode_managed;
				Str = popup_get_param_mp3( p_debit, p_mode_managed );
				gtk_list_store_set (var_file_mp3ogg.Adr_List_Store, &iter, COLUMN_FILEMP3OGG_NEWBITRATE, Str != NULL ? Str : "", -1);
				
				detail->BoolChanged = (Str != NULL) ? TRUE : FALSE;
				if( NULL != Str ) {
					g_free (Str);
					Str = NULL;
				}
			}
			// FILE_IS_OGG
			else if (FILE_IS_OGG == p_TypeFileIs && FILE_IS_OGG == detail->type_infosong_file_is) {
				
				// Str = popup_get_param_ogg( detail->Ogg_Debit, detail->Ogg_Managed, detail->Ogg_Downmix );
				detail->Ogg_Debit   = p_debit;
				detail->Ogg_Managed = p_mode_managed;
				detail->Ogg_Downmix = p_downmix;
				Str = popup_get_param_ogg( p_debit, p_mode_managed, p_downmix );
				gtk_list_store_set (var_file_mp3ogg.Adr_List_Store, &iter, COLUMN_FILEMP3OGG_NEWBITRATE, Str != NULL ? Str : "", -1);
				
				detail->BoolChanged = (Str != NULL) ? TRUE : FALSE;
				if( NULL != Str ) {
					g_free (Str);
					Str = NULL;
				}
			}
		}

		list = g_list_next(list);
	}
	FileMp3Ogg_set_flag_buttons ();
}
// 
// 
void FileMp3Ogg_affiche_glist (void)
{
	DETAIL		*detail = NULL;
	GList		*List = NULL;
	GtkTreeIter	iter;
	GtkAdjustment	*Adj = NULL;
	gdouble		AdjValue;
	gint		Line = 0;		// LIGNE EN COURS
	INFO_MP3	*info_mp3 = NULL;
	INFO_OGG	*info_ogg = NULL;
	gchar		*StrInfo = NULL;
	gchar		*PtrBitrate = NULL;
	gchar		*NameDest = NULL;
	gint		NumLineSelected = -1;
	gboolean	BoolNumLineSelected = FALSE;

	// RECUP SELECTION
	// 
	NumLineSelected = libutils_get_first_line_is_selected( var_file_mp3ogg.Adr_Line_Selected, var_file_mp3ogg.Adr_Tree_Model );
	
	gtk_tree_selection_unselect_all (var_file_mp3ogg.Adr_Line_Selected);

	// DELETE TREEVIEW
	// 
	gtk_list_store_clear (GTK_LIST_STORE (var_file_mp3ogg.Adr_List_Store));
	
	// COORDONNEES POUR UN REAJUSTEMENT VISUEL DE LA PAGE
	// 
	Adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (var_file_mp3ogg.Adr_scroll));
	AdjValue = gtk_adjustment_get_value (Adj);
	
	// AFFICHAGE DE LA LISTE
	// 
	Line = 0;
	List = g_list_first (entetefile);
	while (List) {
		if (NULL != ((detail = (DETAIL *)List->data)) && FALSE == detail->BoolRemove) {
			
			if (FILE_IS_MP3 == detail->type_infosong_file_is) {
				if (NULL != (info_mp3 = (INFO_MP3 *)detail->info)) PtrBitrate = info_mp3->bitrate;
			}
			else if (FILE_IS_OGG == detail->type_infosong_file_is) {
				if (NULL != (info_ogg = (INFO_OGG *)detail->info)) PtrBitrate = info_ogg->Nominal_bitrate;
			}
			else {
				List = g_list_next (List);
				continue;
			}
			if (NULL == info_mp3 && NULL == info_ogg) {
				List = g_list_next (List);
				continue;
			}
			
			if (detail->type_infosong_file_is == FILE_IS_MP3) {
				StrInfo = popup_get_param_mp3( detail->Mp3_Debit, detail->Mp3_Mode );
			}
			else {
				StrInfo = popup_get_param_ogg( detail->Ogg_Debit, detail->Ogg_Managed, detail->Ogg_Downmix );
			}
			
			NameDest = libutils_get_name_without_ext_with_amp (detail->namefile);
			
			gtk_list_store_append (var_file_mp3ogg.Adr_List_Store, &iter);
			gtk_list_store_set (var_file_mp3ogg.Adr_List_Store, &iter,
						COLUMN_FILEMP3OGG_TYPE,			tags_get_str_type_file_is (detail->type_infosong_file_is),
						COLUMN_FILEMP3OGG_BITRATE,		PtrBitrate,
						COLUMN_FILEMP3OGG_NEWBITRATE,		detail->BoolChanged == TRUE ? StrInfo : "",
						COLUMN_FILEMP3OGG_SIZE,			file_get_size (detail),
						COLUMN_FILEMP3OGG_TIME,			file_get_time (detail),
						COLUMN_FILEMP3OGG_NAME,			NameDest,
						// COLUMN_FILEMP3OGG_COLOR,		&YellowColor,
						COLUMN_FILEMP3OGG_POINTER_STRUCT,	detail,
						-1);
			
			g_free (NameDest);	NameDest = NULL;
			g_free (StrInfo);	StrInfo = NULL;
			
			// AFFICHE LES EVENTUELLES LIGNES EN SELECTION
			// 
			if( NumLineSelected == Line ) {
				gtk_tree_selection_select_iter (var_file_mp3ogg.Adr_Line_Selected, &iter);
				BoolNumLineSelected = TRUE;
			}
			
			Line ++;
		}
		List = g_list_next (List);
	}
	
	// SUPPRESSON TABLEAU DES EVENTUELLES LIGNES EN SELECTION
	// 
	if( NumLineSelected == -1 ) {
		if (gtk_tree_model_get_iter_first (var_file_mp3ogg.Adr_Tree_Model, &iter)) {
			gtk_tree_selection_select_iter (var_file_mp3ogg.Adr_Line_Selected, &iter);
			BoolNumLineSelected = TRUE;
		}
	}
	if( NumLineSelected > 0 && NULL != entetefile && BoolNumLineSelected == FALSE ) {
		gtk_tree_selection_select_iter (var_file_mp3ogg.Adr_Line_Selected, &iter);
	}

	// REAJUSTEMENT DE LA LISTE
	// 
	gtk_adjustment_set_value (Adj, AdjValue);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (var_file_mp3ogg.Adr_scroll), Adj);

	FileMp3Ogg_set_flag_buttons ();
}
// 
// 
void on_filemp3ogg_button_efface_bitrate_clicked (GtkButton *button, gpointer user_data)
{
	GtkTreeIter       iter;
	GtkTreeModel     *model = NULL;
	GList            *BeginList = NULL;
	GList            *list = NULL;
	GtkTreePath      *path;
	DETAIL           *detail = NULL;

	// CHERCHE LES ELEMENTS A DETRUIRE
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_mp3ogg.Adr_TreeView));
	BeginList = gtk_tree_selection_get_selected_rows (var_file_mp3ogg.Adr_Line_Selected, &model);
	list = g_list_first (BeginList);
	while (list) {
		if (NULL != (path = list->data)) {
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
			if (NULL != detail) {
				if (detail->type_infosong_file_is == FILE_IS_MP3 || detail->type_infosong_file_is == FILE_IS_OGG) {
					
					detail->Mp3_Debit   = -1;	// SET DEBIT
					detail->Mp3_Mode    = -1;	// SET MODE
					detail->Ogg_Debit   = -1;	// SET DEBIT
					detail->Ogg_Managed = -1;	// SET MANAGED
					detail->Ogg_Downmix = -1;	// SET DOWNMIX
					detail->BoolChanged = FALSE;
				}
				gtk_list_store_set (var_file_mp3ogg.Adr_List_Store, &iter, COLUMN_FILEMP3OGG_NEWBITRATE, "", -1);
			}
		}
		list = g_list_next (list);
	}
	FileMp3Ogg_set_flag_buttons ();
}
// 
// 
void FileMp3Ogg_changed_selection_row (GtkTreeSelection *selection, gpointer data)
{	
	var_file_mp3ogg.Adr_Line_Selected = selection;
	FileMp3Ogg_set_flag_buttons ();
}
// 
// 
gboolean FileMp3Ogg_key_press_event (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	if( TRUE == keys.BoolGDK_Control_A ) {	// CONTROL_A
		gtk_tree_selection_unselect_all (var_file_mp3ogg.Adr_Line_Selected);
		gtk_tree_selection_select_all (var_file_mp3ogg.Adr_Line_Selected);
	}
	if (keys.keyval == GDK_KEY_Delete) {
		GtkTreeIter   iter;
		if (gtk_tree_model_get_iter_first (var_file_mp3ogg.Adr_Tree_Model, &iter)) {
			on_file_button_del_file_clicked (NULL, NULL);
			return (FALSE);
		}
	}
	return (TRUE);
}
// AFFICHAGE DU NOM COMPLET DU FICHIER SI SURVOL PAR LE CURSEUR SOURIS DU CHAMPS 'Nom'
// 
gboolean FileMp3Ogg_event (GtkWidget *treeview, GdkEvent *event, gpointer user_data)
{
	gint                x, y;
	GdkModifierType     state;
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)user_data;
	DETAIL             *detail = NULL;
	gint                Pos_X = 0, Pos_Y = 0;
	gint                i;
	gboolean            BoolSelectColNom = FALSE;
	gboolean            BoolSelectNewBitrate = FALSE;
	
	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &x, &y, &state );
	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &x, &y, &state);
	if (FALSE == gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					   x, y,
					   &path, &column, &Pos_X, &Pos_Y)) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "" );
		return (FALSE);
	}
	
	// RECUPERATION DE LA STRUCTURE POINTEE PAR LE CURSEUR SOURIS
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
	if (NULL == detail) return (FALSE);
	
	// DANS TOUS LES CAS, EFFACE LA BARRE DE TACHE
	StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "" );

	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_FILEMP3OGG_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			switch ( i ) {
			case NUM_TREE_FILEMP3OGG_Nom :			BoolSelectColNom	= TRUE;	break;
			case NUM_TREE_FILEMP3OGG_NOUVEAUBITRATE :	BoolSelectNewBitrate	= TRUE;	break;
			default: 
				StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
				StatusBar_puts();
				return (FALSE);
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}
	if( BoolSelectColNom ) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, detail->namefile );
	}
	else if( BoolSelectNewBitrate ) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("Right click: access popup") );
	}
	
	StatusBar_puts( );
	
	return (FALSE);
}
// 
// 
static void FileMp3Ogg_drag_data_received(
					GtkWidget        *widget,
					GdkDragContext   *context,
					gint              x,
					gint              y,
					GtkSelectionData *data,
					guint             info,
					guint             time,
					gpointer          user_data)
{
	// Une copie ne peut aller vers elle meme !!! */
	if (gtk_drag_get_source_widget(context) != widget) {
		// dragndrop_list_drag_data (widget, (gchar *)data->data);
		dragndrop_list_drag_data( widget, (gchar*)gtk_selection_data_get_data( data ));
	}
}
// 
// 
static void FileMp3Ogg_drag_data_drop (GtkWidget *widget,
					GdkDragContext *dc,
					GtkSelectionData *selection_data,
					guint info,
					guint t,
					gpointer data)
{
	GtkTreeIter       iter;
	GtkTreeModel     *model = NULL;
	GList            *begin_list = NULL;
	GList            *list = NULL;
	GtkTreePath      *path;
	DETAIL           *detail = NULL;
	gchar            *text = NULL;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(widget));
	begin_list = gtk_tree_selection_get_selected_rows (var_file_mp3ogg.Adr_Line_Selected, &model);
	list = g_list_first (begin_list);
	while (list) {
		if ((path = list->data)) {
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
			
			// DEBUG DRAG AND DROP
			// [ Tue, 03 May 2011 17:39:08 +0200 ]
			// XCFA-4.1.0
			// -----------------------------------------------------------
			// OLD CODE:
			// 	text = g_strdup( detail->namefile );
			// NEW_CODE:
			text = g_strdup_printf( "file://%s", detail->namefile );
			
			gdk_drag_status (dc, GDK_ACTION_COPY, t); 

			gtk_selection_data_set( selection_data,
						// GDK_SELECTION_TYPE_STRING,
						// selection_data->target,
						GDK_POINTER_TO_ATOM(gtk_selection_data_get_data( selection_data )),
						8,
						(guchar *)text,
						strlen( text )
						);
			g_free (text);
			text = NULL;
		}
		list = g_list_next (list);
	}
}
// 
// 
gboolean FileMp3Ogg_event_click_mouse( GtkWidget *treeview, GdkEventButton *event, gpointer data )
{
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)data;
	DETAIL             *detail = NULL;
	gint                Pos_X = 0, Pos_Y = 0;
	gboolean            bool_click_droit = (event->button == 3);	
	
	
	
	/* Single clicks only */
	if (event->type != GDK_BUTTON_PRESS) return (FALSE);
	
	/* Si pas de selection a cet endroit retour */
	if (FALSE == gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					  (gint)event->x, (gint)event->y,
					   &path, &column, &Pos_X, &Pos_Y)) return (FALSE);

	// RECUP DE LA STRUCTURE
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_file_mp3ogg.Adr_Tree_Model, &iter, COLUMN_FILEMP3OGG_POINTER_STRUCT, &detail, -1);
	if (NULL == detail) return (FALSE);
	
	/*-----------------*/
	/*if( (bool_click_droit) && (FILE_IS_MP3 == detail->type_infosong_file_is || FILE_IS_OGG == detail->type_infosong_file_is) )
	{
		GtkTreeSelection *selection;

		selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));

		if (gtk_tree_selection_count_selected_rows(selection)  <= 1)
		{
			GtkTreePath *path;

			
			if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(treeview),
							(gint) event->x, 
							(gint) event->y,
							&path, NULL, NULL, NULL))
			{
				gtk_tree_selection_unselect_all(selection);
				gtk_tree_selection_select_path(selection, path);
				gtk_tree_path_free(path);
			}
		}
	}
	*/
	
	/*-----------------*/
	if( bool_click_droit && FILE_IS_MP3 == detail->type_infosong_file_is )
	{
		// DEBIT MODE
		popup_file_mp3_type( detail, detail->Mp3_Debit, detail->Mp3_Mode );
		
		// AUTORISE LE POPUP SUR UNE SELECTION MULTIPLE
		var_file_mp3ogg.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
		gtk_tree_selection_select_path(var_file_mp3ogg.Adr_Line_Selected, path);
		// gtk_tree_path_free(path);
		return TRUE;
		
	}
	else if( bool_click_droit && FILE_IS_OGG == detail->type_infosong_file_is )
	{
		// DEBIT MANAGED DOWNMIX
		popup_file_ogg_type( detail, detail->Ogg_Debit, detail->Ogg_Managed, detail->Ogg_Downmix );
		
		var_file_mp3ogg.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
		// AUTORISE LE POPUP SUR UNE SELECTION MULTIPLE
		gtk_tree_selection_select_path(var_file_mp3ogg.Adr_Line_Selected, path);
		// gtk_tree_path_free(path);
		return TRUE;
		
	}
	
	return (FALSE);
}
// 
// 
static void FileMp3Ogg_add_columns_scrolledwindow (GtkTreeView *treeview)
{
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel      *model = gtk_tree_view_get_model (treeview);
	
	// SIGNAL : 'event'
	g_signal_connect(G_OBJECT(treeview),
			 "event",
                    	 (GCallback) FileMp3Ogg_event,
			 model);

	// SIGNAL : 'Gestion click click'
	g_signal_connect(G_OBJECT(treeview),
			 "button-press-event",
                    	 (GCallback) FileMp3Ogg_event_click_mouse,
			 model);
	
	// SIGNAL : Ligne actuellement selectionnee 'changed'
	var_file_mp3ogg.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	g_signal_connect(G_OBJECT(var_file_mp3ogg.Adr_Line_Selected),
			 "changed",
                   	 G_CALLBACK(FileMp3Ogg_changed_selection_row),
                   	 model);
	
	// SIGNAL 'key-press-event'
	g_signal_connect(G_OBJECT(treeview),
			 "key-press-event",
                    	 (GCallback) FileMp3Ogg_key_press_event,
			 model);
	
	// Drag and drop support
	// SIGNAL : 'drag-data-received'
	gtk_drag_dest_set (GTK_WIDGET (treeview),
			   GTK_DEST_DEFAULT_MOTION |
			   GTK_DEST_DEFAULT_DROP,
			   drag_types, n_drag_types,
			   GDK_ACTION_COPY| GDK_ACTION_MOVE );
	g_signal_connect(G_OBJECT(treeview),
			 "drag-data-received",
			 G_CALLBACK(FileMp3Ogg_drag_data_received),
			 NULL);

	gtk_drag_source_set(
			GTK_WIDGET(treeview),
			GDK_BUTTON1_MASK | GDK_BUTTON2_MASK | GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP,
			drag_types, n_drag_types,
			GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_DEFAULT
			);

	g_signal_connect(G_OBJECT(treeview),
      			"drag-data-get",
			 G_CALLBACK(FileMp3Ogg_drag_data_drop),
			 treeview);
	
	// COLUMN_FILEMP3OGG_PLAY 
	// var_file_mp3ogg.Adr_ColumnFileMp3OggPlay =
	
	// COLUMN_FILEMP3OGG_TYPE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_mp3ogg.Adr_ColumnFileMp3OggType =
	column = gtk_tree_view_column_new_with_attributes (_("Type"),
						     renderer,
						     "text", COLUMN_FILEMP3OGG_TYPE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	/* TRIS */
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_FILEMP3OGG_TYPE);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEMP3OGG_BITRATE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_mp3ogg.Adr_ColumnFileMp3OggBitrate =
	column = gtk_tree_view_column_new_with_attributes ("Bitrate",
						     renderer,
						     "text", COLUMN_FILEMP3OGG_BITRATE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	// TRIS
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_FILEMP3OGG_BITRATE);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEMP3OGG_NEWBITRATE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_mp3ogg.Adr_ColumnFileMp3OggNewBitrate =
	column = gtk_tree_view_column_new_with_attributes (_("New bitrate"),
						     renderer,
						     "markup", COLUMN_FILEMP3OGG_NEWBITRATE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 350);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEMP3OGG_SIZE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_mp3ogg.Adr_ColumnFileMp3OggSize =
	column = gtk_tree_view_column_new_with_attributes (_("Size"),
						     renderer,
						     "text", COLUMN_FILEMP3OGG_SIZE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEMP3OGG_TIME
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_mp3ogg.Adr_ColumnFileMp3OggTime =
	column = gtk_tree_view_column_new_with_attributes (_("Time"),
						     renderer,
						     "text", COLUMN_FILEMP3OGG_TIME,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEMP3OGG_NAME
	var_file_mp3ogg.Renderer =
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_mp3ogg.Adr_ColumnFileMp3OggName =
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
						     renderer,
						     "markup", COLUMN_FILEMP3OGG_NAME,
						     // "background-gdk", COLUMN_FILEMP3OGG_COLOR,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
}
// 
// 
void on_scrolledwindow_file_mp3ogg_realize (GtkWidget *widget, gpointer user_data)
{
	GtkListStore *store;
	GtkTreeModel *model;
	GtkWidget    *treeview;

	var_file_mp3ogg.Adr_scroll = widget;

	var_file_mp3ogg.Pixbuf_NotInstall		= libutils_init_pixbufs ("xcfa/not_install.png");
	
	var_file_mp3ogg.Adr_List_Store = store =
	gtk_list_store_new (	COLUMN_FILE_NUM,	/* TOTAL NUMBER				*/
				G_TYPE_STRING,		/* COLUMN_FILEMP3OGG_TYPE		*/
				G_TYPE_STRING,		/* COLUMN_FILEMP3OGG_BITRATE		*/
				G_TYPE_STRING,		/* COLUMN_FILEMP3OGG_NEWBITRATE		*/
				G_TYPE_STRING,		/* COLUMN_FILEMP3OGG_SIZE		*/
				G_TYPE_STRING,		/* COLUMN_FILEMP3OGG_TIME		*/
				G_TYPE_STRING,		/* COLUMN_FILEMP3OGG_NAME		*/
				// GDK_TYPE_COLOR,		/* COLUMN_FILEMP3OGG_COLOR		*/
				G_TYPE_POINTER          /* COLUMN_FILEMP3OGG_POINTER_STRUCT	*/
			   );
	var_file_mp3ogg.Adr_Tree_Model = model = GTK_TREE_MODEL (store);
	var_file_mp3ogg.Adr_TreeView =
	treeview = gtk_tree_view_new_with_model (model);
	// gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview)), GTK_SELECTION_MULTIPLE);	// GTK_SELECTION_BROWSE MULTIPLE
	g_object_unref (model);
	gtk_container_add (GTK_CONTAINER (widget), treeview);
	FileMp3Ogg_add_columns_scrolledwindow (GTK_TREE_VIEW (treeview));
	gtk_widget_show_all (widget);
}

