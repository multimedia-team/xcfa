 /*
 *  file      : options_faac.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "options.h"


static 	gchar *str_faac_conteneur[] = {
	"",
	"-w"
	};
static 	gchar *str_faac_abr[] = {
	"-b 56",
	"-b 64",
	"-b 80",
	"-b 96",
	"-b 128",
	"-b 152"
	};
static 	gchar *str_faac_vbr[] = {
	"-q 10",
	"-q 30",
	"-q 50",
	"-q 70",
	"-q 80",
	"-q 90",
	"-q 100",
	"-q 120",
	"-q 150",
	"-q 200",
	"-q 250",
	"-q 300",
	"-q 400",
	"-q 500"
	};
gchar *OptionsFaac_get_faac_conteneur (void)
{
	if (NULL == var_options.Adr_Widget_faac_conteneur) return ( (gchar *)str_faac_conteneur[ 0 ]);
	return ( (gchar *)str_faac_conteneur[ gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_conteneur)) ]);	
}
gchar *OptionsFaac_get_faac_set_choice_vbr_abr (void)
{
	if (0 == gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_choice_vbr_abr))) {
		gint	ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_set_choice_vbr_abr));
		if (ind < 0) ind = 0;
		return ( (gchar *)str_faac_vbr[ ind ]);
	}
	return ( (gchar *)str_faac_abr[ gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_set_choice_vbr_abr)) ]);
}



void on_combobox_faac_conteneur_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Transport Stream (ADTS) - Extension .aac"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("MPEG-4 File Format (MP4) - Extension .m4a"));
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.ConteneurFacc);
	var_options.Adr_Widget_faac_conteneur = GTK_COMBO_BOX (widget);

	// on_combobox_faac_set_choice_vbr_abr_changed (NULL,NULL);
}
void on_combobox_faac_conteneur_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_faac_conteneur) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_conteneur))) >= 0)
			Config.ConteneurFacc = ind;
		
		OptionsInternal_set_datas_interne (COLOR_FAAC_CONTENEUR, var_options.Adr_label_faac_m4a, FAAC_WAV_TO_M4A);
	}
}



void on_combobox_faac_choice_vbr_abr_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_faac_choice_vbr_abr = GTK_COMBO_BOX (widget);
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "VBR");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "ABR");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.AbrVbrFacc);
}
void on_combobox_faac_choice_vbr_abr_changed (GtkComboBox *combobox, gpointer user_data)
{	
	if (NULL != var_options.Adr_Widget_faac_choice_vbr_abr) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_choice_vbr_abr))) >= 0)
			Config.AbrVbrFacc = ind;
	}
	if (var_options.Adr_Widget_faac_choice_vbr_abr == NULL) return;
	if (var_options.Adr_Widget_faac_set_choice_vbr_abr == NULL) return;
	
	switch (gtk_combo_box_get_active (var_options.Adr_Widget_faac_choice_vbr_abr)) {
	
	/* VBR
	 * 10, 30, 50, 70, 80, 90, 100, 120, 150, 200, 250, 300, 400, 500
    	 */
	case 0 :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "10");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "30");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "50");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "70");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "80");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "90");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "100");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "120");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "150");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "200");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "250");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "300");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "400");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "500");
		
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_set_choice_vbr_abr), Config.VbrFaccIndice);
		break;
	
	/* ABR
	 * 56, 64, 80, 96, 128, 152
	 */
	case 1 :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "56");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "64");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "80");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "96");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "128");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_faac_set_choice_vbr_abr), "152");
		
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_set_choice_vbr_abr), Config.AbrFaccIndice);
		break;
	}
	OptionsInternal_set_datas_interne (COLOR_FAAC_SET_CHOICE_VBR_ABR, var_options.Adr_label_faac_m4a, FAAC_WAV_TO_M4A);
}



void on_combobox_faac_set_choice_vbr_abr_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_faac_set_choice_vbr_abr = GTK_COMBO_BOX (widget);
	on_combobox_faac_choice_vbr_abr_changed (NULL,NULL);
}
void on_combobox_faac_set_choice_vbr_abr_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_faac_set_choice_vbr_abr) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_set_choice_vbr_abr))) >= 0) {
			switch (gtk_combo_box_get_active (var_options.Adr_Widget_faac_choice_vbr_abr)) {
			case 0 :
				Config.VbrFaccIndice = ind;
				break;
			case 1 :
				Config.AbrFaccIndice = ind;
				break;
			}
		}
		OptionsInternal_set_datas_interne (COLOR_FAAC_SET_CHOICE_VBR_ABR, var_options.Adr_label_faac_m4a, FAAC_WAV_TO_M4A);
	}
}

