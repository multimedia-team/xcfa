 /*
 *  file      : get_info.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifndef get_info_h
#define get_info_h 1

#include "file.h"

typedef struct {
	guint	 SecTime;
	gchar	*time;
	gchar	*size;
} SHNTAG;

SHNTAG		*GetInfo_shntool (gchar *file);
SHNTAG		*GetInfo_free_shntool (SHNTAG *ShnTag);

GString		*GetInfo_file (gchar *file);
GString		*GetInfo_checkmp3 (gchar *file);
GString		*GetInfo_cdparanoia( gchar *p_device );
GString		*GetInfo_faad (gchar *file);
GString		*GetInfo_ogginfo (gchar *file);
GString		*GetInfo_mediainfo (gchar *file);
GString		*GetInfo_which (gchar *file);
GString		*GetInfo_cdplay (gchar *file);
GString		*GetInfo_metaflac (gchar *file);
gchar		*GetInfo_cpu (void);
void		GetInfo_cpu_print (void);
gchar		*GetInfo_cpu_str (void);
gboolean	GetInfo_wget (void);
void		GetInfo_eject (gchar *StrDevice);
TYPE_FILE_IS	GetInfo_file_is (gchar *PathName);
glong		GetInfo_level_df (void);
gboolean	GetInfo_level_bool_percent (void);
gint		GetInfo_level_get_from (TYPE_FILE_IS TypeFileIs, gchar *file);
gchar		*GetInfo_cd_discid( gchar *p_device );

#endif

