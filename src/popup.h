 /*
 *  file      : popup.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifndef popup_h
#define popup_h 1


#include "cd_audio.h"


void	popup_cd (CD_AUDIO *Audio, TYPE_FILE_IS TypeFileIs);
void	popup_file (DETAIL *detail, TYPE_FILE_IS TypeFileIs);
void	popup_file_ReplayGain (DETAIL *detail);
void	popup_file_wav_frequence (DETAIL *detail);
void	popup_file_wav_piste (DETAIL *detail);
void	popup_file_wav_quantification (DETAIL *detail);


#include "poche.h"

void	popup_file_Split (void);
void	popup_flip( IMAGE *p_Image );
void	popup_viewport( GLIST_POCHETTE *gl );
void	popup_trash (void);
void	popup_normalise_cd (void);
void	popup_normalise_dvd (void);



typedef struct {
	gboolean	BoolFromPopup;
	gint		num;
	gchar		*name;
} CD_POPUP_GENRE;
extern CD_POPUP_GENRE CdPopupGenre ;
void	popup_menu_cd( void );


typedef struct {
	gboolean	BoolFromPopup;
	gint		num;
	gchar		*name;
} FILE_POPUP_GENRE;
extern FILE_POPUP_GENRE FilePopupGenre ;
void 	popup_menu_file( void );



void	popup_file_mp3_type( DETAIL *detail, gint p_debit, gint p_mode );
void	popup_file_ogg_type( DETAIL *detail, gint p_debit, gint p_managed, gint p_downmix );
gchar	*popup_get_param_ogg( gint p_debit, gint p_managed, gint p_downmix );
gchar	*popup_get_param_mp3( gint p_debit, gint p_mode );

#endif


