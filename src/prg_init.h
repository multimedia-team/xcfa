 /*
 *  file      : prg_init.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifndef prg_init_h
#define prg_init_h 1


typedef struct {
	GtkListStore      *Adr_List_Store;			// 
	GtkTreeModel      *Adr_Tree_Model;			// 
	GtkWidget         *Adr_scroll;				// 
	GtkTreeSelection  *Adr_Line_Selected;		// 
	gboolean           Bool_Goto_Page_Options;	// 
	GdkPixbuf         *Pixbuf_Ok;				// 
	GdkPixbuf         *Pixbuf_Not_Ok;			// 
} VAR_PRGEXTERN;

typedef enum {
	NMR_a52dec = 0,								// 
	NMR_aacplusenc,								// 
	NMR_cdparanoia,								// 
	NMR_cd_discid,								// 
	NMR_checkmp3,								// 
	NMR_faac,									// 
	NMR_faad,									// 
	NMR_flac,									// 
	NMR_icedax,									// 
	NMR_lame,									// 
	NMR_lsdvd,									// 
	NMR_mac,									// 
	NMR_musepack_tools_mppdec,					// 
	NMR_musepack_tools_mppenc,					// 
	NMR_mplayer,								// 
	NMR_mp3gain,								// 
	NMR_normalize,								// 
	NMR_libnotify_bin,							// 
	NMR_shorten,								// 
	NMR_shntool,								// 
	NMR_sox,									// 
	NMR_vorbis_tools_oggenc,					// 
	NMR_vorbisgain,								// 
	NMR_wavpack,								// 
	NMR_MAX_TABLEAU								// 
} TYPE_PROGINIT;

typedef struct {
	gchar		*PackageDebian64;				// PACKAGEDEBIAN_64
	gchar		*PackageDebian32;				// PACKAGEDEBIAN_32
	gchar		*Name1;							// NAME1
	gchar		*Name2;							// NAME2
	gchar		*Name3;							// NAME3
	gboolean	*BoolFound;						// FOUND
	gchar		*PtrName;						// NAME TREE-VIEW : pointeur sur Name1 | Name2 | Name3
	GdkPixbuf	*Pixbuf;						// PIXBUF TREE-VIEW
	gchar		*Paquage;						// PAQUAGE
	gchar		*Description;					// DESCRIPTION TREE-VIEW
} TABLEAU_PRG_EXTERN;

typedef struct {
	gboolean	bool_a52dec;					// 
	gboolean	bool_notify_send;				// 
	gboolean	bool_ape;						// 
	gboolean	bool_mpc123_mppdec;				// 
	gboolean	bool_mppenc;					// 
	gboolean	bool_cdparanoia;				// 
	gboolean	bool_cdda2wav;					// 
	gboolean	bool_lame;						// 
	gboolean	bool_oggenc;					// 
	gboolean	bool_sox;						// 
	gboolean	bool_normalize;					// 
	gboolean	bool_checkmp3;					// 
	gboolean	bool_faad;						// 
	gboolean	bool_faac;						// 
	gboolean	bool_mplayer;					// 
	gboolean	bool_shorten;					// 
	gboolean	bool_lsdvd;						// 
	gboolean	bool_vorbisgain;				// 
	gboolean	bool_flac;						// 
	gboolean	bool_mp3gain;					// 
	gboolean	bool_wavpack;					// 
	gboolean	bool_shntool;					// 
	gboolean	bool_aacplusenc;				// 
	gboolean	bool_cd_discid;					// 

} PRGINIT;

extern PRGINIT PrgInit;

gchar		*prginit_get_name (TYPE_PROGINIT TypeEnum);


#endif

