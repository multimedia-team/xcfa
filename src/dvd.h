 /*
 *  file      : dvd.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef dvd_audio_h
#define dvd_audio_h 1

typedef enum {
	_DVD_NAME=0,									// Le nom du DVD
	_DVD_TITLE,										// Le titre
	_DVD_CHAPTER,									// Le chapitre
	_DVD_DATA,										// Les datas pour une extraction partielle
	_DVD_DATA_ALL									// Les datas pour une extraction complete
} TYPE_LIST_DVD;

typedef enum {
	_INCONNU_ = 0,									// 
	_DTS_,											// 
	_AC3_											// 
} FORMAT_ID;

typedef enum {
	_COCHE_ = 0,									// Pret a etre selectionne
	_SELECTED_,										// En selection avec le canal d'origine
	_CH_2_											// En selection avec le canal 2
} ETAT_CHOIX;

typedef struct {
	TYPE_LIST_DVD		 type_list_dvd;				// Le type
	gchar			*def;							// La chaine d'info
	gchar			*time;							// La duree
	guint			 GUINT_TempsTotal;				// Duree totale en secondes
	gchar			*name_file;						// Le nom du fichier qui pourra etre modifie
	gchar			*StaticNameFile;				// Le nom du fichier original
	FORMAT_ID		 format_id;						// _INCONNU_, _DTS_ ou _AC3_
	gchar			*StrNumerateTitle;				// Le numero du titre
	gint			 NumerateTitle;					// Le numero du titre
	gchar			*StrNumerateChapter;			// Le numero du chapitre
	gint			 NumerateChapter;				// Le numero du chapitre
	gchar			*StrNumberChannel;				// Le nombre de canaux
	gchar			*StrNumerateStreamId;			// La langue
	gint			 OldChannels;					// Le canal d'origine
	gint			 NewChannels;					// Le nouveau canal
	ETAT_CHOIX		 EtatChoix;						// _COCHE_, _SELECTED_, _CH_2_
	gint			 NumStruct;						// Numero pour dvd_event
	gboolean		 BoolDvdPlay;					// TRUE = Play, FALSE = NONE
	gint			 DebutLecture;					// 
	gboolean		 EtatNormalise;					// 
	
} VAR;

typedef enum {
	_DVDAUDIO_DVD_ = 0,								// La source = DVD
	_DVDAUDIO_FILE_									// La source = dossier (DVD)
} DVDAUDIO_SOURCE;

typedef struct {
	gboolean		 bool_dvd;						// 
	gchar			*path;							// 
} INFO_DVDAUDIO;

typedef struct {
	gdouble			 double_TempsTotal;				//
	guint			 guint_TempsTotal;				// 
	GList			*list;							// 
} PLAY_DVDAUDIO;

typedef struct {

	GtkComboBox		*Adr_combobox_normalise_dvd;	// Adresse
	
	GtkTreeModel	*Adr_Tree_Model;				// 
	GtkWidget		*Adr_TreeView;					// 
	GtkWidget		*Adr_scroll;					// Adresse
	GtkTreeSelection	*Adr_Line_Selected;			// Adresse

	GdkPixbuf		*Pixbuf_FilePlay;				// 
	GdkPixbuf		*Pixbuf_FileStop;				// 
	GdkPixbuf		*Pixbuf_Coche;					// 
	GdkPixbuf		*Pixbuf_Coche_Exist;			// 
	GdkPixbuf		*Pixbuf_Selected;				// 
	GdkPixbuf		*Pixbuf_2ch;					// 

	GtkButton		*Adr_button_destination_dvd;	// 
	GtkComboBox		*Adr_ComboBox_Reader;			// 
	
	GtkComboBox		*Adr_combobox_sub_dvd;			// 
	GtkComboBox		*Adr_combobox_ambiance_dvd;		// 

	gboolean		 bool_click;					// 

	gint			 total_selected_2ch;			// 
	gint			 total_selected;				// 
	gboolean		 bool_selected;					// 
	gboolean		 bool_sa;						// 

	guint			 Handler_Timeout;				// 
	gboolean		 bool_end_pthread;				// 
	gboolean		 bool_read_dvd;					// 
	gboolean		 bool_halt;						// 

	gint			 int_temp;						// 

	INFO_DVDAUDIO		 from;						// 

	gint			 NumStruct;						// Numero pour dvdaudio_event
	gboolean		 bool_dedans;					// 
	
	gint			 bool_err;						// 

	GtkTreeViewColumn	*Adr_Column_Titres;			// 
	GtkTreeViewColumn	*Adr_Column_Play;			// 
	GtkTreeViewColumn	*Adr_Column_Temps;			// 
	GtkTreeViewColumn	*Adr_Column_Format;			// 
	GtkTreeViewColumn	*Adr_Column_Choix;			// 
	GtkTreeViewColumn	*Adr_Column_Normalise;		// 
	GtkCellRenderer		*Renderer;					// 
	GtkTreeViewColumn	*Adr_Column_Nom;			// 
		
	guint			 GUINT_TempsTotal;				// Duree totale en secondes
	
	PLAY_DVDAUDIO		 PlayDvdAudio;				// 
	
	gint			 TTNormPeak;					// Nombre total de Peak
	gint			 TTNormPeakCollectif;			// Nombre total de Peak Collectif
	
	gboolean		 BoolNormIsSelected;
	
	gchar			*PathDvd;
	
} VAR_DVD;

extern VAR_DVD var_dvd;								// 

extern GList *GlistDvd;								// 

typedef struct {
	VAR			*Var;								// 
	gboolean		 EtatNormalise;
	gchar			*Path;							// Nom du fichier final
	GList			*list;							// Glist pour l'extraction
} NEW_DVD_EXTRACT;

extern GList *GlistDvdExtract;


typedef enum {
		
	// SELECTION ou DESELECTION POUR LA NORMALISATION
	CD_NORMALISE_SELECT_V = 0,
	CD_NORMALISE_DESELECT_V
	
} TYPE_SET_FROM_POPUP_DVD;

/*
*---------------------------------------------------------------------------
* DVD_EXTRACT.C
*---------------------------------------------------------------------------
*/
void      dvdextract_dvd_to_file (void);

/*
*---------------------------------------------------------------------------
* DVD_READ.C
*---------------------------------------------------------------------------
*/
void      dvdread_remove_list (void);
gboolean  dvdread_dvd_read (void);

/*
*---------------------------------------------------------------------------
* DVD.C
*---------------------------------------------------------------------------
*/

void      dvd_make_scrolledwindow_realize (GtkWidget *widget);
void      dvd_combobox_peripherique_dvd_realize (GtkWidget *widget);
void      dvd_reffresh_list_dvd (void);
void      dvd_button_destination_dvd_realize (GtkWidget *widget);
void      dvd_button_destination_dvd_clicked (void);
void      dvd_button_extraction_dvd_clicked (void);
void      dvd_button_annuler_lecture_dvd_clicked (void);
void      dvd_set_flag_buttons_dvd (void);
void      dvd_remove_GtkTree (void);
void      dvd_combobox_sub_dvd_realize (GtkWidget *widget);
void      dvd_combobox_ambiance_dvd_realize (GtkWidget *widget);
gboolean  dvd_bool_read_dvd_from_directory (void);
void      dvd_pixbuf_stop_play (void);
void      dvd_set_etat_music_pixbuf (gboolean *PtrBoolDvdPlay, gboolean p_BoolDvdPlay);
void      dvd_set_flag_normalise (gboolean EtatNormalise);
void      dvd_update (void);
void      dvd_combobox_normalise_dvd_realize (GtkWidget *widget);
gint      dvd_get_value_normalise_dvd (void);
void      dvd_button_eject_dvd_clicked (void);
void	  dvd_from_popup (TYPE_SET_FROM_POPUP_DVD TypeSetFromPopup, gboolean EtatNormalise);

/*
*---------------------------------------------------------------------------
* DVD_TABLE.C
*---------------------------------------------------------------------------
*/
gchar     *dvdtable_get (gint channel, gint pan);

#endif


