 /*
 *  file    : tags_wavpack.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>
#include "get_info.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

/*
typedef struct {
    char ckID [4];              "wvpk"
    uint32_t ckSize;            size of entire block (minus 8, of course)
    uint16_t version;           0x402 to 0x410 are currently valid for decode
    uchar track_no;             track number (0 if not used, like now)
    uchar index_no;             track sub-index (0 if not used, like now)
    uint32_t total_samples;     total samples for entire file, but this is
                                only valid if block_index == 0 and a value of
                                -1 indicates unknown length
    uint32_t block_index;       index of first sample in block relative to
                                beginning of file (normally this would start
                                at 0 for the first block)
    uint32_t block_samples;     number of samples in this block (0 = no audio)
    uint32_t flags;             various flags for id and decoding
    uint32_t crc;               crc for actual decoded data
} WavpackHeader;
*/

/*
typedef struct {
	gchar id [ 3 ];
	gchar title [ 30 ];
	gchar interprete [ 30 ];
	gchar album [ 30 ];
	gchar annee [ 4 ];
	gchar comment [ 30 ];
	gchar genre [ 1 ];
} TAGS_WAVPACK;
*/


/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/


INFO_WAVPACK *tagswavpack_remove_info (INFO_WAVPACK *info)
{
	if (info) {
		if (NULL != info->time)		{ g_free (info->time);		info->time = NULL;	}
		if (NULL != info->size)		{ g_free (info->size);		info->size = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}
	return ((INFO_WAVPACK *)NULL);
}

INFO_WAVPACK *tagswavpack_get_info (DETAIL *detail)
{
	INFO_WAVPACK	*ptrinfo = NULL;
	SHNTAG		*ShnTag = GetInfo_shntool (detail->namefile);
	gint		m;
	gint		s;
	gint		sec;

	ptrinfo = (INFO_WAVPACK *)g_malloc0 (sizeof (INFO_WAVPACK));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);

	ptrinfo->size     = g_strdup (ShnTag->size);
		
	ptrinfo->SecTime =
	sec = ShnTag->SecTime;
	s = sec % 60; sec /= 60;
	m = sec % 60; sec /= 60;
	if (sec > 0) ptrinfo->time = g_strdup_printf ("%02d:%02d:%02d", sec, m, s);
	else         ptrinfo->time = g_strdup_printf ("%02d:%02d", m, s);
	
	ShnTag = GetInfo_free_shntool (ShnTag);
	
	return (ptrinfo);
}
