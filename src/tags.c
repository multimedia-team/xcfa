 /*
 *  file      : tags.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>



// 
// 
TAGS *tags_alloc (gboolean bool_tag_cd)
{
	TAGS *new = (TAGS *)g_malloc0 (sizeof (TAGS));
	
	/* TRUE  is CD-AUDIO
	*  FALSE is FILE
	*/
	if (new) {
		new->bool_tag_cd = bool_tag_cd;
		new->bool_save = FALSE;
	}
	
	return ((TAGS *)new);
}
// 
// 
TAGS *tags_remove (TAGS *tags)
{
	if (NULL != tags) {
		if (NULL != tags->Album)	{ g_free (tags->Album);		tags->Album = NULL;		}
		if (NULL != tags->Artist)	{ g_free (tags->Artist);	tags->Artist = NULL;		}
		if (NULL != tags->Title)	{ g_free (tags->Title);		tags->Title = NULL;		}
		if (NULL != tags->Number)	{ g_free (tags->Number);	tags->Number = NULL;		}
		if (NULL != tags->Genre)	{ g_free (tags->Genre);		tags->Genre = NULL;		}
		if (NULL != tags->Year)		{ g_free (tags->Year);		tags->Year = NULL;		}
		if (NULL != tags->Comment)	{ g_free (tags->Comment);	tags->Comment = NULL;		}
		if (NULL != tags->Description)	{ g_free (tags->Description);	tags->Description = NULL;	}
		g_free (tags);
		tags = NULL;
	}
	return ((TAGS *)NULL);
}
// 
// 
void tags_set_flag_modification (TAGS *tags, gboolean p_flag)
{
	if (tags) tags->bool_save = p_flag;
}
// 
// 
gboolean tags_is_modified (TAGS *tags)
{
	return (tags ? tags->bool_save : FALSE);
}
// 
// 
TAGS *tags_set (gchar *filename, TAGS *tags)
{
	gchar *Ptr = NULL;
	
	if (tags->Artist == NULL) {
		Ptr = strrchr (filename, '/');
		if (Ptr) {
			Ptr ++;
			tags->Artist = g_strdup (Ptr);
			Ptr = strrchr (tags->Artist, '.');
			if (Ptr) *Ptr = '\0';
		}
		else {
			tags->Artist = g_strdup (filename);
		}
	}
	if (tags->Title == NULL) {
		Ptr = strrchr (filename, '/');
		if (Ptr) {
			Ptr ++;
			tags->Title = g_strdup (Ptr);
			Ptr = strrchr (tags->Title, '.');
			if (Ptr) *Ptr = '\0';
		}
		else {
			tags->Title = g_strdup (filename);
		}
	}

	// Complete l'allocation
	if (NULL == tags->Album)	tags->Album       = g_strdup ("");
	if (NULL == tags->Artist)	tags->Artist      = g_strdup ("");
	if (NULL == tags->Title)	tags->Title       = g_strdup ("");

	if (NULL == tags->Number) {
		tags->Number      = g_strdup ("1");
		tags->IntNumber   = 1;
	} else {
		tags->IntNumber   = atoi (tags->Number);
	}
	
	if (NULL == tags->Genre) {
		tags->Genre       = g_strdup ("1");
		tags->IntGenre    = 1;
	} else {
		tags->IntGenre    = atoi (tags->Genre);
	}
	
	if (NULL == tags->Year) {
		tags->Year        = g_strdup ("1962");
		tags->IntYear     = 1962;
	} else {
		tags->IntYear     = atoi (tags->Year);
	}

	if (NULL == tags->Comment)	tags->Comment     = g_strdup ("");
	if (NULL == tags->Description)	tags->Description = g_strdup ("");
	
	return ((TAGS *)tags);
}
// 
// 
gchar *tags_get_time (gchar *namefile)
{
	TagLib_File  *file;
	// TagLib_Tag   *tag;
	const TagLib_AudioProperties *properties;
	gint          seconds;
	gint          minutes;
	gchar        *RetTime = NULL;
	
	if ((file = taglib_file_new (namefile))) {
	
		taglib_set_strings_unicode(FALSE);
		// tag = taglib_file_tag(file);
		taglib_file_tag(file);
		properties = taglib_file_audioproperties(file);
		
		seconds = taglib_audioproperties_length(properties) % 60;
		minutes = (taglib_audioproperties_length(properties) - seconds) / 60;

		RetTime = g_strdup_printf ("%02d:%02d", minutes, seconds);
		
		taglib_tag_free_strings();
		taglib_file_free (file);
	}
	else {
		RetTime = g_strdup ("???");
	}
	return (RetTime);
}
// 
// 
gchar *tags_get_time_wav (gchar *namefile)
{
	gchar     *ptr = NULL;
	INFO_WAV *info = NULL;

	if ((info = tagswav_get_info (namefile))) {
		ptr = g_strdup (info->time);
		info = tagswav_remove_info (info);
	}
	if (NULL == ptr) ptr = g_strdup ("???");
	return (ptr);
}
// 
// 
// 
// 
// typedef struct {
// 	gint   num;
// 	gchar *name;
// } STRUCT_TAGS_FILE_MP3;
// 
// 
STRUCT_TAGS_FILE_MP3 StructTagsFileMp3 [] = {

{123, "A Cappella"},
{ 74, "Acid Jazz"},
{ 73, "Acid Punk"},
{ 34, "Acid"},
{ 99, "Acoustic"},
{ 40, "Alt. Rock"},
{ 20, "Alternative"},
{ 26, "Ambient"},
{145, "Anime"},
{ 90, "Avantgarde"},
{116, "Ballad"},
{ 41, "Bass"},
{135, "Beat"},
{ 85, "Bebob"},
{ 96, "Big Band"},
{138, "Black Metal"},
{ 89, "Bluegrass"},
{  0, "Blues"},
{107, "Booty Bass"},
{132, "BritPop"},
{ 65, "Cabaret"},
{ 88, "Celtic"},
{104, "Chamber Music"},
{102, "Chanson"},
{ 97, "Chorus"},
{136, "Christian Gangsta Rap"},
{ 61, "Christian Rap"},
{141, "Christian Rock"},
{  1, "Classic Rock"},
{ 32, "Classical"},
{128, "Club-House"},
{112, "Club"},
{ 57, "Comedy"},
{140, "Contemporary Christian"},
{  2, "Country"},
{139, "Crossover"},
{ 58, "Cult"},
{125, "Dance Hall"},
{  3, "Dance"},
{ 50, "Darkwave"},
{ 22, "Death Metal"},
{  4, "Disco"},
{ 55, "Dream"},
{127, "Drum & Bass"},
{122, "Drum Solo"},
{120, "Duet"},
{ 98, "Easy Listening"},
{ 52, "Electronic"},
{ 48, "Ethnic"},
{124, "Euro-House"},
{ 25, "Euro-Techno"},
{ 54, "Eurodance"},
{ 84, "Fast-Fusion"},
{ 81, "Folk/Rock"},
{115, "Folklore"},
{ 80, "Folk"},
{119, "Freestyle"},
{  5, "Funk"},
{ 30, "Fusion"},
{ 36, "Game"},
{ 59, "Gangsta Rap"},
{126, "Goa"},
{ 38, "Gospel"},
{ 91, "Gothic Rock"},
{ 49, "Gothic"},
{  6, "Grunge"},
{ 79, "Hard Rock"},
{129, "Hardcore"},
{137, "Heavy Metal"},
{  7, "Hip-Hop"},
{ 35, "House"},
{100, "Humour"},
{131, "Indie"},
{ 19, "Industrial"},
{ 46, "Instrumental Pop"},
{ 47, "Instrumental Rock"},
{ 33, "Instrumental"},
{146, "JPop"},
{ 29, "Jazz+Funk"},
{  8, "Jazz"},
{ 63, "Jungle"},
{ 86, "Latin"},
{ 71, "Lo-Fi"},
{ 45, "Meditative"},
{142, "Merengue"},
{  9, "Metal"},
{148, "Misc"},
{ 77, "Musical"},
{ 82, "National Folk"},
{ 64, "Native American"},
{133, "Negerpunk"},
{ 10, "New Age"},
{ 10, "NewAge"},
{ 66, "New Wave"},
{ 39, "Noise"},
{ 11, "Oldies"},
{103, "Opera"},
{ 12, "Other"},
{ 75, "Polka"},
{134, "Polsk Punk"},
{ 53, "Pop-Folk"},
{ 62, "Pop/Funk"},
{ 13, "Pop"},
{109, "Porn Groove"},
{117, "Power Ballad"},
{ 23, "Pranks"},
{108, "Primus"},
{ 92, "Progressive Rock"},
{ 93, "Psychedelic Rock"},
{ 67, "Psychedelic"},
{121, "Punk Rock"},
{ 43, "Punk"},
{ 14, "R&B"},
{ 15, "Rap"},
{ 68, "Rave"},
{ 16, "Reggae"},
{ 76, "Retro"},
{ 87, "Revival"},
{118, "Rhythmic Soul"},
{ 78, "Rock & Roll"},
{ 17, "Rock"},
{143, "Salsa"},
{114, "Samba"},
{110, "Satire"},
{ 69, "Showtunes"},
{ 21, "Ska"},
{111, "Slow Jam"},
{ 95, "Slow Rock"},
{105, "Sonata"},
{ 42, "Soul"},
{ 37, "Sound Clip"},
{ 24, "Soundtrack"},
{ 56, "Southern Rock"},
{ 44, "Space"},
{101, "Speech"},
{ 83, "Swing"},
{ 94, "Symphonic Rock"},
{106, "Symphony"},
{147, "Synthpop"},
{113, "Tango"},
{ 51, "Techno-Industrial"},
{ 18, "Techno"},
{130, "Terror"},
{144, "Thrash Metal"},
{ 60, "Top 40"},
{ 70, "Trailer"},
{ 31, "Trance"},
{ 72, "Tribal"},
{ 27, "Trip-Hop"},
{ 28, "Vocal"},
{102, "Chanson française"},
{-1,  NULL}
};
// 
// 
gint tags_get_genre_by_value( gchar *p_name )
{
	gint	i;
	gint	RetNum = -1;
	gchar	*NameSrc = g_ascii_strdown( p_name, -1 );
	gchar	*NameStruct = NULL;
	
	// g_print( "-- RECHERCHE DE: '%s'  '%s'\n", p_name, NameSrc );
	for( i=0; StructTagsFileMp3[ i ].num != -1; i++ ) {

		NameStruct = g_ascii_strdown( StructTagsFileMp3[ i ].name, -1 );
		
		
		// if( strlen( NameSrc ) == strlen( NameStruct ) && 0 == strcmp( NameSrc, NameStruct ) ) {
		if( strlen( NameSrc ) == strlen( NameStruct ) && 0 == strncmp( NameSrc, NameStruct, strlen( NameSrc ))) {
			// g_print( "NameSrc = %s   NameStruct = %s\n", NameSrc, NameStruct );
			RetNum = StructTagsFileMp3[ i ].num;
			break;
		}
		g_free( NameStruct );	NameStruct = NULL;
	}
	
	if( NULL != NameSrc )	 { g_free( NameSrc );		NameSrc = NULL;		}
	if( NULL != NameStruct ) { g_free( NameStruct );	NameStruct = NULL;	}
	
	return( RetNum );
}
// 
// 
void tags_set_elements_combobox (GtkWidget *widget)
{
	gint i;
	
	for (i=0; StructTagsFileMp3[ i ].num != -1; i++)
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), StructTagsFileMp3[ i ].name);
}
// 
// 
gint tags_get_elements_combobox (gint num)
{
	gint i;
	
	for (i=0; StructTagsFileMp3[ i ].num != num; i++);
	
	if (i > 148) i = 0;

	return (i);
}
// 
// 
gint tags_get_num_combobox (gint value)
{
	return (StructTagsFileMp3[ value ].num);
}
// 
// 
gchar *tags_get_genre_by_name( gint value )
{
	gint i;

	if( value < 0 || value > 148 ) return ((gchar *)NULL );

	for( i=0; StructTagsFileMp3[ i ].num != value; i++ );

	return( (gchar *)StructTagsFileMp3[ i ].name );
}
// 
// 
gchar *tags_get_str_type_file_is (TYPE_FILE_IS type)
{
	static gchar str_type_file [ 8 ];

	/* PRINT_FUNC_LF(); */

	if (type == FILE_IS_FLAC)     	   strcpy (str_type_file, "FLAC");
	else if (type == FILE_IS_WAV)      strcpy (str_type_file, "WAV ");
	else if (type == FILE_IS_MP3)      strcpy (str_type_file, "MP3 ");
	else if (type == FILE_IS_OGG)      strcpy (str_type_file, "OGG ");
	else if (type == FILE_IS_SHN)      strcpy (str_type_file, "SHN ");
	else if (type == FILE_IS_M4A)      strcpy (str_type_file, "M4A ");
	else if (type == FILE_IS_VID_M4A)  strcpy (str_type_file, "M4A ");
	else if (type == FILE_IS_AAC)      strcpy (str_type_file, "AAC ");
	else if (type == FILE_IS_WMA)      strcpy (str_type_file, "WMA ");
	else if (type == FILE_IS_RM)       strcpy (str_type_file, "RM ");
	else if (type == FILE_IS_DTS)      strcpy (str_type_file, "DTS ");
	else if (type == FILE_IS_AIFF)     strcpy (str_type_file, "AIFF ");
	else if (type == FILE_IS_MPC)      strcpy (str_type_file, "MPC ");
	else if (type == FILE_IS_APE)      strcpy (str_type_file, "APE ");
	else if (type == FILE_IS_WAVPACK)  strcpy (str_type_file, "WVP ");
	else if (type == FILE_IS_AC3)      strcpy (str_type_file, "AC3 ");
	else				   strcpy (str_type_file, "???");

	return (&str_type_file[0]);
}





