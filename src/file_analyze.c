 /*
 *  file      : file_analyse.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <dirent.h>
#include <sys/stat.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "win_scan.h"
#include "get_info.h"
#include "tags.h"
#include "win_scan.h"
#include "prg_init.h"
#include "win_info.h"
#include "file.h"




typedef struct {
	gchar		*Name;
	// TYPE_FILESELECTION	TypeFileIs;
	TYPE_FILE_IS	TypeFileIs;
} VAR_LIST_FILES;

typedef struct {
	
	GList			*ListReceive;
	GList			*ListFiles;		// VAR_LIST_FILES
	TYPE_FILESELECTION	TypeFileselection;
	
	// RECHERCHE RECURSIVE DES FICHIERS
	// INDIQUE LE NOMBRE TOTAL DANS: VarAnalyze.RecTotalFile
	// 
	gchar			*RecPtrDir;
	gchar			*RecName;
	gint			RecTotalFile;
	gint			RecActivate;
	double			percent;
	VAR_LIST_FILES		*VarListFiles;
	
	gboolean		BoolInThread;
	gboolean		BoolSetPercent;
	guint			HandlerTimeoutDo;
	gboolean		BoolNoFindCheckMp3;
	
} VAR_ANALYZE;

VAR_ANALYZE VarAnalyze = {NULL, NULL, -1,  NULL, NULL, 0, 0, 0.0, NULL, FALSE, FALSE, -1 };

GList *entetefile = NULL;				/* pointeur sur DETAIL */



// SUPPRESSION DU CONTENU DE entetefile SI BoolRemove==TRUE ET DU GLIST
// 
void fileanalyze_remove_entetefile_detail( void )
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	
	if( NULL != ( list = g_list_first( entetefile ))) {
		while (list) {
			if( NULL != (detail = (DETAIL *)list->data) ) {
			
				if( TRUE == detail->BoolRemove ) {
				
					if( NULL != detail->namefile ) {
						g_free( detail->namefile );
						detail->namefile = NULL;
					}
					if( NULL != detail->NameDest ) {
						g_free( detail->NameDest );
						detail->NameDest = NULL;
					}
					if( NULL != detail->NameFileCopyFromNormalizate ) {
						g_free( detail->NameFileCopyFromNormalizate );
						detail->NameFileCopyFromNormalizate = NULL;
					}
				
					if (detail->type_infosong_file_is == FILE_IS_FLAC) {
						INFO_FLAC *info = (INFO_FLAC *)detail->info;
						detail->info = (INFO_FLAC *)tagsflac_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_WAV) {
						INFO_WAV *info = (INFO_WAV *)detail->info;
						// LevelDbfs    = info->LevelDbfs;
						detail->info = (INFO_WAV *)tagswav_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_MP3) {
						INFO_MP3 *info = (INFO_MP3 *)detail->info;
						// LevelDbfs    = info->LevelDbfs;
						detail->info = (INFO_MP3 *)tagsmp3_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_OGG) {
						INFO_OGG *info = (INFO_OGG *)detail->info;
						// LevelDbfs    = info->LevelDbfs;
						detail->info = (INFO_OGG *)tagsogg_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_M4A) {
						INFO_M4A *info = (INFO_M4A *)detail->info;
						detail->info = (INFO_M4A *)tagsm4a_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_VID_M4A) {
						INFO_M4A *info = (INFO_M4A *)detail->info;
						detail->info = (INFO_M4A *)tagsm4a_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_AAC) {
						INFO_AAC *info = (INFO_AAC *)detail->info;
						detail->info = (INFO_AAC *)tagsaac_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_SHN) {
						INFO_SHN *info = (INFO_SHN *)detail->info;
						detail->info = (INFO_SHN *)tagsshn_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_WMA) {
						INFO_WMA *info = (INFO_WMA *)detail->info;
						detail->info = (INFO_WMA *)tagswma_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_RM) {
						INFO_RM *info = (INFO_RM *)detail->info;
						detail->info = (INFO_RM *)tagsrm_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_DTS) {
						INFO_DTS *info = (INFO_DTS *)detail->info;
						detail->info = (INFO_DTS *)tagsdts_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_AIFF) {
						INFO_AIFF *info = (INFO_AIFF *)detail->info;
						detail->info = (INFO_AIFF *)tagsaiff_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_MPC) {
						INFO_MPC *info = (INFO_MPC *)detail->info;
						detail->info = (INFO_MPC *)tagsmpc_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_APE) {
						INFO_APE *info = (INFO_APE *)detail->info;
						detail->info = (INFO_APE *)tagsape_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_WAVPACK) {
						INFO_WAVPACK *info = (INFO_WAVPACK *)detail->info;
						detail->info = (INFO_WAVPACK *)tagswavpack_remove_info (info);
					}
					else if (detail->type_infosong_file_is == FILE_IS_AC3) {
						INFO_AC3 *info = (INFO_AC3 *)detail->info;
						detail->info = (INFO_AC3 *)tagsac3_remove_info (info);
					}
			
					g_free( detail );
					detail = NULL;
				}
			}
			list->data = NULL;
			list = g_list_next (list);
		}

		g_list_free( entetefile );
	}
	entetefile = NULL;
}
// SUPPRESSION DU CONTENU DE entetefile ET DU GLIST
// 
void fileanalyze_remove_entetefile (void)
{
	GList   *list = NULL;
	DETAIL  *detail = NULL;
	gint	 NbList = 0;
	
	if( NULL != ( list = g_list_first( entetefile ))) {
		while (list) {
			if (NULL != (detail = (DETAIL *)list->data)) {
			
				// fileanalyze_remove_entetefile_detail( &detail );
			
				if( NULL != detail->namefile ) {
					g_free( detail->namefile );
					detail->namefile = NULL;
				}
				if( NULL != detail->NameDest ) {
					g_free( detail->NameDest );
					detail->NameDest = NULL;
				}
				if( NULL != detail->NameFileCopyFromNormalizate ) {
					g_free( detail->NameFileCopyFromNormalizate );
					detail->NameFileCopyFromNormalizate = NULL;
				}
				
				if (detail->type_infosong_file_is == FILE_IS_FLAC) {
					INFO_FLAC *info = (INFO_FLAC *)detail->info;
					detail->info = (INFO_FLAC *)tagsflac_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_WAV) {
					INFO_WAV *info = (INFO_WAV *)detail->info;
					// LevelDbfs    = info->LevelDbfs;
					detail->info = (INFO_WAV *)tagswav_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_MP3) {
					INFO_MP3 *info = (INFO_MP3 *)detail->info;
					// LevelDbfs    = info->LevelDbfs;
					detail->info = (INFO_MP3 *)tagsmp3_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_OGG) {
					INFO_OGG *info = (INFO_OGG *)detail->info;
					// LevelDbfs    = info->LevelDbfs;
					detail->info = (INFO_OGG *)tagsogg_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_M4A) {
					INFO_M4A *info = (INFO_M4A *)detail->info;
					detail->info = (INFO_M4A *)tagsm4a_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_VID_M4A) {
					INFO_M4A *info = (INFO_M4A *)detail->info;
					detail->info = (INFO_M4A *)tagsm4a_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_AAC) {
					INFO_AAC *info = (INFO_AAC *)detail->info;
					detail->info = (INFO_AAC *)tagsaac_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_SHN) {
					INFO_SHN *info = (INFO_SHN *)detail->info;
					detail->info = (INFO_SHN *)tagsshn_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_WMA) {
					INFO_WMA *info = (INFO_WMA *)detail->info;
					detail->info = (INFO_WMA *)tagswma_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_RM) {
					INFO_RM *info = (INFO_RM *)detail->info;
					detail->info = (INFO_RM *)tagsrm_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_DTS) {
					INFO_DTS *info = (INFO_DTS *)detail->info;
					detail->info = (INFO_DTS *)tagsdts_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_AIFF) {
					INFO_AIFF *info = (INFO_AIFF *)detail->info;
					detail->info = (INFO_AIFF *)tagsaiff_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_MPC) {
					INFO_MPC *info = (INFO_MPC *)detail->info;
					detail->info = (INFO_MPC *)tagsmpc_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_APE) {
					INFO_APE *info = (INFO_APE *)detail->info;
					detail->info = (INFO_APE *)tagsape_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_WAVPACK) {
					INFO_WAVPACK *info = (INFO_WAVPACK *)detail->info;
					detail->info = (INFO_WAVPACK *)tagswavpack_remove_info (info);
				}
				else if (detail->type_infosong_file_is == FILE_IS_AC3) {
					INFO_AC3 *info = (INFO_AC3 *)detail->info;
					detail->info = (INFO_AC3 *)tagsac3_remove_info (info);
				}
			
				detail = list->data = NULL;
			
				NbList ++;
			}
			list = g_list_next (list);
		}
		g_list_free( entetefile );
		entetefile = NULL;
	}
	
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\tRemove: %d\n", NbList);
}
// 
// 
gboolean fileanalyze_exist (DETAIL *detail, gchar *NewExt)
{
	gchar     *Name = NULL;
	gboolean   bool_exist = FALSE;
	
	if (NULL == (Name = file_get_pathname_dest (detail, NewExt))) return (FALSE);

	bool_exist = libutils_access_mode (Name);
	g_free (Name);
	Name = NULL;
	return (bool_exist);
}
// RECHERCHE RECURSIVE DES FICHIERS
// INDIQUE LE NOMBRE TOTAL DANS: VarAnalyze.RecTotalFile
// 
// 
// 
void fileanalyze_recherche_recursive (gchar *Directory)
{
	DIR    *dp;
	struct  dirent *entry;
	struct  stat statbuf;
	
	// PRINT_FUNC_LF();
	
	if (NULL == (dp = opendir (Directory))) {
		if (TRUE == libutils_test_file_exist (Directory) && libutils_get_size_file (Directory) > 100) {
			VarAnalyze.VarListFiles             = (VAR_LIST_FILES *)g_malloc0 (sizeof (VAR_LIST_FILES));
			VarAnalyze.VarListFiles->Name       = g_strdup (Directory);
			VarAnalyze.VarListFiles->TypeFileIs = FILE_IS_NONE;
			VarAnalyze.ListFiles                = g_list_append (VarAnalyze.ListFiles, VarAnalyze.VarListFiles);
			VarAnalyze.RecTotalFile ++;
		}
		return;
	}
		
	chdir (Directory);
	while ((entry = readdir (dp)) != NULL) {
		
		if (WindScan_close_request () == TRUE) break;

		lstat (entry->d_name, &statbuf);
		
		if (S_ISDIR (statbuf.st_mode)) {
			if (strcmp (".", entry->d_name) == 0 || strcmp ("..", entry->d_name) == 0) continue;
			fileanalyze_recherche_recursive (entry->d_name);
		}
		else {
			// VarAnalyze.RecPtrDir = g_get_current_dir ();
			// VarAnalyze.RecPtrDir = get_current_dir_name();
			
			// Comme  une  extension  du  standard POSIX.1-2001, la version  Linux (libc4, libc5, glibc) de getcwd() alloue le
			// tampon dynamiquement avec malloc(3), si buf est NULL. Dans ce cas, le tampon alloué a la longueur size à moins
			// que size soit égal à zéro, auquel cas buf est alloué avec la taille nécessaire.
			// L'appelant doit libérer avec free(3) le tampon renvoyé.
			if ((VarAnalyze.RecPtrDir = getcwd(VarAnalyze.RecPtrDir, 0)) != NULL) {
							
				VarAnalyze.RecName = g_strdup_printf ("%s/%s", VarAnalyze.RecPtrDir, entry->d_name);
				free (VarAnalyze.RecPtrDir);
				VarAnalyze.RecPtrDir = NULL;
				
				// LA TAILLE DU FICHIER DOIT ETRE POSITIVE
				if (libutils_get_size_file (VarAnalyze.RecName) > 100) {
					
					// ALIMENTE LA LISTE AVEC LE NOM COMPLET DE FICHIER: PATH + NAMEFILE
					VarAnalyze.VarListFiles             = (VAR_LIST_FILES *)g_malloc0 (sizeof (VAR_LIST_FILES));
					VarAnalyze.VarListFiles->Name       = VarAnalyze.RecName;
					VarAnalyze.VarListFiles->TypeFileIs = FILE_IS_NONE;
					
					VarAnalyze.ListFiles = g_list_append (VarAnalyze.ListFiles, VarAnalyze.VarListFiles);
					VarAnalyze.RecTotalFile ++;
				}
				else {
					g_free (VarAnalyze.RecName);
					VarAnalyze.RecName = NULL;
				}
			}
		}
	}
	chdir ("..");
	closedir (dp);
}
// 
// RETOURNE UN TYPE [ TYPE_FILE_IS ] OU -1
// 
TYPE_FILE_IS fileanalyze_is_valid (gchar *PathName, TYPE_FILESELECTION p_TypeFileselection)
{
	gint		TypeTabFileAll[] = {
					FILE_IS_FLAC,
					FILE_IS_WAV,
					FILE_IS_MP3,
					FILE_IS_OGG,
					FILE_IS_M4A,
					FILE_IS_VID_M4A,
					// FILE_IS_AAC,
					FILE_IS_SHN,
					FILE_IS_WMA,
					FILE_IS_RM,
					FILE_IS_DTS,
					FILE_IS_AIFF,
					FILE_IS_MPC,
					FILE_IS_APE,
					FILE_IS_WAVPACK,
					FILE_IS_AC3,
					-1};
	gint		TypeTabFileWav[] =    { FILE_IS_WAV, -1};
	gint		TypeTabFileMp3Ogg[] = { FILE_IS_MP3, FILE_IS_OGG, -1};
	gint		TypeTabFileTags[] =   { FILE_IS_FLAC, FILE_IS_MP3, FILE_IS_OGG, FILE_IS_MPC, -1};
	gint		Cpt;
	TYPE_FILE_IS	TypeFileIs = -1;
	
	if (_PATH_LOAD_FILE_ALL_ == p_TypeFileselection) {
		TypeFileIs = GetInfo_file_is (PathName);
		for (Cpt = 0; TypeTabFileAll[ Cpt ] != -1; Cpt ++) {
			if (TypeFileIs == TypeTabFileAll[ Cpt ]) return (TypeFileIs);
		}
	}
	else if (_PATH_LOAD_FILE_WAV_ == p_TypeFileselection) {
		TypeFileIs = GetInfo_file_is (PathName);
		for (Cpt = 0; TypeTabFileWav[ Cpt ] != -1; Cpt ++) {
			if (TypeFileIs == TypeTabFileWav[ Cpt ]) return (TypeFileIs);
		}
	}
	else if (_PATH_LOAD_FILE_MP3OGG_ == p_TypeFileselection) {
		TypeFileIs = GetInfo_file_is (PathName);
		for (Cpt = 0; TypeTabFileMp3Ogg[ Cpt ] != -1; Cpt ++) {
			if (TypeFileIs == TypeTabFileMp3Ogg[ Cpt ]) return (TypeFileIs);
		}
	}
	else if (_PATH_LOAD_FILE_TAGS_ == p_TypeFileselection) {
		TypeFileIs = GetInfo_file_is (PathName);
		for (Cpt = 0; TypeTabFileTags[ Cpt ] != -1; Cpt ++) {
			if (TypeFileIs == TypeTabFileTags[ Cpt ]) return (TypeFileIs);
		}
	}
	return (-1);
}
// VERIFIE LA VALIDITE DU FICHIER
// 
gboolean fileanalyze_is_dupply (gchar *namefile)
{
	GList   *list = NULL;
	DETAIL  *detail = NULL;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (*detail->namefile == *namefile)
				if (strcmp (namefile, detail->namefile) == 0) return (TRUE);
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
// VERIFIE LA VALIDITE DU FICHIER
// 
void fileanalyze_verif_validity_files (void)
{
	GList		*List = NULL;
	VAR_LIST_FILES	*VarListFiles = NULL;
	TYPE_FILE_IS	TypeFileIs = FILE_IS_NONE;

	List = g_list_first (VarAnalyze.ListFiles);
	while (List) {
		
		VarAnalyze.RecActivate ++;
		VarAnalyze.BoolSetPercent = TRUE;
			
		if (WindScan_close_request () == TRUE) break;
		
		if (NULL != ((VarListFiles = (VAR_LIST_FILES *)List->data))) {
			
			if (TRUE == fileanalyze_is_dupply (VarListFiles->Name)) {
					// g_print("TRUE == fileanalyze_is_dupply (%s)\n", VarListFiles->Name);
					g_free (VarListFiles->Name);
					VarListFiles->Name = NULL;
					g_free (VarListFiles);
					VarListFiles = List->data = NULL;
			}
			else if (_PATH_LOAD_FILE_ALL_ == VarAnalyze.TypeFileselection) {
				if (-1 == ((TypeFileIs = fileanalyze_is_valid (VarListFiles->Name, _PATH_LOAD_FILE_ALL_)))) {
					// g_print("NIL _PATH_LOAD_FILE_ALL_ : %s\n", VarListFiles->Name);
					g_free (VarListFiles->Name);
					VarListFiles->Name = NULL;
					g_free (VarListFiles);
					VarListFiles = List->data = NULL;
				}
				// PROGRAM checkmp3 (mp3check) NOT FOUND
				else if (FILE_IS_MP3 == TypeFileIs && FALSE == PrgInit.bool_checkmp3) {
					
					VarAnalyze.BoolNoFindCheckMp3 = TRUE;
					
					g_free (VarListFiles->Name);
					VarListFiles->Name = NULL;
					g_free (VarListFiles);
					VarListFiles = List->data = NULL;
				}
			}
			else if (_PATH_LOAD_FILE_WAV_ == VarAnalyze.TypeFileselection) {
				if (-1 == ((TypeFileIs = fileanalyze_is_valid (VarListFiles->Name,_PATH_LOAD_FILE_WAV_)))) {
					// g_print("NIL _PATH_LOAD_FILE_WAV_ : %s\n", VarListFiles->Name);
					g_free (VarListFiles->Name);
					VarListFiles->Name = NULL;
					g_free (VarListFiles);
					VarListFiles = List->data = NULL;
				}
			}
			else if (_PATH_LOAD_FILE_MP3OGG_ == VarAnalyze.TypeFileselection) {
				if (-1 == ((TypeFileIs = fileanalyze_is_valid (VarListFiles->Name,_PATH_LOAD_FILE_MP3OGG_)))) {
					// g_print("NIL _PATH_LOAD_FILE_MP3OGG_ : %s\n", VarListFiles->Name);
					g_free (VarListFiles->Name);
					VarListFiles->Name = NULL;
					g_free (VarListFiles);
					VarListFiles = List->data = NULL;
				}
				// PROGRAM checkmp3 (mp3check) NOT FOUND
				else if (FILE_IS_MP3 == TypeFileIs && FALSE == PrgInit.bool_checkmp3) {
					
					VarAnalyze.BoolNoFindCheckMp3 = TRUE;
					
					g_free (VarListFiles->Name);
					VarListFiles->Name = NULL;
					g_free (VarListFiles);
					VarListFiles = List->data = NULL;
				}
			}
			else if (_PATH_LOAD_FILE_TAGS_ == VarAnalyze.TypeFileselection) {
				if (-1 == ((TypeFileIs = fileanalyze_is_valid (VarListFiles->Name,_PATH_LOAD_FILE_TAGS_)))) {
					// g_print("NIL _PATH_LOAD_FILE_TAGS_ : %s\n", VarListFiles->Name);
					g_free (VarListFiles->Name);
					VarListFiles->Name = NULL;
					g_free (VarListFiles);
					VarListFiles = List->data = NULL;
				}
				// PROGRAM checkmp3 (mp3check) NOT FOUND
				else if (FILE_IS_MP3 == TypeFileIs && FALSE == PrgInit.bool_checkmp3) {
					
					VarAnalyze.BoolNoFindCheckMp3 = TRUE;
					
					g_free (VarListFiles->Name);
					VarListFiles->Name = NULL;
					g_free (VarListFiles);
					VarListFiles = List->data = NULL;
				}
			}
			// STOCKE LE TYPE DE FICHIER
			if (NULL != VarListFiles) {
				// g_print("%s\n", VarListFiles->Name);
				VarListFiles->TypeFileIs = TypeFileIs;
			}
		}
		List = g_list_next (List);
	}
}
// 
// 
void fileanalyze_set_in_list (void)
{
	GList		*List = NULL;
	DETAIL		*detail = NULL;
	VAR_LIST_FILES	*VarListFiles = NULL;
	
	List = g_list_first (VarAnalyze.ListFiles);
	while (List) {
		if (NULL != ((VarListFiles = (VAR_LIST_FILES *)List->data))) {
			
			VarAnalyze.RecActivate ++;
			VarAnalyze.BoolSetPercent = TRUE;
			
			detail = (DETAIL *)g_malloc0 (sizeof (DETAIL));
			detail->namefile              = g_strdup (VarListFiles->Name);
			detail->NameFileCopyFromNormalizate = NULL;
			detail->NameDest              = libutils_get_name_without_ext( VarListFiles->Name );
			
			detail->type_infosong_file_is = VarListFiles->TypeFileIs;
			
			detail->EtatTrash             = FILE_TRASH_NONE;
			
			detail->EtatSelection_Wav     = ETAT_ATTENTE;
			detail->EtatSelection_Flac    = ETAT_ATTENTE;
			detail->EtatSelection_Ape     = ETAT_ATTENTE;
			detail->EtatSelection_WavPack = ETAT_ATTENTE;
			detail->EtatSelection_Ogg     = ETAT_ATTENTE;
			detail->EtatSelection_M4a     = ETAT_ATTENTE;
			detail->EtatSelection_Aac     = ETAT_ATTENTE;
			detail->EtatSelection_Mpc     = ETAT_ATTENTE;
			detail->EtatSelection_Mp3     = ETAT_ATTENTE;
			
			detail->BoolRemove            = STRUCT_NO_REMOVE;
			
			detail->Etat_Normalise        = NORM_NONE;
			if (detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_WAV) {
				detail->Etat_Normalise     = NORM_READY_FOR_SELECT;
			}
			detail->Etat_Scan             = ETAT_SCAN_NONE;
			detail->LevelMix              = -14;
			
			detail->PConv                 = NULL;
			detail->PConvWav              = NULL;
			detail->PConvMp3Ogg           = NULL;

			detail->Etat_ReplayGain       = RPG_NONE;
			if (detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_FLAC ||
			    detail->type_infosong_file_is == FILE_IS_WAVPACK) {
				detail->Etat_ReplayGain = RPG_ATTENTE;
			}
			
			// MP3: DEBIT MODE
			detail->Mp3_Debit    = -1;
			detail->Mp3_Mode     = -1;
			// OGG: DEBIT MANAGED DOWNMIX
			detail->Ogg_Debit    = -1;
			detail->Ogg_Managed  = -1;
			detail->Ogg_Downmix  = -1;
			detail->BoolChanged  = FALSE;
			
			if (VarListFiles->TypeFileIs == FILE_IS_FLAC)         detail->info = tagsflac_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_WAV)     detail->info = tagswav_get_info (detail->namefile);
			else if (VarListFiles->TypeFileIs == FILE_IS_MP3)     detail->info = tagsmp3_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_OGG)     detail->info = tagsogg_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_SHN)     detail->info = tagsshn_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_WMA)     detail->info = tagswma_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_RM)      detail->info = tagsrm_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_DTS)     detail->info = tagsdts_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_AIFF)    detail->info = tagsaiff_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_M4A)     detail->info = tagsm4a_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_VID_M4A) detail->info = tagsm4a_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_MPC)     detail->info = tagsmpc_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_APE)     detail->info = tagsape_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_WAVPACK) detail->info = tagswavpack_get_info (detail);
			else if (VarListFiles->TypeFileIs == FILE_IS_AC3)     detail->info = tagsac3_get_info (detail);
			else						      detail->info = NULL;
			
			// 
			// CONDITION AJOUTEE SUITE A POST DEPUIS:
			//	http://forum.ubuntu-fr.org/viewtopic.php?pid=4018344#p4018344
			// 
			if (NULL != detail->info) {
				// INSERTION DE LA STRUCTURE DETAIL DANS LA LISTE ENTETEFILE
				entetefile = g_list_append (entetefile, detail);
			}
			
			g_free (VarListFiles->Name);
			VarListFiles->Name = NULL;
			g_free (VarListFiles);
			VarListFiles = List->data = NULL;
		}
		List = g_list_next (List);
	}
}
// 
// 
static void fileanalyze_thread (void *arg)
{
	gchar		*Ptr = NULL;
	GList		*List = NULL;
	
	VarAnalyze.BoolInThread = TRUE;
	
	// CONSTRUIT UNE LISTE DE TOUS LES FICHIERS SANS LES DOSSIERS
	List = g_list_first (VarAnalyze.ListReceive);
	while (List) {
		if ((Ptr = (gchar *)List->data) != NULL) {
			// RECHERCHE RECURSIVE DES FICHIERS
			// INDIQUE LE NOMBRE TOTAL DANS: VarAnalyze.RecTotalFile
			fileanalyze_recherche_recursive (Ptr);
		}
		List = g_list_next (List);
	}
	
	// SUPPRESISON DE LA COPIE DU GSLIST
	VarAnalyze.ListReceive = libutils_remove_glist (VarAnalyze.ListReceive);
	VarAnalyze.RecTotalFile *= 2;
	
	// VERIFIE LA VALIDITE DU FICHIER
	VarAnalyze.RecActivate = 0;
	fileanalyze_verif_validity_files ();
	
	// ALIMENTATION DE LA LISTE
	fileanalyze_set_in_list ();
	
	// REMOVE COPIE DU GLIST
	VarAnalyze.ListFiles = libutils_remove_glist (VarAnalyze.ListFiles);

	VarAnalyze.BoolInThread = FALSE;
	VarAnalyze.BoolSetPercent = FALSE;

	pthread_exit(0);
}
// 
// 
static gint fileanalyze_timeout (gpointer data)
{
	if (TRUE == VarAnalyze.BoolSetPercent) {
		gchar	*Str = NULL;
		
		VarAnalyze.BoolSetPercent = FALSE;
		VarAnalyze.percent = (double)VarAnalyze.RecActivate  / (double)VarAnalyze.RecTotalFile;
		Str = g_strdup_printf ("%d%%", (int)(VarAnalyze.percent*100));
		WindScan_set_progress (Str, VarAnalyze.percent);
		g_free (Str);
		Str = NULL;
	}
	else if (FALSE == VarAnalyze.BoolInThread) {
		WindScan_close ();
		g_source_remove (VarAnalyze.HandlerTimeoutDo);
		file_affiche_glist ();
		FileWav_affiche_glist ();
		FileMp3Ogg_affiche_glist ();
		FileTags_affiche_glist ();
		
		if (TRUE == VarAnalyze.BoolNoFindCheckMp3) {
			wind_info_init (
				WindMain,
				_("Package checkmp3 (mp3check) NOT FOUND !"),
				_("Please install or checkmp3 mp3check for\nconsideration of mp3 files"),
				
			  	"");
		}
	}
	return (TRUE);
}
// 
// 
void fileanalyze_add_file_to_treeview (TYPE_FILESELECTION p_TypeFileselection, GSList *p_list)
{
	gchar		*Ptr = NULL;
	GSList		*gs_List = p_list;
	pthread_t	nmr_tid_1;
	
	// PRINT_FUNC_LF();
	
	if (NULL == p_list) return;
	
	// WindScan_open ("Files scan", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Files scan",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Scan directory, verify and load ...</i></b>");
	
	// TYPE DE FICHIER EN RECHERCHE
	VarAnalyze.TypeFileselection = p_TypeFileselection;
	
	// COPIE DE LA GSlist TRANSMISE PAR LE FILESELECT ET QUI DEVRA ETRE SUPRIMEE
	while (gs_List) {
		if ((Ptr = (gchar *)gs_List->data) != NULL) {
			VarAnalyze.ListReceive = g_list_append (VarAnalyze.ListReceive, g_strdup (Ptr));
		}
		gs_List = g_slist_next (gs_List);
	}
	
	VarAnalyze.RecActivate = 0;
	VarAnalyze.RecTotalFile = 0;
	
	// CALL THREAD
	VarAnalyze.BoolInThread = TRUE;
	VarAnalyze.BoolSetPercent = FALSE;
	VarAnalyze.BoolNoFindCheckMp3 = FALSE;
	
	pthread_create (&nmr_tid_1, NULL ,(void *)fileanalyze_thread, (void *)NULL);
	VarAnalyze.HandlerTimeoutDo = g_timeout_add (100, fileanalyze_timeout, 0);
}


