 /*
 *  file      : file_trash.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */





/*
void filetrash_main_test_trash (void )
{
	GFile		*file = NULL;
	
	file = g_file_new_for_path( "/home/cat/coucou.txt" ); 
	if( FALSE == g_file_trash( file, NULL, NULL )) {
		g_print("TRASH BAD\n");
	}
	else {
		g_print("TRASH OK\n");
	}
	g_object_unref( file );
}
*/






#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <pthread.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "win_scan.h"
#include "file.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/
typedef struct {

	gboolean         BoolTrashIsOk;
	gboolean         Bool_kde_full_session;
	
	pthread_t        nmr_tid;
	guint            handler_timeout;
	gboolean         bool_thread_end;
	double           total_percent;
	
	gint             nbr_trash;
	gint             ElementActif;
	
	gboolean         BoolEtat;
	gboolean         bool_pass_conv;
	gint             pass_conv;
	
	gboolean         bool_set_end_of_user;
	
	gchar            PathNameTrash [ 1024 ];
	
} VAR_FILE_TRASH;

VAR_FILE_TRASH var_filetrash = { FALSE, FALSE };



// 
// 
gboolean filetrash_move_to_trash( gchar *p_Name )
{
	GFile		*src_file = NULL;
	gboolean	bool_ret = FALSE;
	
	if( NULL != (src_file = g_file_new_for_path( p_Name ))) {
		if( TRUE == g_file_query_exists( src_file, NULL ) ) {
			bool_ret = g_file_trash( src_file, NULL, NULL );
		}
		g_object_unref( src_file );
	}
	return( bool_ret );
}
// 
// 
static void filetrash_thread (void *arg)
{
	GList     *list = NULL;
	DETAIL    *detail = NULL;

	var_filetrash.bool_thread_end = FALSE;
	var_filetrash.bool_pass_conv = TRUE;
	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (detail->EtatTrash == FILE_TRASH_VERIF_OK) {

				var_filetrash.ElementActif ++;
				var_filetrash.BoolEtat = TRUE;

				if (filetrash_move_to_trash (detail->namefile) == TRUE) {
					detail->BoolRemove = TRUE;
				}
			}
		}
		list = g_list_next (list);
	}
	var_filetrash.bool_pass_conv = FALSE;
	var_filetrash.bool_thread_end = TRUE;
	
	pthread_exit(0);
}
// 
// 
static gint filetrash_timeout (gpointer data)
{
	if (var_filetrash.bool_pass_conv == TRUE) {

		gchar *str = NULL;
    		gchar *spinner="|/-\\";
		gchar  foo [ 2 ];
		
		foo [ 0 ] = spinner[var_filetrash.pass_conv++%4];
		foo [ 1 ] = '\0';
		
		str = g_strdup_printf ("<b>En cours: %d / %d   %s</b>",
					var_filetrash.ElementActif,
					var_filetrash.nbr_trash,
					foo
					);
		WindScan_set_label (str);
		g_free (str);
		str = NULL;
	}
	
	if (var_filetrash.BoolEtat == TRUE) {
		
		gchar	*Str = NULL;
		
		var_filetrash.total_percent = (double)((double)var_filetrash.ElementActif / (double)var_filetrash.nbr_trash);
		Str = g_strdup_printf ("%d%%", (int)(var_filetrash.total_percent * 100));
		WindScan_set_progress (Str, var_filetrash.total_percent);
		g_free (Str);
		Str = NULL;
		var_filetrash.BoolEtat = FALSE;
		return (TRUE);
	}
	
	if (var_filetrash.bool_thread_end == TRUE) {

		WindScan_close ();
		
		fileaction_set_end (TYPE_TRASH);
		g_source_remove (var_filetrash.handler_timeout);
		on_file_button_del_file_clicked (NULL, NULL);
	}
	return (TRUE);
}
// 
// 
void filetrash_action (void)
{
	// PRINT_FUNC_LF();
	
	var_filetrash.bool_thread_end = FALSE;
	var_filetrash.ElementActif     = 0;
	var_filetrash.BoolEtat        = FALSE;
	var_filetrash.bool_pass_conv   = FALSE;
	
	// WindScan_open (_("Trash"), WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		_("Trash"),
		WINDSCAN_PULSE
		);
	WindScan_set_label (_("<b><i>Trash files ...</i></b>"));
		
	pthread_create (&var_filetrash.nmr_tid, NULL ,(void *)filetrash_thread, (void *)NULL);
	var_filetrash.handler_timeout = g_timeout_add (100, filetrash_timeout, 0);
}
// 
// 
gboolean filetrash_ok (void)
{
	GList     *list = NULL;
	DETAIL    *detail = NULL;
	gboolean   BoolTrash = FALSE;
		
	var_filetrash.nbr_trash = 0;
	
	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (detail->EtatTrash != FILE_TRASH_NONE) {
				BoolTrash = TRUE;
				var_filetrash.nbr_trash ++;
			}
		}
		list = g_list_next (list);
	}
	return (BoolTrash);
}









