 /*
 *  file      : split_conv.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include "conv.h"
#include "configuser.h"
#include "win_scan.h"
#include "win_info.h"
#include "prg_init.h"
#include "get_info.h"
#include "split.h"



	/*
	
	>FORMATS A CONVERTIR VERS WAV
	WAV		VERS:		
	---------	---------	------------------------
	WAV		FLAC		FLAC_FLAC_TO_WAV
	WAV		APE		MAC_APE_TO_WAV
	WAV		WAVPACK		WVUNPACK_WAVPACK_TO_WAV
	WAV		OGG		MPLAYER_OGG_TO_WAV
	WAV		M4A		MPLAYER_M4A_TO_WAV
	WAV		MPC		MPPDEC_MPC_TO_WAV * 
	WAV		MP3		MPLAYER_MP3_TO_WAV
	WAV		*WMA		MPLAYER_WMA_TO_WAV
	WAV		*SHORTEN	SHORTEN_SHN_TO_WAV
	WAV		*RM		MPLAYER_RM_TO_WAV
	WAV		*DTS		MPLAYER_DTS_TO_WAV
	WAV		*AIF		MPLAYER_AIFF_TO_WAV
	// SAUF
	WAV		WAV
	
	SI <> 44100 ET <> 2 ET <> 16 ALORS
		CALL_SOX (...
	FIN_SI
	
	SI CLIC ON BUTTON ALORS
		WAVSPLIT_EXTRACT
	FIN_SI
	
	>CONVERTION DU SPLIT_WAV VERS LE FORMAT D ORIGINE
	DE:		VERS WAV
	---------	----------------------------
	FLAC		FLAC_WAV_TO_FLAC
	APE		MAC_WAV_TO_APE
	WAVPACK		WAVPACK_WAV_TO_WAVPACK
	OGG		OGGENC_WAV_TO_OGG
	M4A		FAAC_WAV_TO_M4A
	MPC		MPPENC_WAV_TO_MPC
	MP3		LAME_WAV_TO_MP3
	WMA		-- non
	SHORTEN		-- non
	RM		-- non
	DTS		-- non
	AIF		-- non
	WAV		repositionner HERTZ, VOIES BITS
	*/

typedef struct {
	
	gchar		*SrcPathnameFile;
	gchar		*DestPathnameFile;
	TYPE_FILE_IS	TypeFileIs;
	
} VAR_SPLITCONV;

// TypeFileIs = GetInfo_file_is (Ptr);

VAR_SPLITCONV VarSplitConv = {
				NULL,		// SrcPathnameFile
				NULL,		// DestPathnameFile
				FILE_IS_NONE	// TYPE_FILE_IS
				
				};


// 
// 
static void SplitConv_thread (void *arg)
{
	PARAM_FILELC	param_filelc;
	gint		pos;
	gboolean	BoolPassSox = FALSE;
	gchar		**PtrTabArgs = NULL;
	
printf("---------------------------- SplitConv_thread( \n" );

	conv.bool_thread_conv = TRUE;
	
	if (FILE_IS_WAV == VarSplitConv.TypeFileIs) {
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
		PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
		PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
		PtrTabArgs [ pos++ ] = g_strdup (VarSplitConv.SrcPathnameFile);
		PtrTabArgs [ pos++ ] = g_strdup ("-ao");
		PtrTabArgs [ pos++ ] = g_strdup ("pcm");
		PtrTabArgs [ pos++ ] = g_strdup ("-ao");
		PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s",SPLIT_FILE_TMP_WAV_SOX);
		PtrTabArgs [ pos++ ] = g_strdup ("-srate");
		PtrTabArgs [ pos++ ] = g_strdup ("44100");
		PtrTabArgs [ pos++ ] = NULL;
		conv_to_convert( PtrTabArgs, FALSE, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
		BoolPassSox = TRUE;
	}
	else if (FILE_IS_FLAC == VarSplitConv.TypeFileIs) {
		param_filelc.type_conv            = FLAC_FLAC_TO_WAV;
		param_filelc.With_CommandLineUser = FALSE;
		param_filelc.filesrc              = VarSplitConv.SrcPathnameFile;
		param_filelc.filedest             = SPLIT_FILE_TMP_WAV_SOX;
		param_filelc.tags                 = NULL;
		param_filelc.cdrom                = NULL;
		param_filelc.num_track            = NULL;
		param_filelc.BoolSetBitrate       = FALSE;
		param_filelc.PtrStrBitrate        = NULL;
		PtrTabArgs = filelc_get_command_line (&param_filelc);
		conv_to_convert( PtrTabArgs, FALSE, FLAC_FLAC_TO_WAV, "FLAC_FLAC_TO_WAV");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
		BoolPassSox = TRUE;
	}
	else if (FILE_IS_APE == VarSplitConv.TypeFileIs) {
		param_filelc.type_conv            = MAC_APE_TO_WAV;
		param_filelc.With_CommandLineUser = FALSE;
		param_filelc.filesrc              = VarSplitConv.SrcPathnameFile;
		param_filelc.filedest             = SPLIT_FILE_TMP_WAV_SOX;
		param_filelc.tags                 = NULL;
		param_filelc.cdrom                = NULL;
		param_filelc.num_track            = NULL;
		param_filelc.BoolSetBitrate       = FALSE;
		param_filelc.PtrStrBitrate        = NULL;
		PtrTabArgs = filelc_get_command_line (&param_filelc);
		conv_to_convert( PtrTabArgs, FALSE, MAC_APE_TO_WAV, "MAC_APE_TO_WAV");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
		BoolPassSox = TRUE;
	}
	else if (FILE_IS_WAVPACK == VarSplitConv.TypeFileIs) {
	}
	else if (FILE_IS_OGG == VarSplitConv.TypeFileIs) {
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
		PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
		PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
		PtrTabArgs [ pos++ ] = g_strdup ("-ao");
		PtrTabArgs [ pos++ ] = g_strdup ("pcm");
		PtrTabArgs [ pos++ ] = g_strdup (VarSplitConv.SrcPathnameFile);
		PtrTabArgs [ pos++ ] = g_strdup ("-ao");
		PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", SPLIT_FILE_TMP_WAV_SOX);
		PtrTabArgs [ pos++ ] = NULL;
		conv_to_convert( PtrTabArgs, FALSE, MPLAYER_OGG_TO_WAV, "MPLAYER_OGG_TO_WAV");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
		BoolPassSox = TRUE;
	}
	else if (FILE_IS_M4A == VarSplitConv.TypeFileIs) {
	}
	else if (FILE_IS_MPC == VarSplitConv.TypeFileIs) {
	}
	else if (FILE_IS_MP3 == VarSplitConv.TypeFileIs) {
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
		PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
		PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
		PtrTabArgs [ pos++ ] = g_strdup (VarSplitConv.SrcPathnameFile);
		PtrTabArgs [ pos++ ] = g_strdup ("-ao");
		PtrTabArgs [ pos++ ] = g_strdup ("pcm");
		PtrTabArgs [ pos++ ] = g_strdup ("-ao");
		PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", SPLIT_FILE_TMP_WAV_SOX);
		PtrTabArgs [ pos++ ] = NULL;
		conv_to_convert( PtrTabArgs, FALSE, MPLAYER_MP3_TO_WAV, "MPLAYER_MP3_TO_WAV");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
		BoolPassSox = TRUE;
	}
	else if (FILE_IS_WMA == VarSplitConv.TypeFileIs) {
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
		PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
		PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
		PtrTabArgs [ pos++ ] = g_strdup (VarSplitConv.SrcPathnameFile);
		PtrTabArgs [ pos++ ] = g_strdup ("-ao");
		PtrTabArgs [ pos++ ] = g_strdup ("pcm");
		PtrTabArgs [ pos++ ] = g_strdup ("-ao");
		PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", SPLIT_FILE_TMP_WAV_SOX);
		PtrTabArgs [ pos++ ] = g_strdup ("-af");
		PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
		PtrTabArgs [ pos++ ] = g_strdup ("-srate");
		PtrTabArgs [ pos++ ] = g_strdup ("44100");
		PtrTabArgs [ pos++ ] = NULL;
		conv_to_convert( PtrTabArgs, FALSE, MPLAYER_WMA_TO_WAV, "MPLAYER_WMA_TO_WAV");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
		BoolPassSox = TRUE;
	}
	else if (FILE_IS_SHN == VarSplitConv.TypeFileIs) {
	}
	else if (FILE_IS_RM == VarSplitConv.TypeFileIs) {
	}
	else if (FILE_IS_DTS == VarSplitConv.TypeFileIs) {
	}
	else if (FILE_IS_AIFF == VarSplitConv.TypeFileIs) {
	}
	
	// CONVERSION AVEC SOX
	if (TRUE == BoolPassSox) {
		PtrTabArgs = conv_with_sox_get_param (SPLIT_FILE_TMP_WAV_SOX, SPLIT_FILE_TMP_WAV, "44100", "2", "16");
		conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
	}
	
	PRINT("FIN THREAD SPLITCONV");
	conv.bool_thread_conv = FALSE;
	pthread_exit(0);
}
// 
// 
static gint SplitConv_timeout (gpointer data)
{
	if (FILE_IS_NONE != VarSplitConv.TypeFileIs && (TRUE == conv.BoolIsExtract || TRUE == conv.BoolIsConvert || TRUE == conv.BoolIsCopy || TRUE == conv.BoolIsNormalise || TRUE == conv.BoolIsReplaygain)) {
		gchar	Str [ 200 ];
		
		Str [ 0 ] = '\0';

		if (TRUE == conv.BoolIsExtract) {
			strcat (Str, "<b><i>Extraction</i></b> ");
		}
		if (TRUE == conv.BoolIsConvert) {
			strcat (Str, "<b><i>Conversion</i></b> ");
		}
		if (TRUE == conv.BoolIsCopy) {
			strcat (Str, "<b><i>Copie</i></b> ");
		}
		if (TRUE == conv.BoolIsNormalise) {
			strcat (Str, "<b><i>Normalise</i></b> ");
		}
		if (TRUE == conv.BoolIsReplaygain) {
			strcat (Str, "<b><i>Replaygain</i></b>");
		}
		
		WindScan_set_label (Str);
	}
	if (FILE_IS_NONE != VarSplitConv.TypeFileIs && TRUE == conv.bool_percent_conv) {
		
		gchar	*Str = NULL;
		
		// DEBUG
		if( conv.total_percent > 1.0 ) conv.total_percent = 1.0;
		Str = g_strdup_printf ("%d%%", (int)(conv.total_percent * 100));
		WindScan_set_progress (Str, conv.total_percent);
		g_free (Str);
		Str = NULL;
		conv.bool_percent_conv = FALSE;
	}
	else if (TRUE == conv.bool_thread_conv) {
	}
	else if (FALSE == conv.bool_thread_conv) {
printf("---------------------------- FIN SplitConv_timeout( \n" );
		PRINT("FIN TIMEOUT SPLITCONV");
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("\n");
		g_source_remove (conv.handler_timeout_conv);
		// WindScan_close ();
		while (gtk_events_pending()) gtk_main_iteration();
		if (NULL != VarSplitConv.DestPathnameFile) {
			if (libutils_get_size_file (VarSplitConv.DestPathnameFile) > 44) {
				split_file_load_continue (VarSplitConv.DestPathnameFile);
			}
			else {
				WindScan_close ();
				if( FALSE == WindScan_close_request()) {
					wind_info_init (
						WindMain,
						_("SOX Error"),
						_("The SOX program on your distribution"),
						"\n",
						_("has generated an error during conversion."),
						"");
				}
			}
		}
		else {
			if (libutils_get_size_file (VarSplitConv.SrcPathnameFile) > 44) {
				split_file_load_continue (VarSplitConv.SrcPathnameFile);
			}
			else {
				WindScan_close ();
				if( FALSE == WindScan_close_request()) {
					wind_info_init (
						WindMain,
						_("SOX Error"),
						_("The SOX program on your distribution"),
						"\n",
						_("has generated an error during conversion."),
						"");
				}
			}
		}
		g_unlink (SPLIT_FILE_TMP_WAV_SOX);		
	}
	return (TRUE);
}
// 
// 
void SplitConv_to( gchar *p_PathNameFile )
{
	gint		Channels;
	gint		Hertz;
	gint		Bits;
	pthread_t	nmr_tid;
	
printf("---------------------------- SplitConv_to( %s\n", p_PathNameFile );
 	
	VarSplitConv.TypeFileIs = GetInfo_file_is (p_PathNameFile);
	
	conv_reset_struct (WindScan_close_request);
	
	if (NULL != VarSplitConv.SrcPathnameFile) {
		g_free (VarSplitConv.SrcPathnameFile);
		VarSplitConv.SrcPathnameFile = NULL;
	}
	VarSplitConv.SrcPathnameFile = g_strdup (p_PathNameFile);
	if (NULL != VarSplitConv.DestPathnameFile) {
		g_free (VarSplitConv.DestPathnameFile);
		VarSplitConv.DestPathnameFile = NULL;
	}
	
	if (FILE_IS_WAV == VarSplitConv.TypeFileIs) {
		tagswav_file_GetBitrate (p_PathNameFile, &Channels, &Hertz, &Bits);
		PRINT_FUNC_LF();
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print ("\t%s\n", p_PathNameFile);
			g_print ("\tChannels: %d,  Hertz: %d,   Bits: %d\n", Channels, Hertz, Bits);
		}
		if (2 != Channels || 44100 != Hertz || 16 != Bits) {
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print ("\tNOT OK !!!\n\n");
			VarSplitConv.DestPathnameFile = g_strdup (SPLIT_FILE_TMP_WAV);
			conv.total_convert = 2;
		}
		else {
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print ("\tOK :-)\n\n");
			VarSplitConv.TypeFileIs = FILE_IS_NONE;
		}
	}
	else if (FILE_IS_FLAC == VarSplitConv.TypeFileIs) {
		VarSplitConv.DestPathnameFile = g_strdup (SPLIT_FILE_TMP_WAV);
		conv.total_convert = 2;
	}
	else if (FILE_IS_MP3 == VarSplitConv.TypeFileIs) {
		VarSplitConv.DestPathnameFile = g_strdup (SPLIT_FILE_TMP_WAV);
		conv.total_convert = 2;
	}
	else if (FILE_IS_OGG == VarSplitConv.TypeFileIs) {
		VarSplitConv.DestPathnameFile = g_strdup (SPLIT_FILE_TMP_WAV);
		conv.total_convert = 2;
	}
	else if (FILE_IS_APE == VarSplitConv.TypeFileIs) {
		VarSplitConv.DestPathnameFile = g_strdup (SPLIT_FILE_TMP_WAV);
		conv.total_convert = 2;
	}
	else if (FILE_IS_WMA == VarSplitConv.TypeFileIs) {
		VarSplitConv.DestPathnameFile = g_strdup (SPLIT_FILE_TMP_WAV);
		conv.total_convert = 2;
	}
	if (FILE_IS_NONE != VarSplitConv.TypeFileIs) {
		// WindScan_open ("SOX files", WINDSCAN_PULSE);
		wind_scan_init(
			WindMain,
			"SOX files",
			WINDSCAN_PULSE
			);
		WindScan_set_label ("<b><i>SOX files ...</i></b>");
	}
	else {
		// WindScan_open ("Analyse du fichier", WINDSCAN_PULSE);
		wind_scan_init(
			WindMain,
			"Analyse du fichier",
			WINDSCAN_PULSE
			);
		WindScan_set_label ("<b><i>Analyse du fichier ...</i></b>");
	}

	conv.bool_thread_conv = TRUE;
	PRINT("DEBUT TIMEOUT SPLITCONV");
	PRINT("DEBUT THREAD SPLITCONV");
 	pthread_create (&nmr_tid, NULL ,(void *)SplitConv_thread, (void *)NULL);
	conv.handler_timeout_conv = g_timeout_add (20, SplitConv_timeout, 0);
}






















