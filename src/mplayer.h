 /*
 *  file      : mplayer.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef mplayer_h
#define mplayer_h 1

typedef enum {
	LIST_MPLAYER_FROM_NONE = 0,
	LIST_MPLAYER_FROM_DVD,
	LIST_MPLAYER_FROM_CD
} LIST_MPLAYER_FROM;

#define	MAX_PLAYERS_ARGS	100

typedef struct {
	gdouble			PercentTempsActuel;		// Temps actuel
	gdouble			DOUBLE_TempsTotal;		// Temps Total
	gdouble		 	DOUBLE_TempsTotalSection;
	gboolean		BoolIsPause;			// TRUE = PAUSE | FALSE = NO PAUSE
	LIST_MPLAYER_FROM	ListPlayFrom;			// From dvd, cd, fichier
	gchar			*PlayerArgs[ MAX_PLAYERS_ARGS ];// Tableau de pointeurs pour les parametres
	gboolean		BoolThreadActivate;		// TRUE is activate else FALSE
	pthread_t		NmrTid;				// Numero de process du thread
	gboolean		BoolTimeoutActivate;		// TRUE is activate else FALSE
	guint			HandlerTimeout;			// Numero de process du timeout
	gint			SignalNumchildren;		// Synchronisation
	gint			Tube [ 2 ];			// Les tickets
	pid_t			CodeFork;			// Numero de process pour mplayer
	gboolean		BoolErreurMplayer;		// TRUE is erreur else FALSE
	gint			Button;				// -1, GDK_BUTTON_PRESS, GDK_BUTTON_RELEASE
	void			(*FuncWinClose) (void);		// 
	void			(*FuncSetValueTime) (gdouble p_value);
	void			(*FuncIconeStop) (void);	// 
	gboolean		(*Func_request_stop) (void);	// 
} VAR_MPLAYER;

extern VAR_MPLAYER VarMplayer;

void		mplayer_init (LIST_MPLAYER_FROM p_ListPlayFrom, gdouble p_TempsTotalSection, guint p_TempsTotalSurface, void *p_FuncWinClose, void *p_FuncSetValueTime, void *p_FuncIconeStop, void *p_Func_request_stop);
void		mplayer_set_list (GList *p_list);
void		mplayer_fifo_quit (void);
void		mplayer_remove_fifo (void);
void		mplayer_fifo_seek_with_hundr (gdouble PercentValue);
void		mplayer_fifo_seek (gdouble PercentValue);
void		mplayer_fifo_pause (void);
void		mplayer_set_value_time (gdouble value);
void		mplayer_remove_list_args (void);
gboolean	mplayer_is_used (void);


#endif

