 /*
 *  file      : cd_audio_extract.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Extraction avec cdparanoia en mode expert.
 * XCFA essayes de reprendre quelques fonctionnalités interessantes de 'rubyripper' [ http://code.google.com/p/rubyripper/ ]
 * Deux extractions de la même piste sont faites puis comparées.
 * Les segments non valides seront relus jusqu'à quatre fois si nécessaire puis validés si possible.
 *
	--------------------------------------------------------------------
	VERIF FICHIER cdparanoia mode expert
	--------------------------------------------------------------------
	44        = wav container overhead
	2352      = size for a audiocd sector as used in cdparanoia
	44 + 2352 = 2396
	filesize  = 44 bytes wav overhead + ( 2352 bytes per sector * len sector)
*/



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "options.h"
#include "scan.h"
#include "cd_audio.h"
#include "conv.h"
#include "file.h"
#include "configuser.h"
#include "cd_normalise.h"
#include "prg_init.h"
#include "file.h"
#include "win_info.h"
#include "cd_cue.h"
#include "notify_send.h"
#include "win_scan.h"
#include "win_vte.h"
#include "cd_audio_extract.h"




/*
*---------------------------------------------------------------------------
* DEFINITION EXTERNE
*---------------------------------------------------------------------------
*/
extern char *get_current_dir_name (void);


/*
*---------------------------------------------------------------------------
* DEFINITION
*---------------------------------------------------------------------------
*/
typedef struct {
	gboolean	 BoolFormatUnique;
	gchar		*NameFileFormatUnique;
	GList		*ListFileFormatUnique;
	gint		 NbrFileUnique;
	gint		 PassFileUnique;
	gint		 ChoiceFileCue;

	gchar		*PathName_SrcWavSox;
	gchar		*PathName_DestFlacSox;
	gchar		*PathName_DestOggSox;
	gchar		*PathName_DestMpcSox;
	
	gboolean	 BoolListIsPeakAlbum;
	gint		 ComboNumActive;
	gint		 NbrList;
	gchar		*PtrDirActuel;
	
	gchar		*PathDestinationModeExpert;
	
} VAR_CDEXTRACT;

VAR_CDEXTRACT VarCdExtract;





/*
*---------------------------------------------------------------------------
* THREAD EXTRACTION
*---------------------------------------------------------------------------
*/
GList *cdaudioextract_remove_glist (GList *New)
{
	GList *list = g_list_first (New);
	gchar *ptr = NULL;

	while (list) {
		if (NULL != (ptr = (gchar*)list->data)) {
			g_free (ptr);
			ptr = NULL;
			list->data = NULL;
		}
		list = g_list_next(list);
	}
	g_list_free (New);
	New = NULL;
	return ((GList *)NULL);
}
// 
// 
void cdaudioextract_remove_list (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;
	gint		 Num;
	gint		 Pos;
	
	// PRINT_FUNC_LF();
	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if (NULL != (Audio = List->data)) {
			if (NULL != Audio->ListConv) {
				for (Num = NUM_WAV; Num < NUM_TOTAL; Num ++) {
					for (Pos = 0; NULL != Audio->ListConv[ Num ].Arg [ Pos ]; Pos ++) {
						g_free (Audio->ListConv[ Num ].Arg [ Pos ]);
						Audio->ListConv[ Num ].Arg [ Pos ] = NULL;
					}
				}
				g_free (Audio->ListConv);
				Audio->ListConv = NULL;
			}
		}
		List = g_list_next(List);
	}
}
// SAUVEGARDE DES POINTEURS
// 
void cdaudioextract_save_arg_from_conv( gchar **p_PtrTabArgs, CD_AUDIO *Audio, gint p_Num)
{
	gint	Pos;

	for (Pos = 0; NULL != p_PtrTabArgs [ Pos ]; Pos ++) {
		Audio->ListConv[ p_Num ].Arg [ Pos ] = p_PtrTabArgs [ Pos ];
		p_PtrTabArgs [ Pos ] = NULL;
	}
	Audio->ListConv[ p_Num ].Arg [ Pos ] = NULL;
}
// 
// 
void cdaudioextract_save_arg_from_extract( CD_AUDIO *Audio, gint p_Num)
{
	gint	Pos;

	for (Pos = 0; NULL != conv.ArgExtract [ Pos ]; Pos ++) {
		Audio->ListConv[ p_Num ].Arg [ Pos ] = conv.ArgExtract [ Pos ];
		conv.ArgExtract [ Pos ] = NULL;
	}
	Audio->ListConv[ p_Num ].Arg [ Pos ] = NULL;
}
// RESTITUTION DES POINTEURS
// 
void cdaudioextract_restitue_arg_to_conv( gchar **p_PtrTabArgs, CD_AUDIO *Audio, gint p_Num )
{
	gint	Pos;

	for (Pos = 0; NULL != Audio->ListConv[ p_Num ].Arg [ Pos ]; Pos ++) {
		p_PtrTabArgs [ Pos ] = Audio->ListConv[ p_Num ].Arg [ Pos ];
		Audio->ListConv[ p_Num ].Arg [ Pos ] = NULL;
	}
	p_PtrTabArgs [ Pos ] = NULL;
}
// 
// 
void cdaudioextract_restitue_arg_to_extract (CD_AUDIO *Audio, gint p_Num)
{
	gint	Pos;
	
	for (Pos = 0; NULL != Audio->ListConv[ p_Num ].Arg [ Pos ]; Pos ++) {
		conv.ArgExtract [ Pos ] = Audio->ListConv[ p_Num ].Arg [ Pos ];
		Audio->ListConv[ p_Num ].Arg [ Pos ] = NULL;
	}
	conv.ArgExtract [ Pos ] = NULL;
}
// 
// 
void cdaudioextract_create_list_is_Convert (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;
	PARAM_FILELC	 param_filelc;
	gchar		**PtrTabArgs = NULL;
	
	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if (NULL != (Audio = List->data)) {
			if (Audio->Etat_Sox == ETAT_CONV_CD_WAITING) {
				if (NULL != VarCdExtract.NameFileFormatUnique) {
					
					gchar *Ptr = NULL;
					
					if (NULL == VarCdExtract.PathName_SrcWavSox) {
						VarCdExtract.PathName_SrcWavSox = g_strdup (VarCdExtract.NameFileFormatUnique);
					}
					if (NULL == VarCdExtract.PathName_DestFlacSox) {
						VarCdExtract.PathName_DestFlacSox = g_strdup_printf ("%s  ", VarCdExtract.NameFileFormatUnique);
						Ptr = strrchr (VarCdExtract.PathName_DestFlacSox, '.');
						Ptr ++;
						*Ptr ++ = 'f';
						*Ptr ++ = 'l';
						*Ptr ++ = 'a';
						*Ptr ++ = 'c';
						*Ptr    = '\0';
					}
					if (NULL == VarCdExtract.PathName_DestOggSox) {
						VarCdExtract.PathName_DestOggSox = g_strdup (VarCdExtract.NameFileFormatUnique);
						Ptr = strrchr (VarCdExtract.PathName_DestOggSox, '.');
						Ptr ++;
						*Ptr ++ = 'o';
						*Ptr ++ = 'g';
						*Ptr ++ = 'g';
						*Ptr    = '\0';
					}
					if (NULL == VarCdExtract.PathName_DestMpcSox) {
						VarCdExtract.PathName_DestMpcSox = g_strdup (VarCdExtract.NameFileFormatUnique);
						Ptr = strrchr (VarCdExtract.PathName_DestMpcSox, '.');
						Ptr ++;
						*Ptr ++ = 'm';
						*Ptr ++ = 'p';
						*Ptr ++ = 'c';
						*Ptr    = '\0';
					}
				}
			}
			
			if (Audio->Etat_Sox == ETAT_CONV_CD_WAITING || Audio->Etat_Flac == ETAT_CONV_CD_WAITING) {
				param_filelc.type_conv             = FLAC_WAV_TO_FLAC;
				param_filelc.With_CommandLineUser  = Audio->EtatSelection_Flac >= CD_ETAT_SELECT_EXPERT;
				
				if (FALSE == VarCdExtract.BoolFormatUnique) {
					param_filelc.filesrc       = Audio->PathName_Dest_Wav;
					param_filelc.filedest      = Audio->PathName_Dest_Flac;
				}
				else {
					param_filelc.filesrc       = VarCdExtract.PathName_SrcWavSox;
					param_filelc.filedest      = VarCdExtract.PathName_DestFlacSox;
				}
				param_filelc.tags                  = Audio->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = NULL;
				
				Audio->ListConv[ NUM_FLAC ].WithCommandLineUser = param_filelc.With_CommandLineUser;
				PtrTabArgs = filelc_get_command_line (&param_filelc);
				cdaudioextract_save_arg_from_conv( PtrTabArgs, Audio, NUM_FLAC );
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			}
			if (Audio->Etat_Ape == ETAT_CONV_CD_WAITING) {
				param_filelc.type_conv             = MAC_WAV_TO_APE;
				param_filelc.With_CommandLineUser  = Audio->EtatSelection_Ape >= CD_ETAT_SELECT_EXPERT;
				param_filelc.filesrc               = Audio->PathName_Dest_Wav;
				param_filelc.filedest              = Audio->PathName_Dest_Ape;
				param_filelc.tags                  = Audio->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = NULL;
				
				Audio->ListConv[ NUM_APE ].WithCommandLineUser = param_filelc.With_CommandLineUser;
				PtrTabArgs = filelc_get_command_line (&param_filelc);
				cdaudioextract_save_arg_from_conv (PtrTabArgs, Audio, NUM_APE);
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			}
			if (Audio->Etat_WavPack == ETAT_CONV_CD_WAITING) {
				param_filelc.type_conv             = WAVPACK_WAV_TO_WAVPACK;
				param_filelc.With_CommandLineUser  = Audio->EtatSelection_WavPack >= CD_ETAT_SELECT_EXPERT;
				param_filelc.filesrc               = Audio->PathName_Dest_Wav;
				param_filelc.filedest              = Audio->PathName_Dest_WavPack;
				param_filelc.tags                  = Audio->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = NULL;

				Audio->ListConv[ NUM_WAVPACK ].WithCommandLineUser = param_filelc.With_CommandLineUser;
				PtrTabArgs = filelc_get_command_line (&param_filelc);
				cdaudioextract_save_arg_from_conv (PtrTabArgs, Audio, NUM_WAVPACK);
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			}
			if (Audio->Etat_Sox == ETAT_CONV_CD_WAITING || Audio->Etat_Ogg == ETAT_CONV_CD_WAITING) {
				param_filelc.type_conv             = OGGENC_WAV_TO_OGG;
				param_filelc.With_CommandLineUser  = Audio->EtatSelection_Ogg >= CD_ETAT_SELECT_EXPERT;
				
				if (FALSE == VarCdExtract.BoolFormatUnique) {
					param_filelc.filesrc       = Audio->PathName_Dest_Wav;
					param_filelc.filedest      = Audio->PathName_Dest_Ogg;
				}
				else {
					param_filelc.filesrc       = VarCdExtract.PathName_SrcWavSox;
					param_filelc.filedest      = VarCdExtract.PathName_DestOggSox;
				}
				param_filelc.tags                  = Audio->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = NULL;
				param_filelc.PtrStrBitrate         = options_get_params (OGGENC_WAV_TO_OGG);

				Audio->ListConv[ NUM_OGG ].WithCommandLineUser = param_filelc.With_CommandLineUser;
				PtrTabArgs = filelc_get_command_line (&param_filelc);
				cdaudioextract_save_arg_from_conv (PtrTabArgs, Audio, NUM_OGG);
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			}
			if (Audio->Etat_m4a == ETAT_CONV_CD_WAITING) {
				param_filelc.type_conv             = FAAC_WAV_TO_M4A;
				param_filelc.With_CommandLineUser  = Audio->EtatSelection_M4a >= CD_ETAT_SELECT_EXPERT;
				param_filelc.filesrc               = Audio->PathName_Dest_Wav;
				param_filelc.filedest              = Audio->PathName_Dest_M4a;
				param_filelc.tags                  = Audio->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = NULL;

				Audio->ListConv[ NUM_M4A ].WithCommandLineUser = param_filelc.With_CommandLineUser;
				PtrTabArgs = filelc_get_command_line (&param_filelc);
				cdaudioextract_save_arg_from_conv (PtrTabArgs, Audio, NUM_M4A);
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			}
			if (Audio->Etat_Aac == ETAT_CONV_CD_WAITING) {
				param_filelc.type_conv             = AACPLUSENC_WAV_TO_AAC;
				param_filelc.With_CommandLineUser  = Audio->EtatSelection_Aac >= CD_ETAT_SELECT_EXPERT;
				param_filelc.filesrc               = Audio->PathName_Dest_Wav;
				param_filelc.filedest              = Audio->PathName_Dest_Aac;
				param_filelc.tags                  = Audio->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = NULL;
				param_filelc.PtrStrBitrate         = options_get_params (AACPLUSENC_WAV_TO_AAC);

				Audio->ListConv[ NUM_AAC ].WithCommandLineUser = param_filelc.With_CommandLineUser;
				PtrTabArgs = filelc_get_command_line (&param_filelc);
				cdaudioextract_save_arg_from_conv (PtrTabArgs, Audio, NUM_AAC);
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			}
			if (Audio->Etat_Sox == ETAT_CONV_CD_WAITING || Audio->Etat_Mpc == ETAT_CONV_CD_WAITING) {
				param_filelc.type_conv             = MPPENC_WAV_TO_MPC;
				param_filelc.With_CommandLineUser  = Audio->EtatSelection_Mpc >= CD_ETAT_SELECT_EXPERT;
				
				if (FALSE == VarCdExtract.BoolFormatUnique) {
					param_filelc.filesrc       = Audio->PathName_Dest_Wav;
					param_filelc.filedest      = Audio->PathName_Dest_Mpc;
				}
				else {
					param_filelc.filesrc       = VarCdExtract.PathName_SrcWavSox;
					param_filelc.filedest      = VarCdExtract.PathName_DestMpcSox;
				}
				param_filelc.tags                  = Audio->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = NULL;

				Audio->ListConv[ NUM_MPC ].WithCommandLineUser = param_filelc.With_CommandLineUser;
				PtrTabArgs = filelc_get_command_line (&param_filelc);
				cdaudioextract_save_arg_from_conv (PtrTabArgs, Audio, NUM_MPC);
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			}
			if (Audio->Etat_Mp3 == ETAT_CONV_CD_WAITING) {
				param_filelc.type_conv             = LAME_WAV_TO_MP3;
				param_filelc.With_CommandLineUser  = Audio->EtatSelection_Mp3 >= CD_ETAT_SELECT_EXPERT;
				param_filelc.filesrc               = Audio->PathName_Dest_Wav;
				param_filelc.filedest              = Audio->PathName_Dest_Mp3;
				param_filelc.tags                  = Audio->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.BoolSetBitrate        = FALSE;
				param_filelc.PtrStrBitrate         = NULL;
				param_filelc.PtrStrBitrate         = options_get_params (LAME_WAV_TO_MP3);

				Audio->ListConv[ NUM_MP3 ].WithCommandLineUser = param_filelc.With_CommandLineUser;
				PtrTabArgs = filelc_get_command_line (&param_filelc);
				cdaudioextract_save_arg_from_conv (PtrTabArgs, Audio, NUM_MP3);
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			}
		}
		List = g_list_next(List);
	}
}
// 
// 
size_t cdaudioextract_get_size (gint NumTrack)
{
	return (
		NumTrack == 0
		? (BaseIoctl.Datas[ NumTrack ].length + BaseIoctl.Datas[ NumTrack ].begin) -1
		: BaseIoctl.Datas[ NumTrack ].length -1
	);
}
// 
// 
gboolean cdaudioextract_create_list_is_Extract (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;
	PARAM_FILELC	 param_filelc;
	gboolean	 BoolIsPeakAlbum = FALSE;

	VarCdExtract.NbrFileUnique = 0;
	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if (NULL != (Audio = List->data)) {
			if (Audio->Etat_Wav == ETAT_CONV_CD_WAITING) {
				
				Audio->ListConv = (LIST_CONV *)g_malloc0 (sizeof(LIST_CONV) * (NUM_TOTAL + 2));
				
				if (TRUE == VarCdExtract.BoolFormatUnique) {
					
					Audio->Bool_Delette_Wav = TRUE;
					VarCdExtract.NbrFileUnique ++;
					VarCdExtract.ListFileFormatUnique = g_list_append (VarCdExtract.ListFileFormatUnique, g_strdup (Audio->PathName_Dest_Wav));
				}
				/*
				   1. length   12923 [02:52:23]  begin      32 [00:00:32]	cdparanoia [.0]-[.12954] -d /dev/hdb -O 0 "1.wav"
				   2. length   14175 [03:09:00]  begin   12955 [02:52:55]	cdparanoia [.12955]-[.14174] -d /dev/hdb -O 0 "2.wav"
				   3. length   14627 [03:15:02]  begin   27130 [06:01:55]	cdparanoia [.27130]-[.14626] -d /dev/hdb -O 0 "3.wav"
				   4. length   14823 [03:17:48]  begin   41757 [09:16:57]	cdparanoia [.41757]-[.14822] -d /dev/hdb -O 0 "4.wav"
				   5. length   13282 [02:57:07]  begin   56580 [12:34:30]	cdparanoia [.56580]-[.13281] -d /dev/hdb -O 0 "5.wav"
				   6. length   14290 [03:10:40]  begin   69862 [15:31:37]	cdparanoia [.69862]-[.14289] -d /dev/hdb -O 0 "6.wav"
				   7. length   13048 [02:53:73]  begin   84152 [18:42:02]	cdparanoia [.84152]-[.13047] -d /dev/hdb -O 0 "7.wav"
				   8. length   13170 [02:55:45]  begin   97200 [21:36:00]	cdparanoia [.97200]-[.13169] -d /dev/hdb -O 0 "8.wav"
				   9. length   13707 [03:02:57]  begin  110370 [24:31:45]	cdparanoia [.110370]-[.13706] -d /dev/hdb -O 0 "9.wav"
				  10. length   12458 [02:46:08]  begin  124077 [27:34:27]	cdparanoia [.124077]-[.12457] -d /dev/hdb -O 0 "10.wav"
				  11. length   14235 [03:09:60]  begin  136535 [30:20:35]	cdparanoia [.136535]-[.14234] -d /dev/hdb -O 0 "11.wav"
				  12. length   14857 [03:18:07]  begin  150770 [33:30:20]	cdparanoia [.150770]-[.14856] -d /dev/hdb -O 0 "12.wav"
				  13. length   14723 [03:16:23]  begin  165627 [36:48:27]	cdparanoia [.165627]-[.14722] -d /dev/hdb -O 0 "13.wav"
				  14. length   14657 [03:15:32]  begin  180350 [40:04:50]	cdparanoia [.180350]-[.14656] -d /dev/hdb -O 0 "14.wav"
				  15. length   13133 [02:55:08]  begin  195007 [43:20:07]	cdparanoia [.195007]-[.13132] -d /dev/hdb -O 0 "15.wav"
				  16. length   13520 [03:00:20]  begin  208140 [46:15:15]	cdparanoia [.208140]-[.13519] -d /dev/hdb -O 0 "16.wav"
				  17. length   13185 [02:55:60]  begin  221660 [49:15:35]	cdparanoia [.221660]-[.13184] -d /dev/hdb -O 0 "17.wav"
				  18. length   11895 [02:38:45]  begin  234845 [52:11:20]	cdparanoia [.234845]-[.11894] -d /dev/hdb -O 0 "18.wav"
				  19. length   11282 [02:30:32]  begin  246740 [54:49:65]	cdparanoia [.246740]-[.11281] -d /dev/hdb -O 0 "19.wav"
				  20. length   10973 [02:26:23]  begin  258022 [57:20:22]	cdparanoia [.258022]-[.10972] -d /dev/hdb -O 0 "20.wav"
				  21. length   13007 [02:53:32]  begin  268995 [59:46:45]	cdparanoia [.268995]-[.13006] -d /dev/hdb -O 0 "21.wav"
				  22. length   16310 [03:37:35]  begin  282002 [62:40:02]	cdparanoia [.282002]-[.16309] -d /dev/hdb -O 0 "22.wav"
				  23. length   12188 [02:42:38]  begin  298312 [66:17:37]	cdparanoia [.298312]-[.12187] -d /dev/hdb -O 0 "23.wav"
				  24. length   13470 [02:59:45]  begin  310500 [69:00:00]	cdparanoia [.310500]-[.13469] -d /dev/hdb -O 0 "24.wav"
				TOTAL  323938 [71:59:13]  
				*/
				switch (Config.ExtractCdWith) {
				case EXTRACT_WITH_CDPARANOIA :
				case EXTRACT_WITH_CDPARANOIA_MODE_2 :
					// 
					// SUITE MAIL DE: Levis Florian:
					// TEST EXTRACTION AVEC CDPARANOIA EN MODE EXPERT UNE SEULE PASSE ...
					// 
					if( EXTRACT_WITH_CDPARANOIA == Config.ExtractCdWith )
						param_filelc.type_conv             = CDPARANOIA_CD_TO_WAV;
					else	param_filelc.type_conv             = CDPARANOIA_CD_TO_WAV_EXPERT;
					param_filelc.With_CommandLineUser  = FALSE;
					param_filelc.filesrc               = Audio->PathName_Dest_Wav;
					// param_filelc.filedest              = Audio->PathName_Dest_Wav;
					param_filelc.filedest              = Audio->PathName_Dest_Tmp;
					param_filelc.tags                  = NULL;
					param_filelc.cdrom                 = EnteteCD.NameCD_Device;
					param_filelc.num_track             = Audio->Str_Track;
					Audio->ListConv[ NUM_WAV ].Size                = cdaudioextract_get_size (Audio->Num_Track -1);
					Audio->ListConv[ NUM_WAV ].WithCommandLineUser = param_filelc.With_CommandLineUser;
					filelc_get_command_line_extract (&param_filelc);
					cdaudioextract_save_arg_from_extract (Audio, NUM_WAV);
					break;
				case EXTRACT_WITH_CDPARANOIA_EXPERT :
					
					/* NUM_WAV_EXPERT_1
					*/
					param_filelc.type_conv             = CDPARANOIA_CD_TO_WAV_EXPERT;
					param_filelc.With_CommandLineUser  = FALSE;
					param_filelc.filesrc               = Audio->PathName_Dest_Wav;
					param_filelc.filedest              = Audio->ExtractExpert.Name1;
					param_filelc.tags                  = NULL;
					param_filelc.cdrom                 = EnteteCD.NameCD_Device;
					param_filelc.num_track             = Audio->Str_Track;
					Audio->ListConv[ NUM_WAV_EXPERT_1 ].Size                = cdaudioextract_get_size (Audio->Num_Track -1);
					Audio->ListConv[ NUM_WAV_EXPERT_1 ].WithCommandLineUser = param_filelc.With_CommandLineUser;
					filelc_get_command_line_extract (&param_filelc);
					cdaudioextract_save_arg_from_extract (Audio, NUM_WAV_EXPERT_1);
					
					/* NUM_WAV_EXPERT_2
					*/
					param_filelc.type_conv             = CDPARANOIA_CD_TO_WAV_EXPERT;
					param_filelc.With_CommandLineUser  = FALSE;
					param_filelc.filesrc               = Audio->PathName_Dest_Wav;
					param_filelc.filedest              = Audio->ExtractExpert.Name2;
					param_filelc.tags                  = NULL;
					param_filelc.cdrom                 = EnteteCD.NameCD_Device;
					param_filelc.num_track             = Audio->Str_Track;
					Audio->ListConv[ NUM_WAV_EXPERT_2 ].Size                = cdaudioextract_get_size (Audio->Num_Track -1);
					Audio->ListConv[ NUM_WAV_EXPERT_2 ].WithCommandLineUser = param_filelc.With_CommandLineUser;
					filelc_get_command_line_extract (&param_filelc);
					cdaudioextract_save_arg_from_extract (Audio, NUM_WAV_EXPERT_2);
					break;
				
				case EXTRACT_WITH_CDDA2WAV :
					param_filelc.type_conv             = CDDA2WAV_CD_TO_WAV;
					param_filelc.With_CommandLineUser  = Audio->EtatSelection_Wav >= CD_ETAT_SELECT_EXPERT;
					param_filelc.filesrc               = NULL;
					param_filelc.filedest              = NULL;
					param_filelc.tags                  = NULL;
					param_filelc.cdrom                 = EnteteCD.NameCD_Device;
					param_filelc.num_track             = Audio->Str_Track;
					Audio->ListConv[ NUM_WAV ].Size                = cdaudioextract_get_size (Audio->Num_Track -1);
					Audio->ListConv[ NUM_WAV ].WithCommandLineUser = param_filelc.With_CommandLineUser;
					filelc_get_command_line_extract (&param_filelc);
					cdaudioextract_save_arg_from_extract (Audio, NUM_WAV);
					conv.total_convert ++;
					break;
				}
								
				if (TRUE == Audio->EtatNormalise) {
					switch (VarCdExtract.ComboNumActive) {
					case 0 :
						PRINT("...ADD TO LIST PEAK/ALBUM");
						CdNormalise_set_list_PeakGroup (Audio);
						conv.total_convert ++;
						conv.total_convert ++;
						conv.total_convert ++;
						conv.total_convert ++;
						Audio->EtatPeak = CD_NORM_PEAK;
						BoolIsPeakAlbum = TRUE;
						break;
					case 1 :
						Audio->EtatPeak = CD_NORM_PEAK;
						conv.total_convert ++;
						conv.total_convert ++;
						conv.total_convert ++;
						break;
					}
				}
			}
			else {
				Audio->EtatPeak = CD_NORM_PEAK_NONE;
			}
		}
		List = g_list_next(List);
	}
	return (BoolIsPeakAlbum);
}
// 
// 
gchar *cdaudioextract_new_name (gchar *Name, gchar *New)
{
	gchar *NewName = g_strdup (Name);
	gchar *Ptr = NULL;
	
	if (NULL != (Ptr = strrchr (NewName, '/'))) {
		Ptr ++;
		strcpy (Ptr, New);
	}
	return ((gchar *)NewName);
}
// 
// 
void cdaudioextract_set_list_to_vte (gchar *p_text)
{
	conv.ListPutTextview = g_list_append (conv.ListPutTextview, g_strdup (p_text));
	conv.BoolPutTextview = TRUE;
	// g_print("\n****\n%s****\n", p_text);
}
// 
// 
void cdaudioextract_copy_src_to_dest (gchar *FileExtract_1, gchar *FileExtract_2, gchar *PathNameDest, size_t LenFile)
{
	FILE		*fp1 = NULL;
	FILE		*fp2 = NULL;
	FILE		*fp3 = NULL;
	FILE		*fpElem = NULL;
	gchar		*Ptr1 = NULL;
	gchar		*Ptr2 = NULL;
	gchar		 Buf1 [ 2500 ];
	gchar		 Buf2 [ 2500 ];
	gint		 Elem;
	gint		 Cpt;
	gint		 TotalElem = LenFile;
	gboolean	 BoolDiff = FALSE;
	gchar		*Name1 = NULL;
	gchar		*Name2 = NULL;
	gchar		*Str = NULL;
	gint		 NbrPass;
		
	// Ouverture en lecture des 2 fichiers
	fp1 = fopen (FileExtract_1, "r");
	fp2 = fopen (FileExtract_2, "r");
	// Ouverture de la destination
	fp3 = fopen (PathNameDest, "w");
	
	// Lecture des entetes
	fread (Buf1, 1, 44, fp1);
	fread (Buf2, 1, 44, fp2);
	// Ecriture de l entete du fichier de destination
	fwrite (Buf1, sizeof(gchar), 44, fp3);
	
	// Analyse de tous les segments
	for (Elem = 0; Elem <= TotalElem && !conv.bool_stop; Elem++) {
		
		// Nettoyer les tampons
		memset (Buf1, '\0', 2498);
		memset (Buf2, '\0', 2498);
		
		// Lectures des segments
		fread (Buf1, 1, 2352, fp1);
		fread (Buf2, 1, 2352, fp2);
		
		/* -------------------------------------------------------------
		*  DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
		*  -------------------------------------------------------------
		*
		if (Elem == 0 || Elem == 10 || Elem == 15 || Elem == 21) {
			g_print("-----------------------------------------------------------\n");
			g_print(" ELEM = %d  --> TEST\n", Elem);
			g_print("-----------------------------------------------------------\n");
			Buf1 [ 100 ] = '\0';
			Buf2 [ 100 ] = ' ';
		}
		*/
		
		NbrPass = 1;
		while (TRUE) {						
			// Compare les segments
			BoolDiff = FALSE;
			for (Ptr1 = Buf1, Ptr2 = Buf2, Cpt = 0; Cpt < 2352; Cpt ++, Ptr1 ++, Ptr2 ++) {
				if (*Ptr1 != *Ptr2) {
					if (NbrPass <= 4) {
						// Si le contenu des segments est different
						Str = g_strdup_printf (_("[Error on segment #%d] %d) Playing PASS 1/2\n"), Elem, NbrPass);
						cdaudioextract_set_list_to_vte (Str);
						g_free (Str);
						Str = NULL;
						BoolDiff = TRUE;
					}
					else { // NbrPass > 4
						Str = g_strdup_printf (_("--> Segment %d already read two %d times!\n    Abandon  :/\n"), NbrPass, Elem);					
						cdaudioextract_set_list_to_vte (Str);
						g_free (Str);
						Str = NULL;
					}
					break;
				}
			}
			if (FALSE == BoolDiff) break;
			
			// Extraction du segment invalide pour les tampons: Buf1 et Buf2
			// -------------------------------------------------------------
			// 
						
			// Extraction segment 'Elem' PASS 1/2
			Name1 = cdaudioextract_new_name (FileExtract_1, "1.wav");
			filelc_get_command_line_extract_elem (Elem, EnteteCD.NameCD_Device, Name1);
			conv_exec_extract (FALSE, CDPARANOIA_CD_TO_WAV_EXPERT_SEGMENT, "CDPARANOIA_CD_TO_WAV_EXPERT_SEGMENT");
			
			// Extraction segment 'Elem' PASS 2/2
			Str = g_strdup_printf (_("%d) Read PASS 2/2\n"), NbrPass);
			cdaudioextract_set_list_to_vte (Str);
			g_free (Str);
			Str = NULL;
			Name2 = cdaudioextract_new_name (FileExtract_2, "2.wav");

			filelc_get_command_line_extract_elem (Elem, EnteteCD.NameCD_Device, Name2);
			conv_exec_extract (FALSE, CDPARANOIA_CD_TO_WAV_EXPERT_SEGMENT, "CDPARANOIA_CD_TO_WAV_EXPERT_SEGMENT");

			// Lecture segment 'Elem' pour Buf1
			fpElem = fopen (Name1, "r");
			fread (Buf1, 1, 44, fpElem);
			memset (Buf1, '\0', 2498);
			fread (Buf1, 1, 2352, fpElem);
			fclose (fpElem);
			g_free (Name1);
			Name1 = NULL;
			
			// Lecture segment 'Elem' pour Buf2
			fpElem = fopen (Name2, "r");
			fread (Buf2, 1, 44, fpElem);
			memset (Buf2, '\0', 2498);
			fread (Buf2, 1, 2352, fpElem);
			fclose (fpElem);			
			g_free (Name2);
			Name2 = NULL;
			
			NbrPass ++;
		}
		
		// Sauvergarde du segment valide dans le fichier de destination
		fwrite (Buf1, sizeof(gchar), 2352, fp3);
	}
	fclose (fp1);
	fclose (fp2);
	fclose (fp3);
}
// 
// 
static void cdaudioextract_thread_extraction_from_cd (void *arg)
{
	GList         *ListOne = NULL;
	CD_AUDIO      *Audio = NULL;
	gint           pos;

	conv.bool_thread_extract  = TRUE;
	
	PRINT("DEBUT THREAD EXTRACTION");
	
	ListOne = g_list_first (EnteteCD.GList_Audio_cd);
	while (FALSE == conv.bool_stop && NULL != ListOne) {

		if (NULL != (Audio = (CD_AUDIO *)ListOne->data)) {

			if (FALSE == conv.bool_stop && Audio->Etat_Wav == ETAT_CONV_CD_WAITING) {

				if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
					g_print ("\n");
					g_print ("SOURCE CD<%s>\n", Audio->Str_Track);
					g_print ("DEST   WAV<%s>\n", Audio->PathName_Dest_Wav);
				}
				
				switch (Config.ExtractCdWith) {
				case EXTRACT_WITH_CDPARANOIA :
				case EXTRACT_WITH_CDPARANOIA_MODE_2 :
					if (FALSE == conv.bool_stop) {
					
						cdaudioextract_restitue_arg_to_extract (Audio, NUM_WAV);
						conv_exec_extract (Audio->ListConv[ NUM_WAV ].WithCommandLineUser, CDPARANOIA_CD_TO_WAV, "CDPARANOIA_CD_TO_WAV");
						
						conv_copy_src_to_dest( Audio->PathName_Dest_Tmp, Audio->PathName_Dest_Wav );
						g_unlink( Audio->PathName_Dest_Tmp );
						
					}
					break;
				case EXTRACT_WITH_CDPARANOIA_EXPERT :
					if (FALSE == conv.bool_stop) {
						
						gchar *Str = NULL;
						
						Str = g_strdup_printf (_("\n[Track: %d]\n"), Audio->Num_Track);
						cdaudioextract_set_list_to_vte (Str);
						g_free (Str);
						Str = NULL;
						Str = g_strdup (_("Ripping expert mode PASS 1/2\n"));
						cdaudioextract_set_list_to_vte (Str);
						g_free (Str);
						Str = NULL;
						
						cdaudioextract_restitue_arg_to_extract (Audio, NUM_WAV_EXPERT_1);
						conv_exec_extract (Audio->ListConv[ NUM_WAV_EXPERT_1 ].WithCommandLineUser, CDPARANOIA_CD_TO_WAV_EXPERT, "CDPARANOIA_CD_TO_WAV_EXPERT");
					}
					
					if (FALSE == conv.bool_stop) {
						
						cdaudioextract_set_list_to_vte (_("Ripping expert mode PASS 2/2\n"));
						
						cdaudioextract_restitue_arg_to_extract (Audio, NUM_WAV_EXPERT_2);
						conv_exec_extract (Audio->ListConv[ NUM_WAV_EXPERT_2 ].WithCommandLineUser, CDPARANOIA_CD_TO_WAV_EXPERT, "CDPARANOIA_CD_TO_WAV_EXPERT");
					}
					
					if (FALSE == conv.bool_stop) {
						
						cdaudioextract_set_list_to_vte (_("Control the content of the extracted files\n"));
						
						cdaudioextract_copy_src_to_dest (
								Audio->ExtractExpert.Name1,
								Audio->ExtractExpert.Name2,
								Audio->PathName_Dest_Wav,
								Audio->ListConv[ NUM_WAV_EXPERT_1 ].Size
								);
					}
					conv.ListPutTextview = filelc_remove_glist (conv.ListPutTextview);
					break;
				
				case EXTRACT_WITH_CDDA2WAV :

					if (FALSE == conv.bool_stop) {
							
						cdaudioextract_restitue_arg_to_extract (Audio, NUM_WAV);
						conv_exec_extract (Audio->ListConv[ NUM_WAV ].WithCommandLineUser, CDDA2WAV_CD_TO_WAV, "CDDA2WAV_CD_TO_WAV");
						
						if (FALSE == conv.bool_stop) {
							
							gchar *StrOldName = NULL;
							/*
							in: PATH_TMP_XCFA_AUDIOCD
								audio.inf
								audio.wav
							*/
							StrOldName = g_strdup_printf ("%s/audio.wav", conv.TmpRep);
							conv_copy_src_to_dest (StrOldName, Audio->PathName_Dest_Wav);
							g_free (StrOldName);
							StrOldName = NULL;
						}
					}
					break;
				}
				
				if (FALSE == conv.bool_stop && TRUE == Audio->EtatNormalise) {
					switch (VarCdExtract.ComboNumActive) {
					case 0 :
						Audio->EtatPeak = CD_NORM_PEAK_WAITING;
						PRINT("ADD PEAK/ALBUM");
						CdNormalise_add_PeakGroup ();
						break;
					case 1 :
						Audio->EtatPeak = CD_NORM_PEAK_WAITING;
						break;
					}
				}
				
				Audio->EtatSelection_Wav = CD_ETAT_ATTENTE;

				if (TRUE == conv.bool_stop) Audio->Bool_Delette_Wav = TRUE;
				// conv_inc_rip_completed ();
				Audio->Etat_Wav = ETAT_CONV_CD_CONV_OK;
				
				conv.Bool_MAJ_select = TRUE;
			}
		}
		if (TRUE == conv.bool_stop) {
			Audio->Bool_Delette_Wav = TRUE;
			break;
		}
		
		ListOne = g_list_next(ListOne);
	}
	/*close(conv.tube_extract [ 0 ]);*/

	// g_free (VarCdExtract.PtrDirActuel);
	// VarCdExtract.PtrDirActuel = NULL;
		
	/*  OPERATION DE PEAK/ALBUM
	 *  Amplification maximale du volume pour un groupe de fichiers en respectant les ecarts de niveau entre chacun d'eux.
	 */
	if (FALSE == conv.bool_stop && TRUE == VarCdExtract.BoolListIsPeakAlbum) {
	
		PRINT("DEBUT NORMALISATION PEAK/ALBUM");
			
		if (FALSE == conv.bool_stop &&
			  CdNormalise_get_is_list_PeakGroup () == TRUE &&
			  CdNormalise_list_PeakGroup_is_ready () == TRUE) {
			
			GList				*List2 = NULL;
			VAR_CD_NORMALISE_ELEMENT	*VarCDNormaliseElement = NULL;
			gchar				**PtrTabArgs = NULL;
			
			// COPIE DES FICHIERS DESTINAION -> TEMPORAIRE
			
			List2 = g_list_first (CdNormalise_get_list_PeakGroup ());
			while (List2) {
				if (NULL != (VarCDNormaliseElement = (VAR_CD_NORMALISE_ELEMENT *)List2->data)) {
					conv_copy_src_to_dest (VarCDNormaliseElement->PathNameDest, VarCDNormaliseElement->PathNameSrc);
				}
				List2 = g_list_next(List2);
			}
			
			// -- CHERCHER LA MOYENNE PEAK
			
			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
			PtrTabArgs [ pos++ ] = g_strdup ("-n");
			
			// -- AJOUTER LA LISTE DES FICHIERS

			List2 = g_list_first (CdNormalise_get_list_PeakGroup ());
			while (List2) {
				if (NULL != (VarCDNormaliseElement = (VAR_CD_NORMALISE_ELEMENT *)List2->data)) {
					PtrTabArgs [ pos++ ] = g_strdup_printf ("%s", VarCDNormaliseElement->PathNameSrc);
				}
				List2 = g_list_next(List2);
			}
			PtrTabArgs [ pos++ ] = NULL;
			
			conv.bool_percent_extract = conv.bool_percent_conv = TRUE;
			conv_to_convert (PtrTabArgs, FALSE, NORMALISE_GET_LEVEL, "NORMALISE_GET_LEVEL -> PEAK/GROUP");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			// -- APPLIQUER LA MOYENNE PEAK
			if (FALSE == conv.bool_stop) {
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
				PtrTabArgs [ pos++ ] = g_strdup_printf ("--gain=%fdB", conv.value_PEAK_RMS_GROUP_ARGS);
							
				// -- AJOUTER LA LISTE DES FICHIERS
				
				List2 = g_list_first (CdNormalise_get_list_PeakGroup ());
				while (List2) {
					if (NULL != (VarCDNormaliseElement = (VAR_CD_NORMALISE_ELEMENT *)List2->data)) {
						PtrTabArgs [ pos++ ] = g_strdup_printf ("%s", VarCDNormaliseElement->PathNameSrc);
					}
					List2 = g_list_next(List2);
				}
				PtrTabArgs [ pos++ ] = g_strdup ("--");
				PtrTabArgs [ pos++ ] = NULL;
				
				// -- NORMALISATION
				
				conv.bool_percent_extract = conv.bool_percent_conv = TRUE;
				conv_to_convert (PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> PEAK/GROUP");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

				CdNormalise_set_normalise_ok ();

				// BESOIN DE TEMPS !
				// g_usleep (1000000);
				// usleep (1000000);
				sleep (3);

				// COPIE DES FICHIERS TEMPORAIRE -> DESTINATION
				List2 = g_list_first (CdNormalise_get_list_PeakGroup ());
				while (List2) {
					if (NULL != (VarCDNormaliseElement = (VAR_CD_NORMALISE_ELEMENT *)List2->data)) {
						conv_copy_src_to_dest (VarCDNormaliseElement->PathNameSrc, VarCDNormaliseElement->PathNameDest);
					}
					List2 = g_list_next(List2);
				}
			}
			
			conv.Bool_MAJ_select = TRUE;
		}
		PRINT("FIN NORMALISATION PEAK/ALBUM");
	}
		
	PRINT("FIN THREAD EXTRACTION");
	conv.bool_thread_extract = FALSE;
	pthread_exit(0);
}
/*
*---------------------------------------------------------------------------
* THREAD CONVERSION
*---------------------------------------------------------------------------
*/
static void cdaudioextract_thread_conversion_from_cd (void *arg)
{
	GList		*ListOne = NULL;
	CD_AUDIO	*Audio = NULL;
	gint		pos;
	gchar		**PtrTabArgs = NULL;
	
	conv.bool_thread_conv = TRUE;
	
	PRINT("DEBUT THREAD CONVERSIONS");
				
	ListOne = g_list_first (EnteteCD.GList_Audio_cd);
	while( FALSE == conv.bool_stop && NULL != ListOne ) {
		
		sleep( 0 );
		if( TRUE == conv.bool_stop ) {
			conv.bool_stop = TRUE;
			Audio->Bool_Delette_Mp3 = TRUE;
			Audio->Bool_Delette_Ogg = TRUE;
			break;
		}
		
		if (NULL == (Audio = (CD_AUDIO *)ListOne->data)) {
			ListOne = g_list_next(ListOne);
			continue;
		}
		
		if (TRUE == Audio->EtatNormalise && VarCdExtract.ComboNumActive == 1) {
			/*  PEAK
			 *  Le volume sera ajuste au maximum pour cet element
			 */
			if (FALSE == conv.bool_stop &&
			     Audio &&
			     Audio->EtatPeak == CD_NORM_PEAK_WAITING &&
			     CdNormalise_get_is_list_PeakGroup () == FALSE &&
			     Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK) {
				
				gchar	*Name = NULL;
				gchar	*Ptr = NULL;
				gchar	**PtrTabArgs = NULL;

				// Creation de la list
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
				PtrTabArgs [ pos++ ] = g_strdup ("--peak");
								
				Name = g_strdup_printf ("%s0123456789", Audio->PathName_Dest_Wav);
				Ptr = strrchr (Name, '/');
				Ptr ++;
				strcpy (Ptr, "01.wav");
				conv_copy_src_to_dest (Audio->PathName_Dest_Wav, Name);
				
				PtrTabArgs [ pos++ ] = g_strdup_printf ("%s", Name);
				PtrTabArgs [ pos++ ] = g_strdup ("--");
				PtrTabArgs [ pos++ ] = NULL;
				
				// NORMALISATION PEAK
				conv_to_convert (PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> Peak");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
				conv_copy_src_to_dest (Name, Audio->PathName_Dest_Wav);
				g_unlink (Name);
				
				g_free (Name);
				Name = NULL;
				
				Audio->EtatPeak = CD_NORM_PEAK_NONE;
				Audio->EtatNormalise = FALSE;
				
				conv.Bool_MAJ_select = TRUE;
			}
		}
		
		if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_Sox == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
		          
			VarCdExtract.PassFileUnique ++;
			if (VarCdExtract.PassFileUnique >= VarCdExtract.NbrFileUnique) {
				
				GList	*list = NULL;
				gchar	**PtrTabArgs = NULL;
				
				// conv_product_name_for_label (Audio->PathName_Dest_Wav);
				switch (VarCdExtract.ChoiceFileCue) {
				case 0 :					   break;
				case 1 : Audio->Etat_Flac = ETAT_CONV_CD_WAITING;  break;
			 	case 2 : Audio->Etat_Ogg  = ETAT_CONV_CD_WAITING;  break;
				case 3 : Audio->Etat_Mpc  = ETAT_CONV_CD_WAITING;  break;
			 	}
								
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("sox");
				list = g_list_first (VarCdExtract.ListFileFormatUnique);
				while (list) {
					if (NULL != (gchar *)list->data) {
						PtrTabArgs [ pos++ ] = g_strdup ((gchar *)list->data);
					}
					list = g_list_next (list);
				}
				PtrTabArgs [ pos++ ] = g_strdup ("-t");
				PtrTabArgs [ pos++ ] = g_strdup (".wav");
				PtrTabArgs [ pos++ ] = g_strdup ("-S");
				PtrTabArgs [ pos++ ] = g_strdup ("-r");
				PtrTabArgs [ pos++ ] = g_strdup ("44100");
				PtrTabArgs [ pos++ ] = g_strdup ("-c");
				PtrTabArgs [ pos++ ] = g_strdup ("2");
				PtrTabArgs [ pos++ ] = g_strdup ("-o");
				PtrTabArgs [ pos++ ] = g_strdup (VarCdExtract.NameFileFormatUnique);
				PtrTabArgs [ pos++ ] = NULL;
				
				conv_to_convert (PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
				PRINT("DEBUG FOR @DZEF");
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print("\t%s = %s\n\n", VarCdExtract.NameFileFormatUnique, libutils_test_file_exist (VarCdExtract.NameFileFormatUnique) ? "EXIST" : "NOT FOUND");
	
				conv.Bool_MAJ_select = TRUE;
			}
		          
			Audio->Etat_Sox = ETAT_CONV_CD_CONV_OK;
		}
		
		if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_Flac == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
			
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("\n");
				g_print ("SOURCE WAV <%s>\n", Audio->PathName_Dest_Wav);
				g_print ("DEST   FLAC<%s>\n", Audio->PathName_Dest_Flac);
			}
			
			PtrTabArgs = filelc_AllocTabArgs();
			g_free( PtrTabArgs[ 0 ] );
			g_free( PtrTabArgs[ 1 ] );
			g_free( PtrTabArgs[ 2 ] );
			cdaudioextract_restitue_arg_to_conv( PtrTabArgs, Audio, NUM_FLAC );
			conv_to_convert (PtrTabArgs, Audio->ListConv[ NUM_FLAC ].WithCommandLineUser,  FLAC_WAV_TO_FLAC, "FLAC_WAV_TO_FLAC");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			Audio->EtatSelection_Flac = CD_ETAT_ATTENTE;

			if (TRUE == conv.bool_stop) Audio->Bool_Delette_Flac = TRUE;
			Audio->Etat_Flac = ETAT_CONV_CD_CONV_OK;

			conv.Bool_MAJ_select = TRUE;
		}
		else if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_Ape == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
		          
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("\n");
				g_print ("SOURCE WAV <%s>\n", Audio->PathName_Dest_Wav);
				g_print ("DEST   APE <%s>\n", Audio->PathName_Dest_Ape);
			}

			PtrTabArgs = filelc_AllocTabArgs();
			g_free( PtrTabArgs[ 0 ] );
			g_free( PtrTabArgs[ 1 ] );
			g_free( PtrTabArgs[ 2 ] );
			cdaudioextract_restitue_arg_to_conv (PtrTabArgs, Audio, NUM_APE);
			conv_to_convert (PtrTabArgs, Audio->ListConv[ NUM_APE ].WithCommandLineUser,  MAC_WAV_TO_APE, "MAC_WAV_TO_APE");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			Audio->EtatSelection_Ape = CD_ETAT_ATTENTE;

			if (TRUE == conv.bool_stop) Audio->Bool_Delette_Ape = TRUE;
			Audio->Etat_Ape = ETAT_CONV_CD_CONV_OK;

			conv.Bool_MAJ_select = TRUE;
		}
		else if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_WavPack == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
		          
			gchar   *path = NULL;
			gchar   *ptr = NULL;
				
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("\n");
				g_print ("SOURCE WAV     <%s>\n", Audio->PathName_Dest_Wav);
				g_print ("DEST   WAVPACK <%s>\n", Audio->PathName_Dest_WavPack);
			}
			
			Audio->EtatBoolWavpack = TRUE;
			
			PtrTabArgs = filelc_AllocTabArgs();
			g_free( PtrTabArgs[ 0 ] );
			g_free( PtrTabArgs[ 1 ] );
			g_free( PtrTabArgs[ 2 ] );
			cdaudioextract_restitue_arg_to_conv (PtrTabArgs, Audio, NUM_WAVPACK);
			conv_to_convert (PtrTabArgs, Audio->ListConv[ NUM_WAVPACK ].WithCommandLineUser,  WAVPACK_WAV_TO_WAVPACK, "WAVPACK_WAV_TO_WAVPACK");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			Audio->EtatSelection_WavPack = CD_ETAT_ATTENTE;

			if (*optionsWavpack_get_wavpack_signature_md5 () != '\0') {
				// conv.bool_set_log_wavpack = TRUE;
			}

			path = g_strdup (Audio->PathName_Dest_Wav);
			ptr = strrchr (path, '.');
			ptr ++;
			*ptr ++ = 'w';
			*ptr ++ = 'v';
			*ptr    = '\0';
			if (strcmp (path, Audio->PathName_Dest_WavPack) != 0)
				conv_copy_src_to_dest (path, Audio->PathName_Dest_WavPack);
			g_free (path);
			path = NULL;

			if (TRUE == conv.bool_stop) Audio->Bool_Delette_WavPack = TRUE;
			Audio->Etat_WavPack = ETAT_CONV_CD_CONV_OK;

			conv.Bool_MAJ_select = TRUE;
		}
		else if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_Ogg == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
		          
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("\n");
				g_print ("SOURCE WAV <%s>\n", Audio->PathName_Dest_Wav);
				g_print ("DEST   OGG <%s>\n", Audio->PathName_Dest_Ogg);
			}

			PtrTabArgs = filelc_AllocTabArgs();
			g_free( PtrTabArgs[ 0 ] );
			g_free( PtrTabArgs[ 1 ] );
			g_free( PtrTabArgs[ 2 ] );
			cdaudioextract_restitue_arg_to_conv (PtrTabArgs, Audio, NUM_OGG);
			conv_to_convert( PtrTabArgs, Audio->ListConv[ NUM_OGG ].WithCommandLineUser, OGGENC_WAV_TO_OGG, "OGGENC_WAV_TO_OGG");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			Audio->EtatSelection_Ogg = CD_ETAT_ATTENTE;

			if (TRUE == conv.bool_stop) Audio->Bool_Delette_Ogg = TRUE;
			Audio->Etat_Ogg = ETAT_CONV_CD_CONV_OK;

			conv.Bool_MAJ_select = TRUE;
		}
		else if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_m4a == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
		          
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("\n");
				g_print ("SOURCE WAV <%s>\n", Audio->PathName_Dest_Wav);
				g_print ("DEST   M4A <%s>\n", Audio->PathName_Dest_M4a);
			}

			PtrTabArgs = filelc_AllocTabArgs();
			g_free( PtrTabArgs[ 0 ] );
			g_free( PtrTabArgs[ 1 ] );
			g_free( PtrTabArgs[ 2 ] );
			cdaudioextract_restitue_arg_to_conv (PtrTabArgs, Audio, NUM_M4A);
			conv_to_convert( PtrTabArgs, Audio->ListConv[ NUM_M4A ].WithCommandLineUser, FAAC_WAV_TO_M4A, "FAAC_WAV_TO_M4A");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			Audio->EtatSelection_M4a = CD_ETAT_ATTENTE;

			if (TRUE == conv.bool_stop) Audio->Bool_Delette_M4a = TRUE;
			Audio->Etat_m4a = ETAT_CONV_CD_CONV_OK;

			conv.Bool_MAJ_select = TRUE;
		}
		else if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_Aac == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
		          
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("\n");
				g_print ("SOURCE WAV <%s>\n", Audio->PathName_Dest_Wav);
				g_print ("DEST   AAC <%s>\n", Audio->PathName_Dest_Aac);
			}

			PtrTabArgs = filelc_AllocTabArgs();
			g_free( PtrTabArgs[ 0 ] );
			g_free( PtrTabArgs[ 1 ] );
			g_free( PtrTabArgs[ 2 ] );
			cdaudioextract_restitue_arg_to_conv (PtrTabArgs, Audio, NUM_AAC);
			conv_to_convert( PtrTabArgs, Audio->ListConv[ NUM_AAC ].WithCommandLineUser, AACPLUSENC_WAV_TO_AAC, "AACPLUSENC_WAV_TO_AAC");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			Audio->EtatSelection_Aac = CD_ETAT_ATTENTE;

			if (TRUE == conv.bool_stop) Audio->Bool_Delette_Aac = TRUE;
			Audio->Etat_Aac = ETAT_CONV_CD_CONV_OK;

			conv.Bool_MAJ_select = TRUE;
			conv.bool_percent_conv = TRUE;
		}

		else if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_Mpc == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
		          
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("\n");
				g_print ("SOURCE WAV <%s>\n", Audio->PathName_Dest_Wav);
				g_print ("DEST   MPC <%s>\n", Audio->PathName_Dest_Mpc);
			}

			PtrTabArgs = filelc_AllocTabArgs();
			g_free( PtrTabArgs[ 0 ] );
			g_free( PtrTabArgs[ 1 ] );
			g_free( PtrTabArgs[ 2 ] );
			cdaudioextract_restitue_arg_to_conv (PtrTabArgs, Audio, NUM_MPC);
			conv_to_convert( PtrTabArgs, Audio->ListConv[ NUM_MPC ].WithCommandLineUser, MPPENC_WAV_TO_MPC, "MPPENC_WAV_TO_MPC");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			Audio->EtatSelection_Mpc = CD_ETAT_ATTENTE;

			if (TRUE == conv.bool_stop) Audio->Bool_Delette_Mpc = TRUE;
			Audio->Etat_Mpc = ETAT_CONV_CD_CONV_OK;

			conv.Bool_MAJ_select = TRUE;
		}
		else if (FALSE == conv.bool_stop &&
		          Audio &&
		          Audio->Etat_Mp3 == ETAT_CONV_CD_WAITING &&
		          Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
		          Audio->EtatPeak == CD_NORM_PEAK_NONE) {
		          
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("\n");
				g_print ("SOURCE WAV <%s>\n", Audio->PathName_Dest_Wav);
				g_print ("DEST   MP3 <%s>\n", Audio->PathName_Dest_Mp3);
			}

			PtrTabArgs = filelc_AllocTabArgs();
			g_free( PtrTabArgs[ 0 ] );
			g_free( PtrTabArgs[ 1 ] );
			g_free( PtrTabArgs[ 2 ] );
			cdaudioextract_restitue_arg_to_conv (PtrTabArgs, Audio, NUM_MP3);
			conv_to_convert( PtrTabArgs, Audio->ListConv[ NUM_MP3 ].WithCommandLineUser, LAME_WAV_TO_MP3, "LAME_WAV_TO_MP3");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			Audio->EtatSelection_Mp3 = CD_ETAT_ATTENTE;

			if (TRUE == conv.bool_stop) Audio->Bool_Delette_Mp3 = TRUE;
			Audio->Etat_Mp3 = ETAT_CONV_CD_CONV_OK;

			conv.Bool_MAJ_select = TRUE;
		}
		
		if (TRUE == conv.bool_stop) {
			Audio->Bool_Delette_Mp3 = TRUE;
			Audio->Bool_Delette_Ogg = TRUE;
			break;
		}

		if (Audio &&
		    Audio->Etat_Wav != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_Flac != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_Mp3 != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_Ogg != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_m4a != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_Aac != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_Mpc != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_Ape != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_WavPack != ETAT_CONV_CD_WAITING &&
		    Audio->Etat_Sox != ETAT_CONV_CD_WAITING &&
		    Audio->EtatPeak == CD_NORM_PEAK_NONE) {

		    	ListOne = g_list_next(ListOne);
		}
	
		if (TRUE == conv.bool_stop) {
			Audio->Bool_Delette_Mp3 = TRUE;
			Audio->Bool_Delette_Ogg = TRUE;
			break;
		}
	}
	
	PRINT("FIN THREAD CONVERSIONS");
	
	// close(conv.tube_conv [ 0 ]);
	conv.bool_thread_conv = FALSE;
	pthread_exit(0);
}
// 
// 
static gint cdaudioextract_timeout (gpointer data)
{
	if (TRUE == conv.BoolPutTextview && TRUE == WinVte_is_ok()) {

		GList		*List = NULL;
		gchar		*Ptr = NULL;
		FILE		*fp = NULL;
		
	 	if (NULL != (List = g_list_first (conv.ListPutTextview))) {
	 		
	 		if( TRUE == OptionsCd_get_save_log_mode_expert()) {
	 			
	 			gchar	*StrModeExpert = NULL;
	 			
	 			StrModeExpert = g_strdup_printf( "%s/LogExpertMode.txt", VarCdExtract.PathDestinationModeExpert );
	 			fp = fopen( StrModeExpert, "a" );
	 			g_free( StrModeExpert );
	 			StrModeExpert = NULL;
	 		}
			
			while (List) {
				if (NULL != (Ptr = (gchar *)List->data)) {
					WinVte_window_write_va( Ptr, NULL );
					
					// SAVE LOG FROM CDPARANOIA MODE EXPERT
					if( TRUE == OptionsCd_get_save_log_mode_expert()) {
						fprintf( fp, "%s", Ptr );
					}
					
					g_free (Ptr);
					Ptr = NULL;
					List->data = NULL;
				}
				List = g_list_next(List);
			}
	 		
	 		if( TRUE == OptionsCd_get_save_log_mode_expert())
				fclose( fp );
			
			conv.BoolPutTextview = FALSE;
		}
	}

	if (TRUE == conv.Bool_MAJ_select) {
		conv.Bool_MAJ_select = FALSE;
		cdaudio_update_glist ();
		return TRUE;
	}
	
	if (TRUE == conv.BoolIsExtract || TRUE == conv.BoolIsConvert || TRUE == conv.BoolIsCopy || TRUE == conv.BoolIsNormalise || TRUE == conv.BoolIsReplaygain) {
		gchar	Str [ 200 ];
		
		Str [ 0 ] = '\0';
		
		if (TRUE == conv.BoolIsExtract) {
			strcat (Str, "<b><i>Extraction</i></b> ");
		}
		if (TRUE == conv.BoolIsConvert) {
			strcat (Str, "<b><i>Conversion</i></b> ");
		}
		if (TRUE == conv.BoolIsCopy) {
			strcat (Str, "<b><i>Copie</i></b> ");
		}
		if (TRUE == conv.BoolIsNormalise) {
			strcat (Str, "<b><i>Normalise</i></b> ");
		}
		if (TRUE == conv.BoolIsReplaygain) {
			strcat (Str, "<b><i>Replaygain</i></b>");
		}
		
		WindScan_set_label (Str);
	}
	if (FALSE == conv.bool_thread_conv && FALSE == conv.bool_thread_extract && conv.total_percent < 1.0) {
		conv.total_percent = 1.0;
		conv.bool_percent_extract = TRUE;
	}
	if (TRUE == conv.bool_percent_extract || TRUE == conv.bool_percent_conv) {
		gchar	*Str = NULL;
		
		// DEBUG
		if( conv.total_percent > 1.0 ) conv.total_percent = 1.0;
		Str = g_strdup_printf ("%d%%", (int)(conv.total_percent * 100));
		WindScan_set_progress (Str, conv.total_percent);
		g_free (Str);
		Str = NULL;
		
		if (conv.bool_percent_extract) conv.bool_percent_extract = FALSE;
		if (conv.bool_percent_conv) conv.bool_percent_conv = FALSE;
		if (FALSE == conv.bool_thread_conv && FALSE == conv.bool_thread_extract) return (TRUE);
	}
	
	if (FALSE == conv.bool_thread_conv && FALSE == conv.bool_thread_extract) {
		CD_AUDIO	*Audio = NULL;
		GList		*List = NULL;
		FILE		*fp = NULL;

		g_source_remove (conv.handler_timeout_conv);

		// Creation du fichier *.m3u
		if (TRUE == var_cd.Bool_create_file_m3u) {
			if (NULL != (fp = fopen (var_cd.Pathname_m3u, "w"))) {
				List = g_list_first (EnteteCD.GList_Audio_cd);
				while (List) {
					if (NULL != (Audio = (CD_AUDIO *)List->data)) {
						if (Audio->Etat_Flac == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Mp3 == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Ogg == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_m4a == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Aac == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Mpc == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Ape == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_WavPack == ETAT_CONV_CD_CONV_OK) {

							if (Audio->Etat_Flac == ETAT_CONV_CD_CONV_OK)	fprintf (fp, "%s\n", Audio->PathName_Dest_Flac);
							if (Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK &&
							    Audio->BoolM3uForWav == TRUE)			fprintf (fp, "%s\n", Audio->PathName_Dest_Wav);
							if (Audio->Etat_Mp3 == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "%s\n", Audio->PathName_Dest_Mp3);
							if (Audio->Etat_Ogg == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "%s\n", Audio->PathName_Dest_Ogg);
							if (Audio->Etat_m4a == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "%s\n", Audio->PathName_Dest_M4a);
							if (Audio->Etat_Aac == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "%s\n", Audio->PathName_Dest_Aac);
							if (Audio->Etat_Mpc == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "%s\n", Audio->PathName_Dest_Mpc);
							if (Audio->Etat_Ape == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "%s\n", Audio->PathName_Dest_Ape);
							if (Audio->Etat_WavPack == ETAT_CONV_CD_CONV_OK)	fprintf (fp, "%s\n", Audio->PathName_Dest_WavPack);
						}

					}
					List = g_list_next(List);
				}
				fclose (fp);
			}
			g_free (var_cd.Pathname_m3u);
			var_cd.Pathname_m3u = NULL;
			
			if (NULL != (fp = fopen (var_cd.Pathname_xspf, "w"))) {
				
				fprintf (fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
				fprintf (fp, "<playlist version=\"1\" xmlns=\"http://xspf.org/ns/0/\">\n");
				fprintf (fp, "    <trackList>\n");
				
				List = g_list_first (EnteteCD.GList_Audio_cd);
				while (List) {
					if (NULL != (Audio = (CD_AUDIO *)List->data)) {
						if (Audio->Etat_Flac == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Mp3 == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Ogg == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_m4a == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Aac == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Mpc == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_Ape == ETAT_CONV_CD_CONV_OK ||
						    Audio->Etat_WavPack == ETAT_CONV_CD_CONV_OK) {
							
							if (Audio->Etat_Flac == ETAT_CONV_CD_CONV_OK)
														fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_Flac);
							if (Audio->Etat_Wav == ETAT_CONV_CD_CONV_OK && Audio->BoolM3uForWav == TRUE)
														fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_Wav);
							if (Audio->Etat_Mp3 == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_Mp3);
							if (Audio->Etat_Ogg == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_Ogg);
							if (Audio->Etat_m4a == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_M4a);
							if (Audio->Etat_Aac == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_Aac);
							if (Audio->Etat_Mpc == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_Mpc);
							if (Audio->Etat_Ape == ETAT_CONV_CD_CONV_OK)		fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_Ape);
							if (Audio->Etat_WavPack == ETAT_CONV_CD_CONV_OK)	fprintf (fp, "        <track><location>%s</location></track>\n", Audio->PathName_Dest_WavPack);
						}
					}
					List = g_list_next(List);
				}
				
				fprintf (fp, "    </trackList>\n");
				fprintf (fp, "</playlist>\n");
				
				fclose (fp);
			}
			g_free (var_cd.Pathname_xspf);
			var_cd.Pathname_xspf = NULL;
		}

		// Delete alloc name files of extraction/conversion
		List = g_list_first (EnteteCD.GList_Audio_cd);
		while (List) {
			if (NULL != (Audio = (CD_AUDIO *)List->data)) {
					
				if (TRUE == Audio->EtatBoolWavpack) {
					// WinError_add_to_dsk_ (Audio->PathName_Dest_WavPack);
				}

				if (Audio->PathName_Dest_Flac) {
					if (Audio->Bool_Delette_Flac) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_Flac);
						g_unlink (Audio->PathName_Dest_Flac);
					}
					g_free (Audio->PathName_Dest_Flac);
					Audio->PathName_Dest_Flac = NULL;
				}

				if (Audio->PathName_Dest_Wav) {
					if (Audio->Bool_Delette_Wav) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_Wav);
						g_unlink (Audio->PathName_Dest_Wav);
					}
					g_free (Audio->PathName_Dest_Wav);
					Audio->PathName_Dest_Wav = NULL;
				}
				
				if (Audio->PathName_Dest_Mp3) {
					if (Audio->Bool_Delette_Mp3) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_Mp3);
						g_unlink (Audio->PathName_Dest_Mp3);
					}
					g_free (Audio->PathName_Dest_Mp3);
					Audio->PathName_Dest_Mp3 = NULL;
				}

				if (Audio->PathName_Dest_Ogg) {
					if (Audio->Bool_Delette_Ogg) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_Ogg);
						g_unlink (Audio->PathName_Dest_Ogg);
					}
					g_free (Audio->PathName_Dest_Ogg);
					Audio->PathName_Dest_Ogg = NULL;
				}

				if (Audio->PathName_Dest_M4a) {
					if (Audio->Bool_Delette_M4a) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_M4a);
						g_unlink (Audio->PathName_Dest_M4a);
					}
					g_free (Audio->PathName_Dest_M4a);
					Audio->PathName_Dest_M4a = NULL;
				}

				if (Audio->PathName_Dest_Aac) {
					if (Audio->Bool_Delette_Aac) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_Aac);
						g_unlink (Audio->PathName_Dest_Aac);
					}
					g_free (Audio->PathName_Dest_Aac);
					Audio->PathName_Dest_Aac = NULL;
				}

				if (Audio->PathName_Dest_Mpc) {
					if (Audio->Bool_Delette_Mpc) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_Mpc);
						g_unlink (Audio->PathName_Dest_Mpc);
					}
					g_free (Audio->PathName_Dest_Mpc);
					Audio->PathName_Dest_Mpc = NULL;
				}

				if (Audio->PathName_Dest_Ape) {
					if (Audio->Bool_Delette_Ape) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_Ape);
						g_unlink (Audio->PathName_Dest_Ape);
					}
					g_free (Audio->PathName_Dest_Ape);
					Audio->PathName_Dest_Ape = NULL;
				}

				if (Audio->PathName_Dest_WavPack) {
					if (Audio->Bool_Delette_WavPack) {
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("g_unlink (%s)\n", Audio->PathName_Dest_WavPack);
						g_unlink (Audio->PathName_Dest_WavPack);
					}
					g_free (Audio->PathName_Dest_WavPack);
					Audio->PathName_Dest_WavPack = NULL;
				}
				
				if( Audio->PathName_Dest_Tmp ) {
					g_free( Audio->PathName_Dest_Tmp );
					Audio->PathName_Dest_Tmp = NULL;
				}
				
				if (Audio->ExtractExpert.Name1) {
					g_free (Audio->ExtractExpert.Name1);
					Audio->ExtractExpert.Name1 = NULL;
				}
				if (Audio->ExtractExpert.Name2) {
					g_free (Audio->ExtractExpert.Name2);
					Audio->ExtractExpert.Name2 = NULL;
				}
			}
			List = g_list_next(List);
		}
		
		if (VarCdExtract.ChoiceFileCue > 0) {
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print ("g_unlink (%s)\n", VarCdExtract.PathName_SrcWavSox);
			g_unlink (VarCdExtract.PathName_SrcWavSox);
		}
		
		g_free (VarCdExtract.PathName_SrcWavSox);
		VarCdExtract.PathName_SrcWavSox = NULL;
		g_free (VarCdExtract.PathName_DestFlacSox);
		VarCdExtract.PathName_DestFlacSox = NULL;
		g_free (VarCdExtract.PathName_DestOggSox);
		VarCdExtract.PathName_DestOggSox = NULL;
		g_free (VarCdExtract.PathName_DestMpcSox);
		VarCdExtract.PathName_DestMpcSox = NULL;
		
		CdNormalise_set_list_collectif_remove ();
		
		// Delete tmp rep
		if (conv.TmpRep) conv.TmpRep  = libutils_remove_temporary_rep (conv.TmpRep);

		cdaudio_update_glist ();
		cdaudio_set_flag_buttons ();

		g_chdir (VarCdExtract.PtrDirActuel);
		g_free (VarCdExtract.PtrDirActuel);	VarCdExtract.PtrDirActuel = NULL;
		
		PRINT("FIN TIMEOUT");
		
		cdcue_print_base_ioctl ();
		var_cd.TypeCreateCue = TYPE_CUE_FILE;
		cdcue_write_cue ();
		
		cdaudioextract_remove_list ();
		WindScan_close ();
		
		if (FALSE == conv.bool_stop) {
			NotifySend_msg (_("XCFA: Extraction CD"), _("Ok"), conv.bool_stop);
		} else {
			NotifySend_msg (_("XCFA: Extraction CD"), _("Stop by user"), conv.bool_stop);
		}
	}
	return (TRUE);
}

/*
*---------------------------------------------------------------------------
* INIT DATAS
*---------------------------------------------------------------------------
*/
ETAT_CONV_CD cdaudioextract_get_etat_sox (void)
{
	gchar *Ptr = NULL;
	
	Ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_name_file_cue));
	
	if (TRUE == cdaudio_get_bool_is_wav_extract_to_cue () &&
	    TRUE == gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_creation_fichier_unique_cue"))) &&
	    *Ptr != '\0') {
		return (ETAT_CONV_CD_WAITING);
	}
	return (ETAT_CONV_CD_NONE);
}
// 
// 
void cdaudioextract_set_flags_before_extraction (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;
	gchar		*PathDest = NULL;
	gint		 IndexExtraction;
	gchar		*Ptr = NULL;
	
	// PRINT_FUNC_LF();

	EnteteCD.NameCD_Device = scan_get_text_combo_cd (_CD_);

	if (cdaudioextract_get_etat_sox () == ETAT_CONV_CD_WAITING) {
		VarCdExtract.BoolFormatUnique = TRUE;
		VarCdExtract.ChoiceFileCue = gtk_combo_box_get_active ( GTK_COMBO_BOX ( GTK_WIDGET (GLADE_GET_OBJECT("combobox_choice_file_cue"))));
		// Pour SOX
		// Si conversion vers FLAC | OGG | MPC
		if (VarCdExtract.ChoiceFileCue > 0) conv.total_convert ++;
	}
	
	IndexExtraction = 0;
	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if (NULL != (Audio = (CD_AUDIO *)List->data)) {
			
			cdcue_set_BoolExtract (IndexExtraction, FALSE);
						
			Audio->BoolM3uForWav = FALSE;
			
			// INIT ETAT
			Audio->EtatBoolWavpack = FALSE;
	
			/* MAJ des flags
			*/
			Audio->Etat_Mp3      = Audio->EtatSelection_Mp3     >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
			Audio->Etat_Ogg      = Audio->EtatSelection_Ogg     >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
			Audio->Etat_Wav      = Audio->EtatSelection_Wav     >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
			Audio->Etat_Flac     = Audio->EtatSelection_Flac    >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
			Audio->Etat_m4a      = Audio->EtatSelection_M4a     >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
			Audio->Etat_Aac      = Audio->EtatSelection_Aac     >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
			Audio->Etat_Mpc      = Audio->EtatSelection_Mpc     >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
			Audio->Etat_Ape      = Audio->EtatSelection_Ape     >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
			Audio->Etat_WavPack  = Audio->EtatSelection_WavPack >= CD_ETAT_SELECT ? ETAT_CONV_CD_WAITING : ETAT_CONV_CD_NONE;
		
			Audio->Etat_Sox      = ETAT_CONV_CD_NONE;
			if (Audio->Etat_Wav == ETAT_CONV_CD_WAITING)
			Audio->Etat_Sox      = cdaudioextract_get_etat_sox ();
			if (Audio->Etat_Sox  == ETAT_CONV_CD_WAITING) conv.total_convert ++;
			
			if (Audio->Etat_Mp3  == ETAT_CONV_CD_WAITING)      Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			if (Audio->Etat_Ogg  == ETAT_CONV_CD_WAITING)      Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			if (Audio->Etat_Flac == ETAT_CONV_CD_WAITING)      Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			if (Audio->Etat_m4a  == ETAT_CONV_CD_WAITING)      Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			if (Audio->Etat_Aac  == ETAT_CONV_CD_WAITING)      Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			if (Audio->Etat_Mpc  == ETAT_CONV_CD_WAITING)      Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			if (Audio->Etat_Ape  == ETAT_CONV_CD_WAITING)      Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			if (Audio->Etat_WavPack  == ETAT_CONV_CD_WAITING)  Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			if (Audio->Etat_Sox  == ETAT_CONV_CD_WAITING)      Audio->Etat_Wav = ETAT_CONV_CD_WAITING;
			
			/* calcul total ripping tracks
			*/
			if (Audio->EtatSelection_Flac    >= CD_ETAT_SELECT ||
			    Audio->EtatSelection_Wav     >= CD_ETAT_SELECT ||
			    Audio->EtatSelection_Mp3     >= CD_ETAT_SELECT ||
			    Audio->EtatSelection_Ogg     >= CD_ETAT_SELECT ||
			    Audio->EtatSelection_M4a     >= CD_ETAT_SELECT ||
			    Audio->EtatSelection_Aac     >= CD_ETAT_SELECT ||
			    Audio->EtatSelection_Mpc     >= CD_ETAT_SELECT ||
			    Audio->EtatSelection_Ape     >= CD_ETAT_SELECT ||
			    Audio->EtatSelection_WavPack >= CD_ETAT_SELECT ) {
				
				if (Config.ExtractCdWith == EXTRACT_WITH_CDPARANOIA_EXPERT) {
					conv.total_rip ++;	// PASS 2
				}
				if( Config.ExtractCdWith == EXTRACT_WITH_CDPARANOIA || Config.ExtractCdWith == EXTRACT_WITH_CDPARANOIA_MODE_2 ) {
					conv.total_rip ++;	// NORMAL PASS
				}
				conv.total_rip ++;		// NORMAL PASS
				cdcue_set_BoolExtract (IndexExtraction, TRUE);
			}

			/* calcul total convert file
			*/
			if (Audio->EtatSelection_Mp3     >= CD_ETAT_SELECT)	conv.total_convert ++;
			if (Audio->EtatSelection_Ogg     >= CD_ETAT_SELECT)	conv.total_convert ++;
			if (Audio->EtatSelection_Flac    >= CD_ETAT_SELECT)	conv.total_convert ++;
			if (Audio->EtatSelection_M4a     >= CD_ETAT_SELECT)	conv.total_convert ++;
			if (Audio->EtatSelection_Aac     >= CD_ETAT_SELECT)	conv.total_convert ++;
			if (Audio->EtatSelection_Mpc     >= CD_ETAT_SELECT)	conv.total_convert ++;
			if (Audio->EtatSelection_Ape     >= CD_ETAT_SELECT)	conv.total_convert ++;
			if (Audio->EtatSelection_WavPack >= CD_ETAT_SELECT)	conv.total_convert ++;
			// if (TRUE == Audio->EtatNormalise) conv.total_convert += 2;
			
			/* init var to FALSE and NULL
			*/
			Audio->Bool_Delette_Flac      = FALSE;
			Audio->Bool_Delette_Wav       = FALSE;
			Audio->Bool_Delette_Mp3       = FALSE;
			Audio->Bool_Delette_Ogg       = FALSE;
			Audio->Bool_Delette_M4a       = FALSE;
			Audio->Bool_Delette_Aac       = FALSE;
			Audio->Bool_Delette_Mpc       = FALSE;
			Audio->Bool_Delette_Ape       = FALSE;
			Audio->Bool_Delette_WavPack   = FALSE;
			Audio->PathName_Dest_Flac     = NULL;
			Audio->PathName_Dest_Wav      = NULL;
			Audio->PathName_Dest_Mp3      = NULL;
			Audio->PathName_Dest_Ogg      = NULL;
			Audio->PathName_Dest_M4a      = NULL;
			Audio->PathName_Dest_Aac      = NULL;
			Audio->PathName_Dest_Mpc      = NULL;
			Audio->PathName_Dest_Ape      = NULL;
			Audio->PathName_Dest_WavPack  = NULL;

			// Create REP DEST
			if (NULL == PathDest) {
				PathDest = cdaudio_get_result_destination ();
				g_mkdir_with_parents (PathDest, 0700);
				
				if( NULL != VarCdExtract.PathDestinationModeExpert ) {
					g_free( VarCdExtract.PathDestinationModeExpert );
					VarCdExtract.PathDestinationModeExpert = NULL;
				}
				VarCdExtract.PathDestinationModeExpert = g_strdup( PathDest );
			}
			// Create tmp rep
			if( NULL == conv.TmpRep )
				conv.TmpRep  = libutils_create_temporary_rep (Config.PathnameTMP, PATH_TMP_XCFA_AUDIOCD);
			
			if( NULL == Audio->PathName_Dest_Tmp )
				Audio->PathName_Dest_Tmp = g_strdup_printf( "%s/TempAudioExtract_%02d.wav", conv.TmpRep, IndexExtraction +1 );
		
			if (Audio->Etat_Wav == ETAT_CONV_CD_WAITING) {

				switch (Config.ExtractCdWith) {
				case EXTRACT_WITH_CDPARANOIA :
				case EXTRACT_WITH_CDPARANOIA_MODE_2 :
					if (Audio->EtatSelection_Wav <= CD_ETAT_ATTENTE_EXIST) {

						Audio->PathName_Dest_Wav = g_strdup_printf ("%s/%s.wav", conv.TmpRep, Audio->NameSong);
					}
					else {
						Audio->BoolM3uForWav = TRUE;
						if (Audio->tags->Album && *Audio->tags->Album) {
							Audio->PathName_Dest_Wav = g_strdup_printf ("%s/%s.wav", PathDest, Audio->NameSong);
						} else {
							Audio->PathName_Dest_Wav = g_strdup_printf ("%s/%s.wav", PathDest, Audio->NameSong);
						}
					}
					break;
					
				case EXTRACT_WITH_CDPARANOIA_EXPERT :
					if (Audio->EtatSelection_Wav <= CD_ETAT_ATTENTE_EXIST) {

						Audio->PathName_Dest_Wav = g_strdup_printf ("%s/%s.wav", conv.TmpRep, Audio->NameSong);
					}
					else {
						Audio->BoolM3uForWav = TRUE;
						if (Audio->tags->Album && *Audio->tags->Album) {
							Audio->PathName_Dest_Wav = g_strdup_printf ("%s/%s.wav", PathDest, Audio->NameSong);
						} else {
							Audio->PathName_Dest_Wav = g_strdup_printf ("%s/%s.wav", PathDest, Audio->NameSong);
						}
					}
					Audio->ExtractExpert.Name1 = g_strdup_printf ("%s/TrackExpert_1.wav", conv.TmpRep);
					Audio->ExtractExpert.Name2 = g_strdup_printf ("%s/TrackExpert_2.wav", conv.TmpRep);
					break;
					
				case EXTRACT_WITH_CDDA2WAV :
					if (Audio->EtatSelection_Wav <= CD_ETAT_ATTENTE_EXIST) {
						Audio->PathName_Dest_Wav = g_strdup_printf ("%s/%s.wav", conv.TmpRep, Audio->NameSong);
					}
					else {
						Audio->PathName_Dest_Wav = g_strdup_printf ("%s/%s.wav", PathDest, Audio->NameSong);
						Audio->BoolM3uForWav = TRUE;
					}
					break;
				}
			}
			
			if (TRUE == VarCdExtract.BoolFormatUnique && NULL == VarCdExtract.NameFileFormatUnique) {
				
				Ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_name_file_cue));
				VarCdExtract.NameFileFormatUnique = g_strdup_printf ("%s/%s.wav",  PathDest, Ptr);
			}
			
			g_free (BaseIoctl.PathNameDestFileCue);
			BaseIoctl.PathNameDestFileCue = NULL;
			Ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_name_file_cue));
			BaseIoctl.PathNameDestFileCue = g_strdup_printf ("%s/%s.cue", PathDest, Ptr);
			
			Audio->PathName_Dest_Ogg      = g_strdup_printf ("%s/%s.ogg",  PathDest, Audio->NameSong);
			Audio->PathName_Dest_Mp3      = g_strdup_printf ("%s/%s.mp3",  PathDest, Audio->NameSong);
			Audio->PathName_Dest_Flac     = g_strdup_printf ("%s/%s.flac", PathDest, Audio->NameSong);
			Audio->PathName_Dest_M4a      = g_strdup_printf ("%s/%s.m4a",  PathDest, Audio->NameSong);
			Audio->PathName_Dest_Aac      = g_strdup_printf ("%s/%s.aac",  PathDest, Audio->NameSong);
			Audio->PathName_Dest_Mpc      = g_strdup_printf ("%s/%s.mpc",  PathDest, Audio->NameSong);
			Audio->PathName_Dest_Ape      = g_strdup_printf ("%s/%s.ape",  PathDest, Audio->NameSong);
			Audio->PathName_Dest_WavPack  = g_strdup_printf ("%s/%s.wv",   PathDest, Audio->NameSong);
						
			if (TRUE == var_cd.Bool_create_file_m3u && NULL == var_cd.Pathname_m3u && NULL != PathDest) {
				if (*Config.StringNameFile_m3u_xspf == '\0') {
					var_cd.Pathname_m3u = g_strdup_printf ("%s/FileM3u.m3u", PathDest);
				}
				else {
					var_cd.Pathname_m3u = g_strdup_printf ("%s/%s.m3u", PathDest, Config.StringNameFile_m3u_xspf);
				}
			}
			if (TRUE == var_cd.Bool_create_file_m3u &&  NULL == var_cd.Pathname_xspf && NULL != PathDest) {
				if (*Config.StringNameFile_m3u_xspf == '\0') {
					var_cd.Pathname_xspf = g_strdup_printf ("%s/FileXspf.xspf", PathDest);
				}
				else {
					var_cd.Pathname_xspf = g_strdup_printf ("%s/%s.xspf", PathDest, Config.StringNameFile_m3u_xspf);
				}
			}
		}
		
		IndexExtraction ++;
		
		List = g_list_next(List);
	}

	g_free (PathDest);
	PathDest = NULL;
}
/*
*---------------------------------------------------------------------------
* FUNC FOR EXTRACTIONS / CONVERSIONS
*---------------------------------------------------------------------------
*/
void cdaudioextract_extraction_cd_to_file_action (void)
{
	pthread_t nmr_tid;
	
	// PRINT_FUNC_LF();

	if (TRUE == cdaudio_get_bool_is_wav_extract_to_cue () &&
	    TRUE == gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_creation_fichier_unique_cue")))) {
		
		gchar *Ptr = NULL;
		Ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_name_file_cue));
		if (*Ptr == '\0') {
			wind_info_init (
				WindMain,
				_("File name not found"),
				_("Please enter a file name"),
				  "");
			return;
		}
	}

	if (gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_extract_with_cdparanoia)) == TRUE)
		Config.ExtractCdWith = EXTRACT_WITH_CDPARANOIA;
	else if (gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_cdparanoia_mode_2)) == TRUE)
		Config.ExtractCdWith = EXTRACT_WITH_CDPARANOIA_MODE_2;
	else if (gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_cdparanoia_mode_expert)) == TRUE)
		Config.ExtractCdWith = EXTRACT_WITH_CDPARANOIA_EXPERT;
	else if (gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_extract_with_cdda2wav)) == TRUE)
		Config.ExtractCdWith = EXTRACT_WITH_CDDA2WAV;

	if( conv.bool_thread_conv == TRUE || conv.bool_thread_extract == TRUE ) {
		PRINT("conv.bool_thread_conv == TRUE || conv.bool_thread_extract == TRUE  -> RETURN" );
		return;
	}
	
	conv_reset_struct (WindScan_close_request);
 	VarCdExtract.BoolFormatUnique = FALSE;
 	if (VarCdExtract.NameFileFormatUnique != NULL)
  		g_free (VarCdExtract.NameFileFormatUnique);
	VarCdExtract.NameFileFormatUnique = NULL;
	g_free (BaseIoctl.PathNameDestFileCue);
	BaseIoctl.PathNameDestFileCue = NULL;
	cdaudioextract_set_flags_before_extraction ();

	// WindScan_open ("Extract / Convert from CD", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Extract / Convert from CD",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Extract from CD ...</i></b>");
	WindScan_set_pulse ();
	if (EXTRACT_WITH_CDPARANOIA_EXPERT == Config.ExtractCdWith) {
		WindScan_show_expander ();
		// cwhile (gtk_events_pending()) gtk_main_iteration();
		WinVte_reset();
	}
 	conv.bool_thread_conv = TRUE;
	VarCdExtract.NbrFileUnique = 0;
	VarCdExtract.PassFileUnique = 0;
	VarCdExtract.ListFileFormatUnique = NULL;
		
	VarCdExtract.NbrList = 50;
	VarCdExtract.ComboNumActive = gtk_combo_box_get_active (GTK_COMBO_BOX (var_cd.Adr_combobox_normalise_cd));
	VarCdExtract.BoolListIsPeakAlbum = cdaudioextract_create_list_is_Extract ();
	
	cdaudioextract_create_list_is_Convert ();

	VarCdExtract.PtrDirActuel = g_get_current_dir ();
	g_chdir (conv.TmpRep);
	
	conv.bool_thread_conv = TRUE;
 	pthread_create (&nmr_tid, NULL ,(void *)cdaudioextract_thread_conversion_from_cd, (void *)NULL);
	conv.bool_thread_extract = TRUE;
 	pthread_create (&nmr_tid, NULL ,(void *)cdaudioextract_thread_extraction_from_cd, (void *)NULL);
	PRINT("DEBUT TIMEOUT");
	conv.handler_timeout_conv = g_timeout_add (50, cdaudioextract_timeout, 0);
}


