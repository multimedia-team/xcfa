 /*
 *  file      : poche.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef poche_h
#define poche_h 1


#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gi18n.h>


typedef enum {
	_TYPE_IMAGE_ = 0,
	_TYPE_TEXT_,
	_TYPE_TEXT_TITLE_
} TYPE_IMAGE;

typedef struct {
	TYPE_IMAGE	TypeImage;						// IMAGE | TEXT
	
	// _TYPE_TEXT_
	// _TYPE_TEXT_TITLE_
	gchar		*Texte;							// 
	
	gchar		*FontName;						// 
	gint		SizeFontName;					// 
	gboolean	BoolFontBold;					// TRUE = BOLD,   FALSE = NORMAL
	gboolean	BoolFontItalic;					// TRUE = ITALIC, FALSE = NORMAL
	gint		PosCombobox;					// -1 OR 0 .. 3
	
	// _TYPE_IMAGE_
	GdkPixbuf	*Pixbuf;						// Adresse du PixBuf en traitement
	GdkPixbuf	*PixbufOriginal;				// Adresse du PixBuf original
	
	// VALUES
	gdouble		x0;								// Pos X
	gdouble		y0;								// Pos Y
	gint		image_width;					// Width
	gint		image_height;					// Height
	gdouble		XPointer;						// Position x de la souris lors du clic
	gdouble		YPointer;						// Position y de la souris lors du clic
	gboolean	BoolIsSelected;					// TRUE = selected with mouse cursor
	gboolean	BoolStructRemove;				// TRUE = STRUCT IS REMOVE | FALSE = ACTIVE
	gboolean	BoolFlipVertical;				// TRUE = Force flip Vertical
	gboolean	BoolFlipHorizontal;				// TRUE = Force flip Horizontal
} IMAGE;

typedef enum {
	IMG_HAUT_GAUCHE = 0,						// 
	IMG_HAUT,									// 
	IMG_HAUT_DROIT,								// 
	IMG_DROIT,									// 
	IMG_BAS_DROIT,								// 
	IMG_BAS,									// 
	IMG_BAS_GAUCHE,								// 
	IMG_GAUCHE,									// 
	IMG_MOVE,									// 
	IMG_SIZE,									// For size tab HANDLE_MOVE []
	IMG_NONE									// NO IMAGE SELECTED
} IMG_CORNER;

typedef struct {

	GdkRectangle	zone[ IMG_SIZE ];			// Les IMG_SIZE coordonnees de saisie de l'image
												// EX:
												// 	zone [ IMG_HAUT ] . x
												// 	zone [ IMG_HAUT ] . y
												// 	zone [ IMG_HAUT ] . width
												// 	zone [ IMG_HAUT ] . height
} HANDLE_MOVE;

typedef struct {
												// UNE STRUCTURE PAR FICHIER IMAGE
	gchar		*name_img;						// Nom du fichier image original
	gchar		*name_png;						// Nom du fichier PNG
	GtkWidget	*togglebutton;					// Adresse du bouton
	GtkWidget	*image;							// Adresse de l'image
	gboolean	BoolStructRemove;				// TRUE = STRUCT IS REMOVE | FALSE = ACTIVE
} GLIST_POCHETTE;

typedef struct {
	gboolean	BoolAccessChanged;				// COMBOBOX changed is TRUE
	
	GtkComboBox	*AdrComboBoxChoice;				// 
		
	GtkWidget	*AdrEntrySearchImgWeb;			// Adresse saisie image web
	GtkWidget	*AdrEntryNameFileSave;			// Adresse saisie nom fichier image
	GtkWidget	*AdrEntryTitleCD;				// Adresse saisie titre du CD
	
	gchar		*TitleCD;						// 
	gchar		*Buffer_none;					// 
	gchar		*Buffer_title;					// 
	gchar		*Buffer_title_time;				// 
	gchar		*Buffer_artist_title_time;		// 
	
	GtkWidget	*AdrDrawingarea;				// Adresse de la surface
	gboolean	BoolScaleAdjust;				// TRUE = ajustement de l'echelle avec la surface de la fentre
	cairo_surface_t	*image;						// Adresse de l'image
	gdouble		scale;							// Pourcentage de surface de l'image
	gdouble		x0;								// Pos X
	gdouble		y0;								// Pos Y
	gint		image_width;					// Width
	gint		image_height;					// Height
	GList		*ListImage;						// Pointeur de structure IMAGE pour AdrDrawingarea
	gboolean	BoolEventButtonPress;			// TRUE == BOUTON SOURIS PRESSED
	IMAGE		*GetImage;						// Adresse de l'image en selection
	IMG_CORNER	ImgCorner;						// See IMG_CORNER
	GtkWidget	*Adr_viewport_image_preview;	// Adresse viewport
	GtkWidget	*Adr_table;						// Adresse table contenu dans le viewport
	GList		*glist;							// Contient les structures GLIST_POCHETTE
	GtkWidget	*AdrTextview;					// Adresse textview
	gboolean	BoolSaveToFile;					// TRUE si sauvegarde vers fichier
	HANDLE_MOVE	HandleMove;						// Les coordonnees de saisie de l'image
} VIEW;

extern	VIEW view;

// Taille image lors d'une importation
#define SIZE_IMAGE_VIEWPORT	80
#define SIZE_IMAGE_ADD		80
// La definition SCALE_MIN correspond au pourcentage de representation le plus petit de l'image de fond
// SCALE_MIN doit etre adaptee suivant la taille la plus petite de la fenetre
#define SCALE_MIN		0.55
// Taille minimum d'une image lors d'un redimensionnement
#define MIN_SIZE_IMAGE		20	
// Definition d'un millimetre
// Formule employee en PostScript
#define MM(x)(((gdouble)x * 72.0) / 25.4 )


// 
// POCHE.C
// 
IMAGE		*poche_add_to_glist( gchar *PathNameFile, gdouble x, gdouble y, gboolean p_BoolScale, TYPE_IMAGE p_TypeImage );
IMAGE		*poche_get_struct_selected_is_txt( void );
void		poche_set_selected_flag_image( IMAGE *p_Image );
void		poche_set_selected_first_image( IMAGE *p_Image );
void		poche_set_selected_up_image( IMAGE *p_Image );
void		poche_set_selected_down_image( IMAGE *p_Image );
void		poche_set_selected_last_image( IMAGE *p_Image );
void		poche_add_img_file_to_Drawingarea( GSList *p_list );
void		poche_remove_ListImage( void );
void		poche_remove_image( void );
void		poche_remove_view (void);
void		poche_set_texte_title( gchar *p_str );
void		poche_set_size_request( void );
void		poche_set_flag_buttons (void );
gboolean	pochetxt_get_is_bold( void );
gboolean	pochetxt_get_is_italic( void );
void		poche_print_zoom_changed( gdouble p_zoom_scale );
// 
// POCHE_DIR.C
// 
void		pochedir_add_img_file( GSList *p_list );
void		pochedir_remove_GlistPochette( void );
void		pochedir_table_add_images( void );
void		pochedir_make_glist (gchar *filename);
void		pochedir_destroy_image( GLIST_POCHETTE *gl );
void		pochedir_set_ctrl( gboolean p_Bool );
// 
// POCHE_DRAW.C
// 
// void		pochedraw_paint( cairo_surface_t *cs );
cairo_t 	*pochedraw_paint( cairo_surface_t *cs, cairo_t *p_cr );
void		pochedraw_get_handle_move( IMAGE *p_Image, gboolean p_BoolTestCursor );
// 
// POCHE_TXT.C
// 
void		pochetxt_set_text_to_textview( IMAGE *p_Image );
gchar		*pochetxt_get_ptr_entry_img_web( void );
gchar		*pochetxt_get_ptr_entry_name_file_to_save( void );
void		pochetxt_set_text_from_cd( gchar *p_buffer, gint p_PosCombobox );
void		pochetxt_set_combobox_choice( gint p_activate );
// 
// POCHE_WEB.C
// 
void		pocheweb_get (void);


#endif

