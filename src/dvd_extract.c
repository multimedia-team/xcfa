 /*
 *  file      : dvd_extract.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif


#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "notify_send.h"
#include "configuser.h"
#include "dvd.h"
#include "prg_init.h"
#include "win_scan.h"
#include "file.h"
#include "conv.h"
#include "tags.h"




/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/
typedef struct {

	gboolean	bool_end_pthread;
	guint		Handler_Timeout;

	gboolean	bool_update;

	gint		total_file;
	gint		file_active;
	gboolean	bool_mess;

	gboolean	BoolListIsPeakAlbum;
	gint		TTNormPeak;			/* Nombre total de Peak			*/
	gint		TTNormPeakCollectif;		/* Nombre total de Peak Collectif	*/
	
} VAR_DVDEXTRACT;

VAR_DVDEXTRACT VarDvdextract;


/*
*---------------------------------------------------------------------------
* EXTERN
*---------------------------------------------------------------------------
*/

extern int kill (pid_t pid, int sig);




// 
// 
void dvdextract_get_total_extraction (void)
{
	GList *list = NULL;
	gchar *ptr = NULL;

	VarDvdextract.total_file = 0;

	list = g_list_first (GlistDvdExtract);
	while (list) {

		if ((ptr = (gchar *)list->data)) {
			VarDvdextract.total_file ++;
		}
		list = g_list_next (list);
	}
}
// 
// 
gchar *dvdextract_get_name_in_list (GList *p_list)
{
	GList    *list = NULL;
	gchar    *ptr = NULL;
	
	list = g_list_first (p_list);
	while (list) {
		if ((ptr = (gchar *)list->data)) {
			if (strstr (ptr, "pcm:file=")) {
				
				return ((gchar *)ptr);
			}
		}
		list = g_list_next(list);
	}
	return ((gchar *)NULL);
}
// 
// 
gchar **dvdextract_set_arg( GList *p_list )
{
	GList	*list = NULL;
	gchar	*ptr = NULL;
	gint	pos = 0;
	gchar	**PtrTabArgs = NULL;
	
	PtrTabArgs = filelc_AllocTabArgs();
	
	list = g_list_first (p_list);
	while (list) {
		if ((ptr = (gchar *)list->data)) {
			PtrTabArgs [ pos++ ] = g_strdup (ptr);
		}
		list = g_list_next(list);
	}
	PtrTabArgs [ pos++ ] = NULL;
	
	return( (gchar **)PtrTabArgs );
}
// 
// 
void *dvdextract_pthread (void *data)
{
	GList		*listOne = NULL;
	NEW_DVD_EXTRACT *StructDvdExtract = NULL;
	/*gint             value_fix;*/
	gint		pos;
	gchar		**PtrTabArgs = NULL;
	gboolean	RetConvert;
	
	PRINT_FUNC_LF();
	
	VarDvdextract.bool_end_pthread = FALSE;
	
	listOne = g_list_first (GlistDvdExtract);
	while (FALSE == conv.bool_stop && listOne) {
		if (NULL != (StructDvdExtract = (NEW_DVD_EXTRACT *)listOne->data)) {
			
			VarDvdextract.file_active ++;
			VarDvdextract.bool_mess = TRUE;

			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				g_print ("****** Pathname **********************\n");
				g_print ("%s\n", StructDvdExtract->Path);
				g_print ("**************************************\n");
				g_print ("\n");
			}
			
			PtrTabArgs = dvdextract_set_arg( (GList *)StructDvdExtract->list );
			RetConvert = conv_to_convert( PtrTabArgs, FALSE, MPLAYER_AUDIO_TO_WAV, "NEW:-> MPLAYER_AUDIO_TO_WAV" );
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			if( FALSE == conv.bool_stop && TRUE == RetConvert ) {
								
				VarDvdextract.bool_update = TRUE;
				StructDvdExtract->Var->EtatChoix = _COCHE_;
				
				// MPLAYER
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
				PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
				PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
				PtrTabArgs [ pos++ ] = g_strdup (StructDvdExtract->Path);
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup ("pcm");
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup ("pcm:file=/tmp/toto.wav");
				PtrTabArgs [ pos++ ] = g_strdup ("-srate");
				PtrTabArgs [ pos++ ] = g_strdup ("44100");
				PtrTabArgs [ pos++ ] = NULL;
				conv_to_convert( PtrTabArgs, FALSE, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
				conv_copy_src_to_dest ("/tmp/toto.wav", StructDvdExtract->Path);
				
				g_unlink ("/tmp/toto.wav");
								
				// PEAK
				// 
				if (FALSE == conv.bool_stop && TRUE == StructDvdExtract->Var->EtatNormalise && FALSE == VarDvdextract.BoolListIsPeakAlbum) {
				
					WindScan_set_label (StructDvdExtract->Path);
					VarDvdextract.TTNormPeak ++;
					
					PtrTabArgs = filelc_AllocTabArgs();
					pos = 3;
					PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
					PtrTabArgs [ pos++ ] = g_strdup ("--peak");
					PtrTabArgs [ pos++ ] = g_strdup ("--");
					PtrTabArgs [ pos++ ] = g_strdup (StructDvdExtract->Path);
					PtrTabArgs [ pos++ ] = NULL;
					conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> Peak");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					
					PRINT("TRAITEMENT NORMALISE  PEAK  OK");
					
					StructDvdExtract->Var->EtatNormalise = FALSE;
					VarDvdextract.bool_update = TRUE;
				}
			}
			else {
				gchar *ptr = NULL;
				
				if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
					g_print ("\n");
					g_print ("---------------------------\n");
					g_print ("SORTIE PAR: Audio: no sound\n");
					g_print ("---------------------------\n");
					g_print ("\n");
				}
				var_dvd.bool_err = 3;
				
				if ((ptr = strrchr (dvdextract_get_name_in_list ((GList *)StructDvdExtract->list), '/'))) {
					if( TRUE == OptionsCommandLine.BoolVerboseMode )
						g_print ("%s\n", dvdextract_get_name_in_list ((GList *)StructDvdExtract->list));
					ptr ++;
					if( TRUE == OptionsCommandLine.BoolVerboseMode )
						g_print ("%s\n", ptr);
				}
			}

			if (TRUE == conv.bool_stop) {
				g_unlink (StructDvdExtract->Path);
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("\n\nARRET PAR L'UTILISATEUR.\n\tSuppression du fichier\n\t[%s]\n", StructDvdExtract->Path);
				break;
			}
		}
		listOne = g_list_next (listOne);
	}
	
	if (FALSE == conv.bool_stop && VarDvdextract.BoolListIsPeakAlbum == TRUE) {
		listOne = g_list_first (GlistDvdExtract);
		while (listOne) {
			if (NULL != (StructDvdExtract = (NEW_DVD_EXTRACT *)listOne->data)) {
				
				if (TRUE == conv.bool_stop) break;
				
				// PEAK ALBUM
				// 
				if (FALSE == conv.bool_stop && StructDvdExtract->Var->EtatNormalise == TRUE) {

					VarDvdextract.TTNormPeakCollectif ++;
					if (var_dvd.TTNormPeakCollectif == VarDvdextract.TTNormPeakCollectif) {
						
						GList		*List = NULL;
						NEW_DVD_EXTRACT *StructExtract = NULL;
						if (FALSE == conv.bool_stop) {
							// -- CHERCHER LA MOYENNE PEAK
							PtrTabArgs = filelc_AllocTabArgs();
							pos = 3;
							PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
							PtrTabArgs [ pos++ ] = g_strdup ("-n");
							// -- AJOUTER LA LISTE DES FICHIERS
							List = g_list_first (GlistDvdExtract);
							while (List) {
								if ((StructExtract = (NEW_DVD_EXTRACT *)List->data) && StructExtract->EtatNormalise == TRUE) {
									PtrTabArgs [ pos++ ] = g_strdup (StructDvdExtract->Path);
								}
								List = g_list_next(List);
							}
							PtrTabArgs [ pos++ ] = NULL;
							conv_to_convert( PtrTabArgs, FALSE, NORMALISE_GET_LEVEL, "NORMALISE_GET_LEVEL TEST -> PEAK/GROUP");
							PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
						}
						if (FALSE == conv.bool_stop) {
							// -- APPLIQUER LA MOYENNE PEAK-GROUP
							PtrTabArgs = filelc_AllocTabArgs();
							pos = 3;
							PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
							PtrTabArgs [ pos++ ] = g_strdup_printf ("--gain=%fdB", conv.value_PEAK_RMS_GROUP_ARGS);
							// -- AJOUTER LA LISTE DES FICHIERS
							List = g_list_first (GlistDvdExtract);
							while (List) {
								if ((StructExtract = (NEW_DVD_EXTRACT *)List->data) && StructExtract->EtatNormalise == TRUE) {
									PtrTabArgs [ pos++ ] = g_strdup (StructExtract->Path);
									StructExtract->Var->EtatNormalise = FALSE;
								}
								List = g_list_next(List);
							}
							PtrTabArgs [ pos++ ] = NULL;
							conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> PEAK/GROUP");
							PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
							
							PRINT("TRAITEMENT NORMALISE  PEAK_COLLECTIF  OK");
						}
						
						VarDvdextract.bool_update = TRUE;
					}
				}
			}
			listOne = g_list_next (listOne);
		}
	}
	
	// close(conv.tube_conv [ 0 ]);
	VarDvdextract.bool_end_pthread = TRUE;
	pthread_exit (0);
}
// 
// 
static gint dvdextract_timeout (gpointer data)
{
	if (TRUE == conv.BoolIsExtract || TRUE == conv.BoolIsConvert || TRUE == conv.BoolIsCopy || TRUE == conv.BoolIsNormalise || TRUE == conv.BoolIsReplaygain) {
		gchar	Str [ 200 ];
		
		Str [ 0 ] = '\0';
		
		if (TRUE == conv.BoolIsExtract) {
			strcat (Str, "<b><i>Extraction</i></b> ");
		}
		if (TRUE == conv.BoolIsConvert) {
			strcat (Str, "<b><i>Conversion</i></b> ");
		}
		if (TRUE == conv.BoolIsCopy) {
			strcat (Str, "<b><i>Copie</i></b> ");
		}
		if (TRUE == conv.BoolIsNormalise) {
			strcat (Str, "<b><i>Normalise</i></b> ");
		}
		if (TRUE == conv.BoolIsReplaygain) {
			strcat (Str, "<b><i>Replaygain</i></b>");
		}
		
		WindScan_set_label (Str);
	}
	if (TRUE == VarDvdextract.bool_update) {
		VarDvdextract.bool_update = FALSE;
		dvd_update ();
	}
	if (TRUE == VarDvdextract.bool_end_pthread) {

		NEW_DVD_EXTRACT *StructDvdExtract = NULL;
		GList           *listOne = NULL;
		GList           *listTwo = NULL;
		gchar           *ptr = NULL;
		
		g_source_remove (VarDvdextract.Handler_Timeout);

		WindScan_close ();
		
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("Suppression des structures\n");
		listOne = g_list_first (GlistDvdExtract);
		while (listOne) {
			if ((StructDvdExtract = (NEW_DVD_EXTRACT *)listOne->data)) {
				
				listTwo = g_list_first (StructDvdExtract->list);
				while (listTwo) {
					if ((ptr = (gchar *)listTwo->data)) {
						g_free (ptr);
						ptr = NULL;
						listTwo->data = NULL;
					}
					listTwo = g_list_next (listTwo);
				}
				g_free (StructDvdExtract->Path);
				StructDvdExtract->Path = NULL;
				g_list_free (StructDvdExtract->list);
				StructDvdExtract->list = NULL;
				g_free (StructDvdExtract);
				StructDvdExtract = NULL;
				listOne->data = NULL;
				
			}
			listOne = g_list_next (listOne);
		}
		g_list_free (GlistDvdExtract);
		GlistDvdExtract = NULL;

		dvd_set_flag_buttons_dvd ();
		
		if (FALSE == conv.bool_stop) {
			NotifySend_msg (_("XCFA: DVD rip"), _("Ok"), conv.bool_stop);
		} else {
			NotifySend_msg (_("XCFA: DVD rip"), _("Stop by user"), conv.bool_stop);
		}
	}
	else if (TRUE == VarDvdextract.bool_mess) {

		gchar *str = NULL;

		VarDvdextract.bool_mess = FALSE;
		str = g_strdup_printf ("mplayer dvd -> wav   %d / %d",
					VarDvdextract.file_active,
					VarDvdextract.total_file);
		WindScan_set_label_bar (str);
		g_free (str);
		str = NULL;
	}

	return (TRUE);
}
// 
// 
void dvdextract_dvd_to_file (void)
{
	pthread_t nmr_tid;
	
	conv_reset_struct (WindScan_close_request);
	VarDvdextract.total_file = 0;
	VarDvdextract.file_active = 0;
	dvdextract_get_total_extraction ();
	VarDvdextract.bool_mess = TRUE;
	VarDvdextract.TTNormPeak = 0;
	VarDvdextract.TTNormPeakCollectif = 0;
	VarDvdextract.bool_update = TRUE;
	VarDvdextract.BoolListIsPeakAlbum = dvd_get_value_normalise_dvd () == 0 ? TRUE : FALSE;
	
	// WindScan_open ("Extract With Mplayer", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Extract With Mplayer",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Extraction in progress ...</i></b>");

	VarDvdextract.bool_end_pthread = FALSE;
	VarDvdextract.Handler_Timeout = g_timeout_add (100, dvdextract_timeout, 0);

	pthread_create (&nmr_tid, NULL ,(void *)dvdextract_pthread, (void *)NULL);
	// DEBUG
	// pthread_detach (nmr_tid);
}














