 /*
 *  file      : cd_audio.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cd_audio_extract.h"
#include "fileselect.h"
#include "prg_init.h"
#include "configuser.h"
#include "scan.h"
#include "options.h"
#include "parse.h"
#include "win_reader.h"
#include "mplayer.h"
#include "popup.h"
#include "win_info.h"
#include "cd_curl.h"
#include "cd_audio.h"
#include "statusbar.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/


enum
{
	NUM_TREE_CDAUDIO_PLAY = 0,
	NUM_TREE_CDAUDIO_WAV,
	NUM_TREE_CDAUDIO_FLAC,
	NUM_TREE_CDAUDIO_APE,
	NUM_TREE_CDAUDIO_WAVP,
	NUM_TREE_CDAUDIO_OGG,
	NUM_TREE_CDAUDIO_M4A,
	NUM_TREE_CDAUDIO_AAC,
	NUM_TREE_CDAUDIO_MPC,
	NUM_TREE_CDAUDIO_MP3,
	NUM_TREE_CDAUDIO_NUM,
	NUM_TREE_CDAUDIO_TIME,
	NUM_TREE_CDAUDIO_NORMALISE,
	NUM_TREE_CDAUDIO_NOM,
	NUM_TREE_CDAUDIO_ALL_COLUMN
};



enum
{
	COLUMN_CDAUDIO_PLAY = 0,
	COLUMN_CDAUDIO_WAV,
	COLUMN_CDAUDIO_FLAC,
	COLUMN_CDAUDIO_APE,
	COLUMN_CDAUDIO_WAVPACK,
	COLUMN_CDAUDIO_OGG,
	COLUMN_CDAUDIO_M4A,
	COLUMN_CDAUDIO_AAC,
	COLUMN_CDAUDIO_MPC,
	COLUMN_CDAUDIO_MP3,
	COLUMN_CDAUDIO_TRACK_NUM,
	COLUMN_CDAUDIO_TRACK_TIME,
	COLUMN_CDAUDIO_FILE_NORMALIZE,
	COLUMN_CDAUDIO_TRACK_NAME,
	CD_NUM_COLUMNS
};


typedef struct
{
	GdkPixbuf    *PlayTrack;
	GdkPixbuf    *Flac;
	GdkPixbuf    *Wav;
	GdkPixbuf    *Mp3;
	GdkPixbuf    *Ogg;
	GdkPixbuf    *M4a;
	GdkPixbuf    *Aac;
	GdkPixbuf    *Mpc;
	GdkPixbuf    *Ape;
	GdkPixbuf    *WavPack;
	gchar        *Track_Num;
	gchar        *Track_Time;
	GdkPixbuf    *ColumnCDNormalize;
	gchar        *Track_Name;

} ARTICLES_CD;

GArray     *Articles_cd = NULL;
VAR_CD      var_cd;
ENTETE_CD   EnteteCD;

gboolean BoolPathCaractersStrip = FALSE;




// 
// 
void cdaudio_put_label_titre (gchar *Messag)
{
	gchar     *Str = NULL;
	
	Str = g_strdup_printf ("<span color=\"black\"><b>%s</b></span>", Messag);
	gtk_label_set_markup (GTK_LABEL (var_cd.Adr_Label_Titre), Str);
	g_free (Str);
	Str = NULL;
}
// 
// 
void cdaudio_put_label_duree (gchar *Messag)
{
	gchar     *Str = NULL;
	
	Str = g_strdup_printf ("<span color=\"black\"><b>%s</b></span>", Messag);
	gtk_label_set_markup (GTK_LABEL (var_cd.Adr_Label_Duree), Str);
	g_free (Str);
	Str = NULL;
}
// 
// 
gboolean cdaudio_bool_from_to (TYPE_FILE_IS p_to, gboolean is_mess)
{
	if (p_to == FILE_IS_FLAC) {
		return (PrgInit.bool_flac);
	}
	else if (p_to == FILE_IS_WAV) {
		if (Config.ExtractCdWith == EXTRACT_WITH_CDDA2WAV) {
			return (PrgInit.bool_cdda2wav);
		}
		else {
			return (PrgInit.bool_cdparanoia);
		}
	}
	else if (p_to == FILE_IS_MP3) {
		return (PrgInit.bool_lame);
	}
	else if (p_to == FILE_IS_OGG) {
		return (PrgInit.bool_oggenc);
	}
	else if (p_to == FILE_IS_M4A) {
		return (PrgInit.bool_faac);
	}
	else if (p_to == FILE_IS_AAC) {
		return (PrgInit.bool_aacplusenc);
	}
	else if (p_to == FILE_IS_MPC) {
		return (PrgInit.bool_mppenc);
	}
	else if (p_to == FILE_IS_APE) {
		return (PrgInit.bool_ape);
	}
	else if (p_to == FILE_IS_WAVPACK) {
		return (PrgInit.bool_wavpack);
	}
	else if (p_to == FILE_TO_NORMALISE) {
		return (PrgInit.bool_normalize);
	}
	return (FALSE);
}
// 
// 
gboolean cdaudio_file_exist (CD_AUDIO *Audio, TYPE_FILE_IS TypeFileIs)
{
	gchar    *PathDest = NULL;
	gchar    *PathNameDest = NULL;
	gboolean  BoolExist = FALSE;
	
	if (NULL != Audio) {
		PathDest = cdaudio_get_result_destination ();

		if (TypeFileIs == FILE_IS_FLAC)
			PathNameDest = g_strdup_printf ("%s/%s.flac", PathDest, Audio->NameSong);
		else if (TypeFileIs == FILE_IS_WAV)
			PathNameDest = g_strdup_printf ("%s/%s.wav",  PathDest, Audio->NameSong);
		else if (TypeFileIs == FILE_IS_MP3)
			PathNameDest = g_strdup_printf ("%s/%s.mp3",  PathDest, Audio->NameSong);
		else if (TypeFileIs == FILE_IS_OGG)
			PathNameDest = g_strdup_printf ("%s/%s.ogg",  PathDest, Audio->NameSong);
		else if (TypeFileIs == FILE_IS_M4A)
			PathNameDest = g_strdup_printf ("%s/%s.m4a",  PathDest, Audio->NameSong);
		else if (TypeFileIs == FILE_IS_AAC)
			PathNameDest = g_strdup_printf ("%s/%s.aac",  PathDest, Audio->NameSong);
		else if (TypeFileIs == FILE_IS_MPC)
			PathNameDest = g_strdup_printf ("%s/%s.mpc",  PathDest, Audio->NameSong);
		else if (TypeFileIs == FILE_IS_APE)
			PathNameDest = g_strdup_printf ("%s/%s.ape",  PathDest, Audio->NameSong);
		else if (TypeFileIs == FILE_IS_WAVPACK)
			PathNameDest = g_strdup_printf ("%s/%s.wv",   PathDest, Audio->NameSong);
		
		if (NULL != PathNameDest) {
			// g_print("\tPathNameDest = %s\n",PathNameDest);
			BoolExist = libutils_test_file_exist (PathNameDest);
		}
		
		g_free (PathNameDest);
		PathNameDest = NULL;
		g_free (PathDest);
		PathDest = NULL;
	}
	return (BoolExist);
}
// 
// 
ETAT_SELECTION_CD cdaudio_get_next_flag (TYPE_FILE_IS type_file_is, gboolean p_flag_next, gint p_NumTrack)
{
	GList              *List = NULL;
	CD_AUDIO           *Audio = NULL;
	ETAT_SELECTION_CD   etat = CD_ETAT_ATTENTE;
	gboolean            BoolOpExpert = FALSE;
	gboolean            BoolFileExist = FALSE;
	
	if (FALSE == cdaudio_bool_from_to (type_file_is, TRUE)) return (CD_ETAT_PRG_ABSENT);

	if (NULL != (List = g_list_nth (EnteteCD.GList_Audio_cd, p_NumTrack))) {
		if (NULL != (Audio = (CD_AUDIO *)List->data)) {

			if (type_file_is == FILE_IS_FLAC) {
				etat = Audio->EtatSelection_Flac;
				BoolOpExpert = options_get_entry_is_valid (FLAC_WAV_TO_FLAC);
			}
			else if (type_file_is == FILE_IS_WAV) {
				etat = Audio->EtatSelection_Wav;
				BoolOpExpert = options_get_entry_is_valid (CDPARANOIA_CD_TO_WAV);
			}
			else if (type_file_is == FILE_IS_MP3) {
				etat = Audio->EtatSelection_Mp3;
				BoolOpExpert = options_get_entry_is_valid (LAME_WAV_TO_MP3);
			}
			else if (type_file_is == FILE_IS_OGG) {
				etat = Audio->EtatSelection_Ogg;
				BoolOpExpert = options_get_entry_is_valid (OGGENC_WAV_TO_OGG);
			}
			else if (type_file_is == FILE_IS_M4A) {
				etat = Audio->EtatSelection_M4a;
				BoolOpExpert = options_get_entry_is_valid (FAAC_WAV_TO_M4A);
			}
			else if (type_file_is == FILE_IS_AAC) {
				etat = Audio->EtatSelection_Aac;
				BoolOpExpert = options_get_entry_is_valid (AACPLUSENC_WAV_TO_AAC);
			}
			else if (type_file_is == FILE_IS_MPC) {
				etat = Audio->EtatSelection_Mpc;
				BoolOpExpert = options_get_entry_is_valid (MPPENC_WAV_TO_MPC);
			}
			else if (type_file_is == FILE_IS_APE) {
				etat = Audio->EtatSelection_Ape;
				BoolOpExpert = options_get_entry_is_valid (MAC_WAV_TO_APE);
			}
			else if (type_file_is == FILE_IS_WAVPACK) {
				etat = Audio->EtatSelection_WavPack;
				BoolOpExpert = options_get_entry_is_valid (WAVPACK_WAV_TO_WAVPACK);
			}

			BoolFileExist = cdaudio_file_exist (Audio, type_file_is);
			
			switch (etat) {
			case CD_ETAT_NONE :
				return (BoolFileExist ? CD_ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
				
			case CD_ETAT_PRG_ABSENT :
				return (CD_ETAT_PRG_ABSENT);
				
			case CD_ETAT_ATTENTE :
				if (TRUE == p_flag_next) {
					if (TRUE == BoolFileExist) {
						return (CD_ETAT_SELECT_EXIST);
					}
					return (CD_ETAT_SELECT);
				}
				if (TRUE == BoolFileExist) return (CD_ETAT_ATTENTE_EXIST);
				return (CD_ETAT_ATTENTE);

			case CD_ETAT_ATTENTE_EXIST :
				if (TRUE == p_flag_next) {
					if (TRUE == BoolFileExist) {
						return (CD_ETAT_SELECT_EXIST);
					}
					return (CD_ETAT_SELECT);
				}
				if (TRUE == BoolFileExist) return (CD_ETAT_ATTENTE_EXIST);
				return (CD_ETAT_ATTENTE);

			case CD_ETAT_SELECT :
				if (TRUE == p_flag_next) {
					if (TRUE == BoolFileExist) {
						if (type_file_is != FILE_IS_WAV && BoolOpExpert == TRUE) return (CD_ETAT_SELECT_EXPERT_EXIST);
						return (CD_ETAT_ATTENTE_EXIST);
					}
					if (type_file_is != FILE_IS_WAV && BoolOpExpert == TRUE) return (CD_ETAT_SELECT_EXPERT);
					return (CD_ETAT_ATTENTE);
				}
				if (TRUE == BoolFileExist) return (ETAT_SELECT_EXIST);
				return (CD_ETAT_SELECT);

			case CD_ETAT_SELECT_EXIST :
				if (TRUE == p_flag_next) {
					if (TRUE == BoolFileExist) {
						if (BoolOpExpert == TRUE) return (CD_ETAT_SELECT_EXPERT_EXIST);
						return (ETAT_ATTENTE_EXIST);
					}
					if (BoolOpExpert == TRUE) return (CD_ETAT_ATTENTE_EXIST);
					return (CD_ETAT_ATTENTE);
				}
				if (FALSE == BoolFileExist) return (CD_ETAT_SELECT);
				return (CD_ETAT_SELECT_EXIST);

			case CD_ETAT_SELECT_EXPERT :
				if (TRUE == p_flag_next) {
					if (TRUE == BoolFileExist) {
						return (CD_ETAT_ATTENTE_EXIST);
					}
					return (CD_ETAT_ATTENTE);
				}
				if (TRUE == BoolFileExist) return (CD_ETAT_SELECT_EXPERT_EXIST);
				return (CD_ETAT_SELECT_EXPERT);

			case CD_ETAT_SELECT_EXPERT_EXIST :
				if (TRUE == p_flag_next) {
					if (TRUE == BoolFileExist) {
						return (CD_ETAT_ATTENTE_EXIST);
					}
					return (CD_ETAT_ATTENTE);
				}
				if (FALSE == BoolFileExist) return (CD_ETAT_SELECT_EXPERT);
				return (CD_ETAT_SELECT_EXPERT_EXIST);
			}
		}
	}
	return (CD_ETAT_ATTENTE);
}
// 
// 
GdkPixbuf *cdaudio_get_pixbuf_file (CD_AUDIO *Audio, TYPE_FILE_IS p_type_file_is)
{
	ETAT_SELECTION_CD   etat = CD_ETAT_ATTENTE;
	gint                NumTrack = Audio->Num_Track -1;
	
	if (cdaudio_bool_from_to (p_type_file_is, TRUE) == FALSE) return (var_cd.Pixbuf_NotInstall);

	if (p_type_file_is == FILE_IS_FLAC)		etat = Audio->EtatSelection_Flac    = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_WAV)		etat = Audio->EtatSelection_Wav     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_OGG)		etat = Audio->EtatSelection_Ogg     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_MP3)		etat = Audio->EtatSelection_Mp3     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_M4A)	        etat = Audio->EtatSelection_M4a     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_AAC)	        etat = Audio->EtatSelection_Aac     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_MPC)		etat = Audio->EtatSelection_Mpc     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_APE)		etat = Audio->EtatSelection_Ape     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_WAVPACK)	etat = Audio->EtatSelection_WavPack = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_TO_NORMALISE) {
	
		if (Audio->EtatNormalise == TRUE) {
			return (var_cd.Pixbuf_Selected);
		}
		else {
			return (var_cd.Pixbuf_Coche);
		}
	}

	switch (etat) {
	case CD_ETAT_NONE :
		return (NULL);
	case CD_ETAT_PRG_ABSENT :
		return (var_cd.Pixbuf_NotInstall);
	case CD_ETAT_ATTENTE :
		return (var_cd.Pixbuf_Coche);
	case CD_ETAT_ATTENTE_EXIST :
		return (var_cd.Pixbuf_Coche_exist);
	case CD_ETAT_SELECT :
		return (var_cd.Pixbuf_Selected);
	case CD_ETAT_SELECT_EXIST :
		return (var_cd.Pixbuf_Selected_exist);
	case CD_ETAT_SELECT_EXPERT :
		return (var_cd.Pixbuf_Selected_expert);
	case CD_ETAT_SELECT_EXPERT_EXIST :
		return (var_cd.Pixbuf_Selected_expert_exist);
	}
	
	return (NULL);
	
}
/* Modification complete de l'etat des flags 'Audio->Bool_Extract_xxx' du GList 'EnteteCD.GList_Audio_cd'
*  Modification complete de la representation graphique (a l'ecran) de l'etat d'apres le GList
*  --
*  entree :
*      TYPE_FILE_IS type      : Structure
*      gboolean     Is_Select : Etat de 'var_cd.Pixbuf_Coche' inversee
*  retour : -
*/
void cdaudio_change_all_flags_extract_verticaly (TYPE_FILE_IS type_file_is)
{
	CD_AUDIO         *Audio = NULL;
	GdkPixbuf     *Pixbuf = NULL;
	GList         *List = NULL;
	gboolean       valid;
	GtkTreeIter    iter;
	gint           NumTrack = 0;
	
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, NumTrack);
		if ((Audio = (CD_AUDIO *)List->data)) {

			if (type_file_is == FILE_IS_FLAC) {
				Audio->EtatSelection_Flac = cdaudio_get_next_flag (FILE_IS_FLAC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_FLAC);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FLAC, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_WAV) {
				Audio->EtatSelection_Wav = cdaudio_get_next_flag (FILE_IS_WAV, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAV);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAV, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_MP3) {
				Audio->EtatSelection_Mp3 = cdaudio_get_next_flag (FILE_IS_MP3, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MP3);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MP3, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_OGG) {
				Audio->EtatSelection_Ogg = cdaudio_get_next_flag (FILE_IS_OGG, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_OGG);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_OGG, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_M4A) {
				Audio->EtatSelection_M4a = cdaudio_get_next_flag (FILE_IS_M4A, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_M4A);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_M4A, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_AAC) {
				Audio->EtatSelection_Aac = cdaudio_get_next_flag (FILE_IS_AAC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_AAC);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_AAC, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_MPC) {
				Audio->EtatSelection_Mpc = cdaudio_get_next_flag (FILE_IS_MPC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MPC);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MPC, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_APE) {
				Audio->EtatSelection_Ape = cdaudio_get_next_flag (FILE_IS_APE, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_APE);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_APE, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_WAVPACK) {
				Audio->EtatSelection_WavPack = cdaudio_get_next_flag (FILE_IS_WAVPACK, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAVPACK);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAVPACK, Pixbuf, -1);
			}
			else if (type_file_is == FILE_TO_NORMALISE) {
				Audio->EtatNormalise = (Audio->EtatNormalise == TRUE) ? FALSE : TRUE;
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_TO_NORMALISE);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FILE_NORMALIZE, Pixbuf, -1);
			}

			NumTrack ++;
		}
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
	}
}
// 
//
void cdaudio_set_etat_music_pixbuf (CD_AUDIO *p_audio, ETAT_PLAY_CD EtatPlay)
{
	GtkTreeIter	iter;
	gboolean	valid;
	GList		*List;
	CD_AUDIO		*Audio = NULL;
	gint		Track = 0;
	
	if (EtatPlay == CD_ETAT_PLAY) {
		valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
		while (valid) {
			List = g_list_nth (EnteteCD.GList_Audio_cd, Track);
			if ((Audio = List->data)) {
				if (p_audio == Audio) {
					Audio->EtatPlay = CD_ETAT_PLAY;
					gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_PLAY, var_cd.Pixbuf_CdPlay, -1);
				}
			}
			Track ++;
			valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
		}
	}
}

// 
//
void cdaudio_set_icone_stop (void)
{
	GtkTreeIter	iter;
	gboolean	valid;
	GList		*List;
	CD_AUDIO	*Audio = NULL;
	gint		Track = 0;
	
	// Tous les flags et widget sur FILE_ETAT_PLAY_ATTENTE
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, Track);
		if ((Audio = List->data)) {
			Audio->EtatPlay = CD_ETAT_PLAY_ATTENTE;
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_PLAY, var_cd.Pixbuf_CdStop, -1);
		}
		Track ++;
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
	}
	WinReader_close ();
}
/* Gestion de la selection ou de la deselection des demandes de conversions des fichiers vers un
*  autre format. Cette demande peut etre unique ou globale :
*    - unique  : click gauche
*    - globale : touche Controle appuyee + click gauche
*  --
*  entree :
*      GtkWidget *treeview   :
*      GdkEventButton *event :
*      gpointer data         :
*  retour : -
*      FALSE :
*/
gboolean cdaudio_event_click_mouse (GtkWidget *treeview, GdkEventButton *event, gpointer data)
{
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	gint                i;
	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)data;
	gchar              *Str = NULL;
	gint                NumTrack = -1;
	GdkPixbuf          *Pixbuf = NULL;
	gint                Pos_X, Pos_Y;
	gboolean            bool_key_Control = (keys.keyval == GDK_KEY_Control_L || keys.keyval == GDK_KEY_Control_R);
	gboolean            bool_key_Shift   = (keys.keyval == GDK_KEY_Shift_L || keys.keyval == GDK_KEY_Shift_R);
	gboolean            bool_key_Release = (bool_key_Control == FALSE &&  bool_key_Shift == FALSE);
	gboolean            bool_click_droit = (event->button == 3);
	gboolean            bool_click_gauche = (event->button == 1);
	CD_AUDIO           *Audio = NULL;
	GList              *List = NULL;

	gboolean            BoolSelectColPlay = FALSE;
	gboolean            BoolSelectColWav = FALSE;
	gboolean            BoolSelectColFlac = FALSE;
	gboolean            BoolSelectColApe = FALSE;
	gboolean            BoolSelectColWavP = FALSE;
	gboolean            BoolSelectColOgg = FALSE;
	gboolean            BoolSelectColM4a = FALSE;
	gboolean            BoolSelectColAac = FALSE;
	gboolean            BoolSelectColMpc = FALSE;
	gboolean            BoolSelectColMp3 = FALSE;
	gboolean            BoolSelectColTime = FALSE;
	gboolean            BoolSelectColNormalise = FALSE;
	gboolean            BoolSelectColNom = FALSE;

	/* Single clicks only 	*/
	if (event->type != GDK_BUTTON_PRESS) return (FALSE);

	/* Si pas de selection a cet endroit retour */
	if (!gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
						  (gint)event->x, (gint)event->y,
						   &path, &column, &Pos_X, &Pos_Y)) return (FALSE);


	/*
	enum
	{
		NUM_TREE_CDAUDIO_PLAY
		NUM_TREE_CDAUDIO_WAV
		NUM_TREE_CDAUDIO_FLAC
		NUM_TREE_CDAUDIO_APE
		NUM_TREE_CDAUDIO_WAVP
		NUM_TREE_CDAUDIO_OGG
		NUM_TREE_CDAUDIO_M4A
		NUM_TREE_CDAUDIO_AAC
		NUM_TREE_CDAUDIO_MPC
		NUM_TREE_CDAUDIO_MP3
		NUM_TREE_CDAUDIO_NUM
		NUM_TREE_CDAUDIO_TIME
		NUM_TREE_CDAUDIO_NORMALISE
		NUM_TREE_CDAUDIO_NOM
		NUM_TREE_CDAUDIO_ALL_COLUMN
	};
	*/
	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_CDAUDIO_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			/* g_print ("\tNUM IS: %d\n", i); */
			switch ( i ) {
			case NUM_TREE_CDAUDIO_PLAY :		BoolSelectColPlay		= TRUE;	break;
			case NUM_TREE_CDAUDIO_WAV :			BoolSelectColWav		= TRUE;	break;
			case NUM_TREE_CDAUDIO_FLAC :		BoolSelectColFlac		= TRUE;	break;
			case NUM_TREE_CDAUDIO_APE :			BoolSelectColApe		= TRUE;	break;
			case NUM_TREE_CDAUDIO_WAVP :		BoolSelectColWavP		= TRUE;	break;
			case NUM_TREE_CDAUDIO_OGG :			BoolSelectColOgg		= TRUE;	break;
			case NUM_TREE_CDAUDIO_M4A :			BoolSelectColM4a		= TRUE;	break;
			case NUM_TREE_CDAUDIO_AAC :			BoolSelectColAac		= TRUE;	break;
			case NUM_TREE_CDAUDIO_MPC :			BoolSelectColMpc		= TRUE;	break;
			case NUM_TREE_CDAUDIO_MP3 :			BoolSelectColMp3		= TRUE;	break;
			case NUM_TREE_CDAUDIO_NUM :			break;
			case NUM_TREE_CDAUDIO_TIME :		BoolSelectColTime		= TRUE;	break;
			case NUM_TREE_CDAUDIO_NORMALISE :	BoolSelectColNormalise		= TRUE;	break;
			case NUM_TREE_CDAUDIO_NOM :			BoolSelectColNom		= TRUE;	break;
			
			default: return (FALSE);
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}
	// TODO
	if( BoolSelectColNom );
	if( BoolSelectColTime );
	/*
	BoolSelectColPlay      = strcmp(column->title, "Play") == 0 ? TRUE : FALSE;
	BoolSelectColWav       = strcmp(column->title, "Wav") == 0 ? TRUE : FALSE;
	BoolSelectColFlac      = strcmp(column->title, "Flac") == 0 ? TRUE : FALSE;
	BoolSelectColApe       = strcmp(column->title, "Ape") == 0 ? TRUE : FALSE;
	BoolSelectColWavP      = strcmp(column->title, "WavP") == 0 ? TRUE : FALSE;
	BoolSelectColOgg       = strcmp(column->title, "Ogg") == 0 ? TRUE : FALSE;
	BoolSelectColM4a       = strcmp(column->title, "M4a") == 0 ? TRUE : FALSE;
	BoolSelectColAac       = strcmp(column->title, "Aac") == 0 ? TRUE : FALSE;
	BoolSelectColMpc       = strcmp(column->title, "Mpc") == 0 ? TRUE : FALSE;
	BoolSelectColMp3       = strcmp(column->title, "Mp3") == 0 ? TRUE : FALSE;
	BoolSelectColTime      = strcmp(column->title, "Time") == 0 ? TRUE : FALSE;
	BoolSelectColNormalise = strcmp(column->title, "Normalise") == 0 ? TRUE : FALSE;
	BoolSelectColNom       = strcmp(column->title, "Nom") == 0 ? TRUE : FALSE;
	*/

	/* position du curseur a l'instant du click */
	if (!BoolSelectColNormalise) {
		if (Pos_X < 18 || Pos_X > 30) return (FALSE);
		if (Pos_Y < 6 || Pos_Y > 18) return (FALSE);
	}

	if (BoolSelectColPlay) {
		BoolSelectColPlay &= (PrgInit.bool_mplayer && bool_click_gauche);
	}
	else if ((Config.ExtractCdWith == EXTRACT_WITH_CDPARANOIA_EXPERT ||  Config.ExtractCdWith == EXTRACT_WITH_CDPARANOIA) && PrgInit.bool_cdparanoia == FALSE) {
		// utils_set_prg_not_found ("CDPARANOIA", PrgInit.bool_cdparanoia);
	}
	else if (Config.ExtractCdWith == EXTRACT_WITH_CDDA2WAV && PrgInit.bool_cdda2wav == FALSE) {
		// utils_set_prg_not_found ("CDDA2WAV", PrgInit.bool_cdda2wav);
	}
	else if (BoolSelectColFlac) {
		BoolSelectColFlac &= PrgInit.bool_flac;
	}
	else if (BoolSelectColWav) {
		if (Config.ExtractCdWith == EXTRACT_WITH_CDDA2WAV) {
			BoolSelectColWav &= PrgInit.bool_cdda2wav;
		}
		else {
			BoolSelectColWav &= PrgInit.bool_cdparanoia;
		}
	}
	else if (BoolSelectColMp3) {
		BoolSelectColMp3 &= PrgInit.bool_lame;
	}
	else if (BoolSelectColOgg) {
		BoolSelectColOgg &= PrgInit.bool_oggenc;
	}
	else if (BoolSelectColM4a) {
		BoolSelectColM4a &= PrgInit.bool_faac;
	}
	else if (BoolSelectColAac) {
		BoolSelectColAac &= PrgInit.bool_aacplusenc;
	}
	else if (BoolSelectColMpc) {
		BoolSelectColMpc &= PrgInit.bool_mppenc;
	}
	else if (BoolSelectColApe) {
		BoolSelectColApe &= PrgInit.bool_ape;
	}
	else if (BoolSelectColWavP) {
		BoolSelectColWavP &= PrgInit.bool_wavpack;
	}

	if (bool_key_Shift && bool_click_gauche) {
		if (BoolSelectColWav  ||
		    BoolSelectColFlac ||
		    BoolSelectColMp3  ||
		    BoolSelectColOgg  ||
		    BoolSelectColM4a  ||
		    BoolSelectColAac  ||
		    BoolSelectColMpc  ||
		    BoolSelectColApe  ||
		    BoolSelectColWavP) {
			BoolSelectColWav  &= PrgInit.bool_cdparanoia;
			BoolSelectColFlac &= PrgInit.bool_flac;
			BoolSelectColMp3  &= PrgInit.bool_lame;
			BoolSelectColOgg  &= PrgInit.bool_oggenc;
			BoolSelectColM4a  &= PrgInit.bool_faac;
			BoolSelectColAac  &= PrgInit.bool_aacplusenc;
			BoolSelectColMpc  &= PrgInit.bool_mppenc;
			BoolSelectColApe  &= PrgInit.bool_ape;
			BoolSelectColWavP &= PrgInit.bool_wavpack;
		}
	}

	// utils_put_error_statusbar (FALSE);

	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_cd.Adr_Tree_Model, &iter, COLUMN_CDAUDIO_TRACK_NUM, &Str, -1);
	NumTrack = (gint)g_strtod (Str, NULL);
	NumTrack --;
	if ((List = g_list_nth (EnteteCD.GList_Audio_cd, NumTrack)) == NULL) return (FALSE);
	Audio = (CD_AUDIO *)List->data;
	if (!Audio) return (FALSE);
	
	if (bool_click_droit) {
		
		if (BoolSelectColNormalise) {
			/* PopUp menu normalise CD */
			popup_normalise_cd ();
			return (FALSE);
		}
		else if (!BoolSelectColNormalise) {
		
			/* PopUp menu CD */
			if (bool_click_droit &&
			    (BoolSelectColWav ||
			     BoolSelectColFlac ||
			     BoolSelectColApe ||
			     BoolSelectColWavP ||
			     BoolSelectColOgg ||
			     BoolSelectColM4a ||
			     BoolSelectColAac ||
			     BoolSelectColMpc ||
			     BoolSelectColMp3)) {
				TYPE_FILE_IS TypeFileIs = FILE_IS_NONE;
			     	
				if (BoolSelectColWav)		TypeFileIs = FILE_IS_WAV;
				else if (BoolSelectColFlac)	TypeFileIs = FILE_IS_FLAC;
				else if (BoolSelectColApe)	TypeFileIs = FILE_IS_APE;
				else if (BoolSelectColWavP)	TypeFileIs = FILE_IS_WAVPACK;
				else if (BoolSelectColOgg)	TypeFileIs = FILE_IS_OGG;
				else if (BoolSelectColM4a)	TypeFileIs = FILE_IS_M4A;
				else if (BoolSelectColAac)	TypeFileIs = FILE_IS_AAC;
				else if (BoolSelectColMpc)	TypeFileIs = FILE_IS_MPC;
				else if (BoolSelectColMp3)	TypeFileIs = FILE_IS_MP3;
			     	
				popup_cd (Audio, TypeFileIs);
				return (FALSE);
			}
		}
	}
	
	/* Un simle click change l etat de normalisaion */
	else if (BoolSelectColNormalise) {

		if (PrgInit.bool_normalize == FALSE) {
			// utils_set_prg_not_found (prginit_get_name (NMR_normalize), PrgInit.bool_normalize);
		}

		Audio->EtatNormalise = (Audio->EtatNormalise == TRUE) ? FALSE : TRUE;
		Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_TO_NORMALISE);
		gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FILE_NORMALIZE, Pixbuf, -1);
		cdaudio_set_flag_buttons ();
	}
	
	/* traitements */
	
	else if (BoolSelectColPlay) {
		
		if (TRUE == mplayer_is_used ()) {

			wind_info_init (
				WindMain,
				_("MPLAYER already in action"),
				_("MPLAYER is already in use  !!"),
				  "");
		}
		else {
			gchar *piste = NULL;

			gtk_tree_model_get (var_cd.Adr_Tree_Model, &iter, COLUMN_CDAUDIO_TRACK_NUM, &piste, -1);
			if (piste[0] == '0') {
				piste [0] = piste [1];
				piste [1] = '\0';
			}

			if (Audio->EtatPlay == CD_ETAT_PLAY_ATTENTE) {
				GList	*list = NULL;
				gchar	*StrTitle = NULL;
				
				StrTitle = g_strdup_printf("CD Read  -  Track %s", piste);
				// APPEL DE mplayer_init AVANT WinReader_open ()
				mplayer_init (LIST_MPLAYER_FROM_CD, 0.0, 0, WinReader_close, WinReader_set_value, cdaudio_set_icone_stop, WinReader_is_close);
				// WinReader_open (StrTitle);
				wind_reader_init( WindMain );
				g_free (StrTitle);
				StrTitle = NULL;
				WinReader_set_pause ();
				
				list = g_list_append (list, g_strdup_printf ("cdda://%s", piste));
				list = g_list_append (list, g_strdup ("-cdrom-device"));
				list = g_list_append (list, g_strdup_printf ("%s", EnteteCD.NameCD_Device));
				mplayer_set_list (list);
				list = libutils_remove_glist (list);
				
				// AFFICHE L ICONE DE PLAY
				cdaudio_set_etat_music_pixbuf (Audio, CD_ETAT_PLAY);
			}
		}
	}
	else if (BoolSelectColWavP ||
	         BoolSelectColApe  ||
	         BoolSelectColMpc  ||
	         BoolSelectColFlac ||
	         BoolSelectColWav  ||
	         BoolSelectColMp3  ||
	         BoolSelectColOgg  ||
	         BoolSelectColM4a  ||
	         BoolSelectColAac) {

		if (bool_key_Release && bool_click_gauche) {

			if (BoolSelectColFlac) {
				Audio->EtatSelection_Flac = cdaudio_get_next_flag (FILE_IS_FLAC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_FLAC);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_FLAC,
							Pixbuf,
							-1);
			}
			else if (BoolSelectColWav) {
				Audio->EtatSelection_Wav = cdaudio_get_next_flag (FILE_IS_WAV, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAV);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_WAV,
							Pixbuf,
							-1);
			}
			else if (BoolSelectColMp3) {
				Audio->EtatSelection_Mp3 = cdaudio_get_next_flag (FILE_IS_MP3, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MP3);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_MP3,
							Pixbuf,
							-1);
			}
			else if (BoolSelectColOgg) {
				Audio->EtatSelection_Ogg = cdaudio_get_next_flag (FILE_IS_OGG, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_OGG);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_OGG,
							Pixbuf,
							-1);
			}
			else if (BoolSelectColM4a) {
				Audio->EtatSelection_M4a = cdaudio_get_next_flag (FILE_IS_M4A, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_M4A);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_M4A,
							Pixbuf,
							-1);
			}
			else if (BoolSelectColAac) {
				Audio->EtatSelection_Aac = cdaudio_get_next_flag (FILE_IS_AAC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_AAC);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_AAC,
							Pixbuf,
							-1);
			}
			else if (BoolSelectColMpc) {
				Audio->EtatSelection_Mpc = cdaudio_get_next_flag (FILE_IS_MPC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MPC);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_MPC,
							Pixbuf,
							-1);
			}
			else if (BoolSelectColApe) {
				Audio->EtatSelection_Ape = cdaudio_get_next_flag (FILE_IS_APE, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_APE);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_APE,
							Pixbuf,
							-1);
			}
			else if (BoolSelectColWavP) {
				Audio->EtatSelection_WavPack = cdaudio_get_next_flag (FILE_IS_WAVPACK, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAVPACK);
				gtk_list_store_set (var_cd.Adr_List_Store,
							&iter,
							COLUMN_CDAUDIO_WAVPACK,
							Pixbuf,
							-1);
			}
			
			cdaudio_set_flag_buttons ();
			gtk_tree_selection_select_iter (var_cd.Adr_Line_Selected, &iter);
		}
		else if (bool_key_Control && bool_click_gauche) {
			
			TYPE_FILE_IS TypeFileIs = FILE_IS_NONE;
			
			if (BoolSelectColFlac)		TypeFileIs = FILE_IS_FLAC;
			else if (BoolSelectColWav)	TypeFileIs = FILE_IS_WAV;
			else if (BoolSelectColMp3)	TypeFileIs = FILE_IS_MP3;
			else if (BoolSelectColOgg)	TypeFileIs = FILE_IS_OGG;
			else if (BoolSelectColM4a)	TypeFileIs = FILE_IS_M4A;
			else if (BoolSelectColAac)	TypeFileIs = FILE_IS_AAC;
			else if (BoolSelectColMpc)	TypeFileIs = FILE_IS_MPC;
			else if (BoolSelectColApe)	TypeFileIs = FILE_IS_APE;
			else if (BoolSelectColWavP)	TypeFileIs = FILE_IS_WAVPACK;
			else				TypeFileIs = FILE_IS_NONE;
			
			if (TypeFileIs != FILE_IS_NONE) {
				cdaudio_change_all_flags_extract_verticaly (TypeFileIs);
				cdaudio_set_flag_buttons ();
				gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
				gtk_tree_selection_select_iter (var_cd.Adr_Line_Selected, &iter);
			}
		}
		else if (bool_key_Shift && bool_click_gauche) {

			Audio->EtatSelection_Flac = cdaudio_get_next_flag (FILE_IS_FLAC, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_FLAC);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_FLAC,
						Pixbuf,
						-1);

			Audio->EtatSelection_Wav = cdaudio_get_next_flag (FILE_IS_WAV, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAV);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_WAV,
						Pixbuf,
						-1);

			Audio->EtatSelection_Mp3 = cdaudio_get_next_flag (FILE_IS_MP3, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MP3);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_MP3,
						Pixbuf,
						-1);

			Audio->EtatSelection_Ogg = cdaudio_get_next_flag (FILE_IS_OGG, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_OGG);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_OGG,
						Pixbuf,
						-1);

			Audio->EtatSelection_M4a = cdaudio_get_next_flag (FILE_IS_M4A, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_M4A);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_M4A,
						Pixbuf,
						-1);

			Audio->EtatSelection_Aac = cdaudio_get_next_flag (FILE_IS_AAC, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_AAC);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_AAC,
						Pixbuf,
						-1);

			Audio->EtatSelection_Mpc = cdaudio_get_next_flag (FILE_IS_MPC, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MPC);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_MPC,
						Pixbuf,
						-1);

			Audio->EtatSelection_Ape = cdaudio_get_next_flag (FILE_IS_APE, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_APE);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_APE,
						Pixbuf,
						-1);

			Audio->EtatSelection_WavPack = cdaudio_get_next_flag (FILE_IS_WAVPACK, TRUE, NumTrack);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAVPACK);
			gtk_list_store_set (var_cd.Adr_List_Store,
						&iter,
						COLUMN_CDAUDIO_WAVPACK,
						Pixbuf,
						-1);
			
			cdaudio_set_flag_buttons ();
			gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
			gtk_tree_selection_select_iter (var_cd.Adr_Line_Selected, &iter);
		}
	}

	return (FALSE);
}
/* Edition du contenu d'un champs. La modification sera repercutee dans le Glist 'EnteteCD.GList_Audio_cd'
*  --
*  entree :
*      GtkCellRendererText *cell        :
*      const gchar         *path_string :
*      const gchar         *new_text    :
*      gpointer             data        :
*  retour : -
*/
/*
static void cdaudio_cell_edited_cd (GtkCellRendererText *cell,
					const gchar         *path_string,
					const gchar         *new_text,
					gpointer             data)
{
	GtkTreeModel *model = (GtkTreeModel *)data;
	GtkTreePath  *path = gtk_tree_path_new_from_string (path_string);
	GtkTreeIter   iter;
	gint          column = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cell), "column"));

	gtk_tree_model_get_iter (model, &iter, path);

	if (column == COLUMN_CDAUDIO_TRACK_NAME) {
		gint   Num_cell;
		gchar *old_text;
		GList *List = NULL;
		AUDIO *Audio = NULL;

		gtk_tree_model_get (model, &iter, COLUMN_CDAUDIO_TRACK_NAME, &old_text, -1);
		g_free (old_text);
		old_text = NULL;

		Num_cell = gtk_tree_path_get_indices (path)[0];
		g_free (g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name);
		g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name = NULL;
		g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name = g_strdup (new_text);

		List = g_list_nth (EnteteCD.GList_Audio_cd, Num_cell);
		Audio = List->data;
		if (Audio) {
			if (Audio->NameSong) {
				g_free (Audio->NameSong);
				Audio->NameSong = NULL;
			}
			Audio->NameSong = g_strdup (new_text);
		}

		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_CDAUDIO_TRACK_NAME,
					g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name,
					-1);
	}
}
*/
// 
// 
CD_AUDIO *cdaudio_get_line_selected (void)
{
	GtkTreeModel     *model = NULL;
	GList            *List = NULL;
	GtkTreePath      *path;
	CD_AUDIO            *Audio = NULL;
	gint              NumStruct;
	GList            *GList_cd = NULL;

	if (!var_cd.Adr_TreeView) return (NULL);
	if (!var_cd.Adr_Line_Selected) return (NULL);
	if (!EnteteCD.GList_Audio_cd) return (NULL);

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_cd.Adr_TreeView));
	List = gtk_tree_selection_get_selected_rows (var_cd.Adr_Line_Selected, &model);
	
	/* Bug resolu d'apres une description de @plikplok
	 * plikplok@hotmail.com :-> David
	 */
	if (List == NULL) {
		if ((List = g_list_nth (EnteteCD.GList_Audio_cd, 0))) {
			if ((Audio = (CD_AUDIO *)List->data)) {
				return ((CD_AUDIO *)Audio);
			}
		}
	}

	List = g_list_first (List);
	while (List) {
		path = List->data;
		if (path) {
			/* prend le numero de la structure
			*/
			NumStruct = gtk_tree_path_get_indices (path)[0];

			GList_cd = g_list_nth (EnteteCD.GList_Audio_cd, NumStruct);
			Audio = GList_cd->data;
			return ((CD_AUDIO *)Audio);
		}
		List = g_list_next (List);
	}
	return ((CD_AUDIO *)NULL);
}
// 
// 
CD_AUDIO *cdaudio_get_line_selected_for_extract (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;

	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if ((Audio = (CD_AUDIO *)List->data) != NULL) {

			if (Audio->EtatSelection_Mp3 >= CD_ETAT_SELECT)		return ((CD_AUDIO *)Audio);
			if (Audio->EtatSelection_Ogg >= CD_ETAT_SELECT)		return ((CD_AUDIO *)Audio);
			if (Audio->EtatSelection_Wav >= CD_ETAT_SELECT)		return ((CD_AUDIO *)Audio);
			if (Audio->EtatSelection_Flac >= CD_ETAT_SELECT)	return ((CD_AUDIO *)Audio);
			if (Audio->EtatSelection_M4a >= CD_ETAT_SELECT)		return ((CD_AUDIO *)Audio);
			if (Audio->EtatSelection_Aac >= CD_ETAT_SELECT)		return ((CD_AUDIO *)Audio);
			if (Audio->EtatSelection_Mpc >= CD_ETAT_SELECT)		return ((CD_AUDIO *)Audio);
			if (Audio->EtatSelection_Ape >= CD_ETAT_SELECT)		return ((CD_AUDIO *)Audio);
			if (Audio->EtatSelection_WavPack >= CD_ETAT_SELECT)	return ((CD_AUDIO *)Audio);
		}
		List = g_list_next(List);
	}
	
	return ((CD_AUDIO *)cdaudio_get_line_selected ());
}
/* Enregistrement de la nouvelle ligne en selection dans la variable 'var_cd.Adr_Line_Selected'
*  --
*  entree :
*      GtkTreeSelection *selection :
*      gpointer          data      :
*  retour : -
*/
void cdaudio_changed_selection_row (GtkTreeSelection *selection, gpointer data)
{
	var_cd.Adr_Line_Selected = selection;
	
	// PRINT_FUNC_LF();

	cdexpander_set_sensitive_notebook ();
	cdexpander_set_entry_tag_titre_album ();
	cdexpander_set_entry_tag_nom_artiste ();
	cdexpander_set_spinbutton_tag_annee ();
	cdexpander_set_entry_tag_titre_chanson ();
	cdexpander_set_new_genre ();
	cdexpander_set_entry_tag_commentaire ();
	cdexpander_set_spinbutton_tag_piste ();
	cdexpander_set_entry_tag_titre_fichier_m3u ();
}
// 
// 
gboolean cdaudio_key_press_event (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	return (TRUE);
}
// 
// 
void cdaudio_set_help( CD_AUDIO *Audio, TYPE_FILE_IS p_type_file_is )
{
	ETAT_SELECTION_CD   etat = CD_ETAT_ATTENTE;
	gint                NumTrack = Audio->Num_Track -1;
	
	if (p_type_file_is == FILE_IS_FLAC)		etat = Audio->EtatSelection_Flac    = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_WAV)		etat = Audio->EtatSelection_Wav     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_OGG)		etat = Audio->EtatSelection_Ogg     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_MP3)		etat = Audio->EtatSelection_Mp3     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_M4A)	        etat = Audio->EtatSelection_M4a     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_AAC)	        etat = Audio->EtatSelection_Aac     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_MPC)		etat = Audio->EtatSelection_Mpc     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_APE)		etat = Audio->EtatSelection_Ape     = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	else if (p_type_file_is == FILE_IS_WAVPACK)	etat = Audio->EtatSelection_WavPack = cdaudio_get_next_flag (p_type_file_is, FALSE, NumTrack);
	
	// switch (p_EtatSelectionCd) {
	switch ( etat ) {
	case CD_ETAT_NONE :
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Waiting for selection."));
		break;
	case CD_ETAT_PRG_ABSENT :
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("The external ripping program is missing."));
		break;
	case CD_ETAT_ATTENTE :
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Waiting for selection."));
		break;
	case CD_ETAT_ATTENTE_EXIST :
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Waiting for Selection. File exists.."));
		break;
	case CD_ETAT_SELECT :
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Selected files."));
		break;
	case CD_ETAT_SELECT_EXPERT :
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Selection - expert mode."));
		break;
	case CD_ETAT_SELECT_EXIST :
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Selected files. File exists."));
		break;
	case CD_ETAT_SELECT_EXPERT_EXIST :
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Selection - expert mode. File exists."));
		break;
	}
}
// 
// 
void cdaudio_set_normalise (gboolean p_EtatNormalise)
{
	if (FALSE == p_EtatNormalise) {
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Possibilities for individual adaptation or maximum collection volume"));
	}
	else {
		switch (gtk_combo_box_get_active (GTK_COMBO_BOX (var_cd.Adr_combobox_normalise_cd))) {
		case 0 : // case CD_NORM_PEAK :
			StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / INDIVIDUAL maximum volume adjustment"));
			break;
		case 1 : // case CD_NORM_PEAK_WAITING :
			StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / COLLECTIVE maximum volume adjustment"));
			break;
		}
	}
}
// 
// 
gboolean cdaudio_event (GtkWidget *treeview, GdkEvent *event, gpointer user_data)
{
	gint                x, y;
	GdkModifierType     state;
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)user_data;
	gint                Pos_X = 0, Pos_Y = 0;
	gint                NumTrack;
	gchar              *StrNumTrack;
	CD_AUDIO           *Audio = NULL;
	GList              *GListCD = NULL;

	GtkTreeViewColumn  *ColumnDum;
	gint		    i;

	gboolean            BoolSelectColPlay = FALSE;
	gboolean            BoolSelectColWav = FALSE;
	gboolean            BoolSelectColFlac = FALSE;
	gboolean            BoolSelectColApe = FALSE;
	gboolean            BoolSelectColWavP = FALSE;
	gboolean            BoolSelectColOgg = FALSE;
	gboolean            BoolSelectColM4a = FALSE;
	gboolean            BoolSelectColAac = FALSE;
	gboolean            BoolSelectColMpc = FALSE;
	gboolean            BoolSelectColMp3 = FALSE;
	gboolean            BoolSelectColTime = FALSE;
	gboolean            BoolSelectColNormalise = FALSE;
	gboolean            BoolSelectColNom = FALSE;

	if (keys.keyval == GDK_KEY_PRESS) {
		return (FALSE);
	}

	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &x, &y, &state );
	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &x, &y, &state);
	if (!gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					   x, y,
					   &path, &column, &Pos_X, &Pos_Y)) {
		// StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, "" );
		return (FALSE);
	}

	/* Recuperation de la structure */
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_cd.Adr_Tree_Model, &iter, COLUMN_CDAUDIO_TRACK_NUM, &StrNumTrack, -1);
	NumTrack = atoi (StrNumTrack);
	GListCD = g_list_nth (EnteteCD.GList_Audio_cd, NumTrack-1);
	if (NULL == (Audio = GListCD->data)) return (FALSE);

	// SORTIE SI LE WIDGET SOUS LE CURSEUR NE CORRESPOND PAS AU TREEVIEW
	if (gtk_get_event_widget (event) != var_cd.Adr_TreeView) {
		// StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, "" );
		return (FALSE);
	}
	
	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_CDAUDIO_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			/* g_print ("\tNUM IS: %d\n", i); */
			switch ( i ) {
			case NUM_TREE_CDAUDIO_PLAY :		BoolSelectColPlay		= TRUE;	break;
			case NUM_TREE_CDAUDIO_WAV :		BoolSelectColWav		= TRUE;	break;
			case NUM_TREE_CDAUDIO_FLAC :		BoolSelectColFlac		= TRUE;	break;
			case NUM_TREE_CDAUDIO_APE :		BoolSelectColApe		= TRUE;	break;
			case NUM_TREE_CDAUDIO_WAVP :		BoolSelectColWavP		= TRUE;	break;
			case NUM_TREE_CDAUDIO_OGG :		BoolSelectColOgg		= TRUE;	break;
			case NUM_TREE_CDAUDIO_M4A :		BoolSelectColM4a		= TRUE;	break;
			case NUM_TREE_CDAUDIO_AAC :		BoolSelectColAac		= TRUE;	break;
			case NUM_TREE_CDAUDIO_MPC :		BoolSelectColMpc		= TRUE;	break;
			case NUM_TREE_CDAUDIO_MP3 :		BoolSelectColMp3		= TRUE;	break;
			case NUM_TREE_CDAUDIO_NUM :							break;
			case NUM_TREE_CDAUDIO_TIME :		BoolSelectColTime		= TRUE;	break;
			case NUM_TREE_CDAUDIO_NORMALISE :	BoolSelectColNormalise		= TRUE;	break;
			case NUM_TREE_CDAUDIO_NOM :		BoolSelectColNom		= TRUE;	break;
			
			default: return (FALSE);
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}
	if( BoolSelectColTime );
	
	if (BoolSelectColNormalise == TRUE) {
		cdaudio_set_normalise (Audio->EtatNormalise);
	}
	
	else if (BoolSelectColNom == TRUE) {

		gchar *path = NULL;
		gchar *pathdest = NULL;

		pathdest = cdaudio_get_result_destination ();
		path = g_strdup_printf ("%s%s", pathdest, Audio->NameSong);
		g_free (pathdest);
		pathdest = NULL;
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, path );
		g_free (path);
		path = NULL;
	}
	
	else if (BoolSelectColPlay == TRUE ||
		 BoolSelectColFlac == TRUE ||
		 BoolSelectColWav == TRUE ||
		 BoolSelectColMp3 == TRUE ||
		 BoolSelectColOgg == TRUE ||
		 BoolSelectColM4a == TRUE ||
		 BoolSelectColAac == TRUE ||
		 BoolSelectColMpc == TRUE ||
		 BoolSelectColApe == TRUE ||
		 BoolSelectColWavP == TRUE) {

		/* position du curseur a l'instant du click */
		if (Pos_X < 18 || Pos_X > 30 || Pos_Y < 8 || Pos_Y > 20) {
			return (FALSE);
		}

		if (BoolSelectColPlay == TRUE) {
			StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, " " );
		}
		else if (BoolSelectColFlac == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_FLAC );
		}
		else if (BoolSelectColWav == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_WAV );
		}
		else if (BoolSelectColMp3 == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_MP3 );
		}
		else if (BoolSelectColOgg == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_OGG );
		}
		else if (BoolSelectColM4a == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_M4A );
		}
		else if (BoolSelectColAac == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_AAC );
		}
		else if (BoolSelectColMpc == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_MPC );
		}
		else if (BoolSelectColApe == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_APE );
		}
		else if (BoolSelectColWavP == TRUE) {
			cdaudio_set_help (Audio, FILE_IS_WAVPACK );
		}
	}
	else {
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, " " );
	}
	
	StatusBar_puts();
	
	return (FALSE);
}
// 
// 
void cdaudio_selected_column (GtkTreeViewColumn *treeviewcolumn, gpointer user_data)
{
	gint		NumColonne = GPOINTER_TO_INT(user_data);
	TYPE_FILE_IS	TypeFileIs = FILE_IS_NONE;
	
	// PRINT_FUNC_LF();
	
	switch (NumColonne) {
	case 0 : TypeFileIs = FILE_IS_WAV;	break;
	case 1 : TypeFileIs = FILE_IS_FLAC;	break;
	case 2 : TypeFileIs = FILE_IS_APE;	break;
	case 3 : TypeFileIs = FILE_IS_WAVPACK;	break;
	case 4 : TypeFileIs = FILE_IS_OGG;	break;
	case 5 : TypeFileIs = FILE_IS_M4A;	break;
	case 6 : TypeFileIs = FILE_IS_AAC;	break;
	case 7 : TypeFileIs = FILE_IS_MPC;	break;
	case 8 : TypeFileIs = FILE_IS_MP3;	break;
	case 9 : TypeFileIs = FILE_TO_NORMALISE;break;
	}

	cdaudio_change_all_flags_extract_verticaly (TypeFileIs);
	cdaudio_set_flag_buttons ();
}
/* Creation des champs du 'treeview'
*  --
*  entree :
*      GtkTreeView *treeview :
*  retour : -
*/
static void cdaudio_add_columns_scrolledwindow_cd (GtkTreeView *treeview)
{
	GtkTreeModel      *model = gtk_tree_view_get_model (treeview);
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;

	// SIGNAL : 'event'
	g_signal_connect(G_OBJECT(treeview),
			 "event",
                    	 (GCallback) cdaudio_event,
			 model);

	// SIGNAL 'key-press-event'
	g_signal_connect(G_OBJECT(treeview),
			 "key-press-event",
                    	 (GCallback) cdaudio_key_press_event,
			 model);

	// SIGNAL 'button-press-event'
	g_signal_connect(G_OBJECT(treeview),
			 "button-press-event",
                    	 (GCallback) cdaudio_event_click_mouse,
			 model);

	// Ligne actuellement selectionnee
	var_cd.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	g_signal_connect(G_OBJECT(var_cd.Adr_Line_Selected),
			 "changed",
                   	 G_CALLBACK(cdaudio_changed_selection_row),
                   	 "1");
                   	/* model);*/

	// COLUMN_CDAUDIO_PLAY
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Play =
	column = gtk_tree_view_column_new_with_attributes (
							"Play",
							renderer,
							"pixbuf", COLUMN_CDAUDIO_PLAY,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);

	/*
	@Patachon, @Shankarius, @Dzef
	
	Je plaide aussi pour la réunion logique des formats lossless puis des
	autres. Je propose : 

	    lossless     |       perte      | destruction forte
	<================>
	                 <============================>
					    <=========>
	Wav | Flac | Ape | WPack | Ogg | M4a | Mpc | Mp3

	Je conviens qu'il n'est pas facile de classer, car certains formats sont
	capables d'être utilisés en lossless ou en moindre qualité. Ce n'est
	donc pas vraiment linéaire. Mais c'est une piste...
	
	Wav
	Flac
	Ape
	WPack
	Ogg
	M4a
	Mpc
	Mp3
	*/
	
	// COLUMN_CDAUDIO_WAV
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Wav =
	column = gtk_tree_view_column_new_with_attributes (
							"Wav",
							renderer,
							"pixbuf", COLUMN_CDAUDIO_WAV,
							/*"text", COLUMN_CDAUDIO_WAV,*/
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(0));

	// COLUMN_CDAUDIO_FLAC
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Flac =
	column = gtk_tree_view_column_new_with_attributes (
							"Flac",
						 	renderer,
							"pixbuf", COLUMN_CDAUDIO_FLAC,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(1));

	// COLUMN_CDAUDIO_APE
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Ape =
	column = gtk_tree_view_column_new_with_attributes (
							"Ape",
							renderer,
							"pixbuf", COLUMN_CDAUDIO_APE,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(2));

	// COLUMN_CDAUDIO_WAVPACK
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Wavpack =
	column = gtk_tree_view_column_new_with_attributes (
							"WavP",
							renderer,
							"pixbuf", COLUMN_CDAUDIO_WAVPACK,
						 	NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(3));

	// COLUMN_CDAUDIO_OGG
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Ogg =
	column = gtk_tree_view_column_new_with_attributes (
							"Ogg",
							renderer,
							"pixbuf", COLUMN_CDAUDIO_OGG,
							/*"text", COLUMN_CDAUDIO_OGG,*/
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(4));

	// COLUMN_CDAUDIO_M4A
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_M4a =
	column = gtk_tree_view_column_new_with_attributes (
							"M4a",
							renderer,
							"pixbuf", COLUMN_CDAUDIO_M4A,
							/*"text", COLUMN_CDAUDIO_OGG,*/
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(5));

	// COLUMN_CDAUDIO_AAC
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Aac =
	column = gtk_tree_view_column_new_with_attributes (
							"Aac",
							renderer,
							"pixbuf", COLUMN_CDAUDIO_AAC,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(6));
	
	// COLUMN_CDAUDIO_MPC
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Mpc =
	column = gtk_tree_view_column_new_with_attributes (
							"Mpc",
							renderer,
							"pixbuf", COLUMN_CDAUDIO_MPC,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(7));

	// COLUMN_CDAUDIO_MP3
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Mp3 =
	column = gtk_tree_view_column_new_with_attributes (
							"Mp3",
							renderer,
						  	"pixbuf", COLUMN_CDAUDIO_MP3,
							/*"text", COLUMN_CDAUDIO_MP3,*/
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(8));

	// COLUMN_CDAUDIO_TRACK_NUM
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Num =
	column = gtk_tree_view_column_new_with_attributes (
							"Num",
							renderer,
							"text", COLUMN_CDAUDIO_TRACK_NUM,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);

	// COLUMN_CDAUDIO_TRACK_TIME
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Time =
	column = gtk_tree_view_column_new_with_attributes (
							"Time",
							renderer,
							"text", COLUMN_CDAUDIO_TRACK_TIME,
						 	NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);

	// COLUMN_CDAUDIO_FILE_NORMALIZE
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_cd.Adr_Column_Normalise =
	column = gtk_tree_view_column_new_with_attributes (
							_("Normalise"),
							renderer,
							"pixbuf", COLUMN_CDAUDIO_FILE_NORMALIZE,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 100);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (cdaudio_selected_column),
			GINT_TO_POINTER(9));

	// COLUMN_CDAUDIO_TRACK_NAME
	var_cd.Renderer =
	renderer = gtk_cell_renderer_text_new ();
	/*
	g_signal_connect (renderer, "edited", G_CALLBACK (cdaudio_cell_edited_cd), model);
	*/
	g_object_set (renderer, "xalign", 0.5, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint *)COLUMN_CDAUDIO_TRACK_NAME);
	var_cd.Adr_Column_Nom =
	column = gtk_tree_view_column_new_with_attributes (
							_("Name"),
							renderer,
							"markup", COLUMN_CDAUDIO_TRACK_NAME,
						     //   "background-gdk", COLUMN_CDAUDIO_COLOR,
							/* "editable", CF_COLUMN_EDITABLE */
						 	NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
}
/* Creation du context
*  --
*  entree :
*      GtkWidget *widget :
*  retour : -
*/
void on_scrolledwindow_cd_realize (GtkWidget *widget, gpointer user_data)
{
	GtkListStore *store;
	GtkTreeModel *model;
	GtkWidget    *treeview;

	var_cd.Adr_scroll = widget;

	var_cd.Pixbuf_Coche                 = libutils_init_pixbufs ("coche.png");
	var_cd.Pixbuf_Coche_exist           = libutils_init_pixbufs ("coche_exist.png");
	var_cd.Pixbuf_Selected              = libutils_init_pixbufs ("selected.png");
	var_cd.Pixbuf_Selected_exist        = libutils_init_pixbufs ("selected_exist.png");
	var_cd.Pixbuf_Selected_expert       = libutils_init_pixbufs ("selected_expert.png");
	var_cd.Pixbuf_Selected_expert_exist = libutils_init_pixbufs ("selected_expert_exist.png");
	var_cd.Pixbuf_CdPlay                = libutils_init_pixbufs ("sol.png");
	var_cd.Pixbuf_CdStop                = libutils_init_pixbufs ("no_play.png");
	var_cd.Pixbuf_Normalize             = libutils_init_pixbufs ("norm_rpg_wait.png");
	var_cd.Pixbuf_Normalize_Coche       = libutils_init_pixbufs ("normalize2.png");
	var_cd.Pixbuf_NotInstall            = libutils_init_pixbufs ("not_install.png");

	Articles_cd = g_array_sized_new (FALSE, FALSE, sizeof (ARTICLES_CD), 1);

	var_cd.Adr_List_Store = store =
	gtk_list_store_new (	CD_NUM_COLUMNS,		// TOTAL NUMBER
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_PLAY
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_FLAC
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_WAV
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_MP3
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_OGG
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_M4A
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_AAC
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_MPC
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_APE
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_WAVPACK
				G_TYPE_STRING,		// COLUMN_CDAUDIO_TRACK_NUM
				G_TYPE_STRING,		// COLUMN_CDAUDIO_TRACK_TIME
				GDK_TYPE_PIXBUF,	// COLUMN_CDAUDIO_FILE_NORMALIZE
				G_TYPE_STRING		// COLUMN_CDAUDIO_TRACK_NAME
				//GDK_TYPE_COLOR		// COLUMN_CDAUDIO_COLOR
			   );

	var_cd.Adr_Tree_Model = model = GTK_TREE_MODEL (store);

	/* creation de tree view
	 */
	var_cd.Adr_TreeView =
	treeview = gtk_tree_view_new_with_model (model);
	// gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);
	g_object_unref (model);
	gtk_container_add (GTK_CONTAINER (widget), treeview);
	cdaudio_add_columns_scrolledwindow_cd (GTK_TREE_VIEW (treeview));

	gtk_widget_show_all (widget);
}
// 
// 
gboolean bool_entree_cd_verif_car = TRUE;
// 
// 
void cdaudio_set_titre_chanson (void)
{
	gint		 Num_cell = 0;
	gboolean	 valid;
	GtkTreeIter	 iter;
	GList		*List = NULL;
	// gchar		*ptr_template = NULL;
	gchar		*ptr = NULL;
	CD_AUDIO	*Audio = NULL;
	gchar		*StrLine = NULL;

	if (var_cd.Adr_entry_new_titre_cdaudio == NULL) return;
	if (EnteteCD.GList_Audio_cd == NULL) return;
	if (bool_entree_cd_verif_car == FALSE) return;
	// ptr_template = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_new_titre_cdaudio));

	// 
	var_cd.Bool_create_file_m3u = FALSE;
		
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		
		List = g_list_nth (EnteteCD.GList_Audio_cd, Num_cell);
		if ((Audio = (CD_AUDIO *)List->data)) {

			// POINTEUR SUR LIGNE PARSEE
			StrLine = Parse_get_line (PARSE_TYPE_TITLE_CD, Num_cell);
			
			// free 'Track_Name'
			g_free (g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name);
			g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name = NULL;

			// SUPPRESSION DES ESPACES DE DEBUT
			ptr = StrLine;
			while (*ptr == ' ') ptr ++;
			strcpy (StrLine, ptr);
			// SUPPRESSION DES ESPACES DE FIN
			ptr = StrLine;
			while (*ptr) ptr ++;
			ptr --;
			while (*ptr == ' ') ptr --;
			ptr ++;
			*ptr = '\0';
			
			if (0 == strlen (StrLine)) {
				g_free (StrLine);	StrLine = NULL;
				StrLine = g_strdup (Audio->NameSong);
			}
			
			g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name = g_strdup (StrLine);

			if (Audio->NameSong) {
				g_free (Audio->NameSong);
				Audio->NameSong = NULL;
			}
			Audio->NameSong = g_strdup (StrLine);
			
			// --
			// Thu, 14 Jul 2011 13:04:19 +0200
			// Son nom : LeDub
			// Bonjour, 
			// Je viens de trouver un bug étrange dans la version Xcfa 4.1.0-1 d'une Debian testing. Mettre, dans la partie "Gestion des tags...",
			// un "&" dans le titre d'une piste fait que, dans la sélection des pistes à compresser le titre devienne celui de la piste précédente.
			// Si la deuxième piste doit contenir un "&" alors cette piste sera affichée à l'identique de la première piste, si c'est la première piste
			// alors l'affichage est celui de la dernière piste du disque. 
			// Voilà j'espère avoir été clair. 
			// Je reste à votre entière disposition pour de plus amples détails. 
			// Bonne continuation. 
			// LeDub
			// 
			g_free (StrLine);	StrLine = NULL;
			StrLine = utf8_eperluette_name( Audio->NameSong );

			// AFFICHAGE
			gtk_list_store_set (GTK_LIST_STORE (var_cd.Adr_Tree_Model),
						&iter,
						COLUMN_CDAUDIO_TRACK_NAME,
						// g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name,
						StrLine,
						// COLUMN_CDAUDIO_COLOR,
						// &YellowColor,
						-1);
			
			g_free (StrLine);	StrLine = NULL;
		}
		// suivant
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
		Num_cell ++;
	}

	// MISE A JOUR NOM DOSSIER STOCKAGE CD
	OptionsCd_set_entry_and_label();
}
// 
// 
gchar *cdaudio_get_titre_chanson (CD_AUDIO *Audio, gint Num_cell)
{
	gchar        *StrLine = NULL;

	if (EnteteCD.GList_Audio_cd == NULL) return ((gchar *)NULL);
	if (var_cd.Adr_entry_new_titre_cdaudio == NULL) return ((gchar *)NULL);

	var_cd.Bool_create_file_m3u = FALSE;
	
	StrLine = Parse_get_line (PARSE_TYPE_TITLE_CD, Num_cell);
	if (Audio->NameSong != NULL) {
		g_free (Audio->NameSong);
		Audio->NameSong = NULL;
	}
	Audio->NameSong = g_strdup (StrLine);
	return ((gchar* )Audio->NameSong);
}
/* Deallocation complete des lignes du context
*  --
*  entree : -
*  retour : -
*/
void cdaudio_deallocate_glist_context (void)
{
	gint          Num_cell;
	gboolean      valid;
	GtkTreeIter   iter;

	Num_cell = 0;
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		g_free (g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Num);
		g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Num = NULL;
		
		g_free (g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Time);
		g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Time = NULL;
		
		g_free (g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name);
		g_array_index (Articles_cd, ARTICLES_CD, Num_cell).Track_Name = NULL;

		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
		Num_cell ++;
	}
}
// 
// 
GdkPixbuf *cdaudio_get_pixbuf_play (CD_AUDIO *Audio)
{
	GdkPixbuf *PixBuf = NULL;

	if (PrgInit.bool_mplayer == FALSE) {
		PixBuf = var_cd.Pixbuf_NotInstall;
	}
	else {
		if (Audio->EtatPlay == CD_ETAT_PLAY)		PixBuf = var_cd.Pixbuf_CdPlay;
		if (Audio->EtatPlay == CD_ETAT_PLAY_ATTENTE)	PixBuf = var_cd.Pixbuf_CdStop;
	}
	return ((GdkPixbuf *)PixBuf);
}
/* Reaffichage complet du context depuis les datas du GList 'EnteteCD.GList_Audio_cd'
*  --
*  entree : -
*  retour : -
*/
void cdaudio_affiche_glist_audio (void)
{
	GList           *List = NULL;
	CD_AUDIO        *Audio = NULL;
	GtkAdjustment   *Adj = NULL;
	GtkTreeIter      iter;
	ARTICLES_CD      Foo;
	gint             NumTrack;
	
	cdaudio_deallocate_glist_context ();
	gtk_list_store_clear (var_cd.Adr_List_Store);

	/* Mise a jour du scroll vertical
	*/
	Adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (var_cd.Adr_scroll));
	/*
	g_print ("Adj->value %f\n", Adj->value);
	g_print ("Adj->lower %f\n", Adj->lower);
	g_print ("Adj->upper %f\n", Adj->upper);
	g_print ("Adj->step_increment %f\n", Adj->step_increment);
	g_print ("Adj->page_increment %f\n", Adj->page_increment);
	g_print ("Adj->page_size %f\n", Adj->page_size);
	*/
	gtk_adjustment_set_value (Adj, 0.0);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (var_cd.Adr_scroll), Adj);

	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if ((Audio = (CD_AUDIO *)List->data) != NULL) {

			NumTrack = (gint)g_strtod (Audio->Str_Track, NULL);
			NumTrack --;

			if (Audio->EtatPlay == CD_ETAT_PLAY)			Foo.PlayTrack  = var_cd.Pixbuf_CdPlay;
			else if (Audio->EtatPlay == CD_ETAT_PLAY_ATTENTE)	Foo.PlayTrack  = var_cd.Pixbuf_CdStop;
			
			Foo.PlayTrack         = cdaudio_get_pixbuf_play (Audio);
			Foo.Track_Name        = g_strdup ((gchar *)cdaudio_get_titre_chanson (Audio, NumTrack));
			Foo.Flac              = cdaudio_get_pixbuf_file (Audio, FILE_IS_FLAC);
			Foo.Wav               = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAV);
			Foo.Mp3               = cdaudio_get_pixbuf_file (Audio, FILE_IS_MP3);
			Foo.Ogg               = cdaudio_get_pixbuf_file (Audio, FILE_IS_OGG);
			Foo.M4a               = cdaudio_get_pixbuf_file (Audio, FILE_IS_M4A);
			Foo.Aac               = cdaudio_get_pixbuf_file (Audio, FILE_IS_AAC);
			Foo.Mpc               = cdaudio_get_pixbuf_file (Audio, FILE_IS_MPC);
			Foo.Ape               = cdaudio_get_pixbuf_file (Audio, FILE_IS_APE);
			Foo.WavPack           = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAVPACK);
			Foo.Track_Num         = g_strdup (Audio->Str_Track);
			Foo.Track_Time        = g_strdup (Audio->Duree);
			Foo.ColumnCDNormalize = cdaudio_get_pixbuf_file (Audio, FILE_TO_NORMALISE);

			g_array_append_vals (Articles_cd, &Foo, 1);

			gtk_list_store_append (var_cd.Adr_List_Store, &iter);

			gtk_list_store_set (
				var_cd.Adr_List_Store, &iter,
				COLUMN_CDAUDIO_PLAY,			Foo.PlayTrack,
				COLUMN_CDAUDIO_FLAC,			Foo.Flac,
				COLUMN_CDAUDIO_WAV,			Foo.Wav,
				COLUMN_CDAUDIO_MP3,			Foo.Mp3,
				COLUMN_CDAUDIO_OGG,			Foo.Ogg,
				COLUMN_CDAUDIO_M4A,			Foo.M4a,
				COLUMN_CDAUDIO_AAC,			Foo.Aac,
				COLUMN_CDAUDIO_MPC,			Foo.Mpc,
				COLUMN_CDAUDIO_APE,			Foo.Ape,
				COLUMN_CDAUDIO_WAVPACK,			Foo.WavPack,
				COLUMN_CDAUDIO_TRACK_NUM,		Foo.Track_Num,
				COLUMN_CDAUDIO_TRACK_TIME,		Foo.Track_Time,
				COLUMN_CDAUDIO_FILE_NORMALIZE,		Foo.ColumnCDNormalize,
				COLUMN_CDAUDIO_TRACK_NAME,		Foo.Track_Name,
				//COLUMN_CDAUDIO_COLOR,			&YellowColor,
				-1);
		}
		List = g_list_next(List);
	}

	cdaudio_set_titre_chanson ();

	/* select line number one
	*/
	if (EnteteCD.GList_Audio_cd) {
		gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
		gtk_tree_selection_select_iter (var_cd.Adr_Line_Selected, &iter);
	}
}
// 
// 
void cdaudio_update_glist (void)
{
	CD_AUDIO        *Audio = NULL;
	GdkPixbuf       *Pixbuf = NULL;
	GList           *List = NULL;
	gboolean         valid;
	GtkTreeIter      iter;
	gint             NumTrack = 0;
	
	if (!var_cd.Adr_Tree_Model) return;

	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, NumTrack);
		if (List && (Audio = (CD_AUDIO *)List->data)) {

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAV);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAV, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_FLAC);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FLAC, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_APE);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_APE, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAVPACK);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAVPACK, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_OGG);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_OGG, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_M4A);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_M4A, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_AAC);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_AAC, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MPC);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MPC, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MP3);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MP3, Pixbuf, -1);

			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_TO_NORMALISE);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FILE_NORMALIZE, Pixbuf, -1);			

			NumTrack ++;
		}
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
	}
}
/* Deallocation complete du GList 'EnteteCD.GList_Audio_cd'
*  --
*  entree : -
*  retour : -
*/
void cdaudio_deallocate_glist (void)
{
	CD_AUDIO	*Audio = NULL;
	GList		*List = NULL;
	gint		 NbList = 0;

	// Deallocate ENTETE
	if (NULL != EnteteCD.TitleCD) {
		g_free (EnteteCD.TitleCD);		EnteteCD.TitleCD = NULL;
	}
	if (NULL != EnteteCD.StrDureeCd) {
		g_free (EnteteCD.StrDureeCd);		EnteteCD.StrDureeCd = NULL;
	}
	if (NULL != EnteteCD.StrGenre) {
		g_free (EnteteCD.StrGenre);		EnteteCD.StrGenre = NULL;
	}
	if (NULL != EnteteCD.StrYear) {
		g_free (EnteteCD.StrYear);		EnteteCD.StrYear = NULL;
	}
	if (NULL != EnteteCD.Message) {
		g_free (EnteteCD.Message);		EnteteCD.Message = NULL;
	}
	// Deallocate ALLs struct Audio in GList_Audio_cd
	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if (NULL != (Audio = (CD_AUDIO *)List->data)) {

			g_free (Audio->Str_Track);		Audio->Str_Track = NULL;
			g_free (Audio->NameSong);		Audio->NameSong = NULL;
			g_free (Audio->Duree);			Audio->Duree = NULL;

			g_free (Audio->PathName_Dest_Wav);	Audio->PathName_Dest_Wav = NULL;
			g_free (Audio->PathName_Dest_Mp3);	Audio->PathName_Dest_Mp3 = NULL;
			g_free (Audio->PathName_Dest_Ogg);	Audio->PathName_Dest_Ogg = NULL;

			Audio->tags = (TAGS *)tags_remove (Audio->tags);

			g_free (Audio);
			Audio = NULL;
			List->data = NULL;
			
			NbList ++;
		}
		List = g_list_next(List);
	}

	// Deallocate GList_Audio_cd
	g_list_free (EnteteCD.GList_Audio_cd);
	EnteteCD.GList_Audio_cd = NULL;
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\tRemove: %d\n", NbList);
}
/* Cette fonction retourne l'etat 'TRUE' si au moins une demande d'extraction est activee
*  --
*  entree : -
*  retour :
*      FALSE : Aucune demande d'extraction
*      TRUE  : Au moins une demande d'extraction est activee
*/
gboolean cdaudio_get_bool_all_extract (void)
{
	GList *List = NULL;
	CD_AUDIO *Audio = NULL;

	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if ((Audio = (CD_AUDIO *)List->data) != NULL) {
			if (Audio->EtatSelection_Flac > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Wav > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Mp3 > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Ogg > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_M4a > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Aac > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Mpc > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Ape > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_WavPack > CD_ETAT_ATTENTE_EXIST)  return (TRUE);
		}
		List = g_list_next (List);
	}
	return (FALSE);
}
// 
// 
gboolean cdaudio_get_bool_wav_extract (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;

	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if ((Audio = (CD_AUDIO *)List->data) != NULL) {
			if (Audio->EtatSelection_Wav > CD_ETAT_ATTENTE_EXIST) return (TRUE);
		}
		List = g_list_next (List);
	}
	return (FALSE);
}
// 
// 
gboolean cdaudio_get_bool_mp3_extract (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;

	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if ((Audio = (CD_AUDIO *)List->data) != NULL) {
			if (Audio->EtatSelection_Mp3 > CD_ETAT_ATTENTE_EXIST) return (TRUE);
		}
		List = g_list_next (List);
	}
	return (FALSE);
}
// 
// 
gboolean cdaudio_get_etat_normalise (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;

	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if ((Audio = (CD_AUDIO *)List->data) != NULL) {
			if (Audio->EtatNormalise == TRUE) return (TRUE);
		}
		List = g_list_next (List);
	}
	return (FALSE);
}
// 
// 
gboolean cdaudio_get_bool_is_wav_extract_to_cue (void)
{
	GList		*List = NULL;
	CD_AUDIO	*Audio = NULL;
	gint		 NbrSelectedWav = 0;
	gboolean	 BoolSelectedOthers = FALSE;
	
	List = g_list_first (EnteteCD.GList_Audio_cd);
	while (List) {
		if ((Audio = (CD_AUDIO *)List->data) != NULL) {
			if (Audio->EtatSelection_Wav > CD_ETAT_ATTENTE_EXIST) NbrSelectedWav ++;
			if (Audio->EtatSelection_Flac > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Mp3 > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Ogg > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_M4a > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Aac > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Mpc > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_Ape > CD_ETAT_ATTENTE_EXIST ||
			    Audio->EtatSelection_WavPack > CD_ETAT_ATTENTE_EXIST) BoolSelectedOthers = TRUE;
		}
		List = g_list_next (List);
	}
	if (BoolSelectedOthers == TRUE) return (FALSE);
	return (NbrSelectedWav > 1 ? TRUE : FALSE);
}
/* Rendre utilisable ou non les boutons du notebook CD AUDIO
*  --
*  entree : -
*  retour : -
*/
void cdaudio_set_flag_buttons (void)
{
	gboolean     BoolButtonsAudio [ 3 ] = {FALSE, FALSE, FALSE};

	// raffraichir
	BoolButtonsAudio [ 0 ] = scan_get_text_combo_cd (_CD_) != NULL ? TRUE : FALSE;

	// extraction
	BoolButtonsAudio [ 1 ] = cdaudio_get_bool_all_extract ();

	// action
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_rafraichir_cd")), BoolButtonsAudio [ 0 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_applique_change")), BoolButtonsAudio [ 1 ]);
	
	// Numerate begin file
	BoolButtonsAudio [ 2 ] = EnteteCD.GList_Audio_cd ? TRUE : FALSE;
	gtk_widget_set_sensitive (GTK_WIDGET (GTK_COMBO_BOX (var_cd.Adr_combobox_normalise_cd)), cdaudio_get_etat_normalise ());
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("vbox_options_cue")), cdaudio_get_bool_is_wav_extract_to_cue ());
		
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("table_tags_expanderCD")), NULL != EnteteCD.GList_Audio_cd ? TRUE : FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("vbox_titres_expanderCD")), NULL != EnteteCD.GList_Audio_cd ? TRUE : FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_cue_creation_from_cd")), NULL != EnteteCD.GList_Audio_cd ? TRUE : FALSE );
	
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_import_title_cd")), NULL != EnteteCD.GList_Audio_cd ? TRUE : FALSE );
	
	StatusBar_puts();
}
// Recuperation d'un pointeur de texte sur le contenu du bouton 'destination' qui est
// le chemin de destination des extractions / conversions de pistes musicales
// --
// entree : -
// retour :
//     gchar * : pointeur de texte
// 
gchar *cdaudio_get_result_destination (void)
{
	gchar		*NameDir = NULL;
	gchar		*StrTemplate = NULL;
	CD_AUDIO	*Audio = NULL;

	if ((Audio = cdaudio_get_line_selected ())) {
		StrTemplate = Parse_get_line( PARSE_TYPE_STOCKAGE_CD, Audio->Num_Track -1 );
		if( *StrTemplate == '\0' )
			NameDir = g_strdup_printf( "%s/%s", Config.PathDestinationCD, StrTemplate );
		else	NameDir = g_strdup_printf( "%s/%s/", Config.PathDestinationCD, StrTemplate );
		g_free (StrTemplate);
		StrTemplate = NULL;
	}
	else {
		NameDir = g_strdup_printf ("%s", (gchar *)Config.PathDestinationCD);
	}
	
	return (NameDir);
}

// 
//
void on_combobox_peripherique_cd_realize (GtkWidget *widget, gpointer user_data)
{
	GList *List = NULL;
	MEDIA *Media = NULL;
	gint   Cpt = 0;

	List = g_list_first (scan_get_glist ());
	while (List) {
		if ((Media = (MEDIA *)List->data)) {
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), (gchar *)Media->Full_Name);
			Cpt ++;
		}
		List = g_list_next(List);
	}
	
	if (Config.NumSelectComboBoxCd < 0) Config.NumSelectComboBoxCd = 0;
	if (Cpt > 0) Cpt --;
	if (Config.NumSelectComboBoxCd > Cpt) Config.NumSelectComboBoxCd = Cpt;
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.NumSelectComboBoxCd);
	var_cd.Adr_Combo_Box = GTK_COMBO_BOX (widget);
}
// 
//
void on_combobox_peripherique_cd_changed (GtkComboBox *combobox, gpointer user_data)
{	
	if (NULL != var_cd.Adr_Combo_Box) {
		Config.NumSelectComboBoxCd = gtk_combo_box_get_active (GTK_COMBO_BOX (var_cd.Adr_Combo_Box));
		cd_audio_bool_access_discid( FALSE );
		CdCurl_set_call( -1, NULL, NULL );
		cdaudiotoc_reffresh_list_cd ();
	}
}
// 
//
void on_label_titre_cd_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_Label_Titre = widget;
	cdaudio_put_label_titre ("");
}
// 
// 
void on_label_duree_cd_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_Label_Duree = widget;
	cdaudio_put_label_duree ("");
}
// 
// 
void on_label_StrDureeCd_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_Label_Duree = widget;
	cdaudio_put_label_duree ("");
}
// 
// 
void on_button_applique_change_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_button_applique_change = widget;
}
// 
// 
void on_button_applique_change_clicked (GtkButton *button, gpointer user_data)
{
	if (EnteteCD.GList_Audio_cd != NULL) {

		/*  La normalisation des fichiers se fait a la fin des
		 *  extractions et conversion des fichiers
		 */
		if (cdaudio_get_bool_all_extract ()) {
			cdaudioextract_extraction_cd_to_file_action ();
		}
	}
}
// 
// 
void on_cdaudio_combobox_serveur_cd_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Auto: freedb");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Param CDDB");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Param PROXY");
		
	var_cd.Adr_combobox_serveur = GTK_COMBO_BOX (widget);
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.ServeurCddb);
}
// 
// cddbp-server=servername
// 	sets the server to be contacted for title lookups
// 	définit le serveur à contacter pour les recherches titre
// 	legt die Portnummer für den Titel Lookups verwendet werden
// 	imposta il numero di porta da utilizzare per le ricerche titolo
// 
// cddbp-port=portnumber
// 	sets the port number to be used for title lookups
// 	définit le nombre de port à utiliser pour les recherches titre
// 	legt die Portnummer für den Titel Lookups verwendet werden
// 	imposta il numero di porta da utilizzare per le ricerche titolo
// 
void on_button_init_cddp_clicked (GtkButton *button, gpointer user_data)
{
	if( SERVER_CDDB_PARAM == Config.ServeurCddb ) {
			
		if (NULL != Config.entry_cddp_server) {
			g_free (Config.entry_cddp_server);	Config.entry_cddp_server = NULL;
		}
		Config.entry_cddp_server = g_strdup ("freedb.org");
		gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_server"))), Config.entry_cddp_server);
	
	} else if( SERVER_PROXY_PARAM == Config.ServeurCddb ) {
	
		if (NULL != Config.entry_proxy_server) {
			g_free (Config.entry_proxy_server);	Config.entry_proxy_server = NULL;
		}
		if (NULL != Config.entry_proxy_port) {
			g_free (Config.entry_proxy_port);	Config.entry_proxy_port = NULL;
		}
		Config.entry_proxy_server = g_strdup ("10.0.0.1");
		gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_server"))), Config.entry_proxy_server);
		Config.entry_proxy_port = g_strdup ("8080");
		gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_port"))), Config.entry_proxy_port);
	}	
}
// 
// 
void on_combobox_serveur_cd_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_cd.Adr_combobox_serveur) {
		Config.ServeurCddb = gtk_combo_box_get_active (var_cd.Adr_combobox_serveur);
		if( SERVER_CDDB_PARAM == Config.ServeurCddb ) {
			
			// label101
			gtk_label_set_markup (GTK_LABEL(GTK_WIDGET (GLADE_GET_OBJECT("label101"))), _("<b> Setting up CDDB: [ Server name ]</b>"));
			gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("alignment_cddb_server")));
			gtk_widget_hide (GTK_WIDGET (GLADE_GET_OBJECT("alignment_cddb_port")));
			gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("button_init_cddb")));
			gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_server"))), Config.entry_cddp_server);
		
		} else if( SERVER_PROXY_PARAM == Config.ServeurCddb ) {
			
			// label101
			gtk_label_set_markup (GTK_LABEL(GTK_WIDGET (GLADE_GET_OBJECT("label101"))), _("<b> Proxy settings: [ server ] [ port ] </ b>"));
			gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("alignment_cddb_server")));
			gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("alignment_cddb_port")));
			gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("button_init_cddb")));
			gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_server"))), Config.entry_proxy_server);
			gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_port"))), Config.entry_proxy_port);
		
		} else {
			
			// label101
			gtk_label_set_markup (GTK_LABEL(GTK_WIDGET (GLADE_GET_OBJECT("label101"))), _("<b> Server </b>"));
			gtk_widget_hide (GTK_WIDGET (GLADE_GET_OBJECT("alignment_cddb_server")));
			gtk_widget_hide (GTK_WIDGET (GLADE_GET_OBJECT("alignment_cddb_port")));
			gtk_widget_hide (GTK_WIDGET (GLADE_GET_OBJECT("button_init_cddb")));
		}
	}
}
// 
// 
void on_entry_cddp_server_realize (GtkWidget *widget, gpointer user_data)
{
	if (NULL == Config.entry_cddp_server) {
		Config.entry_cddp_server = g_strdup ("freedb.org");
	}
	if (NULL == Config.entry_proxy_server) {
		Config.entry_proxy_server = g_strdup ("10.0.0.1");
	}
	if (NULL == Config.entry_proxy_port) {
		Config.entry_proxy_port = g_strdup ("8080");
	}
	if( SERVER_CDDB_PARAM == Config.ServeurCddb ) {
		gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_server"))), Config.entry_cddp_server);
	} else if( SERVER_PROXY_PARAM == Config.ServeurCddb ) {	
		gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_server"))), Config.entry_proxy_server);
		gtk_entry_set_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_port"))), Config.entry_proxy_port);
	}
	on_combobox_serveur_cd_changed (NULL, NULL);
}
// 
// 
void on_entry_cddp_server_changed( GtkEditable *editable, gpointer user_data )
{
	gchar	*PtrServeur = (gchar *)gtk_entry_get_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_server"))));
	
	if( SERVER_CDDB_PARAM == Config.ServeurCddb ) {
		
		if( NULL != Config.entry_cddp_server ) {
			g_free( Config.entry_cddp_server );	Config.entry_cddp_server = NULL;
		}
		if( *PtrServeur == '\0' ) {
			Config.entry_cddp_server = g_strdup ("freedb.org");
		} else {
			Config.entry_cddp_server = g_strdup ( PtrServeur );
		}
	
	} else if( SERVER_PROXY_PARAM == Config.ServeurCddb ) {
			
		if( NULL != Config.entry_proxy_server ) {
			g_free( Config.entry_proxy_server );	Config.entry_proxy_server = NULL;
		}
		if( *PtrServeur == '\0' ) {
			Config.entry_proxy_server = g_strdup ("");
		} else {
			Config.entry_proxy_server = g_strdup ( PtrServeur );
		}
	}
}
void on_entry_cddb_port_changed( GtkEditable *editable, gpointer user_data )
{
	gchar	*PtrPort    = (gchar *)gtk_entry_get_text (GTK_ENTRY(GTK_WIDGET(GLADE_GET_OBJECT("entry_cddb_port"))));
	
	if( NULL != Config.entry_proxy_port ) {
		g_free( Config.entry_proxy_port );	Config.entry_proxy_port = NULL;
	}
	if( *PtrPort == '\0' ) {
		Config.entry_proxy_port = g_strdup ("");
	} else {
		Config.entry_proxy_port = g_strdup ( PtrPort );
	}
}
// 
// 
void on_combobox_normalise_cd_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_combobox_normalise_cd = GTK_COMBO_BOX (widget);
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Peak/Album");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Peak");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), 0);
}
// 
// 
void on_combobox_normalise_cd_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL == var_cd.Adr_combobox_normalise_cd) return;
}
// 
// SOLVED
// 	Debian Bug report logs - #673640
// 	xcfa: XCFA crashes when the CD is ejected
// 	http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=673640
// 
void on_button_eject_cd_realize( GtkWidget *widget, gpointer user_data )
{
	// g_print( "on_button_eject_cd_realize\n" );
	// g_print( "\tscan_get_glist() = %p\n", scan_get_glist () );
	gtk_widget_set_sensitive( widget, scan_get_glist() == NULL ? FALSE : TRUE );
}
void on_button_eject_cd_clicked (GtkButton *button, gpointer user_data)
{
	cdaudiotoc_button_eject_cd_clicked ();
}
// 
// 
void on_button_destination_cd_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_Button_Destination = GTK_BUTTON (widget);
	gtk_button_set_use_underline (GTK_BUTTON (var_cd.Adr_Button_Destination), FALSE);
	gtk_button_set_label (GTK_BUTTON (var_cd.Adr_Button_Destination), Config.PathDestinationCD);
}
// 
// 
void cdaudio_apply_result_destination (gchar *path)
{
	if (TRUE == libutils_test_write (path)) {
		g_free (Config.PathDestinationCD);
		Config.PathDestinationCD = NULL;
		Config.PathDestinationCD = g_strdup (path);
		gtk_button_set_label (GTK_BUTTON (var_cd.Adr_Button_Destination), Config.PathDestinationCD);
		cdaudio_update_glist ();
		OptionsCd_set_entry_and_label();
	}
}
// 
// 
void on_button_destination_cd_clicked (GtkButton *button, gpointer user_data)
{
	fileselect_create (_PATH_CHOICE_DESTINATION_, Config.PathDestinationCD, cdaudio_apply_result_destination);
}
// 
// 
void on_button_rafraichir_cd_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_Button_Raffraichir = widget;
}
// REMET LA LISTE A JOUR
// 
void on_button_rafraichir_cd_clicked (GtkButton *button, gpointer user_data)
{
	cd_audio_bool_access_discid( FALSE );
	CdCurl_set_call( -1, NULL, NULL );
	cdaudiotoc_reffresh_list_cd ();
}
// 
// 
void cdaudio_from_popup_select_verticaly (gboolean BoolSelect, gboolean BoolSelectExpert, TYPE_FILE_IS TypeFileIs)
{
	CD_AUDIO      *Audio = NULL;
	GdkPixbuf     *Pixbuf = NULL;
	GList         *List = NULL;
	gboolean       valid;
	GtkTreeIter    iter;
	gint           NumTrack = 0;
	
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, NumTrack);
		if (NULL != (Audio = (CD_AUDIO *)List->data)) {
			
			if (TypeFileIs == FILE_IS_FLAC) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_Flac = CD_ETAT_SELECT;
				else	Audio->EtatSelection_Flac = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_Flac    = cdaudio_get_next_flag (FILE_IS_FLAC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_FLAC);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FLAC, Pixbuf, -1);
			}
			else if (TypeFileIs == FILE_IS_WAV) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_Wav = CD_ETAT_SELECT;
				else	Audio->EtatSelection_Wav = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_Wav    = cdaudio_get_next_flag (FILE_IS_WAV, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAV);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAV, Pixbuf, -1);
			}
			else if (TypeFileIs == FILE_IS_MP3) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_Mp3 = CD_ETAT_SELECT;
				else	Audio->EtatSelection_Mp3 = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_Mp3    = cdaudio_get_next_flag (FILE_IS_MP3, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MP3);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MP3, Pixbuf, -1);
			}
			else if (TypeFileIs == FILE_IS_OGG) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_Ogg = CD_ETAT_SELECT;
				else	Audio->EtatSelection_Ogg = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_Ogg    = cdaudio_get_next_flag (FILE_IS_OGG, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_OGG);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_OGG, Pixbuf, -1);
			}
			else if (TypeFileIs == FILE_IS_M4A) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_M4a = CD_ETAT_SELECT;
				else	Audio->EtatSelection_M4a = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_M4a    = cdaudio_get_next_flag (FILE_IS_M4A, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_M4A);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_M4A, Pixbuf, -1);
			}
			else if (TypeFileIs == FILE_IS_AAC) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_Aac = CD_ETAT_SELECT;
				else	Audio->EtatSelection_Aac = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_Aac    = cdaudio_get_next_flag (FILE_IS_AAC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_AAC);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_AAC, Pixbuf, -1);
			}
			else if (TypeFileIs == FILE_IS_MPC) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_Mpc = CD_ETAT_SELECT;
				else	Audio->EtatSelection_Mpc = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_Mpc    = cdaudio_get_next_flag (FILE_IS_MPC, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MPC);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MPC, Pixbuf, -1);
			}
			else if (TypeFileIs == FILE_IS_APE) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_Ape = CD_ETAT_SELECT;
				else	Audio->EtatSelection_Ape = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_Ape    = cdaudio_get_next_flag (FILE_IS_APE, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_APE);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_APE, Pixbuf, -1);
			}
			else if (TypeFileIs == FILE_IS_WAVPACK) {
				if (BoolSelect && BoolSelectExpert)
					Audio->EtatSelection_WavPack = CD_ETAT_SELECT;
				else	Audio->EtatSelection_WavPack = CD_ETAT_ATTENTE;
				if (TRUE == BoolSelect)
					Audio->EtatSelection_WavPack    = cdaudio_get_next_flag (FILE_IS_WAVPACK, TRUE, NumTrack);
				Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAVPACK);
				gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAVPACK, Pixbuf, -1);
			}

			NumTrack ++;
		}
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
	}
	cdaudio_set_flag_buttons ();
}
// 
// 
void cdaudio_from_popup_select_horizontaly (CD_AUDIO *p_Audio, gboolean BoolSelect, gboolean BoolSelectExpert)
{
	CD_AUDIO      *Audio = NULL;
	GdkPixbuf     *Pixbuf = NULL;
	GList         *List = NULL;
	gboolean       valid;
	GtkTreeIter    iter;
	gint           NumTrack = 0;
	
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, NumTrack);
		if (NULL != (Audio = (CD_AUDIO *)List->data)) {
			if (Audio == p_Audio) break;
			NumTrack ++;
		}
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
	}

	if (TRUE == BoolSelect && TRUE == BoolSelectExpert) {
		Audio->EtatSelection_Flac =
		Audio->EtatSelection_Wav =
		Audio->EtatSelection_Mp3 =
		Audio->EtatSelection_Ogg =
		Audio->EtatSelection_M4a =
		Audio->EtatSelection_Aac =
		Audio->EtatSelection_Mpc =
		Audio->EtatSelection_Ape =
		Audio->EtatSelection_WavPack = CD_ETAT_SELECT;
	}
	else {
		Audio->EtatSelection_Flac =
		Audio->EtatSelection_Wav =
		Audio->EtatSelection_Mp3 =
		Audio->EtatSelection_Ogg =
		Audio->EtatSelection_M4a =
		Audio->EtatSelection_Aac =
		Audio->EtatSelection_Mpc =
		Audio->EtatSelection_Ape =
		Audio->EtatSelection_WavPack = CD_ETAT_ATTENTE;
	}
	
	if (TRUE == BoolSelect) {
		Audio->EtatSelection_Flac    = cdaudio_get_next_flag (FILE_IS_FLAC, TRUE, NumTrack);
		Audio->EtatSelection_Wav     = cdaudio_get_next_flag (FILE_IS_WAV, TRUE, NumTrack);
		Audio->EtatSelection_Mp3     = cdaudio_get_next_flag (FILE_IS_MP3, TRUE, NumTrack);
		Audio->EtatSelection_Ogg     = cdaudio_get_next_flag (FILE_IS_OGG, TRUE, NumTrack);
		Audio->EtatSelection_M4a     = cdaudio_get_next_flag (FILE_IS_M4A, TRUE, NumTrack);
		Audio->EtatSelection_Aac     = cdaudio_get_next_flag (FILE_IS_AAC, TRUE, NumTrack);
		Audio->EtatSelection_Mpc     = cdaudio_get_next_flag (FILE_IS_MPC, TRUE, NumTrack);
		Audio->EtatSelection_Ape     = cdaudio_get_next_flag (FILE_IS_APE, TRUE, NumTrack);
		Audio->EtatSelection_WavPack = cdaudio_get_next_flag (FILE_IS_WAVPACK, TRUE, NumTrack);
	}
				
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_FLAC);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FLAC, Pixbuf, -1);
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAV);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAV, Pixbuf, -1);
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MP3);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MP3, Pixbuf, -1);
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_OGG);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_OGG, Pixbuf, -1);
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_M4A);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_M4A, Pixbuf, -1);
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_AAC);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_AAC, Pixbuf, -1);
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MPC);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MPC, Pixbuf, -1);
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_APE);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_APE, Pixbuf, -1);
	Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAVPACK);
	gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAVPACK, Pixbuf, -1);

	cdaudio_set_flag_buttons ();
}
//
//
void cdaudio_from_popup_clear (void)
{
	CD_AUDIO      *Audio = NULL;
	GdkPixbuf     *Pixbuf = NULL;
	GList         *List = NULL;
	gboolean       valid;
	GtkTreeIter    iter;
	gint           NumTrack = 0;
	
	// PRINT_FUNC_LF();
	
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, NumTrack);
		if ((Audio = (CD_AUDIO *)List->data)) {
			
			Audio->EtatSelection_Flac =
			Audio->EtatSelection_Wav =
			Audio->EtatSelection_Mp3 =
			Audio->EtatSelection_Ogg =
			Audio->EtatSelection_M4a =
			Audio->EtatSelection_Aac =
			Audio->EtatSelection_Mpc =
			Audio->EtatSelection_Ape =
			Audio->EtatSelection_WavPack = CD_ETAT_ATTENTE;
			
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_FLAC);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FLAC, Pixbuf, -1);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAV);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAV, Pixbuf, -1);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MP3);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MP3, Pixbuf, -1);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_OGG);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_OGG, Pixbuf, -1);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_M4A);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_M4A, Pixbuf, -1);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_AAC);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_AAC, Pixbuf, -1);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_MPC);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_MPC, Pixbuf, -1);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_APE);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_APE, Pixbuf, -1);
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_IS_WAVPACK);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_WAVPACK, Pixbuf, -1);
			
			NumTrack ++;
		}
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
	}
	cdaudio_set_flag_buttons ();
}
//
//
void cdaudio_from_popup_change_normalise (gboolean BoolSelect)
{
	CD_AUDIO      *Audio = NULL;
	GList         *List = NULL;
	gboolean       valid;
	GtkTreeIter    iter;
	gint           NumTrack = 0;
	GdkPixbuf     *Pixbuf = NULL;
	
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, NumTrack);
		if ((Audio = (CD_AUDIO *)List->data)) {
			
			Audio->EtatNormalise = BoolSelect;
			Pixbuf = cdaudio_get_pixbuf_file (Audio, FILE_TO_NORMALISE);
			gtk_list_store_set (var_cd.Adr_List_Store, &iter, COLUMN_CDAUDIO_FILE_NORMALIZE, Pixbuf, -1);
		}
		NumTrack ++;
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
	}
	cdaudio_set_flag_buttons ();
}
//
//
void cdaudio_from_popup (TYPE_SET_FROM_POPUP_CD TypeSetFromPopup, CD_AUDIO *Audio, TYPE_FILE_IS TypeFileIs)
{
	switch (TypeSetFromPopup) {
	
	// SELECTION ou DESELECTION POUR LES CONVERSIONS CD
	
	case CD_CONV_DESELECT_ALL :		// Deselection globale
		cdaudio_from_popup_clear ();
		break;
	case CD_CONV_DESELECT_V :		// Deselection verticale
		cdaudio_from_popup_select_verticaly (FALSE, FALSE, TypeFileIs);
		break;
	case CD_CONV_DESELECT_H :		// Deselection horizontale
		cdaudio_from_popup_select_horizontaly (Audio, FALSE, FALSE);
		break;
	case CD_CONV_SELECT_V :			// Selection verticale
		cdaudio_from_popup_select_verticaly (TRUE, FALSE, TypeFileIs);
		break;
	case CD_CONV_SELECT_EXPERT_V :		// Selection Expert verticale
		cdaudio_from_popup_select_verticaly (TRUE, TRUE, TypeFileIs);
		break;
	case CD_CONV_SELECT_H :			// Selection horizontale
		cdaudio_from_popup_select_horizontaly (Audio, TRUE, FALSE);
		break;
	case CD_CONV_SELECT_EXPERT_H :		// Selection Expert horizontale
		cdaudio_from_popup_select_horizontaly (Audio, TRUE, TRUE);
		break;
	
	// SELECTION ou DESELECTION POUR LA NORMALISATION
	
	case CD_REPLAYGAIN_SELECT_V :		// Deselection verticale
		cdaudio_from_popup_change_normalise (FALSE);
		break;
	case CD_REPLAYGAIN_DESELECT_V :		// Selection verticale
		cdaudio_from_popup_change_normalise (TRUE);
		break;
	
	}
}
// 
// 
// 
// 
gboolean BoolAccess = TRUE;
// 
// 
void cd_audio_bool_access_discid( gboolean p_bool_access )
{
	BoolAccess = p_bool_access;
}
//
//
void on_combobox_discid_changed( GtkComboBox *combobox, gpointer user_data )
{
	if( NULL != var_cd.Adr_combobox_discid_cd ) {
		if( TRUE == BoolAccess ) {
			cd_audio_bool_access_discid( FALSE );
			CdCurl_set_call( gtk_combo_box_get_active(GTK_COMBO_BOX (var_cd.Adr_combobox_discid_cd)), NULL, NULL );
			cdaudiotoc_reffresh_list_cd ();
		}
	}
}
//
//
void on_combobox_discid_realize( GtkWidget *widget, gpointer user_data )
{
	gtk_combo_box_set_active( GTK_COMBO_BOX(widget), 0 );
	var_cd.Adr_combobox_discid_cd = GTK_COMBO_BOX (widget);
}
















