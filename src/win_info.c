 /*
 *  file      : win_info.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "win_info.h"


GtkBuilder	*GtkBuilderProjet_wind_info = NULL;
GtkWidget	*WindMain_wind_info = NULL;



// QUITTER
// 
gboolean on_wind_info_destroy_event( GtkWidget *widget, GdkEvent *event, gpointer user_data )
{
	gtk_widget_destroy( WindMain_wind_info );
	WindMain_wind_info = NULL;
	GtkBuilderProjet_wind_info = NULL;
	return TRUE;
}
// QUITTER
// 
gboolean on_wind_info_delete_event( GtkWidget *widget, GdkEvent *event, gpointer user_data )
{
	gtk_widget_destroy( WindMain_wind_info );
	WindMain_wind_info = NULL;
	GtkBuilderProjet_wind_info = NULL;
	return TRUE;
}
// QUITTER
// 
void on_button_quitter_wininfo_clicked (GtkButton *button, gpointer user_data)
{
	gtk_widget_destroy( WindMain_wind_info );
	WindMain_wind_info = NULL;
	GtkBuilderProjet_wind_info = NULL;
}
// WIN-READER OPEN
// 
gboolean wind_info_init( GtkWidget *FromWindMain, gchar *title, ...  )
{
	// gchar		*PackageVersion = NULL;
	gchar		*StrTitle = NULL;
	va_list		Arguments;
	gchar		*PtrArg = NULL;
	GString		*StrTxt = NULL;
	// GtkWidget	*button_quitter_wininfo = NULL;

	if( NULL == (GtkBuilderProjet_wind_info = Builder_open( "xcfa_win_info.glade", GtkBuilderProjet_wind_info ))) {
		return( EXIT_FAILURE );
	}
	// Add language support from glade file
	gtk_builder_set_translation_domain(  GtkBuilderProjet_wind_info, "wind_info" );
	gtk_builder_connect_signals( GtkBuilderProjet_wind_info, NULL );

	WindMain_wind_info = GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_info, "wind_info" ));
	// button_quitter_wininfo=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_info, "button_quitter_wininfo" ));

	if( NULL != FromWindMain ) {
		gtk_window_set_transient_for (GTK_WINDOW(WindMain_wind_info), GTK_WINDOW(FromWindMain));
		gtk_window_set_modal (GTK_WINDOW(WindMain_wind_info), TRUE);
	}
	libutils_set_default_icone_to_win( WindMain_wind_info );

	gtk_window_set_title( GTK_WINDOW( WindMain_wind_info ), title );

	StrTitle = g_strdup_printf ("\n              <span font_desc=\"Courier bold 12\"><b>%s</b></span>              \n", title);
	gtk_label_set_markup (GTK_LABEL (gtk_builder_get_object( GtkBuilderProjet_wind_info, "label_titre_wininfo")), StrTitle);
	g_free (StrTitle);	StrTitle = NULL;

	StrTxt = g_string_new (NULL);								//  Creation du GString
	va_start (Arguments, title);								//  Debut des arguments
	while ( *(PtrArg = va_arg (Arguments, gchar *)) != '\0') {	//  TANSQUE message ALORS
		g_string_append_printf (StrTxt, "%s", &PtrArg[0]);		//  	message.stock
	}															//  FIN_TANSQUE
	va_end (Arguments);											//  Fin des arguments
	g_string_append_printf (StrTxt, "\n\n");

	gtk_label_set_markup (GTK_LABEL (gtk_builder_get_object( GtkBuilderProjet_wind_info, "label_texte_wininfo")), StrTxt->str);
	g_string_free (StrTxt, TRUE);

	gtk_widget_show_all( WindMain_wind_info );

	return( TRUE );
}









