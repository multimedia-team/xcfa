 /*
 *  file      : cd_expander.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "file.h"
#include "cd_audio.h"
#include "configuser.h"
#include "parse.h"
#include "popup.h"




typedef struct {
	gboolean BoolChangedOk;
} VAR_CDEXPANDER;
VAR_CDEXPANDER	VarCdExpander = {
				TRUE	// BoolChangedOk
				};


// 
// 
void cdexpander_set_sensitive_notebook (void)
{
	CD_AUDIO	*Audio = NULL;
	
	if (NULL == var_cd.Adr_notebook) return;
	Audio = cdaudio_get_line_selected ();
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("table_tags_expanderCD")), Audio ? TRUE : FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("vbox_titres_expanderCD")), Audio ? TRUE : FALSE);
}
// 
// 
void cdexpander_set_entry_tag_titre_album (void)
{
	CD_AUDIO            *Audio = NULL;
	
	if (NULL == var_cd.Adr_entry_tag_titre_album) return;
	if (NULL != (Audio = cdaudio_get_line_selected ())) {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_album), Audio->tags->Album);
		VarCdExpander.BoolChangedOk = TRUE;
	}
	else {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_album), "");
		VarCdExpander.BoolChangedOk = TRUE;
	}
}
// 
// 
void cdexpander_set_entry_tag_nom_artiste (void)
{
	CD_AUDIO            *Audio = NULL;
	
	if (NULL == var_cd.Adr_entry_tag_nom_artiste) return;
	if (NULL != (Audio = cdaudio_get_line_selected ())) {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_nom_artiste), Audio->tags->Artist);
		VarCdExpander.BoolChangedOk = TRUE;
	}
	else {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_nom_artiste), "");
		VarCdExpander.BoolChangedOk = TRUE;
	}
}
// 
// 
void cdexpander_set_spinbutton_tag_piste (void)
{
	CD_AUDIO        *Audio = NULL;
	
	if (NULL != (Audio = cdaudio_get_line_selected ()) && NULL != var_cd.Adr_spinbutton_tag_piste) {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (var_cd.Adr_spinbutton_tag_piste), (gdouble)Audio->tags->IntNumber);
		VarCdExpander.BoolChangedOk = TRUE;
	}
}
// 
// 
void cdexpander_set_spinbutton_tag_annee (void)
{
	CD_AUDIO        *Audio = NULL;
	
	if (NULL != (Audio = cdaudio_get_line_selected ()) && NULL != var_cd.Adr_spinbutton_tag_annee) {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (var_cd.Adr_spinbutton_tag_annee), (gdouble)Audio->tags->IntYear);
		VarCdExpander.BoolChangedOk = TRUE;
	}
}
// 
// 
gint cdexpander_get_spinbutton_tag_annee (void)
{
	if (NULL != var_cd.Adr_spinbutton_tag_annee) {
		return (gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(var_cd.Adr_spinbutton_tag_annee)));
	}
	return (0);
}
// 
// 
void cdexpander_set_entry_tag_titre_chanson (void)
{
	CD_AUDIO            *Audio = NULL;
	
	if (NULL != (Audio = cdaudio_get_line_selected ()) && NULL != var_cd.Adr_entry_tag_titre_chanson) {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_chanson), Audio->tags->Title);
		VarCdExpander.BoolChangedOk = TRUE;		
	}
	else {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_chanson), "");
		VarCdExpander.BoolChangedOk = TRUE;
	}
}
// 
// 
void cdexpander_set_new_genre (void)
{
	CD_AUDIO            *Audio = NULL;

	if (NULL != (Audio = cdaudio_get_line_selected ()) ) {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text( GTK_ENTRY(GLADE_GET_OBJECT("entry_tag_genre")), tags_get_genre_by_name( Audio->tags->IntGenre ));
		VarCdExpander.BoolChangedOk = TRUE;
	}
}
// 
// 
void cdexpander_set_entry_new_titre_cdaudio (gchar *p_str)
{
	if (NULL != var_cd.Adr_entry_new_titre_cdaudio && NULL != p_str) {
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_new_titre_cdaudio), p_str);
	}
}
// 
// 
void cdexpander_set_entry_tag_commentaire (void)
{
	CD_AUDIO            *Audio = NULL;
	
	if (NULL != (Audio = cdaudio_get_line_selected ()) && NULL != var_cd.Adr_entry_tag_commentaire) {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_commentaire), Audio->tags->Comment);
		VarCdExpander.BoolChangedOk = TRUE;		
	}
	else {
		VarCdExpander.BoolChangedOk = FALSE;
		gtk_entry_set_text( GTK_ENTRY(var_cd.Adr_entry_tag_commentaire), Config.StringCommentCD );
		VarCdExpander.BoolChangedOk = TRUE;
	}
}
// 
// 
void cdexpander_set_entry_tag_titre_fichier_m3u (void)
{
	if (FALSE == VarCdExpander.BoolChangedOk) return;
	if (NULL != var_cd.Adr_entry_tag_titre_fichier) {
		VarCdExpander.BoolChangedOk = FALSE;
		// m3u
		// xspf
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_fichier), Config.StringNameFile_m3u_xspf ? Config.StringNameFile_m3u_xspf : "");
		VarCdExpander.BoolChangedOk = TRUE;		
	}
}
// 
// 
void on_entry_tag_titre_chanson_changed (GtkEditable *editable, gpointer user_data)
{
	gchar        *ptr_template = NULL;
	CD_AUDIO     *Audio = NULL;
	gchar        *str = NULL;
	gchar        *ptr = NULL;
	
	if (!var_cd.Adr_entry_tag_titre_chanson) return;
	ptr_template = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_chanson));
	
	/* Suppression du caracteres '/' interdit si il existe */
	str = g_strdup (ptr_template);
	if (strchr (str, '/')) {
		while ((ptr = strchr (str, '/'))) {
			strcpy (ptr, ptr+1);
		}
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_chanson), str);
	}
	g_free (str);
	str = NULL;

	if (FALSE == VarCdExpander.BoolChangedOk) return;
	if (NULL != (Audio = cdaudio_get_line_selected ())) {
		g_free (Audio->tags->Title);
		Audio->tags->Title = NULL;
		Audio->tags->Title = g_strdup (ptr_template);
	}
	cdaudio_set_titre_chanson ();
}
// 
// 
void on_entry_tag_titre_album_changed (GtkEditable *editable, gpointer user_data)
{
	gint          Num_cell = 0;
	gboolean      valid;
	GtkTreeIter   iter;
	GList        *List = NULL;
	gchar        *ptr_template = NULL;
	CD_AUDIO     *Audio = NULL;
	
	if (NULL == var_cd.Adr_entry_tag_titre_album) return;
	if (FALSE == VarCdExpander.BoolChangedOk) return;
	ptr_template = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_album));

	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, Num_cell);
		if (NULL != (Audio = (CD_AUDIO *)List->data)) {
			g_free (Audio->tags->Album);
			Audio->tags->Album = NULL;
			Audio->tags->Album = g_strdup (ptr_template);
		}
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
		Num_cell ++;
	}

	cdaudio_set_titre_chanson ();
}
// 
// 
void on_entry_tag_nom_artiste_changed (GtkEditable *editable, gpointer user_data)
{
	gint          Num_cell = 0;
	gboolean      valid;
	GtkTreeIter   iter;
	GList        *List = NULL;
	gchar        *ptr_template = NULL;
	CD_AUDIO     *Audio = NULL;
	
	if (NULL == var_cd.Adr_entry_tag_nom_artiste) return;
	if (FALSE == VarCdExpander.BoolChangedOk) return;
	ptr_template = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_tag_nom_artiste));
	
	if (TRUE == EnteteCD.BoolMultiArtiste) {
		if ((Audio = cdaudio_get_line_selected ())) {
			g_free (Audio->tags->Artist);
			Audio->tags->Artist = NULL;
			Audio->tags->Artist = g_strdup (ptr_template);
		}
	}
	else {
		valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
		while (valid) {
			List = g_list_nth (EnteteCD.GList_Audio_cd, Num_cell);
			if (NULL != (Audio = (CD_AUDIO *)List->data)) {
				g_free (Audio->tags->Artist);
				Audio->tags->Artist = NULL;
				Audio->tags->Artist = g_strdup (ptr_template);
			}
			valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
			Num_cell ++;
		}
	}
	cdaudio_set_titre_chanson ();
}
// 
// 
void on_entry_tag_commentaire_changed (GtkEditable *editable, gpointer user_data)
{
	gchar        *ptr_template = NULL; 
	CD_AUDIO     *Audio = NULL;
	
	if (NULL == var_cd.Adr_entry_tag_commentaire) return;
	
	ptr_template = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_tag_commentaire));
	if( NULL != Config.StringCommentCD ) {
		g_free( Config.StringCommentCD );
		Config.StringCommentCD = NULL;
	}
	Config.StringCommentCD = g_strdup( ptr_template );
	
	if( NULL != ( Audio = cdaudio_get_line_selected())) {
		g_free( Audio->tags->Comment );
		Audio->tags->Comment = NULL;
		Audio->tags->Comment = g_strdup( Config.StringCommentCD );
	}
	
	cdaudio_set_titre_chanson ();
}
// 
// 
void on_spinbutton_tag_annee_value_changed (GtkSpinButton *spinbutton, gpointer user_data)
{
	gint          Num_cell = 0;
	gboolean      valid;
	GtkTreeIter   iter;
	GList        *List = NULL;
	CD_AUDIO     *Audio = NULL;
	gint          Value;
	
	if (NULL == var_cd.Adr_spinbutton_tag_annee) return;
	Value = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(var_cd.Adr_spinbutton_tag_annee));
	
	if (TRUE == EnteteCD.BoolMultiArtiste) {
		if (NULL != (Audio = cdaudio_get_line_selected ())) {
			g_free (Audio->tags->Year);
			Audio->tags->Year = NULL;
			Audio->tags->Year = g_strdup_printf ("%d", Value);
			Audio->tags->IntYear = Value;
		}
	}
	else {
		valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
		while (valid) {
			List = g_list_nth (EnteteCD.GList_Audio_cd, Num_cell);
			if (NULL != (Audio = (CD_AUDIO *)List->data)) {
				g_free (Audio->tags->Year);
				Audio->tags->Year = NULL;
				Audio->tags->Year = g_strdup_printf ("%d", Value);
				Audio->tags->IntYear = Value;
			}
			valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
			Num_cell ++;
		}
	}
	cdaudio_set_titre_chanson ();
}
// 
// 
void on_spinbutton_tag_piste_value_changed (GtkSpinButton *spinbutton, gpointer user_data)
{
	CD_AUDIO     *Audio = NULL;
	gint          Value;
	
	if (NULL == var_cd.Adr_spinbutton_tag_piste) return;
	Value = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(var_cd.Adr_spinbutton_tag_piste));
	
		if (NULL != (Audio = cdaudio_get_line_selected ())) {
			g_free (Audio->tags->Number);
			Audio->tags->Number = NULL;
			Audio->tags->Number = g_strdup_printf ("%d", Value);
			Audio->tags->IntNumber = Value;
		}
	cdaudio_set_titre_chanson ();
}
// 
// 
void on_entry_tag_titre_fichier_m3u_changed (GtkEditable *editable, gpointer user_data)
{
	if (FALSE == VarCdExpander.BoolChangedOk) return;
	if (NULL != var_cd.Adr_entry_tag_titre_fichier) {
		g_free (Config.StringNameFile_m3u_xspf);
		Config.StringNameFile_m3u_xspf = NULL;
		Config.StringNameFile_m3u_xspf = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_tag_titre_fichier)));
		
		if (NULL == strstr (gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_new_titre_cdaudio)), "%f")) {

			gchar *Str = NULL;
		
			Str = g_strdup_printf ("%s %%f", gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_new_titre_cdaudio)));
			gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_new_titre_cdaudio), Str);
			g_free (Str);
			Str = NULL;
		}
	}
}
// 
// 
void cd_expander_set_genre( gint p_num, gchar *p_name )
{
	gint          Num_cell = 0;
	gboolean      valid;
	GtkTreeIter   iter;
	GList        *List = NULL;
	CD_AUDIO     *Audio = NULL;
	
	valid = gtk_tree_model_get_iter_first (var_cd.Adr_Tree_Model, &iter);
	while (valid) {
		List = g_list_nth (EnteteCD.GList_Audio_cd, Num_cell);
		if (NULL != (Audio = (CD_AUDIO *)List->data)) {
			g_free (Audio->tags->Genre);
			Audio->tags->Genre = NULL;
			Audio->tags->Genre = g_strdup( p_name );
			Audio->tags->IntGenre = p_num;
		}
		valid = gtk_tree_model_iter_next (var_cd.Adr_Tree_Model, &iter);
		Num_cell ++;
	}
	cdaudio_set_titre_chanson ();
}
void on_entry_tag_genre_changed( GtkEditable *editable, gpointer user_data )
{
	gchar	*PtrEntry = NULL;
	gint	NumEntry = -1;
	
	if( FALSE == CdPopupGenre.BoolFromPopup ) {
		PRINT_FUNC_LF();
		PtrEntry = (gchar *)gtk_entry_get_text( GTK_ENTRY(GLADE_GET_OBJECT("entry_tag_genre")));
		NumEntry = tags_get_genre_by_value( PtrEntry );
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print( "\tPtrEntry = %s\n", PtrEntry );
			g_print( "\tNumEntry = %d\n", NumEntry);
		}
		cd_expander_set_genre( NumEntry, PtrEntry );
	}
}
// 
// Completion de la saisie clavier
// http://coding.debuntu.org/c-gtk-text-completion-gtkentry-gtkentrycompletion
// 
#define _CONTACT_NAME_	0
// typedef struct {
// 	gint   num;
// 	gchar *name;
// } STRUCT_TAGS_FILE_MP3;
/**
gboolean on_match_select(GtkEntryCompletion *widget, GtkTreeModel *model, GtkTreeIter *iter, gpointer user_data)
{  
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		GValue value = {0, };
		gtk_tree_model_get_value(model, iter, _CONTACT_NAME_, &value);
		g_print( "You have selected %s\n", g_value_get_string(&value) );
		g_value_unset(&value);
	}
	return FALSE;
}
*/    
void on_entry_tag_genre_realize( GtkWidget *widget, gpointer user_data )
{
	STRUCT_TAGS_FILE_MP3	*Contact;
	GtkEntryCompletion	*completion;
	GtkListStore		*model;
	GtkTreeIter		iter;
	
	// set up completion
	completion = gtk_entry_completion_new();
	gtk_entry_completion_set_text_column( completion, _CONTACT_NAME_ );
	gtk_entry_set_completion( GTK_ENTRY(widget), completion );
	// g_signal_connect( G_OBJECT(completion), "match-selected", G_CALLBACK(on_match_select), NULL );
	g_signal_connect( G_OBJECT(widget), "changed", G_CALLBACK(on_entry_tag_genre_changed), widget );
	model = gtk_list_store_new( 1, G_TYPE_STRING );
	for( Contact = StructTagsFileMp3; Contact && Contact->name; Contact++ ) {
		gtk_list_store_append( model, &iter );
		gtk_list_store_set( model, &iter, _CONTACT_NAME_, Contact->name, -1 );
	}
	gtk_entry_completion_set_model( completion, GTK_TREE_MODEL(model) );
}
// 
// Saisie CD -> ARRANGEMENT DES TITRES DU CD
// 
void on_entry_new_titre_cdaudio_changed (GtkEditable *editable, gpointer user_data)
{
	if( FALSE == var_cd.bool_enter_parse ) return;
	var_cd.bool_enter_parse = FALSE;
	
	if (NULL != var_cd.Adr_entry_new_titre_cdaudio) {
		
		// SUPPRESSION DE LA LIGNE SAISIE PAR L UTILISTATEUR
		if (NULL != Config.Templates_title_cdaudio) {
			g_free (Config.Templates_title_cdaudio);
			Config.Templates_title_cdaudio = NULL;
		}
		Config.Templates_title_cdaudio = g_strdup( (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_new_titre_cdaudio)) );
		
		Parse_entry (PARSE_TYPE_TITLE_CD);

		cdaudio_set_titre_chanson ();
	}
	var_cd.bool_enter_parse = TRUE;
}
// 
// 
void on_expander_cd_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_Expander = widget;
	gtk_expander_set_expanded (GTK_EXPANDER (var_cd.Adr_Expander), Config.BoolEtatExpanderCd);
}
// 
// 
void on_notebook_expander_cd_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_notebook = widget;
}
// 
// 
void on_entry_tag_titre_chanson_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_entry_tag_titre_chanson = widget;
}
// 
// 
void on_entry_tag_titre_album_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_entry_tag_titre_album = widget;
}
// 
// 
void on_entry_tag_nom_artiste_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_entry_tag_nom_artiste = widget;
}
// 
// 
void on_entry_tag_commentaire_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_entry_tag_commentaire = widget;
	if( NULL == Config.StringCommentCD ) {
		Config.StringCommentCD = g_strdup( " " );
	}
	gtk_entry_set_text( GTK_ENTRY(var_cd.Adr_entry_tag_commentaire), Config.StringCommentCD );
}
// 
// 
void on_spinbutton_tag_annee_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_spinbutton_tag_annee = widget;
}
// 
// 
void on_spinbutton_tag_piste_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_spinbutton_tag_piste = widget;
}
// 
// 
void on_entry_tag_titre_fichier_m3u_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_entry_tag_titre_fichier = widget;
}
// 
// 
void on_entry_new_titre_cdaudio_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.bool_enter_parse = TRUE;
	var_cd.Adr_entry_new_titre_cdaudio = widget;
	if (NULL == Config.Templates_title_cdaudio) {
		Config.Templates_title_cdaudio = g_strdup ("%c %d");
	}
	gtk_entry_set_text (GTK_ENTRY (widget), Config.Templates_title_cdaudio);
	var_cd.Bool_create_file_m3u = FALSE;
	var_cd.Pathname_m3u = NULL;
}
// 
// 
void on_checkbutton_creation_fichier_unique_cue_clicked (GtkButton *button, gpointer user_data)
{
	if (FALSE == gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (button)))
		gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_creation_fichier_cue")), FALSE);
}
// 
// 
void on_combobox_choice_file_cue_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "WAV");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "FLAC");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "OGG");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "MPC");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), 0);
}
// 
// 
void on_button_tag_call_popup_clicked( GtkButton *button, gpointer user_data )
{
	popup_menu_cd();
}


















