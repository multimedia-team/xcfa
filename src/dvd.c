 /*
 *  file      : dvd.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "fileselect.h"
#include "scan.h"
#include "win_scan.h"
#include "win_reader.h"
#include "mplayer.h"
#include "popup.h"
#include "win_info.h"
#include "dvd.h"
#include "statusbar.h"




/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

enum
{
	DVD_TITLE_COLUMN = 0,
	DVD_PIXBUF_COLUMN_PLAY,
	DVD_TIME_COLUMN,
	DVD_CHANNELS_COLUMN,
	DVD_PIXBUF_CHOICE_COLUMN,

	DVD_PIXBUF_FILE_NORMALIZE,

	DVD_NAME_COLUMN,
	DVD_NAME_EDITABLE_COLUMN,
	DVD_POINTER_STRUCT_COLUMN,
	DVD_NUM_COLUMNS
};

VAR_DVD var_dvd;


GList *GlistDvdExtract = NULL;
GList *GlistDvd = NULL;

enum
{
	NUM_TREE_Titres = 0,
	NUM_TREE_Play,
	NUM_TREE_Temps,
	NUM_TREE_Format,
	NUM_TREE_Choix,
	NUM_TREE_Normalise,
	NUM_TREE_Nom,
	NUM_TREE_ALL_COLUMN
};




//
//
gboolean dvd_foreach_remove_GtkTree (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	VAR        *var = NULL;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);
	if (NULL != var) {
		g_free (var->def);
		g_free (var->time);
		g_free (var->name_file);
		g_free (var->StrNumerateTitle);
		g_free (var->StrNumerateChapter);
		g_free (var->StrNumberChannel);
		g_free (var->StrNumerateStreamId);
		g_free (var);
		var = NULL;
	}
	 /* continuer à parcourir l'arbre */
	return FALSE;
}
//
//
void dvd_remove_GtkTree (void)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_remove_GtkTree, NULL);
	gtk_tree_store_clear ((GtkTreeStore *)var_dvd.Adr_Tree_Model);
}
//
//
gchar *dvd_get_channels (VAR *var)
{
	/*
	1: Original
	2: 2ch: stéréo
	3: 3ch
	4: 4ch
	5: 5ch
	6: 6ch: surround 5.1
	*/
	switch (atoi (var->StrNumberChannel)) {
	case 1 : return ("<span color=\"black\"><b>1ch</b></span>");
	case 2 : return ("<span color=\"black\"><b>2ch</b></span>");
	case 3 : return ("<span color=\"black\"><b>3ch</b></span>");
	case 4 : return ("<span color=\"black\"><b>4ch</b></span>");
	case 5 : return ("<span color=\"black\"><b>5ch</b></span>");
	case 6 : return ("<span color=\"black\"><b>6ch</b></span>");
	default :
		break;
	}
	return ("???");
}
//
//
GdkPixbuf *dvd_get_pixbuf_normalise (gboolean EtatNormaliseCd)
{
	if (EtatNormaliseCd == FALSE) return (var_dvd.Pixbuf_Coche);
	return (var_dvd.Pixbuf_Selected);
}
//
//
gchar *dvd_get_name_to_treview (VAR *var)
{
	gboolean	BoolIdem = FALSE;
	gchar		*NameSrc = NULL;

	BoolIdem = (strlen(var->name_file) == strlen(var->StaticNameFile) && 0 == strcmp (var->name_file, var->StaticNameFile)) ? TRUE : FALSE;

	if (TRUE == BoolIdem)
		NameSrc = g_strdup (var->name_file);
	else	NameSrc = g_strdup_printf ("<span color=\"red\"><b><i>%s</i></b></span>", var->name_file);

	return ((gchar *)NameSrc);
}
//
//
void dvd_affiche_list (void)
{
	GtkTreeIter      piter0 = {0,NULL,NULL,NULL};
	GtkTreeIter      piter1 = {0,NULL,NULL,NULL};
	GtkTreeIter      piter2 = {0,NULL,NULL,NULL};
	GtkTreeIter      piter3 = {0,NULL,NULL,NULL};
	GtkTreeIter      piter4 = {0,NULL,NULL,NULL};
	GtkTreeStore    *model = (GtkTreeStore *)var_dvd.Adr_Tree_Model;
	GList           *list = NULL;
	VAR             *var =  NULL;
	gchar           *time = NULL;
	gchar           *str = NULL;
	gint             NumStruct = 1;
	GtkAdjustment   *Adj = NULL;
	gdouble          AdjValue;
	gchar		*NameDest = NULL;

	// COORDONNEES DU SCROLL VERTICAL
	Adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (var_dvd.Adr_scroll));
	AdjValue = gtk_adjustment_get_value (Adj);

	// VIDER DE TREEVIEW
	gtk_tree_store_clear ((GtkTreeStore *)var_dvd.Adr_Tree_Model);

	list = g_list_first (GlistDvd);
	while (list) {
		if (NULL != (var = (VAR *)list->data)) {

			var->NumStruct = NumStruct ++;
			var->BoolDvdPlay = FALSE;
			var->EtatNormalise = FALSE;

			switch (var->type_list_dvd) {
			case _DVD_NAME :
				str = g_strdup_printf ("<span color=\"red\"><b>%s</b></span>", var->def);

				gtk_tree_store_append (model, &piter0, NULL);
				gtk_tree_store_set (model, &piter0,
						DVD_TITLE_COLUMN,			str,
						DVD_PIXBUF_COLUMN_PLAY,		NULL,
						DVD_TIME_COLUMN,			"",
						DVD_CHANNELS_COLUMN,		"",
						DVD_PIXBUF_CHOICE_COLUMN,	NULL,
						DVD_PIXBUF_FILE_NORMALIZE,	NULL,
						DVD_NAME_COLUMN,			"",
						DVD_NAME_EDITABLE_COLUMN,	FALSE,
						DVD_POINTER_STRUCT_COLUMN,	var,
						-1);

				g_free (str);	str = NULL;
				break;

			case _DVD_TITLE :
				str = g_strdup_printf ("<span color=\"black\"><b>%s</b></span>", var->def);

				gtk_tree_store_append (model, &piter1, &piter0);
				gtk_tree_store_set (model, &piter1,
						DVD_TITLE_COLUMN,			str,
						DVD_PIXBUF_COLUMN_PLAY,		NULL,
						DVD_TIME_COLUMN,			"",
						DVD_CHANNELS_COLUMN,		"",
						DVD_PIXBUF_CHOICE_COLUMN,	NULL,
						DVD_PIXBUF_FILE_NORMALIZE,	NULL,
						DVD_NAME_COLUMN,			"",
						DVD_NAME_EDITABLE_COLUMN,	FALSE,
						DVD_POINTER_STRUCT_COLUMN,	var,
						-1);

				g_free (str);	str = NULL;
				break;

			case _DVD_DATA_ALL :
				var->OldChannels = var->NewChannels = (gint)atoi (var->StrNumberChannel);

				NameDest = dvd_get_name_to_treview (var);
				time = g_strdup_printf ("<b>%s</b>", var->time);

				gtk_tree_store_append (model, &piter4, &piter1);
				gtk_tree_store_set (model, &piter4,
						DVD_TITLE_COLUMN,			var->def,
						DVD_PIXBUF_COLUMN_PLAY,		var->GUINT_TempsTotal == 0 ? NULL : var_dvd.Pixbuf_FilePlay,
						DVD_TIME_COLUMN,			time,
						DVD_CHANNELS_COLUMN,		dvd_get_channels (var),
						DVD_PIXBUF_CHOICE_COLUMN,	var->GUINT_TempsTotal == 0 ? NULL : var_dvd.Pixbuf_Coche,
						DVD_PIXBUF_FILE_NORMALIZE,	var->GUINT_TempsTotal == 0 ? NULL : var_dvd.Pixbuf_Coche,
						DVD_NAME_COLUMN,			var->GUINT_TempsTotal == 0 ? NULL : NameDest,
						DVD_NAME_EDITABLE_COLUMN,	TRUE,
						DVD_POINTER_STRUCT_COLUMN,	var,
						-1);

				g_free (time);		time = NULL;
				g_free (NameDest);	NameDest = NULL;
				break;

			case _DVD_CHAPTER :
				str = g_strdup_printf ("<span color=\"black\"><b>%s</b></span>", var->def);

				gtk_tree_store_append (model, &piter2, &piter1);
				gtk_tree_store_set (model, &piter2,
						DVD_TITLE_COLUMN,			str,
						DVD_PIXBUF_COLUMN_PLAY,		NULL,
						DVD_TIME_COLUMN,			"",
						DVD_CHANNELS_COLUMN,		"",
						DVD_PIXBUF_CHOICE_COLUMN,	NULL,
						DVD_PIXBUF_FILE_NORMALIZE,	NULL,
						DVD_NAME_COLUMN,			"",
						DVD_NAME_EDITABLE_COLUMN,	FALSE,
						DVD_POINTER_STRUCT_COLUMN,	var,
						-1);

				g_free (str);	str = NULL;
				break;

			case _DVD_DATA :
				var->OldChannels = var->NewChannels = (gint)atoi (var->StrNumberChannel);

				NameDest = dvd_get_name_to_treview (var);
				time = g_strdup_printf ("<b>%s</b>", var->time);

				gtk_tree_store_append (model, &piter3, &piter2);
				gtk_tree_store_set (model, &piter3,
						DVD_TITLE_COLUMN,		var->def,
						DVD_PIXBUF_COLUMN_PLAY,		var->GUINT_TempsTotal == 0 ? NULL : var_dvd.Pixbuf_FilePlay,
						DVD_TIME_COLUMN,			time,
						DVD_CHANNELS_COLUMN,		dvd_get_channels (var),
						DVD_PIXBUF_CHOICE_COLUMN,	var->GUINT_TempsTotal == 0 ? NULL : var_dvd.Pixbuf_Coche,
						DVD_PIXBUF_FILE_NORMALIZE,	var->GUINT_TempsTotal == 0 ? NULL : var_dvd.Pixbuf_Coche,
						DVD_NAME_COLUMN,			var->GUINT_TempsTotal == 0 ? NULL : NameDest,
						DVD_NAME_EDITABLE_COLUMN,	TRUE,
						DVD_POINTER_STRUCT_COLUMN,	var,
						-1);

				g_free (time);	time = NULL;
				g_free (NameDest);	NameDest = NULL;
				break;

			}
		}
		/* Détacher le pointeur de la structure */
		list->data = NULL;

		/* Liste suivante */
		list = g_list_next (list);
	}

	// AJUSTEMENT DU SCROLL VERTICAL
	gtk_adjustment_set_value (Adj, AdjValue);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (var_dvd.Adr_scroll), Adj);
}
//
//
gboolean dvd_foreach_normalise (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	gint	 value = GPOINTER_TO_INT (user_data);
	VAR	*var = NULL;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);

	if (var->type_list_dvd == _DVD_DATA_ALL || var->type_list_dvd == _DVD_DATA) {
		if( var->GUINT_TempsTotal > 0 ) {
			var->EtatNormalise = value;

			gtk_tree_store_set (
				GTK_TREE_STORE (model),
				iter,
				DVD_PIXBUF_FILE_NORMALIZE,
				dvd_get_pixbuf_normalise (var->EtatNormalise),
				-1
				);
		}
	}

	/* continuer à parcourir l'arbre */
	return FALSE;
}
//
//
void dvd_set_flag_normalise (gboolean EtatNormalise)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_normalise, (gpointer)(glong)EtatNormalise);
}
//
//
gboolean dvd_foreach_func_if (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	VAR        *var = NULL;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);
	if (var && (var->type_list_dvd == _DVD_DATA || var->type_list_dvd == _DVD_DATA_ALL)) {

		if (var->EtatChoix == _SELECTED_ || var->EtatChoix == _CH_2_) {
			var_dvd.bool_selected = TRUE;
			var_dvd.total_selected ++;
		}

		if (var->EtatChoix == _CH_2_) {
			if (var->OldChannels > 3) var_dvd.bool_sa = TRUE;
			var_dvd.bool_selected = TRUE;
			var_dvd.total_selected_2ch ++;
		}

	}

	/* continuer à parcourir l'arbre */
	return FALSE;
}
//
//
gboolean dvd_foreach_is_norm_selected (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	VAR	*var = NULL;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);

	if (var->type_list_dvd == _DVD_DATA_ALL || var->type_list_dvd == _DVD_DATA) {

		if (var->EtatNormalise == TRUE) var_dvd.BoolNormIsSelected = TRUE;
	}

	return FALSE;
}
//
//
gboolean dvd_get_is_norm_selected (void)
{
	var_dvd.BoolNormIsSelected = FALSE;
	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_is_norm_selected, NULL);
	return (var_dvd.BoolNormIsSelected);
}
/*
	gchar *str = NULL;

	if (GlistDvd == NULL) {
		str = g_strdup (" ");
	}
	else {
		if (var_dvd.bool_err == 0) {
			str = g_strdup_printf (_("Total selection%s: %d,     2ch: %d"),
				var_dvd.total_selected > 1 ? _("s") : "",
				var_dvd.total_selected,
				var_dvd.total_selected_2ch
				);
		}
		else {
			str = g_strdup(_("<b>Ce fichier ne peut etre extrait !</b>"));
			var_dvd.bool_err --;
		}
	}
	StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, str );
	g_free (str);	str = NULL;

*/

//
//
void dvd_set_flag_buttons_dvd (void)
{
	var_dvd.bool_selected = FALSE;
	var_dvd.bool_sa = FALSE;
	var_dvd.total_selected_2ch = 0;
	var_dvd.total_selected = 0;

	if (GlistDvd == NULL) {
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_deplier_dvd")), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_replier_dvd")), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_extraction_dvd")), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("frame187")), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("frame110")), FALSE);
		if (var_dvd.Adr_combobox_normalise_dvd != NULL)
			gtk_widget_set_sensitive (GTK_WIDGET (GTK_COMBO_BOX (var_dvd.Adr_combobox_normalise_dvd)), FALSE);
	}
	else {
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_deplier_dvd")), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_replier_dvd")), TRUE);
		gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_func_if, NULL);
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_extraction_dvd")), var_dvd.total_selected > 0);
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("frame187")), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("frame110")), var_dvd.bool_sa);

		gtk_widget_set_sensitive (GTK_WIDGET (GTK_COMBO_BOX (var_dvd.Adr_combobox_normalise_dvd)), dvd_get_is_norm_selected ());
	}
	StatusBar_puts();
}
//
//
gboolean dvd_foreach_stop_play (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	VAR        *var = NULL;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);
	if (var) {
		if (var->type_list_dvd == _DVD_DATA_ALL || var->type_list_dvd == _DVD_DATA) {
			var->BoolDvdPlay = FALSE;
			gtk_tree_store_set (GTK_TREE_STORE (model), iter, DVD_PIXBUF_COLUMN_PLAY, var_dvd.Pixbuf_FilePlay, -1);
		}
	}
	 /* continuer à parcourir l'arbre */
	return FALSE;
}
//
//
gboolean dvd_foreach_affiche_play (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	VAR        *var = NULL;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);
	if (var) {
		if (var->type_list_dvd == _DVD_DATA_ALL || var->type_list_dvd == _DVD_DATA) {
			if (var->BoolDvdPlay == FALSE)
			gtk_tree_store_set (GTK_TREE_STORE (model), iter, DVD_PIXBUF_COLUMN_PLAY, var_dvd.Pixbuf_FilePlay, -1);
			else
			gtk_tree_store_set (GTK_TREE_STORE (model), iter, DVD_PIXBUF_COLUMN_PLAY, var_dvd.Pixbuf_FileStop, -1);
		}
	}
	 /* continuer à parcourir l'arbre */
	return FALSE;
}
//
//
void dvd_set_etat_music_pixbuf (gboolean *PtrBoolDvdPlay, gboolean p_BoolDvdPlay)
{
	static gboolean *BoolDvdPlay = NULL;

	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_stop_play, NULL);

	if (PtrBoolDvdPlay != NULL)
		BoolDvdPlay = PtrBoolDvdPlay;
	if (BoolDvdPlay != NULL) {
		*BoolDvdPlay = p_BoolDvdPlay;
	}

	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_affiche_play, NULL);
}
//
//
void dvd_set_icone_stop (void)
{
	dvd_set_etat_music_pixbuf (NULL, FALSE);
}
//
//
void dvd_set_etat_music_pixbuf_with_detail (VAR *p_var, gboolean EtatPlay)
{
	/* Tous les flags et widget sur FILE_ETAT_PLAY_ATTENTE */

	if (EtatPlay == TRUE) {

	}
}
//
//
void dvd_pixbuf_stop_play (void)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_stop_play, NULL);
}
//
//
gboolean dvd_event_click_mouse (GtkWidget *treeview, GdkEventButton *event, gpointer data)
{
	gint                Pos_X = 0, Pos_Y = 0;
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	gint                i;
	// guint               button;
	GtkTreeModel       *model = (GtkTreeModel *)data;
	GtkTreeIter         iter;
	VAR                *var = NULL;
	GdkPixbuf          *Pixbuf = NULL;
	gboolean            bool_key_Control = (keys.keyval == GDK_KEY_Control_L || keys.keyval == GDK_KEY_Control_R);
	gboolean            bool_key_Shift   = (keys.keyval == GDK_KEY_Shift_L || keys.keyval == GDK_KEY_Shift_R);
	gboolean            bool_key_Release = (bool_key_Control == FALSE &&  bool_key_Shift == FALSE);
	gboolean            bool_click_gauche = (event->button == 1);
	gboolean            bool_click_droit = (event->button == 3);

	gboolean            BoolSelectColTitres = FALSE;
	gboolean            BoolSelectColPlay = FALSE;
	gboolean            BoolSelectColTemps = FALSE;
	gboolean            BoolSelectColFormat = FALSE;
	gboolean            BoolSelectColChoix = FALSE;
	gboolean            BoolSelectColNormalise = FALSE;
	gboolean            BoolSelectColNom = FALSE;

	// Mise a jour du scroll vertical
	GtkAdjustment   *Adj = NULL;
	gdouble          AdjValue;
	// COORDONNEES DU SCROLL VERTICAL
	Adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (var_dvd.Adr_scroll));
	AdjValue = gtk_adjustment_get_value (Adj);

	var_dvd.bool_click = FALSE;

	/* Single clicks only */
	if (event->type != GDK_BUTTON_PRESS) return (FALSE);

	/*if (bool_key_Release == FALSE || bool_click_gauche == FALSE) return (FALSE);*/
	if (bool_key_Release == FALSE) return (FALSE);

	/* Si pas de selection a cet endroit retour */
	if (FALSE == gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					  (gint)event->x, (gint)event->y,
					   &path, &column, &Pos_X, &Pos_Y)) return (FALSE);

	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			/* g_print ("\tNUM IS: %d\n", i); */
			switch ( i ) {
			case NUM_TREE_Titres :		BoolSelectColTitres		= TRUE;	break;
			case NUM_TREE_Play :		BoolSelectColPlay		= TRUE;	break;
			case NUM_TREE_Temps :		BoolSelectColTemps		= TRUE;	break;
			case NUM_TREE_Format :		BoolSelectColFormat		= TRUE;	break;
			case NUM_TREE_Choix :		BoolSelectColChoix		= TRUE;	break;
			case NUM_TREE_Normalise :	BoolSelectColNormalise		= TRUE;	break;
			case NUM_TREE_Nom :		BoolSelectColNom		= TRUE;	break;
			default: return (FALSE);
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}
	if( BoolSelectColNom );
	if( BoolSelectColFormat );
	if( BoolSelectColTemps );
	if( BoolSelectColTitres );

	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (model, &iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);

	// if (var && (var->type_list_dvd == _DVD_DATA || var->type_list_dvd == _DVD_DATA_ALL)) {
	if (var && var->GUINT_TempsTotal > 0 && (var->type_list_dvd == _DVD_DATA || var->type_list_dvd == _DVD_DATA_ALL)) {

		// button = ((GdkEventButton*)event)->button;
		if (bool_click_droit && BoolSelectColNormalise == TRUE) {
			popup_normalise_dvd ();
		}

		else if (bool_click_gauche && BoolSelectColNormalise == TRUE) {

			var->EtatNormalise = (var->EtatNormalise == TRUE) ? FALSE : TRUE;

			gtk_tree_store_set (
				GTK_TREE_STORE (model),
				&iter, DVD_PIXBUF_FILE_NORMALIZE,
				dvd_get_pixbuf_normalise (var->EtatNormalise),
				-1
				);

			dvd_set_flag_buttons_dvd ();
		}

		else if (bool_click_gauche && BoolSelectColChoix == TRUE) {

			if (var->OldChannels == 2) {
				switch (var->EtatChoix) {
				case _COCHE_ :
					var->EtatChoix = _CH_2_;
					Pixbuf = var_dvd.Pixbuf_2ch;
					var->NewChannels = 2;
					break;
				case _CH_2_ :
					var->EtatChoix = _COCHE_;
					Pixbuf = var_dvd.Pixbuf_Coche;
					var->NewChannels = -1;
					break;
				case _SELECTED_ :
					break;
				}
			}
			else if (var->OldChannels > 2) {
				switch (var->EtatChoix) {
				case _COCHE_ :
					var->EtatChoix = _CH_2_;
					Pixbuf = var_dvd.Pixbuf_2ch;
					var->NewChannels = 2;
					break;
				case _CH_2_ :
					var->EtatChoix = _SELECTED_;
					Pixbuf = var_dvd.Pixbuf_Selected;
					var->NewChannels = var->OldChannels;
					break;
				case _SELECTED_ :
					var->EtatChoix = _COCHE_;
					Pixbuf = var_dvd.Pixbuf_Coche;
					var->NewChannels = -1;
					break;
				}
			}
			gtk_tree_store_set (GTK_TREE_STORE (model), &iter, DVD_PIXBUF_CHOICE_COLUMN, Pixbuf, -1);
			dvd_set_flag_buttons_dvd ();
		}
		else if (bool_click_gauche && BoolSelectColPlay == TRUE) {

			if (TRUE == mplayer_is_used ()) {

				wind_info_init (
					WindMain,
					_("MPLAYER already in action"),
					_("MPLAYER is already in use  !!"),
					  "");
			}
			else {

				GList	*list = NULL;
				// APPEL DE mplayer_init AVANT WinReader_open ()
				mplayer_init (LIST_MPLAYER_FROM_DVD, (gdouble)var->GUINT_TempsTotal, var_dvd.GUINT_TempsTotal, WinReader_close, WinReader_set_value, dvd_set_icone_stop, WinReader_is_close);
				// WinReader_open ("Dvd Read");
				wind_reader_init( WindMain );
				WinReader_set_pause ();

				list = g_list_append (list, g_strdup ("-vo"));
				list = g_list_append (list, g_strdup ("null"));
				list = g_list_append (list, g_strdup ("-chapter"));
				if (*var->StrNumerateChapter == '0')
					list = g_list_append (list, g_strdup ("1"));
				else	list = g_list_append (list, g_strdup_printf ("%s-%s", var->StrNumerateChapter, var->StrNumerateChapter));
				list = g_list_append (list, g_strdup_printf ("dvd://%d", var->NumerateTitle));
				list = g_list_append (list, g_strdup ("-dvd-device"));
				list = g_list_append (list, g_strdup (scan_get_text_combo_cd (_DVD_)));
				list = g_list_append (list, g_strdup ("-aid"));
				list = g_list_append (list, g_strdup_printf ("%d",libutils_hexa_to_int (var->StrNumerateStreamId)));
				mplayer_set_list (list);
				list = libutils_remove_glist (list);

				gtk_tree_model_get (model, &iter, DVD_PIXBUF_COLUMN_PLAY, &Pixbuf, -1);
				// AFFICHE L ICONE DE PLAY
				dvd_set_etat_music_pixbuf (&var->BoolDvdPlay, TRUE);
			}
		}

		/* position du curseur a l'instant du click */
		if (Pos_X < 10 || Pos_X > 22) return (FALSE);
		if (Pos_Y < 6 || Pos_Y > 18) return (FALSE);

		var_dvd.bool_click = TRUE;
	}
	// AJUSTEMENT DU SCROLL VERTICAL
	gtk_adjustment_set_value (Adj, AdjValue);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (var_dvd.Adr_scroll), Adj);
	while (gtk_events_pending()) gtk_main_iteration();
	return (FALSE);
}
//
//
static void dvd_cell_edited_namefile (GtkCellRendererText *cell,
                                            gchar       *path_string,
                                            gchar       *new_text,
                                            gpointer     data)
{
	GtkTreeModel *model = (GtkTreeModel *)data;
	GtkTreePath  *path = gtk_tree_path_new_from_string (path_string);
	GtkTreeIter   iter;
	gint          column = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cell), "column"));
	VAR          *var = NULL;

	gtk_tree_model_get_iter (model, &iter, path);

	if (column == DVD_NAME_COLUMN) {
		gchar   *old_text;
		gchar	*NameDest = NULL;

		gtk_tree_model_get (model, &iter, DVD_NAME_COLUMN, &old_text, -1);
		g_free (old_text);
		old_text = NULL;

		gtk_tree_model_get (model, &iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);
		g_free (var->name_file);
		var->name_file = NULL;

		// UN CHAMPS VIDE ACTUALISE LE NOM D ORIGINE
		if (0 == strlen (new_text))
			var->name_file = g_strdup (var->StaticNameFile);
		else	var->name_file = g_strdup (new_text);

		// actualise le nom
		NameDest = dvd_get_name_to_treview (var);
		gtk_tree_store_set (GTK_TREE_STORE (model), &iter, column, NameDest, -1);
		g_free (NameDest);	NameDest = NULL;

	}
	gtk_tree_path_free (path);
}
//
//
gboolean dvd_event (GtkWidget *treeview, GdkEvent *event, gpointer user_data)
{
	gint                x, y;
	GdkModifierType     state;
	GtkTreeIter         iter;
	gint                Pos_X = 0, Pos_Y = 0;
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	gint                i;
	GtkTreeModel       *model = (GtkTreeModel *)user_data;
	VAR                *var = NULL;
	gchar              *str = NULL;
	GdkPixbuf          *Pixbuf = NULL;

	gboolean            BoolSelectColTitres = FALSE;
	gboolean            BoolSelectColPlay = FALSE;
	gboolean            BoolSelectColTemps = FALSE;
	gboolean            BoolSelectColFormat = FALSE;
	gboolean            BoolSelectColChoix = FALSE;
	gboolean            BoolSelectColNormalise = FALSE;
	gboolean            BoolSelectColNom = FALSE;

	if (keys.keyval == GDK_KEY_PRESS) {
		return (FALSE);
	}

	if (event->type == GDK_ENTER_NOTIFY) {
		var_dvd.NumStruct = 0;
		var_dvd.bool_dedans = TRUE;
	}
	else if (event->type == GDK_LEAVE_NOTIFY) {
		var_dvd.NumStruct = 0;
		var_dvd.bool_dedans = FALSE;
		dvd_set_flag_buttons_dvd ();
		return (FALSE);
	}
	else if (event->type == GDK_MOTION_NOTIFY)  {
		var_dvd.bool_dedans = TRUE;
	}

	if (var_dvd.bool_dedans == FALSE) {
		var_dvd.NumStruct = 0;
		dvd_set_flag_buttons_dvd ();
		return (FALSE);
	}


	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &x, &y, &state );
	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &x, &y, &state);
	if (FALSE == gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					   x, y,
					   &path, &column, &Pos_X, &Pos_Y)) {
		dvd_set_flag_buttons_dvd ();
		return (FALSE);
	}

	/* Recuperation de la structure */
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_dvd.Adr_Tree_Model, &iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);
	if (var == NULL) return (FALSE);
	if( var->GUINT_TempsTotal == 0 ) {
		StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, " ");
		StatusBar_puts(  );
		return( FALSE );
	}

	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			/* g_print ("\tNUM IS: %d\n", i); */
			switch ( i ) {
			case NUM_TREE_Titres :		BoolSelectColTitres		= TRUE;	break;
			case NUM_TREE_Play :		BoolSelectColPlay		= TRUE;	break;
			case NUM_TREE_Temps :		BoolSelectColTemps		= TRUE;	break;
			case NUM_TREE_Format :		BoolSelectColFormat		= TRUE;	break;
			case NUM_TREE_Choix :		BoolSelectColChoix		= TRUE;	break;
			case NUM_TREE_Normalise :	BoolSelectColNormalise		= TRUE;	break;
			case NUM_TREE_Nom :		BoolSelectColNom		= TRUE;	break;
			default: return (FALSE);
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}
	if( BoolSelectColFormat );
	if( BoolSelectColTemps );
	if( BoolSelectColTitres );

	if (BoolSelectColNormalise == TRUE) {
		if (var->EtatNormalise == TRUE)	{
			switch (dvd_get_value_normalise_dvd ()) {
			case 0 :
				StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu)  / Maximum volume amplification of a group of files keeping the level difference between each of them."
));
				
				break;
			case 1 :
				StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, _("PEAK: Maximum volume amplification of each file."));
				break;
			}
		}
		else {
			StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / ReplayGain: Waiting for selection."));
		}
	}

	else if (BoolSelectColNom == TRUE) {

		if (var_dvd.NumStruct == var->NumStruct) return (FALSE);
		var_dvd.NumStruct = var->NumStruct;

		if (var->name_file && *var->name_file) {
			str = g_strdup_printf ("%s.wav", var->name_file);
			StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, str );
			g_free (str);
			str = NULL;
		}
		else {
			StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, " " );
		}
	}

	else if (BoolSelectColChoix == TRUE) {

		if (var->name_file && *var->name_file == '\0' )return (FALSE);

		/* position du curseur a l'instant du click */
		if (Pos_X < 10 || Pos_X > 38 || Pos_Y < 6 || Pos_Y > 22) {
			dvd_set_flag_buttons_dvd ();
			return (FALSE);
		}

		if (var->OldChannels == 2) {
			switch (var->EtatChoix) {
			case _COCHE_ :
				str = g_strdup (_("Waiting for selection. A click will activate the conversion to 2 channels."));
				StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_,  str );
				break;
			case _SELECTED_ :
				/*
				str = g_strdup_printf (_("Conversion vers %s canaux."), var->StrNumberChannel);
				// main_puts_statusbar_global (str);
				StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_,  str );
				PRINT("SELECTED");
				*/
				break;
			case _CH_2_ :
				str = g_strdup_printf (_("Conversion to 2 channels."));
				StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_,  str );
				break;
			}
		}
		else if (var->OldChannels > 2) {
			switch (var->EtatChoix) {
			case _COCHE_ :
				str = g_strdup (_("Waiting for selection. A click will activate the conversion to 2 channels."));
				StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_,  str );
				break;
			case _SELECTED_ :
				str = g_strdup_printf (_("Conversion to %s channels."), var->StrNumberChannel);
				StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_,  str );
				break;
			case _CH_2_ :
				str = g_strdup_printf (
					_("Conversion to 2 channels. A second click will activate the conversion to %s channels."), var->StrNumberChannel);
				StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, str );
				break;
			}
		}
		if (str != NULL) {
			g_free (str);
			str = NULL;
		}
	}

	else if (BoolSelectColPlay == TRUE) {

		gtk_tree_model_get (model, &iter, DVD_PIXBUF_COLUMN_PLAY, &Pixbuf, -1);
		if (Pixbuf == var_dvd.Pixbuf_FileStop) {
			StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, _("Playing music."));
		}
		else if (Pixbuf == var_dvd.Pixbuf_FilePlay) {
			StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, _("Music paused."));
		}
		else {
			StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, " ");
		}
	}

	else {
		gchar *str = NULL;

		if (GlistDvd == NULL) {
			str = g_strdup (" ");
		}
		else {
			if (var_dvd.bool_err == 0) {
				str = g_strdup_printf (_("Total selection%s: %d,     2ch: %d"),
					var_dvd.total_selected > 1 ? _("s") : "",
					var_dvd.total_selected,
					var_dvd.total_selected_2ch
					);
			}
			else {
				str = g_strdup(_("<b>This file can not be ripped !</b>"));
				var_dvd.bool_err --;
			}
		}
		StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_, str );
		g_free (str);	str = NULL;

		dvd_set_flag_buttons_dvd ();
	}
	StatusBar_puts(  );

	return (FALSE);
}
//
//
void dvd_changed_selection_row_file_dvd (GtkTreeSelection *selection, gpointer data)
{
	var_dvd.Adr_Line_Selected = selection;
}
//
//
gboolean dvd_foreach_normalise_column (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	VAR	*var = NULL;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);

	if (var->type_list_dvd == _DVD_DATA_ALL || var->type_list_dvd == _DVD_DATA) {

		var->EtatNormalise = (var->EtatNormalise == TRUE) ? FALSE : TRUE;
		gtk_tree_store_set (
			GTK_TREE_STORE (model),
			iter,
			DVD_PIXBUF_FILE_NORMALIZE,
			dvd_get_pixbuf_normalise (var->EtatNormalise),
			-1
			);
	}

	/* continuer à parcourir l'arbre */
	return FALSE;
}
//
//
void dvd_selected_column (GtkTreeViewColumn *treeviewcolumn, gpointer user_data)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_normalise_column, NULL);
}
//
//
gboolean dvd_foreach_update (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	VAR		*var = NULL;
	GdkPixbuf	*Pixbuf = NULL;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);

	if (var->type_list_dvd == _DVD_DATA_ALL || var->type_list_dvd == _DVD_DATA) {

		/* Raffraihcissement COCHE */
		switch (var->EtatChoix) {
		case _COCHE_ :
			Pixbuf = var_dvd.Pixbuf_Coche;
			var->NewChannels = -1;
			break;
		case _CH_2_ :
			Pixbuf = var_dvd.Pixbuf_2ch;
			var->NewChannels = 2;
			break;
		case _SELECTED_ :
			Pixbuf = var_dvd.Pixbuf_Selected;
			var->NewChannels = var->OldChannels;
			break;
		}
		gtk_tree_store_set (GTK_TREE_STORE (model), iter, DVD_PIXBUF_CHOICE_COLUMN, Pixbuf, -1);

		/* Raffraihcissement NORMALISE */
		gtk_tree_store_set (GTK_TREE_STORE (model), iter, DVD_PIXBUF_FILE_NORMALIZE, dvd_get_pixbuf_normalise (var->EtatNormalise), -1);
	}
	/* continuer à parcourir l'arbre */
	return FALSE;
}
//
//
void dvd_update (void)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_update, NULL);
}
// INIT TREVIEW
//
static void dvd_add_columns (GtkTreeView *treeview)
{
	gint               col_offset;
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel      *model = gtk_tree_view_get_model (treeview);

	var_dvd.NumStruct = 0;
	var_dvd.bool_dedans = FALSE;

	// SIGNAL : 'event'
	g_signal_connect(G_OBJECT(treeview), "event", (GCallback) dvd_event, model);

	// SIGNAL : 'button-press-event'
	g_signal_connect(G_OBJECT(treeview), "button-press-event", (GCallback) dvd_event_click_mouse, model);

	// SIGNAL : Ligne actuellement selectionnee 'changed'
	var_dvd.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	g_signal_connect(G_OBJECT(var_dvd.Adr_Line_Selected),
			 "changed",
                   	 G_CALLBACK(dvd_changed_selection_row_file_dvd),
                   	 "1");

	/* DVD_TITLE_COLUMN */
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.0, NULL);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							-1,
							_("Titles"),	renderer,
							"markup",	DVD_TITLE_COLUMN,
							NULL);
	var_dvd.Adr_Column_Titres =
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);

	/* DVD_PIXBUF_COLUMN_PLAY */
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							-1,
							_("Play"), renderer,
							"pixbuf", DVD_PIXBUF_COLUMN_PLAY,
							NULL);
	var_dvd.Adr_Column_Play =
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);

	/* DVD_TIME_COLUMN */
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							-1,
							_("Time"),	renderer,
							"markup",	DVD_TIME_COLUMN,
							NULL);
	var_dvd.Adr_Column_Temps =
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);

	/* DVD_CHANNELS_COLUMN */
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							-1,
							_("Format"),	renderer,
							"markup",	DVD_CHANNELS_COLUMN,
							NULL);
	var_dvd.Adr_Column_Format =
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);

	/* DVD_PIXBUF_CHOICE_COLUMN */
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							-1,
							_("Choice"), renderer,
							"pixbuf", DVD_PIXBUF_CHOICE_COLUMN,
							NULL);
	var_dvd.Adr_Column_Choix =
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);

	/* ITEM_WAV_SELECT_COLUMN */
	/*
	renderer = gtk_cell_renderer_toggle_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint *)ITEM_WAV_SELECT_COLUMN);
	g_signal_connect (renderer, "toggled", G_CALLBACK (dvd_item_toggled_wav), model);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							-1,
							"Wav", renderer,
							"active",
							ITEM_WAV_SELECT_COLUMN,
							"visible",
							VISIBLE_COLUMN,
							"activatable",
							WORLD_COLUMN, NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	*/

	/* DVD_PIXBUF_FILE_NORMALIZE
	*/
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							-1,
							_("Normalise"),	renderer,
							"pixbuf",	DVD_PIXBUF_FILE_NORMALIZE,
							NULL);
	var_dvd.Adr_Column_Normalise =
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (dvd_selected_column),
			GINT_TO_POINTER(0));

	/* DVD_NAME_COLUMN */
	var_dvd.Renderer =
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.0, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint *)DVD_NAME_COLUMN);
	g_signal_connect (renderer, "edited", G_CALLBACK (dvd_cell_edited_namefile), model);
	col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							-1,
							_("Name"),
							renderer,
							"markup", DVD_NAME_COLUMN,
							 "editable", DVD_NAME_EDITABLE_COLUMN,
							NULL);
	var_dvd.Adr_Column_Nom =
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
}
//
//
void on_scrolledwindow_dvd_audio_realize (GtkWidget *widget, gpointer user_data)
{
	/* Chargement des PIXBUF */
	var_dvd.Pixbuf_FilePlay    = libutils_init_pixbufs ("xcfa/no_play.png");
	var_dvd.Pixbuf_FileStop    = libutils_init_pixbufs ("xcfa/sol.png");
	var_dvd.Pixbuf_Coche       = libutils_init_pixbufs ("xcfa/coche.png");
	var_dvd.Pixbuf_Coche_Exist = libutils_init_pixbufs ("xcfa/coche_exist.png");
	var_dvd.Pixbuf_Selected    = libutils_init_pixbufs ("xcfa/selected.png");
	var_dvd.Pixbuf_2ch         = libutils_init_pixbufs ("xcfa/2ch.png");

	var_dvd.Adr_scroll = widget;

	var_dvd.Adr_Tree_Model = (GtkTreeModel *) gtk_tree_store_new (DVD_NUM_COLUMNS,
					G_TYPE_STRING,		/* DVD_TITLE_COLUMN		*/
					GDK_TYPE_PIXBUF,	/* DVD_PIXBUF_COLUMN_PLAY	*/
					G_TYPE_STRING,		/* DVD_TIME_COLUMN		*/
					G_TYPE_STRING,		/* DVD_CHANNELS_COLUMN		*/
					GDK_TYPE_PIXBUF,	/* DVD_PIXBUF_CHOICE_COLUMN	*/
					GDK_TYPE_PIXBUF,	/* DVD_PIXBUF_FILE_NORMALIZE	*/
					G_TYPE_STRING,		/* DVD_NAME_COLUMN		*/
					G_TYPE_BOOLEAN,		/* DVD_NAME_EDITABLE_COLUMN	*/
					G_TYPE_POINTER		/* DVD_POINTER_STRUCT_COLUMN	*/
					);

	/* create tree view */
	var_dvd.Adr_TreeView = gtk_tree_view_new_with_model (var_dvd.Adr_Tree_Model);
	g_object_unref (var_dvd.Adr_Tree_Model);
	// gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (var_dvd.Adr_TreeView), TRUE);

	dvd_add_columns (GTK_TREE_VIEW (var_dvd.Adr_TreeView));

	gtk_container_add (GTK_CONTAINER (widget), var_dvd.Adr_TreeView);

	gtk_widget_show_all (widget);
}
//
//
void on_combobox_peripherique_dvd_realize (GtkWidget *widget, gpointer user_data)
{
	GList *List = NULL;
	MEDIA *Media = NULL;

	List = g_list_first (scan_get_glist ());
	while (List) {
		if ((Media = (MEDIA *)List->data)) {
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), (gchar *)Media->Full_Name );
		}
		List = g_list_next(List);
	}
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("DVD file structure"));
	if( Config.NumSelectComboBoxDvd < 0 ) Config.NumSelectComboBoxDvd = 0;
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.NumSelectComboBoxDvd);
	var_dvd.Adr_ComboBox_Reader = GTK_COMBO_BOX (widget);
}
//
//
void  dvd_pthread_reffresh_list (void *data)
{
	var_dvd.bool_end_pthread = FALSE;
	var_dvd.bool_read_dvd = TRUE;

	dvdread_remove_list ();

	/*
	if (dvdread_dvd_found () == FALSE) {
		var_dvd.bool_read_dvd = FALSE;
	}
	else {
		if (var_dvd.bool_halt == FALSE) {
			if (dvdread_dvd_read () == TRUE) {
				var_dvd.bool_read_dvd = TRUE;
			}
			else {
				var_dvd.bool_read_dvd = FALSE;
			}
		}
	}
	*/
	var_dvd.bool_read_dvd = dvdread_dvd_read();

	var_dvd.bool_end_pthread = TRUE;
	pthread_exit (0);
}
//
//
static gint dvd_timeout (gpointer data)
{
	if (var_dvd.bool_end_pthread == TRUE) {

		if (var_dvd.bool_halt == TRUE) {
			// main_puts_statusbar_global (_("Arret par l'utilisateur"));
		}
		else {
			if (var_dvd.bool_read_dvd == TRUE) {
				dvd_affiche_list ();
				gtk_tree_view_expand_all ( (GtkTreeView *)var_dvd.Adr_TreeView);
			}
		}

		dvd_set_flag_buttons_dvd ();

		if (var_dvd.bool_read_dvd == FALSE) {
			StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_,  _("The DVD player is absent."));
		}
		else {
			StatusBar_set_mess( NOTEBOOK_DVD_AUDIO, _STATUSBAR_SIMPLE_,  "" );
		}

		g_source_remove (var_dvd.Handler_Timeout);
		WindScan_close ();
	}
	else if (var_dvd.bool_halt == TRUE) {
		// conv_stop_conversion ();
	}

	return (TRUE);
}
//
//
void dvd_reffresh_list_dvd_action (void)
{
	pthread_t nmr_tid;

	if (TRUE == mplayer_is_used ()) {

		wind_info_init (
			WindMain,
			_("MPLAYER already in action"),
			_("MPLAYER is already in use  !!"),
			  "");
		return;
	}

	dvd_remove_GtkTree ();

	var_dvd.PathDvd = scan_get_text_combo_cd (_DVD_);
	// g_print("---------------> var_dvd.PathDvd = %s\n",var_dvd.PathDvd );

	// WindScan_open (_("Scan ..."), WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Scan ...",
		WINDSCAN_PULSE
		);
	var_dvd.bool_read_dvd = TRUE;
	var_dvd.bool_halt = FALSE;
	var_dvd.bool_end_pthread = FALSE;
	var_dvd.Handler_Timeout = g_timeout_add (100, dvd_timeout, 0);
	pthread_create (&nmr_tid, NULL ,(void *)dvd_pthread_reffresh_list, (void *)NULL);
	// pthread_detach (nmr_tid);
}
//
//
void dvd_reffresh_list_directory_dvd (gchar *path)
{
	gchar	*Ptr = NULL;

	var_dvd.from.bool_dvd = FALSE;
	if( NULL != var_dvd.from.path ) {
		g_free (var_dvd.from.path);	var_dvd.from.path = NULL;
	}
	var_dvd.from.path = g_strdup (path);

	if( NULL != Config.PathChoiceFileDVD ) {
		g_free (Config.PathChoiceFileDVD);	Config.PathChoiceFileDVD = NULL;
	}
	Config.PathChoiceFileDVD = g_strdup (path);

	if (NULL != (Ptr = strrchr (Config.PathChoiceFileDVD, '/'))) {
		*Ptr = '\0';
	}
	dvd_reffresh_list_dvd_action ();
}
//
//
gboolean dvd_bool_read_dvd_from_directory (void)
{
	guint nbr = g_list_length (scan_get_glist ());
	gint  Nmr_Combo_Actif = gtk_combo_box_get_active (var_dvd.Adr_ComboBox_Reader);

	return (Nmr_Combo_Actif >= nbr ? TRUE : FALSE);
}
//
//
void dvd_reffresh_list_dvd (void)
{
	if (NULL == var_dvd.Adr_ComboBox_Reader) return;

	if (libutils_find_file ("lsdvd") == FALSE) {

		wind_info_init (
			WindMain,
			_("lsdvd is missing  !"),
			_("You must install the program: lsdvd"),
			  "");

		// g_print("VEUILLEZ INSTALLER L APPLICATION [ LSDVD  ]\n");
		return;
	}

	if (dvd_bool_read_dvd_from_directory () == TRUE) {
		fileselect_create (_PATH_LOAD_ONE_FILE_, Config.PathChoiceFileDVD, dvd_reffresh_list_directory_dvd);
	}
	else {
		var_dvd.from.bool_dvd = TRUE;
		g_free (var_dvd.from.path);
		var_dvd.from.path = NULL;
		// mplayer_fifo_quit ();
		dvd_reffresh_list_dvd_action ();
	}
}
//
//
void on_button_destination_dvd_realize (GtkWidget *widget, gpointer user_data)
{
	var_dvd.bool_selected = FALSE;
	dvd_set_flag_buttons_dvd ();

	var_dvd.Adr_button_destination_dvd = GTK_BUTTON (widget);
	gtk_button_set_use_underline (GTK_BUTTON (var_dvd.Adr_button_destination_dvd), FALSE);
	gtk_button_set_label (GTK_BUTTON (var_dvd.Adr_button_destination_dvd), Config.PathDestinationDVD);
}
//
//
void dvd_maj_destination_DVD (gchar *path)
{
	if (libutils_test_write (path) == TRUE) {
		g_free (Config.PathDestinationDVD);
		Config.PathDestinationDVD = NULL;
		Config.PathDestinationDVD = g_strdup (path);
		gtk_button_set_label (GTK_BUTTON (var_dvd.Adr_button_destination_dvd), Config.PathDestinationDVD);
	}
}
//
//
void on_button_destination_dvd_clicked (GtkButton *button, gpointer user_data)
{
	fileselect_create (_PATH_CHOICE_DESTINATION_, Config.PathDestinationDVD, dvd_maj_destination_DVD);
}
//
//
void dvd_button_annuler_lecture_dvd_clicked (void)
{
	var_dvd.bool_halt = TRUE;
}
//
//
gboolean dvd_foreach_func (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
	VAR               *var = NULL;
	gchar             *chapter = NULL;
	gchar             *param = NULL;
	gchar             *ptr = NULL;
	NEW_DVD_EXTRACT   *StructDvdExtract = NULL;
	gchar            **Larrbuf = NULL;
	gint               cpt;

	gtk_tree_model_get (model, iter, DVD_POINTER_STRUCT_COLUMN, &var, -1);

	if (var && (var->type_list_dvd == _DVD_DATA || var->type_list_dvd == _DVD_DATA_ALL)) {

		if (var->EtatChoix == _SELECTED_ || var->EtatChoix == _CH_2_) {

			if (*var->StrNumerateChapter == '0')
				chapter = g_strdup ("-chapter 1");
			else	chapter = g_strdup_printf ("-chapter %s-%s", var->StrNumerateChapter, var->StrNumerateChapter);

			if (var->format_id == _AC3_) {
				ptr = dvdtable_get (var->OldChannels, var->NewChannels);
				param = g_strdup_printf ("-rawaudio format=0x0100 -demuxer -rawaudio -channels %d -af pan=%d:%s",
					var->OldChannels,
					var->NewChannels,
					ptr);
				/*
				param = g_strdup_printf ("-rawaudio format=0x2000 -channels %d -af pan=%d:%s",
					var->OldChannels,
					var->NewChannels,
					ptr);
				*/
				g_free (ptr);
				ptr = NULL;
			}
			else if (var->format_id == _DTS_) {
				if (var->OldChannels == var->NewChannels) {
					param = g_strdup_printf ("-af channels=%d", var->OldChannels);
				}
				else {
					ptr = dvdtable_get (var->OldChannels, var->NewChannels);
					param = g_strdup_printf ("-channels %d -af pan=%d:%s",
						var->OldChannels,
						var->NewChannels,
						ptr);
					g_free (ptr);
					ptr = NULL;
				}
			}
			else if (var->format_id == _INCONNU_) {
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("\nFORMAT == _INCONNU_\n\tExtraction comme AC3\n");

				ptr = dvdtable_get (var->OldChannels, var->NewChannels);
				param = g_strdup_printf ("-channels %d -af pan=%d:%s",
					var->OldChannels,
					var->NewChannels,
					ptr);
				g_free (ptr);
				ptr = NULL;
			}

			/* Allocate Structure DVD_EXTRACT */
			/* dvd_extract = (DVD_EXTRACT *)g_malloc0 (sizeof (DVD_EXTRACT)); */

			/* And set datas */
			/*
			dvd_extract->Name        = g_strdup_printf ("%s/%s.wav", Config.PathDestinationDVD, var->name_file);
			dvd_extract->NameTemp    = g_strdup_printf ("%s/_%04d_tmp_xcfa.wav", Config.PathDestinationDVD, var_dvd.int_temp);
			var_dvd.int_temp ++;

			dvd_extract->LineCommand =
				g_strdup_printf ("nice -n %s mplayer -vo null -ao pcm:file=%s %s dvd://%s -dvd-device %s -aid %d %s",
					conv.valuenice,
					dvd_extract->NameTemp,
					chapter,
					var->StrNumerateTitle,
					scancd_get_text_combo_cd (_DVD_),
					utils_hexa_to_int (var->StrNumerateStreamId),
					param
					);

			g_print ("dvd_extract->LineCommand =%s\n", dvd_extract->LineCommand);

			GlistDvdExtract = g_list_append (GlistDvdExtract, dvd_extract);
			*/

			/* NOUVEAU */
			StructDvdExtract = (NEW_DVD_EXTRACT *)g_malloc0 (sizeof (NEW_DVD_EXTRACT));

			StructDvdExtract->Path = g_strdup_printf ("%s/%s.wav", Config.PathDestinationDVD, var->name_file);
			StructDvdExtract->Var  = var;

			StructDvdExtract->EtatNormalise = var->EtatNormalise;

			if (var->EtatNormalise == TRUE)	{
				switch (dvd_get_value_normalise_dvd ()) {
				case 0 : var_dvd.TTNormPeakCollectif ++;	break;
				case 1 : var_dvd.TTNormPeak ++;			break;
				}
			}

			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("nice"));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("-n"));
			// StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("0"));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup_printf ("%d", Config.Nice));

			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("mplayer"));

			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("-nojoystick"));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("-nolirc"));

			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("-vo"));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("null"));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("-ao"));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list,
				g_strdup_printf ("pcm:file=%s/%s.wav", Config.PathDestinationDVD, var->name_file));

			Larrbuf = g_strsplit (chapter, " ", 0);
			for (cpt=0; Larrbuf[cpt]; cpt++) {
				StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup (Larrbuf[cpt]));
			}
			g_strfreev(Larrbuf);

			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup_printf ("dvd://%s", var->StrNumerateTitle));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("-dvd-device"));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup (scan_get_text_combo_cd (_DVD_)));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup ("-aid"));
			StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup_printf ("%d",libutils_hexa_to_int (var->StrNumerateStreamId)));

			Larrbuf = g_strsplit (param, " ", 0);
			for (cpt=0; Larrbuf[cpt]; cpt++) {
				StructDvdExtract->list = g_list_append (StructDvdExtract->list, g_strdup (Larrbuf[cpt]));
			}
			g_strfreev(Larrbuf);

			GlistDvdExtract = g_list_append (GlistDvdExtract, StructDvdExtract);

			g_free (param);		param = NULL;
		}
		else {
			var->EtatNormalise = FALSE;
		}
	}

	 /* continuer à parcourir l'arbre */
	return FALSE;
}
//
//
void on_button_extraction_dvd_clicked (GtkButton *button, gpointer user_data)
{
	var_dvd.int_temp = 0;
	var_dvd.TTNormPeak          = 0;
	var_dvd.TTNormPeakCollectif = 0;

	gtk_tree_model_foreach (GTK_TREE_MODEL(var_dvd.Adr_Tree_Model), dvd_foreach_func, NULL);

	// mplayer_fifo_quit ();
	dvdextract_dvd_to_file ();
}
//
//
void on_combobox_sub_dvd_realize (GtkWidget *widget, gpointer user_data)
{
	var_dvd.Adr_combobox_sub_dvd = GTK_COMBO_BOX (widget);

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Normal");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "-3dB");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "-6dB");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), 0);
}
//
//
void on_combobox_ambiance_dvd_realize (GtkWidget *widget, gpointer user_data)
{
	var_dvd.Adr_combobox_ambiance_dvd = GTK_COMBO_BOX (widget);
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Normal");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "-3dB");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "-6dB");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), 0);
}
//
//
void on_combobox_normalise_dvd_realize (GtkWidget *widget, gpointer user_data)
{
	var_dvd.Adr_combobox_normalise_dvd = GTK_COMBO_BOX (widget);
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Peak/Album");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Peak");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), 0);
}
//
//
gint dvd_get_value_normalise_dvd (void)
{
	if (var_dvd.Adr_combobox_normalise_dvd == NULL) return (-1);
	return (gtk_combo_box_get_active (var_dvd.Adr_combobox_normalise_dvd));
}
//
// SOLVED
// 	Debian Bug report logs - #673640
// 	xcfa: XCFA crashes when the CD is ejected
// 	http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=673640
//
void on_button_eject_dvd_realize( GtkWidget *widget, gpointer user_data )
{
	// g_print( "on_button_eject_dvd_realize\n" );
	// g_print( "\tscan_get_glist() = %p\n", scan_get_glist () );
	gtk_widget_set_sensitive( widget, scan_get_glist() == NULL ? FALSE : TRUE );
}
void on_button_eject_dvd_clicked (GtkButton *button, gpointer user_data)
{
	dvd_remove_GtkTree ();
	dvdread_remove_list ();
	dvd_set_flag_buttons_dvd ();
	scan_eject_media (_DVD_);
}
//
//
void on_combobox_peripherique_dvd_changed (GtkComboBox *combobox, gpointer user_data)
{
	if( NULL != var_dvd.Adr_ComboBox_Reader ) {
		Config.NumSelectComboBoxDvd = gtk_combo_box_get_active (GTK_COMBO_BOX (var_dvd.Adr_ComboBox_Reader));
		dvd_reffresh_list_dvd ();
	}
}
//
//
void on_button_rafraichir_dvd_clicked (GtkButton *button, gpointer user_data)
{
	dvd_reffresh_list_dvd ();
}
//
//
void on_button_deplier_dvd_clicked (GtkButton *button, gpointer user_data)
{
	gtk_tree_view_expand_all ( (GtkTreeView *)var_dvd.Adr_TreeView);
}
//
//
void on_button_replier_dvd_clicked (GtkButton *button, gpointer user_data)
{
	gtk_tree_view_collapse_all ( (GtkTreeView *)var_dvd.Adr_TreeView);
}
//
//
void dvd_from_popup (TYPE_SET_FROM_POPUP_DVD TypeSetFromPopup, gboolean EtatNormalise)
{
	switch (TypeSetFromPopup) {

	// SELECTION ou DESELECTION POUR LES ICONES DE CONVERSIONS

	case CD_NORMALISE_SELECT_V :		// Deselection verticale
		dvd_set_flag_normalise (FALSE);
		break;
	case CD_NORMALISE_DESELECT_V :		// Selection verticale
		dvd_set_flag_normalise (TRUE);
		break;

	}
}

