 /*
 *  file      : cd_cue.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gprintf.h>
#include <string.h>
#include <string.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cd_audio.h"
#include "get_info.h"
#include "scan.h"
#include "configuser.h"
#include "parse.h"
#include "cd_cue.h"





typedef struct {
	guint	 handler_timeout;
} VAR_CDCUE;

VAR_CDCUE cdcue;

BASE_IOCTL BaseIoctl = { 0, NULL, NULL, NULL, NULL, NULL };





// 
// 
gboolean cdcue_is_alloc (void)
{
	return ((BaseIoctl.Cue == NULL) ? FALSE : TRUE);
}
// 
// 
gint cdcue_get_total_tracks (gchar *buffer)
{
	gchar  **Larrbuf = NULL;
	gint     i;
	gint     piste;
	gchar    *ptr;

	Larrbuf = g_strsplit(buffer, "\n", 0);
	for (i=0; Larrbuf[i]; i++) {
		if (strstr (Larrbuf[i], "TOTAL")) {
			i --;
			break;
		}
	}
	ptr = Larrbuf[i];
	while (*ptr == ' ') ptr ++;
	piste = atoi (ptr);
	g_strfreev(Larrbuf);
	return (piste);
}
// 
// 
void cdcue_remove_base_ioctl (void)
{
	BaseIoctl.TotalTracks = 0;
	if (BaseIoctl.Datas != NULL) {
		g_free (BaseIoctl.Datas);
		BaseIoctl.Datas = NULL;
	}
	
	// g_free (BaseIoctl.PathNameDestFileCue);	BaseIoctl.PathNameDestFileCue = NULL;
	if( NULL != BaseIoctl.Performer ) {
		g_free (BaseIoctl.Performer);		BaseIoctl.Performer = NULL;
	}
	if( NULL != BaseIoctl.Title ) {
		g_free (BaseIoctl.Title);		BaseIoctl.Title = NULL;
	}
	if( NULL != BaseIoctl.File ) {
		g_free (BaseIoctl.File);		BaseIoctl.File = NULL;
	}
	
	if (BaseIoctl.Cue != NULL) {
		gint i;
		for (i = 0; i < BaseIoctl.TotalTracks; i++) {
			if( NULL != BaseIoctl.Cue [ i ].Performer ) {
				g_free (BaseIoctl.Cue [ i ].Performer);		BaseIoctl.Cue [ i ].Performer = NULL;
			}
			if( NULL != BaseIoctl.Cue [ i ].Title ) {
				g_free (BaseIoctl.Cue [ i ].Title);		BaseIoctl.Cue [ i ].Title = NULL;
			}
			if( NULL != BaseIoctl.Cue [ i ].Partiel_Title ) {
				g_free (BaseIoctl.Cue [ i ].Partiel_Title);	BaseIoctl.Cue [ i ].Partiel_Title = NULL;
			}
		}
		g_free (BaseIoctl.Cue);
		BaseIoctl.Cue = NULL;
	}
}
// 
// 
void cdcue_alloc_base_ioctl (gint p_taille)
{
	gint	index;
	
	cdcue_remove_base_ioctl ();

	BaseIoctl.TotalTracks = p_taille;
	BaseIoctl.Datas = (BASE_IOCTL_DATAS *)g_malloc0 (sizeof(BASE_IOCTL_DATAS) * (p_taille +4));
	
	BaseIoctl.Cue = (CDCUE *)g_malloc0 (sizeof(CDCUE) * (p_taille +4));
	
	// Aucune extraction
	for (index = 0; index < p_taille; index ++)
		BaseIoctl.Cue [ index ].BoolExtract = FALSE;
}
// 
// 
void cdcue_set_BoolExtract (gint p_index, gboolean p_flag)
{
	BaseIoctl.Cue [ p_index ].BoolExtract = p_flag;
}
// 
// 
void cdcue_print_base_ioctl (void)
{
	gint   base;
	// gint   TempDiv;
	// gint   TempMod;
	
	if( FALSE == OptionsCommandLine.BoolVerboseMode ) return;
	
	// 1. length   12923 [02:52:23]  begin      32 [00:00:32]	cdparanoia [.begin<44 ? 0 : begin][.(length + begin) -1] -d /dev/hdb -O 0 "1.wav"
	// 2. length   14175 [03:09:00]  begin   12955 [02:52:55]	cdparanoia [.begin][.length -1] -d /dev/hdb -O 0 "2.wav"
	// 3. length   14627 [03:15:02]  begin   27130 [06:01:55]	cdparanoia [.begin][.length -1] -d /dev/hdb -O 0 "3.wav"
	// 20. length   15848 [03:31:23]  begin  303192 [67:22:42]	cdparanoia [.303192]-[.15847] -d /dev/sr0 -O 0 "20.wav"
	g_print("  ------------------ B A S E ----------------------------       ---------------- E X T R A C T I O N -----------------\n" );
	for (base = 0; base < BaseIoctl.TotalTracks; base++) {
		// TempDiv = BaseIoctl.Datas[ base ].length / 75;
		// TempMod = BaseIoctl.Datas[ base ].length % 75;
		
		g_print("  %2d. ", base +1);
		g_print("length %7d [%02d:%02d:%02d]  ",
			BaseIoctl.Datas[ base ].length,
			BaseIoctl.Datas[ base ].length_min,
			BaseIoctl.Datas[ base ].length_sec,
			BaseIoctl.Datas[ base ].length_cent);
		
		// TempDiv = BaseIoctl.Datas[ base ].begin / 75;
		// TempMod = BaseIoctl.Datas[ base ].begin % 75;
	
		g_print("begin %7d [%02d:%02d:%02d]",
			BaseIoctl.Datas[ base ].begin,
			BaseIoctl.Datas[ base ].begin_min,
			BaseIoctl.Datas[ base ].begin_sec,
			BaseIoctl.Datas[ base ].begin_cent);
		
		g_print("\tcdparanoia [.%d]-[.%d] -d %s -O 0 \"%d.wav\"",
			BaseIoctl.Datas[ base ].begin < 44 ? 0 : BaseIoctl.Datas[ base ].begin,
			base == 0
				? 
				(BaseIoctl.Datas[ base ].length + BaseIoctl.Datas[ base ].begin) -1
				:
				BaseIoctl.Datas[ base ].length -1,
			EnteteCD.NameCD_Device,
			base +1
			);
		
		if (BaseIoctl.Cue [ base ].BoolExtract == TRUE)
			g_print(" -> Extract");
		
		g_print("\n");
	}

	// TempDiv = BaseIoctl.Datas[ BaseIoctl.TotalTracks ].begin / 75;
	// TempMod = BaseIoctl.Datas[ BaseIoctl.TotalTracks ].begin % 75;
	
	g_print("TOTAL %7d [%02d:%02d:%02d]  ",
			BaseIoctl.Datas[ BaseIoctl.TotalTracks ].begin,
			BaseIoctl.Datas[ BaseIoctl.TotalTracks ].begin_min,
			BaseIoctl.Datas[ BaseIoctl.TotalTracks ].begin_sec,
			BaseIoctl.Datas[ BaseIoctl.TotalTracks ].begin_cent);
	g_print("\n\n");
}
// 
// 
void cdcue_make_cue (void)
{
	gint	piste;
	
	if (BaseIoctl.Cue == NULL) return;
	
	for (piste = 0; piste < BaseIoctl.TotalTracks; piste ++) {
		BaseIoctl.Cue [ piste ].BeginTime.total  = BaseIoctl.Datas[ piste ].begin / 75;
		BaseIoctl.Cue [ piste ].BeginTime.min    = BaseIoctl.Datas[ piste ].begin_min;
		BaseIoctl.Cue [ piste ].BeginTime.sec    = BaseIoctl.Datas[ piste ].begin_sec;
		BaseIoctl.Cue [ piste ].BeginTime.cent   = BaseIoctl.Datas[ piste ].begin_cent;
					
		if (BaseIoctl.Cue [ piste ].Title == NULL) {
			BaseIoctl.Cue [ piste ].Title = g_strdup_printf ("Track_%02d", piste + 1);
		}
	}

	for (piste = 0; piste < BaseIoctl.TotalTracks; piste ++) {
		if (piste > 0) {
			
			BaseIoctl.Cue [ piste ].EndTime.total  = BaseIoctl.Cue [ piste ].BeginTime.total - 3;
			BaseIoctl.Cue [ piste ].EndTime.sec    = BaseIoctl.Cue [ piste ].EndTime.total % 60;
			BaseIoctl.Cue [ piste ].EndTime.total -= BaseIoctl.Cue [ piste ].EndTime.sec;
			BaseIoctl.Cue [ piste ].EndTime.min    = BaseIoctl.Cue [ piste ].EndTime.total / 60;
			BaseIoctl.Cue [ piste ].EndTime.cent   = BaseIoctl.Cue [ piste ].BeginTime.cent;
		}
	}
}
// 
// 
void cdcue_write_cue_ok (void)
{
	gint	 piste;
	gchar	*PathName = NULL;
	FILE	*fp;
	gchar	*StrLine = NULL;
	
	PathName = g_strdup_printf ("%s/%s.cue", (gchar *)Config.PathDestinationCD, BaseIoctl.Performer);
	
	PRINT("Production du fichier:");
	g_print("\t%s\n", PathName );

	fp = fopen (PathName, "w");

	fprintf (fp,   "REM Cue file written by Xcfa\n\n");

	fprintf (fp,   "PERFORMER \"%s\"\n", BaseIoctl.Performer);
	fprintf (fp,   "TITLE \"%s\"\n", BaseIoctl.Title);
	fprintf (fp,   "FILE \"%s\" WAVE\n", BaseIoctl.File);
	for (piste = 0; piste < BaseIoctl.TotalTracks; piste ++) {
		fprintf (fp,   "  TRACK %02d AUDIO\n", piste +1);
		fprintf (fp,   "    PERFORMER \"%s\"\n", BaseIoctl.Cue [ piste ].Performer);
		
		// PARSING DU TITRE DE LA PISTE
		// fprintf (fp,   "    TITLE \"%s\"\n", BaseIoctl.Cue [ piste ].Title);
		StrLine = Parse_get_line( PARSE_TYPE_TITLE_CD, piste );
		fprintf (fp,   "    TITLE \"%s\"\n", StrLine);
		g_free( StrLine );
		StrLine = NULL;

		if (piste > 0)
		fprintf (fp,   "    INDEX 00 %02d:%02d:%02d\n", BaseIoctl.Cue [ piste ].EndTime.min, BaseIoctl.Cue [ piste ].EndTime.sec, BaseIoctl.Cue [ piste ].EndTime.cent);
		fprintf (fp,   "    INDEX 01 %02d:%02d:%02d\n", BaseIoctl.Cue [ piste ].BeginTime.min, BaseIoctl.Cue [ piste ].BeginTime.sec, BaseIoctl.Cue [ piste ].BeginTime.cent);
// g_print ("    PERFORMER \"%s\"\n", BaseIoctl.Cue [ piste ].Performer);
// g_print ("    TITLE \"%s\"\n", BaseIoctl.Cue [ piste ].Title);
// if (piste > 0)
// g_print ("    INDEX 00 %02d:%02d:%02d\n", BaseIoctl.Cue [ piste ].FMin, BaseIoctl.Cue [ piste ].FSec, BaseIoctl.Cue [ piste ].FCent);
// g_print ("    INDEX 01 %02d:%02d:%02d\n", BaseIoctl.Cue [ piste ].DMin, BaseIoctl.Cue [ piste ].DSec, BaseIoctl.Cue [ piste ].DCent);
	}
	fprintf (fp,   "\n");

	fclose (fp);
	g_free (PathName);
	PathName = NULL;
		
	// FIN de creation CUE-FILE
	var_cd.TypeCreateCue = TYPE_CUE_NONE;
}
// 
// ADD == 0 : no change
// ADD == 1 : + 3 secondes
// ADD == 2 : - 3 secondes
// 
void cdcue_calcul_start (gint piste, gint Add)
{
	gint	 TempDiv = 0;
	gint	 TempMod = 0;
	
	if (Add == 0) {
		TempDiv = BaseIoctl.Cue [ piste ].CuePartiel.Start / 75;
		TempMod = BaseIoctl.Cue [ piste ].CuePartiel.Start % 75;
	}
	else if (Add == 1) {
		TempDiv = (BaseIoctl.Cue [ piste ].CuePartiel.Start +32) / 75;
		TempMod = (BaseIoctl.Cue [ piste ].CuePartiel.Start +32) % 75;
	}
	else if (Add == 2) {
		TempDiv = (BaseIoctl.Cue [ piste ].CuePartiel.Start -32) / 75;
		TempMod = (BaseIoctl.Cue [ piste ].CuePartiel.Start -32) % 75;
	}
	BaseIoctl.Cue [ piste ].CuePartiel.StartMin  = TempDiv / 60;
	BaseIoctl.Cue [ piste ].CuePartiel.StartSec  = TempDiv % 60;
	BaseIoctl.Cue [ piste ].CuePartiel.StartCent = TempMod;
}
/*
REM Cue file written by Xcfa

PERFORMER "James Brown"
TITLE "Sex Machine - The Very Best Of James Brown"
FILE "aurelie.wav" WAVE
  TRACK 01 AUDIO
    PERFORMER "James Brown"
    TITLE "Please"
    INDEX 01 00:00:32		DEBUT	Please
  TRACK 02 AUDIO
    PERFORMER "James Brown"
    TITLE "Think"
    INDEX 00 02:44:00		FIN	Please
    INDEX 01 02:45:00		DEBUT	Think
  TRACK 03 AUDIO
    PERFORMER "James Brown"
    TITLE "Nigth"
    INDEX 00 05:00:00		FIN	Think
    INDEX 01 05:00:32		DEBUT	Nigth
*/
// 
// 
void cdcue_write_cue_ok_partiel (void)
{
	gint	 piste;
	gint	 track;
	gint	 old_piste = 0;
	gchar	 *Ptr = NULL;
	FILE	*fp;
	gchar	*PathName = NULL;
	gchar	*StrLine = NULL;
	
	// Initialisation
	for (piste = 0; piste < BaseIoctl.TotalTracks; piste ++) {
		BaseIoctl.Cue [ piste ].CuePartiel.Start     = 0;
		BaseIoctl.Cue [ piste ].CuePartiel.StartMin  = 0;
		BaseIoctl.Cue [ piste ].CuePartiel.StartSec  = 0;
		BaseIoctl.Cue [ piste ].CuePartiel.StartCent = 0;
		
		BaseIoctl.Cue [ piste ].CuePartiel.Len     = 0;
	}
	
	// init Len
	for (piste = 0; piste < BaseIoctl.TotalTracks; piste ++) {
		BaseIoctl.Cue [ piste ].CuePartiel.Len = BaseIoctl.Datas[ piste ].length;
	}
	
	// Init Start ZERO
	BaseIoctl.Cue [ 0 ].CuePartiel.Start = BaseIoctl.Datas[ 0 ].begin;
	
	// Init Start ZERO
	old_piste = 0;
	for (piste = 0; piste < BaseIoctl.TotalTracks; piste ++) {
		if (BaseIoctl.Cue [ piste ].BoolExtract == TRUE) {
			
			BaseIoctl.Cue [ piste  ].CuePartiel.Start = BaseIoctl.Cue [ old_piste ].CuePartiel.Start + BaseIoctl.Cue [ piste ].CuePartiel.Len;
			cdcue_calcul_start (piste, 0);
			old_piste = piste;
		}
	}
	
	for (piste = 0; piste < BaseIoctl.TotalTracks; piste ++) {
		
		g_print ("%2d  LEN [ %7d ]    START [ %7d ]  %02d:%02d:%02d   %s  %s\n",
			piste,
			BaseIoctl.Cue [ piste ].CuePartiel.Len,
			BaseIoctl.Cue [ piste ].CuePartiel.Start,
			BaseIoctl.Cue [ piste ].CuePartiel.StartMin,
			BaseIoctl.Cue [ piste ].CuePartiel.StartSec,
			BaseIoctl.Cue [ piste ].CuePartiel.StartCent,
			BaseIoctl.Cue [ piste ].BoolExtract == TRUE ? "EXTRACT" : "       ",
			BaseIoctl.Cue [ piste ].Title
			);
	}
	
	Ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_name_file_cue));
	PathName = g_strdup_printf ("%s/%s.cue", (gchar *)Config.PathDestinationCD, Ptr);
	
	fp = fopen (BaseIoctl.PathNameDestFileCue, "w");
	
	g_print("\n");
	fprintf (fp,   "REM Cue file written by Xcfa\n\n");

	fprintf (fp,   "PERFORMER \"%s\"\n", BaseIoctl.Performer);
	fprintf (fp,   "TITLE \"%s\"\n", BaseIoctl.Title);


	switch (gtk_combo_box_get_active ( GTK_COMBO_BOX ( GTK_WIDGET (GLADE_GET_OBJECT("combobox_choice_file_cue"))))) {
	case 0 : fprintf (fp,   "FILE \"%s.wav\" WAVE\n", Ptr);	break;
	case 1 : fprintf (fp,   "FILE \"%s.flac\" WAVE\n", Ptr);	break;
	case 2 : fprintf (fp,   "FILE \"%s.ogg\" WAVE\n", Ptr);	break;
	case 3 : fprintf (fp,   "FILE \"%s.mpc\" WAVE\n", Ptr);	break;
	}
	
	old_piste = 0;
	for (track = 0, piste = 0; piste < BaseIoctl.TotalTracks; piste ++) {
		
		if (BaseIoctl.Cue [ piste ].BoolExtract == FALSE) continue;
		
		fprintf (fp,   "  TRACK %02d AUDIO\n", track +1);
		
		fprintf (fp,   "    PERFORMER \"%s\"\n", BaseIoctl.Cue [ piste ].Performer);
		// fprintf (fp,   "    TITLE \"%s\"\n", BaseIoctl.Cue [ piste ].Title);
		StrLine = Parse_get_line( PARSE_TYPE_TITLE_CD, track );
		fprintf (fp,   "    TITLE \"%s\"\n", StrLine );
		g_free( StrLine );
		StrLine = NULL;
		
		if (track == 0) {
			fprintf (fp,   "    INDEX 01 00:00:00\n");
			track ++;
			old_piste = piste;
			continue;
		}
		if (track > 0) {
			cdcue_calcul_start (old_piste, 2);
			fprintf (fp,   "    INDEX 00 %02d:%02d:%02d\n",
				BaseIoctl.Cue [ old_piste ].CuePartiel.StartMin,
				BaseIoctl.Cue [ old_piste ].CuePartiel.StartSec,
				BaseIoctl.Cue [ old_piste ].CuePartiel.StartCent);
			cdcue_calcul_start (old_piste, 1);
		}

		fprintf (fp,   "    INDEX 01 %02d:%02d:%02d\n",
				BaseIoctl.Cue [ old_piste ].CuePartiel.StartMin,
				BaseIoctl.Cue [ old_piste ].CuePartiel.StartSec,
				BaseIoctl.Cue [ old_piste ].CuePartiel.StartCent);

		track ++;
		old_piste = piste;
	}
	
	fclose (fp);
	g_free (PathName);
	PathName = NULL;
		
	// FIN de creation CUE-FILE
	var_cd.TypeCreateCue = TYPE_CUE_NONE;
}
// 
// 
void cdcue_write_cue (void)
{
	// gchar	*PathName = NULL;
	// gchar	*Str = NULL;
	// ggchar	*Ptr = NULL;
	
	if (BaseIoctl.Cue == NULL) return;
	
	switch (var_cd.TypeCreateCue) {
	case TYPE_CUE_NONE :
		break;
		
	case TYPE_CUE_FILE :
		if (gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_creation_fichier_cue"))) == TRUE) {
			/*
			if (libutils_test_file_exist (BaseIoctl.PathNameDestFileCue) == TRUE) {
				
				Str = g_strdup_printf (_("<b>\nLe fichier existe:\n%s       \nVoulez vous le remplacer  ?       \n</b>"), BaseIoctl.PathNameDestFileCue);
				windchoice_open (
					_("Le fichier existe"),
					Str,
					TRUE,
					cdcue_write_cue_ok_partiel
					);
				break;
			}
			*/
			cdcue_write_cue_ok_partiel ();
		}
		break;
		
	case TYPE_CUE_CD :
		/*
		PathName = g_strdup_printf ("%s/%s.cue", (gchar *)Config.PathDestinationCD, BaseIoctl.Performer);
		
		if (libutils_test_file_exist (PathName) == TRUE) {
			
			Str = g_strdup_printf (_("<b>\nLe fichier existe:\n%s       \nVoulez vous le remplacer  ?       \n</b>"), PathName);
			windchoice_open (
				_("Le fichier existe"),
				Str,
				TRUE,
				cdcue_write_cue_ok
				);
			break;
		}
		*/
		cdcue_write_cue_ok ();
		break;
	}
	
	// FIN de creation CUE-FILE
	var_cd.TypeCreateCue = TYPE_CUE_NONE;
	
	/*
	if (PathName != NULL) {
		g_free (PathName);
		PathName = NULL;
	}
	if  (Str != NULL) {
		g_free (Str);
		Str = NULL;
	}
	Ptr = NULL;
	*/
}
// 
// 
static gint cdcue_timeout (gpointer data)
{
	if (var_cd.bool_timeout_read_toc_cd == FALSE) {
		
		// Creation de fichier CUE
		cdcue_write_cue ();
		
		// FIN timeout
		g_source_remove (cdcue.handler_timeout);
	}
	
	return (TRUE);
}
// Demande de creation CUE totale
// 
void on_button_cue_creation_from_cd_clicked (GtkButton *button, gpointer user_data)
{
	
	if (cdcue_is_alloc () == TRUE) {
		
		// DEBUT de creation de fichier CUE
		var_cd.TypeCreateCue = TYPE_CUE_CD;
		
		cdcue_write_cue ();
	}
	else {
		// DEBUT de creation de fichier CUE
		var_cd.TypeCreateCue = TYPE_CUE_CD;

		// Pré-initialisations indispensable
		var_cd.bool_thread_read_toc_cd = TRUE;
		var_cd.bool_timeout_read_toc_cd = TRUE;
		
		// Le timer de fin
		cdcue.handler_timeout = g_timeout_add (100, cdcue_timeout, 0);
		
		// Et action
		cdaudiotoc_reffresh_list_cd ();
	}
}
// 
// 
void on_entry_name_fichier_unique_cue_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_entry_name_file_cue = widget;
}
// 
// 
void on_entry_name_fichier_unique_cue_changed (GtkEditable *editable, gpointer user_data)
{
	gchar        *ptr_template = NULL;
	gchar        *str = NULL;
	gchar        *ptr = NULL;
	
	if (var_cd.Adr_entry_name_file_cue == NULL) return;

	ptr_template = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_name_file_cue));
	
	// Suppression du caracteres '/' interdit si il existe
	str = g_strdup (ptr_template);
	if (strchr (str, '/')) {

		while ((ptr = strchr (str, '/'))) {
			strcpy (ptr, ptr+1);
		}
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_name_file_cue), str);

		// utils_puts_statusbar_global (_("Le caractere  /  est interdit  !!!"));
	}

	g_free (str);
	str = NULL;

}
// 
// 
void on_checkbutton_creation_fichier_cue_clicked (GtkButton *button, gpointer user_data)
{
	if (gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (button)) == TRUE)
		gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_creation_fichier_unique_cue")), TRUE);
}











