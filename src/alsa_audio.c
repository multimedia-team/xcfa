 /*
 *  file      : alsa_audio.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  Code de lecture audio pour fichiers de type WAV reprit et retravaille
 *  depuis:
 *  	Analyse code depuis: wavbreaker
 *  	par Timothy Robinson et Thomas Perl
 *  	http://wavbreaker.sourceforge.net/
 *
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>

#include <errno.h>
#include <alsa/asoundlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "alsa_audio.h"



VAR_ALSA VarAlsa = {
	NULL,		// snd_pcm_t	*playback_handle;					// Handle
	0,			// guint	bytesPerFrame;							// 8, 16, 24, 32, 64 ...
	0,			// guint	channels;								// 1, 2, 4 ...
	0,			// gint		SampleRate;								// Frequence: 44100 ...
	0,			// gint		TotalChunckSize;						// 
	NULL,		// guchar	*buffer;								// Pointeur de buffer
	0,			// guint	SizeBuffer;								// Taille requise du buffer
	0,			// gint		Sec;									// Duree total en secondes
	0.0,		// gdouble	Percent;								// Poucentage en retour sur la duree total
	0.0,		// gdouble	PercentSel;								// Poucentage en retour depuis la selection
	
	0.0,		// gdouble	PercentBegin;							// 
	0.0,		// gdouble	PercentEnd;								// 
	
	FALSE,		// gboolean	BoolThreadEnd;							// 
	0,			// guint	HandlerTimeout;							// 
	
	TRUE,		// gboolean	StopPlay;								// 
	TRUE,		// gboolean	PauseAlsa;								// 
	NULL,		// void		(*FuncSetValueTime) (gdouble p_value);	// 
	NULL,		// void		(*FuncIconeStop) (void);				// 
	
	NULL		// FILE		*pFile;									// Handler du fichier ouvert
};


void AlsaAudio_close_device (void)
{
	if (VarAlsa.playback_handle != NULL ) {
		snd_pcm_close (VarAlsa.playback_handle);
	}
	VarAlsa.playback_handle = NULL;
}

gboolean AlsaAudio_write(int size)
{
	gint			err;
	snd_pcm_uframes_t	uframes;
	
	/*
	* Alsa takes a short value for each sample.  Also, the size parameter
	* is the number of frames.  So, I divide the size by the number
	* of channels multiplied by 2.  The 2 is the difference between
	* the size of a char and a short.
	*/
	uframes = size / ( VarAlsa.channels * VarAlsa.bytesPerFrame );
	err = snd_pcm_writei( VarAlsa.playback_handle, VarAlsa.buffer, uframes );	
	if (err < 0 ) {
		/*
		* This code should let us recover from a buffer underrun in ALSA.
		* This happens, for example, when the user backgrounds (^Z) the 
		* wavbreaker process while playing back audio or if the machine is 
		* too slow to keep up with normal playback speed.
		*
		* After every error, we have to call "snd_pcm_prepare" and hope 
		* that ALSA recovers. If not, we have a problem, so spit out an 
		* error message to let the user know something's wrong.
		*/
// 
// TODO ESTRPIPE
// Mon, 06 Dec 2010 23:59:33 +0100
// 
// ESTRPIPE is not define under KFreeBSD :/
// MODIF POUR COMPIL CORRECTE DANS KFreeBSD suite a un mail de Christian Marillat:
// 
// Christian Marillat a écrit :
// Il ne vaut pas mieux utiliser BSM_ERRNO_ESTRPIPE à la place de 92 ?
// Pour Kfreebsd c'est '__FreeBSD_kernel__'
// Christian
// 
#ifdef __FreeBSD_kernel__
		#include <bsm/audit_errno.h>
		if( err == -BSM_ERRNO_ESTRPIPE ) {
			while( (err = snd_pcm_resume (VarAlsa.playback_handle)) == -EAGAIN ) {
				sleep( 1);
			}
		}
		err = snd_pcm_prepare (VarAlsa.playback_handle);

		if( err != 0 ) {
			fprintf( stderr, "write to audio interface failed (%s)\n", snd_strerror( err));
		}
#else
// 
// FIXME
// 
// ESTRPIPE is not define under HURD :/
// https://buildd.debian.org/fetch.cgi?pkg=xcfa;ver=4.0.5~beta2-1;arch=hurd-i386;stamp=1301412239
// ESTRPIPE  undeclared (first use in this function)
// 
#ifdef ESTRPIPE
		if( err == -ESTRPIPE ) {
			while( (err = snd_pcm_resume (VarAlsa.playback_handle)) == -EAGAIN ) {
				sleep( 1);
			}
		}
		err = snd_pcm_prepare (VarAlsa.playback_handle);

		if( err != 0 ) {
			fprintf( stderr, "write to audio interface failed (%s)\n", snd_strerror( err));
		}
#endif
#endif
	}

	return ( ( err == 0 ) ? TRUE : FALSE );
}

gboolean AlsaAudio_open_device(
	const gchar *audio_dev,		// "default"
	gint p_bitsPerSample,		// 8, 16, 24, ...
	gint p_channels,			// 1, 2, 4, , ...
	guint p_samplesPerSec,		// 44100, ...
	guint *p_bufferSize			// Taille buffer retournee
	)
{
	gint err;
	gint dir;
	guint rate;
	guint rrate;
	snd_pcm_format_t format;
	snd_pcm_hw_params_t *hw_params;
	snd_pcm_uframes_t buffer_size;
	snd_pcm_uframes_t period_size;
	
	guint buffer_time = 500000;         /* ring buffer length in us */
	guint period_time = 100000;         /* period time in us */

	VarAlsa.bytesPerFrame = 2;
	format = SND_PCM_FORMAT_S16_LE;

	if (p_bitsPerSample == 8 ) {
		VarAlsa.bytesPerFrame = 1;
		format = SND_PCM_FORMAT_U8;
	}
	else if (p_bitsPerSample == 16 ) {
		VarAlsa.bytesPerFrame = 2;
		format = SND_PCM_FORMAT_S16_LE;
	}
	else if (p_bitsPerSample == 24 ) {
		VarAlsa.bytesPerFrame = 3;
		format = SND_PCM_FORMAT_S24_3LE;
	}
	else if (p_bitsPerSample == 32 ) {
		VarAlsa.bytesPerFrame = 4;
		format = SND_PCM_FORMAT_S32;
	}
	else {
		PRINT_FUNC_LF();
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print("Format de bitrate [ %d ] non prit en charge\n", p_bitsPerSample);
		return (FALSE);	
	}
	
	VarAlsa.SampleRate = p_samplesPerSec;
	rate = p_samplesPerSec;
	VarAlsa.channels = p_channels;

	/* setup dsp device */
	err = snd_pcm_open(&VarAlsa.playback_handle, audio_dev, SND_PCM_STREAM_PLAYBACK, 0);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			fprintf(stderr, "cannot open audio device %s (%s)\n", audio_dev, snd_strerror(err));
		return (FALSE);
	}
 
	err = snd_pcm_hw_params_malloc(&hw_params);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			fprintf(stderr, "cannot allocate hardware parameter structure (%s)\n", snd_strerror(err));
		return (FALSE);
	}

	err = snd_pcm_hw_params_any(VarAlsa.playback_handle, hw_params);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			fprintf(stderr, "cannot initialize hardware parameter structure (%s)\n", snd_strerror(err));
		return (FALSE);
	}

	err = snd_pcm_hw_params_set_access(VarAlsa.playback_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
	if (err < 0 ) {
		fprintf(stderr, "cannot set access type (%s)\n", snd_strerror(err));
		return (FALSE);
	}

	/* set format */
	err = snd_pcm_hw_params_set_format(VarAlsa.playback_handle, hw_params, format);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			fprintf(stderr, "cannot set sample format (%s)\n", snd_strerror(err));
		return (FALSE);
	}

	/* set sample rate */
	rrate = rate;
	err = snd_pcm_hw_params_set_rate_near(VarAlsa.playback_handle, hw_params, &rrate, 0);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			fprintf(stderr, "cannot set sample rate (%s)\n", snd_strerror(err));
		return (FALSE);
	}

	/* set channels */
	err = snd_pcm_hw_params_set_channels(VarAlsa.playback_handle, hw_params, VarAlsa.channels);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			fprintf(stderr, "cannot set channel count (%s)\n", snd_strerror(err));
		return (FALSE);
	}

	/* set the buffer time */
	err = snd_pcm_hw_params_set_buffer_time_near(VarAlsa.playback_handle, hw_params, &buffer_time, &dir);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			printf("Unable to set buffer time %i for playback: %s\n", buffer_time, snd_strerror(err));
		return (FALSE);
	}

	/* set the period time */
	err = snd_pcm_hw_params_set_period_time_near(VarAlsa.playback_handle, hw_params, &period_time, &dir);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			printf("Unable to set period time %i for playback: %s\n", period_time, snd_strerror(err));
		return (FALSE);
	}

	/* commit parameters */
	err = snd_pcm_hw_params(VarAlsa.playback_handle, hw_params);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			fprintf(stderr, "cannot set parameters (%s)\n", snd_strerror(err));
		return (FALSE);
	}

	err = snd_pcm_hw_params_get_buffer_size(hw_params, &buffer_size);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			printf("Unable to get buffer size for playback: %s\n", snd_strerror(err));
		return (FALSE);
	}

	err = snd_pcm_hw_params_get_period_size(hw_params, &period_size, 0);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			printf("Unable to get period size for playback: %s\n", snd_strerror(err));
		return (FALSE);
	}
	*p_bufferSize = snd_pcm_frames_to_bytes(VarAlsa.playback_handle, period_size);

	snd_pcm_hw_params_free(hw_params);

	err = snd_pcm_prepare(VarAlsa.playback_handle);
	if (err < 0 ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			fprintf(stderr, "cannot prepare audio interface for use (%s)\n", snd_strerror(err));
		return (FALSE);
	}

	return (TRUE);
}

