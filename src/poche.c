 /*
 *  file      : poche.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>	// M_PI
#include <cairo.h>
#include <cairo-pdf.h>
#include <cairo-ps.h>
#include <cairo-xlib.h>
#include <X11/Xlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "popup.h"
#include "cursor.h"
#include "dragNdrop.h"
#include "fileselect.h"
#include "configuser.h"
#include "cd_curl.h"
#include "poche.h"



VIEW view = {
	FALSE,			// BoolAccessChanged;				// COMBOBOX changed is TRUE
	
	NULL,			// *AdrComboBoxChoice;				// 
	
	NULL,			// *AdrEntrySearchImgWeb;			// Adresse saisie image web
	NULL,			// *AdrEntryNameFileSave;			// Adresse saisie nom fichier image
	NULL,			// *AdrEntryTitleCD;				// Adresse saisie titre du CD
	
	NULL,			// *TitleCD;						// 
	NULL,			// *Buffer_none;					// 
	NULL,			// *Buffer_title;					// 
	NULL,			// *Buffer_title_time;				// 
	NULL,			// *Buffer_artist_title_time;		// 
	
	NULL,			// *AdrDrawingarea;					// Adresse de la surface
	FALSE,			// BoolScaleAdjust;					// TRUE = ajustement de l'echelle avec la surface de la fentre
	NULL,			// *image;							// Adresse de l'image
	1.0,			// scale;							// Pourcentage de surface de l'image
	0.0,			// x0;								// Pos X
	0.0,			// y0;								// Pos Y
	0,				// image_width;						// Width
	0,				// image_height;					// Height
	NULL,			// *ListImage;						// Pointeur de structure IMAGE pour AdrDrawingarea
	FALSE,			// BoolEventButtonPress;			// TRUE == BOUTON SOURIS PRESSED
	NULL,			// *GetImage;						// Adresse de l'image en selection
	IMG_NONE,		// ImgCorner;						// See IMG_CORNER
	NULL,			// *Adr_viewport_image_preview;		// Adresse viewport
	NULL,			// *Adr_table;						// Adresse table contenu dans le viewport
	NULL,			// *glist;							// Contient les structures GLIST_POCHETTE
	NULL,			// *AdrTextview;					// Adresse textview
	FALSE,			// BoolSaveToFile;					// TRUE si sauvegarde vers fichier
					// HandleMove;						// Les coordonnees de saisie de l'image
	};


gdouble oldX = -1.0;
gdouble oldY = -1.0;



void draw_queue( void )
{
	/**
	void
	gtk_widget_queue_draw_area( GtkWidget *widget,
								gint x,
								gint y,
								gint width,
								gint height);
	GtkAllocation	allocation;
	gtk_widget_get_allocation( view.AdrDrawingarea, &allocation );

	GtkAllocation	allocation;
	
	
	
	gtk_widget_get_allocation( GTK_WIDGET(GLADE_GET_OBJECT("viewport1")), &allocation );

	gtk_widget_queue_draw_area(
						// view.AdrDrawingarea,
						GTK_WIDGET(GLADE_GET_OBJECT("viewport1")),
						0,
						0,
						// view.image_width,
						// view.image_height
						// allocation.width,
						// allocation.height
						//  gtk_widget_get_allocated_width( view.AdrDrawingarea ) ,
						//  gtk_widget_get_allocated_height( view.AdrDrawingarea ) 
						gtk_widget_get_allocated_width( GTK_WIDGET(GLADE_GET_OBJECT("viewport1")) ) ,
						gtk_widget_get_allocated_height( GTK_WIDGET(GLADE_GET_OBJECT("viewport1")) ) 
						);
	printf("1) 0, 0, %d %d \n", view.image_width, view.image_height );
	printf("2) 0, 0, %d %d \n", allocation.width, allocation.height );
	
	
	gtk_widget_queue_draw_area(
						view.AdrDrawingarea,
						0,
						0,
						view.image_width,
						view.image_height
						);

	gtk_widget_queue_draw(
						view.AdrDrawingarea
						);
	

	gtk_widget_queue_draw_area(
						view.AdrDrawingarea,
						0,
						0,
						view.image_width,
						view.image_height
						);
	*/
	gtk_widget_queue_draw(
						view.AdrDrawingarea
						);
}
// 
// 
void poche_remove_view (void)
{
	if( NULL != view.TitleCD ) {
		g_free( view.TitleCD );
		view.TitleCD = NULL;
	}
	if( NULL != view.Buffer_none ) {
		g_free( view.Buffer_none );
		view.Buffer_none = NULL;
	}
	if( NULL != view.Buffer_title ) {
		g_free( view.Buffer_title );
		view.Buffer_title = NULL;
	}
	if( NULL != view.Buffer_title_time ) {
		g_free( view.Buffer_title_time );
		view.Buffer_title_time = NULL;
	}
	if( NULL != view.Buffer_artist_title_time ) {
		g_free( view.Buffer_artist_title_time );
		view.Buffer_artist_title_time = NULL;
	}
}
// 
// 
void poche_remove_ListImage( void )
{
	GList	*List = NULL;
        IMAGE	*Image = NULL;
        gint	NbrFree = 0;
        
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data )) {
			if( NULL != Image->Texte ) {
				g_free( Image->Texte );
				Image->Texte = NULL;
			}
			if( NULL != Image->FontName ) {
				g_free( Image->FontName );
				Image->FontName = NULL;
			}
			if( NULL != Image->Pixbuf ) {
				g_object_unref( Image->Pixbuf );
				Image->Pixbuf = NULL;
			}
			if( NULL != Image->PixbufOriginal ) {
				g_object_unref( Image->PixbufOriginal );
				Image->PixbufOriginal = NULL;
			}
			g_free( Image );
			List->data = Image = NULL;
			NbrFree ++;
		}
		List = g_list_next( List );
	}
	g_list_free( view.ListImage );
	view.ListImage = NULL;
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print( "\tRemove: %d\n", NbrFree );
}
// 
// 
gboolean poche_is_erase_active( void )
{
	GList	*List = NULL;
	IMAGE	*Image = NULL;
	gboolean	BoolEraseActive = FALSE;
        
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			if( TRUE == Image->BoolIsSelected ) return( TRUE );
		}
		List = g_list_next( List );
	}
	return( BoolEraseActive );
}
// 
// 
void poche_set_flag_buttons (void )
{
	IMAGE	*Image = poche_get_struct_selected_is_txt();
	
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_erase")), poche_is_erase_active() );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_load_file")), TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_save")), *pochetxt_get_ptr_entry_name_file_to_save() == '\0' ? FALSE : TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_moins")), TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_normal")), TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_plus")), TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_ajuster")), TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_change_font")), Image && Image->BoolIsSelected ? TRUE : FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("togglebutton_font_bold")), Image && Image->BoolIsSelected ? TRUE : FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("togglebutton_font_italic")), Image && Image->BoolIsSelected ? TRUE : FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("button_import_img_web")), *pochetxt_get_ptr_entry_img_web() == '\0' ? FALSE : TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("combobox_choice_get_cd")), Image && Image->BoolIsSelected ? TRUE : FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET(GLADE_GET_OBJECT("frame_texte_poche")), (NULL != Image) ? TRUE : FALSE );

	// For 'button_import_title_cd' see: void cdaudio_set_flag_buttons (void)

	
	if( NULL != Image ) {
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(GLADE_GET_OBJECT("togglebutton_font_bold")), Image->BoolFontBold );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(GLADE_GET_OBJECT("togglebutton_font_italic")), Image->BoolFontItalic );
	}
	else {
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(GLADE_GET_OBJECT("togglebutton_font_bold")), FALSE );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(GLADE_GET_OBJECT("togglebutton_font_italic")), FALSE );
	}
}
// 
// 
void poche_print_zoom_changed( gdouble p_zoom_scale )
{
	gchar	*Str = NULL;
	
	Str = g_strdup_printf( "  %3.0f%%  ", 100.0 * p_zoom_scale );
	gtk_label_set_text( GTK_LABEL(GLADE_GET_OBJECT("label_percent")), Str );
	// g_print( "scale = %f   %3.0f%%  \n", view.scale, 100.0 * view.scale );
	g_free( Str );
	Str = NULL;
}
// 
// 
void poche_set_size_request( void )
{
	gtk_widget_set_size_request(
					view.AdrDrawingarea,
					(gint)((gdouble)view.image_width * view.scale),
					(gint)((gdouble)view.image_height * view.scale)
					// view.image_width * view.scale,
					// view.image_height * view.scale
					);
}
// 
// 
gboolean on_drawingareamain_draw( GtkWidget *widget, cairo_t *p_cr, gpointer user_data )
{
	pochedraw_paint( NULL, p_cr );
	// poche_set_flag_buttons();
	return( FALSE );
}
/*
gboolean on_drawingareamain_expose_event( GtkWidget *widget, GdkEventExpose *event, gpointer user_data )
{
	pochedraw_paint( NULL, p_cr );
	poche_set_flag_buttons();
	return( FALSE );
}
*/
gboolean on_scrolledwindow_poche_draw( GtkWidget *widget, cairo_t *p_cr, gpointer user_data )
{
	pochedraw_paint( NULL, p_cr );
	// poche_set_flag_buttons();
	return( FALSE );
}

// 
// 
IMAGE *poche_get_is_move( gdouble x, gdouble y )
{
	cairo_t	*cr;
	GdkRectangle	zone;
	// GdkRegion	*region = NULL;
	cairo_region_t *region = NULL;
	IMAGE		*Image = NULL;
	GList		*List = NULL;
	gboolean	BoolInZone = FALSE;
	
	cr = gdk_cairo_create( gtk_widget_get_window(view.AdrDrawingarea) );
	cairo_scale (cr, view.scale, view.scale);
	cairo_translate (cr, view.x0, view.y0);
	List = g_list_last( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			
			BoolInZone = FALSE;

			// IMG_MOVE
			zone.x      = Image->x0;
			zone.y      = Image->y0;
			zone.width  = Image->image_width;
			zone.height = Image->image_height;
			region = cairo_region_create_rectangle (&zone);
			if( cairo_region_contains_point( region, x, y )) {
			// if( gdk_region_point_in( region, x, y )) {
				BoolInZone = TRUE;
				Image->XPointer = x;
				Image->YPointer = y;
			}
			cairo_region_destroy (region);
			if( TRUE == BoolInZone ) {
				cairo_destroy( cr );
				return( Image );
			}
		}
		List = g_list_previous( List );
	}
	cairo_destroy( cr );
	return( NULL );
}
// 
// 
IMAGE *poche_get( gdouble x, gdouble y )
{
	cairo_t	*cr;
	// GdkRegion	*region = NULL;
	cairo_region_t *region = NULL;
	IMAGE		*Image = NULL;
	GList		*List = NULL;
	gboolean	BoolInZone = FALSE;
	gint		CptImgCorner;
		
	cr = gdk_cairo_create( gtk_widget_get_window(view.AdrDrawingarea) );
	cairo_scale (cr, view.scale, view.scale);
	cairo_translate (cr, view.x0, view.y0);
	List = g_list_last( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			
			if( TRUE != Image->BoolIsSelected ) {
				List = g_list_previous( List );
				continue;
			}
			
			BoolInZone = FALSE;
			
			pochedraw_get_handle_move( Image, TRUE );
			for( CptImgCorner = 0; CptImgCorner < IMG_SIZE; CptImgCorner ++ ) {
				region = cairo_region_create_rectangle( &view.HandleMove.zone [ CptImgCorner ] );
				// if( gdk_region_point_in( region, x, y )) {
				if( cairo_region_contains_point( region, x, y )) {
					BoolInZone = TRUE;
					Image->XPointer = x;
					Image->YPointer = y;
				}
				cairo_region_destroy (region);
				if( TRUE == BoolInZone ) {
					cairo_destroy( cr );
					view.ImgCorner = CptImgCorner;
					switch( CptImgCorner ) {
					case IMG_HAUT_GAUCHE :	cursor_set_haut_gauche();	break;
					case IMG_HAUT :		cursor_set_haut();		break;
					case IMG_HAUT_DROIT :	cursor_set_haut_droit();	break;
					case IMG_DROIT :	cursor_set_droit();		break;
					case IMG_BAS_DROIT :	cursor_set_bas_droit();		break;
					case IMG_BAS :		cursor_set_bas();		break;
					case IMG_BAS_GAUCHE :	cursor_set_bas_gauche();	break;
					case IMG_GAUCHE :	cursor_set_gauche();		break;
					case IMG_MOVE :		cursor_set_move();		break;
					case IMG_SIZE :		
					case IMG_NONE :	
						cursor_set_old();
						break;
					}
					return( Image );
				}
			}
		}
		List = g_list_previous( List );
	}
	cursor_set_old();
	view.ImgCorner = IMG_NONE;
	cairo_destroy( cr );
	return( NULL );
}
// 
// Image[ n ]		image en dessous  / image below
// Image[ n +1 ]	
// Image[ n +2 ]	
// Image[ n +3 ]	
// Image[ n +4 ]	image au dessus / image above
// 
void poche_set_selected_first_image( IMAGE *p_Image )
{
	GList	*List = NULL;
        IMAGE	*Image = NULL;
	GList	*ListLast = NULL;
        IMAGE	*ImageLast = NULL;
 	
	// GET List OF p_Image
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			if( p_Image == Image ) {
				break;
			}
		}
		List = g_list_next( List );
	}
	
	// GET ListLast for image above ( image au dessus )
	ListLast = g_list_last( view.ListImage );
	while( ListLast ) {
		if( NULL != (ImageLast = (IMAGE *)ListLast->data ) && FALSE == Image->BoolStructRemove ) {
			break;
		}
		ListLast = g_list_previous( ListLast );
	}
	
	// CHANGE List <-> ListLast
	if( NULL != List && NULL != ListLast  ) {
		ListLast->data = Image;
		List->data     = ImageLast;
		// gtk_widget_queue_draw( view.AdrDrawingarea );
		draw_queue();
	}
}
void poche_set_selected_up_image( IMAGE *p_Image )
{
	GList	*List = NULL;
        IMAGE	*Image = NULL;
	GList	*ListLast = NULL;
        IMAGE	*ImageLast = NULL;
 	
	if( g_list_length( view.ListImage ) <= 1 ) return;
	
	// GET List OF p_Image
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			if( p_Image == Image ) {
				if( NULL != (ListLast = g_list_next( List ))) {
					if( NULL != (ImageLast = (IMAGE *)ListLast->data ) && FALSE == ImageLast->BoolStructRemove ) {
						// CHANGE List <-> ListLast
						if( NULL != List && NULL != ListLast  ) {
							ListLast->data = Image;
							List->data     = ImageLast;
							// gtk_widget_queue_draw( view.AdrDrawingarea );
							draw_queue();
						}
					}
				}
				break;
			}
		}
		List = g_list_next( List );
	}
}
void poche_set_selected_down_image( IMAGE *p_Image )
{
	GList	*List = NULL;
        IMAGE	*Image = NULL;
	GList	*ListLast = NULL;
        IMAGE	*ImageLast = NULL;
 	
	if( g_list_length( view.ListImage ) <= 1 ) return;
	
	// GET List OF p_Image
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			if( p_Image == Image ) {
				if( NULL != (ListLast = g_list_previous( List ))) {
					if( NULL != (ImageLast = (IMAGE *)ListLast->data ) && FALSE == ImageLast->BoolStructRemove ) {
						// CHANGE List <-> ListLast
						if( NULL != List && NULL != ListLast  ) {
							ListLast->data = Image;
							List->data     = ImageLast;
							// gtk_widget_queue_draw( view.AdrDrawingarea );
							draw_queue();
						}
					}
				}
				break;
			}
		}
		List = g_list_next( List );
	}
}
// 
// La derniere image doit etre en haut de la liste
// 
void poche_set_selected_last_image( IMAGE *p_Image )
{
	GList	*List = NULL;
        IMAGE	*Image = NULL;
	GList	*ListFisrt = NULL;
        IMAGE	*ImageLast = NULL;
 	
	// GET List OF p_Image
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			if( p_Image == Image ) {
				break;
			}
		}
		List = g_list_next( List );
	}
	
	// GET ListFisrt for image above ( image au dessus )
	ListFisrt = g_list_first( view.ListImage );
	while( ListFisrt ) {
		if( NULL != (ImageLast = (IMAGE *)ListFisrt->data ) && FALSE == Image->BoolStructRemove ) {
			break;
		}
		ListFisrt = g_list_next( ListFisrt );
	}
	
	// CHANGE List <-> ListFisrt
	if( NULL != List && NULL != ListFisrt  ) {
		ListFisrt->data = Image;
		List->data     = ImageLast;
		// gtk_widget_queue_draw( view.AdrDrawingarea );
		draw_queue();
	}
}
// 
// 
gboolean on_drawingareamain_button_press_event( GtkWidget *widget, GdkEventButton *event, gpointer user_data )
{
	cairo_t		*cr;
        gdouble		x, y;
        guint		state;
        IMAGE		*Image = NULL;
        IMAGE		*ListImage = NULL;
	GList		*List = NULL;
 	gboolean	bool_click_droit = (event->button == 3);	
	
	cr = gdk_cairo_create( gtk_widget_get_window(view.AdrDrawingarea) );
	cairo_scale (cr, view.scale, view.scale);
	cairo_translate (cr, view.x0, view.y0);
	x = event->x;
	y = event->y;
	cairo_device_to_user (cr, &x, &y);
	
	if( TRUE == bool_click_droit ) {
		if( NULL != (Image = (IMAGE *)poche_get_is_move( x, y ))) {
			cairo_destroy( cr );
			if( TRUE == Image->BoolIsSelected ) {
				popup_flip( Image );
			}
			return( FALSE );
		}
	}
	
	state = event->state;
	
	// IMAGE A DESSUS AVEC: CTRL + click
	if( state & GDK_CONTROL_MASK ) {
		if( IMG_NONE == view.ImgCorner || IMG_MOVE == view.ImgCorner ) {
			if( NULL != (Image = (IMAGE *)poche_get_is_move( x, y ))) {
				poche_set_selected_first_image( Image );
			}
		}
	}
	
	// NOUVELLE SELECTION D IMAGE
	if( IMG_NONE == view.ImgCorner || IMG_MOVE == view.ImgCorner ) {
		if( NULL != (Image = (IMAGE *)poche_get_is_move( x, y ))) {
			if( FALSE == Image->BoolIsSelected ) {
				List = g_list_first( view.ListImage );
				while( List ) {
					if( NULL != (ListImage = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
						ListImage->BoolIsSelected = FALSE;
					}
					List = g_list_next( List );
				}
				Image->BoolIsSelected = TRUE;
				cursor_set_move();
				view.ImgCorner = IMG_MOVE;
				view.GetImage = Image;
				cairo_destroy( cr );
				// gtk_widget_queue_draw( view.AdrDrawingarea );
				draw_queue();
				view.BoolEventButtonPress = TRUE;
				
				pochetxt_set_text_to_textview( Image );
				poche_set_flag_buttons();
				return( FALSE );
			}
		}
	}
	
	// DESELECTION
	if( IMG_NONE == view.ImgCorner && NULL == Image ) {
		List = g_list_first( view.ListImage );
		while( List ) {
			if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
				Image->BoolIsSelected = FALSE;
			}
			List = g_list_next( List );
		}
		cursor_set_old();
		cairo_destroy( cr );
		// gtk_widget_queue_draw( view.AdrDrawingarea );
		draw_queue();
		view.BoolEventButtonPress = TRUE;
		return( FALSE );
	}
	
	cairo_destroy( cr );
	view.BoolEventButtonPress = TRUE;
	
	return( FALSE );
}
// 
// 
gboolean on_drawingareamain_button_release_event( GtkWidget *widget, GdkEventButton *event, gpointer user_data )
{
	cairo_t	*cr;
	gdouble	x, y;
        
	cr = gdk_cairo_create( gtk_widget_get_window(view.AdrDrawingarea) );
	cairo_scale (cr, view.scale, view.scale);
	cairo_translate (cr, view.x0, view.y0);
	x = event->x;
	y = event->y;
	cairo_device_to_user (cr, &x, &y);
	view.BoolEventButtonPress = FALSE;
	view.GetImage = NULL;
	cairo_destroy( cr );	

	poche_set_flag_buttons();

	return( FALSE );
}
// 
// 
gboolean on_drawingareamain_motion_notify_event( GtkWidget *widget, GdkEventMotion *event, gpointer user_data )
{
	cairo_t		*cr;
	gdouble		x, y;
	gboolean	BoolRedraw = FALSE;


	cr = gdk_cairo_create( gtk_widget_get_window(view.AdrDrawingarea) );
	cairo_scale (cr, view.scale, view.scale);
	cairo_translate (cr, view.x0, view.y0);
	x = event->x;
	y = event->y;
	cairo_device_to_user (cr, &x, &y);
	
	if (oldX == -1.0 || oldY == -1.0) {
		oldX = x;
		oldY = y;
	}
	
	if( FALSE == view.BoolEventButtonPress ) {
		view.GetImage = (IMAGE *)poche_get( x, y ); 
	}
		
	if( TRUE == view.BoolEventButtonPress ) {
		if( NULL != view.GetImage ) {
			
			if( IMG_MOVE == view.ImgCorner ) {
				view.GetImage->x0       += (x - view.GetImage->XPointer);
				view.GetImage->y0       += (y - view.GetImage->YPointer);
				view.GetImage->XPointer  = x;
				view.GetImage->YPointer  = y;
				BoolRedraw = TRUE;
			}
			else if( IMG_DROIT == view.ImgCorner ) {
				if( x > oldX ) {
					gint add = x - (view.GetImage->x0 + view.GetImage->image_width);
					view.GetImage->image_width += add;
				}
				else if( x < oldX ) {
					gint add = (view.GetImage->x0 + view.GetImage->image_width) - x;
					view.GetImage->image_width -= add;
				}
				if( x != oldX ) {
					// VERIFICATION DIMENSION DROIT
					if( view.GetImage->image_width  < MIN_SIZE_IMAGE ) view.GetImage->image_width = MIN_SIZE_IMAGE;
					BoolRedraw = TRUE;
				}
			}
			else if( IMG_GAUCHE == view.ImgCorner ) {
				if( x > oldX ) {
					gint add = x - view.GetImage->x0;
					view.GetImage->x0 += add;
					view.GetImage->image_width -= add;
				}
				else if( x < oldX ) {
					gint add = view.GetImage->x0 - x;
					view.GetImage->x0 -= add;
					view.GetImage->image_width += add;
				}
				if( x != oldX ) {
					// VERIFICATION DIMENSION GAUCHE
					while( view.GetImage->image_width < MIN_SIZE_IMAGE ) {
						view.GetImage->x0 --;
						view.GetImage->image_width ++;
					}
					BoolRedraw = TRUE;
				}
			}
			else if( IMG_HAUT == view.ImgCorner ) {
				if( y > oldY ) {
					gint add = y - view.GetImage->y0;
					view.GetImage->y0 += add;
					view.GetImage->image_height -= add;
				}
				else if( y < oldY ) {
					gint add = view.GetImage->y0 - y;
					view.GetImage->y0 -= add;
					view.GetImage->image_height += add;
				}
				if( y != oldY ) {
					// VERIFICATION DIMENSION HAUT
					while( view.GetImage->image_height < MIN_SIZE_IMAGE ) {
						view.GetImage->y0 --;
						view.GetImage->image_height ++;
					}
					BoolRedraw = TRUE;
				}
			}
			else if( IMG_BAS == view.ImgCorner ) {
				if( y > oldY ) {
					gint add = y - (view.GetImage->y0 + view.GetImage->image_height);
					view.GetImage->image_height += add;
				}
				else if( y < oldY ) {
					gint add = (view.GetImage->y0 + view.GetImage->image_height) - y;
					view.GetImage->image_height -= add;
				}
				if( y != oldY ) {
					// VERIFICATION DIMENSION BAS
					if( view.GetImage->image_height  < MIN_SIZE_IMAGE ) view.GetImage->image_height = MIN_SIZE_IMAGE;
					BoolRedraw = TRUE;
				}
			}
			else if( IMG_HAUT_GAUCHE == view.ImgCorner ) {
				// HAUT
				if( y > oldY ) {
					gint add = y - view.GetImage->y0;
					view.GetImage->y0 += add;
					view.GetImage->image_height -= add;
				}
				else if( y < oldY ) {
					gint add = view.GetImage->y0 - y;
					view.GetImage->y0 -= add;
					view.GetImage->image_height += add;
				}
				// VERIFICATION DIMENSION HAUT
				while( view.GetImage->image_height < MIN_SIZE_IMAGE ) {
					view.GetImage->y0 --;
					view.GetImage->image_height ++;
				}
				// GAUCHE
				if( x > oldX ) {
					gint add = x - view.GetImage->x0;
					view.GetImage->x0 += add;
					view.GetImage->image_width -= add;
				}
				else if( x < oldX ) {
					gint add = view.GetImage->x0 - x;
					view.GetImage->x0 -= add;
					view.GetImage->image_width += add;
				}
				// VERIFICATION DIMENSION GAUCHE
				while( view.GetImage->image_width < MIN_SIZE_IMAGE ) {
					view.GetImage->x0 --;
					view.GetImage->image_width ++;
				}
				if( x != oldX || y != oldY ) {
					BoolRedraw = TRUE;
				}
			}
			else if( IMG_HAUT_DROIT == view.ImgCorner ) {
				// HAUT
				if( y > oldY ) {
					gint add = y - view.GetImage->y0;
					view.GetImage->y0 += add;
					view.GetImage->image_height -= add;
				}
				else if( y < oldY ) {
					gint add = view.GetImage->y0 - y;
					view.GetImage->y0 -= add;
					view.GetImage->image_height += add;
				}
				// VERIFICATION DIMENSION HAUT
				while( view.GetImage->image_height < MIN_SIZE_IMAGE ) {
					view.GetImage->y0 --;
					view.GetImage->image_height ++;
				}
				// DROIT
				if( x > oldX ) {
					gint add = x - (view.GetImage->x0 + view.GetImage->image_width);
					view.GetImage->image_width += add;
				}
				else if( x < oldX ) {
					gint add = (view.GetImage->x0 + view.GetImage->image_width) - x;
					view.GetImage->image_width -= add;
				}
				// VERIFICATION DIMENSION DROIT
				if( view.GetImage->image_width  < MIN_SIZE_IMAGE ) view.GetImage->image_width = MIN_SIZE_IMAGE;
				if( x != oldX || y != oldY ) {
					BoolRedraw = TRUE;
				}
			}
			else if( IMG_BAS_DROIT == view.ImgCorner ) {
				// BAS
				if( y > oldY ) {
					gint add = y - (view.GetImage->y0 + view.GetImage->image_height);
					view.GetImage->image_height += add;
				}
				else if( y < oldY ) {
					gint add = (view.GetImage->y0 + view.GetImage->image_height) - y;
					view.GetImage->image_height -= add;
				}
				// VERIFICATION DIMENSION BAS
				if( view.GetImage->image_height  < MIN_SIZE_IMAGE ) view.GetImage->image_height = MIN_SIZE_IMAGE;
				// DROIT
				if( x > oldX ) {
					gint add = x - (view.GetImage->x0 + view.GetImage->image_width);
					view.GetImage->image_width += add;
				}
				else if( x < oldX ) {
					gint add = (view.GetImage->x0 + view.GetImage->image_width) - x;
					view.GetImage->image_width -= add;
				}
				// VERIFICATION DIMENSION DROIT
				if( view.GetImage->image_width  < MIN_SIZE_IMAGE ) view.GetImage->image_width = MIN_SIZE_IMAGE;
				if( x != oldX || y != oldY ) {
					BoolRedraw = TRUE;
				}
			}
			else if( IMG_BAS_GAUCHE == view.ImgCorner ) {
				// BAS
				if( y > oldY ) {
					gint add = y - (view.GetImage->y0 + view.GetImage->image_height);
					view.GetImage->image_height += add;
				}
				else if( y < oldY ) {
					gint add = (view.GetImage->y0 + view.GetImage->image_height) - y;
					view.GetImage->image_height -= add;
				}
				// VERIFICATION DIMENSION BAS
				if( view.GetImage->image_height  < MIN_SIZE_IMAGE ) view.GetImage->image_height = MIN_SIZE_IMAGE;
				// GAUCHE
				if( x > oldX ) {
					gint add = x - view.GetImage->x0;
					view.GetImage->x0 += add;
					view.GetImage->image_width -= add;
				}
				else if( x < oldX ) {
					gint add = view.GetImage->x0 - x;
					view.GetImage->x0 -= add;
					view.GetImage->image_width += add;
				}
				// VERIFICATION DIMENSION GAUCHE
				while( view.GetImage->image_width < MIN_SIZE_IMAGE ) {
					view.GetImage->x0 --;
					view.GetImage->image_width ++;
				}
				if( x != oldX || y != oldY ) {
					BoolRedraw = TRUE;
				}
			}
		}
	}
	oldX = x;
	oldY = y;
	cairo_destroy( cr );
	if( TRUE == BoolRedraw ) {
		// gtk_widget_queue_draw( view.AdrDrawingarea );	
		draw_queue();
	}
	return( FALSE );
}
// 
// 
void on_button_moins_clicked( GtkButton *button, gpointer user_data )
{
	view.BoolScaleAdjust = FALSE;
	view.scale -= 0.4;
	if( view.scale < 0.4 ) view.scale = 0.4;
	poche_set_size_request();
	// gtk_widget_queue_draw( view.AdrDrawingarea );
	draw_queue();
}
// 
// 
void on_button_normal_clicked( GtkButton *button, gpointer user_data )
{
	view.BoolScaleAdjust = FALSE;
	view.scale  = 1.0;	
	poche_set_size_request();
	// gtk_widget_queue_draw( view.AdrDrawingarea );
	draw_queue();
}
// 
// 
void on_button_plus_clicked( GtkButton *button, gpointer user_data )
{
	view.BoolScaleAdjust = FALSE;
	view.scale += 0.4;
	if( view.scale > 6.0 ) view.scale = 6.0;
	poche_set_size_request();
	// gtk_widget_queue_draw( view.AdrDrawingarea );
	draw_queue();
}
// 
// 
void on_button_ajuster_clicked( GtkButton *button, gpointer user_data )
{
	view.BoolScaleAdjust = TRUE;
	// gtk_widget_queue_draw( view.AdrDrawingarea );
	draw_queue();
}
// 
// 
IMAGE *poche_add_to_glist( gchar *PathNameFile, gdouble x, gdouble y, gboolean p_BoolScale, TYPE_IMAGE p_TypeImage )
{
	IMAGE	*New = (IMAGE *)g_malloc0( sizeof(IMAGE) );
	
	New->TypeImage          = p_TypeImage;
	// _TYPE_TEXT_
	// _TYPE_TEXT_TITLE_
	New->Texte              = NULL;
	New->FontName           = NULL;
	New->SizeFontName       = 8;
	New->BoolFontBold       = FALSE;
	New->BoolFontItalic     = FALSE;
	New->PosCombobox        = 0;
	// _TYPE_IMAGE_
	New->Pixbuf             = NULL;
	New->PixbufOriginal     = NULL;
	// VALUES
	New->x0                 = 0.0;
	New->y0                 = 0.0;
	New->image_width        = 0;
	New->image_height       = 0;
	New->XPointer           = 0.0;
	New->YPointer           = 0.0;
	New->BoolIsSelected     = FALSE;
	New->BoolStructRemove   = FALSE;
	New->BoolFlipVertical   = FALSE;
	New->BoolFlipHorizontal = FALSE;
	
	if( p_TypeImage == _TYPE_IMAGE_ ) {
		New->PixbufOriginal = gdk_pixbuf_new_from_file( PathNameFile, NULL );
		if( TRUE == p_BoolScale )
			New->Pixbuf = gdk_pixbuf_scale_simple( New->PixbufOriginal, SIZE_IMAGE_ADD, SIZE_IMAGE_ADD, 2 );	// GDK_INTERP_NEAREST
		else	New->Pixbuf = NULL;

		// New->image_width    = cairo_image_surface_get_width( New->image );
		// New->image_height   = cairo_image_surface_get_height( New->image );
		if( NULL != New->Pixbuf ) {
			New->image_width    = gdk_pixbuf_get_width( New->Pixbuf );
			New->image_height   = gdk_pixbuf_get_height( New->Pixbuf );
		}
		else {
			New->image_width    = gdk_pixbuf_get_width( New->PixbufOriginal );
			New->image_height   = gdk_pixbuf_get_height( New->PixbufOriginal );
		}

		New->x0                 = x - (New->image_width / 2);
		New->y0                 = y - (New->image_height / 2);
		New->BoolIsSelected     = FALSE;
		New->BoolStructRemove   = FALSE;
		New->BoolFlipVertical   = FALSE;
		New->BoolFlipHorizontal = FALSE;
	}
	else if( p_TypeImage == _TYPE_TEXT_ ) {
	
		// _TYPE_TEXT_TITLE_
		// _TYPE_TEXT_
		New->Texte              = g_strdup_printf( "\n Write text %d", g_list_length( view.ListImage ));
		New->FontName           = g_strdup( "Sans" );
		New->SizeFontName       = 8;
		New->BoolFontBold       = FALSE;
		New->BoolFontItalic     = FALSE;
		New->PosCombobox        = 0;
		
		// _TYPE_IMAGE_
		New->Pixbuf             = NULL;
		New->PixbufOriginal     = NULL;
		New->BoolFontBold       = FALSE;
		New->BoolFontItalic     = FALSE;
	
		// VALUES
		New->x0                 = x - 50;
		New->y0                 = y - 50;
		New->image_width        = 100;
		New->image_height       = 50;
		New->XPointer           = 0.0;
		New->YPointer           = 0.0;
		New->BoolIsSelected     = FALSE;
		New->BoolStructRemove   = FALSE;
		New->BoolFlipVertical   = FALSE;
		New->BoolFlipHorizontal = FALSE;
		
	}
	else if( p_TypeImage == _TYPE_TEXT_TITLE_ ) {
		
		// _TYPE_TEXT_TITLE_
		// _TYPE_TEXT_
		New->Texte              = NULL;
		New->FontName           = g_strdup( "Sans" );
		New->SizeFontName       = 8;
		New->PosCombobox        = 0;
		
		// _TYPE_IMAGE_
		New->Pixbuf             = NULL;
		New->PixbufOriginal     = NULL;
	
		// VALUES
		New->x0                 = 0;
		New->y0                 = 0;
		New->image_width        = 100;
		New->image_height       = 50;
		New->XPointer           = 0.0;
		New->YPointer           = 0.0;
		New->BoolIsSelected     = FALSE;
		New->BoolStructRemove   = FALSE;
		New->BoolFlipVertical   = FALSE;
		New->BoolFlipHorizontal = FALSE;
	}
	
	view.ListImage = g_list_append( view.ListImage, New );
	
	return( New );
}
// 
// AJOUT D UNE STRUCTURE _TYPE_TEXT_TITLE_ SI ELLE N EXISTE PAS
// INSERTION DE LA CHANE p_str DANS LA STRUCTURE
// 
void poche_set_texte_title( gchar *p_str )
{
	GList		*List = NULL;
        IMAGE		*Image = NULL;
        gboolean	BoolExist = FALSE;
        
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && _TYPE_TEXT_TITLE_ == Image->TypeImage ) {
			BoolExist = TRUE;
			break;
		}
		List = g_list_next( List );
	}
	
	if( FALSE == BoolExist ) {
		Image = poche_add_to_glist( NULL, 0.0, 0.0, FALSE, _TYPE_TEXT_TITLE_ );
		Image->Texte = g_strdup( p_str );
	}
	else {
		if( NULL != Image->Texte ) {
			g_free( Image->Texte );
			Image->Texte = NULL;
		}
		Image->Texte = g_strdup( p_str );
	}
	// gtk_widget_queue_draw( view.AdrDrawingarea );
	draw_queue();
}
// 
// 
static void poche_drag_data_received(
					GtkWidget        *widget,
					GdkDragContext   *context,
					gint              x,
					gint              y,
					GtkSelectionData *data,
					guint             info,
					guint             time,
					gpointer          user_data)
{
	// PRINT("DND FROM DESKTOP TO DRAWING"); 
	// Une copie ne peut aller vers elle meme !!!
	if (gtk_drag_get_source_widget(context) != widget) {
		// dragndrop_list_drag_data (widget, (gchar *)data->data);
		dragndrop_list_drag_data( widget, (gchar*)gtk_selection_data_get_data( data ));
	}
}
// 
// 
void on_drawingareamain_realize( GtkWidget *widget, gpointer user_data )
{	
	view.AdrDrawingarea  = widget;
	view.BoolScaleAdjust = TRUE;
	view.scale           = SCALE_MIN;
	view.BoolSaveToFile  = FALSE;
	
	view.image = cairo_image_surface_create(
				CAIRO_FORMAT_ARGB32,
				372,
				527
				);
	
	view.image_width  = cairo_image_surface_get_width( view.image );
	view.image_height = cairo_image_surface_get_height( view.image );
		
	poche_set_size_request();
	
	// Drag and drop support
	// SIGNAL : 'drag-data-received'
	gtk_drag_dest_set (GTK_WIDGET (widget),
			   GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP | GTK_DEST_DEFAULT_HIGHLIGHT,
			   drag_types,
			   n_drag_types,
			   GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_DEFAULT
			   );
	g_signal_connect(G_OBJECT(widget), "drag-data-received", G_CALLBACK(poche_drag_data_received), NULL);
	
	gtk_button_set_label( GTK_BUTTON(GLADE_GET_OBJECT("button_stock_img")), Config.PathSaveImg );
	
	// EMPLACEMENT DE STOCKAGE DES IMAGES CHARGEES DEPUIS LE WEB
	// Config.PathPochette = libutils_create_temporary_rep( "/tmp", "zoomc" );
	Config.PathPochette = libutils_create_temporary_rep( Config.PathnameTMP, PATH_SAVE_POCHETTE_IMG );
}
// 
// 
void poche_gestion_images (gchar *path)
{
	if( NULL != Config.PathLoadImg ) {
		g_free( Config.PathLoadImg );
		Config.PathLoadImg = NULL;
	}
	Config.PathLoadImg = g_strdup( path );
}
// 
// 
void on_button_load_file_clicked (GtkButton *button, gpointer user_data)
{
	fileselect_create( _PATH_IMPORT_IMAGES_, Config.PathLoadImg, poche_gestion_images );
}
// 
// 
void poche_remove_image( void )
{
	GList	*List = NULL;
        IMAGE	*Image = NULL;
        
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			if( TRUE == Image->BoolIsSelected ) {
				Image->BoolStructRemove = TRUE;
				if( _TYPE_TEXT_ == Image->TypeImage && NULL != Image->Texte ) {
				
					g_free( Image->Texte );
					Image->Texte = NULL;
					pochetxt_set_text_to_textview( Image );

				}
				// gtk_widget_queue_draw( view.AdrDrawingarea );
				draw_queue();
				poche_set_flag_buttons();
				// DEBUG CDDB_CURL
				pochetxt_set_combobox_choice( 0 );
				break;
			}
		}
		List = g_list_next( List );
	}
}
// 
// Cette fonction suppose un flag de (non) validite d'une structure IMAGE: BoolStructRemove
// 
void on_button_erase_clicked (GtkButton *button, gpointer user_data)
{
	poche_remove_image();
}
// 
// 
static void poche_drag_data_received_file(
					GtkWidget        *widget,
					GdkDragContext   *context,
					gint              x,
					gint              y,
					GtkSelectionData *data,
					guint             info,
					guint             time,
					gpointer          user_data)
{
	// dragndrop_list_drag_data (widget, (gchar *)data->data);
	dragndrop_list_drag_data( widget, (gchar*)gtk_selection_data_get_data( data ));
}
// 
// 
void poche_table_add_images_init (void)
{
	if( NULL != view.Adr_table ) {
		gtk_widget_destroy( view.Adr_table );
		view.Adr_table = NULL;
	}
	// view.Adr_table = gtk_grid_new (4, 5, TRUE);
	view.Adr_table = gtk_grid_new();
	gtk_container_add (GTK_CONTAINER (view.Adr_viewport_image_preview), view.Adr_table);
	gtk_widget_show_all (view.Adr_table);
}
// 
// 
void on_viewport_stock_realize (GtkWidget *widget, gpointer user_data)
{
	view.Adr_viewport_image_preview = widget;
	view.Adr_table = NULL;
	poche_table_add_images_init ();

	// Drag and drop support
	// SIGNAL : 'drag-data-received'
	// 
	gtk_drag_dest_set (GTK_WIDGET (widget),
			   GTK_DEST_DEFAULT_MOTION |
			   GTK_DEST_DEFAULT_DROP,
			   drag_types, n_drag_types,
			   GDK_ACTION_COPY| GDK_ACTION_MOVE );
	g_signal_connect(G_OBJECT(widget),
			 "drag-data-received",
			 G_CALLBACK(poche_drag_data_received_file),
			 NULL);
}
// 
// 
void poche_set_selected_flag_image( IMAGE *p_Image )
{
	GList	*List = NULL;
        IMAGE	*Image = NULL;
 	
	// GET List OF p_Image to FALSE
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			Image->BoolIsSelected = FALSE;
		}
		List = g_list_next( List );
	}
	
	// SET p_Image to TRUE
	p_Image->BoolIsSelected = TRUE;
}
// 
// 
void poche_add_img_file_to_Drawingarea( GSList *p_list )
{
	GSList		*gs_List = p_list;
	gchar		*Ptr = NULL;
	IMAGE		*Image = NULL;
	gdouble		x, y;
	gint		X, Y;
	GdkModifierType	state;
	cairo_t		*cr;
	
	PRINT_FUNC_LF();
	
	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( gtk_widget_get_window(view.AdrDrawingarea), device, &X, &Y, &state );
	// gdk_window_get_pointer( view.AdrDrawingarea->window, &X, &Y, &state ); 
		
	cr = gdk_cairo_create( gtk_widget_get_window(view.AdrDrawingarea) );
	cairo_scale (cr, view.scale, view.scale);
	cairo_translate (cr, view.x0, view.y0);
	x = X;
	y = Y;
	cairo_device_to_user (cr, &x, &y);
	cairo_destroy( cr );
	
	while (gs_List) {
		if ((Ptr = (gchar *)gs_List->data) != NULL) {
			// PRENDRE UNIQUEMENT LA PREMIERE IMAGE VALIDE DANS LA LISTE
			if( TRUE == FileIs_image( Ptr )) {
				Image = poche_add_to_glist( Ptr, x, y, TRUE, _TYPE_IMAGE_ );
				poche_set_selected_flag_image( Image );
				// gtk_widget_queue_draw( view.AdrDrawingarea );
				draw_queue();
				return;
			}
		}
		gs_List = g_slist_next (gs_List);
	}

	if(NULL != Ptr &&  FALSE == FileIs_image( Ptr )) {
		PRINT("BAD FILE");
		g_print( "\t%s\n", Ptr );
	}
}
// 
// 
IMAGE *poche_get_struct_selected_is_txt( void )
{
	IMAGE	*Image = NULL;
	GList	*List = NULL;
	
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && TRUE == Image->BoolIsSelected && _TYPE_TEXT_ == Image->TypeImage  && FALSE == Image->BoolStructRemove ) {
			return( Image );
		}
		List = g_list_next( List );
	}
	return( NULL );
}







