 /*
 *  file      : cd_normalise.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef cd_normalise_h
#define cd_normalise_h 1


typedef struct {
	gchar		*PathNameSrc;		// Le fichier a normaliser
	gchar		*PathNameDest;		// Destination du fichier
	CD_AUDIO	*Audio;
} VAR_CD_NORMALISE_ELEMENT;


void		CdNormalise_set_list_PeakGroup (CD_AUDIO *Audio);
void		CdNormalise_set_list_collectif_remove (void);
gboolean	CdNormalise_list_PeakGroup_is_ready (void);
gboolean	CdNormalise_get_is_list_PeakGroup (void);
GList		*CdNormalise_get_list_PeakGroup (void);
void		CdNormalise_set_normalise_ok (void);
void		CdNormalise_add_PeakGroup (void);

#endif

