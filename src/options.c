 /*
 *  file      : options.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "fileselect.h"
#include "win_info.h"
#include "options.h"



/*
*---------------------------------------------------------------------------
* VARIABLE
*---------------------------------------------------------------------------
*/

VAR_OPTIONS var_options;


/*
*---------------------------------------------------------------------------
* CODE
*---------------------------------------------------------------------------
*/


// 
// PARAMETRES POUR UNE ECOUTE MUSICALE EN FIN DE CONVERSION
// 
void on_checkbutton_end_of_convert_realize( GtkWidget *widget, gpointer user_data )
{
	var_options.Adr_checkbutton_end_of_convert = widget;
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(var_options.Adr_checkbutton_end_of_convert), Config.BoolCheckbuttonEndOfConvert );
}
// 
// 
void on_checkbutton_end_of_convert_clicked( GtkButton *button, gpointer user_data )
{
	Config.BoolCheckbuttonEndOfConvert = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(var_options.Adr_checkbutton_end_of_convert));
}
// 
// 
void on_button_music_file_end_of_convert_realize( GtkWidget *widget, gpointer user_data )
{
	var_options.Adr_button_music_file_end_of_convert = widget;

	if( NULL != Config.FileMusicFileEndOfConvert && '\0' != *Config.FileMusicFileEndOfConvert ) {
		gtk_button_set_label( GTK_BUTTON(var_options.Adr_button_music_file_end_of_convert), Config.FileMusicFileEndOfConvert );
	}
	else {
		gtk_button_set_label( GTK_BUTTON(var_options.Adr_button_music_file_end_of_convert), Config.PathMusicFileEndOfConvert );
	}
}
// 
// 
void options_choice_music( gchar *path )
{
	gchar	*Ptr = NULL;
	
	if( NULL != Config.PathMusicFileEndOfConvert ) {
		g_free( Config.PathMusicFileEndOfConvert );
		Config.PathMusicFileEndOfConvert = NULL;
	}	
	if( NULL != Config.FileMusicFileEndOfConvert ) {
		g_free( Config.FileMusicFileEndOfConvert );
		Config.FileMusicFileEndOfConvert = NULL;
	}
	Config.PathMusicFileEndOfConvert = g_strdup( path );
	if( NULL != (Ptr = strrchr( Config.PathMusicFileEndOfConvert, '/'))) {
		Ptr ++;
		Config.FileMusicFileEndOfConvert = g_strdup( Ptr );
		Ptr --;
		*Ptr = '\0';
		gtk_button_set_label( GTK_BUTTON(var_options.Adr_button_music_file_end_of_convert), Config.FileMusicFileEndOfConvert );
	}
}
// 
// 
void on_button_music_file_end_of_convert_clicked( GtkButton *button, gpointer user_data )
{
	fileselect_create( _PATH_LOAD_FILE_MUSIC_, Config.PathMusicFileEndOfConvert, options_choice_music);
}


// 
// INIT NICE
// 
void on_combobox_nice_realize (GtkWidget *widget, gpointer user_data)
{
	gint   i;
	gchar *Ptr = NULL;

	for (i = 0; i < 20; i++) {
		Ptr = g_strdup_printf ("%d", i);
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), Ptr );
		g_free (Ptr);
		Ptr = NULL;
	}
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.Nice);
	var_options.Adr_Widget_Nice = GTK_COMBO_BOX (widget);
}
// CHANGED VALUE NICE
// 
void on_combobox_nice_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (var_options.Adr_Widget_Nice != NULL) {
		Config.Nice = gtk_combo_box_get_active (var_options.Adr_Widget_Nice);
	}
}
// DOSSIER TEMPORAIRE DES CONVERSIONS
// 
void on_button_dossier_de_conversion_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_path_temp = widget;
	gtk_button_set_label (GTK_BUTTON (widget), Config.PathnameTMP);
}
// CALL FROM fileselect_create
// 
void options_path_tmp (gchar *path)
{
	if (libutils_test_write (path) == TRUE) {
		g_free (Config.PathnameTMP);
		Config.PathnameTMP = NULL;
		Config.PathnameTMP = g_strdup (path);
		gtk_button_set_label (GTK_BUTTON (var_options.Adr_path_temp), Config.PathnameTMP);
	}
	else {
		wind_info_init (
			WindMain,
			_("ERROR : storage location not permitted !"),
			_("Please start again."),
			  "");
	}
}
// CHANGEMENT DE DOSSIER TEMPORAIRE DES CONVERSIONS
// 
void on_button_dossier_de_conversion_clicked (GtkButton *button, gpointer user_data)
{
	fileselect_create (_PATH_CHOICE_DESTINATION_, Config.PathnameTMP, options_path_tmp);
}
// 
//
void options_set_all_interne (void)
{
	OptionsInternal_set_datas_interne (COLOR_NONE, var_options.Adr_label_lame_mp3, LAME_WAV_TO_MP3);
	OptionsInternal_set_datas_interne (COLOR_NONE, var_options.Adr_label_oggenc_ogg, OGGENC_WAV_TO_OGG);	
	OptionsInternal_set_datas_interne (COLOR_NONE, var_options.Adr_label_flac_flac, FLAC_WAV_TO_FLAC);
	OptionsInternal_set_datas_interne (COLOR_NONE, var_options.Adr_label_mac_ape, MAC_WAV_TO_APE);
	OptionsInternal_set_datas_interne (COLOR_NONE, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	OptionsInternal_set_datas_interne (COLOR_NONE, var_options.Adr_label_musepack_mpc, MPPENC_WAV_TO_MPC);
	OptionsInternal_set_datas_interne (COLOR_NONE, var_options.Adr_label_faac_m4a, FAAC_WAV_TO_M4A);
}
// 
//
gchar *options_get_params( TYPE_CONV TypeConv )
{
	if (TypeConv == LAME_WAV_TO_MP3) {
		return( (gchar *)optionsLame_get_param() );
	}
	else if (TypeConv == OGGENC_WAV_TO_OGG || TypeConv == OGGENC_FLAC_TO_OGG) {
		return ((gchar *)optionsOggenc_get_param() );
	}
	return ((gchar *)NULL);
}
// 
//
gboolean options_get_entry_is_valid (TYPE_CONV p_verif_conv)
{
	gchar     *ptr = NULL;
	GtkWidget *wd = NULL;
	
	switch (p_verif_conv) {
	
	case FLAC_WAV_TO_FLAC :		wd = var_options.Adr_entry_flac_flac;		break;
	case LAME_WAV_TO_MP3 :		wd = var_options.Adr_entry_lame_mp3;		break;
	case OGGENC_WAV_TO_OGG :	wd = var_options.Adr_entry_oggenc_ogg;		break;
	case FAAC_WAV_TO_M4A :		wd = var_options.Adr_entry_faac_m4a;		break;
	case MPPENC_WAV_TO_MPC :	wd = var_options.Adr_entry_musepack_mpc;	break;
	case MAC_WAV_TO_APE :		wd = var_options.Adr_entry_mac_ape;		break;
	case WAVPACK_WAV_TO_WAVPACK :	wd = var_options.Adr_entry_wavpack_wv;		break;
	case CDPARANOIA_CD_TO_WAV_EXPERT :
	case CDPARANOIA_CD_TO_WAV :
	case CDDA2WAV_CD_TO_WAV :
		return(gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_cdparanoia_mode_expert)));
	default :
		return (FALSE);
	}
	if (wd == NULL) return (FALSE);
	ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY(wd));
	return ((ptr && *ptr != '\0' && *ptr != ' ') ? TRUE : FALSE);
}
// 
// void options_default_values (GtkButton *button)
void on_button_defaut_convertisseur_clicked (GtkButton *button, gpointer user_data)
{
	// PRINT_FUNC_LF();

	if (button == GTK_BUTTON (GLADE_GET_OBJECT("button_defaut_lame"))) {
		// g_print("\tbutton_defaut_lame\n");
		// BitrateLameIndice
		// g_print("\t\tConfig.BitrateLameIndice = %d -> %d\n",Config.BitrateLameIndice, ConfigSaveToRest.BitrateLameIndice);
		Config.BitrateLameIndice = ConfigSaveToRest.BitrateLameIndice;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_abr_cbr_vbr), Config.BitrateLameIndice);
		// TabBitrateLame
		// g_print("\t\tConfig.TabBitrateLame    = %d -> %d\n",Config.TabBitrateLame [ Config.BitrateLameIndice ], ConfigSaveToRest.TabBitrateLame [ ConfigSaveToRest.BitrateLameIndice ]);
		Config.TabBitrateLame [ Config.BitrateLameIndice ] = ConfigSaveToRest.TabBitrateLame [ ConfigSaveToRest.BitrateLameIndice ];
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_bitrate), Config.TabBitrateLame [ Config.BitrateLameIndice ]);
		// TabModeLame
		// g_print("\t\tConfig.TabModeLame       = %d -> %d\n",Config.TabModeLame [ Config.BitrateLameIndice ], ConfigSaveToRest.TabModeLame [ ConfigSaveToRest.BitrateLameIndice ]);
		Config.TabModeLame [ Config.BitrateLameIndice ] = ConfigSaveToRest.TabModeLame [ ConfigSaveToRest.BitrateLameIndice ];
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_Mode), Config.TabModeLame [ Config.BitrateLameIndice ]);
	}
	else if (button == GTK_BUTTON (GLADE_GET_OBJECT("button_defaut_oggenc"))) {
		// g_print("\tbutton_defaut_oggenc\n");
		// BitrateOggenc
		// g_print("\t\tConfig.BitrateOggenc = %d -> %d\n",Config.BitrateOggenc, ConfigSaveToRest.BitrateOggenc);
		Config.BitrateOggenc = ConfigSaveToRest.BitrateOggenc;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_bitrate), Config.BitrateOggenc);
		// ManagedOggenc
		// g_print("\t\tConfig.ManagedOggenc = %d -> %d\n",Config.ManagedOggenc, ConfigSaveToRest.ManagedOggenc);
		Config.ManagedOggenc = ConfigSaveToRest.ManagedOggenc;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_managed), Config.ManagedOggenc);
		// DownmixOggenc
		// g_print("\t\tConfig.DownmixOggenc = %d -> %d\n",Config.DownmixOggenc, ConfigSaveToRest.DownmixOggenc);
		Config.DownmixOggenc = ConfigSaveToRest.DownmixOggenc;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Oggenc_downmix), Config.DownmixOggenc);
	}
	else if (button == GTK_BUTTON (GLADE_GET_OBJECT("button_defaut_flac"))) {
		// g_print("\tbutton_defaut_flac\n");
		// CompressionLevelFlac
		// g_print("\t\tConfig.CompressionLevelFlac = %d -> %d\n", Config.CompressionLevelFlac, ConfigSaveToRest.CompressionLevelFlac);
		Config.CompressionLevelFlac = ConfigSaveToRest.CompressionLevelFlac;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_flac_compression), Config.CompressionLevelFlac);
	}
	else if (button == GTK_BUTTON (GLADE_GET_OBJECT("button_defaut_mac"))) {
		// g_print("\tbutton_defaut_mac\n");
		// CompressionLevelApeMac
		// g_print("\t\tConfig.CompressionLevelApeMac = %d -> %d\n",Config.CompressionLevelApeMac, ConfigSaveToRest.CompressionLevelApeMac);
		Config.CompressionLevelApeMac = ConfigSaveToRest.CompressionLevelApeMac;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_ape_compression), Config.CompressionLevelApeMac);
	}
	else if (button == GTK_BUTTON (GLADE_GET_OBJECT("button_defaut_wavpack"))) {
		// g_print("\tbutton_defaut_wavpack\n");
		// CompressionWavpack
		// g_print("\t\tConfig.CompressionWavpack        = %d -> %d\n", Config.CompressionWavpack, ConfigSaveToRest.CompressionWavpack);
		// Config.CompressionWavpack = ConfigSaveToRest.CompressionWavpack;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack), Config.CompressionWavpack);
		// SoundWavpack
		// g_print("\t\tConfig.SoundWavpack              = %d -> %d\n", Config.SoundWavpack, ConfigSaveToRest.SoundWavpack);
		Config.SoundWavpack = ConfigSaveToRest.SoundWavpack;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_sound), Config.SoundWavpack);
		// ModeHybrideWavpack
		// g_print("\t\tConfig.ModeHybrideWavpack        = %d -> %d\n", Config.ModeHybrideWavpack, ConfigSaveToRest.ModeHybrideWavpack);
		Config.ModeHybrideWavpack = ConfigSaveToRest.ModeHybrideWavpack;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_mode_hybride), Config.ModeHybrideWavpack);
		// CorrectionFileWavpack
		// g_print("\t\tConfig.CorrectionFileWavpack     = %d -> %d\n", Config.CorrectionFileWavpack, ConfigSaveToRest.CorrectionFileWavpack);
		Config.CorrectionFileWavpack = ConfigSaveToRest.CorrectionFileWavpack;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_correction_file), Config.CorrectionFileWavpack);
		// CompressionMaximumWavpack
		// g_print("\t\tConfig.CompressionMaximumWavpack = %d -> %d\n", Config.CompressionMaximumWavpack, ConfigSaveToRest.CompressionMaximumWavpack);
		Config.CompressionMaximumWavpack = ConfigSaveToRest.CompressionMaximumWavpack;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_maximum_compression), Config.CompressionMaximumWavpack);
		// SignatureMd5Wavpack
		// g_print("\t\tConfig.SignatureMd5Wavpack       = %d -> %d\n", Config.SignatureMd5Wavpack, ConfigSaveToRest.SignatureMd5Wavpack);
		Config.SignatureMd5Wavpack = ConfigSaveToRest.SignatureMd5Wavpack;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_signature_md5), Config.SignatureMd5Wavpack);
		// ExtraEncodingWavpack
		// g_print("\t\tConfig.ExtraEncodingWavpack      = %d -> %d\n", Config.ExtraEncodingWavpack, ConfigSaveToRest.ExtraEncodingWavpack);
		Config.ExtraEncodingWavpack = ConfigSaveToRest.ExtraEncodingWavpack;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_extra_encoding), Config.ExtraEncodingWavpack);
	}
	else if (button == GTK_BUTTON (GLADE_GET_OBJECT("button_defaut_musepack"))) {
		// g_print("\tbutton_defaut_musepack\n");
		// g_print("\t\tConfig.QualityMppenc = %d -> %d\n", Config.QualityMppenc, ConfigSaveToRest.QualityMppenc);
		// QualityMppenc
		Config.QualityMppenc = ConfigSaveToRest.QualityMppenc;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Mppenc), Config.QualityMppenc);
	}
	else if (button == GTK_BUTTON (GLADE_GET_OBJECT("button_defaut_faac"))) {
		// g_print("\tbutton_defaut_faac\n");
		// g_print("\t\tConfig.ConteneurFacc = %d -> %d\n", Config.ConteneurFacc, ConfigSaveToRest.ConteneurFacc);
		// ConteneurFacc
		Config.ConteneurFacc = ConfigSaveToRest.ConteneurFacc;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_conteneur), Config.ConteneurFacc);
		// g_print("\t\tConfig.AbrVbrFacc    = %d -> %d\n", Config.AbrVbrFacc, ConfigSaveToRest.AbrVbrFacc);
		// AbrVbrFacc
		Config.AbrVbrFacc = ConfigSaveToRest.AbrVbrFacc;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_choice_vbr_abr), Config.AbrVbrFacc);
		// g_print("\t\tConfig.VbrFaccIndice = %d -> %d\n", Config.VbrFaccIndice, ConfigSaveToRest.VbrFaccIndice);
		// VbrFaccIndice
		Config.VbrFaccIndice = ConfigSaveToRest.VbrFaccIndice;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_faac_set_choice_vbr_abr), Config.VbrFaccIndice);
	}
	else if (button == GTK_BUTTON (GLADE_GET_OBJECT("button_defaut_aacplusenc"))) {
		// g_print("\tbutton_defaut_aacplusenc\n");
		// g_print("\t\tConfig.ChoiceMonoAacplusenc   = %d -> %d\n", Config.ChoiceMonoAacplusenc, ConfigSaveToRest.ChoiceMonoAacplusenc);
		// ChoiceMonoAacplusenc
		Config.ChoiceMonoAacplusenc = ConfigSaveToRest.ChoiceMonoAacplusenc;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_aacplusenc_mono), Config.ChoiceMonoAacplusenc);
		// g_print("\t\tConfig.ChoiceStereoAacplusenc = %d -> %d\n", Config.ChoiceMonoAacplusenc, ConfigSaveToRest.ChoiceStereoAacplusenc);
		// ChoiceStereoAacplusenc
		Config.ChoiceStereoAacplusenc = ConfigSaveToRest.ChoiceStereoAacplusenc;
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_aacplusenc_stereo), Config.ChoiceStereoAacplusenc);
	}
}












