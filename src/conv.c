 /*
 *  file      : conv.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
// #include <fcntl.h>	// PROCESSUS NON-BLOQUANT

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "file.h"
#include "conv.h"
#include "configuser.h"


/*
*---------------------------------------------------------------------------
* EXTERN
*---------------------------------------------------------------------------
*/
extern int kill (pid_t pid, int sig);


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

#define CONV_MAX_CARS 1024
#define CONV_MAX_VALUE_PERCENT 0.978597

CONV conv;


//
// Allocate Tab Args
//
gchar **conv_AllocTabArgs( gint *p_tab )
{
//	gint	len_malloc =
//			50 +								// nbr elements
//			C_list_length( Detail.ListFile );	// list length
	gint	len_malloc = 50;
	gchar	**PtrTab = (gchar **)g_malloc0( sizeof(gchar **) * len_malloc );

	*p_tab = 0;
	//if( Detail.Nice >= 0 && Detail.Nice <= 20) {
		PtrTab [ 0 ] = g_strdup( "nice" );
		PtrTab [ 1 ] = g_strdup( "-n" );
		// PtrTab [ 2 ] = g_strdup_printf( "%d", Detail.Nice );
		PtrTab [ 2 ] = g_strdup_printf( "%d", Config.Nice );
		*p_tab = 3;
	//}
	return( (gchar **)PtrTab );
}

//
// Remove Tab Args
//
gchar **conv_RemoveTab( gchar **p_PtrTabArgs )
{
	gint	i;

	for( i = 0; p_PtrTabArgs[ i ] != NULL; i ++ ) {
		g_free( p_PtrTabArgs[ i ] );
		p_PtrTabArgs[ i ] = NULL;
	}
	g_free( p_PtrTabArgs );
	p_PtrTabArgs = NULL;

	return( (gchar **)NULL );
}




/*
*---------------------------------------------------------------------------
* STOP THREAD AND TIMEOUT
*---------------------------------------------------------------------------
*/
/*
void conv_print (void)
{
	g_print("\tconv.encode_completed->%d  conv.total_convert->%d\n", conv.encode_completed, conv.total_convert);
	g_print("\tconv.rip_completed---->%d  conv.total_rip----->%d\n", conv.rip_completed, conv.total_rip);
	g_print("\tconv.total_percent      = %f\n", conv.total_percent);
	g_print("\tconv.extract_percent    = %f\n", conv.extract_percent);
	g_print("\tconv.conversion_percent = %f\n", conv.conversion_percent);
	g_print("\n");
}
*/
void conv_inc_rip_completed (void)
{
	if (conv.rip_completed < conv.total_rip) {
		conv.rip_completed ++;
		conv.total_percent = ((double)conv.rip_completed +
					(double)conv.encode_completed +
					(double)conv.extract_percent +
					(double)conv.conversion_percent) /
					((double)conv.total_rip + (double)conv.total_convert);
	}
	conv.bool_percent_conv = TRUE;
}
void conv_inc_encode_completed (void)
{
	if (conv.encode_completed < conv.total_convert) {
		conv.encode_completed ++;
		conv.total_percent = ((double)conv.rip_completed +
					(double)conv.encode_completed +
					(double)conv.extract_percent +
					(double)conv.conversion_percent) /
					((double)conv.total_rip + (double)conv.total_convert);
	}
	conv.bool_percent_conv = TRUE;
}

void conv_stop_conversion (void)
{
	gint Ret_Kill;

	// PRINT_FUNC_LF();

	conv.bool_stop = TRUE;

	if (conv.code_fork_conv > 0) {
		g_print ("kill (%d, SIGKILL)\n", conv.code_fork_conv);
		Ret_Kill = kill (conv.code_fork_conv, SIGKILL);
		if (Ret_Kill != 0) {
			g_print ("--------------------------\n");
			g_print ("ERREUR Ret_Kill = %d\n", Ret_Kill);
			g_print ("-------------------------\n");
		}
	}
	conv.code_fork_conv = -1;

	if (conv.code_fork_extract > 0) {
		g_print ("kill (%d, SIGKILL)\n", conv.code_fork_extract);
		Ret_Kill = kill (conv.code_fork_extract, SIGKILL);
		if (Ret_Kill != 0) {
			g_print ("--------------------------\n");
			g_print ("ERREUR Ret_Kill = %d\n", Ret_Kill);
			g_print ("-------------------------\n");
		}
	}
	conv.code_fork_extract = -1;
}
/*
*---------------------------------------------------------------------------
* SIGNAL ET EXTRACTION
*---------------------------------------------------------------------------
*/
void conv_sigchld_extract (gint signum)
{
        gint status;
        wait(&status);
	/* PRINT_FUNC_LF(); */
        /* if there are still children waiting
        *  re-install the signal handler
	*/
	conv.signal_numchildren_extract --;
        if (conv.signal_numchildren_extract > 0)
        {
                /* re-install the signal handler */
                signal (SIGCHLD, conv_sigchld_extract);
        }
}
int conv_extract_call_exec (gchar **args, pid_t *p, gint p_output)
{
	gchar	**ptr = (gchar **)args;
	// gint	status;

	conv.signal_numchildren_extract = 0;
	if (pipe (conv.tube_extract) != 0)
	{
		fprintf (stderr, "error: pipe\n");
		exit (1);
	}
	if ((*p = fork()) == 0)
	{
		dup2 (conv.tube_extract [1 ], p_output);
		close (conv.tube_extract [ 1 ]);
		execvp ((gchar *)*(ptr+0), ptr);
		fprintf (stderr, "error: exec");
		exit (2);
	}
	conv.signal_numchildren_extract ++;
	signal (SIGCHLD, conv_sigchld_extract);
	close(conv.tube_extract [ 1 ]);

	// LE FICHIER EST OUVERT EN MODE « NON-BLOQUANT »
	// status = fcntl( conv.tube_extract [ 0 ], F_GETFL );
	// status = fcntl( conv.tube_extract [ 0 ], F_SETFL, status | O_NONBLOCK );

	return (conv.tube_extract [ 0 ]);
}
/*
*---------------------------------------------------------------------------
* SIGNAL ET CONVERSIONS
*---------------------------------------------------------------------------
*/
void conv_sigchld_convert (gint signum)
{
	gint status;
        wait(&status);
	/* PRINT_FUNC_LF(); */
        /* if there are still children waiting
        *  re-install the signal handler
	*/
	conv.signal_numchildren_conv --;
        if (conv.signal_numchildren_conv > 0)
        {
                /* re-install the signal handler */
                signal (SIGCHLD, conv_sigchld_convert);
        }
}
int conv_call_exec (gchar **args, pid_t *p, gint p_output)
{
	gchar	**ptr = (gchar **)args;
	// gint	status;

	conv.signal_numchildren_conv = 0;
	if (pipe (conv.tube_conv) != 0)
	{
		fprintf (stderr, "error: pipe\n");
		exit (1);
	}
	if ((*p = fork()) == 0)
	{
		dup2 (conv.tube_conv [ 1 ], p_output);
		close (conv.tube_conv [ 1 ]);
		execvp ((gchar *)*(ptr+0), ptr);
		fprintf (stderr, "error: exec");
		exit (2);
	}
	conv.signal_numchildren_conv ++;
	signal (SIGCHLD, conv_sigchld_convert);
	close (conv.tube_conv [ 1 ]);

	// LE FICHIER EST OUVERT EN MODE « NON-BLOQUANT »
	// status = fcntl( conv.tube_conv [ 0 ], F_GETFL );
	// status = fcntl( conv.tube_conv [ 0 ], F_SETFL, status | O_NONBLOCK );

	return (conv.tube_conv [ 0 ]);
}
// RESET STRUCTURE CONV
//
void conv_reset_struct (void *p_func)
{
	gint	i;

	/*PRINT_FUNC_LF();*/
	conv.Func_request_stop = p_func;
	conv.BoolIsExtract    = FALSE;
	conv.BoolIsConvert    = FALSE;
	conv.BoolIsCopy       = FALSE;
	conv.BoolIsNormalise  = FALSE;
	conv.BoolIsReplaygain = FALSE;

	conv.extract_percent = 0.0;
	conv.total_rip = 0;
	conv.rip_completed = 0;
	conv.conversion_percent = 0.0;
	conv.total_convert = 0;
	conv.encode_completed = 0;
	conv.total_percent = 0.0;
	conv.bool_percent_conv = FALSE;
	conv.bool_percent_extract = FALSE;

	conv.bool_stop = FALSE;

	if( NULL != conv.TmpRep ) {
		g_free (conv.TmpRep);
		conv.TmpRep = NULL;
	}
	conv.code_fork_conv = -1;
	conv.code_fork_extract = -1;
	conv.handler_timeout_conv = 0;
	conv.bool_thread_conv = FALSE;
	conv.bool_thread_extract = FALSE;
	conv.signal_numchildren_conv = 0;
	conv.signal_numchildren_extract = 0;

	conv.Bool_MAJ_select = FALSE;

	for (i = 0; i < MAX_ARG_CONV; i++) {
		conv.ArgExtract [ i ] = NULL;
	}
}
// LSDVD
//
void conv_with_lsdvd_ARGS (gchar **args)
{
	gint       fd;
	gint       size;
	gint       pos = 0;
	gchar      buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);

	do
	{
		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos++] = '\n';
		buf[pos] = '\0';
		libutils_add_datas_on_disk (buf);

	} while (size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// MPLAYER
//
gboolean conv_with_mplayer_ARGS (gchar **args, TYPE_CONV p_TypeConv)
{
	gint		pos = 0;
	gint		fd = -1;
	gint		size;
	gchar		buf [ CONV_MAX_CARS + 10 ];
	gboolean	BoolRet = TRUE;
	gchar		*Ptr = NULL;
	gboolean	BoolFinfTotal = FALSE;
	gint		TimeTotal = 0;
	gboolean	BoolErreurMplayer = FALSE;
	gint		Total = 0;
	gint		Value = 0;

	/* PRINT_FUNC_LF(); */

	conv.conversion_percent = 0.0;
	conv.bool_percent_conv = TRUE;
	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);

	/* Ne pas stopper mplayer */
	/* conv.code_fork_conv = -1; */

	do {
		pos = -1;
		do {

			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

			if (strstr(buf,"Audio: no sound")) {
				BoolRet = FALSE;
				BoolErreurMplayer = TRUE;
				break;
			}
			if (strstr(buf,"relocation error")) {
				BoolRet = FALSE;
				BoolErreurMplayer = TRUE;
				break;
			}

		} while (FALSE == conv.bool_stop && (buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		if (TRUE == BoolErreurMplayer) break;

		if (FALSE == conv.bool_stop && p_TypeConv != MPLAYER_AUDIO_TO_WAV) {

			// A:   7.3 (07.2) of 276.0 (04:36.0)  0.6%
			// A:4061.6 ( 1:07:41.6) of 4089.0 ( 1:08:09.0)  0.0

			if (BoolFinfTotal == FALSE) {
				if ((Ptr = strstr(buf,"A:")) != NULL) {
					if ((Ptr = strstr(buf,"of")) != NULL) {
						Ptr ++;
						Ptr ++;
						Ptr ++;
						sscanf( Ptr, "%d", &TimeTotal );
						Total = TimeTotal;
						BoolFinfTotal = TRUE;
					}
				}
			}
			else if( buf[0] == 'A' && buf[1] == ':' ) {
				Ptr = &buf[2];
				while (*Ptr == ' ') Ptr++;
				sscanf( Ptr, "%d", &Value );
				conv.conversion_percent = ( (double)Value / (double)Total ) ;
				if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
				conv.total_percent = ((double)conv.rip_completed +
						      (double)conv.encode_completed +
						      (double)conv.extract_percent +
						      (double)conv.conversion_percent) /
						      ((double)conv.total_rip + (double)conv.total_convert);
				conv.bool_percent_conv = TRUE;
			}
		}

		if (buf[pos] != '\n') {
			pos ++;
			buf[pos++] = '\n';
			buf[pos] = '\0';
		}

	} while (FALSE == conv.bool_stop && (size > 0));
	close(fd);

	if (TRUE == BoolErreurMplayer) {
		g_print("\n");
		g_print("BoolErreurMplayer = TRUE\n");
		g_print("KILL Process mplayer = ");
		if ((kill (conv.code_fork_conv, SIGKILL) != 0))
			g_print ("ERREUR\n");
		else	g_print ("OK\n");
		g_print("\n");
	}

	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
	return (BoolRet);
}
// SOX
//
void conv_with_sox_ARGS (gchar **args)
{
	gint	fd;
	gint	size;
	gint	pos = 0;
	gchar	buf [ CONV_MAX_CARS + 10 ];
	float	Percent;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		if( strstr( buf, "In:" )) {

			gchar	*Ptr = NULL;

			// In:76.6% 00:52:11.54 [00:15:58.40] Out:138M  [======|=====!] Hd:0.0 Clip:0
			if( NULL != ( Ptr = strchr( buf, ':' ))) {
				Ptr ++ ;
				if( sscanf( Ptr, "%f", &Percent ) > 0 ) {
					conv.conversion_percent = (Percent / 100.0);
					if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.0;
					conv.total_percent = ((double)conv.rip_completed +
							      (double)conv.encode_completed +
							      (double)conv.extract_percent +
							      (double)conv.conversion_percent) /
							      ((double)conv.total_rip + (double)conv.total_convert);
					conv.bool_percent_conv = TRUE;
				}
			}
		}

		buf [ 0 ] = '\0';
		buf [ 1 ] = '\0';
		buf [ 2 ] = '\0';

	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
gchar **conv_with_sox_get_param( gchar *filesrc, gchar *filedest, gchar *frequence, gchar *voie, gchar *bits )
{
	gint	pos;
	gchar	**PtrTabArgs = NULL;

	PtrTabArgs = filelc_AllocTabArgs();
	pos = 3;
	PtrTabArgs [ pos++ ] = g_strdup ("sox");
	PtrTabArgs [ pos++ ] = g_strdup ("-t");
	PtrTabArgs [ pos++ ] = g_strdup (".wav");
	PtrTabArgs [ pos++ ] = g_strdup (filesrc);
	PtrTabArgs [ pos++ ] = g_strdup ("-S");
	PtrTabArgs [ pos++ ] = g_strdup ("-r");
	PtrTabArgs [ pos++ ] = g_strdup (frequence);
	PtrTabArgs [ pos++ ] = g_strdup ("-c");
	PtrTabArgs [ pos++ ] = g_strdup (voie);

	/*
	switch (atoi (bits)) {
		case 8 :  PtrTabArgs [ pos++ ] = g_strdup ("-1"); break;
		case 16 : PtrTabArgs [ pos++ ] = g_strdup ("-2"); break;
		case 24 : PtrTabArgs [ pos++ ] = g_strdup ("-3"); break;
		case 32 : PtrTabArgs [ pos++ ] = g_strdup ("-4"); break;
		case 64 : PtrTabArgs [ pos++ ] = g_strdup ("-8"); break;
		default : PtrTabArgs [ pos++ ] = g_strdup ("-2"); break;
	}
	PtrTabArgs [ pos++ ] = g_strdup ("-o");
	PtrTabArgs [ pos++ ] = g_strdup (filedest);
	PtrTabArgs [ pos++ ] = NULL;
	*/
	switch (atoi (bits)) {
		case 8 :  PtrTabArgs [ pos++ ] = g_strdup ("-b"); PtrTabArgs [ pos++ ] = g_strdup ("8"); break;
		case 16 : PtrTabArgs [ pos++ ] = g_strdup ("-b"); PtrTabArgs [ pos++ ] = g_strdup ("16"); break;
		case 24 : PtrTabArgs [ pos++ ] = g_strdup ("-b"); PtrTabArgs [ pos++ ] = g_strdup ("24"); break;
		case 32 : PtrTabArgs [ pos++ ] = g_strdup ("-b"); PtrTabArgs [ pos++ ] = g_strdup ("32"); break;
		case 64 : PtrTabArgs [ pos++ ] = g_strdup ("-b"); PtrTabArgs [ pos++ ] = g_strdup ("64"); break;
		default : PtrTabArgs [ pos++ ] = g_strdup ("-b"); PtrTabArgs [ pos++ ] = g_strdup ("16"); break;
	}
	PtrTabArgs [ pos++ ] = g_strdup (filedest);
	PtrTabArgs [ pos++ ] = NULL;

	return( (gchar **)PtrTabArgs );
}
gchar **conv_with_sox_float_get_param( gchar *filesrc, gchar *filedest )
{
	gint	pos;
	gchar	**PtrTabArgs = NULL;

	PtrTabArgs = filelc_AllocTabArgs();
	pos = 3;
	PtrTabArgs [ pos++ ] = g_strdup ("sox");
	PtrTabArgs [ pos++ ] = g_strdup (filesrc);
	PtrTabArgs [ pos++ ] = g_strdup ("-t");
	PtrTabArgs [ pos++ ] = g_strdup ("wavpcm");
	PtrTabArgs [ pos++ ] = g_strdup ("-S");
	//PtrTabArgs [ pos++ ] = C_strdup ("-f");
	PtrTabArgs [ pos++ ] = g_strdup ("-e");
	PtrTabArgs [ pos++ ] = g_strdup ("floating-point");
	// PtrTabArgs [ pos++ ] = C_strdup ("-o");
	PtrTabArgs [ pos++ ] = g_strdup (filedest);
	PtrTabArgs [ pos++ ] = NULL;

	return( (gchar **)PtrTabArgs );
}
gchar **conv_lsdvd_read_get_param( gchar *p_PathDvd )
{
	gint	pos;
	gchar	**PtrTabArgs = NULL;

	PtrTabArgs = filelc_AllocTabArgs();
	pos = 3;
	PtrTabArgs [ pos++ ] =  g_strdup( "lsdvd" );
	PtrTabArgs [ pos++ ] =  g_strdup( p_PathDvd );
	PtrTabArgs [ pos++ ] =  g_strdup( "-x" );
	PtrTabArgs [ pos++ ] =  g_strdup( "2>&1" );
	PtrTabArgs [ pos++ ] = NULL;

	return( (gchar **)PtrTabArgs );
}
// FLAC
//
gboolean conv_with_flac_ARGS (gchar **args)
{
	gint		 pos = 0;
	gint		 fd;
	gint		 size;
	gint		 sector = 0;
	gchar		 buf [ CONV_MAX_CARS + 10 ];
	gchar		*Ptr = NULL;
	gboolean	 BoolNoErrorRet = TRUE;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do {
		/*
		01.flac: 27% complete
		*/
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		if ((Ptr = strstr (buf, "ERROR"))) {
			/*
			coldplay_test.flac: ERROR, MD5 signature mismatch
			coldplay_test.wav: ERROR while decoding metadata
				state = FLAC__STREAM_DECODER_END_OF_STREAM
			*/
			BoolNoErrorRet = FALSE;
			break;
		}

		/* exemple du contenu de buf
		*
		a.wav: 5% complete, ratio=0,534
		a.wav: 41% complete, ratio=0,489
		a.wav: 91% complete, ratio=0,455
		*/
		if ((Ptr = strchr (buf, ':'))) {
			Ptr ++;
			Ptr ++;
			sscanf(Ptr, "%d", &sector);

			conv.conversion_percent = (double)((sector * 0.1) / 10);
			if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
			conv.total_percent = ((double)conv.rip_completed +
					      (double)conv.encode_completed +
					      (double)conv.extract_percent +
					      (double)conv.conversion_percent) /
					      ((double)conv.total_rip + (double)conv.total_convert);
			conv.bool_percent_conv = TRUE;
		}
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;

	return (BoolNoErrorRet);
}
// MAC
//
void conv_with_mac_ARGS (gchar **args)
{
	gint   pos = 0;
	gint   fd;
	gint   size;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gint   percent;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	/*
	mac ./01.wav ./02.ape -c2000
	--- Monkey's Audio Console Front End (v 3.99) (c) Matthew T. Ashland ---
	Compressing (normal)...
	Progress: 8.4% (8.6 seconds remaining, 0.8 seconds total)
	Progress: 13.3% (7.6 seconds remaining, 1.2 seconds total)
	Progress: 19.3% (6.9 seconds remaining, 1.6 seconds total)
	Progress: 24.1% (6.4 seconds remaining, 2.0 seconds total)
	Progress: 26.6% (6.1 seconds remaining, 2.2 seconds total)
	Progress: 29.0% (5.9 seconds remaining, 2.4 seconds total)
	Progress: 53.1% (3.8 seconds remaining, 4.3 seconds total)
	Progress: 79.7% (1.7 seconds remaining, 6.6 seconds total)
	Progress: 100.0% (0.0 seconds remaining, 8.2 seconds total)
	Success...
	*/
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		sscanf(buf, "Progress: %d)", &percent);
		if (percent == 0) continue;

		conv.conversion_percent = (double)((percent * 0.1) / 10);
		if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
		conv.total_percent = ((double)conv.rip_completed +
				      (double)conv.encode_completed +
				      (double)conv.extract_percent +
				      (double)conv.conversion_percent) /
				      ((double)conv.total_rip + (double)conv.total_convert);
		conv.bool_percent_conv = TRUE;
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// WAVPACK
//
void conv_with_wavpack_ARGS (gchar **args)
{
	gint       pos = 0;
	gboolean   BoolPass = FALSE;
	gint       fd;
	gint       size;
	gchar      buf [ CONV_MAX_CARS + 10 ];
	gchar     *ptr = NULL;
	gint       percent;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	conv.conversion_percent = 1.0;
	conv.bool_percent_conv = TRUE;
	do
	{
		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		/*
		fish@debian:~/tmp$ wavpack -y -f -j0 ./1.wav

		 WAVPACK  Hybrid Lossless Audio Compressor  Linux Version 4.32  2006-04-05
		 Copyright (c) 1998 - 2005 Conifer Software.  All Rights Reserved.

		creating ./1.wv,  10% done...
		 14% done...
		 18% done...
		 32% done...
		 38% done...
		 41% done...
		 49% done...
		 53% done...
		created ./1.wv in 4.25 secs (lossless, 20.89%)
		*/

		if ((ptr = strstr (&buf[0], "done"))) {
			ptr -= 5;
			while (*ptr == ' ') ptr ++;

			if (sscanf(ptr, "%d",&percent) == 1) {

				if (percent == 0) continue;

				conv.conversion_percent = (double)((percent * 0.1) / 10);
				if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
				conv.total_percent = ((double)conv.rip_completed +
						      (double)conv.encode_completed +
						      (double)conv.extract_percent +
						      (double)conv.conversion_percent) /
						      ((double)conv.total_rip + (double)conv.total_convert);
				conv.bool_percent_conv = TRUE;
				BoolPass = TRUE;
			}
		}

	} while (FALSE == conv.bool_stop && size != 0);

	if (BoolPass == FALSE) {

		percent = 100;
		conv.conversion_percent = 1.0;
		conv.total_percent = ((conv.encode_completed) + (double)conv.conversion_percent) / conv.total_convert;
		conv.bool_percent_conv = TRUE;
	}

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// OGGENC
//
void conv_with_oggenc_ARGS (gchar **args)
{
	gint   pos = 0;
	gint   fd;
	gint   size;
	gint   sector;
	gint   end;
	gchar  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	conv.conversion_percent = 0.0;

	do {
		size = -1;
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while (buf[pos] != '\b' && buf[pos] != '\r' && buf[pos] != '\n' && size > 0);
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		sector = -1;
		end = -1;

		/*
		[  3,0%] [ 0m17s remaining]
		*/
		if (sscanf(buf, "\t[%d.%d%%]", &sector, &end) != 2) {
			sscanf(buf, "\t[%d,%d%%]", &sector, &end);
		}

		if (sector > -1 && end > -1)
		{
			conv.conversion_percent = (double)(sector + (end * 0.1)) / 100;
			if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
			conv.total_percent = ((double)conv.rip_completed +
					      (double)conv.encode_completed +
					      (double)conv.extract_percent +
					      (double)conv.conversion_percent) /
					      ((double)conv.total_rip + (double)conv.total_convert);
			conv.bool_percent_conv = TRUE;
		}
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// FAAC
//
void conv_with_faac_ARGS (gchar **args)
{
	gint   pos = 0;
	gint   fd;
	gint   size;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gint   dum = 0;
	gint   sector = 0;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	/*
	   53/9518  (  0%)|  108.2  |    0.1/18.0   |   12.31x | 17.9
	  250/9518  (  2%)|  117.7  |    0.5/17.5   |   12.62x | 17.1
	  450/9518  (  4%)|  122.8  |    0.8/17.9   |   12.38x | 17.0
	  650/9518  (  6%)|  124.8  |    1.2/17.9   |   12.37x | 16.6
	 2100/9518  ( 22%)|  120.5  |    3.8/17.2   |   12.89x | 13.4
	 9518/9518  (100%)|  124.0  |   17.2/17.2   |   12.86x | 0.0
	*/
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		sscanf(buf, "%d/%d (%d)", &dum, &dum, &sector);
		if (sector == 0) continue;

		conv.conversion_percent = (double)((sector * 0.1) / 10);
		if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
		conv.total_percent = ((double)conv.rip_completed +
				      (double)conv.encode_completed +
				      (double)conv.extract_percent +
				      (double)conv.conversion_percent) /
				      ((double)conv.total_rip + (double)conv.total_convert);
		conv.bool_percent_conv = TRUE;
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// LAME
//
void conv_with_lame_ARGS (gchar **args)
{
	gint   pos = 0;
	gint   fd;
	gint   size;
	gint   sector;
	gint   end;
	gchar  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		sector = -1;
		end = -1;

		if (sscanf(buf, "%d/%d", &sector, &end) == 2) {

			conv.conversion_percent = (double)sector/end;
			if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
			conv.total_percent = ((double)conv.rip_completed +
					      (double)conv.encode_completed +
					      (double)conv.extract_percent +
					      (double)conv.conversion_percent) /
					      ((double)conv.total_rip + (double)conv.total_convert);
			conv.bool_percent_conv = TRUE;
		}
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// OGG123
//
void conv_with_ogg123_ARGS (gchar **args)
{
	gint   fd;
	gint   pos = 0;
	gint   size;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gint   Dummy;
	gint   Min = -1, Sec = -1, Cent = -1;
	// gint   Code = -1;
	gint   TT_Sector = -1;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do {
		size = 0;
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		/*
		Time: 00:39,17 [03:57,86] of 04:37,03  (190,3 kbit/s)  Output Buffer 100,0%
		*/
		// Code = -1;

		if (TT_Sector == -1) {
			sscanf(buf, "Time: %d:%d,%d [%d:%d,%d] of %d:%d,%d",
				&Dummy, &Dummy, &Dummy, &Dummy, &Dummy, &Dummy, &Min, &Sec, &Cent);
			if (Min > -1 && Sec > -1 && Cent > -1) {
				TT_Sector = ((Min * 60) + Sec);
			}
		} else {
			sscanf(buf, "Time: %d:%d,%d", &Min, &Sec, &Cent);
			if (Min > -1 && Sec > -1 && Cent > -1) {

				conv.conversion_percent = (double)((Min * 60) + Sec) / TT_Sector;

				if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;

				conv.total_percent = (conv.encode_completed + (double)((Min * 60) + Sec) / TT_Sector) / conv.total_convert;
				conv.bool_percent_conv = TRUE;
			}
		}
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// NORMALISE
//
void conv_with_normalise_ARGS (gchar **args)
{
	gint       end = 0;
	gchar     *ptr = NULL;
	gchar      buf [ CONV_MAX_CARS + 10 ];
	gint       fd;
	gint       size = 0;
	gint       pos = 0;
	gboolean   bool_pass_2 = TRUE;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do
	{
		/*
		$ normalize-audio --peak "/home/cat/Musique/CD/PEAK_Charles Aznavour/01.wav" --
		Computing levels...
		 01.wav            100% done, ETA 00:00:00 (batch 100% done, ETA 00:00:00)
		Applying adjustment of 0,82dB to /home/cat/Musique/CD/PEAK_Charles Aznavour/01.wav...
		 01.wav            100% done, ETA 00:00:00 (batch 100% done, ETA 00:00:00)
		*/

		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);
		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		// g_print("BUF = %s\n", buf);
		// 1_77842900.wav     35% done, ETA 00:00:02 (batch  35% done, ETA 00:00:02)

		if( NULL != ( ptr = strstr( buf, "done" ))) {

			ptr --;
			ptr --;
			while( *ptr != ' ' ) ptr --;
			ptr ++;

			if (sscanf(ptr, "%d",&end) == 1) {

				if (bool_pass_2 == TRUE) {
					if (end >= 100) bool_pass_2 = FALSE;
					conv.conversion_percent = (double)end / 100.0;
					if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
					// conv.total_percent = (conv.encode_completed + (double)conv.conversion_percent) / conv.total_convert;
					conv.total_percent = ((double)conv.rip_completed +
							      (double)conv.encode_completed +
							      (double)conv.extract_percent +
							      (double)conv.conversion_percent) /
							      ((double)conv.total_rip + (double)conv.total_convert);
					conv.bool_percent_conv = TRUE;
				}
				else {
					// conv.conversion_percent = (double)((end * 0.1) / 10);
					conv.conversion_percent = (double)end / 100.0;
					if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
					conv.total_percent = ((double)conv.rip_completed +
							      (double)conv.encode_completed +
							      (double)conv.extract_percent +
							      (double)conv.conversion_percent) /
							      ((double)conv.total_rip + (double)conv.total_convert);
					conv.bool_percent_conv = TRUE;
				}
			}
		}
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
void conv_with_normalise_get_PEAK_RMS_GROUP_ARGS (gchar **args)
{
	gchar     *ptr = NULL;
	gchar      buf [ CONV_MAX_CARS + 10 ];
	gint       fd;
	gint       size;
	gint       pos = 0;
	gdouble    value = 0.0;
	gdouble    dmin = 0.0;
	gdouble    dmax = 0.0;
	gboolean   BoolPass = TRUE;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);
	do
	{
		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		if ((ptr = strstr (&buf[0], "dBFS"))) {
			while (*ptr != ' ') ptr ++;
			while (*ptr == ' ') ptr ++;

			/*g_print ("buf = %s\n", buf);*/
			value = atof(ptr);
			if (BoolPass == TRUE) {
				dmin = dmax = value;
				BoolPass = FALSE;
			}
			if (dmin > value) dmin = value;
			if (dmax < value) dmax = value;
		}

	} while (FALSE == conv.bool_stop && size != 0);

	/*
	g_print ("value = %f, dmin = %f,  dmax = %f\n", value, dmin, dmax);
	g_print ("dmax - dmin - 0.1 = %f\n", (dmax - dmin) - 0.1);
	g_print ("dmin - dmax - 0.1 = %f\n", (dmin - dmax) - 0.1);
	conv.value_PEAK_RMS_GROUP_ARGS = (dmin - dmax) - 0.1;
	conv.value_PEAK_RMS_GROUP_ARGS = dmax - dmin - 0.1;
	g_print ("conv.value_PEAK_RMS_GROUP_ARGS = %f\n", conv.value_PEAK_RMS_GROUP_ARGS);
	g_print ("****\n");
	*/
	g_print ("\nSUR LES TRES BONS CONSEILS DE @Dzef  ;-)\n");
	g_print ("\tdmin = %f\n", dmin);
	g_print ("\tdmax = %f\n", dmax);
	g_print ("\t0.0 - dmax(%f) - 0.1 = %f\n", dmax, 0.0 - dmax - 0.1);
	conv.value_PEAK_RMS_GROUP_ARGS = 0.0 - dmax - 0.1;
	g_print ("\tconv.value_PEAK_RMS_GROUP_ARGS = %f\n\n", conv.value_PEAK_RMS_GROUP_ARGS);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// REPLAGAIN
//
void conv_with_replaygain_ARGS (gchar **args)
{
	gint            pos = 0;
	gint            size = 0;
	gchar           buf[CONV_MAX_CARS + 10];
	gint            fd;

	conv.bool_percent_conv = TRUE;
	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do
	{
		size = -1;
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);
			conv.bool_percent_conv = TRUE;

		} while (buf[pos] != '\b' && buf[pos] != '\r' && buf[pos] != '\n' && size > 0);
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
}
// AACPLUSENC
//
void conv_with_aacplusenc_ARGS (gchar **args)
{
	gint             pos = 0;
	gint             size = 0;
	gchar            buf[CONV_MAX_CARS + 10];
	gint             fd;
	gchar		*Ptr = NULL;

	conv.conversion_percent = 0.0;
	conv.bool_percent_conv = TRUE;
	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		if ((Ptr = strchr (buf, '['))) {
			Ptr ++;
			conv.conversion_percent = (double)((atoi(Ptr) * 0.1) / 10);
			if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
			conv.total_percent = ((double)conv.rip_completed +
					      (double)conv.encode_completed +
					      (double)conv.extract_percent +
					      (double)conv.conversion_percent) /
					      ((double)conv.total_rip + (double)conv.total_convert);
			conv.bool_percent_conv = TRUE;
		}

	} while (FALSE == conv.bool_stop && size != 0);

	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
	close(fd);
}
// MPPENC
//
void conv_with_mppenc_ARGS (gchar **args)
{
	gint   pos = 0;
	gint   fd;
	gint   size;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gint   percent = 0;
	gint   old_percent = 0;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		sscanf(buf, "%d", &percent);
		if (percent <= 0) continue;
		if (percent >= old_percent) {
			conv.conversion_percent = (double)((percent * 0.1) / 10);
			if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
			conv.total_percent = ((double)conv.rip_completed +
					      (double)conv.encode_completed +
					      (double)conv.extract_percent +
					      (double)conv.conversion_percent) /
					      ((double)conv.total_rip + (double)conv.total_convert);
			conv.bool_percent_conv = TRUE;
			old_percent = percent;
		}
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// ICEDAX
//
gboolean conv_with_icedax_tool_ARGS (gchar **args)
{
	gint		pos = 0;
	gint		size = 0;
	gchar		buf[CONV_MAX_CARS + 10];
	gint		fd;
	gboolean	BoolRet = TRUE;
	gint		NbrError = 0;

	conv.bool_percent_conv = TRUE;
	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		/*
		POSSIBILITE D ERREUR PRODUITE:
		------------------------------
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... retrying in 1 second.
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... retrying in 1 second.
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... retrying in 1 second.
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... retrying in 1 second.
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... giving up.
		BUF = device /dev/hdb does not support generic_scsi; falling back to cooked_ioctl instead
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... retrying in 1 second.
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... retrying in 1 second.
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... retrying in 1 second.
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... retrying in 1 second.
		BUF = Error trying to open /dev/hdb exclusively (Device or resource busy)... giving up.
		*/
		if (buf [ 0 ] != '\n') g_print("BUF = %s", buf);
		if (NULL != (strstr (buf, "Device or resource busy"))) {
			if (NbrError++ > 3) {
				PRINT("ERREUR TROUVEE 3 FOIS:\n\tDevice or resource busy");
				BoolRet = FALSE;
				conv_stop_conversion ();
			}
		}

	} while (FALSE == conv.bool_stop && size != 0);

	conv.code_fork_conv = -1;
	close(fd);

	return (BoolRet);
}
// MPG321
//
void conv_with_mpg321_ARGS (gchar **args)
{
	gint   fd;
	gint   pos = 0;
	gint   size = -1;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gchar  type [ 20 ];
	gint   Code, TT_Sector = -1;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		/*
		Frame#   792 [12190], Time: 00:20.68 [05:18.45]
		sscanf(buf, "##: %d [%d]%s @ %d", &code, type, &sector);
		*/
		Code = -1;
		if (TT_Sector == -1) {
			sscanf(buf, "%s %d [%d]", type, &Code, &TT_Sector);
		} else {
			sscanf(buf, "%s %d", type, &Code);
			if (Code > -1 && Code <= TT_Sector) {
				conv.conversion_percent = (double)Code / TT_Sector;

				if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;

				conv.total_percent = (conv.encode_completed + (double)Code / TT_Sector) / conv.total_convert;
				conv.bool_percent_conv = TRUE;
			}
		}
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// SHORTEN
//
void conv_with_shorten_ARGS (gchar **args)
{
	gint   fd;
	gint   size;
	gint   pos = 0;
	gchar  buf [ CONV_MAX_CARS + 10 ];

	/* PRINT_FUNC_LF(); */

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		conv.conversion_percent = 1.0;
		conv.total_percent = 1.0;
		conv.bool_percent_conv = TRUE;
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// FAAD
//
void conv_with_faad_ARGS (gchar **args)
{
	gint   fd;
	gint   size;
	gint   pos = 0;
	gchar  buf [ CONV_MAX_CARS + 10 ];

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	/*
	39% decoding 05.m4a.
	52% decoding 05.m4a.
	60% decoding 05.m4a.
	67% decoding 05.m4a.
	77% decoding 05.m4a.
	84% decoding 05.m4a.
	*/
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

			if (strstr(buf,"Audio: no sound")) {
				conv_stop_conversion ();
				break;
			}

		} while (!conv.bool_stop && (buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));

		if (buf[pos] != '\n') {
			pos ++;
			buf[pos++] = '\n';
			buf[pos] = '\0';
		}

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

	} while (FALSE == conv.bool_stop && (size > 0));

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// MPPDEC
//
void conv_with_mppdec_ARGS (gchar **args)
{
	gint   fd;
	gint   size;
	gint   pos = 0;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gint   percent;
	gchar *ptr = NULL;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		/*
		0:00.02/    3:51.29 decoded ( 0.0%)
		1:40.64/    3:51.29 decoded (43.5%)
		*/
		/*sscanf(buf, "%d", &percent);*/

		if ((ptr = strstr( buf, "decoded ("))) {
			/*g_print ("-->%s\n", ptr);*/
			ptr += 9;
			/*g_print ("-->%s\n", ptr);*/
			sscanf(ptr, "%d", &percent);
			/*g_print ("---->%d\n", percent);*/
		}
		else {
			continue;
		}
		/*
		pos = sscanf(buf, "%d:%d.%d/\t%d:%d:.%d decoded (%d", &dummy,&dummy,&dummy,&dummy,&dummy,&dummy, &percent);
		g_print (">pos     = %d\n", pos);
		if (pos != 7) {
			continue;
		}
		g_print (">pos     = %d\n", pos);
		g_print (">percent = %d\n", percent);
		*/
		conv.conversion_percent = (double)((percent * 0.1) / 10);
		if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
		conv.total_percent = ((double)conv.rip_completed +
				      (double)conv.encode_completed +
				      (double)conv.extract_percent +
				      (double)conv.conversion_percent) /
				      ((double)conv.total_rip + (double)conv.total_convert);
		conv.bool_percent_conv = TRUE;
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
// WVUNPACK_WAVPACK_TO_WAV
//
void conv_with_wvunpack_ARGS (gchar **args)
{
	gint       fd;
	gint       size;
	gint       pos = 0;
	gchar      buf [ CONV_MAX_CARS + 10 ];
	gchar     *ptr = NULL;
	gint       percent;

	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do
	{
		pos = -1;
		do
		{
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		/*
		fish@debian:~/Musique/aaa$ wvunpack -y ./1.wv

		 WVUNPACK  Hybrid Lossless Audio Decompressor  Linux Version 4.32  2006-04-05
		 Copyright (c) 1998 - 2005 Conifer Software.  All Rights Reserved.

		restoring ./1.wav,  49% done...
		 61% done...
		 82% done...
		restored ./1.wav in 3.21 secs (lossless, 20.89%)
		*/
		if ((ptr = strstr (&buf[0], "done"))) {
			ptr -= 5;
			while (*ptr == ' ') ptr ++;

			if (sscanf(ptr, "%d", &percent) == 1) {

				if (percent == 0) continue;

				conv.conversion_percent = (double)((percent * 0.1) / 10);
				if (conv.conversion_percent >= CONV_MAX_VALUE_PERCENT) conv.conversion_percent = 1.000000;
				conv.total_percent = ((double)conv.rip_completed +
						      (double)conv.encode_completed +
						      (double)conv.extract_percent +
						      (double)conv.conversion_percent) /
						      ((double)conv.total_rip + (double)conv.total_convert);
				conv.bool_percent_conv = TRUE;
			}
		}
		/*
		fish@debian:~/Musique/aaa/new$ wvunpack -y -m ./20.wv

		 WVUNPACK  Hybrid Lossless Audio Decompressor  Linux Version 4.32  2006-04-05
		 Copyright (c) 1998 - 2005 Conifer Software.  All Rights Reserved.

		restoring ./20.wav,  34% done...

		original md5:  2b74aed5a5d62d1c3b0e6b96d5147cc9
		unpacked md5:  2b74aed5a5d62d1c3b0e6b96d5147cc9
		restored ./20.wav in 17.23 secs (lossless, 26.45%)
		else if ((ptr = strstr (&buf[0], "original md5:"))) {
		}
		else if ((ptr = strstr (&buf[0], "unpacked md5:"))) {
		}
		else if ((ptr = strstr (&buf[0], "restored"))) {
		}
		*/
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_conv = -1;
	conv.conversion_percent = 0.0;
}
//
// SPLIT
void conv_with_split_ARGS (gchar **args)
{
	gint            pos = 0;
	gint            size = 0;
	gchar           buf[CONV_MAX_CARS + 10];
	gint            fd;

	conv.bool_percent_conv = TRUE;
	fd = conv_call_exec (args, &conv.code_fork_conv, STDERR_FILENO);

	do
	{
		size = -1;
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);
			conv.bool_percent_conv = TRUE;

		} while (buf[pos] != '\b' && buf[pos] != '\r' && buf[pos] != '\n' && size > 0);
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

	} while (FALSE == conv.bool_stop && size != 0);

	conv.code_fork_conv = -1;
	close(fd);
}
// CDDBTOOL
//
void conv_with_cddb_tool_ARGS (gchar **args)
{
	gint             pos = 0;
	gint             size = 0;
	gchar            buf[CONV_MAX_CARS + 10];
	gint             fd;

	conv.bool_percent_conv = TRUE;
	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

	} while (FALSE == conv.bool_stop && size != 0);

	conv.code_fork_conv = -1;
	close(fd);
}
// WAVSPLIT
//
void conv_with_wavsplit_ARGS (gchar **args)
{
	gint             pos = 0;
	gint             size = 0;
	gchar            buf[CONV_MAX_CARS + 10];
	gint             fd;

	conv.bool_percent_conv = TRUE;
	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

	} while (FALSE == conv.bool_stop && size != 0);

	conv.code_fork_conv = -1;
	close(fd);
}
// A52DEC
//
void conv_with_a52dec_ARGS (gchar **args)
{
	gint             pos = 0;
	gint             size = 0;
	gchar            buf[CONV_MAX_CARS + 10];
	gint             fd;
	FILE		*fp = NULL;
	gchar		*FileSave = g_strdup (args[ 8 ]);

	g_free (args[ 7 ]);	args[ 7 ] = NULL;
	g_free (args[ 8 ]);	args[ 8 ] = NULL;

	conv.bool_percent_conv = TRUE;
	fd = conv_call_exec (args, &conv.code_fork_conv, STDOUT_FILENO);

	fp = fopen (FileSave, "w");

	do {
		pos = 0;
		do {
			/*
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			*/
			size = read(fd, &buf [ pos ], 1);
			fprintf (fp, "%c", (short)buf [ 0 ]);


		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

	} while (FALSE == conv.bool_stop && size != 0);

	fclose (fp);
	g_free (FileSave);	FileSave = NULL;

	conv.code_fork_conv = -1;
	close(fd);
}

// CALL CONV
//
gboolean conv_to_convert( gchar **p_TabArgs, gboolean WithParameter, TYPE_CONV type_conv, gchar *info )
{
	gint             pos = 0;
	gboolean	 RetBool = TRUE;

	// PRINT_FUNC_LF();

	if (NORMALISE_CALCUL == type_conv || NORMALISE_APPLIQUE == type_conv || NORMALISE_EXEC == type_conv || NORMALISE_GET_LEVEL == type_conv) {
		conv.BoolIsNormalise = TRUE;
	}
	else if (REPLAYGAIN == type_conv) {
		conv.BoolIsReplaygain = TRUE;
	}
	else if (MPLAYER_AUDIO_TO_WAV == type_conv) {
		conv.BoolIsExtract = TRUE;
	}
	else {
		conv.BoolIsConvert = TRUE;
	}

	// INFORMATION
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("!-----------------------------------------------------------!\n");
		if (WithParameter == TRUE)
		g_print ("!  --With Expert Parameters--\n");
		else
		g_print ("!  --Without Expert Parameters--\n");
		g_print ("!  %s\n", info);
		g_print ("!-----------------------------------------------------------!\n");
		g_print ("!  ");
		for (pos = 0; p_TabArgs[ pos ] != NULL; pos ++) {
			g_print ("%s ", p_TabArgs[ pos ]);
		}
		g_print ("\n!-----------------------------------------------------------!\n");
		for (pos = 0; p_TabArgs[ pos ] != NULL; pos ++) {
			g_print ("p_TabArgs[ %02d ] = %s\n", pos, p_TabArgs[ pos ]);
		}
		g_print ("\n");
	}

	switch (type_conv) {
	case NONE_CONV :
		break;
	case FLAC_FLAC_TO_WAV :
	case FLAC_WAV_TO_FLAC :
		RetBool = conv_with_flac_ARGS( p_TabArgs );
		break;
	case LAME_WAV_TO_MP3 :
	case LAME_FLAC_TO_MP3 :
		conv_with_lame_ARGS( p_TabArgs );
		break;
	case OGGENC_WAV_TO_OGG :
	case OGGENC_FLAC_TO_OGG :
		conv_with_oggenc_ARGS( p_TabArgs );
		break;
	case OGG123_OGG_TO_WAV :
		conv_with_ogg123_ARGS( p_TabArgs );
		break;
	case MPG321_MP3_TO_WAV :
		conv_with_mpg321_ARGS( p_TabArgs );
		break;
	case SOX_WAV_TO_WAV :
		conv_with_sox_ARGS( p_TabArgs );
		break;
	case SHORTEN_SHN_TO_WAV :
		conv_with_shorten_ARGS( p_TabArgs );
		break;
	case FAAD_M4A_TO_WAV :
		conv_with_faad_ARGS( p_TabArgs );
		break;
	case FAAC_WAV_TO_M4A :
		conv_with_faac_ARGS( p_TabArgs );
		break;
	case MPLAYER_WMA_TO_WAV :
	case MPLAYER_RM_TO_WAV :
	case MPLAYER_DTS_TO_WAV :
	case MPLAYER_AIFF_TO_WAV :
	case MPLAYER_WAV_TO_WAV :
	case MPLAYER_AUDIO_TO_WAV :
	case MPLAYER_M4A_TO_WAV :
	case MPLAYER_OGG_TO_WAV :
	case MPLAYER_MP3_TO_WAV :
	case MPLAYER_MPC_TO_WAV :
		RetBool = conv_with_mplayer_ARGS( p_TabArgs, type_conv );
		break;
	case COPY_FILE :
		break;
	case NORMALISE_CALCUL :
	case NORMALISE_APPLIQUE :
	case NORMALISE_EXEC :
		conv_with_normalise_ARGS( p_TabArgs );
		break;
	case NORMALISE_GET_LEVEL:
		conv_with_normalise_get_PEAK_RMS_GROUP_ARGS( p_TabArgs );
		break;
	case MPPDEC_MPC_TO_WAV :
		conv_with_mppdec_ARGS( p_TabArgs );
		break;
	case MPPENC_WAV_TO_MPC :
		conv_with_mppenc_ARGS( p_TabArgs );
		break;
	case MAC_APE_TO_WAV :
	case MAC_WAV_TO_APE :
		conv_with_mac_ARGS( p_TabArgs );
		break;
	case WAVPACK_WAV_TO_WAVPACK :
		conv_with_wavpack_ARGS( p_TabArgs );
		break;
	case WVUNPACK_WAVPACK_TO_WAV :
		conv_with_wvunpack_ARGS( p_TabArgs );
		break;

	case CDPARANOIA_CD_TO_WAV :
	case CDPARANOIA_CD_TO_WAV_EXPERT :
	case CDPARANOIA_CD_TO_WAV_EXPERT_SEGMENT :
	case CDDA2WAV_CD_TO_WAV :
		break;

	case REPLAYGAIN :
		conv_with_replaygain_ARGS( p_TabArgs );
		break;
	case LSDVD :
		conv_with_lsdvd_ARGS( p_TabArgs );
		break;
	case SPLIT :
		conv_with_split_ARGS( p_TabArgs );
		break;
	case CDDB_TOOL :
		conv_with_cddb_tool_ARGS( p_TabArgs );
		break;

	case ICEDAX :
		RetBool = conv_with_icedax_tool_ARGS( p_TabArgs );
		break;

	case AACPLUSENC_WAV_TO_AAC :
		conv_with_aacplusenc_ARGS( p_TabArgs );
		break;

	case WAVSPLIT_EXTRACT :
		conv_with_wavsplit_ARGS( p_TabArgs );
		break;

	case A52DEC_AC3_TO_WAV :
		conv_with_a52dec_ARGS( p_TabArgs );
		break;
	}

	conv_inc_encode_completed ();

	conv.BoolIsConvert = FALSE;
	conv.BoolIsNormalise  = FALSE;
	conv.BoolIsReplaygain = FALSE;
	if (MPLAYER_AUDIO_TO_WAV == type_conv) 	conv.BoolIsExtract = FALSE;

	return (RetBool);
}
//
//
void conv_with_cdparanoia_ARGS (gchar **args)
{
	gint   pos = 0;
	gint   fd;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gint   size = 1;
	gint   start;
	gint   end;
	gint   code;
	gchar  type[50];
	gint   sector;

	/* PRINT_FUNC_LF(); */

	conv.bool_percent_extract = FALSE;

	fd = conv_extract_call_exec (args, &conv.code_fork_extract, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		if ((buf[0] == 'R') && (buf[1] == 'i')) {
			sscanf(buf, "Ripping from sector %d", &start);
		}
		else if (buf[0] == '\t') {
			sscanf(buf, "\t to sector %d", &end);
		}
		else if (buf[0] == '#') {
			sscanf(buf, "##: %d %s @ %d", &code, type, &sector);
			sector /= 1176;
			if (strncmp("[wrote]", type, 7) == 0) {

				conv.extract_percent = (double)(sector-start) / (end-start);
				if (conv.extract_percent >= CONV_MAX_VALUE_PERCENT) conv.extract_percent = 1.;
				conv.total_percent = ((double)conv.rip_completed +
							(double)conv.encode_completed +
							(double)conv.extract_percent +
							(double)conv.conversion_percent) /
							((double)conv.total_rip + (double)conv.total_convert);
				conv.bool_percent_extract = TRUE;
			}
		}
	} while (FALSE == conv.bool_stop && 0 != size);

	close(fd);
	conv.code_fork_extract = -1;
	conv.extract_percent = 0.0;
}
//
//
void conv_with_cdparanoia_mode_expert_ARGS (gchar **args)
{
	gint   pos = 0;
	gint   fd;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gint   size = 1;
	/*
	gint   start;
	gint   end;
	gint   code;
	gchar  type[50];
	gint   sector;
	*/

	/* PRINT_FUNC_LF(); */

	conv.bool_percent_extract = FALSE;

	fd = conv_extract_call_exec (args, &conv.code_fork_extract, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		/*
		if ((buf[0] == 'R') && (buf[1] == 'i')) {
			sscanf(buf, "Ripping from sector %d", &start);
		}
		else if (buf[0] == '\t') {
			sscanf(buf, "\t to sector %d", &end);
		}
		else if (buf[0] == '#') {
			sscanf(buf, "##: %d %s @ %d", &code, type, &sector);
			sector /= 1176;
			if (strncmp("[wrote]", type, 7) == 0) {
				conv.extract_percent = (double)(sector-start) / (end-start);

				if (conv.extract_percent >= CONV_MAX_VALUE_PERCENT) conv.extract_percent = 1.000000;

				conv.total_percent = (conv.rip_completed +
							conv.encode_completed +
							conv.extract_percent +
							conv.conversion_percent) /
							(conv.total_rip + conv.total_convert);

				conv.bool_percent_extract = TRUE;
			}
		}
		*/
	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_extract = -1;
	conv.extract_percent = 0.0;
}
//
//
void conv_with_cdda2wav_ARGS (gchar **args)
{
	gint   pos = 0;
	gint   fd;
	gchar  buf [ CONV_MAX_CARS + 10 ];
	gchar *ptr = NULL;
	gint   size = 1;
	gint   percent;

	conv.bool_percent_extract = FALSE;

	fd = conv_extract_call_exec (args, &conv.code_fork_extract, STDERR_FILENO);
	do {
		/*
		 93%
		 96%
		 99%
		100%  track  7 recorded successfully
		*/
		pos = -1;
		do {
			pos++;
			if (pos >= CONV_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, CONV_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while (((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n')) && (size > 0));
		if (buf[ pos ] != '\n') {
			pos ++;
			buf[ pos ] = '\0';
		}
		pos ++;
		buf[ pos ] = '\0';

		// SI LA FONCTION EST NON NULLE
		if (NULL != conv.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (conv.bool_stop = (*conv.Func_request_stop)())) {
				// ARRET DES CONVERTISSEURS / EXTRACTEURS
				conv_stop_conversion ();
			}
		}

		if ((ptr = strchr (buf, '%')) == NULL) continue;
		ptr --;
		if (ptr > buf && (*ptr >= '0' && *ptr <= '9')) ptr --;
		if (ptr > buf && (*ptr >= '0' && *ptr <= '9')) ptr --;
		if (ptr > buf && (*ptr >= '0' && *ptr <= '9')) ptr --;
		if (*ptr == ' ') ptr ++;

		percent = atol (ptr);
		conv.extract_percent = (double)((percent * 0.1) / 10);
		if (conv.extract_percent >= CONV_MAX_VALUE_PERCENT) conv.extract_percent = 1.;
		conv.total_percent = ((double)conv.rip_completed +
					(double)conv.encode_completed +
					(double)conv.extract_percent +
					(double)conv.conversion_percent) /
					((double)conv.total_rip + (double)conv.total_convert);
		conv.bool_percent_extract = TRUE;

	} while (FALSE == conv.bool_stop && size != 0);

	close(fd);
	conv.code_fork_extract = -1;
	conv.extract_percent = 0.0;
}
//
//
gboolean conv_exec_extract (gboolean WithParameter, TYPE_CONV type_conv, gchar *info)
{
	gint            pos = 0;
	gboolean        RetBool = TRUE;

	// PRINT_FUNC_LF();

	conv.BoolIsExtract = TRUE;

	/* Infos */
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("!-----------------------------------------------------------!\n");
		if (WithParameter == TRUE)
		g_print ("!  --With Expert Parameters--\n");
		else
		g_print ("!  --Without Expert Parameters--\n");
		g_print ("!  %s\n", info);
		g_print ("!-----------------------------------------------------------!\n");
		g_print ("!  ");
		for (pos = 0; conv.ArgExtract[ pos ] != NULL; pos ++) {
			g_print ("%s ", conv.ArgExtract[ pos ]);
		}
		g_print ("\n!-----------------------------------------------------------!\n");

		for (pos = 0; conv.ArgExtract[ pos ] != NULL; pos ++) {
			g_print ("ArgExtract[ %02d ] = %s\n", pos, conv.ArgExtract[ pos ]);
		}
		g_print ("\n");
	}
	if (type_conv == CDPARANOIA_CD_TO_WAV || type_conv == CDPARANOIA_CD_TO_WAV_EXPERT)
		conv_with_cdparanoia_ARGS (conv.ArgExtract);
	else if (type_conv == CDPARANOIA_CD_TO_WAV_EXPERT_SEGMENT)
		conv_with_cdparanoia_mode_expert_ARGS (conv.ArgExtract);
	else if (type_conv == CDDA2WAV_CD_TO_WAV)
		conv_with_cdda2wav_ARGS (conv.ArgExtract);

	conv_inc_rip_completed ();
	conv.BoolIsExtract = FALSE;

	return (RetBool);
}
//
// Algo retravaille depuis:
//	C EN ACTION - 2 ieme Edition - Yves Mettier
//
gboolean conv_copy_src_to_dest (gchar *filesrc, gchar *filedest)
{
	FILE	*fn = NULL;
	FILE	*fe = NULL;
	gchar	buf [ BUFSIZ + 10 ];	// 8192
	size_t	size = libutils_get_size_file (filesrc);
	gulong	blk;
	gulong	cpt;
	size_t	l_read;


	if (0 == size) {
		fprintf (stderr, "La taille de : %s est NULLE !!!\n", filesrc);
		conv_inc_encode_completed ();
		return (FALSE);
	}
	if (NULL == (fe = fopen (filesrc, "r"))) {
		fprintf (stderr, "Echec ouverture en lecture de: %s\n", filesrc);
		conv_inc_encode_completed ();
		return (FALSE);
	}
	if (NULL == (fn = fopen (filedest, "w"))) {
		fprintf (stderr, "Echec ouverture en ecriture de: %s\n", filedest);
		fclose (fe);
		conv_inc_encode_completed ();
		return (FALSE);
	}

	conv.conversion_percent = 0.;
	blk = size / BUFSIZ;
	// conv.conversion_percent = 0.;
	PRINT_FUNC_LF();
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\t%s\n\t%s\n", filesrc, filedest);
	cpt = 0;
	conv.BoolIsCopy = TRUE;
	while ( ! feof (fe)) {

		l_read = fread (buf, sizeof (*buf), BUFSIZ, fe);
		if (ferror (fe)) {
			fprintf (stderr, "Erreur de lecture\n");
			fclose (fn);
			fclose (fe);
			conv.BoolIsCopy = FALSE;
			conv_inc_encode_completed ();
			return (FALSE);
		}
		fwrite (buf, sizeof (*buf), l_read, fn);
		if (ferror (fn)) {
			fprintf (stderr, "Erreur d'ecriture\n");
			fclose (fn);
			fclose (fe);
			conv.BoolIsCopy = FALSE;
			conv_inc_encode_completed ();
			return (FALSE);
		}
		fflush( fn );
		conv.conversion_percent = (double)cpt / (double)blk;
		// if (conv.conversion_percent > 1.) conv.conversion_percent = 1.0;
		// if (conv.conversion_percent < 0.) conv.conversion_percent = 0.0;
		conv.total_percent = ((double)conv.rip_completed +
				      (double)conv.encode_completed +
				      (double)conv.extract_percent +
				      (double)conv.conversion_percent) /
				      ((double)conv.total_rip + (double)conv.total_convert);
		conv.bool_percent_conv = TRUE;
		cpt ++;
	}
	fclose (fn);
	fclose (fe);
	conv.conversion_percent = 0.;
	conv_inc_encode_completed ();
	conv.BoolIsCopy = FALSE;
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\tOK\n");

	return (TRUE);
}








