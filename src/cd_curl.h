 /*
 *  file      : cd_curl.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef cd_curl_h
#define cd_curl_h 1

#include <gtk/gtk.h>

typedef enum {
	SERVER_CDDB_DEFAULT = 0,	// freedb.org
	SERVER_CDDB_PARAM,			// CDDB:  NAME
	SERVER_PROXY_PARAM			// PROXY: NAME and PORT
} SERVER_CDDB;

typedef enum {
	_CDDB_BUFFER_NONE_ = 0,
	_CDDB_BUFFER_TITLE_,
	_CDDB_BUFFER_TITLE_TIME_,
	_CDDB_BUFFER_ARTIST_TITLE_TIME_
} TYPE_CDDB_BUFFER;


gboolean	CdCurl_get_info_cd( void );
void		CdCurl_remove_struct_all( void );
gboolean	CdCurl_test_access_web( void );
void		CdCurl_set_call( gint p_num_from_popup, gchar *p_CallStrDiscID, gchar *p_CallStrGenre );
void		CdCurl_set_list_discid( void );
gchar		*CdCurl_get_title_cd( void );


#endif

