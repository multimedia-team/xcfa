/*
 *  file      : src/builder.c
 *  project   : xcfa
 *  copyright : (C) 2015 by BULIN Claude
 * 
 *  This file is part of xcfa project
 * 
 *  xcfa is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; version 3.
 * 
 *  xcfa is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *  USA
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif
#include <gtk/gtk.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"

GtkBuilder *Builder_open( gchar *p_name_glade, GtkBuilder *p_GtkBuilder )
{
	GError			*p_err = NULL;
	gchar			*CurrentDir = g_get_current_dir ();
	gchar			*PathCurrentDir = NULL;
	gchar			*PathShareProjet = NULL;
	gchar			*Path = NULL;
	GtkBuilder		*GtkBuilderLocal = NULL;
	// if exist alloc return ok
	if( NULL != p_GtkBuilder ) {
		return( p_GtkBuilder );
	}
	// if not init return
	if( NULL == ( GtkBuilderLocal = gtk_builder_new() )) {
		return( NULL );
	}
	gtk_builder_set_translation_domain( GtkBuilderLocal, GETTEXT_PACKAGE );
	PathCurrentDir = g_strdup_printf( "%s/glade/%s", CurrentDir, p_name_glade );
	g_free( CurrentDir );
	CurrentDir = NULL;
/**
#ifdef ENABLE_XDG
	printf(" SEARCH WITH XDG\n" );
	PathShareProjet = xdg_search_data_xdg( p_name_glade );
#else
	printf(" SEARCH WITH-OUT XDG\n" );
	PathShareProjet = g_strdup_printf( "%s/%s/%s", DATA_DIR, PACKAGE_NAME, p_name_glade );
#endif
*/
	if( NULL == (PathShareProjet = xdg_search_data_xdg( p_name_glade )))
		PathShareProjet = g_strdup_printf( "%s/%s/%s", DATA_DIR, PACKAGE_NAME, p_name_glade );
	
	if( FALSE == g_file_test( PathCurrentDir, G_FILE_TEST_EXISTS ) && FALSE == g_file_test( PathShareProjet, G_FILE_TEST_EXISTS )) {
		g_print( "!---------------------------------------------------------------------------------------------------\n" );
		g_print( "! LE FICHIER XML: [ %s ] N'EXISTE PAS !!!\n", PathCurrentDir );
		g_print( "!---------------------------------------------------------------------------------------------------\n" );
		g_print( "! LE FICHIER XML: [ %s ] N'EXISTE PAS !!!\n", PathShareProjet );
		g_print( "!---------------------------------------------------------------------------------------------------\n" );
		g_free( PathCurrentDir );
		PathCurrentDir = NULL;
		g_free( PathShareProjet );
		PathShareProjet = NULL;
		return( NULL );
	}
	else if( TRUE == g_file_test( PathCurrentDir, G_FILE_TEST_EXISTS )) {
		Path = PathCurrentDir;
	}
	else if( TRUE == g_file_test( PathShareProjet, G_FILE_TEST_EXISTS )) {
		Path = PathShareProjet;
	}
	// g_print( "\nPATHNAME FILE %s: %s\n\n", p_name_glade, Path );
	if( gtk_builder_add_from_file( GtkBuilderLocal, Path, &p_err) <= 0 ) {
		g_warning( "Couldn't load builder file: %s", p_err->message );
		g_error_free( p_err );
		g_free( PathCurrentDir );
		PathCurrentDir = NULL;
		g_free( PathShareProjet );
		PathShareProjet = NULL;
		Path = NULL;
		return( NULL );
	}
	g_free( PathCurrentDir );
	PathCurrentDir = NULL;
	g_free( PathShareProjet );
	PathShareProjet = NULL;
	Path = NULL;
	return( GtkBuilderLocal );
}

