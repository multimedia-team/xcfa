 /*
 *  file      : dragNdrop.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */




#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gprintf.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "file.h"
#include "fileselect.h"
#include "poche.h"
#include "split.h"
#include "dragNdrop.h"



/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

extern VAR_SPLIT VarSplit;

enum {
        TARGET_STRING = 0,
        TARGET_URL,
        TARGET_ROOTWIN
};

const GtkTargetEntry drag_types[] = {
	{ "STRING",        0, TARGET_STRING },
	{ "text/uri-list", 0, TARGET_URL }

};

gint n_drag_types = G_N_ELEMENTS (drag_types);


/*
*---------------------------------------------------------------------------
* FUNCTIONS
*---------------------------------------------------------------------------
*/

/* Cette fonction est appelle depuis 'file.c :: file_drag_data_received_file' et elle
*  extrait les noms de fichiers pour les injecter dans le 'GList EnteteFile.GList_file'
*  --
*  entree :
*      GtkWidget *widget : L'adresse du widget destinataire
*      gchar *string     : pointeur buffer de texte
*  retour : -
*/
void dragndrop_list_drag_data (GtkWidget *widget, gchar *string)
{
	gboolean	Bool_add_ok = FALSE;
	gchar		*Src = NULL;
	gchar		*Ptr = NULL;
	gchar		*temp = NULL;
	GSList		*List = NULL;

	// test for input
	if (widget != var_file.Adr_TreeView &&
	    widget != var_file_wav.Adr_TreeView &&
	    widget != var_file_mp3ogg.Adr_TreeView &&
	    widget != var_file_tags.Adr_TreeView &&
	    widget != view.Adr_viewport_image_preview &&
	    widget != view.AdrDrawingarea &&
	    widget != VarSplit.AdrWidgetSpectre) {
		PRINT("ERREUR DRAG N DROP : WIDGET(dragndrop_list_drag_data) INCONNU --> RETOUR\n");
		return;
	}
	
	// ALLOC CHAINE
	Src = dragndrop_xcfa_convert_utf8 (string);
	
	// POINTEUR SUR LA CHAINE TRADUITE
	Ptr = Src;
	while (*Ptr)
	{
		if (NULL != (temp = strchr (Ptr, '\n'))) {
			if (*(temp - 1) == '\r') *(temp - 1) = '\0';
			*temp = '\0';
		}
		// if (widget == VarSplit.AdrWidgetSpectre)
		// 	g_print("PTR = %s\n", Ptr);
		List = g_slist_append (List, Ptr);
		Bool_add_ok = TRUE;
		
		if (NULL == temp) break;

		Ptr = temp + 1;
	}
	
	if (TRUE == Bool_add_ok) {
	
		if (widget == var_file.Adr_TreeView) {

			fileanalyze_add_file_to_treeview (_PATH_LOAD_FILE_ALL_, List);
		}
		else if (widget == var_file_wav.Adr_TreeView) {
			
			fileanalyze_add_file_to_treeview (_PATH_LOAD_FILE_WAV_, List);
		}
		else if (widget == var_file_mp3ogg.Adr_TreeView) {
			
			fileanalyze_add_file_to_treeview (_PATH_LOAD_FILE_MP3OGG_, List);
		}
		else if (widget == var_file_tags.Adr_TreeView) {
			
			fileanalyze_add_file_to_treeview (_PATH_LOAD_FILE_TAGS_, List);
		}
		else if (widget == VarSplit.AdrWidgetSpectre) {
			
			split_load_from_dnd (List);
		}
		else if( widget == view.Adr_viewport_image_preview ) {
			
			pochedir_add_img_file (List);
		}
		else if( widget == view.AdrDrawingarea ) {
			
			poche_add_img_file_to_Drawingarea (List);
		}
		
		g_slist_free (List);
		List = NULL;
	}
	
	// LIBERATION CHAINE
	g_free (Src);
	Src = Ptr = NULL;
}

/* Lors d'un drag and drop, les caracteres sont curieusements transformés en CAR utf-8,
*  par exemple : c8 apparait comme 'C' et '8' dans le texte, il faut alors le chercher et
*  le retransformer en numerique decimale puis le reinjecter dans le buffer
*  --
*  entree :
*      gchar *Texte : pointeur buffer de texte
*  retour : -
*      gchar * : pointeur nouveau buffer de texte
*/
gchar *dragndrop_xcfa_convert_utf8 (gchar *Texte)
{
	gchar    *Ptr = NULL;
	gchar    *New_Str = NULL;
	gchar    *Ptr_Txt = NULL;
	gchar     Str [ 8 ];
	size_t    Len;
	
	/* allocate new string
	*/
	Len = strlen (Texte);

	New_Str = g_strnfill (Len * 3, '\0');
	Ptr = New_Str;

	/* delette this 'file://' in old string
	*/
	while (NULL != (Ptr_Txt = strstr (Texte, "file://"))) {
		strcpy (Ptr_Txt, Ptr_Txt + 7);
	}
	Ptr_Txt = Texte;

	Str [ 0 ] = '0';
	Str [ 1 ] = 'x';
	Str [ 2 ] = '\0';
	Str [ 3 ] = '\0';
	Str [ 4 ] = '\0';
	Str [ 5 ] = '\0';

	while (*Ptr_Txt) {
		if (*Ptr_Txt == '%') {
			Ptr_Txt ++;
			Str [ 2 ] = *Ptr_Txt ++;
			Str [ 3 ] = *Ptr_Txt ++;
			*Ptr = (gint)libutils_hexa_to_int (Str);
			Ptr ++;
		}
		else {
			*Ptr ++ = *Ptr_Txt ++;
		}
	}

	return (New_Str);
}



