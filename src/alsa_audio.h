 /*
 *  file      : alsa_audio.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  Code de lecture audio pour fichiers de type WAV reprit et retravaille
 *  depuis:
 *  	Analyse code depuis: wavbreaker
 *  	par Timothy Robinson et Thomas Perl
 *  	http://wavbreaker.sourceforge.net/
 *
 */


#ifndef alsa_audio_h
#define alsa_audio_h 1

#include <gtk/gtk.h>
#include <alsa/asoundlib.h>

typedef struct {
	snd_pcm_t	*playback_handle;							// Handle
	guint		bytesPerFrame;								// 8, 16, 24, 32, 64 ...
	guint		channels;									// 1, 2, 4 ...
	gint		SampleRate;									// Frequence: 44100 ...
	gint		TotalChunckSize;							// 
	guchar		*buffer;									// Pointeur de buffer
	guint		SizeBuffer;									// Taille requise du buffer
	gint		Sec;										// Duree total en secondes
	gdouble		Percent;									// Poucentage en retour sur la duree total
	gdouble		PercentSel;									// Poucentage en retour depuis la selection
	
	gdouble		PercentBegin;								// 
	gdouble		PercentEnd;									// 
	
	gboolean	BoolThreadEnd;								// 
	guint		HandlerTimeout;								// 
	
	gboolean	StopPlay;									// 
	gboolean	PauseAlsa;									// 
	void		(*FuncSetValueTime) (gdouble p_value);
	void		(*FuncIconeStop) (void);					// 
	
	FILE		*pFile;										// Handler du fichier ouvert
} VAR_ALSA;

extern VAR_ALSA VarAlsa;

void     AlsaAudio_close_device (void);
gboolean AlsaAudio_open_device (const gchar *audio_dev,		// default
				gint p_bitsPerSample,						// 8, 16, 24, ...
				gint p_channels,							// 1, 2, 4, ...
				guint p_samplesPerSec,						// 44100, ...
				guint *p_bufferSize);						// Taille buffer retournee
gboolean AlsaAudio_write (gint size);


#endif

