 /*
 *  file      : split_wav.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
 /*
  * LA SUPPRESSION DE L'APPLICATION WAVSPLIT QUI N'EST PLUS MAINTENUE PAR SON AUTEUR (tobias@fomalhaut.de) ET
  * LE CODAGE EN INTERNE DU SPLIT DEPUIS XCFA ONT ETE INSPIRES PAR CE MAIL:
  *
  * Hello.
  * I use XCFA to manipulate my audio files.
  * I'm running Debian Testing and I have almost all the "external programs" installed.
  * And I say almost, because there is not wavsplit package in Debian Repository, and compiling it conflicts with some issues.
  * Anyway, wavsplit is already a more or less obsolete and abandoned program.
  * Thus, I think it should be better if XCFA would need an alternative program, just like wavbraker, or even sox that it is already suggested by XCFA.
  * Could you please let me know if there is any plan to substitute wavsplit with some other program as bult-in for spliting wave files?
  * Thank you.
  */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include "configuser.h"
#include "split.h"


extern VAR_SPLIT VarSplit;


// 
// 
void SplitWav_extract( void )
{
	gint	Plage;
	WAVE	WaveHeader;
	gchar	*NewName = NULL;
	FILE	*fp = NULL;
	size_t	Ret;
	size_t	RetRead;
	size_t	WriteValue;
	gchar	*buffer;
	guint	SizeBuffer;
	gdouble	Percent;

	tagswav_print( VarSplit.PathNameFile );
		
	SizeBuffer = BUFSIZ;	// BUFSIZ = 8192
	buffer = (gchar *)g_malloc0 ((sizeof(gchar) * SizeBuffer) + 10);
	
	for (Plage = 0; VarSplit.Selecteur [ Plage ] . Nmr != -1; Plage ++) {
				
		if (FALSE == tagswav_read_file( VarSplit.PathNameFile, &WaveHeader )) {
			tagswav_close_file (&WaveHeader);
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print ("\nImpossible d'ouvrir le fichier : \"%s\"\n\n", VarSplit.PathNameFile);
			g_free( buffer );
			buffer = NULL;	
			return;
		}
		
		NewName = g_strdup_printf( "%s/%02d.wav", Config.PathDestinationSplit, Plage +1 );
		fp = fopen( NewName, "w" );
		
		// ECRITURE ENTETE FICHIER WAV
		
		fwrite( &WaveHeader.RIFF.ChunkID, 4, 1, fp );		// RIFF
		fwrite( &WaveHeader.RIFF.ChunkSize, 4, 1, fp );		// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
		fwrite( &WaveHeader.RIFF.Format, 4, 1, fp );		// WAVE
		fwrite( &WaveHeader.FMT.Subchunk1ID, 4, 1, fp );	// 'fmt '
		fwrite( &WaveHeader.FMT.Subchunk1Size, 4, 1, fp );	// taille en octet des données à suivre
		fwrite( &WaveHeader.FMT.AudioFormat, 2, 1, fp );	// format de compression (une valeur autre que 1 indique une compression) 
		fwrite( &WaveHeader.FMT.NumChannels, 2, 1, fp );	// nombre de canaux
		fwrite( &WaveHeader.FMT.SampleRate, 4, 1, fp );		// fréquence d'échantillonage (nombre d'échantillons par secondes) 
		fwrite( &WaveHeader.FMT.ByteRate, 4, 1, fp );		// nombre d'octects par secondes
		fwrite( &WaveHeader.FMT.Blockalign, 2, 1, fp );		// nombre d'octects pour coder un échantillon
		fwrite( &WaveHeader.FMT.BitsPerSample, 2, 1, fp );	// nombre de bits pour coder un échantillon
		fwrite( &WaveHeader.DATA.Subchunk2ID, 4, 1, fp );	// 'data'
		fwrite( &WaveHeader.DATA.Subchunk2Size, 4, 1, fp );	//  taille des données audio( nombre total d'octets codant les données audio)  
		
		RetRead = 0;
		WriteValue = 0;
		g_print( "NewName = %s\n", NewName );
		while(( Ret = fread (buffer,  1, SizeBuffer, WaveHeader.file )) > 0) {
			RetRead += Ret;
			Percent = (float)(RetRead / (float)WaveHeader.DATA.Subchunk2Size) * 100.0;
			if( Percent > VarSplit.Selecteur [ Plage ] . PercentEnd ) break;
			if( Percent >= VarSplit.Selecteur [ Plage ] . PercentBegin ) {
				fwrite( buffer, Ret, 1, fp );
				WriteValue += Ret;
			}
		}
		
		fseek( fp, 4L, SEEK_SET );
		WriteValue += 44;
		WriteValue -= 8;
		fwrite( &WriteValue, 4, 1, fp );	// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
		fseek( fp, 40L, SEEK_SET );
		WriteValue += 8;
		WriteValue -= 44;
		fwrite( &WriteValue, 4, 1, fp );	//  taille des données audio( nombre total d'octets codant les données audio)  
		
		fclose (fp );
		fp = NULL;
		
		tagswav_print( NewName );
		
		g_free( NewName );
		NewName = NULL;
		
		tagswav_close_file (&WaveHeader);
		
	}
	g_free( buffer );
	buffer = NULL;	
}


