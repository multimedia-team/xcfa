 /*
 *  file      : normalise.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
 
#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "file.h"
#include "prg_init.h"
#include "configuser.h"
#include "conv.h"
#include "tags.h"
#include "win_info.h"
#include "win_scan.h"
#include "notify_send.h"
#include "normalise.h"




/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/
typedef struct {
	gchar           *NameFile;
	gchar           *NameFileCopieWav;
	gchar           *NameFileCopieOgg;
	gchar           *NameFileCopieMp3;

	gchar           *NameDestWav;
	gchar           *NameDestOgg;
	gchar           *NameDestMp3;

	gchar           *StrDBFS;
	TAGS            *tags;

	gboolean         BoolWithConvert;

	ETAT_NORMALISE  *EtatNormalise;
	ETAT_TRASH_FILE *EtatTrash;
	
} INDI_FIX;

typedef struct {
	gboolean	bool_etat;
	gchar		*MessUser;
	
	/* Individuel */
	GList		*ListFixWav;
	GList		*ListFixOgg;
	GList		*ListFixMp3;

	GList		*ListPeakWav;
	GList		*ListPeakOgg;
	GList		*ListPeakMp3;

	GList		*ListMixGroupWav;
	GList		*ListMixGroupOgg;
	GList		*ListMixGroupMp3;

	GList		*ListPeakGroupWav;
	GList		*ListPeakGroupOgg;
	GList		*ListPeakGroupMp3;

	/* Exec */
	guint		Handler_Timeout;
	pthread_t	nmr_tid;
	gboolean	bool_thread_end;
	gboolean	bool_set_end_of_user;

	/* Controle duree */
	gboolean	bool_pass_conv;
	gint		pass_conv;
	gint		NbrElementsInList;
	gint		ElementActif;
	double		total_percent;

	/**/
	gchar		*FileOgg;
	gchar		*FileWav;
	gchar		*TmpRep;

	gint		LevelMix;
	
} NORMALISE;

NORMALISE normalise;



// 
// 
/*
GList *normalise_remove_glist (GList *p_list)
{
	GList     *List = NULL;
	gchar     *Ptr = NULL;

	List = g_list_first (p_list);
	while (List) {
		if (NULL != (Ptr = (gchar *)List->data)) {
			g_free (Ptr);
			Ptr = List->data = NULL;
		}
		List = g_list_next(List);
	}
	g_list_free (p_list);
	p_list = NULL;
	return ((GList *)NULL);
}
*/
// 
// 
GList *normalise_remove_glist_STRUCT (GList *p_list)
{
	GList     *List = NULL;
	INDI_FIX  *IndiFix = NULL;

	List = g_list_first (p_list);
	while (List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
			if (NULL != IndiFix->NameFile) {
				g_free (IndiFix->NameFile);
				IndiFix->NameFile = NULL;
			}
			if (NULL != IndiFix->NameFileCopieWav) {
				g_free (IndiFix->NameFileCopieWav);
				IndiFix->NameFileCopieWav = NULL;
			}
			if (NULL != IndiFix->NameFileCopieOgg) {
				g_free (IndiFix->NameFileCopieOgg);
				IndiFix->NameFileCopieOgg = NULL;
			}
			if (NULL != IndiFix->NameFileCopieMp3) {
				g_free (IndiFix->NameFileCopieMp3);
				IndiFix->NameFileCopieMp3 = NULL;
			}

			if (NULL != IndiFix->NameDestWav) {
				g_free (IndiFix->NameDestWav);
				IndiFix->NameDestWav = NULL;
			}
			if (NULL != IndiFix->NameDestOgg) {
				g_free (IndiFix->NameDestOgg);
				IndiFix->NameDestOgg = NULL;
			}
			if (NULL != IndiFix->NameDestMp3) {
				g_free (IndiFix->NameDestMp3);
				IndiFix->NameDestMp3 = NULL;
			}

			if (NULL != IndiFix->StrDBFS) {
				g_free (IndiFix->StrDBFS);
				IndiFix->StrDBFS = NULL;
			}
			IndiFix->tags = NULL;

			IndiFix->EtatNormalise = NULL;
			
			g_free (IndiFix);
			IndiFix = List->data = NULL;
		}
		List = g_list_next(List);
	}
	g_list_free (p_list);
	p_list = NULL;
	return ((GList *)NULL);
}
// 
// 
/*
void normalise_print_DEBUG (GList *p_list)
{
	GList     *List = NULL;
	INDI_FIX  *IndiFix = NULL;

	List = g_list_first (p_list);
	while (List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
			g_print ("IndiFix->NameFile      = %s\n", IndiFix->NameFile);
			g_print ("IndiFix->NameFileCopieWav = %s\n", IndiFix->NameFileCopieWav);
		}
		List = g_list_next(List);
	}
}
*/
// 
// 
void normalise_copy_wav_to_tmp (GList *p_list)
{
	GList         *List = NULL;
	INDI_FIX      *IndiFix = NULL;
	
	List = g_list_first (p_list);
	while (List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
			conv_copy_src_to_dest (IndiFix->NameFile, IndiFix->NameFileCopieWav);
			normalise.ElementActif ++;
		}
		List = g_list_next(List);
	}
}
// 
// 
void normalise_set_mess (gchar *p_Mess)
{
	if (FALSE == normalise.bool_etat) {
		// normalise.bool_etat = TRUE;
		// normalise.MessUser = g_strdup (p_Mess);
	}
}
// 
// 
void normalise_FIX_wav (void)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;
	gint		pos;
	gchar		**PtrTabArgs = NULL;

	List = g_list_first (normalise.ListFixWav);
	while (FALSE == normalise.bool_set_end_of_user && List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {

			// Copie temporaire
			normalise.ElementActif ++;

			normalise_set_mess ("Copie");
			conv_copy_src_to_dest (IndiFix->NameFile, IndiFix->NameFileCopieWav);

			// Normalisation du Wav
			// 
			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
			PtrTabArgs [ pos++ ] = g_strdup ("-a");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("%s,0000dB", IndiFix->StrDBFS);
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = NULL;

			normalise_set_mess ("Normalise Wav -> Fix");

			conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> Fix");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			normalise.ElementActif ++;

			/* Restitution
			*/
			/*
			NameDestWav *
			NameDestOgg
			NameDestMp3
			*/
			if (FALSE == IndiFix->BoolWithConvert) {

				normalise_set_mess ("Restitution");

				conv_copy_src_to_dest (IndiFix->NameFileCopieWav, IndiFix->NameDestWav);
				g_unlink (IndiFix->NameFileCopieWav);
				
				normalise.ElementActif ++;
			}

			/* *IndiFix->EtatNormalise = NORM_SCAN_ACTIF;*/
			if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
		}
		List = g_list_next(List);
	}
}
// 
// 
void normalise_FIX_ogg (void)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;
	PARAM_FILELC	 param_filelc;
	gint		 pos;
	gchar		**PtrTabArgs = NULL;

	List = g_list_first (normalise.ListFixOgg);
	while (FALSE == normalise.bool_set_end_of_user && List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {

			/* Conversion Ogg vers Wav
			*/

			normalise_set_mess ("Conversion Ogg vers Wav");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
			PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
			PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
			PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
			PtrTabArgs [ pos++ ] = g_strdup ("-ao");
			PtrTabArgs [ pos++ ] = g_strdup ("pcm");
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFile);
			PtrTabArgs [ pos++ ] = g_strdup ("-ao");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = NULL;

			conv_to_convert( PtrTabArgs, FALSE, MPLAYER_OGG_TO_WAV, "MPLAYER_OGG_TO_WAV");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			normalise.ElementActif ++;

			/* Normalisation du Wav
			*/

			normalise_set_mess ("Normalise Wav -> Fix");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
			PtrTabArgs [ pos++ ] = g_strdup ("-a");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("%s,0000dB", IndiFix->StrDBFS);
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = NULL;
			
			conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC NORMALISATION du Wav");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			normalise.ElementActif ++;

			/* Restitution: Conversion Wav vers Ogg
			*/
			/*
			NameDestWav
			NameDestOgg *
			NameDestMp3
			*/
			if (IndiFix->BoolWithConvert == FALSE) {

				normalise_set_mess ("Conversion Wav vers Ogg");

				param_filelc.type_conv             = OGGENC_WAV_TO_OGG;
				param_filelc.With_CommandLineUser  = FALSE;
				param_filelc.filesrc               = IndiFix->NameFileCopieWav;
				param_filelc.filedest              = IndiFix->NameDestOgg;
				param_filelc.tags                  = IndiFix->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.PtrStrBitrate         = NULL;

				PtrTabArgs = filelc_get_command_line (&param_filelc);
				conv_to_convert( PtrTabArgs, FALSE, OGGENC_WAV_TO_OGG, "OGGENC_WAV_TO_OGG");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

				g_unlink (IndiFix->NameFileCopieWav);
								
				normalise.ElementActif ++;
			}

			if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
		}
		List = g_list_next(List);
	}
}
// 
// 
void normalise_FIX_mp3 (void)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;
	PARAM_FILELC	param_filelc;
	gint		pos;
	gchar		**PtrTabArgs = NULL;

	List = g_list_first (normalise.ListFixMp3);
	while (FALSE == normalise.bool_set_end_of_user && List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {

			// CONVERSION MP3 to WAV

			normalise_set_mess ("Conversion Mp3 vers Wav");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
			PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
			PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFile);
			PtrTabArgs [ pos++ ] = g_strdup ("-ao");
			PtrTabArgs [ pos++ ] = g_strdup ("pcm");
			PtrTabArgs [ pos++ ] = g_strdup ("-ao");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s",IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = g_strdup ("-af");
			PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
			PtrTabArgs [ pos++ ] = g_strdup ("-srate");
			PtrTabArgs [ pos++ ] = g_strdup ("44100");
			PtrTabArgs [ pos++ ] = NULL;
			
			conv_to_convert( PtrTabArgs, FALSE, MPLAYER_MP3_TO_WAV, "MPLAYER_MP3_TO_WAV");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			normalise.ElementActif ++;

			/* Normalisation du Wav
			*/

			normalise_set_mess ("Normalise Wav -> Fix");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
			PtrTabArgs [ pos++ ] = g_strdup ("-a");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("%s,0000dB", IndiFix->StrDBFS);
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = NULL;
			
			conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC NORMALISATION du Wav");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			normalise.ElementActif ++;

			/* Restitution: Conversion Wav vers Mp3
			*/
			/*
			NameDestWav
			NameDestOgg
			NameDestMp3 *
			*/
			if (IndiFix->BoolWithConvert == FALSE) {

				normalise_set_mess ("Conversion du Wav vers Mp3");

				param_filelc.type_conv            = LAME_WAV_TO_MP3;
				param_filelc.With_CommandLineUser = FALSE;
				param_filelc.filesrc              = IndiFix->NameFileCopieWav;
				param_filelc.filedest             = IndiFix->NameDestMp3;
				param_filelc.tags                 = IndiFix->tags;
				param_filelc.cdrom                = NULL;
				param_filelc.num_track            = NULL;
				param_filelc.PtrStrBitrate        = NULL;

				PtrTabArgs = filelc_get_command_line (&param_filelc);
				conv_to_convert( PtrTabArgs, FALSE, LAME_WAV_TO_MP3, "LAME_WAV_TO_MP3");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
				g_unlink (IndiFix->NameFileCopieWav);
				
				normalise.ElementActif ++;
			}

			/* *IndiFix->EtatNormalise = NORM_SCAN_ACTIF; */
			if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
		}
		List = g_list_next(List);
	}
}
// 
// 
void normalise_PEAK_wav (void)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;
	gint		pos;
	gchar		**PtrTabArgs = NULL;
	
	List = g_list_first (normalise.ListPeakWav);
	while (FALSE == normalise.bool_set_end_of_user && List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {

			/* Copie temporaire
			*/

			normalise_set_mess ("Copie");

			conv_copy_src_to_dest (IndiFix->NameFile, IndiFix->NameFileCopieWav);
			
			normalise.ElementActif ++;

			/* Normalisation du Wav
			*/

			normalise_set_mess ("Normalise Wav -> Peak");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
			PtrTabArgs [ pos++ ] = g_strdup ("--peak");
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = g_strdup ("--");
			PtrTabArgs [ pos++ ] = NULL;

			if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;

			conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> Peak");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			normalise.ElementActif ++;

			/* Restitution
			*/
			/*
			NameDestWav *
			NameDestOgg
			NameDestMp3
			*/
			if (FALSE == IndiFix->BoolWithConvert) {

				normalise_set_mess ("Restitution");

				conv_copy_src_to_dest (IndiFix->NameFileCopieWav, IndiFix->NameDestWav);
				g_unlink (IndiFix->NameFileCopieWav);
								
				normalise.ElementActif ++;
			}

		}
		List = g_list_next(List);
	}
}
// 
// 
void normalise_PEAK_ogg (void)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;
	PARAM_FILELC	param_filelc;
	gint		pos;
	gchar		**PtrTabArgs = NULL;

	List = g_list_first (normalise.ListPeakOgg);
	while (FALSE == normalise.bool_set_end_of_user && List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {

			/* Conversion Ogg vers Wav
			*/
			normalise_set_mess ("Conversion Ogg vers Wav");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup ("ogg123");
			PtrTabArgs [ pos++ ] = g_strdup ("-d");
			PtrTabArgs [ pos++ ] = g_strdup ("wav");
			PtrTabArgs [ pos++ ] = g_strdup ("-f");
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFile);
			PtrTabArgs [ pos++ ] = NULL;

			conv_to_convert( PtrTabArgs, FALSE, OGG123_OGG_TO_WAV, "OGG123_OGG_TO_WAV CONVERSION Ogg vers Wav");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			normalise.ElementActif ++;
		
			/* Normalisation du Wav
			*/
			normalise_set_mess ("Normalise Wav -> Fix");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = g_strdup ("--peak");
			PtrTabArgs [ pos++ ] = NULL;
			
			if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;

			conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> Peak");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			normalise.ElementActif ++;
		
			/* Restitution: Conversion Wav vers Ogg
			*/
			/*
			NameDestWav
			NameDestOgg *
			NameDestMp3
			*/
			if (FALSE == IndiFix->BoolWithConvert) {

				normalise_set_mess ("Conversion Wav vers Ogg");

				param_filelc.type_conv             = OGGENC_WAV_TO_OGG;
				param_filelc.With_CommandLineUser  = FALSE;
				param_filelc.filesrc               = IndiFix->NameFileCopieWav;
				param_filelc.filedest              = IndiFix->NameDestOgg;
				param_filelc.tags                  = IndiFix->tags;
				param_filelc.cdrom                 = NULL;
				param_filelc.num_track             = NULL;
				param_filelc.PtrStrBitrate         = NULL;

				PtrTabArgs = filelc_get_command_line (&param_filelc);
				conv_to_convert( PtrTabArgs, FALSE, OGGENC_WAV_TO_OGG, "OGGENC_WAV_TO_OGG");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
				g_unlink (IndiFix->NameFileCopieWav);
				
				normalise.ElementActif ++;
			}

		}
		List = g_list_next(List);
	}
}
// 
// 
void normalise_PEAK_mp3 (void)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;
	PARAM_FILELC	param_filelc;
	gint		pos;
	gchar		**PtrTabArgs = NULL;

	List = g_list_first (normalise.ListPeakMp3);
	while (FALSE == normalise.bool_set_end_of_user && List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {

			/* Conversion Mp3 vers Wav
			*/
			normalise_set_mess ("Conversion Mp3 vers Wav");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
			PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
			PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFile);
			PtrTabArgs [ pos++ ] = g_strdup ("-ao");
			PtrTabArgs [ pos++ ] = g_strdup ("pcm");
			PtrTabArgs [ pos++ ] = g_strdup ("-ao");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s",IndiFix->NameFileCopieWav);
			PtrTabArgs [ pos++ ] = g_strdup ("-af");
			PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
			PtrTabArgs [ pos++ ] = g_strdup ("-srate");
			PtrTabArgs [ pos++ ] = g_strdup ("44100");
			PtrTabArgs [ pos++ ] = NULL;
			
			conv_to_convert( PtrTabArgs, FALSE, MPLAYER_MP3_TO_WAV, "MPLAYER_MP3_TO_WAV");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
			
			normalise.ElementActif ++;

			/* Normalisation du Wav
			*/
			normalise_set_mess ("Normalise Wav -> Peak");

			PtrTabArgs = filelc_AllocTabArgs();
			pos = 3;
			PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
			PtrTabArgs [ pos++ ] = g_strdup ("--peak");
			PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieMp3);
			PtrTabArgs [ pos++ ] = NULL;
			
			conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> Peak");
			PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

			normalise.ElementActif ++;

			/* Restitution: Conversion Wav vers Mp3
			*/
			/*
			NameDestWav
			NameDestOgg
			NameDestMp3 *
			*/
			if (FALSE == IndiFix->BoolWithConvert) {

				normalise_set_mess ("Conversion du Wav vers Mp3");

				param_filelc.type_conv            = LAME_WAV_TO_MP3;
				param_filelc.With_CommandLineUser = FALSE;
				param_filelc.filesrc              = IndiFix->NameFileCopieWav;
				param_filelc.filedest             = IndiFix->NameDestMp3;
				param_filelc.tags                 = IndiFix->tags;
				param_filelc.cdrom                = NULL;
				param_filelc.num_track            = NULL;
				param_filelc.PtrStrBitrate        = NULL;

				PtrTabArgs = filelc_get_command_line (&param_filelc);
				conv_to_convert( PtrTabArgs, FALSE, LAME_WAV_TO_MP3, "LAME_WAV_TO_MP3");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
				g_unlink (IndiFix->NameFileCopieWav);
								
				normalise.ElementActif ++;
			}

			/* *IndiFix->EtatNormalise = NORM_SCAN_ACTIF; */
			if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
		}
		List = g_list_next(List);
	}
}
// 
// 
void normalise_MIX_RMS_GROUP_wav_ogg_mp3 (void)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;
	PARAM_FILELC	param_filelc;
	gint		pos;
	gchar		**PtrTabArgs = NULL;

	PRINT_FUNC_LF();

	/* COPIE  WAV OGG MP3
	*/
	if (FALSE == normalise.bool_set_end_of_user && NULL != normalise.ListMixGroupWav) {

		normalise_set_mess ("(MIX)RMS/GROUPE (Temp): Copie 'wav' vers dossier temporaire");
		
		conv.bool_percent_conv = TRUE;
		normalise_copy_wav_to_tmp (normalise.ListMixGroupWav);
	}
	/* Conversion Ogg vers Wav */
	if (FALSE == normalise.bool_set_end_of_user && NULL != normalise.ListMixGroupOgg) {

		normalise_set_mess ("(MIX)RMS/GROUPE (Temp): Conversion Ogg vers Wav");
		
		List = g_list_first (normalise.ListMixGroupOgg);
		while (List) {
			if ((IndiFix = (INDI_FIX *)List->data)) {
				
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("ogg123");
				PtrTabArgs [ pos++ ] = g_strdup ("-d");
				PtrTabArgs [ pos++ ] = g_strdup ("wav");
				PtrTabArgs [ pos++ ] = g_strdup ("-f");
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFile);
				PtrTabArgs [ pos++ ] = NULL;

				conv_to_convert( PtrTabArgs, FALSE, OGG123_OGG_TO_WAV, "OGG123_OGG_TO_WAV CONVERSION Ogg vers Wav");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

				normalise.ElementActif ++;
			}
			List = g_list_next(List);
		}
	}
	/* Conversion Mp3 vers Wav */
	if (FALSE == normalise.bool_set_end_of_user && NULL != normalise.ListMixGroupMp3) {

		normalise_set_mess ("(MIX)RMS/GROUPE (Temp): Conversion Mp3 vers Wav");

		List = g_list_first (normalise.ListMixGroupMp3);
		while (List) {
			if ((IndiFix = (INDI_FIX *)List->data)) {
				
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
				PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
				PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFile);
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup ("pcm");
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s",IndiFix->NameFileCopieWav);
				PtrTabArgs [ pos++ ] = g_strdup ("-af");
				PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
				PtrTabArgs [ pos++ ] = g_strdup ("-srate");
				PtrTabArgs [ pos++ ] = g_strdup ("44100");
				PtrTabArgs [ pos++ ] = NULL;

				conv_to_convert( PtrTabArgs, FALSE, MPLAYER_MP3_TO_WAV, "MPLAYER_MP3_TO_WAV");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

				normalise.ElementActif ++;
			}
			List = g_list_next(List);
		}
	}
	
	/* NORMALISE  WAV
	*/
	if (FALSE == normalise.bool_set_end_of_user) {

		normalise_set_mess ("(MIX)RMS/GROUPE: Normalise Wav");
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
		PtrTabArgs [ pos++ ] = g_strdup ("-a");
		PtrTabArgs [ pos++ ] = g_strdup_printf ("%d,0000dB", normalise.LevelMix);
		PtrTabArgs [ pos++ ] = g_strdup ("--batch");
		/* Creation liste WAV */
		List = g_list_first (normalise.ListMixGroupWav);
		while (List) {
			if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
				if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
			}
			List = g_list_next(List);
		}
		/* Creation liste OGG */
		List = g_list_first (normalise.ListMixGroupOgg);
		while (List) {
			if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
				if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
			}
			List = g_list_next(List);
		}
		/* Creation liste MP3 */
		List = g_list_first (normalise.ListMixGroupMp3);
		while (List) {
			if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
				if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
			}
			List = g_list_next(List);
		}
		PtrTabArgs [ pos++ ] = NULL;
		
		/* Normalisation */
		conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> (MIX)RMS/GROUPE");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

		normalise.ElementActif ++;
	}
	
	/* RESTITUE  WAV OGG MP3
	*/
	if (FALSE == normalise.bool_set_end_of_user) {
		/* Restitue liste WAV */
		if (FALSE == IndiFix->BoolWithConvert) {

			normalise_set_mess ("(MIX)RMS/GROUPE (Restitution): Restitution des fichiers 'wav'");
			
			List = g_list_first (normalise.ListMixGroupWav);
			while (List) {
				if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
					conv_copy_src_to_dest (IndiFix->NameFileCopieWav, IndiFix->NameDestWav);
					g_unlink (IndiFix->NameFileCopieWav);
					normalise.ElementActif ++;
					conv.bool_percent_conv = TRUE;
				}
				List = g_list_next(List);
			}
		}
		/* Restitue liste OGG */
		if (FALSE == IndiFix->BoolWithConvert) {

			normalise_set_mess ("(MIX)RMS/GROUPE (Restitution): Conversion Wav vers Ogg");
			
			List = g_list_first (normalise.ListMixGroupOgg);
			while (List) {
				if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
					param_filelc.type_conv             = OGGENC_WAV_TO_OGG;
					param_filelc.With_CommandLineUser  = FALSE;
					param_filelc.filesrc               = IndiFix->NameFileCopieWav;
					param_filelc.filedest              = IndiFix->NameDestOgg;
					param_filelc.tags                  = IndiFix->tags;
					param_filelc.cdrom                 = NULL;
					param_filelc.num_track             = NULL;
					param_filelc.PtrStrBitrate         = NULL;

					PtrTabArgs = filelc_get_command_line (&param_filelc);
					conv_to_convert( PtrTabArgs, FALSE, OGGENC_WAV_TO_OGG, "OGGENC_WAV_TO_OGG");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
					g_unlink (IndiFix->NameFileCopieWav);

					normalise.ElementActif ++;
				}
				List = g_list_next(List);
			}
		}
		/* Restitue liste MP3 */
		if (FALSE == IndiFix->BoolWithConvert) {

			normalise_set_mess ("(MIX)RMS/GROUPE (Restitution): Conversion Wav vers Mp3");
			
			List = g_list_first (normalise.ListMixGroupMp3);
			while (List) {
				if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
					param_filelc.type_conv            = LAME_WAV_TO_MP3;
					param_filelc.With_CommandLineUser = FALSE;
					param_filelc.filesrc              = IndiFix->NameFileCopieWav;
					param_filelc.filedest             = IndiFix->NameDestMp3;
					param_filelc.tags                 = IndiFix->tags;
					param_filelc.cdrom                = NULL;
					param_filelc.num_track            = NULL;
					param_filelc.PtrStrBitrate        = NULL;

					PtrTabArgs = filelc_get_command_line (&param_filelc);
					conv_to_convert( PtrTabArgs,FALSE, LAME_WAV_TO_MP3, "LAME_WAV_TO_MP3");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					g_unlink (IndiFix->NameFileCopieWav);

					normalise.ElementActif ++;
				}
				List = g_list_next(List);
			}
		}
	}
}
// 
// 
void normalise_PEAK_RMS_GROUP_wav_ogg_mp3 (void)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;
	PARAM_FILELC	param_filelc;
	gint		pos;
	gchar		**PtrTabArgs = NULL;
	
	/* COPIE  WAV OGG MP3
	*/
	if (FALSE == normalise.bool_set_end_of_user && NULL != normalise.ListPeakGroupWav) {

		normalise_set_mess ("PEAK/GROUPE (Temp): Copie 'wav' vers dossier temporaire");
		
		normalise_copy_wav_to_tmp (normalise.ListPeakGroupWav);
	}
	/* Conversion Ogg vers Wav */
	if (FALSE == normalise.bool_set_end_of_user && NULL != normalise.ListPeakGroupOgg) {

		normalise_set_mess ("PEAK/GROUPE (Temp): Conversion Ogg vers Wav");
		
		List = g_list_first (normalise.ListPeakGroupOgg);
		while (List) {
			if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
				
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("ogg123");
				PtrTabArgs [ pos++ ] = g_strdup ("-d");
				PtrTabArgs [ pos++ ] = g_strdup ("wav");
				PtrTabArgs [ pos++ ] = g_strdup ("-f");
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFile);
				PtrTabArgs [ pos++ ] = NULL;

				conv_to_convert( PtrTabArgs, FALSE, OGG123_OGG_TO_WAV, "OGG123_OGG_TO_WAV CONVERSION Ogg vers Wav");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

				normalise.ElementActif ++;
			}
			List = g_list_next(List);
		}
	}
	/* Conversion Mp3 vers Wav */
	if (FALSE == normalise.bool_set_end_of_user && NULL != normalise.ListPeakGroupMp3) {

		normalise_set_mess ("PEAK/GROUPE (Temp): Conversion Mp3 vers Wav");
		
		List = g_list_first (normalise.ListPeakGroupMp3);
		while (List) {
			if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
				
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
				PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
				PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFile);
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup ("pcm");
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s",IndiFix->NameFileCopieWav);
				PtrTabArgs [ pos++ ] = g_strdup ("-af");
				PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
				PtrTabArgs [ pos++ ] = g_strdup ("-srate");
				PtrTabArgs [ pos++ ] = g_strdup ("44100");
				PtrTabArgs [ pos++ ] = NULL;

				conv_to_convert( PtrTabArgs, FALSE, MPLAYER_MP3_TO_WAV, "MPLAYER_MP3_TO_WAV");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
				normalise.ElementActif ++;
			}
			List = g_list_next(List);
		}
	}
	
	/* NORMALISE  WAV
	*/
	if (FALSE == normalise.bool_set_end_of_user) {
		
		normalise_set_mess ("PEAK/GROUPE: Chercher la moyenne Peak");
		
		/* CHERCHER LA MOYENNE PEAK */
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
		PtrTabArgs [ pos++ ] = g_strdup ("-n");

		/* Creation liste WAV */
		List = g_list_first (normalise.ListPeakGroupWav);
		while (List) {
			if ((IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			}
			List = g_list_next(List);
		}
		/* Creation liste OGG */
		List = g_list_first (normalise.ListPeakGroupOgg);
		while (List) {
			if ((IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			}
			List = g_list_next(List);
		}
		/* Creation liste MP3 */
		List = g_list_first (normalise.ListPeakGroupMp3);
		while (List) {
			if ((IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
			}
			List = g_list_next(List);
		}
		PtrTabArgs [ pos++ ] = NULL;
		
		/* Chercher la moyenne PEAK */
		conv_to_convert( PtrTabArgs, FALSE, NORMALISE_GET_LEVEL, "NORMALISE_GET_LEVEL -> PEAK/GROUP");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
		
		/* APPLIQUER LA MOYENNE PEAK */

		normalise_set_mess ("PEAK/GROUPE: Normalise Wav");
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup (prginit_get_name (NMR_normalize));
		PtrTabArgs [ pos++ ] = g_strdup_printf ("--gain=%fdB", conv.value_PEAK_RMS_GROUP_ARGS);
		
		/* Creation liste WAV */
		List = g_list_first (normalise.ListPeakGroupWav);
		while (List) {
			if ((IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
				if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
			}
			List = g_list_next(List);
		}
		/* Creation liste OGG */
		List = g_list_first (normalise.ListPeakGroupOgg);
		while (List) {
			if ((IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
				if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
			}
			List = g_list_next(List);
		}
		/* Creation liste MP3 */
		List = g_list_first (normalise.ListPeakGroupMp3);
		while (List) {
			if ((IndiFix = (INDI_FIX *)List->data)) {
				PtrTabArgs [ pos++ ] = g_strdup (IndiFix->NameFileCopieWav);
				if (*IndiFix->EtatTrash == FILE_TRASH_OK) *IndiFix->EtatTrash = FILE_TRASH_VERIF_OK;
			}
			List = g_list_next(List);
		}
		PtrTabArgs [ pos++ ] = NULL;
		
		/* Normalisation */
		conv_to_convert( PtrTabArgs, FALSE, NORMALISE_EXEC, "NORMALISE_EXEC Wav -> PEAK/GROUP");
		PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

		normalise.ElementActif ++;
	}
	
	/* RESTITUE  WAV OGG MP3
	*/
	if (FALSE == normalise.bool_set_end_of_user) {
		/* Restitue liste WAV */
		if (FALSE == IndiFix->BoolWithConvert) {

			normalise_set_mess ("PEAK/GROUPE (Restitution): Restitution des fichiers 'wav'");
			
			List = g_list_first (normalise.ListPeakGroupWav);
			while (List) {
				if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
					/* Bug de copie signalee par @Dzef
					*/
					if (TRUE == IndiFix->BoolWithConvert) {
						conv_copy_src_to_dest (IndiFix->NameFileCopieWav, IndiFix->NameDestWav);
					}
					g_unlink (IndiFix->NameFileCopieWav);
					normalise.ElementActif ++;
				}
				List = g_list_next(List);
			}
		}
		/* Restitue liste OGG */
		if (FALSE == IndiFix->BoolWithConvert) {

			normalise_set_mess ("PEAK/GROUPE (Restitution): Conversion Wav vers Ogg");

			List = g_list_first (normalise.ListPeakGroupOgg);
			while (List) {
				if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
					param_filelc.type_conv             = OGGENC_WAV_TO_OGG;
					param_filelc.With_CommandLineUser  = FALSE;
					param_filelc.filesrc               = IndiFix->NameFileCopieWav;
					param_filelc.filedest              = IndiFix->NameDestOgg;
					param_filelc.tags                  = IndiFix->tags;
					param_filelc.cdrom                 = NULL;
					param_filelc.num_track             = NULL;
					param_filelc.PtrStrBitrate         = NULL;

					PtrTabArgs = filelc_get_command_line (&param_filelc);
					conv_to_convert( PtrTabArgs, FALSE, OGGENC_WAV_TO_OGG, "OGGENC_WAV_TO_OGG");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					g_unlink (IndiFix->NameFileCopieWav);

					normalise.ElementActif ++;
				}
				List = g_list_next(List);
			}
		}
		/* Restitue liste MP3 */
		if (FALSE == IndiFix->BoolWithConvert) {

			normalise_set_mess ("PEAK/GROUPE (Restitution): Conversion Wav vers Mp3");

			List = g_list_first (normalise.ListPeakGroupMp3);
			while (List) {
				if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
					param_filelc.type_conv            = LAME_WAV_TO_MP3;
					param_filelc.With_CommandLineUser = FALSE;
					param_filelc.filesrc              = IndiFix->NameFileCopieWav;
					param_filelc.filedest             = IndiFix->NameDestMp3;
					param_filelc.tags                 = IndiFix->tags;
					param_filelc.cdrom                = NULL;
					param_filelc.num_track            = NULL;
					param_filelc.PtrStrBitrate        = NULL;

					PtrTabArgs = filelc_get_command_line (&param_filelc);
					conv_to_convert( PtrTabArgs, FALSE, LAME_WAV_TO_MP3, "LAME_WAV_TO_MP3");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

					g_unlink (IndiFix->NameFileCopieWav);

					normalise.ElementActif ++;
				}
				List = g_list_next(List);
			}
		}
	}
}
// 
// 
void normalise_action_from_thread (void)
{
	// PRINT_FUNC_LF();

	normalise.bool_pass_conv = TRUE;
	
	if (NULL != normalise.ListFixWav) normalise_FIX_wav ();
	if (NULL != normalise.ListFixOgg) normalise_FIX_ogg ();
	if (NULL != normalise.ListFixMp3) normalise_FIX_mp3 ();
	
	if (NULL != normalise.ListPeakWav) normalise_PEAK_wav ();
	if (NULL != normalise.ListPeakOgg) normalise_PEAK_ogg ();
	if (NULL != normalise.ListPeakMp3) normalise_PEAK_mp3 ();
	
	if (NULL != normalise.ListMixGroupWav ||
	    NULL != normalise.ListMixGroupOgg ||
	    NULL != normalise.ListMixGroupMp3) normalise_MIX_RMS_GROUP_wav_ogg_mp3 ();
	
	if (NULL != normalise.ListPeakGroupWav ||
	    NULL != normalise.ListPeakGroupOgg ||
	    NULL != normalise.ListPeakGroupMp3) normalise_PEAK_RMS_GROUP_wav_ogg_mp3 ();
	
	normalise.bool_pass_conv = FALSE;
}
// 
// 
gboolean normalise_is_individuel (void)
{
	GList            *List = NULL;
	DETAIL           *detail = NULL;

	List = g_list_first (entetefile);
	while (List) {
		if (NULL != (detail = (DETAIL *)List->data)) {
			if (detail->Etat_Normalise == NORM_RMS_FIX) {

				gint Level = 0, NewLevel = 0;

				if (detail->type_infosong_file_is == FILE_IS_WAV) {
					INFO_WAV *info = (INFO_WAV *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				else if (detail->type_infosong_file_is == FILE_IS_OGG) {
					INFO_OGG *info = (INFO_OGG *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				else if (detail->type_infosong_file_is == FILE_IS_MP3) {
					INFO_MP3 *info = (INFO_MP3 *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				if (Level != NewLevel) return (TRUE);
			}
			else if (detail->Etat_Normalise > NORM_READY_FOR_SELECT) {
				return (TRUE);
			}
		}
		List = g_list_next(List);
	}
	return (FALSE);
}
// 
// 
gboolean normalise_acces (DETAIL *detail)
{
	if (NULL == detail) return (FALSE);

	if (detail->Etat_Normalise == NORM_RMS_FIX) {

		gint Level = 0, NewLevel = 0;

		if (detail->type_infosong_file_is == FILE_IS_WAV) {
			INFO_WAV *info = (INFO_WAV *)detail->info;
			Level    = info->LevelDbfs.level;
			NewLevel = info->LevelDbfs.NewLevel;
		}
		else if (detail->type_infosong_file_is == FILE_IS_OGG) {
			INFO_OGG *info = (INFO_OGG *)detail->info;
			Level    = info->LevelDbfs.level;
			NewLevel = info->LevelDbfs.NewLevel;
		}
		else if (detail->type_infosong_file_is == FILE_IS_MP3) {
			INFO_MP3 *info = (INFO_MP3 *)detail->info;
			Level    = info->LevelDbfs.level;
			NewLevel = info->LevelDbfs.NewLevel;
		}
		if (Level != NewLevel) return (TRUE);
	}
	else if (detail->Etat_Normalise > NORM_READY_FOR_SELECT) {
		return (TRUE);
	}
	return (FALSE);
}
// 
// 
void normalise_get_NameFileCopie (INDI_FIX *p_IndiFix, gint pos)
{
	// Create tmp rep
	if( NULL == conv.TmpRep )
		conv.TmpRep  = libutils_create_temporary_rep (Config.PathnameTMP, PATH_TMP_XCFA_AUDIOFILE);

	p_IndiFix->NameFileCopieWav = g_strdup_printf ("%s/%d.wav", conv.TmpRep, pos);
	p_IndiFix->NameFileCopieOgg = g_strdup_printf ("%s/%d.ogg", conv.TmpRep, pos);
	p_IndiFix->NameFileCopieMp3 = g_strdup_printf ("%s/%d.mp3", conv.TmpRep, pos);
}
// 
// 
gboolean normalise_with_convert (DETAIL *detail)
{
	if (detail->type_infosong_file_is == FILE_IS_WAV ||
	    detail->type_infosong_file_is == FILE_IS_OGG ||
	    detail->type_infosong_file_is == FILE_IS_MP3) {

		if (detail->EtatSelection_Wav > ETAT_ATTENTE_EXIST ||
		    detail->EtatSelection_Mp3 > ETAT_ATTENTE_EXIST ||
		    detail->EtatSelection_Ogg > ETAT_ATTENTE_EXIST) {

			if (detail->Etat_Normalise == NORM_RMS_FIX) {

				gint Level = 0, NewLevel = 0;

				if (detail->type_infosong_file_is == FILE_IS_WAV) {
					INFO_WAV *info = (INFO_WAV *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				else if (detail->type_infosong_file_is == FILE_IS_OGG) {
					INFO_OGG *info = (INFO_OGG *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				else if (detail->type_infosong_file_is == FILE_IS_MP3) {
					INFO_MP3 *info = (INFO_MP3 *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				if (Level != NewLevel) {
					return (TRUE);
				}
			}
			else if (detail->Etat_Normalise > NORM_RMS_FIX) {
				return (TRUE);
			}
		}
	}

	return (FALSE);
}
// 
// 
void normalise_force_scan (gchar *p_Name)
{
	GList		*List = NULL;
	DETAIL		*detail = NULL;
	
	List = g_list_first (entetefile);
	while (List) {
		if (NULL != (detail = (DETAIL *)List->data)) {
			if (detail->type_infosong_file_is == FILE_IS_WAV ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_MP3) {
			    	
			    	if (0 == strcmp (detail->namefile, p_Name)) {
			    	
			    		if (detail->Etat_Normalise == NORM_RMS_MIX_ALBUM || detail->Etat_Normalise == NORM_RMS_FIX) {
						detail->Etat_Scan = ETAT_SCAN_NONE;
						break;
					}
				}
			}
		}
		List = g_list_next(List);
	}
}
// 
// 
void normalise_set_verif (GList *p_List)
{
	GList		*List = NULL;
	INDI_FIX	*IndiFix = NULL;

	List = g_list_first (p_List);
	while (List) {
		if (NULL != (IndiFix = (INDI_FIX *)List->data)) {
			normalise_force_scan (IndiFix->NameFile);
			normalise_force_scan (IndiFix->NameDestWav);
			normalise_force_scan (IndiFix->NameDestOgg);
			normalise_force_scan (IndiFix->NameDestMp3);
		}
		List = g_list_next(List);
	}
}
// 
// 
static void normalise_struct_lists (void *arg)
{
	GList            *List = NULL;
	DETAIL           *detail = NULL;
	INDI_FIX         *IndiFix = NULL;

	// PRINT_FUNC_LF();

	normalise.bool_thread_end = FALSE;
	normalise.bool_set_end_of_user = FALSE;
	
	/* Si liste individuel */
	if (TRUE == normalise_is_individuel ()) {

		gboolean bool_mix_wav = FALSE;
		gboolean bool_mix_ogg = FALSE;
		gboolean bool_mix_mp3 = FALSE;
		gboolean bool_peak_wav = FALSE;
		gboolean bool_peak_ogg = FALSE;
		gboolean bool_peak_mp3 = FALSE;

		/* FIX */
		normalise.ListFixWav		= normalise_remove_glist_STRUCT (normalise.ListFixWav);
		normalise.ListFixOgg		= normalise_remove_glist_STRUCT (normalise.ListFixOgg);
		normalise.ListFixMp3		= normalise_remove_glist_STRUCT (normalise.ListFixMp3);
		/* PEAK */
		normalise.ListPeakWav		= normalise_remove_glist_STRUCT (normalise.ListPeakWav);
		normalise.ListPeakOgg		= normalise_remove_glist_STRUCT (normalise.ListPeakOgg);
		normalise.ListPeakMp3		= normalise_remove_glist_STRUCT (normalise.ListPeakMp3);
		/* MIX */
		normalise.ListMixGroupWav	= normalise_remove_glist_STRUCT (normalise.ListMixGroupWav);
		normalise.ListMixGroupOgg	= normalise_remove_glist_STRUCT (normalise.ListMixGroupOgg);
		normalise.ListMixGroupMp3	= normalise_remove_glist_STRUCT (normalise.ListMixGroupMp3);
		/* PEAK-GROUP */
		normalise.ListPeakGroupWav	= normalise_remove_glist_STRUCT (normalise.ListPeakGroupWav);
		normalise.ListPeakGroupOgg	= normalise_remove_glist_STRUCT (normalise.ListPeakGroupOgg);
		normalise.ListPeakGroupMp3	= normalise_remove_glist_STRUCT (normalise.ListPeakGroupMp3);

		List = g_list_first (entetefile);
		while (List) {
			if (NULL != (detail = (DETAIL *)List->data)) {
				/*
				bool_mix_wav = FALSE;
				bool_mix_ogg = FALSE;
				bool_mix_mp3 = FALSE;
				bool_peak_wav = FALSE;
				bool_peak_ogg = FALSE;
				bool_peak_mp3 = FALSE;

				if (normalise_with_convert (detail) == FALSE) {
					List = g_list_next(List);
					continue;
				}
				*/

				/* FILE_IS_WAV */
				if (detail->type_infosong_file_is == FILE_IS_WAV && normalise_acces (detail)) {

					INFO_WAV *info = (INFO_WAV *)detail->info;
					if (detail->Etat_Normalise == NORM_RMS_FIX ||
					    detail->Etat_Normalise == NORM_PEAK ||
					    detail->Etat_Normalise == NORM_RMS_MIX_ALBUM ||
					    detail->Etat_Normalise == NORM_PEAK_ALBUM) {

						info->LevelDbfs.level = detail->LevelMix;
						
						IndiFix = (INDI_FIX *)g_malloc0 (sizeof (INDI_FIX));
						IndiFix->NameFile      = g_strdup (detail->namefile);
						IndiFix->StrDBFS       = g_strdup_printf ("%d", info->LevelDbfs.NewLevel);
						IndiFix->tags          = info->tags;
						
						IndiFix->EtatNormalise = &detail->Etat_Normalise;
						
						IndiFix->EtatTrash     = &detail->EtatTrash;
						
						normalise_get_NameFileCopie (IndiFix, g_list_position (entetefile, List));
						// GList *g_list_nth (GList *list, guint n);
									
						if (TRUE == (IndiFix->BoolWithConvert = normalise_with_convert (detail))) {
							detail->NameFileCopyFromNormalizate = g_strdup (IndiFix->NameFileCopieWav);
						}

						IndiFix->NameDestWav = file_get_pathname_dest (detail, "wav");
						IndiFix->NameDestOgg = file_get_pathname_dest (detail, "ogg");
						IndiFix->NameDestMp3 = file_get_pathname_dest (detail, "mp3");
					}

					if (detail->Etat_Normalise == NORM_RMS_FIX) {
						normalise.ListFixWav = g_list_append (normalise.ListFixWav, IndiFix);
						/* actions: 3 */
						normalise.NbrElementsInList += 2;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						
						/* detail->Etat_Scan = ETAT_SCAN_DEMANDE;*/
					}
					else if (detail->Etat_Normalise == NORM_PEAK) {
						normalise.ListPeakWav = g_list_append (normalise.ListPeakWav, IndiFix);
						/* actions: 3 */
						normalise.NbrElementsInList += 2;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
					}
					else if (detail->Etat_Normalise == NORM_RMS_MIX_ALBUM) {
						normalise.ListMixGroupWav = g_list_append (normalise.ListMixGroupWav, IndiFix);
						/*-*/
						normalise.NbrElementsInList ++;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						bool_mix_wav = TRUE;
						normalise.LevelMix = detail->LevelMix;
						
						/* detail->Etat_Scan = ETAT_SCAN_DEMANDE;*/
					}
					else if (detail->Etat_Normalise == NORM_PEAK_ALBUM) {
						normalise.ListPeakGroupWav = g_list_append (normalise.ListPeakGroupWav, IndiFix);
						/*-*/
						normalise.NbrElementsInList ++;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						bool_peak_wav = TRUE;
					}
				}
				/* FILE_IS_OGG */
				else if (detail->type_infosong_file_is == FILE_IS_OGG && normalise_acces (detail)) {

					INFO_OGG *info = (INFO_OGG *)detail->info;

					if (detail->Etat_Normalise == NORM_RMS_FIX ||
					    detail->Etat_Normalise == NORM_PEAK ||
					    detail->Etat_Normalise == NORM_RMS_MIX_ALBUM ||
					    detail->Etat_Normalise == NORM_PEAK_ALBUM) {

						info->LevelDbfs.level = detail->LevelMix;
						
						IndiFix = (INDI_FIX *)g_malloc0 (sizeof (INDI_FIX));
						IndiFix->NameFile      = g_strdup (detail->namefile);
						IndiFix->StrDBFS       = g_strdup_printf ("%d", info->LevelDbfs.NewLevel);
						IndiFix->tags          = info->tags;
						
						IndiFix->EtatNormalise = &detail->Etat_Normalise;
						
						IndiFix->EtatTrash     = &detail->EtatTrash;
						
						normalise_get_NameFileCopie (IndiFix, g_list_position (entetefile, List));
						if ((IndiFix->BoolWithConvert = normalise_with_convert (detail)) == TRUE) {
							detail->NameFileCopyFromNormalizate = g_strdup (IndiFix->NameFileCopieWav);
						}

						IndiFix->NameDestWav = file_get_pathname_dest (detail, "wav");
						IndiFix->NameDestOgg = file_get_pathname_dest (detail, "ogg");
						IndiFix->NameDestMp3 = file_get_pathname_dest (detail, "mp3");
					}

					if (detail->Etat_Normalise == NORM_RMS_FIX) {
						normalise.ListFixOgg = g_list_append (normalise.ListFixOgg, IndiFix);
						/*-*/
						normalise.NbrElementsInList += 2;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						
						/* detail->Etat_Scan = ETAT_SCAN_DEMANDE;*/
						
					}
					else if (detail->Etat_Normalise == NORM_PEAK) {
						normalise.ListPeakOgg = g_list_append (normalise.ListPeakOgg, IndiFix);
						/*-*/
						normalise.NbrElementsInList += 2;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
					}
					else if (detail->Etat_Normalise == NORM_RMS_MIX_ALBUM) {
						normalise.ListMixGroupOgg = g_list_append (normalise.ListMixGroupOgg, IndiFix);
						normalise.NbrElementsInList ++;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						bool_mix_ogg = TRUE;
						normalise.LevelMix = detail->LevelMix;
						
						/* detail->Etat_Scan = ETAT_SCAN_DEMANDE;*/
					}
					else if (detail->Etat_Normalise == NORM_PEAK_ALBUM) {
						normalise.ListPeakGroupOgg = g_list_append (normalise.ListPeakGroupOgg, IndiFix);
						/*-*/
						normalise.NbrElementsInList ++;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						bool_peak_ogg = TRUE;
					}
				}
				/* FILE_IS_MP3 */
				else if (detail->type_infosong_file_is == FILE_IS_MP3 && normalise_acces (detail)) {

					INFO_MP3 *info = (INFO_MP3 *)detail->info;

					if (detail->Etat_Normalise == NORM_RMS_FIX ||
					    detail->Etat_Normalise == NORM_PEAK ||
					    detail->Etat_Normalise == NORM_RMS_MIX_ALBUM ||
					    detail->Etat_Normalise == NORM_PEAK_ALBUM) {

						info->LevelDbfs.level = detail->LevelMix;
						
						IndiFix = (INDI_FIX *)g_malloc0 (sizeof (INDI_FIX));
						IndiFix->NameFile      = g_strdup (detail->namefile);
						IndiFix->StrDBFS       = g_strdup_printf ("%d", info->LevelDbfs.NewLevel);
						IndiFix->tags          = info->tags;
						
						IndiFix->EtatNormalise = &detail->Etat_Normalise;
						
						IndiFix->EtatTrash     = &detail->EtatTrash;
						
						normalise_get_NameFileCopie (IndiFix, g_list_position (entetefile, List));
						if ((IndiFix->BoolWithConvert = normalise_with_convert (detail)) == TRUE) {
							detail->NameFileCopyFromNormalizate = g_strdup (IndiFix->NameFileCopieWav);
						}

						IndiFix->NameDestWav = file_get_pathname_dest (detail, "wav");
						IndiFix->NameDestOgg = file_get_pathname_dest (detail, "ogg");
						IndiFix->NameDestMp3 = file_get_pathname_dest (detail, "mp3");
					}

					if (detail->Etat_Normalise == NORM_RMS_FIX) {
						normalise.ListFixMp3 = g_list_append (normalise.ListFixMp3, IndiFix);
						/*-*/
						normalise.NbrElementsInList += 2;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						
						/* detail->Etat_Scan = ETAT_SCAN_DEMANDE;*/
					}
					else if (detail->Etat_Normalise == NORM_PEAK) {
						normalise.ListPeakMp3 = g_list_append (normalise.ListPeakMp3, IndiFix);
						/*-*/
						normalise.NbrElementsInList += 2;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
					}
					else if (detail->Etat_Normalise == NORM_RMS_MIX_ALBUM) {
						normalise.ListMixGroupMp3 = g_list_append (normalise.ListMixGroupMp3, IndiFix);
						normalise.NbrElementsInList ++;
						/*-*/
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						bool_mix_mp3 = TRUE;
						normalise.LevelMix = detail->LevelMix;
						
						/* detail->Etat_Scan = ETAT_SCAN_DEMANDE;*/
					}
					else if (detail->Etat_Normalise == NORM_PEAK_ALBUM) {
						normalise.ListPeakGroupMp3 = g_list_append (normalise.ListPeakGroupMp3, IndiFix);
						/*-*/
						normalise.NbrElementsInList ++;
						if (IndiFix->BoolWithConvert == FALSE) normalise.NbrElementsInList ++;
						bool_peak_mp3 = TRUE;
					}
				}
			}
			List = g_list_next(List);
		}
		if (TRUE == bool_mix_wav) normalise.NbrElementsInList ++;
		if (TRUE == bool_mix_ogg) normalise.NbrElementsInList ++;
		if (TRUE == bool_mix_mp3) normalise.NbrElementsInList ++;
		if (TRUE == bool_peak_wav) normalise.NbrElementsInList ++;
		if (TRUE == bool_peak_ogg) normalise.NbrElementsInList ++;
		if (TRUE == bool_peak_mp3) normalise.NbrElementsInList ++;
		/**/
		normalise_action_from_thread ();
	}

	/* POUR GARDER LA FENETRE OUVERTE
	*/
	normalise.bool_thread_end = TRUE;
	normalise.bool_set_end_of_user = TRUE;
	pthread_exit(0);
}
// 
// 
static gint normalise_timeout (gpointer data)
{
	if (TRUE == normalise.bool_etat) {
		if (NULL != normalise.MessUser) {
			g_print("normalise.MessUser = %s\n",normalise.MessUser);
			g_free (normalise.MessUser);
			normalise.MessUser = NULL;
		}
		normalise.bool_etat = FALSE;
	}
	if (TRUE == conv.BoolIsExtract || TRUE == conv.BoolIsConvert || TRUE == conv.BoolIsCopy || TRUE == conv.BoolIsNormalise || TRUE == conv.BoolIsReplaygain) {
		gchar	Str [ 200 ];
		
		Str [ 0 ] = '\0';
		
		if (TRUE == conv.BoolIsExtract) {
			strcat (Str, "<b><i>Extraction</i></b> ");
		}
		if (TRUE == conv.BoolIsConvert) {
			strcat (Str, "<b><i>Conversion</i></b> ");
		}
		if (TRUE == conv.BoolIsCopy) {
			strcat (Str, "<b><i>Copie</i></b> ");
		}
		if (TRUE == conv.BoolIsNormalise) {
			strcat (Str, "<b><i>Normalise</i></b> ");
		}
		if (TRUE == conv.BoolIsReplaygain) {
			strcat (Str, "<b><i>Replaygain</i></b>");
		}
		
		WindScan_set_label (Str);
	}
	if (TRUE == conv.bool_percent_conv || TRUE == normalise.bool_pass_conv) {
		
		gchar	*Str = NULL;
		
		normalise.total_percent = (double)((double)normalise.ElementActif + (double)conv.conversion_percent) / (double)normalise.NbrElementsInList;
		Str = g_strdup_printf ("%d%%", (int)(normalise.total_percent * 100));
		WindScan_set_progress (Str, normalise.total_percent);
		g_free (Str);
		Str = NULL;
		conv.bool_percent_conv = FALSE;
		return (TRUE);
	}
	if (TRUE == normalise.bool_pass_conv) {

		gchar *str = NULL;
    		gchar *spinner="|/-\\";
		gchar  foo [ 2 ];
		
		foo [ 0 ] = spinner[normalise.pass_conv++%4];
		foo [ 1 ] = '\0';
		
		str = g_strdup_printf ("<b>En cours: %d sur %d   %s</b>",
					normalise.ElementActif +1,
					normalise.NbrElementsInList,
					foo
					);
		g_free (str);
		str = NULL;

		return (TRUE);
	}

	else if (TRUE == normalise.bool_thread_end && TRUE == normalise.bool_set_end_of_user) {
		
		/* FIX */
		
		normalise_set_verif (normalise.ListFixWav);
		normalise_set_verif (normalise.ListFixOgg);
		normalise_set_verif (normalise.ListFixMp3);
		
		normalise.ListFixWav		= normalise_remove_glist_STRUCT (normalise.ListFixWav);
		normalise.ListFixOgg		= normalise_remove_glist_STRUCT (normalise.ListFixOgg);
		normalise.ListFixMp3		= normalise_remove_glist_STRUCT (normalise.ListFixMp3);
		
		/* PEAK */
		
		normalise_set_verif (normalise.ListPeakWav);
		normalise_set_verif (normalise.ListPeakOgg);
		normalise_set_verif (normalise.ListPeakMp3);
	
		normalise.ListPeakWav		= normalise_remove_glist_STRUCT (normalise.ListPeakWav);
		normalise.ListPeakOgg		= normalise_remove_glist_STRUCT (normalise.ListPeakOgg);
		normalise.ListPeakMp3		= normalise_remove_glist_STRUCT (normalise.ListPeakMp3);
		
		/* MIX */
		
		normalise_set_verif (normalise.ListMixGroupWav);
		normalise_set_verif (normalise.ListMixGroupOgg);
		normalise_set_verif (normalise.ListMixGroupMp3);
		
		normalise.ListMixGroupWav	= normalise_remove_glist_STRUCT (normalise.ListMixGroupWav);
		normalise.ListMixGroupOgg	= normalise_remove_glist_STRUCT (normalise.ListMixGroupOgg);
		normalise.ListMixGroupMp3	= normalise_remove_glist_STRUCT (normalise.ListMixGroupMp3);
		
		/* PEAK-GROUP */
		
		normalise_set_verif (normalise.ListPeakGroupWav);
		normalise_set_verif (normalise.ListPeakGroupOgg);
		normalise_set_verif (normalise.ListPeakGroupMp3);
		
		normalise.ListPeakGroupWav	= normalise_remove_glist_STRUCT (normalise.ListPeakGroupWav);
		normalise.ListPeakGroupOgg	= normalise_remove_glist_STRUCT (normalise.ListPeakGroupOgg);
		normalise.ListPeakGroupMp3	= normalise_remove_glist_STRUCT (normalise.ListPeakGroupMp3);

		g_source_remove (normalise.Handler_Timeout);
		file_pixbuf_update_glist ();
		fileaction_set_end (TYPE_NORMALISE);
	}

	return (TRUE);
}
// 
// 
void normalise_action (void)
{
	// PRINT_FUNC_LF();
 	
	// WindScan_open ("Normalisation", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Normalisation",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Normalisation des fichiers ...</i></b>");
	normalise.bool_thread_end = FALSE;
	normalise.bool_set_end_of_user = FALSE;
	normalise.bool_pass_conv = FALSE;
	normalise.pass_conv = -1;
	normalise.bool_etat = FALSE;
	normalise.MessUser = NULL;
	normalise.NbrElementsInList = 0;
	normalise.ElementActif = 0;
	conv_reset_struct (WindScan_close_request);
	pthread_create (&normalise.nmr_tid, NULL ,(void *)normalise_struct_lists, (void *)NULL);
	normalise.Handler_Timeout = g_timeout_add (100, normalise_timeout, 0);

}




