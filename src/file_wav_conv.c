 /*
 *  file      : file_wav_conv.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include "configuser.h"
#include "notify_send.h"
#include "win_scan.h"
#include "conv.h"
#include "file.h"




/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

typedef struct {
	gboolean	 BoolEndTimeout;
	gint		 PassMplayer;
	gboolean	 BoolCopy;
	gboolean	 BoolRest;
	gboolean	 BoolSox;
	gboolean	 BoolMplayer;
	
} VAR_FILEWAVCONF;

VAR_FILEWAVCONF VarFilewavconv;




// 
// 
static void filewavconv_thread (void *arg)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	INFO_WAV	*info = NULL;
	gint		pos;
	CONV_FIC_WAV	*PConvWav = NULL;
	gchar		**PtrTabArgs = NULL;

	// PRINT_FUNC_LF();

	conv.bool_thread_conv  = TRUE;

	/* Copie vers le dossier temporaire
	*/
	list = g_list_first (entetefile);
	while (FALSE == conv.bool_stop && list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (detail->type_infosong_file_is != FILE_IS_WAV) {
				list = g_list_next(list);
				continue;
			}
			info = (INFO_WAV *)detail->info;
			PConvWav = detail->PConvWav;

			if (FALSE == info->BoolConv) {
				list = g_list_next(list);
				continue;
			}

			if (libutils_test_file_exist (detail->namefile)) {
				VarFilewavconv.BoolCopy = TRUE;
				conv_copy_src_to_dest (detail->namefile, PConvWav->TmpSrc);
				VarFilewavconv.BoolCopy = FALSE;
			}
		}
		list = g_list_next (list);
	}

	/* Transformation avec MPLAYER et SOX
	*/	
	list = g_list_first (entetefile);
	while (FALSE == conv.bool_stop && list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			
			if (detail->type_infosong_file_is != FILE_IS_WAV) {
				list = g_list_next(list);
				continue;
			}

			info = (INFO_WAV *)detail->info;
			
			if (TRUE == info->BoolConv) {
				
				PConvWav = detail->PConvWav;
				
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
				PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
				PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
				PtrTabArgs [ pos++ ] = g_strdup (PConvWav->TmpSrc);
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup ("pcm");
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", PConvWav->TmpMplayer);
				PtrTabArgs [ pos++ ] = g_strdup ("-af");
				PtrTabArgs [ pos++ ] = g_strdup_printf ("channels=%s", info->NewVoie);
				PtrTabArgs [ pos++ ] = g_strdup ("-srate");
				PtrTabArgs [ pos++ ] = g_strdup (info->NewHertz);
				PtrTabArgs [ pos++ ] = NULL;
				
				VarFilewavconv.BoolMplayer = TRUE;
				conv_to_convert( PtrTabArgs, FALSE, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				VarFilewavconv.BoolMplayer = FALSE;

				VarFilewavconv.BoolCopy = TRUE;
				conv_copy_src_to_dest (PConvWav->TmpMplayer, PConvWav->TmpSrc);
				VarFilewavconv.BoolCopy = FALSE;

				if (atoi (info->bits) != atoi (info->NewBits)) {
					
					VarFilewavconv.BoolSox = TRUE;
					PtrTabArgs = conv_with_sox_get_param (PConvWav->TmpSrc, PConvWav->TmpDest, info->NewHertz, info->NewVoie, info->NewBits);
					conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
					
					VarFilewavconv.BoolSox = FALSE;
					VarFilewavconv.BoolCopy = TRUE;
					conv_copy_src_to_dest (PConvWav->TmpDest, PConvWav->TmpSrc);
					VarFilewavconv.BoolCopy = FALSE;
				}
			}
		}
		list = g_list_next(list);
	}

	/* Copie du dossier temporaire vers l'origine
	*/
	list = g_list_first (entetefile);
	while (FALSE == conv.bool_stop && list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			
			if (detail->type_infosong_file_is != FILE_IS_WAV) {
				list = g_list_next(list);
				continue;
			}

			info = (INFO_WAV *)detail->info;
			PConvWav = detail->PConvWav;
			if (FALSE == info->BoolConv) {
				list = g_list_next(list);
				continue;
			}
			
			info->BoolConv = FALSE;
			
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				if (FALSE == libutils_test_file_exist (PConvWav->TmpSrc)) {
					g_print ("LE FICHIER <%s>\n", PConvWav->TmpDest);
					g_print ("\tEST RESTITUE SANS AVOIR ETE MODIFIE :\n");
					g_print ("\tL' ENTETE NE CORRESPOND PAS A L'EXTENTION !!!\n");
				}
			}
			VarFilewavconv.BoolRest = TRUE;
			conv_copy_src_to_dest (PConvWav->TmpSrc, PConvWav->NameDest);
			VarFilewavconv.BoolRest = FALSE;
						
			detail->info = (INFO_WAV *)tagswav_remove_info (info);
			info = (INFO_WAV *)detail->info;
			// detail->info = (INFO_WAV *)tagswav_get_info (detail);
			detail->info = (INFO_WAV *)tagswav_get_info (detail->namefile);
			info = (INFO_WAV *)detail->info;
			// g_print("info->hertz = %s\tinfo->NewHertz  = %s\n", info->hertz, info->NewHertz);
			// g_print("info->voie  = %s\t\tinfo->NewVoie = %s\n", info->voie, info->NewVoie);
			// g_print("info->bits  = %s\tinfo->NewBits   = %s\n", info->bits, info->NewBits);
			
			g_unlink (PConvWav->TmpSrc);
			g_unlink (PConvWav->TmpDest);
			g_unlink (PConvWav->TmpMplayer);

			g_free (PConvWav->TmpSrc);		PConvWav->TmpSrc = NULL;
			g_free (PConvWav->TmpDest);		PConvWav->TmpDest = NULL;
			g_free (PConvWav->TmpMplayer);		PConvWav->TmpMplayer = NULL;
			g_free (PConvWav->NameDest);		PConvWav->NameDest = NULL;
		}
		list = g_list_next (list);
	}

	conv.bool_thread_conv = FALSE;
	pthread_exit(0);
}
// 
// 
gchar filewavconv_get_spinner( void )
{
	gchar *spinner="|/-\\";
	
	return( (gchar)spinner[ VarFilewavconv.PassMplayer++ %4 ] );
}
// 
// 
static gint filewavconv_timeout (gpointer data)
{
	if (conv.bool_percent_conv) {
	
		gchar  foo [ 4 ];
		gchar *Str = NULL;
		
		// DEBUG
		if( conv.total_percent > 1.0 ) conv.total_percent = 1.0;
		Str = g_strdup_printf ("%d%%", (gint)(conv.total_percent * 100));
		WindScan_set_progress (Str, conv.total_percent);
		g_free (Str);
		Str = NULL;
		
		if (VarFilewavconv.BoolCopy == TRUE) {
			foo [ 0 ] = filewavconv_get_spinner();
			foo [ 1 ] = '\0';
			Str = g_strdup_printf (_("<b><i>%s   Copy file: %d%%</i></b>"),
							foo,
							(gint)(conv.total_percent * 100));
			WindScan_set_label (Str);
			g_free (Str);
			Str = NULL;
		}
		else if (VarFilewavconv.BoolRest == TRUE) {	
			foo [ 0 ] = filewavconv_get_spinner();
			foo [ 1 ] = '\0';
			Str = g_strdup_printf (_("<b><i>%s   Return file: %d%%</i></b>"),
							foo,
							(gint)(conv.total_percent * 100));
			WindScan_set_label (Str);
			g_free (Str);
			Str = NULL;
		}
		else if (VarFilewavconv.BoolSox == TRUE) {
			foo [ 0 ] = filewavconv_get_spinner();
			foo [ 1 ] = '\0';
			Str = g_strdup_printf (_("<b><i>%s   Use Sox: %d%%</i></b>"),
							foo,
							(gint)(conv.total_percent * 100));
			WindScan_set_label (Str);
			g_free (Str);
			Str = NULL;
		}
		conv.bool_percent_conv = FALSE;
	}
	
	if (VarFilewavconv.BoolMplayer == TRUE) {
    		
		gchar  foo [ 4 ];
		gchar *Str = NULL;
		
		foo [ 0 ] = filewavconv_get_spinner();
		foo [ 1 ] = '\0';
		Str = g_strdup_printf (_("<b><i>%s   Use Mplayer: %d%%</i></b>"),
						foo,
						(gint)(conv.total_percent * 100));
		WindScan_set_label (Str);
		g_free (Str);
		Str = NULL;
		return (TRUE);
	}
	
	if (conv.bool_thread_conv == FALSE) {
		
		GList		*list = NULL;
		CONV_FIC_WAV	*PConvWav = NULL;
		DETAIL		*detail = NULL;
		
		g_source_remove (conv.handler_timeout_conv);
		WindScan_close ();
	
		// SUPPRESSION DE LA STRUCTURE: DETAIL->CONV_FIC_WAV
		list = g_list_first (entetefile);
		while (list) {
			if (NULL != (detail = (DETAIL *)list->data)) {
				if (NULL != (PConvWav = detail->PConvWav)) {
					
					if (NULL != PConvWav->Path)		{ g_free (PConvWav->Path);		PConvWav->Path       = NULL; }
					if (NULL != PConvWav->TmpSrc)		{ g_free (PConvWav->TmpSrc);		PConvWav->TmpSrc     = NULL; }
					if (NULL != PConvWav->TmpDest)		{ g_free (PConvWav->TmpDest);		PConvWav->TmpDest    = NULL; }
					if (NULL != PConvWav->TmpMplayer)	{ g_free (PConvWav->TmpMplayer);	PConvWav->TmpMplayer = NULL; }
					if (NULL != PConvWav->NameDest)		{ g_free (PConvWav->NameDest);		PConvWav->NameDest   = NULL; }
					g_free (PConvWav);
					PConvWav = detail->PConvWav = NULL;
				}
			}
			list = g_list_next (list);
		}
		
		// mise a jour des champs de 'EnteteWav.glist'
		FileWav_change_parameters ();
		
		// DELETTE TEMPORAY REP
		if (NULL != conv.TmpRep)  {
			conv.TmpRep  = libutils_remove_temporary_rep (conv.TmpRep);
		}
		
		if (FALSE == conv.bool_stop) {
			NotifySend_msg (_("XCFA: Conversions WAV TO WAV"), _("Ok"), conv.bool_stop);
		} else {
			NotifySend_msg (_("XCFA: Conversions WAV TO WAV"), _("Stop by user"), conv.bool_stop);
		}
	}
	
	return (TRUE);
}
// Initialisation des variables avant l'activation des threads d'extraction et de conversion
// 
void filewavconv_set_flags_before (void)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	gint		NumFile = 0;
	INFO_WAV	*info = NULL;
	CONV_FIC_WAV	*PConvWav = NULL;
	
	// PRINT_FUNC_LF();

	list = g_list_first (entetefile);
	while (!conv.bool_stop && list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			
			if (FILE_IS_WAV != detail->type_infosong_file_is) {
				list = g_list_next(list);
				continue;
			}
			
			info = (INFO_WAV *)detail->info;
			
			if (FALSE == info->BoolConv) {
				list = g_list_next(list);
				continue;
			}

			// CREATION STRUCTURE
			detail->PConvWav = (CONV_FIC_WAV *)g_malloc0 (sizeof (CONV_FIC_WAV));
			PConvWav = detail->PConvWav;
			
			// Create tmp rep
			if( NULL == conv.TmpRep )
				conv.TmpRep  = libutils_create_temporary_rep (Config.PathnameTMP, PATH_TMP_XCFA_AUDIOFILEWAVCONV);

			// Nouvelles: source et destination
			
			// $TMP/original
			PConvWav->TmpSrc     = g_strdup_printf ("%s/%03d.wav", conv.TmpRep, NumFile ++);
			
			//  $TMP/modifie
			PConvWav->TmpDest    = g_strdup_printf ("%s/%03d.wav", conv.TmpRep, NumFile ++);
			
			// $TMP/
			PConvWav->TmpMplayer = g_strdup_printf ("%s/%03d.wav", conv.TmpRep, NumFile ++);
	 		
	 		// Copie vers la destination
	 		PConvWav->NameDest   = file_get_pathname_dest (detail, "wav");

	 		
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		 		g_print ("fic->TmpSrc     = %s\n", PConvWav->TmpSrc);
		 		g_print ("fic->TmpDest    = %s\n", PConvWav->TmpDest);
		 		g_print ("fic->TmpMplayer = %s\n", PConvWav->TmpMplayer);
		 		g_print ("fic->NameDest   = %s\n\n", PConvWav->NameDest);
			}
			// Rectification des parametres si incomprehensible par SOX
			if (*info->NewHertz == '\0') {
				glong value = atol (info->hertz);
				g_free (info->NewHertz);
				info->NewHertz = NULL;
				if (value < 0 || value > 44100) info->NewHertz = g_strdup ("44100");
				else				info->NewHertz = g_strdup (info->hertz);
			}
			if (*info->NewVoie == '\0') {
				glong value = atol (info->voie);
				g_free (info->NewVoie);
				info->NewVoie = NULL;
				if (value != '1' || value != '2' || value != '4') info->NewVoie = g_strdup ("2");
				else					 	  info->NewVoie = g_strdup (info->voie);
			}
			if (*info->NewBits == '\0') {
				glong value = atol (info->bits);
				g_free (info->NewBits);
				info->NewBits = NULL;
				if (value != 8 || value != 16 || value != 32 || value != 64) info->NewBits = g_strdup ("16");
				else							     info->NewBits = g_strdup (info->bits);
			}

			// Copie vers le dossier temporaire
			conv.total_convert ++;
			// Transformation avec MPLAYER et SOX
			if (TRUE == info->BoolConv) {
				conv.total_convert ++;
				conv.total_convert ++;
			}
			if (atoi (info->bits) != atoi (info->NewBits)) {
				conv.total_convert ++;
				conv.total_convert ++;
			}
			// Copie du dossier temporaire vers l'origine
			conv.total_convert ++;			
			
			// g_print ("--> conv.total_convert = %d\n", conv.total_convert);
			
		}
		list = g_list_next (list);
	}
}
// 
// 
void filewavconv_apply (void)
{
	pthread_t nmr_tid;

	// PRINT_FUNC_LF();

	// WindScan_open ("Conversions files WAV", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Conversions files WAV",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Conversions WAV TO WAV ...</i></b>");
	
	VarFilewavconv.PassMplayer = 0;
	conv_reset_struct (WindScan_close_request);
	filewavconv_set_flags_before ();

	conv.bool_thread_conv = TRUE;
	conv.handler_timeout_conv = g_timeout_add (50, filewavconv_timeout, 0);
 	pthread_create (&nmr_tid, NULL ,(void *)filewavconv_thread, (void *)NULL);
}


