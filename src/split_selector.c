 /*
 *  file      : split_selector.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "alsa_play.h"
#include "split.h"
#include "statusbar.h"




extern VAR_SPLIT VarSplit;




void DEBUG_PRINT_SplitSelector (void)
{
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		gint	IndiceSrc;
	
		g_print ("\n");
		for (IndiceSrc = 0; IndiceSrc < MAX_SELECTEURS_SPLIT; IndiceSrc ++) {
			if (-1 != VarSplit.Selecteur [ IndiceSrc ] . Nmr) {
				g_print ("[ %2d ]  Begin = %6d   End = %6d\t",
						IndiceSrc,
						(gint)VarSplit.Selecteur [ IndiceSrc ] . BeginPaint,
						(gint)VarSplit.Selecteur [ IndiceSrc ] . EndPaint
						);
				g_print ("PercentBegin = %2.6f   PercentEnd = %2.6f\n",
						VarSplit.Selecteur [ IndiceSrc ] . PercentBegin,
						VarSplit.Selecteur [ IndiceSrc ] . PercentEnd
						);
			}
		}
	}
}
// 
// 
void SplitSelector_init (void)
{
	gint	Cpt;
	
	// PRINT_FUNC_LF();
	
	VarSplit.BoolPlay = FALSE;
	VarSplit.SelecteurActif = 0;
	VarSplit.NbrSelecteurs = 0;
	VarSplit.PercentActivePlay = 0.0;
	VarSplit.BoolBlankWithCue = FALSE;
	for (Cpt = 0; Cpt < MAX_SELECTEURS_SPLIT; Cpt ++) {
		VarSplit.Selecteur [ Cpt ] . Nmr = -1;
		VarSplit.Selecteur [ Cpt ] . BeginPaint = -1;
		VarSplit.Selecteur [ Cpt ] . EndPaint = -1;
		VarSplit.Selecteur [ Cpt ] . PercentBegin =
		VarSplit.Selecteur [ Cpt ] . PercentEnd = 0.0;
	}
}
// SELECTEUR: IL N EN RESTERA QU UN  ;-)
// 
void on_button_del_cut_split_clicked (GtkButton *button, gpointer user_data)
{
	if (VarSplit.NbrSelecteurs > 1) {
		
		PRINT_FUNC_LF();
		
		DEBUG_PRINT_SplitSelector ();
		
		// INIT STRUCTURES: Selecteur ET Points 50 db
		SplitSelector_init ();
		VarSplit.SelecteurActif = 0;
		VarSplit.NbrSelecteurs = 1;
		VarSplit.Selecteur [ 0 ] . Nmr = 0;
		VarSplit.Selecteur [ 0 ] . BeginPaint = 0;
		VarSplit.Selecteur [ 0 ] . EndPaint = (SplitSpectre_get_with() -1 );
		VarSplit.Selecteur [ 0 ] . PercentBegin =
		VarSplit.Selecteur [ 0 ] . PercentEnd = 0.0;
		VarSplit.Selecteur [ 0 ] . PercentBegin = ((gdouble)VarSplit.Selecteur [ 0 ] . BeginPaint / (gdouble)SplitSpectre_get_with() ) * 100.0;
		VarSplit.Selecteur [ 0 ] . PercentEnd   = ((gdouble)VarSplit.Selecteur [ 0 ] . EndPaint / (gdouble)SplitSpectre_get_with() ) * 100.0;
		
		gtk_widget_queue_draw (VarSplit.AdrWidgetSpectre);
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		
		DEBUG_PRINT_SplitSelector ();
		split_set_flag_buttons ();
	}
}
// DESTRUCTION DU SELCTEUR ACTIF
// 
void SplitSelector_cut (void)
{
	if (0 == VarSplit.NbrSelecteurs) {
		PRINT("\tAUCUN SELECTEUR  !!!");
	}
	else if (VarSplit.NbrSelecteurs == 1) {
		PRINT("\tLE SELECTEUR RESTANT EST OBLIGATOIRE : Il n'en restera qu'un  ;-)");
	}
	else if (VarSplit.NbrSelecteurs > 1) {
		
		if (TRUE == AlsaPlay_is_play ()) {
			StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Deleting not during playback") );
		}
		else {
			gint	Zone = 0;
		
			// PRINT_FUNC_LF();
			// g_print("\tSUPPRESSION DU SELECTEUR ACTIF: %d\n", VarSplit.SelecteurActif);
			// 
			// g_memmove (dest, src, len)
			// 
			// DEPLACER LES BUFFERS VERS L AVANT EN COMMENCANT PAR LA FIN
			for (Zone = VarSplit.SelecteurActif +1; Zone < VarSplit.NbrSelecteurs; Zone ++) {
				g_memmove(&VarSplit.Selecteur [ Zone -1 ], &VarSplit.Selecteur [ Zone ], sizeof(SELECTEUR));
			}

			VarSplit.NbrSelecteurs --;
			if (VarSplit.SelecteurActif >= VarSplit.NbrSelecteurs -1 && VarSplit.SelecteurActif > 0) VarSplit.SelecteurActif --;
			
			VarSplit.Selecteur [ VarSplit.NbrSelecteurs ] . Nmr = -1;
			VarSplit.Selecteur [ VarSplit.NbrSelecteurs ] . BeginPaint = -1;
			VarSplit.Selecteur [ VarSplit.NbrSelecteurs ] . EndPaint = -1;
			VarSplit.Selecteur [ VarSplit.NbrSelecteurs ] . PercentBegin =
			VarSplit.Selecteur [ VarSplit.NbrSelecteurs ] . PercentEnd = 0.0;

			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		}
	}
}
// 
// 
gint SplitSelector_get_diff_sec (gdouble p_PercentBegin, gdouble p_PercentEnd)
{
	gint		Len = -1;
	gdouble		TimeSongSec;
	gint		sec_begin;
	gint		sec_end;
	
	// ACQUISITION DUREE TOTALE DU FICHIER EN SECONDES
	TimeSongSec = VarSplit.Tags->SecTime;
	
	sec_begin = (gint) (((gdouble)TimeSongSec * (gdouble)p_PercentBegin) / 100.0);
	sec_end   = (gint) (((gdouble)TimeSongSec * (gdouble)p_PercentEnd) / 100.0);
	
	// Len = (sec_end % 60) - (sec_begin % 60);
	Len = (sec_end ) - (sec_begin );
	return (Len);
}
// 
// 
gint SplitSelector_get_PosSelecteurActif (gint p_CursorX)
{
	gint	PosSelecteurActif = -1;
	
	for (PosSelecteurActif = 0; PosSelecteurActif < VarSplit.NbrSelecteurs ; PosSelecteurActif ++) {
		if (p_CursorX > SplitSelector_get_pos_begin (PosSelecteurActif) && p_CursorX < SplitSelector_get_pos_end (PosSelecteurActif)) {
			return (PosSelecteurActif);
		}
	}
	return (-1);
}
// DOUBLE CLICK : GENERATION D UN NOUVEL INDEX
// 
// @Dzef: TODO
//	- Pour ajouter manuellement un marqueur de début, j'ajoute, par défaut, au même endroit celui de sortie de la plage précédente.
//	- En mode lecture un double clic sur le bandeau des index place un sélecteur à la position de la tête de lecture ?
//	- En mode pause un double clic sur le bandeau des index place un sélecteur à l'endroit où on a cliqué ?
// 	- La règle  étant que 2 plages ne doivent pas pouvoir se chevaucher.
// 
void SplitSelector_add (gint p_CursorX)
{
	gint			IndiceDest = 0;
	gint			IndiceSrc = 0;
	gint			CursorX = p_CursorX;
	gint			Cpt; 	
	gdouble			PercentCursorX;
	gint			Len;
	GtkAllocation	allocation;

	// PRINT_FUNC_LF();
		
	DEBUG_PRINT_SplitSelector ();
	// 
	// CAS 1 : CursorX < BEGIN_MIN
	// 
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
	if (CursorX < SplitSelector_get_pos_begin (0)) {
		
		PRINT("CAS 1 : CursorX < BEGIN_MIN");
		// 
		// --
		// 
		PercentCursorX = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
		Len = SplitSelector_get_diff_sec (0.0, PercentCursorX);
		if (Len < 5) {
			StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Time of less than 5 seconds between marker and cursor !") );
			return;
		}
		
		Len = SplitSelector_get_diff_sec (PercentCursorX, VarSplit.Selecteur [ 0 ] . PercentBegin);
		if (Len < 5) {
			StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Time of less than 5 seconds between marker and cursor !") );
			return;
		}
		
		// 
		// g_memmove (dest, src, len)
		// 
		// DEPLACER LES BUFFERS VERS L AVANT EN COMMENCANT PAR LA FIN
		if (VarSplit.NbrSelecteurs < MAX_SELECTEURS_SPLIT) {
			for (IndiceDest = VarSplit.NbrSelecteurs; IndiceDest > 0; IndiceDest --) {
				IndiceSrc = IndiceDest -1;
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("\tg_memmove(%d, %d, %d);\n", IndiceDest, IndiceSrc, (gint)sizeof(SELECTEUR));
				g_memmove(&VarSplit.Selecteur [ IndiceDest ], &VarSplit.Selecteur [ IndiceSrc ], sizeof(SELECTEUR));
			}
		}
		
		// VALIDER LA NOUVELLE SELECTION ACTIVE
		VarSplit.SelecteurActif = 0;
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("VarSplit.SelecteurActif = %d\n",VarSplit.SelecteurActif);
		
		// AJOUTER UN SELECTEUR
		VarSplit.NbrSelecteurs ++;
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("VarSplit.NbrSelecteurs = %d\n",VarSplit.NbrSelecteurs);
		
		// REINDEXER LA NUMEROTATION
		for (IndiceSrc = 0; IndiceSrc < VarSplit.NbrSelecteurs; IndiceSrc ++) {
			VarSplit.Selecteur [ IndiceSrc ] . Nmr = IndiceSrc;
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print ("VarSplit.Selecteur [ %d ] . Nmr = %d\n",IndiceSrc, VarSplit.Selecteur [ IndiceSrc ] . Nmr);
		}
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("VarSplit.Selecteur [ %d ] . Nmr = %d\n",IndiceSrc, VarSplit.Selecteur [ IndiceSrc ] . Nmr);
		
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . BeginPaint   = CursorX;
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentBegin = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
		
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint   = VarSplit.Selecteur [ VarSplit.SelecteurActif +1 ] . BeginPaint;
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentEnd = VarSplit.Selecteur [ VarSplit.SelecteurActif +1 ] . PercentBegin;
	}
	// 
	// CAS 2 : CursorX > END_MAX
	// 
	else if (CursorX > SplitSelector_get_pos_end (VarSplit.NbrSelecteurs -1)) {
		
		PRINT("CAS 2 : CursorX > END_MAX");
		// 
		// --
		// 
		PercentCursorX = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
		Len = SplitSelector_get_diff_sec (VarSplit.Selecteur [ 0 ] . PercentEnd, PercentCursorX);
		if (Len < 5) {
			StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Time of less than 5 seconds between marker and cursor !") );
			return;
		}
		
		Len = SplitSelector_get_diff_sec (PercentCursorX, 100.0);
		if (Len < 5) {
			StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Time of less than 5 seconds between marker and cursor !") );
			return;
		}
		
		// 
		// g_memmove (dest, src, len)
		// 
		// DEPLACER LES BUFFERS VERS L AVANT EN COMMENCANT PAR LA FIN
		if (VarSplit.NbrSelecteurs < MAX_SELECTEURS_SPLIT) {
			for (IndiceDest = VarSplit.NbrSelecteurs; IndiceDest > VarSplit.NbrSelecteurs -1; IndiceDest --) {
				IndiceSrc = IndiceDest -1;
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("\tg_memmove(%d, %d, %d);\n", IndiceDest, IndiceSrc, (gint)sizeof(SELECTEUR));
				g_memmove(&VarSplit.Selecteur [ IndiceDest ], &VarSplit.Selecteur [ IndiceSrc ], sizeof(SELECTEUR));
			}
		}
		
		// VALIDER LA NOUVELLE SELECTION ACTIVE
		VarSplit.SelecteurActif = VarSplit.NbrSelecteurs;
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("VarSplit.SelecteurActif = %d\n",VarSplit.SelecteurActif);
		
		// AJOUTER UN SELECTEUR
		VarSplit.NbrSelecteurs ++;
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("VarSplit.NbrSelecteurs = %d\n",VarSplit.NbrSelecteurs);
		
		// REINDEXER LA NUMEROTATION
		for (IndiceSrc = 0; IndiceSrc < VarSplit.NbrSelecteurs; IndiceSrc ++) {
			VarSplit.Selecteur [ IndiceSrc ] . Nmr = IndiceSrc;
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print ("VarSplit.Selecteur [ %d ] . Nmr = %d\n",IndiceSrc, VarSplit.Selecteur [ IndiceSrc ] . Nmr);
		}
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("VarSplit.Selecteur [ %d ] . Nmr = %d\n",IndiceSrc, VarSplit.Selecteur [ IndiceSrc ] . Nmr);
		
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . BeginPaint   = CursorX;
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentBegin = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;

		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint   = VARSPLIT_SPECTRE_W ;
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentEnd = ((gdouble)VARSPLIT_SPECTRE_W  / (gdouble)SplitSpectre_get_with() ) * 100.0;
		
	}
	else {
		for (Cpt = 0; Cpt <= VarSplit.NbrSelecteurs; Cpt ++) {
			// 
			// CAS 3 : CursorX IN BEGIN .. END
			// 
			if (CursorX > SplitSelector_get_pos_begin (Cpt) && CursorX < SplitSelector_get_pos_end (Cpt)) {
				PRINT("CAS 3 : CursorX IN BEGIN .. END");
				// 
				// --
				// 
				PercentCursorX = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
				Len = SplitSelector_get_diff_sec (VarSplit.Selecteur [ Cpt ] . PercentBegin, PercentCursorX);
				if (Len < 5) {
				PRINT("1- if (Len < 5) {");
					StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Time of less than 5 seconds between marker and cursor !") );
					return;
				}
				
				Len = SplitSelector_get_diff_sec (PercentCursorX, VarSplit.Selecteur [ Cpt ] . PercentEnd);
				if (Len < 5) {
				PRINT("2- if (Len < 5) {");
					StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Time of less than 5 seconds between marker and cursor !") );
					return;
				}
		
				// 
				// g_memmove (dest, src, len)
				// 
				// DEPLACER LES BUFFERS VERS L AVANT
				if (VarSplit.NbrSelecteurs < MAX_SELECTEURS_SPLIT) {
					for (IndiceDest = VarSplit.NbrSelecteurs; IndiceDest > Cpt; IndiceDest --) {
						IndiceSrc = IndiceDest -1;
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("\tg_memmove(%d, %d, %d);\n", IndiceDest, IndiceSrc, (gint)sizeof(SELECTEUR));
						g_memmove(&VarSplit.Selecteur [ IndiceDest ], &VarSplit.Selecteur [ IndiceSrc ], sizeof(SELECTEUR));
					}
				}
				
				// VALIDER LA NOUVELLE SELECTION ACTIVE
				VarSplit.SelecteurActif = Cpt +1;
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("VarSplit.SelecteurActif = %d\n",VarSplit.SelecteurActif);
				
				// AJOUTER UN SELECTEUR
				VarSplit.NbrSelecteurs ++;
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("VarSplit.NbrSelecteurs = %d\n",VarSplit.NbrSelecteurs);
				
				// REINDEXER LA NUMEROTATION
				for (IndiceSrc = 0; IndiceSrc < VarSplit.NbrSelecteurs; IndiceSrc ++) {
					VarSplit.Selecteur [ IndiceSrc ] . Nmr = IndiceSrc;
					if( TRUE == OptionsCommandLine.BoolVerboseMode )
						g_print ("VarSplit.Selecteur [ %d ] . Nmr = %d\n",IndiceSrc, VarSplit.Selecteur [ IndiceSrc ] . Nmr);
				}
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("VarSplit.Selecteur [ %d ] . Nmr = %d\n",IndiceSrc, VarSplit.Selecteur [ IndiceSrc ] . Nmr);
				
				/*
				RESLOLU @Dzef
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . BeginPaint    = CursorX;
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentBegin  = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
				if (CursorX + 50 < VarSplit.SpectreW)
					CursorX += 50;
				else	CursorX = VarSplit.SpectreW;
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint      = CursorX;
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentEnd    = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
				*/
				// end 0  = end -1
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint      = VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . EndPaint;
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentEnd    = VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . PercentEnd;
				
				// end -1 = cursorX
				VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . EndPaint      = CursorX;
				VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . PercentEnd    = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
				
				// Begin  = cursorX
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . BeginPaint    = CursorX;
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentBegin  = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
				break;
			}
			// 
			// CAS 4 : CursorX IN END .. BEGIN
			// 
			else if (CursorX > SplitSelector_get_pos_end (Cpt) && CursorX < SplitSelector_get_pos_begin (Cpt +1)) {
				PRINT("CAS 4 : CursorX IN END .. BEGIN");
				// 
				// --
				// 
				PercentCursorX = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
				Len = SplitSelector_get_diff_sec (VarSplit.Selecteur [ Cpt ] . PercentEnd, PercentCursorX);
				if (Len < 5) {
					StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Time of less than 5 seconds between marker and cursor !") );
					return;
				}
				
				Len = SplitSelector_get_diff_sec (PercentCursorX, VarSplit.Selecteur [ Cpt +1 ] . PercentBegin);
				if (Len < 5) {
					StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_WARN_, _("Time of less than 5 seconds between marker and cursor !") );
					return;
				}
		
				// 
				// g_memmove (dest, src, len)
				// 
				// DEPLACER LES BUFFERS VERS L AVANT
				if (VarSplit.NbrSelecteurs < MAX_SELECTEURS_SPLIT) {
					for (IndiceDest = VarSplit.NbrSelecteurs; IndiceDest > Cpt; IndiceDest --) {
						IndiceSrc = IndiceDest -1;
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("\tg_memmove(%d, %d, %d);\n", IndiceDest, IndiceSrc, (gint)sizeof(SELECTEUR));
						g_memmove(&VarSplit.Selecteur [ IndiceDest ], &VarSplit.Selecteur [ IndiceSrc ], sizeof(SELECTEUR));
					}
				}
				
				// VALIDER LA NOUVELLE SELECTION ACTIVE
				VarSplit.SelecteurActif = Cpt +1;
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("VarSplit.SelecteurActif = %d\n",VarSplit.SelecteurActif);
				
				// AJOUTER UN SELECTEUR
				VarSplit.NbrSelecteurs ++;
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("VarSplit.NbrSelecteurs = %d\n",VarSplit.NbrSelecteurs);
				
				// REINDEXER LA NUMEROTATION
				for (IndiceSrc = 0; IndiceSrc < VarSplit.NbrSelecteurs; IndiceSrc ++) {
					VarSplit.Selecteur [ IndiceSrc ] . Nmr = IndiceSrc;
					if( TRUE == OptionsCommandLine.BoolVerboseMode )
						g_print ("VarSplit.Selecteur [ %d ] . Nmr = %d\n",IndiceSrc, VarSplit.Selecteur [ IndiceSrc ] . Nmr);
				}
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("VarSplit.Selecteur [ %d ] . Nmr = %d\n",IndiceSrc, VarSplit.Selecteur [ IndiceSrc ] . Nmr);
				
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . BeginPaint    = CursorX;
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentBegin  = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
				
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint      = VarSplit.Selecteur [ VarSplit.SelecteurActif +1 ] . BeginPaint;
				VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentEnd    = VarSplit.Selecteur [ VarSplit.SelecteurActif +1 ] . PercentBegin;
				
				break;
			}
		}
	}	
	// gtk_widget_queue_draw (VarSplit.AdrWidgetSpectre);
}
// 
// TRANSFORME 4 SECONDES EN PIXELS ET RETOUR
// 
gint SplitSelector_get_4_secondes_to_int (void)
{	
	gint		Cpt;
	gint		sec;
	// gdouble		dsec;
	gdouble		TimeSongSec;
	gdouble		Percent = 0.0;
	gint		H, M, S;
	// gint		hundr, C;
	gint		Len = -1;
	
	if (NULL == VarSplit.Tags || VarSplit.NbrSelecteurs <= 0) {
		return (-1);
	}
	
	// PRINT_FUNC_LF();
	
	// ACQUISITION DUREE TOTALE DU FICHIER EN SECONDES
	TimeSongSec = VarSplit.Tags->SecTime;
	
	for (Cpt = 0; ; Cpt ++) {
		
		sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
		// dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
		// hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
		H = (sec / 60) / 60;
		M = (sec / 60) % 60;
		S = sec % 60;
		// C = hundr;
		if (H == 0 && M == 0 && S == 5) {
			Len = (gint)(((gdouble)SplitSpectre_get_with()  * Percent) / 100.0);
			break;
		}
		if (Cpt > 100000 && S == 0) break;
		Percent += 0.001;
	}
	return (Len);
}
// 
// TRANSFORME 4 SECONDES EN PIXELS ET RETOUR
// 
gdouble SplitSelector_get_percent_for_x_secondes (gint p_secondes)
{	
	gint		Cpt;
	gint		sec;
	// gdouble		dsec;
	gdouble		TimeSongSec;
	gdouble		Percent = 0.0;
	gint		H, M, S;
	// gint		hundr, C;
	
	if (NULL == VarSplit.Tags || VarSplit.NbrSelecteurs <= 0) {
		return (-1);
	}
	
	// PRINT_FUNC_LF();
	
	// ACQUISITION DUREE TOTALE DU FICHIER EN SECONDES
	TimeSongSec = VarSplit.Tags->SecTime;
	
	for (Cpt = 0; ; Cpt ++) {
		
		sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
		// dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
		// hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
		H = (sec / 60) / 60;
		M = (sec / 60) % 60;
		S = sec % 60;
		// C = hundr;
		if (H == 0 && M == 0 && S == p_secondes) {
			return (Percent);
		}
		if (Cpt > 100000 && S == 0) break;
		Percent += 0.001;
	}
	return (Percent);
}
// 
// 
void SplitSelector_get_pos (void)
{
	gint	Cpt;
	
	for (Cpt = 0; Cpt < MAX_SELECTEURS_SPLIT; Cpt ++) {
		
		VarSplit.Selecteur [ Cpt ] . BeginPaint  = (gint)(((gdouble)SplitSpectre_get_with()  * VarSplit.Selecteur [ Cpt ] . PercentBegin) / 100.0);
		VarSplit.Selecteur [ Cpt ] . EndPaint    = (gint)(((gdouble)SplitSpectre_get_with()  * VarSplit.Selecteur [ Cpt ] . PercentEnd) / 100.0);
	}
}
// 
// TODO : 4 SECONDES EST LA DUREE MINIMUM POUR UNE PLAGE
// 
void SplitSelector_set_pos_begin (gint p_begin)
{
	gint	TimeForSec = SplitSelector_get_4_secondes_to_int ();
	gint	With       = SplitSpectre_get_with();
	
	// PRINT_FUNC_LF();
	
	if (-1 == TimeForSec) return;	
	if (p_begin < 0) p_begin = 0;
	if (p_begin > With) p_begin = With;
	if (p_begin + TimeForSec >= With) return;
	
	if (VarSplit.SelecteurActif > 0) {

		// VERS LE PRECEDENT

		if (p_begin < VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . BeginPaint + TimeForSec) {
			p_begin = VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . BeginPaint + TimeForSec;
			VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . PercentEnd = ((gdouble)p_begin / (gdouble)With ) * 100.0;
		}
		if (p_begin < VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . EndPaint) {
			VarSplit.Selecteur [ VarSplit.SelecteurActif -1 ] . PercentEnd = ((gdouble)p_begin / (gdouble)With ) * 100.0;
		}
		
	}
	
	// VERS LE SUIVANT
	
	if (VarSplit.SelecteurActif  < VarSplit.NbrSelecteurs -1 &&
	    p_begin > VarSplit.Selecteur [ VarSplit.SelecteurActif +1 ] . BeginPaint - TimeForSec) {
		
		p_begin = VarSplit.Selecteur [ VarSplit.SelecteurActif +1 ] . BeginPaint - TimeForSec;
	}
	
	if (p_begin > VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint - TimeForSec) {
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint   = p_begin + TimeForSec;
		VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentEnd = ((gdouble)(p_begin + TimeForSec) / (gdouble)With ) * 100.0;
	}	
	
	VarSplit.Selecteur [ VarSplit.SelecteurActif  ] . PercentBegin = ((gdouble)p_begin / (gdouble)With ) * 100.0;
		
	SplitSelector_get_pos ();
}
// 
// TODO : 4 SECONDES EST LA DUREE MINIMUM POUR UNE PLAGE
// 
void SplitSelector_set_pos_end (gint p_end)
{
	gint	TimeForSec = SplitSelector_get_4_secondes_to_int();
	gint	With       = SplitSpectre_get_with();
	
	// PRINT_FUNC_LF();
	
	if (-1 == TimeForSec) return;
	if (p_end < 0) p_end = 0;
	if( p_end > With ) p_end = With - 2;
	
	// LE SELECTEUR DE FIN DOIT BUTTER SUR LE SELECTEUR SUIVANT
	if (VarSplit.NbrSelecteurs -1 > VarSplit.SelecteurActif) {
		if (p_end > VarSplit.Selecteur [ VarSplit.SelecteurActif +1 ] . BeginPaint) {
			p_end = VarSplit.Selecteur [ VarSplit.SelecteurActif +1 ] . BeginPaint;
		}
	}
	
	// BUTTEE SUR SELECTEUR DE DEBUT
	// CHEVAUCHEMENT D INDEX INTERDIT ! PREVOIR ESPACE DE 4 SECONDES
	if (p_end < VarSplit.Selecteur [ VarSplit.SelecteurActif ] . BeginPaint + TimeForSec)
		p_end = VarSplit.Selecteur [ VarSplit.SelecteurActif ] . BeginPaint + TimeForSec;	
	
	VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint   = p_end;
	VarSplit.Selecteur [ VarSplit.SelecteurActif ] . PercentEnd = ((gdouble)VarSplit.Selecteur [ VarSplit.SelecteurActif ] . EndPaint / (gdouble)With ) * 100.0;
}
// 
// 
gint SplitSelector_get_pos_begin (gint p_Sel)
{
	SplitSelector_get_pos ();

	return( VarSplit.Selecteur [ p_Sel ] . BeginPaint );
}
// 
// 
gint SplitSelector_get_pos_end (gint p_Sel)
{
	SplitSelector_get_pos ();

	return (VarSplit.Selecteur [ p_Sel ] . EndPaint);
}
// 
// 
gdouble SplitSelector_get_percent_begin (gint p_Sel)
{
	SplitSelector_get_pos ();

	return (VarSplit.Selecteur [ p_Sel ] . PercentBegin);
}
// 
// 
gdouble SplitSelector_get_percent_end (gint p_Sel)
{
	SplitSelector_get_pos ();

	return (VarSplit.Selecteur [ p_Sel ] . PercentEnd);
}

// 
// 
gint SplitSelector_get_pos_play (void)
{
	return ((gint)(((gdouble)SplitSpectre_get_with()  * VarSplit.PercentActivePlay) / 100.0));
}
// 
// 
gboolean SplitSelector_cursor_in_box_play (gint p_cursor_x, gint p_cursor_y)
{
	if (VarSplit.PercentActivePlay >= 0.0) {
		
		gint	SelPlay = SplitSelector_get_pos_play ();
		GtkAllocation	allocation;
		
		gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
		if (p_cursor_x > SelPlay -6 && p_cursor_x < SelPlay +7)
			if (p_cursor_y > VARSPLIT_SPECTRE_Y + VARSPLIT_SPECTRE_H -12 && p_cursor_y < VARSPLIT_SPECTRE_Y + VARSPLIT_SPECTRE_H)
				return (TRUE);
	}
	return (FALSE);
}
// 
// 
gboolean SplitSelector_cursor_in_line_play (gint p_cursor_x, gint p_cursor_y)
{
	if (VarSplit.PercentActivePlay >= 0.0) {
		
		GtkAllocation	allocation;
		gint	SelPlay = SplitSelector_get_pos_play ();
	
		gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
		if (p_cursor_x    == SelPlay ||
		    p_cursor_x +1 == SelPlay ||
		    p_cursor_x -1 == SelPlay) {
			if (p_cursor_y > VARSPLIT_SPECTRE_Y && p_cursor_y < VARSPLIT_SPECTRE_Y + VARSPLIT_SPECTRE_H)
				return (TRUE);
		}
	}
	return (FALSE);
}
// 
// 
void SplitSelector_set_pos_play( gint p_play )
{
	gint	With = SplitSpectre_get_with();
	
	if( p_play < 0 ) p_play = 0;
	if( p_play > With ) p_play = With;
	VarSplit.PercentActivePlay = ((gdouble)p_play / (gdouble)With ) * 100.0;
}








