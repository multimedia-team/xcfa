 /*
 *  file      : options_internal.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "conv.h"
#include "options.h"


	/*
	StringExpanderLame
	StringExpanderOggenc
	StringExpanderFlac
	StringExpanderFaac
	StringExpanderMppenc
	StringExpanderMac
	StringExpanderWavpack
	
	BoolArtistTag
	BoolTitleTag
	BoolAlbumTag
	BoolNumerateTag
	BoolGenreTag
	BoolYearTag
	BoolCommentTag
	
	*/

// 
// 
GList *OptionsInternal_add_options_for_visual (GList *p_list, gboolean p_bool_color, gchar *p_str)
{
	if (p_bool_color) {
		p_list = g_list_append (p_list, g_strdup_printf ("<span foreground=\"#0000FF\">%s</span>", p_str));
	} else {
		p_list = g_list_append (p_list, g_strdup (p_str));
	}
	return  ((GList *)p_list);
}

// RECUPERATION DES PARAMETRES INTERNES POUR VISUALISATION DEPUIS LA LIGNE POUR EXPERT
// 
GString *OptionsInternal_get_options_for_visual (TYPE_CONV type_conv)
{
	GList     *New = NULL;
	GList     *list = NULL;
	GString   *gstr = NULL;
	gchar     *ptr = NULL;
	gchar     *str = NULL;
	
	if (type_conv == LAME_WAV_TO_MP3) {
		gchar         **Larrbuf = NULL;
		gint            i;

		New = g_list_append (New, g_strdup ("-h"));
		New = g_list_append (New, g_strdup ("--nohist"));
		/*
		LES OPTIONS CHANGEES EN COULEUR
		IDEE DE ROMAIN -> alteo_gange
		---------------------------------------------------------------------------------------------
		<span foreground=\"#0000FF\">  Xcfa sur le web</span>
		New = g_list_append (New, g_strdup ("<span foreground=\"#0000FF\">--noreplaygain</span>"));
		*/
		New = g_list_append (New, g_strdup ("--noreplaygain"));

		if (*optionsLame_get_str_val_mode_lame () != '\0') {
			Larrbuf = g_strsplit (optionsLame_get_str_val_mode_lame (), " ", 0);
			for (i=0; Larrbuf[i]; i++) {
				New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_LAME_MODE, Larrbuf[i]);
			}
			g_strfreev(Larrbuf);
		}
		Larrbuf = g_strsplit (optionsLame_get_str_val_bitrate_abr_vbr_lame (), " ", 0);
		for (i=0; Larrbuf[i]; i++) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_LAME_DEBIT, Larrbuf[i]);
		}
		g_strfreev(Larrbuf);

		if (Config.BoolArtistTag || Config.BoolTitleTag || Config.BoolAlbumTag ||
		    Config.BoolNumerateTag || Config.BoolGenreTag || Config.BoolYearTag || Config.BoolCommentTag) {

			New = OptionsInternal_add_options_for_visual (New, FALSE, "--add-id3v2");
		}
		if (Config.BoolArtistTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ARTIST, "--ta %a");
		}
		if (Config.BoolTitleTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_TITLE, "--tt %t");
		}
		if (Config.BoolAlbumTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ALBUM, "--tl %l");
		}
		if (Config.BoolNumerateTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_NUMERATE, "--tn %N");
		}
		if (Config.BoolGenreTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_GENRE, "--tg %G");
		}
		if (Config.BoolYearTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_YEAR, "--ty %d");
		}
		if (Config.BoolCommentTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_COMMENT, "--tc %c");
		}
	}
	else if (type_conv == OGGENC_WAV_TO_OGG || type_conv == OGGENC_FLAC_TO_OGG) {

		New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_OGGENC_DEBIT, optionsOggenc_get_val_bitrate_oggenc ());
		
		if (optionsOggenc_get_bool_managed_oggenc () == TRUE) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_OGGENC_MANAGED, "--managed");
		}
		if (optionsOggenc_get_bool_downmix_oggenc () == TRUE) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_OGGENC_DOWNMIX, "--downmix");
		}
		
		if (Config.BoolArtistTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ARTIST, "-a %a");
		}
		if (Config.BoolTitleTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_TITLE, "-t %t");
		}
		if (Config.BoolAlbumTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ALBUM, "-l %l");
		}
		if (Config.BoolNumerateTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_NUMERATE, "-N %N");
		}
		if (Config.BoolGenreTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_GENRE, "-G %G");
		}
		if (Config.BoolYearTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_YEAR, "--date %d");
		}
		if (Config.BoolCommentTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_COMMENT, "-c %c");
		}
	}
	else if (type_conv == FLAC_WAV_TO_FLAC) {

		New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_FLAC_TAUX_COMPRESSION, optionsFlac_get_compression_level_flac ());

		if (Config.BoolArtistTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ARTIST, "-T artist=%a");
		}
		if (Config.BoolTitleTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_TITLE, "-T title=%t");
		}
		if (Config.BoolAlbumTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ALBUM, "-T album=%l");
		}
		if (Config.BoolNumerateTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_NUMERATE, "-T tracknumber=%N");
		}
		if (Config.BoolGenreTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_GENRE, "-T genre=%G");
		}
		if (Config.BoolYearTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_YEAR, "-T date=%d");
		}
		if (Config.BoolCommentTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_COMMENT, "-T Description=%c");
		}
	}
	else if (type_conv == FAAC_WAV_TO_M4A) {
	
		New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_FAAC_CONTENEUR, OptionsFaac_get_faac_conteneur ());
		New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_FAAC_SET_CHOICE_VBR_ABR, OptionsFaac_get_faac_set_choice_vbr_abr ());

		if (Config.BoolArtistTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ARTIST, "--artist %a");
		}
		if (Config.BoolTitleTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_TITLE, "--title %t");
		}
		if (Config.BoolAlbumTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ALBUM, "--album %l");
		}
		if (Config.BoolGenreTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_GENRE, "--genre %G");
		}
		if (Config.BoolYearTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_YEAR, "--year %d");
		}
		if (Config.BoolCommentTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_COMMENT, "--comment %c");
		}
	}
	else if (type_conv == MPPENC_WAV_TO_MPC) {

		New = g_list_append (New, g_strdup ("--verbose"));
		New = g_list_append (New, g_strdup ("--overwrite"));

		New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_MUSEPACK_QUALITE, optionsMusepack_get_quality_mppenc ());

		if (Config.BoolArtistTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ARTIST, "--artist %a");
		}
		if (Config.BoolTitleTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_TITLE, "--title %t");
		}
		if (Config.BoolAlbumTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ALBUM, "--album %l");
		}
		if (Config.BoolGenreTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_GENRE, "--genre %G");
		}
		if (Config.BoolYearTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_YEAR, "--year %d");
		}
		if (Config.BoolNumerateTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_NUMERATE, "--track %N");
		}
		if (Config.BoolCommentTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_COMMENT, "--comment %c");
		}
	}
	else if (type_conv == MAC_WAV_TO_APE || type_conv == MAC_APE_TO_WAV) {

		New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_MAC_QUALITE, optionsApe_get_compression_level_ape ());

	}
	else if (type_conv == WAVPACK_WAV_TO_WAVPACK) {

		New = g_list_append (New, g_strdup ("-y"));

		New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_WAVPACK_COMPRESSION, optionsWavpack_get_wavpack_compression ());

		New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_WAVPACK_SOUND, optionsWavpack_get_wavpack_sound ());

		if (*optionsWavpac_get_wavpack_hybride () != '\0') {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_WAVPACK_MODE_HYBRIDE, optionsWavpac_get_wavpack_hybride ());

			if (*optionsWavpack_get_wavpack_correction_file () != '\0') {
				New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_WAVPACK_FICHIER_CORRECTION, optionsWavpack_get_wavpack_correction_file ());
			}
			if (*optionsWavpack_get_wavpack_maximum_compression () != '\0') {
				New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_WAVPACK_COMPRESSION_MAXIMUM, optionsWavpack_get_wavpack_maximum_compression ());
			}
		}

		if (*optionsWavpack_get_wavpack_signature_md5 () != '\0') {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_WAVPACK_SIGNATURE_MD5, optionsWavpack_get_wavpack_signature_md5 ());
		}

		if (*optionsWavpack_get_wavpack_extra_encoding () != '\0') {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_WAVPACK_EXTRA_ENCODING, optionsWavpack_get_wavpack_extra_encoding ());
		}

		if (Config.BoolArtistTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ARTIST, "-w Artist=%a");
		}
		if (Config.BoolTitleTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_TITLE, "-w Title=%t");
		}
		if (Config.BoolAlbumTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_ALBUM, "-w Album=%l");
		}
		if (Config.BoolGenreTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_GENRE, "-w Genre=%G");
		}
		if (Config.BoolYearTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_YEAR, "-w Year=%d");
		}
		if (Config.BoolNumerateTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_NUMERATE, "-w Track=%N");
		}
		if (Config.BoolCommentTag) {
			New = OptionsInternal_add_options_for_visual (New, var_options.ColorLineCommand == COLOR_TAG_COMMENT, "-w Comment=%c");
		}
	}

	/* Construction du GSTRING */
	list = g_list_first (New);
	if (list) {
		gstr = g_string_new (NULL);
		while (list) {
			if (NULL != (ptr = (gchar*)list->data)) {
				g_string_append_printf (gstr, "%s ", ptr);
			}
			list = g_list_next(list);
		}
	}
	
	// SUPPRESSION DES ' '
	for (ptr = gstr->str; *ptr == ' '; ptr ++);
	str = g_strdup (ptr);
	g_string_free (gstr, TRUE);
	gstr = NULL;
	gstr = g_string_new (NULL);
	g_string_append_printf (gstr, "%s", str);
	g_free (str);
	str = NULL;
	
	/* Suppression du GLIST */
	list = g_list_first (New);
	while (list) {
		if (NULL != (ptr = (gchar*)list->data)) {
			g_free (ptr);
			ptr = list->data = NULL;
		}
		list = g_list_next(list);
	}
	g_list_free (New);
	New = NULL;

	return ((GString *)gstr);
}

// CONSTRUCTION DES OPTIONS INTERNES
// 
void OptionsInternal_set_datas_interne (COLOR_LINE_COMMAND ColorLineCommand, GtkWidget *widget, TYPE_CONV type_conv)
{
	GString     *gstr = NULL;

	if (NULL == widget) return;
	if (NULL == var_options.Adr_Widget_Oggenc_bitrate) return;
	if (NULL == var_options.Adr_Widget_Lame_Mode) return;
	if (NULL == var_options.Adr_Widget_Lame_abr_cbr_vbr) return;
	if (NULL == var_options.Adr_Widget_Lame_bitrate) return;
	
	gtk_label_set_markup (GTK_LABEL (widget), "");

	if (var_options.ColorLineCommand != COLOR_INIT) {
		var_options.ColorLineCommand = ColorLineCommand;
	}

	if ((gstr = OptionsInternal_get_options_for_visual (type_conv))) {

		gchar *str = g_strdup_printf ("<b>%s</b>", gstr->str);
		gtk_label_set_use_markup (GTK_LABEL (widget), TRUE);
		gtk_label_set_justify (GTK_LABEL (widget), GTK_JUSTIFY_LEFT);
		gtk_label_set_markup (GTK_LABEL (widget), str);
		gtk_widget_show (widget);

		g_string_free (gstr, TRUE);
		g_free (str);
		str = NULL;
	}
	
	var_options.ColorLineCommand = COLOR_NONE;
}
// RECUPERATION OPTIONS INTERNES MODIFIEE PAR L UTILISATEUR
// 
void on_entry_op_internal_changed (GtkEditable *editable, gpointer user_data)
{
	if (GTK_EDITABLE (editable) == GTK_EDITABLE (var_options.Adr_entry_flac_flac)) {
		g_free (Config.StringExpanderFlac);
		Config.StringExpanderFlac = NULL;
		Config.StringExpanderFlac = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY(editable)));
	} else if (GTK_EDITABLE (editable) == GTK_EDITABLE (var_options.Adr_entry_lame_mp3)) {
		g_free (Config.StringExpanderLame);
		Config.StringExpanderLame = NULL;
		Config.StringExpanderLame = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY(editable)));
	} else if (GTK_EDITABLE (editable) == GTK_EDITABLE (var_options.Adr_entry_oggenc_ogg)) {
		g_free (Config.StringExpanderOggenc);
		Config.StringExpanderOggenc = NULL;
		Config.StringExpanderOggenc = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY(editable)));
	} else if (GTK_EDITABLE (editable) == GTK_EDITABLE (var_options.Adr_entry_faac_m4a)) {
		g_free (Config.StringExpanderFaac);
		Config.StringExpanderFaac = NULL;
		Config.StringExpanderFaac = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY(editable)));
	} else if (GTK_EDITABLE (editable) == GTK_EDITABLE (var_options.Adr_entry_musepack_mpc)) {
		g_free (Config.StringExpanderMppenc);
		Config.StringExpanderMppenc = NULL;
		Config.StringExpanderMppenc = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY(editable)));
	} else if (GTK_EDITABLE (editable) == GTK_EDITABLE (var_options.Adr_entry_mac_ape)) {
		g_free (Config.StringExpanderMac);
		Config.StringExpanderMac = NULL;
		Config.StringExpanderMac = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY(editable)));
	} else if (GTK_EDITABLE (editable) == GTK_EDITABLE (var_options.Adr_entry_wavpack_wv)) {
		g_free (Config.StringExpanderWavpack);
		Config.StringExpanderWavpack = NULL;
		Config.StringExpanderWavpack = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY(editable)));
	}
}
// INIT
// 
void on_entry_lame_mp3_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_entry_lame_mp3 = widget;
	if (Config.StringExpanderLame)       gtk_entry_set_text (GTK_ENTRY(var_options.Adr_entry_lame_mp3), Config.StringExpanderLame);
}
void on_label_lame_mp3_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_label_lame_mp3 = widget;
}
void on_entry_oggenc_ogg_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_entry_oggenc_ogg = widget;
	if (Config.StringExpanderOggenc)     gtk_entry_set_text (GTK_ENTRY(var_options.Adr_entry_oggenc_ogg), Config.StringExpanderOggenc);
}
void on_label_oggenc_ogg_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_label_oggenc_ogg = widget;
}
void on_entry_flac_flac_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_entry_flac_flac = widget;
	if (Config.StringExpanderFlac)       gtk_entry_set_text (GTK_ENTRY(var_options.Adr_entry_flac_flac), Config.StringExpanderFlac);
}
void on_label_flac_flac_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_label_flac_flac = widget;
}
void on_entry_mac_ape_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_entry_mac_ape = widget;
	if (Config.StringExpanderMac)        gtk_entry_set_text (GTK_ENTRY(var_options.Adr_entry_mac_ape), Config.StringExpanderMac);
}
void on_label_mac_ape_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_label_mac_ape = widget;
}
void on_entry_wavpack_wv_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_entry_wavpack_wv = widget;
	if (Config.StringExpanderWavpack)    gtk_entry_set_text (GTK_ENTRY(var_options.Adr_entry_wavpack_wv), Config.StringExpanderWavpack);
}
void on_label_wavpack_wv_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_label_wavpack_wv = widget;
}
void on_entry_musepack_mpc_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_entry_musepack_mpc = widget;
	if (Config.StringExpanderMppenc)     gtk_entry_set_text (GTK_ENTRY(var_options.Adr_entry_musepack_mpc), Config.StringExpanderMppenc);
}
void on_label_musepack_mpc_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_label_musepack_mpc = widget;
}
void on_entry_faac_m4a_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_entry_faac_m4a = widget;
	if (Config.StringExpanderFaac)       gtk_entry_set_text (GTK_ENTRY(var_options.Adr_entry_faac_m4a), Config.StringExpanderFaac);
}
void on_label_faac_m4a_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_label_faac_m4a = widget;
}

