 /*
 *  file      : win_scan.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef win_scan_h
#define win_scan_h 1

typedef enum {
	WINDSCAN_PULSE = 0,
	WINDSCAN_PROGRESS
} TYPE_PROGRESS;

gboolean wind_scan_init( GtkWidget *FromWindMain, gchar *title, TYPE_PROGRESS TypeProgress );
// gboolean WindScan_open (gchar *title, TYPE_PROGRESS TypeProgress);
void	 WindScan_close (void);
void	 WindScan_set_pulse (void);
void	 WindScan_set_progress (gchar *p_str, double p_etat);
gboolean WindScan_close_request (void);
void	 WindScan_set_label_bar (gchar *p_str);
void	 WindScan_set_label (gchar *p_str);
void	 WindScan_hide_expander (void);
void	 WindScan_show_expander (void);
void	 WindScan_show_cancel( gboolean p_bool_show );

#endif

