 /*
 *  file      : split.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef split_h
#define split_h 1


#include "file.h"

typedef struct {											// DEFINITION D'UN POINT POUR LA LECTURE DU FICHIER WAV
	float		Max;										// Partie haute
	float		Min;										// Partie basse
} POINTS_FILE;


typedef struct {											// SPECIFICATION D'UN SELECTEUR
	gint	Nmr;											// Numero du selecteur
	float	BeginPaint;										// Position debut
	float	EndPaint;										// Position fin
	float	PercentBegin;									// Pourcentage debut
	float	PercentEnd;										// Pourcentage fin
} SELECTEUR;


typedef enum {
	_CURSOR_IS_NONE_ = 0,									// 
	_CURSOR_IS_LEFT_,										// 
	_CURSOR_IS_RIGHT_,										// 
	_CURSOR_IS_HAND_										// 
} TYPE_CURSOR;

/*
From Dzef:
Salut la bande,
Ouaip, 99 me semble être le bon choix : c'est le nombre de plages maximum
que l'on puisse trouver car c'est le nombre maximum que peuvent afficher les
afficheurs standards des players de salon...
Les CDs de 99 plages existent, c'est par exemple assez fréquent dans les
disques de bruitage...
*/
#define	MAX_SELECTEURS_SPLIT	100
#define	SPLIT_FILE_TMP_WAV	"/tmp/split_wav2sox.wav"
#define	SPLIT_FILE_TMP_WAV_SOX	"/tmp/split_wav2wavsox.wav"


typedef struct {
	GtkWidget		*AdrWidgetSpectre;						// Adr Widget Spectre
	GtkWidget		*AdrWidgetSpectreChrono;				// Adr Widget Spectre Chrono
	GtkWidget		*AdrWidgetSpectreTop;					// Adr Widget Spectre Chrono
	GtkButton		*Adr_button_destination;				// Adr du widget de la destinations des decoupes
	gint			NbrSelecteurs;							// Nombre total de selecteurs
	SELECTEUR		Selecteur [ MAX_SELECTEURS_SPLIT +2 ];	// Tableau des selecteurs
	gint			SelecteurActif;							// Le selecteur actif si NbrSelecteurs > -1
	gboolean		BoolReadFileSpectre;					// TRUE = BoolReadFileSpectre else NONE
	gdouble			TimeSongSec;							// Duree totale du fichier en secondes
	void			(*FuncExternBegin) (gdouble Percent);	// Rappel modif pointeur DEBUT
	void			(*FuncExternEnd) (gdouble Percent);		// Rappel modif pointeur FIN
	void			(*FuncExternWaitPlay) (void);			// Rappel selection d'ecoute
	void			(*FuncExternStopPlay) (void);			// Rappel fin d'ecoute
	gdouble			PercentActivePlay;						// Pointeur de lecture audio
	gboolean		BoolEventButtonPressSpectre;			// 
	POINTS_FILE		*Tab;									// Tableau des amplitudes du fichier actif
	glong			TotalAllocation;						// Longeur du tableau Tab
	gint			MaxPointsInTab;							// 
	glong			TotalAllocationTabScreen;				// Longeur du tableau TabScreen
	gshort			nBitsPerSample;							// 8 16 24 et 32
	gint			nTotalChunckSize;						// Taille des données
	gchar			*PathNameFile;							// Nom complet du fichier
	gchar			*PathNameFileReal;						// Nom reel du fichier
	TYPE_FILE_IS	TypeFileIs;								// WAV,  MP3, OGG, FLAC, SHN, WAVPACK
	
	INFO_WAV		*Tags;									// 
	guint			sec_in_time;
	
	gboolean		BoolInThread;							//
	guint			HandlerTimeoutDo;						//
	TYPE_CURSOR		TypeCursorSpectre;						// NONE | LEFT | RIGHT
	gboolean		BoolPlay;								// FALSE -> TRUE -> FALSE
	
	GtkAdjustment	*AdjScroll;								//
	GtkRange		*RangeAdjScroll;
	
	gboolean		BoolBlankWithCue;						// 
} VAR_SPLIT;


#define VARSPLIT_CHRONO_X	( 0 )
#define VARSPLIT_CHRONO_Y	( 0 )
#define VARSPLIT_CHRONO_W	( allocation.width - 1 )
#define VARSPLIT_CHRONO_H	( allocation.height - 1 )
	
#define VARSPLIT_SPECTRE_TOP_X	( 0 )
#define VARSPLIT_SPECTRE_TOP_Y	( 0 )
#define VARSPLIT_SPECTRE_TOP_W	( allocation.width - 1 )
#define VARSPLIT_SPECTRE_TOP_H	( allocation.height - 1 )
	
#define VARSPLIT_SPECTRE_X	( 0 )
#define VARSPLIT_SPECTRE_Y	( 0 )
#define VARSPLIT_SPECTRE_W	( allocation.width - 1 )
#define VARSPLIT_SPECTRE_H	( allocation.height - 1 )

#define VARSPLIT_SPECTRE_WITH	( allocation.width )


typedef struct {
	gfloat	value;											// 
	gfloat	lower;											// 
	gfloat	upper;											// 
	gfloat	step_increment;									// 
	gfloat	page_size;										// 
	gfloat	page_increment;									// 
	gint	with;											// 
	gint	mul;											// 
} ADJUST;

// 
// ---------------------------------------------------------------------------
//  SPLIT_SPECTRE.C
// ---------------------------------------------------------------------------
// 
gboolean	SplitSpectre_read_file_spectre (gchar *PathNameFile);
void		SplitSpectre_realize (GtkWidget *widget);
void		split_draw (void);
void		SplitSpectre_draw_chrono ( GtkWidget *widget, cairo_t *p_cr );
void		SplitSpectre_draw_top ( GtkWidget *widget, cairo_t *p_cr );
void		SplitSpectre_draw_lines (GtkWidget *widget, cairo_t *p_cr);
gint		SplitSpectre_get_with (void);
void		SplitSpectre_remove (void);
// 
// ---------------------------------------------------------------------------
//  SPLIT.C
// ---------------------------------------------------------------------------
// 
void		split_file_load_continue (gchar *p_PathNameFile);
void		split_load_from_dnd (GSList *p_list);
void		split_set_name_file (void);
void		split_play (void);
void		split_set_stop (void);
void		split_from_popup (gint p_choice);
void		split_set_flag_buttons (void);
// 
// ---------------------------------------------------------------------------
//  SPLIT_SELECTOR.C
// ---------------------------------------------------------------------------
// 
void		SplitSelector_init (void);
void		SplitSelector_cut (void);
void		SplitSelector_add (gint p_CursorX);
void		SplitSelector_set_pos_begin (gint p_begin);
void		SplitSelector_set_pos_end (gint p_end);
void		SplitSelector_get_pos (void);
gint		SplitSelector_get_pos_begin (gint p_Sel);
gint		SplitSelector_get_pos_end (gint p_Sel);
gdouble		SplitSelector_get_percent_begin (gint p_Sel);
gdouble		SplitSelector_get_percent_end (gint p_Sel);
gint		SplitSelector_get_4_secondes_to_int (void);
gdouble		SplitSelector_get_percent_for_x_secondes (gint p_secondes);
gboolean	SplitSelector_cursor_in_box_play (gint p_cursor_x, gint p_cursor_y);
gboolean	SplitSelector_cursor_in_line_play (gint p_cursor_x, gint p_cursor_y);
gint		SplitSelector_get_pos_play (void);
void		SplitSelector_set_pos_play (gint p_play);
gint		SplitSelector_get_diff_sec (gdouble p_PercentBegin, gdouble p_PercentEnd);
// 
// ---------------------------------------------------------------------------
//  SPLIT_CUE.C
// ---------------------------------------------------------------------------
// 
gchar		*SplitCue_read_cue_file (gchar *p_pathname);
// 
// ---------------------------------------------------------------------------
//  SPLIT_CONV.C
// ---------------------------------------------------------------------------
// 
void		SplitConv_to (gchar *p_PathNameFile);
// 
// ---------------------------------------------------------------------------
//  SPLIT_WAV.C
// ---------------------------------------------------------------------------
// 
void		SplitWav_extract( void );


#endif


