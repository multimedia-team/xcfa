 /*
 *  file      : split.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "fileselect.h"
#include "win_info.h"
#include "cursor.h"
#include "dragNdrop.h"
#include "get_info.h"
#include "tags.h"
#include "win_scan.h"
#include "popup.h"
#include "mplayer.h"
#include "alsa_play.h"
#include "prg_init.h"
#include "split.h"
#include "statusbar.h"



extern gint		 n_drag_types;			// Drag And Drop
VAR_SPLIT	VarSplit;
ADJUST		Adjust;

void SetAjust (void);

// 
// PERCENT = ( VALUE_X / LONGUEUR_TOTALE ) * 100.0
// VALUE_X = ( LONGUEUR_TOTALE * PERCENT ) / 100.0
// 
void split_set_time (void)
{
	gint		sec;
	// gdouble		dsec;
	gdouble		TimeSongSec;
	gdouble		Percent;
	gint		H, M, S;
	// gint		C, hundr;
	gchar		*Str = NULL;

	if (NULL == VarSplit.Tags || VarSplit.NbrSelecteurs <= 0) {
		gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_begin_time_split")), TRUE);
		gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_begin_time_split")),
					" 00:00:00 ");
		
		gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_end_time_split")), TRUE);
		gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_end_time_split")),
					" 00:00:00 ");
		
		gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_time_split")), TRUE);
		gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_time_split")),
					" 00:00:00 ");
		
		gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_nmr_plage_split")), TRUE);
		gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_nmr_plage_split")), " Plage <b>-- / --</b> ");
	
		// 
		gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_curseur_lecture_split")), TRUE);
		gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_curseur_lecture_split")), " 00:00:00 ");
		return;
	}
	
	// ACQUISITION DUREE TOTALE DU FICHIER EN SECONDES
	TimeSongSec = VarSplit.Tags->SecTime;
	
	// POSITION INDEX DEBUT
	Percent = SplitSelector_get_percent_begin (VarSplit.SelecteurActif);
	sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
	// dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
	// hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
	H = (sec / 60) / 60;
	M = (sec / 60) % 60;
	S = sec % 60;
	// C = hundr;
	Str = g_strdup_printf (" <b>%02d:%02d:%02d</b> ", H, M, S);
	gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_begin_time_split")), TRUE);
	gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_begin_time_split")), Str);
	g_free (Str);  Str = NULL;

	// POSITION INDEX FIN
	Percent = SplitSelector_get_percent_end (VarSplit.SelecteurActif);
	sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
	// dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
	// hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
	H = (sec / 60) / 60;
	M = (sec / 60) % 60;
	S = sec % 60;
	// C = hundr;
	Str = g_strdup_printf (" <b>%02d:%02d:%02d</b> ", H, M, S);
	gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_end_time_split")), TRUE);
	gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_end_time_split")), Str);
	g_free (Str);  Str = NULL;

	// NUMERO DE PLAGE EN COURS
	gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_nmr_plage_split")), TRUE);
	Str = g_strdup_printf (" Plage <b>%02d / %02d</b> ", VarSplit.SelecteurActif +1, VarSplit.NbrSelecteurs);
	gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_nmr_plage_split")), Str);
	g_free (Str);  Str = NULL;
	
	// DUREE DE LA PLAGE
	Percent  = SplitSelector_get_percent_end (VarSplit.SelecteurActif) - SplitSelector_get_percent_begin (VarSplit.SelecteurActif);
	sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
	// dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
	// hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
	H = (sec / 60) / 60;
	M = (sec / 60) % 60;
	S = sec % 60;
	// C = hundr;
	Str = g_strdup_printf (" <b>%02d:%02d:%02d</b> ", H, M, S);
	gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_time_split")), TRUE);
	gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_time_split")), Str);
	g_free (Str);  Str = NULL;
	
	// TEMPS DU PLAY
	Percent = VarSplit.PercentActivePlay;
	sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
	// dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
	// hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
	H = (sec / 60) / 60;
	M = (sec / 60) % 60;
	S = sec % 60;
	// C = hundr;
	Str = g_strdup_printf (" <b>%02d:%02d:%02d</b> ", H, M, S);
	gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_curseur_lecture_split")), TRUE);
	gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_curseur_lecture_split")), Str);
	g_free (Str);  Str = NULL;
}
// 
// 
void split_set_flag_buttons (void)
{
	gboolean	Bool [ 3 ];
	
	Bool [ 0 ] = VarSplit.BoolReadFileSpectre;
	Bool [ 1 ] = VarSplit.NbrSelecteurs > 1 && !VarSplit.BoolPlay ? TRUE : FALSE;
	Bool [ 2 ] = VarSplit.NbrSelecteurs > 0 && !VarSplit.BoolPlay? TRUE : FALSE;

	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_cut_split")),	Bool [ 1 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_begin_split")),	Bool [ 2 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_split")),		Bool [ 2 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_end_split")),	Bool [ 2 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_gen_cue_split")),	Bool [ 2 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_action_split")),		Bool [ 2 ]);

	gtk_widget_grab_focus (VarSplit.AdrWidgetSpectre);
	
	split_set_time ();
	split_set_name_file ();
	StatusBar_puts();
}
// 
// 
void SplitSpectre_scroll_droite_play (void)
{
	gdouble			Value;
	gint			Begin;
	gint			LenScroll;
	GtkAllocation	allocation;
	
	if (VarSplit.NbrSelecteurs == 0) return;
	SplitSelector_get_pos ();
	Value = gtk_adjustment_get_value( VarSplit.AdjScroll );
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
	LenScroll = allocation.width -1;
	Begin     = SplitSelector_get_pos_play ()  - (gint)Value;
		
	while( Begin > 100 && Value + LenScroll < SplitSpectre_get_with() ) {
		Begin --;
		Value ++;
	}
	if (Value < 0.0) {
		Value = 0.0;
	}
	Adjust.value = Value;
	SetAjust ();
}

// DEMANDE DE REDRAW
// 
void split_redraw_image (void)
{
	/**
	GtkAllocation	allocation;
	
	if (NULL == VarSplit.AdrWidgetSpectre) return;
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
	gtk_widget_queue_draw_area (
				VarSplit.AdrWidgetSpectre,
				allocation.x,
				allocation.y,
				allocation.width,
				allocation.height
				);
	*/
	if (NULL == VarSplit.AdrWidgetSpectre) return;
	gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
}
// 
// 
static void split_drag_data_received_file(
					GtkWidget        *widget,
					GdkDragContext   *context,
					gint              x,
					gint              y,
					GtkSelectionData *data,
					guint             info,
					guint             time,
					gpointer          user_data)
{
	/**
	if( TRUE == VarSplit.BoolPlay ) {
		wind_info_init (
			WindMain,
			_("Veuillez arreter la lecture du fichier"),
			  "\n",
			_("avant d'impôrter un fichier."),
			  "");
		return;
	}
	*/
	if (gtk_drag_get_source_widget(context) != widget) {
		// dragndrop_list_drag_data (widget, (gchar *)data->data);
		dragndrop_list_drag_data( widget, (gchar*)gtk_selection_data_get_data( data ));
	}
}
// 
// 
void split_set_name_file (void)
{
	gchar	*Ptr = NULL;
	gchar	*NewName = NULL;
	
	if (NULL != VarSplit.PathNameFileReal && NULL != (Ptr = strrchr (VarSplit.PathNameFileReal, '/'))) {
		gchar	*Str = NULL;
		
		Ptr ++;
		NewName = utf8_eperluette_name( Ptr );
		if (NULL != VarSplit.Tags) {
			Str = g_strdup_printf ("%s      %s\n%s Hertz\n%s voies\n%s bits ",
						NewName, VarSplit.Tags->time, VarSplit.Tags->hertz, VarSplit.Tags->voie, VarSplit.Tags->bits);
		}
		else {
			Str = g_strdup_printf ("<b>%s</b>", NewName);
		}
		g_free( NewName );	NewName = NULL;
		
		gtk_label_set_use_markup (GTK_LABEL(GLADE_GET_OBJECT("label_temps_actuel_curseur_split")), TRUE);
		gtk_label_set_markup (GTK_LABEL(GLADE_GET_OBJECT("label_temps_actuel_curseur_split")), Str);
		
		g_free (Str);	Str = NULL;
		
		if (VarSplit.NbrSelecteurs > 1) {
			StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_SIMPLE_, _("Menu: right click / Adding track: Click the center button  /  Zoom: Ctrl+Scroll") );
		}
		else {
			StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_SIMPLE_, _("Adding track: Click the center button  /  Zoom: Ctrl+Scroll") );
		}
	}
	else {
		StatusBar_set_mess( NOTEBOOK_SPLIT, _STATUSBAR_SIMPLE_, _("<i>Ready for imports and a CUE file, WAV, FLAC, OGG, MP3, APE, WMA</i>") );
	}
}
//
// 
static void VarSplit_thread (void *arg)
{
	VarSplit.BoolInThread = TRUE;
	
	SplitSpectre_remove();
	VarSplit.Tags = (INFO_WAV *)tagswav_get_info (VarSplit.PathNameFile);
	SplitSpectre_read_file_spectre (VarSplit.PathNameFile);
	
	VarSplit.BoolInThread = FALSE;
	PRINT("FIN THREAD SPLIT");
	pthread_exit(0);
}
// 
// 
static gint VarSplit_timeout (gpointer data)
{
	if (FALSE == VarSplit.BoolInThread) {
		gint	IndicePoints = 0;
				
		g_source_remove (VarSplit.HandlerTimeoutDo);
		
		// VarSplit.Tags = tagswav_remove_info (VarSplit.Tags);
		// VarSplit.Tags = (INFO_WAV *)tagswav_get_info (VarSplit.PathNameFile);
		
		// 
		split_set_name_file ();

		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print("\ttime     : Temps d'ecoute             = %s\n", VarSplit.Tags->time);
			g_print("\tSecTime  : Temps d'ecoute en secondes = %d\n", VarSplit.Tags->SecTime);
			g_print("\thertz    : Taux d'échantillonnage     = %s\n", VarSplit.Tags->hertz);
			g_print("\tvoie     : 1, 2, 4, 6                 = %s\n", VarSplit.Tags->voie);
			g_print("\tbits     : 8, 16, 24, 32 ou 64        = %s\n", VarSplit.Tags->bits);
			g_print("\n");
		}
		
		for (IndicePoints = 0; IndicePoints < MAX_SELECTEURS_SPLIT; IndicePoints ++) {
			if (-1 != VarSplit.Selecteur [ IndicePoints ] . Nmr) {
				VarSplit.NbrSelecteurs ++;
			}
		}
		
		SplitSelector_get_pos ();
			
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		
		split_set_flag_buttons ();
		split_set_time ();
		VarSplit.PercentActivePlay = 0.0;
		gtk_widget_queue_draw (VarSplit.AdrWidgetSpectre);
		WindScan_close ();
		PRINT("FIN TIMEOUT SPLIT");
	}
	return (TRUE);
}
// 
// IMPORT D'UN FICHIER
// 
void split_file_load( gchar *p_PathNameFile )
{
	gchar		*PathNameFile = NULL;
	TYPE_FILE_IS	TypeFileIs = GetInfo_file_is (p_PathNameFile);

printf("\n---------------------------- BEGIN split_file_load( %s\n", p_PathNameFile );

	if (0 == strcmp (p_PathNameFile, SPLIT_FILE_TMP_WAV)) {
		wind_info_init (
			WindMain,
			_("This file belongs XCFA"),
			_("Select another file."),
			  "");
		return;
	}
	
	if (libutils_get_size_file (p_PathNameFile) <= 0) {
		wind_info_init (
			WindMain,
			_("Empty file"),
			_("The file contains no data !!!"),
			  "");
		return;
	}
	
	// INIT STRUCTURES: Selecteur
	SplitSelector_init ();
	
	if (TRUE == FileIs_g_str_has_suffix (p_PathNameFile, ".CUE")) {
		
		PRINT("FILE IS CUE  ;-)");
		
		if (NULL == (PathNameFile = SplitCue_read_cue_file (p_PathNameFile))) {
			g_print ("\tBAD FORMAT OF CUE FILE :/\n");
			return;
		}
		if (0 == strcmp (PathNameFile, SPLIT_FILE_TMP_WAV)) {
			wind_info_init (
				WindMain,
				_("This file belongs XCFA"),
				_("Select another file."),
				  "");
			g_free (PathNameFile);
			PathNameFile = NULL;
			return;
		}
		
		VarSplit.BoolBlankWithCue = TRUE;
	}
	else {
		PathNameFile = g_strdup (p_PathNameFile);
	}
	
	if (FILE_IS_WAV == TypeFileIs ||
	    FILE_IS_FLAC == TypeFileIs ||
	    FILE_IS_MP3 == TypeFileIs ||
	    FILE_IS_OGG == TypeFileIs ||
	    FILE_IS_APE == TypeFileIs ||
	    FILE_IS_WMA== TypeFileIs
	    ) {
		
		if (FALSE == PrgInit.bool_sox || FALSE == PrgInit.bool_mplayer) {
		
			wind_info_init (
				WindMain,
				_("Sox and Mplayer not found"),
				_("Sox et Mplayer are not found"),
			  	"\n",
				_("in your configuration."),
			  	"\n\n",
				_("Please install it and resume"),

				"");
			return;
		}
	}
	
	if (NULL != VarSplit.PathNameFileReal) {
		g_free (VarSplit.PathNameFileReal);
		VarSplit.PathNameFileReal = NULL;
	}
	VarSplit.PathNameFileReal = g_strdup (p_PathNameFile);
	
	SplitConv_to (PathNameFile);
printf("\n---------------------------- END split_file_load( %s\n", p_PathNameFile );
}
void split_file_load_continue( gchar *p_PathNameFile )
{
	pthread_t	nmr_tid;
	gchar		*PathNameFile = p_PathNameFile;
	TYPE_FILE_IS	TypeFileIs = libutils_test_file_exist (p_PathNameFile);
	
printf("---------------------------- BEGIN split_file_load_continue( %s\n", p_PathNameFile );

	if (TRUE == libutils_test_file_exist (PathNameFile) &&
	    (FILE_IS_WAV == TypeFileIs || FILE_IS_FLAC == TypeFileIs || FILE_IS_MP3 == TypeFileIs || FILE_IS_OGG == TypeFileIs || FILE_IS_APE == TypeFileIs)) {
		
		AlsaPlay_stop ();
		
		// REAJUSTEMENT DU ZOOM A 100%
		Adjust.mul = 1;
		SetAjust() ;
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreChrono );
		
		WindScan_set_pulse ();
		WindScan_set_label ("<b><i>Analyse du fichier ...</i></b>");
		
		if (NULL != VarSplit.PathNameFile) {
			g_free (VarSplit.PathNameFile);
			VarSplit.PathNameFile = NULL;
		}
		VarSplit.PathNameFile = g_strdup (PathNameFile);

		VarSplit.BoolInThread = TRUE;
		PRINT("DEBUT TIMEOUT SPLIT");
		PRINT("DEBUT THREAD SPLIT");
		VarSplit.HandlerTimeoutDo = g_timeout_add (100, VarSplit_timeout, 0);
		pthread_create (&nmr_tid, NULL ,(void *)VarSplit_thread, (void *)NULL);
	}
	else {
		wind_info_init (
			WindMain,
			_("Error file select"),
			_("Please select a file type of MP3 WAV OGG FLAC APE"),
			  "");
	}
	split_set_flag_buttons ();
printf("---------------------------- END   split_file_load_continue( %s\n", p_PathNameFile );
}

	
// IMPORT D'UN FICHIER
// 
void split_maj_file_load (gchar *path)
{
	gchar		*Ptr = NULL;
	TYPE_FILE_IS	TypeFileIs = FILE_IS_NONE;
	gboolean	BoolTypeFileIsOk = FALSE;
	
	g_free (Config.PathLoadSplit);
	Config.PathLoadSplit = NULL;
	Config.PathLoadSplit = g_strdup (path);
	if (NULL != (Ptr = strrchr (Config.PathLoadSplit, '/'))) {
		*Ptr = '\0';
	}

	TypeFileIs = GetInfo_file_is (path);
	if (FILE_IS_WAV == TypeFileIs ||
	    FILE_IS_FLAC == TypeFileIs ||
	    FILE_IS_MP3 == TypeFileIs ||
	    FILE_IS_OGG == TypeFileIs ||
	    FILE_IS_APE == TypeFileIs ||
	    FILE_IS_WMA == TypeFileIs ||
	    TRUE == FileIs_g_str_has_suffix (path, ".CUE")) {
		split_file_load( path );
		BoolTypeFileIsOk = TRUE;
	}
	
	if (FALSE == BoolTypeFileIsOk) {
		wind_info_init (
			WindMain,
			_("Wrong type of file"),
			_("File types accepted\nsare: WAV FLAC MP3 OGG APE WMA CUE"),
			  "");
	}
}
// 
// 
void on_button_importer_split_clicked (GtkButton *button, gpointer user_data)
{
	if (GDK_KEY_space != keys.keyval)
		fileselect_create (_PATH_LOAD_SPLIT_FILE_, Config.PathLoadSplit, split_maj_file_load);
}
// INIT WIDGET DE LA DESTINAION DES DECOUPES
// 
void on_button_destination_split_realize (GtkWidget *widget, gpointer user_data)
{
	VarSplit.Adr_button_destination = GTK_BUTTON (widget);
	gtk_button_set_use_underline (GTK_BUTTON (VarSplit.Adr_button_destination), FALSE);
	gtk_button_set_label (GTK_BUTTON (VarSplit.Adr_button_destination), Config.PathDestinationSplit);
}
// CHOIX D'UNE DESTINATION POUR LES DECOUPES
// 
void split_maj_destination (gchar *path)
{
	if (libutils_test_write (path) == TRUE) {
		g_free (Config.PathDestinationSplit);
		Config.PathDestinationSplit = NULL;
		Config.PathDestinationSplit = g_strdup (path);
		gtk_button_set_label (GTK_BUTTON (VarSplit.Adr_button_destination), Config.PathDestinationSplit);
	}
}
// 
// 
void on_button_destination_split_clicked (GtkButton *button, gpointer user_data)
{
	if (GDK_KEY_space != keys.keyval) {
		fileselect_create (_PATH_CHOICE_DESTINATION_, Config.PathDestinationSplit, split_maj_destination);
		split_set_flag_buttons ();
	}
}
// LOAD DEPUIS UN DRAG AND DROP
// 
void split_load_from_dnd (GSList *p_list)
{
	GSList		*gs_List = p_list;
	gchar		*Ptr = NULL;
	TYPE_FILE_IS	TypeFileIs = FILE_IS_NONE;
	gboolean	BoolTypeFileIsOk = FALSE;
	
	while (NULL != gs_List) {
		if (NULL != (Ptr = (gchar *)gs_List->data)) {
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print("\t%s\n", Ptr);
			TypeFileIs = GetInfo_file_is (Ptr);
			if (FILE_IS_WAV == TypeFileIs ||
			    FILE_IS_FLAC == TypeFileIs ||
			    FILE_IS_MP3 == TypeFileIs ||
			    FILE_IS_OGG == TypeFileIs ||
			    FILE_IS_APE == TypeFileIs ||
	 		    FILE_IS_WMA == TypeFileIs ||
			    TRUE == FileIs_g_str_has_suffix (Ptr, ".CUE")) {
				split_file_load( Ptr );
				BoolTypeFileIsOk = TRUE;
				break;
			}
		}
		gs_List = g_slist_next (gs_List);
	}
	
	if (FALSE == BoolTypeFileIsOk) {
		wind_info_init (
			WindMain,
			_("Wrong type of file"),
			_("File types accepted\nsare: WAV FLAC MP3 OGG APE WMA CUE"),
			  "");
	}
	
	split_set_flag_buttons ();
	split_set_time ();
}
// 
// 
void split_set_value (gdouble p_value)
{
	GtkAllocation	allocation;
	gdouble			Value;
	
	Value = gtk_adjustment_get_value( VarSplit.AdjScroll );
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
	
	VarSplit.PercentActivePlay = p_value;
	if (VarSplit.PercentActivePlay >= 100.0) VarSplit.PercentActivePlay = 99.99;

	if( SplitSelector_get_pos_play () > VARSPLIT_SPECTRE_WITH + (gint)Value &&
	    (gint)Value < SplitSpectre_get_with() ) {
		SplitSpectre_scroll_droite_play ();
	}
	split_set_time ();
	gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
	gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
}
// 
// 
void split_end_play (void)
{
	split_set_stop ();
	
	gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("button_play_split")));
	gtk_widget_hide (GTK_WIDGET (GLADE_GET_OBJECT("button_pause_split")));
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_begin_split")),	TRUE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_split")),		TRUE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_end_split")),	TRUE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_cut_split")),	TRUE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_gen_cue_split")), 	TRUE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_action_split")), 	TRUE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_importer_split")), 	TRUE);
}
// 
// 
void split_set_stop (void)
{
	VarSplit.BoolPlay = FALSE;
}
// 
// 
gboolean split_is_stop (void)
{
	return (VarSplit.BoolPlay ? FALSE : TRUE);
}
// DEBUT DE LECTURE
// 
void on_button_play_split_clicked (GtkButton *button, gpointer user_data)
{
	VarSplit.BoolPlay = TRUE;
	
	gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("button_pause_split")));
	gtk_widget_hide (GTK_WIDGET (GLADE_GET_OBJECT("button_play_split")));
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_begin_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_end_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_cut_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_importer_split")), 	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_gen_cue_split")), 	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_action_split")), 	FALSE);
	
	AlsaPlay_song (
		VarSplit.PathNameFile,
		VarSplit.PercentActivePlay,
		100.0,
		split_set_value,
		split_end_play
		);
}
// FIN DE LECTURE
// 
void on_button_pause_split_clicked (GtkButton *button, gpointer user_data)
{
	VarSplit.BoolPlay = FALSE;
	
	AlsaPlay_stop ();
	gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("button_play_split")));
	gtk_widget_hide (GTK_WIDGET (GLADE_GET_OBJECT("button_pause_split")));
}
// FROM BARRE ESPACE
// 
void split_play (void)
{
	if (VarSplit.NbrSelecteurs > 0) {
		if (FALSE == VarSplit.BoolPlay) {
			on_button_play_split_clicked (NULL, NULL);
		}
		else {
			on_button_pause_split_clicked (NULL, NULL);
		}
	}
}

// LECTURE 5 SECONDES DEBUT
// 
void on_button_play_begin_split_clicked (GtkButton *button, gpointer user_data)
{
	gdouble	PercentBegin = SplitSelector_get_percent_begin (VarSplit.SelecteurActif);
	gdouble	PercentEnd   = SplitSelector_get_percent_begin (VarSplit.SelecteurActif) + SplitSelector_get_percent_for_x_secondes (5);
	
	VarSplit.BoolPlay = TRUE;
	SplitSelector_get_percent_begin (VarSplit.SelecteurActif);
	AlsaPlay_song (
		VarSplit.PathNameFile,
		PercentBegin,
		PercentEnd,
		split_set_value,
		split_end_play
		);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_begin_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_split")),		FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_end_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_cut_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_gen_cue_split")), 	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_action_split")), 	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_importer_split")), 	FALSE);
}
// LECTURE 5 SECONDES FIN
// 
void on_button_play_end_split_clicked (GtkButton *button, gpointer user_data)
{
	gdouble	PercentBegin = SplitSelector_get_percent_end (VarSplit.SelecteurActif) - SplitSelector_get_percent_for_x_secondes (5);
	gdouble	PercentEnd   = SplitSelector_get_percent_end (VarSplit.SelecteurActif);
	
	VarSplit.BoolPlay = TRUE;
	SplitSelector_get_percent_begin (VarSplit.SelecteurActif);
	AlsaPlay_song (
		VarSplit.PathNameFile,
		PercentBegin,
		PercentEnd,
		split_set_value,
		split_end_play
		);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_begin_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_split")),		FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_play_end_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_cut_split")),	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_gen_cue_split")), 	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_action_split")), 	FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_importer_split")), 	FALSE);
}
// 
// 
void split_from_popup (gint p_choice)
{
	// PRINT_FUNC_LF();
	switch (p_choice) {
	// Selecteur suivant
	case 0 :
		if (VarSplit.NbrSelecteurs -1 == VarSplit.SelecteurActif)
			VarSplit.SelecteurActif = 0;
		else	VarSplit.SelecteurActif ++;
		gtk_widget_queue_draw (VarSplit.AdrWidgetSpectre);
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		break;
	// Selecteur precedant
	case 1 :
		if (VarSplit.SelecteurActif == 0)
			VarSplit.SelecteurActif = VarSplit.NbrSelecteurs -1;
		else	VarSplit.SelecteurActif --;
		gtk_widget_queue_draw (VarSplit.AdrWidgetSpectre);
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		break;
	// Supprimer le selecteur actif
	case 2 :
		SplitSelector_cut ();
		gtk_widget_queue_draw (VarSplit.AdrWidgetSpectre);
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		break;
	}
}
// 
// 
void SetAjust (void)
{
	if( NULL == VarSplit.AdjScroll ) return;
	gtk_adjustment_set_value( VarSplit.AdjScroll, Adjust.value );
	gtk_adjustment_set_lower( VarSplit.AdjScroll, Adjust.lower );
	gtk_adjustment_set_upper( VarSplit.AdjScroll, Adjust.upper );
	gtk_adjustment_set_step_increment( VarSplit.AdjScroll, Adjust.step_increment );
	gtk_adjustment_set_page_increment( VarSplit.AdjScroll, Adjust.page_increment );
	gtk_adjustment_set_page_size( VarSplit.AdjScroll, Adjust.page_size );
	
	gtk_adjustment_value_changed( VarSplit.AdjScroll );
}
// 
// 
void page_plus_scrolled ( gint p_CursorX )
{
	gdouble			Value;
	GtkAllocation	allocation;

	Value = gtk_adjustment_get_value( VarSplit.AdjScroll );
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );

	Adjust.mul           += 1;
	Adjust.with           = SplitSpectre_get_with();
	Adjust.lower          = 0.;
	Adjust.upper          = Adjust.with;
	Adjust.step_increment = 10;
	Adjust.page_size      = allocation.width;
	Adjust.page_increment = allocation.width;
	Adjust.value          = Value + (gdouble)p_CursorX;

	SetAjust();
	SetAjust();
}
// 
// 
void rigtht_plus_scrolled (void)
{
	gdouble			Value;
	GtkAllocation	allocation;

	Value = gtk_adjustment_get_value( VarSplit.AdjScroll );
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );

	// Adjust.mul           += 1;
	Adjust.with           = SplitSpectre_get_with();
	Adjust.lower          = 0.;
	Adjust.upper          = Adjust.with;
	Adjust.step_increment = 10;
	Adjust.page_size      = allocation.width;
	Adjust.page_increment = allocation.width;
	Adjust.value          = Value;
	Adjust.value         += 10;
	while( Adjust.value > Adjust.with - allocation.width )
		Adjust.value --;
	SetAjust ();
}
// 
// 
void page_moins_scrolled ( gint p_CursorX )
{
	gdouble			Value;

	Value = gtk_adjustment_get_value( VarSplit.AdjScroll );
	GtkAllocation	allocation;

	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );

	Adjust.mul           -= 1;
	if( Adjust.mul < 1 ) Adjust.mul = 1;
	Adjust.with           = SplitSpectre_get_with();
	if( Adjust.with < allocation.width ) {
		Adjust.with   = allocation.width;
		Adjust.value  = 0;
	} else {
		Adjust.value          = Value - p_CursorX;
	}
	while( Adjust.value > Adjust.with - allocation.width )
		Adjust.value --;
	if( Adjust.value < 0. ) Adjust.value = 0.;
	Adjust.lower          = 0.;
	Adjust.upper          = Adjust.with;
	Adjust.step_increment = 10;
	Adjust.page_size      = allocation.width;
	Adjust.page_increment = allocation.width;
	SetAjust ();
}
// 
// 
void left_plus_scrolled (void)
{
	gdouble			Value;
	GtkAllocation	allocation;

	Value = gtk_adjustment_get_value( VarSplit.AdjScroll );
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );

	// Adjust.mul           += 1;
	Adjust.with           = SplitSpectre_get_with();
	Adjust.lower          = 0.;
	Adjust.upper          = Adjust.with;
	Adjust.step_increment = 10;
	Adjust.page_size      = allocation.width;
	Adjust.page_increment = allocation.width;
	Adjust.value          = Value;
	Adjust.value         -= 10;
	while( Adjust.value < 0.0 )
		Adjust.value = 0.0;
	SetAjust ();
}

// CHRONO
// 
void on_image_splitspectre_chrono_realize (GtkWidget *widget, gpointer user_data)
{
	VarSplit.AdrWidgetSpectreChrono = widget;
}
// 
// 
gboolean on_eventbox_splitspectre_chrono_draw(  GtkWidget *widget, cairo_t *p_cr, gpointer user_data )
{
	SplitSpectre_draw_chrono( VarSplit.AdrWidgetSpectreChrono, p_cr );
	return FALSE;
}
// 
// 
gboolean on_eventbox_splitspectre_chrono_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gint			CursorX, CursorY;
	GdkModifierType		state;
	GtkAllocation	allocation;
	
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );	
	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &CursorX, &CursorY, &state );
 	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &CursorX, &CursorY, &state);	
	
	// SI LE CURSEUR N EST PAS DANS LA ZONE ALORS SORTIE
	if (CursorX < VARSPLIT_CHRONO_X) return FALSE;
	if (CursorX > VARSPLIT_CHRONO_X + VARSPLIT_CHRONO_W) return FALSE;
	if (CursorY < VARSPLIT_CHRONO_Y) return FALSE;
	if (CursorY > VARSPLIT_CHRONO_Y + VARSPLIT_CHRONO_H) return FALSE;
	
	gtk_widget_queue_draw (VarSplit.AdrWidgetSpectreChrono);
	return FALSE;
}
// TOP
// 
void on_image_splitspectre_top_realize (GtkWidget *widget, gpointer user_data)
{
	VarSplit.AdrWidgetSpectreTop = widget;
}
// 
// 
gboolean on_eventbox_splitspectre_top_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gboolean		BoolEventTypeScroll = (GDK_SCROLL == event->type) ? TRUE : FALSE;
	gint			EventTypeScrollDirection = ((GdkEventScroll*)event)->direction;
	gint			CursorX, CursorY;
	GdkModifierType		state;
	// gboolean		bool_click_droit = (((GdkEventButton*)event)->button == 3);
	// gboolean		bool_click_centre = (((GdkEventButton*)event)->button == 2);
	gboolean		bool_click_gauche = (((GdkEventButton*)event)->button == 1);
	gdouble			Value;
	Value = gtk_adjustment_get_value( VarSplit.AdjScroll );
	GtkAllocation	allocation;

	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );

	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &CursorX, &CursorY, &state );
 	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &CursorX, &CursorY, &state);	
	
	// UN CLICK BOUTON SOURIS : PRESSED
	if (TRUE == bool_click_gauche && event->type == GDK_BUTTON_PRESS) {
		gint Pos;
		CursorX += (gint)Value;
		for (Pos = 0; Pos < VarSplit.NbrSelecteurs ; Pos ++) {
			if (CursorX > SplitSelector_get_pos_begin (Pos) && CursorX < SplitSelector_get_pos_end (Pos)) {
				VarSplit.SelecteurActif = Pos;
				split_set_time ();
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
				break;
			}
		}
	}
	else if( TRUE == BoolEventTypeScroll ) {
		
		if( GDK_SCROLL_UP == EventTypeScrollDirection || GDK_SCROLL_DOWN == EventTypeScrollDirection ) {
					
			if( GDK_SCROLL_UP == EventTypeScrollDirection ) {
				if (VarSplit.SelecteurActif  < VarSplit.NbrSelecteurs -1) {
					VarSplit.SelecteurActif ++;
				}
			} else if( GDK_SCROLL_DOWN == EventTypeScrollDirection ) {
				if (VarSplit.SelecteurActif > 0) {
					VarSplit.SelecteurActif --;
				}
			}
			
			gint	BeginPos = SplitSelector_get_pos_begin (VarSplit.SelecteurActif);
			gint	EndPos   = SplitSelector_get_pos_end (VarSplit.SelecteurActif);
			gint	LenPos   = EndPos - BeginPos;
			gint	LenWin	 = VARSPLIT_SPECTRE_WITH;
			gint	demi;

			// LA SELECTION EST PLUS PETITE QUE LA FENETRE
			if( LenPos < LenWin ) {
				demi = ( LenWin - LenPos ) / 2;
				Adjust.value = BeginPos - demi;
			}
			// LA SELECTION EST PLUS GRANDE QUE LA FENETRE
			else if( LenPos > LenWin ) {
				Adjust.value = BeginPos - 50;
			}
			while( Adjust.value + LenWin > SplitSpectre_get_with() )
				Adjust.value --;
			if( Adjust.value < 0.0 ) Adjust.value = 0.0;
			
			SetAjust();
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		}
	}
	else {
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreChrono );
	}

	return FALSE;
}
// 
// 
gboolean on_eventbox_splitspectre_top_draw( GtkWidget *widget, cairo_t *p_cr, gpointer user_data )
{
	SplitSpectre_draw_top( VarSplit.AdrWidgetSpectreTop, p_cr );
	return FALSE;
}

void split_realize( void )
{
	if( NULL != VarSplit.RangeAdjScroll && NULL != VarSplit.AdrWidgetSpectre ) {

		GtkAllocation	allocation;
		gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
		
		VarSplit.AdjScroll    = gtk_range_get_adjustment( GTK_RANGE(VarSplit.RangeAdjScroll) );
		Adjust.value          = gtk_adjustment_get_value( VarSplit.AdjScroll );
		Adjust.lower          = gtk_adjustment_get_lower( VarSplit.AdjScroll );
		Adjust.upper          = gtk_adjustment_get_upper( VarSplit.AdjScroll );
		Adjust.step_increment = gtk_adjustment_get_step_increment( VarSplit.AdjScroll );
		Adjust.page_size      = gtk_adjustment_get_page_size( VarSplit.AdjScroll );
		Adjust.page_increment = gtk_adjustment_get_page_increment( VarSplit.AdjScroll );
		Adjust.with           = allocation.width;
		Adjust.mul            = 1;
	}
}

// IMAGE
// 
void on_image_split_spectre_realize (GtkWidget *widget, gpointer user_data)
{
	GtkAllocation	allocation;

	// INIT COORDONNEES WIDGET
	VarSplit.AdrWidgetSpectre = widget;
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
	// 
	gtk_widget_grab_focus (VarSplit.AdrWidgetSpectre);
	
	/* Drag and drop support
	*  SIGNAL : 'drag-data-received'
	*/
	gtk_drag_dest_set (GTK_WIDGET (widget),
			   GTK_DEST_DEFAULT_MOTION |
			   GTK_DEST_DEFAULT_DROP,
			   drag_types, n_drag_types,
			   GDK_ACTION_COPY| GDK_ACTION_MOVE );

	gtk_drag_source_set(
			GTK_WIDGET(widget),
			GDK_BUTTON1_MASK | GDK_BUTTON2_MASK | GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP,
			drag_types, n_drag_types,
			GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_DEFAULT
			);
	g_signal_connect(G_OBJECT(widget),
			 "drag-data-received",
			 G_CALLBACK(split_drag_data_received_file),
			 NULL);
	
	VarSplit.BoolEventButtonPressSpectre = FALSE;
	VarSplit.BoolReadFileSpectre         = FALSE;
	VarSplit.PathNameFile                = NULL;
	VarSplit.NbrSelecteurs               = 0;
	VarSplit.TypeCursorSpectre           = _CURSOR_IS_NONE_;
	VarSplit.BoolPlay                    = FALSE;
	VarSplit.PercentActivePlay           = 0.0;
	VarSplit.BoolBlankWithCue            = FALSE;
		
	split_realize();
	SetAjust ();

	gtk_widget_show (GTK_WIDGET (GLADE_GET_OBJECT("button_play_split")));
	gtk_widget_hide (GTK_WIDGET (GLADE_GET_OBJECT("button_pause_split")));
	
	split_set_flag_buttons ();
	split_set_time ();
}


// 
// 
gboolean on_eventbox_splitspectre_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gboolean		BoolEventTypeScroll = (GDK_SCROLL == event->type) ? TRUE : FALSE;
	// gint			EventTypeScrollDirection = ((GdkEventScroll*)event)->direction;
	gint			CursorX, _CursorX, CursorY;
	GdkModifierType		state;
	gboolean		bool_click_droit = (((GdkEventButton*)event)->button == 3);
	gboolean		bool_click_centre = (((GdkEventButton*)event)->button == 2);
	gboolean		bool_click_gauche = (((GdkEventButton*)event)->button == 1);
	gdouble			Value;
	gdouble 		direction_x, direction_y;
	GtkAllocation	allocation;

	// PRINT_FUNC_LF();
	// IMPOSER UN REAFFICHAGE COMPLET
	// g_print( "event->type = %d\n", event->type );
	if( 29 == event->type ) {
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
		// return FALSE;
	}
	
 	if (VarSplit.NbrSelecteurs > 1 && GDK_BUTTON_PRESS == event->type && TRUE == bool_click_droit && VarSplit.NbrSelecteurs > 0) {
		popup_file_Split ();
		return FALSE;
	}
	
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
	
	Adjust.value          = 
	Value                 = gtk_adjustment_get_value( VarSplit.AdjScroll );
	Adjust.with           = SplitSpectre_get_with();
	Adjust.lower          = gtk_adjustment_get_lower( VarSplit.AdjScroll );
	Adjust.upper          = Adjust.with;
	Adjust.step_increment = gtk_adjustment_get_step_increment( VarSplit.AdjScroll );
	Adjust.page_size      = allocation.width;
	Adjust.page_increment = allocation.width;
	while( Adjust.value + Adjust.page_size > Adjust.upper )
		Adjust.value --; 
	SetAjust ();
 	
	// Si pas de selection a cet endroit retour
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &CursorX, &CursorY, &state );
 	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &CursorX, &CursorY, &state);	
	_CursorX = CursorX + (gint)gtk_adjustment_get_value( VarSplit.AdjScroll );

	// CHERCHE SI SELECTION
	if (FALSE == VarSplit.BoolEventButtonPressSpectre) {
		if (VarSplit.NbrSelecteurs > 0) {
			
			gint	SelBegin = SplitSelector_get_pos_begin (VarSplit.SelecteurActif);
			gint	SelEnd   = SplitSelector_get_pos_end (VarSplit.SelecteurActif);
			
			if( TRUE == SplitSelector_cursor_in_box_play( _CursorX, CursorY )) {
				cursor_set_hand ();
				VarSplit.TypeCursorSpectre = _CURSOR_IS_HAND_;
			}
			else if( SelBegin -1 == _CursorX ||
				 SelBegin    == _CursorX ||
				 SelBegin +1 == _CursorX ) {
				cursor_set_gauche ();
				VarSplit.TypeCursorSpectre = _CURSOR_IS_LEFT_;
			}
			else if( SelEnd -1 == _CursorX ||
				 SelEnd    == _CursorX ||
				 SelEnd +1 == _CursorX ) {
				cursor_set_droit ();
				VarSplit.TypeCursorSpectre = _CURSOR_IS_RIGHT_;
			}
			else {
				cursor_set_old ();
				VarSplit.TypeCursorSpectre = _CURSOR_IS_NONE_;
			}
		}
		else {
			cursor_set_old ();
			VarSplit.TypeCursorSpectre = _CURSOR_IS_NONE_;
		}
	}

	gdk_event_get_scroll_deltas( (const GdkEvent *)event, &direction_x, &direction_y );
	
	// SCROLL
	if( TRUE == BoolEventTypeScroll && VarSplit.NbrSelecteurs > 0 ) {
		if( TRUE == keys.BoolGDK_Control_L_R ) {
			if( direction_y == -1. ) {
				page_plus_scrolled( CursorX );
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
			}
			else if( direction_y == 1. ) {
				page_moins_scrolled( CursorX );
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
			}
		}
		else {
			if( direction_y == -1. ) {
				rigtht_plus_scrolled ();
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
			}
			else if( direction_y == 1. ) {
				left_plus_scrolled ();
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
				gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
			}
		}
	}
	
	// 
	else if (TRUE == bool_click_centre && GDK_BUTTON_PRESS == event->type && VarSplit.NbrSelecteurs > 0) {
		
		// CLICK CENTRE : GENERE UN NOUVEL INDEX
		// 
		// @Dzef:
		// Pour ajouter manuellement un marqueur de début, j'ajoute, par défaut, au même endroit celui de sortie de la plage précédente.
		// En mode lecture un double clic sur le bandeau des index place un sélecteur à la position de la tête de lecture ?
		// En mode pause un double clic sur le bandeau des index place un sélecteur à l'endroit où on a cliqué ?
		// 
		// 4 CAS D INSERTION SONT POSSISBLES. VOIR LA FONCTION : void SplitSelector_add (gint p_CursorX);
		// SplitSelector_add (CursorX + (gint)Adj->value);
		
		if (TRUE == VarSplit.BoolPlay) {
			SplitSelector_add( SplitSelector_get_pos_play ());
		}
		else {
			SplitSelector_add( _CursorX );
		}
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
	}
	
	// UN CLICK BOUTON SOURIS : PRESSED
	else if (TRUE == bool_click_gauche && event->type == GDK_BUTTON_PRESS) {
		if (VarSplit.TypeCursorSpectre == _CURSOR_IS_LEFT_ ||
		    VarSplit.TypeCursorSpectre == _CURSOR_IS_RIGHT_ ||
		    VarSplit.TypeCursorSpectre == _CURSOR_IS_HAND_) {
			    
			if (VarSplit.TypeCursorSpectre == _CURSOR_IS_HAND_) {
				AlsaPlay_Pause ();
			}
		}
		// REPOSITIONNEMENT DU CURSEUR DE LECTURE PAR CLICK SUR LE SPECTRE
		else {
			SplitSelector_set_pos_play( _CursorX );
			AlsaPlay_fseek( VarSplit.PercentActivePlay );
		}
		VarSplit.BoolEventButtonPressSpectre = TRUE;
		// 1 000 000 = 1 seconde
		// g_usleep( 150000 );
	}
	
	// GESTION CLICK BOUTON SOURIS : RELEASE
	else if (event->type == GDK_BUTTON_RELEASE) {
		if (TRUE == VarSplit.BoolEventButtonPressSpectre) {
	
			VarSplit.BoolEventButtonPressSpectre = FALSE;
			
			if (VarSplit.TypeCursorSpectre == _CURSOR_IS_HAND_) {
				AlsaPlay_fseek (VarSplit.PercentActivePlay);
			}
			else if (VarSplit.TypeCursorSpectre == _CURSOR_IS_LEFT_) {
				SplitSelector_set_pos_begin( _CursorX );
			}
			else if (VarSplit.TypeCursorSpectre == _CURSOR_IS_RIGHT_) {
				SplitSelector_set_pos_end( _CursorX );
			}
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
			cursor_set_old ();
			VarSplit.TypeCursorSpectre = _CURSOR_IS_NONE_;
		}
		split_set_name_file ();
		// g_usleep( 500000 );
	}
	
	// MOVE CURSEUR
	if (TRUE == VarSplit.BoolEventButtonPressSpectre && TRUE == bool_click_gauche) {
	
		gint	end      = VARSPLIT_SPECTRE_WITH + (gint)Value;
	
		// SCROLL RIGHT
		if( (gint)Value >= 0 && _CursorX >= end ) {
			rigtht_plus_scrolled ();
			rigtht_plus_scrolled ();
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		}
		// SCROLL LEFT
		else if( (gint)Value > 0 && _CursorX < (gint)Value ) {
			left_plus_scrolled ();
			left_plus_scrolled ();
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		}
		
		if( VarSplit.TypeCursorSpectre == _CURSOR_IS_HAND_ ) {
			SplitSelector_set_pos_play( _CursorX );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		}
		if( VarSplit.TypeCursorSpectre == _CURSOR_IS_LEFT_ ) {
			SplitSelector_set_pos_begin( _CursorX );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		}
		else if( VarSplit.TypeCursorSpectre == _CURSOR_IS_RIGHT_ ) {
			SplitSelector_set_pos_end( _CursorX );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
			gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
		}
		split_set_time ();
	}
	
	split_set_flag_buttons ();
	
	if( 4 == event->type || 7 == event->type  || 29 == event->type ) {
		gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
	}
	gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreChrono );
	gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );

	return FALSE;
}
// 
// 
gboolean on_eventbox_splitspectre_draw( GtkWidget *widget, cairo_t *p_cr, gpointer user_data )
{
	SplitSpectre_draw_lines( VarSplit.AdrWidgetSpectre, p_cr );
	return FALSE;
}
// 
// 
void on_scrollbar_split_realize( GtkWidget *widget, gpointer user_data )
{
	VarSplit.RangeAdjScroll = GTK_RANGE(widget);
	split_realize();
}
// SCROLLEDWINDOW
// 
gboolean on_scrollbar_split_event( GtkWidget *widget, GdkEvent *event, gpointer user_data )
{
	Adjust.value          = gtk_adjustment_get_value( VarSplit.AdjScroll );
	gtk_widget_queue_draw( VarSplit.AdrWidgetSpectre );
	gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreTop );
	gtk_widget_queue_draw( VarSplit.AdrWidgetSpectreChrono );

	return FALSE;
}












