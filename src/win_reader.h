 /*
 *  file      : win_reader.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef win_reader_h
#define win_reader_h 1

gboolean 	wind_reader_init( GtkWidget *FromWindMain );
// void		WinReader_open (gchar *title);
void		WinReader_set_pause (void);
void		WinReader_set_lecture (void);
void		WinReader_close (void);
void		WinReader_set_value (gdouble p_value);
gboolean	WinReader_is_close (void);

#endif

