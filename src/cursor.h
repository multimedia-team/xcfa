 /*
 *  file      : cursor.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef cursor_h
#define cursor_h 1


void	 cursor_set_clear (void);
void	 cursor_set_hand (void);
void     cursor_set_saisie (void);
void     cursor_set_gauche_droite (void);
void     cursor_set_haut (void);
void     cursor_set_bas (void);
void     cursor_set_gauche (void);
void     cursor_set_droit (void);
void     cursor_set_haut_gauche (void);
void     cursor_set_haut_droit (void);
void     cursor_set_bas_gauche (void);
void     cursor_set_bas_droit (void);
void     cursor_set_move (void);
void     cursor_set_old (void);
void     cursor_set_watch (void);
gboolean cursor_get_watch (void);


#endif
