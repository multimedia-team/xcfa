 /*
 *  file      : options_tags.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "options.h"


		/*
		Config.BoolArtistTag
		Config.BoolTitleTag
		Config.BoolAlbumTag
		Config.BoolNumerateTag
		Config.BoolGenreTag
		Config.BoolYearTag
		Config.BoolCommentTag
		*/
			
void on_checkbutton_artist_tag_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_checkbutton_artist_tag = widget;
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_artist_tag), Config.BoolArtistTag);
}
void on_checkbutton_title_tag_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_checkbutton_title_tag = widget;
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_title_tag), Config.BoolTitleTag);
}
void on_checkbutton_album_tag_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_checkbutton_album_tag = widget;
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_album_tag), Config.BoolAlbumTag);
}
void on_checkbutton_numerate_tag_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_checkbutton_numerate_tag = widget;
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_numerate_tag), Config.BoolNumerateTag);
}
void on_checkbutton_genre_tag_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_checkbutton_genre_tag = widget;
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_genre_tag), Config.BoolGenreTag);
}
void on_checkbutton_year_tag_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_checkbutton_year_tag = widget;
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_year_tag), Config.BoolYearTag);
}
void on_checkbutton_comment_tag_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_checkbutton_comment_tag = widget;
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_comment_tag), Config.BoolCommentTag);
}

void on_checkbutton_tag_clicked (GtkButton *button, gpointer user_data)
{
	// COLOR_LINE_COMMAND ColorLineCommand = COLOR_NONE;
	
	// PRINT_FUNC_LF();

	if (GTK_TOGGLE_BUTTON (button) == GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_artist_tag"))) {
		// ColorLineCommand = COLOR_TAG_ARTIST;
		Config.BoolArtistTag   = gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_artist_tag));
	} else if (GTK_TOGGLE_BUTTON (button) == GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_title_tag"))) {
		// ColorLineCommand = COLOR_TAG_TITLE;
		Config.BoolTitleTag    = gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_title_tag));
	} else if (GTK_TOGGLE_BUTTON (button) == GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_album_tag"))) {
		// ColorLineCommand = COLOR_TAG_ALBUM;
		Config.BoolAlbumTag    = gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_album_tag));
	} else if (GTK_TOGGLE_BUTTON (button) == GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_numerate_tag"))) {
		// ColorLineCommand = COLOR_TAG_NUMERATE;
		Config.BoolNumerateTag = gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_numerate_tag));
	} else if (GTK_TOGGLE_BUTTON (button) == GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_genre_tag"))) {
		// ColorLineCommand = COLOR_TAG_GENRE;
		Config.BoolGenreTag    = gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_genre_tag));
	} else if (GTK_TOGGLE_BUTTON (button) == GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_year_tag"))) {
		// ColorLineCommand = COLOR_TAG_YEAR;
		Config.BoolYearTag     = gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_year_tag));
	} else if (GTK_TOGGLE_BUTTON (button) == GTK_TOGGLE_BUTTON (GLADE_GET_OBJECT("checkbutton_comment_tag"))) {
		// ColorLineCommand = COLOR_TAG_COMMENT;
		Config.BoolCommentTag  = gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (var_options.Adr_checkbutton_comment_tag));
	}
	
	/*
	options_set_datas_interne (ColorLineCommand, var_options.Adr_label_lame_mp3, LAME_WAV_TO_MP3);
	options_set_datas_interne (ColorLineCommand, var_options.Adr_label_oggenc_ogg, OGGENC_WAV_TO_OGG);
	options_set_datas_interne (ColorLineCommand, var_options.Adr_label_flac_flac, FLAC_WAV_TO_FLAC);
	options_set_datas_interne (ColorLineCommand, var_options.Adr_label_mac_ape, MAC_WAV_TO_APE);
	options_set_datas_interne (ColorLineCommand, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	options_set_datas_interne (ColorLineCommand, var_options.Adr_label_musepack_mpc, MPPENC_WAV_TO_MPC);
	options_set_datas_interne (ColorLineCommand, var_options.Adr_label_faac_m4a, FAAC_WAV_TO_M4A);
	*/
}

