 /*
 *  file      : statusbar.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef statusbar_h
#define statusbar_h 1


#include "global.h"

typedef enum
{
	_STATUSBAR_SIMPLE_ = 0,					// 
	_STATUSBAR_WARN_,						// 
	_STATUSBAR_ERR_							// 
} STATUS_BAR_MESS;

typedef struct {
	gchar		*Mess;						// Type: _STATUSBAR_SIMPLE_
	gchar		*MessWarn;					// Type: _STATUSBAR_WARN_
	gchar		*MessError;					// Type: _STATUSBAR_ERR_
} STATUSBAR_MESS;

STATUSBAR_MESS	StatusBar [ NOTEBOOK_PRGEXTERNES +2 ];	// #define NOTEBOOK_DVD_AUDIO	0
											// #define NOTEBOOK_CD_AUDIO	1
											// #define NOTEBOOK_FICHIERS	2
											// #define NOTEBOOK_SPLIT	3
											// #define NOTEBOOK_POCHETTE	4
											// #define NOTEBOOK_OPTIONS	5
											// #define NOTEBOOK_PRGEXTERNES	6

void	StatusBar_remove( void );
void 	StatusBar_set_mess(
			gint		p_which,			// NOTEBOOK_DVD_AUDIO .. NOTEBOOK_PRGEXTERNES
			STATUS_BAR_MESS	p_StatusMess,	// _STATUSBAR_SIMPLE_ || _STATUSBAR_WARN_ || _STATUSBAR_ERR_
			gchar		*p_mess				// Message
			);
void	StatusBar_puts( void );


#endif


