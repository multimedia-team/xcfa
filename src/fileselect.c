 /*
 *  file      : fileselect.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "file.h"
#include "poche.h"
#include "fileselect.h"




/*
*---------------------------------------------------------------------------
* DEFINITIONS
*---------------------------------------------------------------------------
*/

typedef enum {

	_REP_SIMPLE_,
	_REP_MULTIPLE_,
	_FILE_SIMPLE_,
	_FILE_MULTIPLE_,
	_FILE_IMG_SIMPLE_,
	_FILE_IMG_MULTIPLE_

} TYPE_SELECT;


typedef struct {
	gchar		 *Title;
	TYPE_SELECT	  TypeSelect;
	GtkWidget	 *AdrFileChooser;
	void		  (*Call_Func) (GtkButton *button, gpointer user_data);
	void		  (*Func_Extern) (gchar *string);
} SELECT;


typedef struct {

	TYPE_FILESELECTION	 TypeFileSelection;
	
} VAR_FILESELECT;

VAR_FILESELECT	VarFileSelect;


/*
*---------------------------------------------------------------------------
* PROTOTYPES
*---------------------------------------------------------------------------
*/

void ON_button_PATH_CHOICE_DESTINATION_clicked (GtkButton *button, gpointer user_data);
void ON_button_PATH_LOAD_ONE_FILE_clicked (GtkButton *button, gpointer user_data);
void ON_button_PATH_IMPORT_IMAGES_clicked (GtkButton *button, gpointer user_data);
void ON_button_PATH_POCHETTE_clicked (GtkButton *button, gpointer user_data);


/*
*---------------------------------------------------------------------------
* DATAS
*---------------------------------------------------------------------------
*/

SELECT Select [ ] =
{
{gettext_noop("Choosing a destination folder"),					_REP_SIMPLE_,			NULL, ON_button_PATH_CHOICE_DESTINATION_clicked,	NULL},
{gettext_noop("Choosing a file CUE WAV FLAC OGG MP3 APE WMA"),	_FILE_SIMPLE_,			NULL, ON_button_PATH_LOAD_ONE_FILE_clicked,			NULL},
{gettext_noop("Choosing a file musical"),						_FILE_SIMPLE_,			NULL, ON_button_PATH_LOAD_ONE_FILE_clicked,			NULL},
{gettext_noop("Choosing a file"),								_FILE_SIMPLE_,			NULL, ON_button_PATH_LOAD_ONE_FILE_clicked,			NULL},
{gettext_noop("Choosing a file"),								_FILE_MULTIPLE_,		NULL, ON_button_PATH_LOAD_ONE_FILE_clicked,			NULL},
{gettext_noop("Choosing a WAV file"),							_FILE_MULTIPLE_,		NULL, ON_button_PATH_LOAD_ONE_FILE_clicked,			NULL},
{gettext_noop("Choosing a MP3 OGG file"),						_FILE_MULTIPLE_,		NULL, ON_button_PATH_LOAD_ONE_FILE_clicked,			NULL},
{gettext_noop("Choosing a FLAC MPC OGG MP3 file"),				_FILE_MULTIPLE_,		NULL, ON_button_PATH_LOAD_ONE_FILE_clicked,			NULL},
{gettext_noop("Import pictures for the cover"),					_FILE_IMG_MULTIPLE_,	NULL, ON_button_PATH_IMPORT_IMAGES_clicked,			NULL},
{gettext_noop("Choosing an Image Storage path"),				_FILE_MULTIPLE_,		NULL, ON_button_PATH_POCHETTE_clicked,				NULL},
{gettext_noop("Choosing a place for backup"),					_FILE_MULTIPLE_,		NULL, ON_button_PATH_POCHETTE_clicked,				NULL},
 
{NULL, -1}
};



// CHOIX D UN LIEU DE SAUVEGARDE
// 
void ON_button_PATH_POCHETTE_clicked (GtkButton *button, gpointer user_data)
{
	gint	Choice = GPOINTER_TO_INT(user_data);
	gchar	*Path = NULL;
	
	Path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
	gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
	Select[ Choice ].AdrFileChooser = NULL;
	if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
	g_free (Path);
	Path = NULL;
}
// CHOIX D UN FICHIER IMAGE
// 
void ON_button_PATH_IMPORT_IMAGES_clicked (GtkButton *button, gpointer user_data)
{
/*
	gint	Choice = GPOINTER_TO_INT(user_data);
	PRINT_FUNC_LF();
	gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
	Select[ Choice ].AdrFileChooser = NULL;
*/
	gint	Choice = GPOINTER_TO_INT(user_data);
	gchar	*Path = NULL;
	GSList	*List = NULL;

	Path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
	if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
	g_free (Path);
	Path = NULL;
	List = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
	gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
	Select[ Choice ].AdrFileChooser = NULL;
	
	pochedir_add_img_file (List);
	
	g_slist_free (List);
	List = NULL;
}
// CHOIX D UN FICHIER
// 
void ON_button_PATH_LOAD_ONE_FILE_clicked (GtkButton *button, gpointer user_data)
{
	gint	Choice = GPOINTER_TO_INT(user_data);
	gchar	*Path = NULL;
	GSList	*List = NULL;
	
	if( _PATH_LOAD_FILE_MUSIC_ == VarFileSelect.TypeFileSelection ) {
		Path = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
		g_free (Path);
		Path = NULL;
		gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
		Select[ Choice ].AdrFileChooser = NULL;
	}
	else if (_PATH_LOAD_ONE_FILE_ == VarFileSelect.TypeFileSelection || _PATH_LOAD_SPLIT_FILE_ == VarFileSelect.TypeFileSelection) {
		Path = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
		Select[ Choice ].AdrFileChooser = NULL;
		if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
		g_free (Path);
		Path = NULL;
	}
	else if (_PATH_LOAD_FILE_ALL_ == VarFileSelect.TypeFileSelection) {
		Path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
		g_free (Path);
		Path = NULL;
		List = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
		Select[ Choice ].AdrFileChooser = NULL;
		fileanalyze_add_file_to_treeview (VarFileSelect.TypeFileSelection, List);
		g_slist_free (List);
		List = NULL;
	}
	else if (_PATH_LOAD_FILE_WAV_ == VarFileSelect.TypeFileSelection) {
		Path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
		g_free (Path);
		Path = NULL;
		List = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
		Select[ Choice ].AdrFileChooser = NULL;
		fileanalyze_add_file_to_treeview (VarFileSelect.TypeFileSelection, List);
		g_slist_free (List);
		List = NULL;
	}
	else if (_PATH_LOAD_FILE_MP3OGG_ == VarFileSelect.TypeFileSelection) {
		Path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
		g_free (Path);
		Path = NULL;
		List = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
		Select[ Choice ].AdrFileChooser = NULL;
		fileanalyze_add_file_to_treeview (VarFileSelect.TypeFileSelection, List);
		g_slist_free (List);
		List = NULL;
	}
	else if (_PATH_LOAD_FILE_TAGS_ == VarFileSelect.TypeFileSelection) {
		Path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
		g_free (Path);
		Path = NULL;
		List = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
		gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
		Select[ Choice ].AdrFileChooser = NULL;
		fileanalyze_add_file_to_treeview (VarFileSelect.TypeFileSelection, List);
		g_slist_free (List);
		List = NULL;
	}
}
// CHOIX D UN DOSSIER DE DESTINATION
// 
void ON_button_PATH_CHOICE_DESTINATION_clicked (GtkButton *button, gpointer user_data)
{
	gchar  *Path = NULL;
	gint    Choice = GPOINTER_TO_INT(user_data);

	Path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (Select[ Choice ].AdrFileChooser));
	gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
	Select[ Choice ].AdrFileChooser = NULL;
	if (Select[ Choice ].Func_Extern) (*Select[ Choice ].Func_Extern) (Path);
	g_free (Path);
	Path = NULL;
}


/*
*---------------------------------------------------------------------------
* GESTION FILESELECT
*---------------------------------------------------------------------------
*/

// Demande de destruction de la fenetre
// 
gboolean fileselect_on_filechooserdialog_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gint    Choice = GPOINTER_TO_INT(user_data);

	/* PRINT_FUNC_LF(); */
	/*
	g_print ("user_data = %d\n", (gint)user_data);
	g_print ("Choice    = %d\n", Choice);
	*/
	gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
	Select[ Choice ].AdrFileChooser = NULL;
	return FALSE;
}
// Demande de destruction de la fenetre
// 
gboolean fileselect_on_filechooserdialog_destroy_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gint    Choice = GPOINTER_TO_INT(user_data);

	/* PRINT_FUNC_LF(); */
	/*
	g_print ("user_data = %d\n", (gint)user_data);
	g_print ("Choice    = %d\n", Choice);
	*/
	gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
	Select[ Choice ].AdrFileChooser = NULL;
	return FALSE;
}
// 
// 
void fileselect_on_button_cancel_clicked (GtkButton *button, gpointer user_data)
{
	gint    Choice = GPOINTER_TO_INT(user_data);

	/* PRINT_FUNC_LF(); */
	/*
	g_print ("user_data = %d\n", (gint)user_data);
	g_print ("Choice    = %d\n", Choice);
	*/
	gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
	Select[ Choice ].AdrFileChooser = NULL;
}
// 
// 
void fileselect_on_button_ok_clicked (GtkButton *button, gpointer user_data)
{
	gint    Choice = GPOINTER_TO_INT(user_data);

	/* PRINT_FUNC_LF(); */
	/*
	g_print ("user_data = %d\n", (gint)user_data);
	g_print ("Choice    = %d\n", Choice);
	*/
	gtk_widget_destroy(Select[ Choice ].AdrFileChooser);
	Select[ Choice ].AdrFileChooser = NULL;
}
// http://forum.gtk-fr.org/viewtopic.php?pid=22743
// 
void chooserDialog_preview_update (GtkFileChooser *filechooser, gpointer data)
{
	GtkWidget	*preview = NULL;
	GdkPixbuf	*pixbuf = NULL;
	gchar		*filename = NULL;

	preview = GTK_WIDGET (data);
	if( NULL != (filename = gtk_file_chooser_get_preview_filename(filechooser))) {
		pixbuf = gdk_pixbuf_new_from_file_at_size(filename, 200, 200, NULL); 
		g_free (filename);
		filename = NULL;
		if( NULL != pixbuf ) {
			gtk_image_set_from_pixbuf (GTK_IMAGE(preview), pixbuf);
			g_object_unref(pixbuf);
			gtk_file_chooser_set_preview_widget_active(filechooser, TRUE);
		}
	}
}
// Demande de creation de la fenetre de selection de fichier(s) ou de repertoire
// https://developer.gnome.org/gtk3/stable/GtkFileChooserDialog.html
// 
void fileselect_create (TYPE_FILESELECTION Choice, gchar *Path, void *Func_Extern)
{
	GtkWidget *filechooserdialog;
	// GtkWidget *dialog_vbox1;
	// GtkWidget *dialog_action_area1;
	// GtkWidget *button_cancel;
	// GtkWidget *button_ok;
	GtkWidget *preview;
	GtkFileFilter *filter;
	gint			res;
	
	if (_NB_PATH_ <= 0) return;
	if (Choice < 0 || Choice > _NB_PATH_) return;
	
	VarFileSelect.TypeFileSelection = Choice;
	
	if (Choice < _PATH_CHOICE_DESTINATION_ || Choice > _NB_PATH_) return;
	filechooserdialog = Select[ Choice ].AdrFileChooser;

	if (filechooserdialog != NULL) {
		gdk_window_raise( gtk_widget_get_window(filechooserdialog) );
		return;
	}

	/* CORRECTIF DU 26 01 2006 */
	Select[ Choice ].Func_Extern = Func_Extern;

	/* CORRECTIF DU 28 05 2006 */
	g_utf8_validate (Select[ Choice ].Title, -1, NULL);
	if (NULL == Select[ Choice ].Title) {
		PRINT("\tTITLE == NULL");
		return;
	}

	/*
	typedef enum
	{
	  GTK_FILE_CHOOSER_ACTION_OPEN,
	  GTK_FILE_CHOOSER_ACTION_SAVE,
	  GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
	  GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER
	} GtkFileChooserAction;
	*/
	if (Select[ Choice ].TypeSelect == _REP_SIMPLE_ || Select[ Choice ].TypeSelect == _REP_MULTIPLE_) {
		/* selection de directory */
		/**
		filechooserdialog = gtk_file_chooser_dialog_new (Select[ Choice ].Title,
								  NULL,
								  GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
								  // GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER,
								  NULL,
								  NULL);
		*/
		filechooserdialog = gtk_file_chooser_dialog_new (
								Select[ Choice ].Title,
								NULL,
								GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
								_("Cancel"),
								GTK_RESPONSE_CANCEL,
								_("Open"),
								GTK_RESPONSE_ACCEPT,
								NULL);
	}
	else if (Select[ Choice ].TypeSelect == _FILE_SIMPLE_ || Select[ Choice ].TypeSelect == _FILE_MULTIPLE_) {
		/* selection de fichiers(s) */
		/**
		filechooserdialog = gtk_file_chooser_dialog_new (Select[ Choice ].Title,
								  NULL,
								  GTK_FILE_CHOOSER_ACTION_OPEN,
								  NULL,
								  NULL);
		*/
		filechooserdialog = gtk_file_chooser_dialog_new (
								Select[ Choice ].Title,
								NULL,
								GTK_FILE_CHOOSER_ACTION_OPEN,
								_("Cancel"),
								GTK_RESPONSE_CANCEL,
								_("Open"),
								GTK_RESPONSE_ACCEPT,
								NULL);
		if (_PATH_LOAD_SPLIT_FILE_ == Choice) {
			filter = gtk_file_filter_new ();
			gtk_file_filter_add_pattern (filter, "*.wav");
			gtk_file_filter_add_pattern (filter, "*.WAV");
			gtk_file_filter_add_pattern (filter, "*.bwf");
			gtk_file_filter_add_pattern (filter, "*.BWF");
			gtk_file_filter_add_pattern (filter, "*.flac");
			gtk_file_filter_add_pattern (filter, "*.FLAC");
			gtk_file_filter_add_pattern (filter, "*.ogg");
			gtk_file_filter_add_pattern (filter, "*.OGG");
			gtk_file_filter_add_pattern (filter, "*.mp3");
			gtk_file_filter_add_pattern (filter, "*.MP3");
			gtk_file_filter_add_pattern (filter, "*.ape");
			gtk_file_filter_add_pattern (filter, "*.APE");
			
			gtk_file_filter_add_pattern (filter, "*.wma");
			gtk_file_filter_add_pattern (filter, "*.WMA");
			
			gtk_file_filter_add_pattern (filter, "*.cue");
			gtk_file_filter_add_pattern (filter, "*.CUE");
			gtk_file_filter_set_name (filter, "*.wav *.flac *.ogg *.mp3 *.ape *.wma *.cue");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "WAV (*.wav)");
			gtk_file_filter_add_pattern (filter, "*.wav");
			gtk_file_filter_add_pattern (filter, "*.WAV");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "BWF (*.bwf)");
			gtk_file_filter_add_pattern (filter, "*.bwf");
			gtk_file_filter_add_pattern (filter, "*.BWF");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "FLAC (*.flac)");
			gtk_file_filter_add_pattern (filter, "*.flac");
			gtk_file_filter_add_pattern (filter, "*.FLAC");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "MP3 (*.mp3)");
			gtk_file_filter_add_pattern (filter, "*.mp3");
			gtk_file_filter_add_pattern (filter, "*.MP3");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "OGG (*.ogg)");
			gtk_file_filter_add_pattern (filter, "*.ogg");
			gtk_file_filter_add_pattern (filter, "*.OGG");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "APE (*.ape)");
			gtk_file_filter_add_pattern (filter, "*.ape");
			gtk_file_filter_add_pattern (filter, "*.APE");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "WMA (*.wma)");
			gtk_file_filter_add_pattern (filter, "*.wma");
			gtk_file_filter_add_pattern (filter, "*.WMA");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);

			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "CUE (*.cue)");
			gtk_file_filter_add_pattern (filter, "*.cue");
			gtk_file_filter_add_pattern (filter, "*.CUE");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
		
		}
		else if( Choice == _PATH_LOAD_FILE_ALL_ || Choice == _PATH_LOAD_FILE_MUSIC_ ) {
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, _("All Files (*.*)"));
			gtk_file_filter_add_pattern (filter, "*.wav");
			gtk_file_filter_add_pattern (filter, "*.WAV");
			gtk_file_filter_add_pattern (filter, "*.bwf");
			gtk_file_filter_add_pattern (filter, "*.BWF");
			gtk_file_filter_add_pattern (filter, "*.flac");
			gtk_file_filter_add_pattern (filter, "*.FLAC");
			gtk_file_filter_add_pattern (filter, "*.ape");
			gtk_file_filter_add_pattern (filter, "*.APE");
			gtk_file_filter_add_pattern (filter, "*.wv");
			gtk_file_filter_add_pattern (filter, "*.WV");
			gtk_file_filter_add_pattern (filter, "*.ogg");
			gtk_file_filter_add_pattern (filter, "*.OGG");
			gtk_file_filter_add_pattern (filter, "*.oga");
			gtk_file_filter_add_pattern (filter, "*.OGA");
			gtk_file_filter_add_pattern (filter, "*.m4a");
			gtk_file_filter_add_pattern (filter, "*.M4A");
			gtk_file_filter_add_pattern (filter, "*.aac");
			gtk_file_filter_add_pattern (filter, "*.AAC");
			gtk_file_filter_add_pattern (filter, "*.mpc");
			gtk_file_filter_add_pattern (filter, "*.MPC");
			gtk_file_filter_add_pattern (filter, "*.mp3");
			gtk_file_filter_add_pattern (filter, "*.MP3");
			gtk_file_filter_add_pattern (filter, "*.rm");
			gtk_file_filter_add_pattern (filter, "*.RM");
			gtk_file_filter_add_pattern (filter, "*.dts");
			gtk_file_filter_add_pattern (filter, "*.DTS");
			gtk_file_filter_add_pattern (filter, "*.aif");
			gtk_file_filter_add_pattern (filter, "*.aiff");
			gtk_file_filter_add_pattern (filter, "*.AIF");
			gtk_file_filter_add_pattern (filter, "*.AIFF");
			gtk_file_filter_add_pattern (filter, "*.shn");
			gtk_file_filter_add_pattern (filter, "*.SHN");
			gtk_file_filter_add_pattern (filter, "*.wma");
			gtk_file_filter_add_pattern (filter, "*.WMA");
			gtk_file_filter_add_pattern (filter, "*.ac3");
			gtk_file_filter_add_pattern (filter, "*.AC3");
			
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "WAV (*.wav)");
			gtk_file_filter_add_pattern (filter, "*.wav");
			gtk_file_filter_add_pattern (filter, "*.WAV");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "BWF (*.bwf)");
			gtk_file_filter_add_pattern (filter, "*.bwf");
			gtk_file_filter_add_pattern (filter, "*.BWF");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "FLAC (*.flac)");
			gtk_file_filter_add_pattern (filter, "*.flac");
			gtk_file_filter_add_pattern (filter, "*.FLAC");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "APE (*.ape)");
			gtk_file_filter_add_pattern (filter, "*.ape");
			gtk_file_filter_add_pattern (filter, "*.APE");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "WAVPACK (*.wv)");
			gtk_file_filter_add_pattern (filter, "*.wv");
			gtk_file_filter_add_pattern (filter, "*.WV");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "OGG (*.ogg)");
			gtk_file_filter_add_pattern (filter, "*.ogg");
			gtk_file_filter_add_pattern (filter, "*.OGG");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "M4A (*.m4a)");
			gtk_file_filter_add_pattern (filter, "*.m4a");
			gtk_file_filter_add_pattern (filter, "*.M4A");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "AAC (*.aac)");
			gtk_file_filter_add_pattern (filter, "*.aac");
			gtk_file_filter_add_pattern (filter, "*.AAC");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "MPC (*.mpc)");
			gtk_file_filter_add_pattern (filter, "*.mpc");
			gtk_file_filter_add_pattern (filter, "*.MPC");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "MP3 (*.mp3)");
			gtk_file_filter_add_pattern (filter, "*.mp3");
			gtk_file_filter_add_pattern (filter, "*.MP3");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "AIFF (*.aif*)");
			gtk_file_filter_add_pattern (filter, "*.aif*");
			gtk_file_filter_add_pattern (filter, "*.AIF*");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "RM (*.rm)");
			gtk_file_filter_add_pattern (filter, "*.rm");
			gtk_file_filter_add_pattern (filter, "*.RM");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "DTS (*.dts)");
			gtk_file_filter_add_pattern (filter, "*.dts");
			gtk_file_filter_add_pattern (filter, "*.DTS");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "SHORTEN (*.shn)");
			gtk_file_filter_add_pattern (filter, "*.shn");
			gtk_file_filter_add_pattern (filter, "*.SHN");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "WMA (*.wma)");
			gtk_file_filter_add_pattern (filter, "*.wma");
			gtk_file_filter_add_pattern (filter, "*.WMA");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "AC3 (*.ac3)");
			gtk_file_filter_add_pattern (filter, "*.ac3");
			gtk_file_filter_add_pattern (filter, "*.AC3");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
		}
		else if (Choice == _PATH_LOAD_FILE_WAV_) {
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, _("All Files (*.*)"));
			gtk_file_filter_add_pattern (filter, "*.wav");
			gtk_file_filter_add_pattern (filter, "*.WAV");
			gtk_file_filter_add_pattern (filter, "*.bwf");
			gtk_file_filter_add_pattern (filter, "*.BWF");
			gtk_file_filter_set_name (filter, "*.wav *.bwf");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
		}
		else if (Choice == _PATH_LOAD_FILE_MP3OGG_) {
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, _("All Files (*.*)"));
			gtk_file_filter_add_pattern (filter, "*.ogg");
			gtk_file_filter_add_pattern (filter, "*.OGG");
			gtk_file_filter_add_pattern (filter, "*.oga");
			gtk_file_filter_add_pattern (filter, "*.OGA");
			gtk_file_filter_add_pattern (filter, "*.mp3");
			gtk_file_filter_add_pattern (filter, "*.MP3");
			gtk_file_filter_set_name (filter, "*.mp3 *.ogg");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "OGG (*.ogg)");
			gtk_file_filter_add_pattern (filter, "*.ogg");
			gtk_file_filter_add_pattern (filter, "*.OGG");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "MP3 (*.mp3)");
			gtk_file_filter_add_pattern (filter, "*.mp3");
			gtk_file_filter_add_pattern (filter, "*.MP3");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
		}
		else if (Choice == _PATH_LOAD_FILE_TAGS_) {
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, _("All Files (*.*)"));
			gtk_file_filter_add_pattern (filter, "*.flac");
			gtk_file_filter_add_pattern (filter, "*.FLAC");
			gtk_file_filter_add_pattern (filter, "*.ogg");
			gtk_file_filter_add_pattern (filter, "*.OGG");
			gtk_file_filter_add_pattern (filter, "*.oga");
			gtk_file_filter_add_pattern (filter, "*.OGA");
			gtk_file_filter_add_pattern (filter, "*.mpc");
			gtk_file_filter_add_pattern (filter, "*.MPC");
			gtk_file_filter_add_pattern (filter, "*.mp3");
			gtk_file_filter_add_pattern (filter, "*.MP3");
			gtk_file_filter_set_name (filter, "*.flac *.mpc *.mp3 *.ogg");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "FLAC (*.flac)");
			gtk_file_filter_add_pattern (filter, "*.flac");
			gtk_file_filter_add_pattern (filter, "*.FLAC");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "OGG (*.ogg)");
			gtk_file_filter_add_pattern (filter, "*.ogg");
			gtk_file_filter_add_pattern (filter, "*.OGG");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "MPC (*.mpc)");
			gtk_file_filter_add_pattern (filter, "*.mpc");
			gtk_file_filter_add_pattern (filter, "*.MPC");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
			filter = gtk_file_filter_new ();
			gtk_file_filter_set_name (filter, "MP3 (*.mp3)");
			gtk_file_filter_add_pattern (filter, "*.mp3");
			gtk_file_filter_add_pattern (filter, "*.MP3");
			gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (filechooserdialog), filter);
		}
	}
	else if (Select[ Choice ].TypeSelect == _FILE_IMG_SIMPLE_ || Select[ Choice ].TypeSelect == _FILE_IMG_MULTIPLE_) {
		/* selection de fichiers images avec previsualisation */
		/**
		filechooserdialog = gtk_file_chooser_dialog_new (Select[ Choice ].Title,
								  NULL,
								  GTK_FILE_CHOOSER_ACTION_OPEN,
								  // GTK_STOCK_CANCEL,
								  // GTK_RESPONSE_CANCEL,
								  // GTK_STOCK_OPEN,
								  // GTK_RESPONSE_ACCEPT,
								  NULL,
								  NULL);
		*/
		filechooserdialog = gtk_file_chooser_dialog_new (
								Select[ Choice ].Title,
								NULL,
								GTK_FILE_CHOOSER_ACTION_OPEN,
								_("Cancel"),
								GTK_RESPONSE_CANCEL,
								_("Open"),
								GTK_RESPONSE_ACCEPT,
								NULL);
		preview = gtk_image_new ();
		gtk_widget_set_size_request(GTK_WIDGET(preview), 200, 200);
		gtk_file_chooser_set_preview_widget(GTK_FILE_CHOOSER(filechooserdialog),  GTK_WIDGET(preview));
		gtk_file_chooser_set_preview_widget_active (GTK_FILE_CHOOSER (filechooserdialog), TRUE);
		g_signal_connect(GTK_FILE_CHOOSER(filechooserdialog), "update-preview", G_CALLBACK (chooserDialog_preview_update), preview);
	}


	if (Select[ Choice ].TypeSelect == _REP_MULTIPLE_ ||
		Select[ Choice ].TypeSelect == _FILE_MULTIPLE_ ||
		Select[ Choice ].TypeSelect == _FILE_IMG_MULTIPLE_) {
		/* selection 'multiple' */
		g_object_set (filechooserdialog, "select-multiple", TRUE, NULL);
		gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (filechooserdialog), TRUE);
	}

	gtk_window_set_position (GTK_WINDOW (filechooserdialog), GTK_WIN_POS_CENTER);

	gtk_window_set_transient_for (GTK_WINDOW(filechooserdialog), GTK_WINDOW(WindMain));
	gtk_window_set_modal (GTK_WINDOW (filechooserdialog), TRUE);

	gtk_file_chooser_set_current_folder (
					GTK_FILE_CHOOSER (filechooserdialog),
					Path);

	Select[ Choice ].AdrFileChooser = filechooserdialog;

	gtk_window_set_type_hint (GTK_WINDOW (filechooserdialog), GDK_WINDOW_TYPE_HINT_DIALOG);

	g_signal_connect ((gpointer) filechooserdialog, "delete_event", G_CALLBACK (fileselect_on_filechooserdialog_delete_event), (gpointer)Choice);
	g_signal_connect ((gpointer) filechooserdialog, "destroy_event", G_CALLBACK (fileselect_on_filechooserdialog_destroy_event), (gpointer)Choice);
  	
  	res = gtk_dialog_run (GTK_DIALOG (filechooserdialog));
  	if (res == GTK_RESPONSE_ACCEPT) {
		Select[ Choice ].Call_Func( NULL, (gpointer)Choice );
	}
	else if(res == GTK_RESPONSE_CANCEL) {
		fileselect_on_button_cancel_clicked( NULL, (gpointer)Choice );
	}
}

