 /*
 *  file      : cd_cue.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef cd_cue_h
#define cd_cue_h 1


typedef struct {
	gint	total;
	gint	min;
	gint	sec;
	gint	cent;
} CUE_TIME;


typedef struct {
	// 1. length   12375 [02:45:00]  begin      32 [00:00:32]
	gint	Len;											// 
  	gint	Start;											// 
	gint	StartMin;										// 
	gint	StartSec;										// 
	gint	StartCent;										// 
 } CDCUE_PARTIEL;
	
typedef struct {
	gchar			*Performer;								// 
	gchar			*Title;									// 
	gboolean		 BoolExtract;							// TRUE si extraction
	gchar			*Partiel_Title;							// 
	CUE_TIME		 EndTime;								// 
	CUE_TIME		 BeginTime;								// 
	CDCUE_PARTIEL		 CuePartiel;						// 
} CDCUE;

// length DIV 75
// 	DIV 60 = Min
// 	MOD 60 = Sec
// length MOD 75 = Cent
typedef struct {
	
	gint			 length;								// Longueur totale
	gint			 length_min ,length_sec ,length_cent;	// Details
	gint			 begin;									// Longueur totale
	gint			 begin_min ,begin_sec ,begin_cent;		// Details

} BASE_IOCTL_DATAS;

typedef struct {
	// Base obtenue pae IOCTL ou CDPARANOIA
	gint			 TotalTracks;							// Nombre total de pistes
	BASE_IOCTL_DATAS	*Datas;								//
	// CUE
	gchar			*Performer;								//
	gchar			*Title;									//
	gchar			*File;									//
	CDCUE			*Cue;									//
	gchar			*PathNameDestFileCue;					//
	
} BASE_IOCTL;

extern BASE_IOCTL BaseIoctl;


void		cdcue_remove_base_ioctl (void);
void		cdcue_print_base_ioctl (void);
void		cdcue_alloc_base_ioctl (gint p_taille);
void		cdcue_write_cue (void);
void		cdcue_remove (void);
gboolean	cdcue_is_alloc (void);
void		cdcue_make_cue (void);
void            cdcue_set_BoolExtract (gint p_index, gboolean p_flag);


#endif

