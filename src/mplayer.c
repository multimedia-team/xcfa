 /*
 *  file      : mplayer.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>	// PROCESSUS NON-BLOQUANT
#include <string.h>
 
#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "mplayer.h"


/*
*---------------------------------------------------------------------------
* DEFINE and STRUCT and VAR
*---------------------------------------------------------------------------
*/
#define XCFA_TMP_FIFO	"/tmp/XcfaCmdToMplayer"

VAR_MPLAYER VarMplayer;





/*
*---------------------------------------------------------------------------
* EXTERNAL REF
*---------------------------------------------------------------------------
*/
extern int kill(pid_t pid, int sig);




// SIGNAL ET EXECUTE EXTERNAL
// 
void mplayer_sigchld (gint signum)
{
	gint status;
       
        wait(&status);

	VarMplayer.SignalNumchildren --;
        if (VarMplayer.SignalNumchildren > 0)
        {
                /* re-install the signal handler */
                signal (SIGCHLD, mplayer_sigchld);
        }
}
// 
// 
int mplayer_call_exec (gchar **args, pid_t *p, gint p_output)
{
	gchar **ptr = (gchar **)args;

	VarMplayer.SignalNumchildren = 0;
	if (pipe (VarMplayer.Tube) != 0)
	{
		fprintf (stderr, "error: pipe\n");
		exit (1);
	}
	if ((*p = fork()) == 0)
	{
		dup2 (VarMplayer.Tube [ 1 ], p_output);
		close (VarMplayer.Tube [ 1 ]);
		execvp ((gchar *)*(ptr+0), ptr);
		fprintf (stderr, "error: exec");
		exit (2);
	}
	VarMplayer.SignalNumchildren ++;
	signal (SIGCHLD, mplayer_sigchld);
	close (VarMplayer.Tube [ 1 ]);
	return (VarMplayer.Tube [ 0 ]);
}

// FIFO CREATE and REMOVE
// 
void mplayer_remove_list_args (void)
{
	gint	 PosArg = 0;
	
	for( PosArg = 0; PosArg < MAX_PLAYERS_ARGS; PosArg ++ ) {
		if(  NULL != VarMplayer.PlayerArgs[ PosArg ] ) {
			g_free (VarMplayer.PlayerArgs[ PosArg ]);
		}
		VarMplayer.PlayerArgs[ PosArg ] = NULL;
	}
}
// 
// 
/*
 man -k mkfifo 
mkfifo (1)           - Créer des tubes nommés (FIFO)
mkfifo (3)           - Créer un fichier spécial FIFO
mkfifoat (3)         - Créer une FIFO (un tube nommé) relatif à un descripteur de fichier d'un répertoire

NOM
       mkfifo - Créer un fichier spécial FIFO

SYNOPSIS
       #include <sys/types.h>
       #include <sys/stat.h>

       int mkfifo(const char *pathname, mode_t mode);

DESCRIPTION
       La  fonction mkfifo() crée un fichier spécial FIFO (tube nommé)  à l’emplacement pathname. mode indique les permissions d’accès. Ces permissions sont modifiées par la
       valeur d’umask du processus : les permissions d’accès effectivement adoptées sont (mode & ~umask).

       L’ouverture d’un FIFO en lecture est généralement  bloquante,  jusqu’à  ce qu’un autre processus ouvre le même FIFO en écriture, et inversement.
*/
void mplayer_create_fifo (void)
{
	 if (0 == mkfifo(XCFA_TMP_FIFO, 0644)) {
	 	gint	status;
		gint	descripteur;
	 	
	 	descripteur = open (XCFA_TMP_FIFO, O_RDWR);
	 	
		// LE FICHIER EST OUVERT EN MODE « NON-BLOQUANT »
	 	status = fcntl(descripteur, F_GETFL);
	 	status = fcntl(descripteur, F_SETFL, status | O_NONBLOCK);
	 	
	 	close (descripteur);
	 }
}
// 
// 
void mplayer_remove_fifo (void)
{
	if (FALSE == libutils_access_mode (XCFA_TMP_FIFO)) return;
	g_unlink (XCFA_TMP_FIFO);
}
// FIFO COMMAND
// 
void mplayer_set_fifo (gchar *str)
{
	if (VarMplayer.BoolErreurMplayer == TRUE) return;
	if (VarMplayer.BoolThreadActivate == FALSE) return;
	
	if (TRUE == libutils_access_mode (XCFA_TMP_FIFO)) {

		FILE *fp = NULL;
		if ((fp = fopen (XCFA_TMP_FIFO, "w")) != NULL) {
			fprintf (fp, "%s\n", str);
			fclose (fp);
		}
	}
}
// 
// 
void mplayer_fifo_quit (void)
{
	mplayer_set_fifo ("quit");
	// VarMplayer.TypeButtonPush = PUSH_BUTTON_MPLAYER_STOP;
	// g_usleep (500000);
	VarMplayer.BoolIsPause = FALSE;
}
// 
// 
void mplayer_fifo_pause (void)
{
	// VarMplayer.TypeButtonPush = PUSH_BUTTON_MPLAYER_PAUSE;
	VarMplayer.BoolIsPause = TRUE;
	mplayer_set_fifo ("pause");
}
// 
// 
void mplayer_fifo_get_percent_pos (void)
{
	mplayer_set_fifo ("get_percent_pos");
}
// 
// 
void mplayer_fifo_get_time_pos (void)
{
	mplayer_set_fifo ("get_time_pos");
}
// 
// 
void mplayer_fifo_get_time_length (void)
{
	mplayer_set_fifo ("get_time_length");
}
// 
// 
void mplayer_fifo_seek (gdouble PercentValue)
{
	gchar *Str = NULL;
	guint sec;
	
	if (PercentValue < 0.0) PercentValue = 0.0;
	if (PercentValue > 100.0) PercentValue = 100.0;
	// VarMplayer.TypeButtonPush = PUSH_BUTTON_MPLAYER_PLAY;
	VarMplayer.BoolIsPause = FALSE;

	sec = (gint) ((VarMplayer.DOUBLE_TempsTotal * PercentValue) / 100.0);
	Str = g_strdup_printf ("seek %d 2", sec);
	mplayer_set_fifo (Str);
	g_free (Str);
	Str = NULL;
}
// 
// 
void mplayer_fifo_seek_with_hundr (gdouble PercentValue)
{
	gchar  *Mess = NULL;
	gint    sec;
	gdouble dsec;
	gint    hundr;
	
	if (PercentValue < 0.0) PercentValue = 0.0;
	if (PercentValue > 100.0) PercentValue = 100.0;
	// VarMplayer.TypeButtonPush = PUSH_BUTTON_MPLAYER_PLAY;
	VarMplayer.BoolIsPause = FALSE;
	
	sec = (gint) ((VarMplayer.DOUBLE_TempsTotal * PercentValue) / 100.0);
	dsec  = (VarMplayer.DOUBLE_TempsTotal * PercentValue) / 100.0;
	hundr = (dsec - (gdouble)sec) * 1000.0;
	if (hundr >= 1000) hundr = 999;
	Mess = g_strdup_printf ("seek %d.%d 2", sec, hundr);
	mplayer_set_fifo (Mess);
	g_free (Mess);
	Mess = NULL;
}
// THREAD
// 
static void mplayer_thread (void *arg)
{
#define MAX_BUF_MPLAYER 1024
	gint		 pos = 0;
	gint		 fd;
	gint		 size;
	gchar		 buf [ MAX_BUF_MPLAYER + 10 ];
	gchar		*Ptr = NULL;
	guint		 Sec;
	guint		 Hundr;
	gdouble		 SecondesTempsActuel = 0.0;
	gboolean         BoolEndRead = FALSE;
	// gboolean         BoolPass = FALSE;
	
	PRINT("DEBUT THREAD");
	VarMplayer.BoolThreadActivate = TRUE;

	fd = mplayer_call_exec (VarMplayer.PlayerArgs, &VarMplayer.CodeFork, STDOUT_FILENO);  // STDERR_FILENO  STDOUT_FILENO
	// 
	// mplayer -input cmdlist
	// 
	do {
		if (fd < 0) {
			VarMplayer.BoolErreurMplayer = TRUE;
			break;
		}

		pos = -1;
		do {			
			pos++;
			if (pos >= MAX_BUF_MPLAYER) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= CONV_MAX_CARS(%d)\n", pos, MAX_BUF_MPLAYER);
				pos --;
			}
			size = read (fd, &buf[pos], 1);
			
			// g_print ("size = %d\n",size);
			
			if (strstr(buf,"Audio: no sound")) {
				VarMplayer.BoolErreurMplayer = TRUE;
				g_print("\tAudio: no sound\n");
				break;
			}
			if (strstr(buf,"Cannot sync MAD frame")) {
				VarMplayer.BoolErreurMplayer = TRUE;
				g_print("\tCannot sync MAD frame\n");
				break;
			}
			if (strstr(buf,"Not a valid DCA frame")) {
				VarMplayer.BoolErreurMplayer = TRUE;
				g_print("\tNot a valid DCA frame\n");
				break;
			}

		} while (VarMplayer.BoolThreadActivate == TRUE && (buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		buf [ pos +2 ] = '\0';
		buf [ pos +3 ] = '\0';
		
		if (LIST_MPLAYER_FROM_CD == VarMplayer.ListPlayFrom) {
			// PRINT("LIST_MPLAYER_FROM_CD");
		}
		else if (LIST_MPLAYER_FROM_DVD == VarMplayer.ListPlayFrom) {
			// PRINT("LIST_MPLAYER_FROM_DVD");
		}
		
		
		if (VarMplayer.BoolErreurMplayer == TRUE) {
			break;
		}
		
		if (NULL != VarMplayer.Func_request_stop) {
			// SI DEMANDE DE STOP  EST VALIDE
			if (TRUE == (*VarMplayer.Func_request_stop)()) {
				BoolEndRead = TRUE;
				break;
			}
		}
		
		if (VarMplayer.ListPlayFrom == LIST_MPLAYER_FROM_CD) {
			if (strstr(buf,"A:") == NULL && VarMplayer.PercentTempsActuel > 0. && *buf == '\n') {
			}
			else if (strstr(buf,"A:")) {
			}
			else if (strstr(buf,"A:") == NULL) {
			}
		}

		// ANS_PERCENT_POSITION=8.0 (04:36.0)  0.8%
		//
		if (VarMplayer.ListPlayFrom == LIST_MPLAYER_FROM_DVD) {
			if ((Ptr = strstr(buf,"ANS_PERCENT_POSITION=")) != NULL) {
				while (*Ptr != '=') Ptr++;
				if (*Ptr == '=') Ptr++;
				if (atoi(Ptr) >= 99) {
					PRINT("FIN ET ARRET LECTURE DVD-AUDIO");
					BoolEndRead = TRUE;
					break;
				}
			}
		}

		// ANS_TIME_POSITION=0.0.0 (01:25.0) ??,?% 
		// ANS_TIME_POSITION=72.785.0 (01:25.0)  3.3% 
		if ((Ptr = strstr(buf,"ANS_TIME_POSITION=")) != NULL && strstr(buf,"??,?") == NULL) {
			while (*Ptr != '=') Ptr++;
			if (*Ptr == '=') Ptr++;
			Sec = atoi (Ptr);
			while (*Ptr != '.') Ptr++;
			if (*Ptr == '.') Ptr++;
			Hundr = atoi (Ptr);
			SecondesTempsActuel = (gdouble)Sec + ((gdouble)Hundr / 10.0);
			VarMplayer.PercentTempsActuel = (SecondesTempsActuel / VarMplayer.DOUBLE_TempsTotal) * 100.0;
						
			if (VarMplayer.ListPlayFrom != LIST_MPLAYER_FROM_DVD) {
				if (VarMplayer.PercentTempsActuel >=  99.855072) {
					BoolEndRead = TRUE;
					break;
				}
			}
			// BoolPass = TRUE;
		}
		
		// ANS_LENGTH=85.00of 85.0 (01:25.0)  2.7% 
		// 
		else if ((Ptr = strstr(buf,"ANS_LENGTH=")) != NULL) {
			while (*Ptr != '=') Ptr++;
			if (*Ptr == '=') Ptr++;
			Sec = atoi (Ptr);
			while (*Ptr != '.') Ptr++;
			if (*Ptr == '.') Ptr++;
			Hundr = atoi (Ptr);
			VarMplayer.DOUBLE_TempsTotal = (gdouble)Sec;
		}
		
		if (buf[pos] != '\n') {
			pos ++;
			buf[pos++] = '\n';
			buf[pos] = '\0';
		}
	} while (VarMplayer.BoolThreadActivate == TRUE && (size > 0));

	close(fd);
	
	if (TRUE == BoolEndRead ) {
		mplayer_set_fifo ("quit");
	}
	BoolEndRead = TRUE;
	
	VarMplayer.Button = -1;
	VarMplayer.BoolIsPause = FALSE;
	
	VarMplayer.BoolThreadActivate = FALSE;
	PRINT("FIN THREAD");

	
	if (VarMplayer.BoolErreurMplayer == TRUE) {
		g_print("\n");
		g_print("KILL Process mplayer = ");
		if ((kill (VarMplayer.CodeFork, SIGKILL) != 0))
			g_print ("ERREUR\n");
		else	g_print ("OK\n");
		g_print("\n");
	}

#undef MAX_BUF_MPLAYER

	pthread_exit (0);
}
// 
// TIMEOUT
// 
static gint mplayer_timeout (gpointer data)
{
	static gint cpt = -1;
	
	if (VarMplayer.BoolThreadActivate == TRUE) {
		if (TRUE == VarMplayer.BoolIsPause) return (TRUE);
		if (VarMplayer.Button != -1) return (TRUE);
		
		if (cpt ++ == 0) mplayer_fifo_get_percent_pos ();
		if (cpt >= 10) cpt = -1;

		switch (VarMplayer.ListPlayFrom) {
		case LIST_MPLAYER_FROM_NONE :
			break;
		
		case  LIST_MPLAYER_FROM_DVD :
			mplayer_fifo_get_time_pos ();
			break;
			
		case  LIST_MPLAYER_FROM_CD :
			if (VarMplayer.DOUBLE_TempsTotal == 0.0)
				mplayer_fifo_get_time_length ();
			else	mplayer_fifo_get_time_pos ();
			if (NULL != VarMplayer.FuncSetValueTime && VarMplayer.PercentTempsActuel > 0.0) (*VarMplayer.FuncSetValueTime) (VarMplayer.PercentTempsActuel);
			break;
		}
	}
	else if (VarMplayer.BoolThreadActivate == FALSE) {
		
		g_source_remove (VarMplayer.HandlerTimeout);

		// Widget de l'onglet dvd, cd ou fichiers sur ATTENTE
		switch (VarMplayer.ListPlayFrom) {
		case LIST_MPLAYER_FROM_NONE :
			break;
		case LIST_MPLAYER_FROM_DVD :
			VarMplayer.PercentTempsActuel = 0.0;
			if (VarMplayer.FuncSetValueTime) (*VarMplayer.FuncSetValueTime) (VarMplayer.PercentTempsActuel);
			if (VarMplayer.FuncWinClose) (*VarMplayer.FuncWinClose) ();
			if (VarMplayer.FuncIconeStop) (*VarMplayer.FuncIconeStop) ();
			break;
		case LIST_MPLAYER_FROM_CD :
			VarMplayer.PercentTempsActuel = 0.0;
			if (VarMplayer.FuncSetValueTime) (*VarMplayer.FuncSetValueTime) (VarMplayer.PercentTempsActuel);
			if (VarMplayer.FuncIconeStop) (*VarMplayer.FuncIconeStop) ();
			break;
		}
		
		VarMplayer.ListPlayFrom   = LIST_MPLAYER_FROM_NONE;
		
		mplayer_remove_fifo ();
		
		VarMplayer.BoolTimeoutActivate = FALSE;
		PRINT("FIN TIMEOUT");
	}
	return (TRUE);
}
// 
// MPLAYER INIT
// 
void mplayer_init (
	LIST_MPLAYER_FROM p_ListPlayFrom,
	gdouble p_TempsTotalSection,
	guint p_TempsTotalSurface,
	void *p_FuncWinClose,
	void *p_FuncSetValueTime, 
	void *p_FuncIconeStop,
	void *p_Func_request_stop
	)
{

	VarMplayer.PercentTempsActuel = -1.0;
	VarMplayer.DOUBLE_TempsTotal = p_TempsTotalSurface;		// ***
	VarMplayer.DOUBLE_TempsTotalSection = p_TempsTotalSection;	// ***
	VarMplayer.BoolIsPause = FALSE;
	VarMplayer.ListPlayFrom = p_ListPlayFrom;			// ***

	VarMplayer.BoolThreadActivate = FALSE;
	VarMplayer.NmrTid = 0;
	VarMplayer.BoolTimeoutActivate = FALSE;
	VarMplayer.HandlerTimeout = 0;
	VarMplayer.SignalNumchildren = 0;
	VarMplayer.CodeFork = 0;
	VarMplayer.BoolErreurMplayer = FALSE;
	
	VarMplayer.Button = -1;						// ***
	VarMplayer.FuncWinClose = p_FuncWinClose;			// ***
	VarMplayer.FuncSetValueTime = p_FuncSetValueTime;		// ***
	VarMplayer.FuncIconeStop = p_FuncIconeStop;			// ***
	VarMplayer.Func_request_stop = p_Func_request_stop;		// ***
}
// 
// EXTERNAL CALL
// 
void mplayer_set_list (GList *p_list)
{
	gint	 PosArg = 0;
	GList	*list = NULL;
	gchar	*Ptr = NULL;
	
	// PRINT_FUNC_LF();
		
	// DETRUIT MPLAYER VIA LE FIFO
	mplayer_fifo_quit ();
	
	// CREATE CONTROLE FILE MPLAYER VIA FIFO
	mplayer_create_fifo ();
		

	// DESTRUIT L ANCIENNE LIST D ARGUMENTS
	mplayer_remove_list_args ();

	PosArg = 0;
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("nice");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-n");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup_printf ("%d", Config.Nice);
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("mplayer");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-nojoystick");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-nolirc");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-nortc");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-noautosub");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-nomouseinput");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-slave");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-idle");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-cache");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("8192");
	list = g_list_first (p_list);
	while (list) {
		if (NULL != (Ptr = (gchar *)list->data)) {
			VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup (Ptr);
		}
		list = g_list_next (list);
	}
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup ("-input");
	VarMplayer.PlayerArgs[ PosArg++ ] = g_strdup_printf ("file=%s", XCFA_TMP_FIFO);
	VarMplayer.PlayerArgs[ PosArg++ ] = NULL;
	
	// LISTER LES ARGUMENTS SUR STDOUT
	g_print("\n");
	g_print("!-----------------------------------------------------------!\n");
	g_print("! READ WITH MPLAYER  \n");
	g_print("!-----------------------------------------------------------!\n");
	g_print("! ");
	// PRINT COMMAND LINE
	for (PosArg = 0; VarMplayer.PlayerArgs[ PosArg ] != NULL; PosArg ++) {
		g_print ("%s ", VarMplayer.PlayerArgs[ PosArg ]);
	}
	// PRINT COMMAND LINE IN STRUCT
	g_print("\n!-----------------------------------------------------------!\n");
	for (PosArg = 0; VarMplayer.PlayerArgs[ PosArg ] != NULL; PosArg ++) {
		g_print ("VarMplayer.PlayerArgs[ %02d ] = %s\n", PosArg, VarMplayer.PlayerArgs[ PosArg ]);
	}
	g_print("!-----------------------------------------------------------!\n\n");

	if (VarMplayer.PercentTempsActuel < 0.0) VarMplayer.PercentTempsActuel = 0.0;
	
	// ACTIVATE THREAD
	if (FALSE == VarMplayer.BoolThreadActivate) {
		pthread_create (&VarMplayer.NmrTid, NULL ,(void *)mplayer_thread, (void *)NULL);
	}
	
	// ACTIVATE TIMEOUT
	if (FALSE == VarMplayer.BoolTimeoutActivate) {
		VarMplayer.BoolTimeoutActivate = TRUE;
		PRINT("DEBUT TIMEOUT");
		VarMplayer.HandlerTimeout = g_timeout_add (100, mplayer_timeout, 0);
	}
}


gboolean mplayer_is_used (void)
{
	return (VarMplayer.BoolTimeoutActivate);
}








