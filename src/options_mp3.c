 /*
 *  file      : options_mp3.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "options.h"



static 	gchar *str_abr [] = {
	"-b 32",
	"-b 40",
	"-b 48",
	"-b 56",
	"-b 64",
	"-b 80",
	"-b 96",
	"-b 112",
	"-b 128",
	"-b 160",
	"-b 192",
	"-b 224",
	"-b 256",
	"-b 320"
	};
static 	gchar *str_cbr [] = {
	"-b 32",
	"-b 40",
	"-b 48",
	"-b 56",
	"-b 64",
	"-b 80",
	"-b 96",
	"-b 112",
	"-b 128",
	"-b 160",
	"-b 192",
	"-b 224",
	"-b 256",
	"-b 320",
	"--preset insane"
	};
static 	gchar *str_vbr [] = {
	"--preset medium",
	"--preset standard",
	"--preset extreme",
	"--preset fast standard",
	"--preset fast extreme",
	"-V0",
	"-V1",
	"-V2",
	"-V3",
	"-V4",
	"-V5",
	"-V6",
	"-V7",
	"-V8",
	"-V9"
	};
static 	gchar *str_vbr_new [] = {
	"--vbr-new",
	"--vbr-new --preset medium",
	"--vbr-new --preset standard",
	"--vbr-new --preset extreme",
	"--vbr-new --preset fast standard",
	"--vbr-new --preset fast extreme",
	"--vbr-new -V0",
	"--vbr-new -V1",
	"--vbr-new -V2",
	"--vbr-new -V3",
	"--vbr-new -V4",
	"--vbr-new -V5",
	"--vbr-new -V6",
	"--vbr-new -V7",
	"--vbr-new -V8",
	"--vbr-new -V9"
	};
static 	gchar *str_val_mode[] = {
	"",		// Defaut
	"-m s",		// Stereo
	"-m j",		// Join Stereo
	"-m f",		// Forced Join Stereo
	"-m d",		// Duo Channels
	"-m m"		// Mono
	};
static gchar StrLame[ 100 ];

gboolean bool_do_enter_options_mp3 = TRUE;




// 
// 
gchar *optionsLame_get_param( void )
{
	gchar StrLame[ 1000 ];
	
	StrLame [ 0 ] = '\0';

	switch (Config.BitrateLameIndice) {
	case 0 :
		strcpy (StrLame, str_abr [ Config.TabBitrateLame[ Config.BitrateLameIndice ] ]);		
		break;
	case 1 :
		strcpy (StrLame, str_cbr [ Config.TabBitrateLame[ Config.BitrateLameIndice ] ]);
		break;
	case 2 :
		strcpy (StrLame, str_vbr [ Config.TabBitrateLame[ Config.BitrateLameIndice ] ]);
		break;
	case 3 :
		strcpy (StrLame, str_vbr_new [ Config.TabBitrateLame[ Config.BitrateLameIndice ] ]);
		break;
	}
	if (Config.TabModeLame[ Config.BitrateLameIndice ] > 0) {
		if (*StrLame != '\0') strcat (StrLame, " ");
		strcat (StrLame, str_val_mode [ Config.TabModeLame[ Config.BitrateLameIndice ] ] );
	}
	if( *StrLame == '\0' ) return( (gchar *)NULL );
	return ((gchar *)strdup (StrLame));
}	

gchar *optionsLame_get_str_val_bitrate_abr_vbr_lame (void)
{
	gchar *ptr = NULL;

	strcpy (StrLame, "");
	if (var_options.Adr_Widget_Lame_bitrate == NULL || var_options.Adr_Widget_Lame_abr_cbr_vbr == NULL) return ((gchar *)ptr);

	switch (gtk_combo_box_get_active (var_options.Adr_Widget_Lame_abr_cbr_vbr)) {
	
	/* ABR */
	case 0 :
		// Config_User.RateMp3OggOptions.If_abr = gtk_combo_box_get_active (var_options.Adr_Widget_Lame_bitrate);
		ptr = (gchar *)str_abr [ Config.TabBitrateLame [ 0 ] ]; 
		break;
	
	/* CBR */
	case 1 :
		// Config_User.RateMp3OggOptions.If_cbr = gtk_combo_box_get_active (var_options.Adr_Widget_Lame_bitrate);
		ptr = (gchar *)str_cbr [ Config.TabBitrateLame [ 1 ] ];
		break;
	
	/* VBR */
	case 2 :
		// Config_User.RateMp3OggOptions.If_vbr = gtk_combo_box_get_active (var_options.Adr_Widget_Lame_bitrate);
		ptr = (gchar *)str_vbr [ Config.TabBitrateLame [ 2 ] ];
		break;
		
	/* VBR_NEW */
	case 3 :
		// Config_User.RateMp3OggOptions.If_vbr_new = gtk_combo_box_get_active (var_options.Adr_Widget_Lame_bitrate);
		ptr = (gchar *)str_vbr_new [ Config.TabBitrateLame [ 3 ] ];
		break;
	default :
		return ((gchar *)StrLame);
	}
	strcat (StrLame, (gchar *)ptr);
	return ((gchar *)StrLame);
}
gchar *optionsLame_get_str_val_mode_lame (void)
{	
	strcpy (StrLame, "");
	if (NULL == var_options.Adr_Widget_Lame_Mode) return ( (gchar *)str_val_mode[ 0 ]);
	strcpy (StrLame, str_val_mode[ gtk_combo_box_get_active (var_options.Adr_Widget_Lame_Mode) ]);
	return ((gchar *)StrLame);
}



void on_combobox_lame_abr_cbr_vbr_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "ABR");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "CBR");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "VBR");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "VBR-NEW");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.BitrateLameIndice);
	var_options.Adr_Widget_Lame_abr_cbr_vbr = GTK_COMBO_BOX (widget);
}

void on_combobox_lame_abr_cbr_vbr_changed (GtkComboBox *combobox, gpointer user_data)
{
	gint ind;
	
	if (NULL == var_options.Adr_Widget_Lame_abr_cbr_vbr) return;
	if (NULL == var_options.Adr_Widget_Lame_bitrate) return;
	if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_abr_cbr_vbr))) < 0) return;
	Config.BitrateLameIndice = ind;
	bool_do_enter_options_mp3 = FALSE;
	libcombo_remove_options (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_bitrate));
	
	switch (Config.BitrateLameIndice) {
	
	/* ABR 0 .. 13 */
	case 0 :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 32");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 40");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 48");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 56");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 64");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 80");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 96");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "112");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "128");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "160");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "192");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "224");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "256");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "320");
		break;
	
	/* CBR 0 .. 14 */
	case 1 :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 32");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 40");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 48");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 56");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 64");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 80");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " 96");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "112");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "128");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "160");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "192");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "224");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "256");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "320");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset insane");
		break;
	
	/* VBR 0 .. 14 */
	case 2 :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset medium");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset standard");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset extreme");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset fast standard");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset fast extreme");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), _("V0     (Best quality)"));
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V1");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V2");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V3");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V4");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V5");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V6");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V7");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V8");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), _("V9     (Poor quality)"));
		break;
	
	/* VBR_NEW 0 .. 15 */
	case 3 :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), " ");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset medium");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset standard");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset extreme");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset fast standard");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "preset fast extreme");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V0");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V1");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V2");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V3");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V4");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V5");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V6");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V7");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V8");
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_options.Adr_Widget_Lame_bitrate), "V9");
		break;
	}
	bool_do_enter_options_mp3 = TRUE;
	gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_bitrate), Config.TabBitrateLame [ Config.BitrateLameIndice ]);
	if (NULL != var_options.Adr_Widget_Lame_Mode)
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_Mode), Config.TabModeLame [ Config.BitrateLameIndice ]);
}


void on_combobox_lame_type_bitrate_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_Lame_bitrate = GTK_COMBO_BOX (widget);
	on_combobox_lame_abr_cbr_vbr_changed (NULL,NULL);
}
void on_combobox_lame_type_bitrate_changed (GtkComboBox *combobox, gpointer user_data)
{
	gint ind;
	
	if( FALSE == bool_do_enter_options_mp3 ) return;
	if (var_options.Adr_Widget_Lame_abr_cbr_vbr == NULL && var_options.Adr_Widget_Lame_bitrate == NULL) return;
	if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_bitrate))) < 0) return;
	Config.TabBitrateLame [ Config.BitrateLameIndice ] = ind;
	OptionsInternal_set_datas_interne (COLOR_LAME_DEBIT, var_options.Adr_label_lame_mp3, LAME_WAV_TO_MP3);
}


void on_combobox_lame_mode_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Defaut");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Stereo");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Join Stereo");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Forced Join Stereo");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Duo Channels");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Mono");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.TabModeLame[ Config.BitrateLameIndice ]);
	var_options.Adr_Widget_Lame_Mode = GTK_COMBO_BOX (widget);
	on_combobox_lame_abr_cbr_vbr_changed (NULL, NULL);
}
void on_combobox_lame_mode_changed (GtkComboBox *combobox, gpointer user_data)
{
	gint ind;
	
	if (var_options.Adr_Widget_Lame_Mode == NULL) return;
	if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_Lame_Mode))) < 0) return;
	Config.TabModeLame [ Config.BitrateLameIndice ] = ind;
	OptionsInternal_set_datas_interne (COLOR_LAME_MODE, var_options.Adr_label_lame_mp3, LAME_WAV_TO_MP3);
}













