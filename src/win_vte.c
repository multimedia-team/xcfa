/*
 * file      : win_vte.h
 * project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 * xcfa - Creation d'une base de programmation en langage C de type GNU avec les autotools
 * GNU General Public License
 * 
 * This file is part of XCFA.
 * 
 * XCFA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * 
 * XCFA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This module was inspired by GRIP
 *	http://nostatic.org/grip/
 */

#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#if defined(FILE_DEBIAN_VTE_H_IS_OK)
	#include <vte/vte.h>
#elif defined(FILE_ARCH_VTE_H_IS_OK)
	#include <vte-2.91/vte/vte.h>
#elif defined(FILE_FW_VTE_H_IS_OK)
	#include <vte-2.90/vte/vte.h>
#endif

#include "global.h"
#include "win_vte.h"




typedef struct {
	GtkWidget	*AdrWidget;
} VAR_VTE;

VAR_VTE	VarVte = { NULL };


// 
// WRITE a line of output to a status window
// 
void WinVte_window_write( gchar *msg )
{
	gchar	*buf;
	gsize	len;
	gint	pos = 0;

	len = strlen( msg );
	buf = (gchar *)g_malloc0(( len * 2 ) +1 );
	while( *msg ) {
		if( 1 ) {
			if( *msg == '\n' ) {
				buf[pos++] = '\r';
				buf[pos++] = '\n';
			} 
			else {
				buf[pos++]=*msg;
			}
		}
		msg++;
	}
	buf[pos] = '\0';
	vte_terminal_feed( VTE_TERMINAL(VarVte.AdrWidget), buf, strlen( buf ));
	g_free( buf );
	buf = NULL;
}
// PLACER DU TEXTE DANS LE TEXTVIEW
// 
void WinVte_window_write_va( gchar *p_text, ... )
{
	va_list	VaList;
	gchar	*Ptr = NULL;
	
	if( NULL != VarVte.AdrWidget ) {
		WinVte_window_write( p_text );
		va_start( VaList, p_text );
		while( NULL != ( Ptr = va_arg( VaList, gchar * ))) {
			 WinVte_window_write( Ptr );
		}
		va_end( VaList );
	}
}
// 
// VTE RESET
// 
void WinVte_reset( void )
{
	if( NULL != VarVte.AdrWidget ) {
		// vte_terminal_reset( VteTerminal *terminal, gboolean full, gboolean clear_history);
		vte_terminal_reset( VTE_TERMINAL(VarVte.AdrWidget), TRUE, TRUE );
		WinVte_window_write( "Init VTE is ok.\n" );
	}
}
// 
// REALIZE VTE
// 
void WinVte_realize( GtkWidget *widget )
{
	VarVte.AdrWidget = vte_terminal_new();
	vte_terminal_set_encoding( VTE_TERMINAL( VarVte.AdrWidget ),"UTF-8", NULL );
	gtk_container_add (GTK_CONTAINER (widget), VarVte.AdrWidget );
	gtk_widget_show_all( widget );
	WinVte_window_write( "Init VTE is ok.\n" );
}
// 
// REALIZE VTE ?
// 
gboolean WinVte_is_ok( void )
{
	return( NULL !=	VarVte.AdrWidget ? TRUE : FALSE );
}




