 /*
 *  file    : tags_flac.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/




/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/

INFO_FLAC *tagsflac_remove_info (INFO_FLAC *info)
{
	if (info) {
		g_free (info->time);

		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}
	return ((INFO_FLAC *)NULL);
}

INFO_FLAC *tagsflac_get_info (DETAIL *detail)
{
	TagLib_File	*file;
	TagLib_Tag	*tag;
	INFO_FLAC	*ptrinfo = NULL;
	const TagLib_AudioProperties *properties;
	gint		m;
	gint		s;
	gint		sec;

	ptrinfo = (INFO_FLAC *)g_malloc0 (sizeof (INFO_FLAC));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);

	if ((file = taglib_file_new (detail->namefile))) {

		taglib_set_strings_unicode(FALSE);
		tag = taglib_file_tag(file);
		properties = taglib_file_audioproperties(file);

		ptrinfo->tags->Title     = g_strdup (taglib_tag_title(tag));
		ptrinfo->tags->Artist    = g_strdup (taglib_tag_artist(tag));
		ptrinfo->tags->Album     = g_strdup (taglib_tag_album(tag));
		ptrinfo->tags->IntYear   = taglib_tag_year(tag);
		ptrinfo->tags->Year      = g_strdup_printf ("%d", ptrinfo->tags->IntYear);
		ptrinfo->tags->Comment   = g_strdup (taglib_tag_comment(tag));
		ptrinfo->tags->IntNumber = taglib_tag_track(tag);
		ptrinfo->tags->Number    = g_strdup_printf ("%d", ptrinfo->tags->IntNumber);
		ptrinfo->tags->Genre     = g_strdup (taglib_tag_genre(tag));
		ptrinfo->tags->IntGenre  = tags_get_genre_by_value (ptrinfo->tags->Genre);
		ptrinfo->SecTime         =  taglib_audioproperties_length(properties);

		sec = taglib_audioproperties_length(properties);
		s = sec % 60; sec /= 60;
		m = sec % 60; sec /= 60;
		if (sec > 0) ptrinfo->time = g_strdup_printf ("%02d:%02d:%02d", sec, m, s);
		else         ptrinfo->time = g_strdup_printf ("%02d:%02d", m, s);

		/*
		printf("title   - \"%s\"\n", taglib_tag_title(tag));
		printf("artist  - \"%s\"\n", taglib_tag_artist(tag));
		printf("album   - \"%s\"\n", taglib_tag_album(tag));
		printf("year    - \"%i\"\n", taglib_tag_year(tag));
		printf("comment - \"%s\"\n", taglib_tag_comment(tag));
		printf("track   - \"%i\"\n", taglib_tag_track(tag));
		printf("genre   - \"%s\"\n", taglib_tag_genre(tag));
		printf("-- AUDIO --\n");
		printf("bitrate     - %i\n", taglib_audioproperties_bitrate(properties));
		printf("sample rate - %i\n", taglib_audioproperties_samplerate(properties));
		printf("channels    - %i\n", taglib_audioproperties_channels(properties));
		printf("length      - %i:%02i\n", minutes, seconds);
		*/

		taglib_tag_free_strings();
		taglib_file_free (file);
	}

	return (ptrinfo);
}


