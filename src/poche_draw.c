 /*
 *  file      : poche_draw.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */




#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>	// M_PI
#include <cairo.h>
#include <cairo-pdf.h>
#include <cairo-ps.h>
#include <cairo-xlib.h>
#include <X11/Xlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cursor.h"
#include "dragNdrop.h"
#include "fileselect.h"
#include "configuser.h"
#include "poche.h"




// 
// 
gdouble pochedraw_get_scale_adjust( void )
{
	gdouble	return_scale = 1.0;
	GtkAllocation	allocation;
	
	if( TRUE == view.BoolScaleAdjust ) {
		gdouble	scale_width = 1.0;
		gdouble	scale_height = 1.0;
		gdouble	new_scale = 1.0;
		
		// La definition SCALE_MIN correspond au pourcentage de representation le plus petit de l'image de fond
		// SCALE_MIN doit etre adaptee suivant la taille la plus petite de la fenetre
		if( view.scale > SCALE_MIN ) {
			view.scale = SCALE_MIN;
			poche_set_size_request();
		}
		gtk_widget_get_allocation( view.AdrDrawingarea, &allocation );
		// scale_width  = (gdouble)view.AdrDrawingarea->allocation.width / (gdouble)view.image_width;
		scale_width  = (gdouble)allocation.width / (gdouble)view.image_width;
		// scale_height = (gdouble)view.AdrDrawingarea->allocation.height / (gdouble)view.image_height;
		scale_height = (gdouble)allocation.height / (gdouble)view.image_height;
		new_scale = MIN(scale_width, scale_height);
		return_scale = new_scale;
	}
	else {
		return_scale = view.scale;
	}
	poche_print_zoom_changed( return_scale );
	return( return_scale );
}
// 
// ECRITURE TEXTE
// 
void pochedraw_print_cairo_text( cairo_t *cr, IMAGE *p_Image )
{
	gchar			**Larrbuf = g_strsplit( p_Image->Texte, "\n", 0 );
	gdouble			LineY = 0.0 + (gdouble)p_Image->SizeFontName;
	gint			i;
	gchar			*Ptr = NULL;
	GString			*gstr = NULL;
	cairo_font_slant_t	Param_3;
	cairo_font_weight_t	Param_4;
	gchar			*StrOne = NULL;
	gchar			*StrTwo = NULL;
	gchar			*PtrStr = NULL;
	cairo_text_extents_t	extents;

	// PARAM 3
	// 
	// CAIRO_FONT_SLANT_NORMAL		Upright font style
	// CAIRO_FONT_SLANT_ITALIC		Italic font style
	// CAIRO_FONT_SLANT_OBLIQUE		Oblique font style
	if( TRUE == p_Image->BoolFontItalic )
		Param_3 = CAIRO_FONT_SLANT_ITALIC;
	else	Param_3 = CAIRO_FONT_SLANT_NORMAL;
	
	// PARAM 4
	// 
	// CAIRO_FONT_WEIGHT_NORMAL		Normal font weight
	// CAIRO_FONT_WEIGHT_BOLD		Bold font weight
	if( TRUE == p_Image->BoolFontBold )
		Param_4 = CAIRO_FONT_WEIGHT_BOLD;
	else	Param_4 = CAIRO_FONT_WEIGHT_NORMAL;
	
	cairo_select_font_face( cr, p_Image->FontName, Param_3, Param_4 );
	cairo_set_font_size( cr, ((gdouble)p_Image->SizeFontName  ));
	cairo_set_source_rgb( cr, 0.0, 0.0, 0.0 );
	cairo_text_extents( cr, "0", &extents );

	for( i = 0; Larrbuf [ i ]; i++ ) {
		
		gstr = g_string_new (NULL);
		
		for( Ptr = Larrbuf [ i ]; *Ptr; Ptr ++ ) {
			if( *Ptr == '\t' ) {
				gstr = g_string_append (gstr, "        ");
			}
			else {
				g_string_append_printf (gstr, "%c", *Ptr);
			}
		}
		// TEST SI PRESENCE OPTION DE DECOUPE: ][
		if( NULL != strstr( gstr->str, "][" )) {
			StrOne = g_strdup( gstr->str );
			PtrStr = strstr( StrOne, "][" );
			*PtrStr = '\0';
			PtrStr += 2;
			while( *PtrStr == ' ' ) PtrStr++;
			StrTwo  = g_strdup( PtrStr );
			cairo_move_to( cr, 0.0, LineY );
			cairo_show_text( cr, StrOne );
			// cairo_move_to( cr, p_Image->image_width - (3 *((gdouble)p_Image->SizeFontName)) - 2, LineY );
			cairo_move_to( cr, p_Image->image_width - (extents.width * 7), LineY );
			cairo_show_text( cr, StrTwo );
			g_free( StrOne );
			StrOne = NULL;
			g_free(StrTwo );
			StrTwo = NULL;
		}
		else {
			cairo_move_to( cr, 0.0, LineY );
			cairo_show_text( cr, gstr->str );
		}
		g_string_free( gstr, TRUE );
		LineY += (gdouble)p_Image->SizeFontName;
		LineY += 2;
	}
	g_strfreev( Larrbuf );
}
// 
// 
void pochedraw_print_cairo_title_text( cairo_t *cr, IMAGE *p_Image )
{
	gdouble	LocateX;
	gdouble	LocateY;
	
	LocateX = MM(18);
	LocateY = MM(172);
	cairo_save( cr );
	cairo_scale( cr, view.scale, view.scale );
	cairo_translate( cr, view.x0, view.y0 );
	cairo_select_font_face( cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD );
	cairo_set_font_size( cr, 6);
	cairo_set_source_rgb( cr, 0.0, 0.0, 0.0 );
	cairo_move_to( cr, LocateX,  LocateY );
	cairo_rotate( cr, -M_PI / 2.0 );
	cairo_show_text( cr, p_Image->Texte );
	cairo_restore (cr);
	
	LocateX = MM(111);
	LocateY = MM(172);
	cairo_save( cr );
	cairo_scale( cr, view.scale, view.scale );
	cairo_translate( cr, view.x0, view.y0 );
	cairo_select_font_face( cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD );
	cairo_set_font_size( cr, 6);
	cairo_set_source_rgb( cr, 0.0, 0.0, 0.0 );
	cairo_move_to( cr, LocateX,  LocateY );
	cairo_rotate( cr, -M_PI / 2.0 );
	cairo_show_text( cr, p_Image->Texte );
	cairo_restore (cr);
}
// 
// 
void pochedraw_object( cairo_t *cr, IMAGE *p_Image )
{
	if( _TYPE_IMAGE_ ==  p_Image->TypeImage ) {
		if( NULL != p_Image->Pixbuf ) {
			g_object_unref( p_Image->Pixbuf );
			p_Image->Pixbuf = NULL;
		}
		p_Image->Pixbuf = gdk_pixbuf_scale_simple( p_Image->PixbufOriginal, p_Image->image_width, p_Image->image_height, 2 );	// GDK_INTERP_NEAREST
	
		if( TRUE == p_Image->BoolFlipVertical ) {
			GdkPixbuf	*PixBuf = gdk_pixbuf_flip( p_Image->Pixbuf, FALSE );
			g_object_unref( p_Image->Pixbuf );
			p_Image->Pixbuf = NULL;
			p_Image->Pixbuf = PixBuf;
		}
		if( TRUE == p_Image->BoolFlipHorizontal ) {
			GdkPixbuf	*PixBuf = gdk_pixbuf_flip( p_Image->Pixbuf, TRUE );
			g_object_unref( p_Image->Pixbuf );
			p_Image->Pixbuf = NULL;
			p_Image->Pixbuf = PixBuf;
		}
	
		p_Image->image_width  = gdk_pixbuf_get_width (p_Image->Pixbuf);
		p_Image->image_height = gdk_pixbuf_get_height (p_Image->Pixbuf);
	
		cairo_scale( cr, view.scale, view.scale );
		cairo_translate( cr, view.x0 + p_Image->x0, view.y0 + p_Image->y0 );
		gdk_cairo_set_source_pixbuf (cr, (GdkPixbuf *)p_Image->Pixbuf, 0., 0. );
		cairo_paint( cr );
	}
	else if( _TYPE_TEXT_ == p_Image->TypeImage ) {
		
		cairo_scale( cr, view.scale, view.scale );
		cairo_translate( cr, view.x0 + p_Image->x0, view.y0 + p_Image->y0 );
		
		// LA SELECTION DU TEXTE EST DANS UNE BOITE
		if( TRUE == p_Image->BoolIsSelected ) {
			cairo_set_source_rgba( cr, 0.7, 0.7, 0.7, 0.30 );
			cairo_set_line_width( cr, 0.1 );
			cairo_rectangle( cr,
					 0,
					 0,
					 p_Image->image_width,
					 p_Image->image_height
					 );			
			cairo_clip (cr);
			cairo_new_path (cr);
			cairo_fill( cr );
		}
		// BOITE TRANSPARENTE NECESSAIRE POUR LE CLIP
		else {
			cairo_set_source_rgba( cr, 0.0, 0.0, 0.0, 0.0 );
			cairo_set_line_width( cr, 0.1 );
			cairo_rectangle( cr,
					 0,
					 0,
					 p_Image->image_width,
					 p_Image->image_height
					 );			
			cairo_clip (cr);
			cairo_new_path (cr);
		}
		
		// PRINT TEXTE
		pochedraw_print_cairo_text( cr, p_Image );
	}
	else if( _TYPE_TEXT_TITLE_ == p_Image->TypeImage ) {
		
		pochedraw_print_cairo_title_text( cr, p_Image );
	}
}
// 
// 
void pochedraw_get_handle_move( IMAGE *p_Image, gboolean p_BoolTestCursor )
{
	if( TRUE == p_BoolTestCursor ) {
		
		// POUR LES TESTS CURSEUR SOURIS
		
		view.HandleMove.zone [ IMG_HAUT_GAUCHE ] .x      =  p_Image->x0 -3;
		view.HandleMove.zone [ IMG_HAUT_GAUCHE ] .y      =  p_Image->y0 -3;
		view.HandleMove.zone [ IMG_HAUT_GAUCHE ] .width  =  6;
		view.HandleMove.zone [ IMG_HAUT_GAUCHE ] .height =  6;
	
		view.HandleMove.zone [ IMG_HAUT ] .x             = (p_Image->x0 + (p_Image->image_width / 2)) -3;
		view.HandleMove.zone [ IMG_HAUT ] .y             =  p_Image->y0 -3;
		view.HandleMove.zone [ IMG_HAUT ] .width         =  6;
		view.HandleMove.zone [ IMG_HAUT ] .height        =  6;
		
		view.HandleMove.zone [ IMG_HAUT_DROIT ] .x       = (p_Image->x0 + p_Image->image_width) -3;
		view.HandleMove.zone [ IMG_HAUT_DROIT ] .y       =  p_Image->y0 -3;
		view.HandleMove.zone [ IMG_HAUT_DROIT ] .width   =  6;
		view.HandleMove.zone [ IMG_HAUT_DROIT ] .height  =  6;
		
		view.HandleMove.zone [ IMG_DROIT ] .x            = (p_Image->x0 + p_Image->image_width) -3;
		view.HandleMove.zone [ IMG_DROIT ] .y            = (p_Image->y0 + (p_Image->image_height / 2)) -3;
		view.HandleMove.zone [ IMG_DROIT ] .width        =  6;
		view.HandleMove.zone [ IMG_DROIT ] .height       =  6;
		
		view.HandleMove.zone [ IMG_BAS_DROIT ] .x        = (p_Image->x0 + p_Image->image_width) -3;
		view.HandleMove.zone [ IMG_BAS_DROIT ] .y        = (p_Image->y0 + p_Image->image_height) -3;
		view.HandleMove.zone [ IMG_BAS_DROIT ] .width    =  6;
		view.HandleMove.zone [ IMG_BAS_DROIT ] .height   =  6;
		
		view.HandleMove.zone [ IMG_BAS ] .x              = (p_Image->x0 + (p_Image->image_width / 2)) -3;
		view.HandleMove.zone [ IMG_BAS ] .y              =  (p_Image->y0 + p_Image->image_height) -3;
		view.HandleMove.zone [ IMG_BAS ] .width          =  6;
		view.HandleMove.zone [ IMG_BAS ] .height         =  6;
		
		view.HandleMove.zone [ IMG_BAS_GAUCHE ] .x       =  p_Image->x0  -3;
		view.HandleMove.zone [ IMG_BAS_GAUCHE ] .y       = (p_Image->y0 + p_Image->image_height) -3;
		view.HandleMove.zone [ IMG_BAS_GAUCHE ] .width   =  6;
		view.HandleMove.zone [ IMG_BAS_GAUCHE ] .height  =  6;
		
		view.HandleMove.zone [ IMG_GAUCHE ] .x           =  p_Image->x0  -3;
		view.HandleMove.zone [ IMG_GAUCHE ] .y           = (p_Image->y0 + (p_Image->image_height / 2)) -3;
		view.HandleMove.zone [ IMG_GAUCHE ] .width       =  6;
		view.HandleMove.zone [ IMG_GAUCHE ] .height      =  6;
		
		view.HandleMove.zone [ IMG_MOVE ] .x             =  p_Image->x0;
		view.HandleMove.zone [ IMG_MOVE ] .y             =  p_Image->y0;
		view.HandleMove.zone [ IMG_MOVE ] .width         =  p_Image->image_width;
		view.HandleMove.zone [ IMG_MOVE ] .height        =  p_Image->image_height;
	}
	else {
		// POUR L AFFICHAGE DES RECTANGLES DE SAISIES
		
		view.HandleMove.zone [ IMG_HAUT_GAUCHE ] .x      = -3;
		view.HandleMove.zone [ IMG_HAUT_GAUCHE ] .y      = -3;
		view.HandleMove.zone [ IMG_HAUT_GAUCHE ] .width  =  6;
		view.HandleMove.zone [ IMG_HAUT_GAUCHE ] .height =  6;
	
		view.HandleMove.zone [ IMG_HAUT ] .x             = (p_Image->image_width / 2) -3;
		view.HandleMove.zone [ IMG_HAUT ] .y             = -3;
		view.HandleMove.zone [ IMG_HAUT ] .width         =  6;
		view.HandleMove.zone [ IMG_HAUT ] .height        =  6;
		
		view.HandleMove.zone [ IMG_HAUT_DROIT ] .x       =  p_Image->image_width -3;
		view.HandleMove.zone [ IMG_HAUT_DROIT ] .y       = -3;
		view.HandleMove.zone [ IMG_HAUT_DROIT ] .width   =  6;
		view.HandleMove.zone [ IMG_HAUT_DROIT ] .height  =  6;
		
		view.HandleMove.zone [ IMG_DROIT ] .x            =  p_Image->image_width -3;
		view.HandleMove.zone [ IMG_DROIT ] .y            = (p_Image->image_height / 2) -3;
		view.HandleMove.zone [ IMG_DROIT ] .width        =  6;
		view.HandleMove.zone [ IMG_DROIT ] .height       =  6;
		
		view.HandleMove.zone [ IMG_BAS_DROIT ] .x        = p_Image->image_width -3;
		view.HandleMove.zone [ IMG_BAS_DROIT ] .y        = p_Image->image_height -3;
		view.HandleMove.zone [ IMG_BAS_DROIT ] .width    =  6;
		view.HandleMove.zone [ IMG_BAS_DROIT ] .height   =  6;
		
		view.HandleMove.zone [ IMG_BAS ] .x              = (p_Image->image_width / 2) -3;
		view.HandleMove.zone [ IMG_BAS ] .y              =  p_Image->image_height -3;
		view.HandleMove.zone [ IMG_BAS ] .width          =  6;
		view.HandleMove.zone [ IMG_BAS ] .height         =  6;
		
		view.HandleMove.zone [ IMG_BAS_GAUCHE ] .x       = -3;
		view.HandleMove.zone [ IMG_BAS_GAUCHE ] .y       =  p_Image->image_height -3;
		view.HandleMove.zone [ IMG_BAS_GAUCHE ] .width   =  6;
		view.HandleMove.zone [ IMG_BAS_GAUCHE ] .height  =  6;
		
		view.HandleMove.zone [ IMG_GAUCHE ] .x           = -3;
		view.HandleMove.zone [ IMG_GAUCHE ] .y           = (p_Image->image_height / 2) -3;
		view.HandleMove.zone [ IMG_GAUCHE ] .width       =  6;
		view.HandleMove.zone [ IMG_GAUCHE ] .height      =  6;
		
		view.HandleMove.zone [ IMG_MOVE ] .x             =  0;
		view.HandleMove.zone [ IMG_MOVE ] .y             =  0;
		view.HandleMove.zone [ IMG_MOVE ] .width         =  p_Image->image_width;
		view.HandleMove.zone [ IMG_MOVE ] .height        =  p_Image->image_height;
	}
}
// 
// 
void pochedraw_set_handle( cairo_t *cr )
{
	gint	i;
	
	// EPAISSEUR DU TRAIT
	cairo_set_line_width( cr, 0.3 );
	
	// SET RECTANGLES TO GREEN COLOR
	cairo_set_source_rgba( cr, 0.0, 1.0, 0.0, 0.30 );
	for( i = 0; i < IMG_SIZE -1; i ++ ) {
		cairo_rectangle( cr,
				 view.HandleMove.zone [ i ] .x,
				 view.HandleMove.zone [ i ] .y,
				 view.HandleMove.zone [ i ] .width,
				 view.HandleMove.zone [ i ] .height
				 );
	}
	// FILL IN GREEN COLOR
	cairo_fill( cr );
	
	// SET RECTANGLES TO BLACK COLOR
	cairo_set_source_rgb( cr, 0, 0, 0 );
	for( i = 0; i < IMG_SIZE -1; i ++ ) {
		cairo_rectangle( cr,
				 view.HandleMove.zone [ i ] .x,
				 view.HandleMove.zone [ i ] .y,
				 view.HandleMove.zone [ i ] .width,
				 view.HandleMove.zone [ i ] .height
				 );
	}
	
	// SET RECTANGLE AROUND TO BLACK COLOR
	cairo_set_source_rgb( cr, 0, 0, 0 );
	cairo_rectangle( cr,
			 view.HandleMove.zone [ i ] .x,
			 view.HandleMove.zone [ i ] .y,
			 view.HandleMove.zone [ i ] .width,
			 view.HandleMove.zone [ i ] .height
			 );
}
/*

		HAUT RECTO
		!-----------!
		!           !
  GAUCHE RECTO	!           !  DROITE RECTO
		!           !
		!           !
		!-----------!
		BAS RECTO
	   

		HAUT VERSO
		!--!-----------!--!---!
		!  !           !  !   !
		V1 !           !  !   !
		!  V2          !  !   !
		!  !           V3 !   !
		!  !           !  V4  !
		!  !           !  !   V5
		!  !           !  !   !
		!--!-----------!--!---!
		BAS VERSO
	
*/
void pochedraw_set_format( cairo_t *cr )
{
	gdouble	LocateX;
	gdouble	LocateY;
	gchar	*PackageVersion = NULL;
	
	cairo_scale( cr, view.scale, view.scale );
	cairo_translate( cr, view.x0, view.y0 );
	cairo_set_source_rgb( cr, 0.0, 0.0, 0.0 );
	// cairo_set_source_rgb( cr, red/255.0, green/255.0, blue/255.0 );
	if( FALSE == view.BoolSaveToFile ) {
		// CONTOUR DE LA FEUILLE
		// EPAISSEUR
		cairo_set_line_width( cr, 0.5 );
		// HAUT
		cairo_move_to( cr, 0.0, 0.0 );
		cairo_rel_line_to( cr, view.image_width, 0.0 );
		// GAUCHE
		cairo_move_to( cr, 0.0, 0.0 );
		cairo_rel_line_to( cr, 0.0,  view.image_height );
		cairo_stroke ( cr );
		// EPAISSEUR
		cairo_set_line_width( cr, 1.5 );
		// DROITE
		cairo_move_to( cr, view.image_width -1, 0.0 );
		cairo_rel_line_to( cr, 0.0,  view.image_height );
		// BAS
		cairo_move_to( cr, 0.0,  view.image_height -1 );
		cairo_rel_line_to( cr, view.image_width, 0.0 );
		cairo_stroke ( cr );
		// EPAISSEUR
		cairo_set_line_width( cr, 0.3 );
	}
	else {
		cairo_set_line_width( cr, 0.1 );
	}	
	
	// HAUT RECTO
	cairo_move_to( cr, MM(17), MM(10) );
	cairo_rel_line_to( cr, MM(83), 0 );
	// GAUCHE RECTO
	cairo_move_to( cr, MM(19), MM(7) );
	cairo_rel_line_to( cr, 0, MM(84) );
	// DROITE RECTO
	cairo_move_to( cr, MM(97), MM(7) );
	cairo_rel_line_to( cr, 0, MM(84) );
	// BAS RECTO
	cairo_move_to( cr, MM(17), MM(88) );
	cairo_rel_line_to( cr, MM(83), 0 );
	
	// HAUT VERSO
	cairo_move_to( cr, MM(13), MM(98) );
	cairo_rel_line_to( cr, MM(107), 0 );
	// V1 VERSO
	cairo_move_to( cr, MM(15), MM(95) );
	cairo_rel_line_to( cr, 0, MM(82) );
	// V2 VERSO
	cairo_move_to( cr, MM(19), MM(95) );
	cairo_rel_line_to( cr, 0, MM(82) );
	// V3 VERSO
	cairo_move_to( cr, MM(108), MM(95) );
	cairo_rel_line_to( cr, 0, MM(82) );
	// V4 VERSO
	cairo_move_to( cr, MM(112), MM(95) );
	cairo_rel_line_to( cr, 0, MM(82) );
	// V5 VERSO
	cairo_move_to( cr, MM(118), MM(95) );
	cairo_rel_line_to( cr, 0, MM(82) );
	// BAS VERSO
	cairo_move_to( cr, MM(13), MM(174) );
	cairo_rel_line_to( cr, MM(107), 0 );
	cairo_stroke ( cr );

	cairo_save( cr );
	cairo_select_font_face( cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL );
	cairo_set_font_size( cr, 4.5);
	cairo_set_source_rgb( cr, 0.0, 0.0, 0.0 );
	LocateX = MM(112);
	LocateY = MM(93);
	cairo_move_to( cr, LocateX,  LocateY );
	cairo_rotate( cr, -M_PI / 2.0 );
	cairo_show_text( cr, "<-- cut here, if you don't need a tongue --- Couper ici si vous n'avez pas besoin de cette bordure" );
	cairo_restore (cr);

	cairo_save( cr );
	cairo_select_font_face( cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL );
	cairo_set_font_size( cr, 5);
	cairo_set_source_rgb( cr, 0.0, 0.0, 0.0 );
	LocateX = MM(21);
	LocateY = MM(177);
	cairo_move_to( cr, LocateX,  LocateY );
	
	PackageVersion = g_strdup_printf ("X Convert File Audio %s  -  GNU General Public License  -  2003..2015, Claude Bulin  -  fr", VERSION);
	cairo_show_text( cr, PackageVersion );
	// cairo_show_text( cr, "ZoomC 0.0.19  GNU General Public License  -  2003 - 2015, Claude Bulin  -  fr" );
	g_free( PackageVersion );
	PackageVersion = NULL;
	cairo_restore (cr);
}
cairo_t *pochedraw_paint( cairo_surface_t *cs, cairo_t *p_cr )
{
	cairo_t		*cr;
	gint		canvas_w, canvas_h;
	GList		*List = NULL;
	IMAGE		*Image = NULL;
	IMAGE		*ImageSelected = NULL;
	GtkAllocation	allocation;
	
	if( TRUE == view.BoolSaveToFile ) {
		// cr = gdk_cairo_create( gtk_widget_get_window(view.AdrDrawingarea) );
		cr = cairo_create( cs );
	}
	else {
		// cr = cairo_create( cs );
		cr = p_cr;
	}
	// cairo_save (cr);
	view.scale = pochedraw_get_scale_adjust();
	gtk_widget_get_allocation( view.AdrDrawingarea, &allocation );
	
	// gdk_drawable_get_size (view.AdrDrawingarea->window, &canvas_w, &canvas_h );
	// gdk_drawable_get_size( gtk_widget_get_window( view.AdrDrawingarea ), &canvas_w, &canvas_h );
	// canvas_w = view.AdrDrawingarea->allocation.width;
	canvas_w = allocation.width;
	// canvas_h = view.AdrDrawingarea->allocation.height;
	canvas_h = allocation.height;
	// canvas_w = view.image_width;
	// canvas_h = view.image_height;
	
	view.x0 = ((canvas_w / view.scale) - view.image_width) / 2.0;
	view.y0 = ((canvas_h / view.scale) - view.image_height) / 2.0;

	cairo_save (cr);
	cairo_scale( cr, view.scale, view.scale );
	cairo_translate( cr, view.x0, view.y0 );
	cairo_set_source_surface( cr, view.image, 0.0, 0.0 );
	cairo_paint( cr );


	// PAINT PREVIEW SURFACE
	cairo_set_source_rgb( cr, 1.0, 1.0, 1.0 );
	// cairo_set_source_rgb( cr, 0.0, 0.0, 0.0 );
	cairo_rectangle( cr,
			 0,
			 0,
			 view.image_width,
			 view.image_height
			 );
	cairo_fill( cr );

	cairo_restore (cr);
	// 
	// Image[ n ]		image en dessous  / image below
	// Image[ n +1 ]	
	// Image[ n +2 ]	
	// Image[ n +3 ]	
	// Image[ n +4 ]	image au dessus / image above
	// 
	cairo_save (cr);
	List = g_list_first( view.ListImage );
	while( List ) {
		if( NULL != (Image = (IMAGE *)List->data ) && FALSE == Image->BoolStructRemove ) {
			cairo_save (cr);
			pochedraw_object( cr , Image );
			if( TRUE == Image->BoolIsSelected ) ImageSelected = Image;
			cairo_restore (cr);
		}
		List = g_list_next( List );
	}
	if( NULL != ImageSelected && TRUE == ImageSelected->BoolIsSelected && FALSE == view.BoolSaveToFile ) {
		cairo_scale( cr, view.scale, view.scale );
		cairo_translate( cr, view.x0 + ImageSelected->x0, view.y0 + ImageSelected->y0 );
		cairo_set_source_rgba( cr, 0.0, 0.0, 0.0, 0.30 );
		cairo_translate( cr, 0.0, 0.0 );
		pochedraw_get_handle_move( ImageSelected, FALSE );
		pochedraw_set_handle( cr );
		cairo_stroke ( cr );
	}
	cairo_restore (cr);
	pochedraw_set_format( cr );
	
	return( cr );
	// cairo_destroy( cr );
}


