 /*
 *  file      : win_scan.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "win_vte.h"
#include "win_scan.h"



typedef struct {

	GtkWidget	*AdrProgressBar;
	GtkWidget	*AdrLabel;
	guint		HandlerTimeout;
	TYPE_PROGRESS	TypeProgress;
	gboolean	close_request;
	gboolean	CloseByUser;

} VAR_WINDSCAN;

VAR_WINDSCAN VarWindScan = {
	NULL,
	NULL,
	0,
	WINDSCAN_PULSE,
	FALSE
};


GtkBuilder	*GtkBuilderProjet_wind_scan = NULL;
GtkWidget	*WindMain_wind_scan = NULL;
// GtkWidget	*WindMain
// 
// 
void on_label_windscan_realize (GtkWidget *widget, gpointer user_data)
{
	VarWindScan.AdrLabel = widget;
}
// 
// 
void on_progressbar_windscan_realize (GtkWidget *widget, gpointer user_data)
{
	VarWindScan.AdrProgressBar = widget;
}
// 
// 
void WindScan_quit (void)
{
	VarWindScan.close_request = TRUE;
}
// 
// 
gboolean on_WindScan_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	VarWindScan.close_request = TRUE;
	return TRUE;
}
// 
// 
gboolean on_WindScan_destroy_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	VarWindScan.close_request = TRUE;
	return TRUE;
}
void on_button_cancel_windscan_clicked (GtkButton *button, gpointer user_data)
{
	VarWindScan.close_request = TRUE;
	VarWindScan.CloseByUser = TRUE;
}
// 
// 
static gint WindScan_timeout (gpointer data)
{
	if( TRUE == VarWindScan.close_request ) {
		VarWindScan.close_request = FALSE;
		g_source_remove (VarWindScan.HandlerTimeout);
		gtk_widget_destroy( WindMain_wind_scan );
		WindMain_wind_scan = NULL;
		GtkBuilderProjet_wind_scan = NULL;
	}
	else {
		if (VarWindScan.TypeProgress == WINDSCAN_PULSE) {
			gtk_progress_bar_pulse (GTK_PROGRESS_BAR(VarWindScan.AdrProgressBar));
		}
	}
	return (TRUE);
}
// 
// 
gboolean wind_scan_init( GtkWidget *FromWindMain, gchar *title, TYPE_PROGRESS TypeProgress )
{
	VarWindScan.CloseByUser = FALSE;
	VarWindScan.close_request = FALSE;
	if( NULL == (GtkBuilderProjet_wind_scan = Builder_open( "xcfa_win_scan.glade", GtkBuilderProjet_wind_scan ))) {
		return( EXIT_FAILURE );
	}
	// Add language support from glade file
	gtk_builder_set_translation_domain(  GtkBuilderProjet_wind_scan, "WindScan" );
	gtk_builder_connect_signals( GtkBuilderProjet_wind_scan, NULL );

	WindMain_wind_scan = GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_scan, "WindScan" ));
	// button_cancel_windscan=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_scan, "button_cancel_windscan" ));
	// label_windscan=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_scan, "label_windscan" ));
	// progressbar_windscan=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_scan, "progressbar_windscan" ));
	// scrolledwindow_windscan=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_scan, "scrolledwindow_windscan" ));

	if( NULL != FromWindMain ) {
		gtk_window_set_transient_for (GTK_WINDOW(WindMain_wind_scan), GTK_WINDOW(FromWindMain));
		gtk_window_set_modal (GTK_WINDOW(WindMain_wind_scan), TRUE);
	}
	// resize window
	// gtk_window_resize(GTK_WINDOW(WindMain_wind_scan), Config.WinWidth, Config.WinHeight);
	// place the window
	// gtk_window_move(GTK_WINDOW(WindMain_wind_scan), Config.WinPos_X, Config.WinPos_Y);
	// icon apply
	libutils_set_default_icone_to_win( WindMain_wind_scan );
	gtk_window_set_title( GTK_WINDOW( WindMain_wind_scan ), title );

	// place your initialization code here

	gtk_widget_show_all( WindMain_wind_scan );
	WindScan_hide_expander ();
	if (VarWindScan.TypeProgress == WINDSCAN_PULSE) {
		gtk_progress_bar_set_ellipsize (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), PANGO_ELLIPSIZE_START);
		gtk_progress_bar_set_pulse_step (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), 0.1);
		gtk_progress_bar_pulse (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar));
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), "");
	}
	else {
		gtk_orientable_set_orientation( GTK_ORIENTABLE(VarWindScan.AdrProgressBar), GTK_ORIENTATION_HORIZONTAL);
		// gtk_orientable_set_orientation( GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), GTK_PROGRESS_LEFT_TO_RIGHT );
		// gtk_progress_bar_set_orientation (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), GTK_PROGRESS_LEFT_TO_RIGHT );
		gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), 0.0);
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), _("Waiting ..."));
	}
	WindScan_show_cancel( TRUE );
	VarWindScan.HandlerTimeout = g_timeout_add( 200, WindScan_timeout, 0 );

	return( TRUE );
}









// 
// 
void WindScan_set_pulse (void)
{
	VarWindScan.TypeProgress = WINDSCAN_PULSE;
	gtk_progress_bar_set_ellipsize (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), PANGO_ELLIPSIZE_START);
	gtk_progress_bar_set_pulse_step (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), 0.1);
	gtk_progress_bar_pulse (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar));
}
// 
// 
void WindScan_set_progress (gchar *p_str, double p_etat)
{
	VarWindScan.TypeProgress = WINDSCAN_PROGRESS;
	// gtk_progress_bar_set_orientation (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), GTK_PROGRESS_LEFT_TO_RIGHT);
	gtk_orientable_set_orientation( GTK_ORIENTABLE(VarWindScan.AdrProgressBar), GTK_ORIENTATION_HORIZONTAL);
	if (p_etat < 0.0)	p_etat = 0.0;
	if (p_etat > 1.0)	p_etat = 1.0;
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), p_etat);
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), p_str);
}
// 
// 
gboolean WindScan_close_request (void)
{
	return (VarWindScan.CloseByUser);
}
// 
// 
void WindScan_close (void)
{
	WindScan_quit ();
}
// 
// 
void WindScan_set_label_bar (gchar *p_str)
{
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (VarWindScan.AdrProgressBar), p_str);
}
// 
// 
void WindScan_set_label (gchar *p_str)
{
	gtk_label_set_justify (GTK_LABEL (VarWindScan.AdrLabel),GTK_JUSTIFY_LEFT);
	gtk_label_set_label (GTK_LABEL (VarWindScan.AdrLabel), p_str);
}
// 
// 
void WindScan_hide_expander (void)
{
	gtk_widget_hide (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_scan, "expander_windscan")));
}
// 
// 
void WindScan_show_expander (void)
{
	gtk_widget_show (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_scan, "expander_windscan")));
	gtk_expander_set_expanded (GTK_EXPANDER (gtk_builder_get_object( GtkBuilderProjet_wind_scan, "expander_windscan")), TRUE);
}
// 
// 
void WindScan_show_cancel( gboolean p_bool_show )
{
	gtk_widget_set_sensitive( GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_scan, "button_cancel_windscan" )), p_bool_show );
}
// 
// 
void on_scrolledwindow_windscan_realize( GtkWidget *widget, gpointer user_data )
{
	WinVte_realize( widget );
}
