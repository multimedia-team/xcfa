 /*
 *  file      : web.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */

 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "win_info.h"
#include "extra.h"
#include "configuser.h"
#include "web.h"
#include "cd_curl.h"



typedef struct {
	gchar *Navigateur;
	gchar *NameNavi;
	gchar *TemporaryRep;
} VAR_WEB ;

VAR_WEB VarWeb = { NULL, NULL, NULL };




// 
// 
void web_remove (void)
{
	if (VarWeb.Navigateur != NULL) {
		g_free (VarWeb.Navigateur);
		VarWeb.Navigateur = NULL;
	}
	
	if (VarWeb.NameNavi != NULL) {
		g_free (VarWeb.NameNavi);
		VarWeb.NameNavi = NULL;
	}
}
// Appelle un navigateur pour aller sur le site d'XCFA et cela en thread
// 
void web_call_navigator (void)
{
	pid_t  pid;

	if ((pid = fork ()) == 0) {
		/*
		// kfmclient newTab http://www.xcfa.tuxfamily.org &
		if (strcmp (VarWeb.Navigateur, "konqueror") == 0) {
			execlp ("kfmclient",
				"kfmclient",
				"newTab",
				VarWeb.NameNavi,
				NULL);
		}
		else if (strcmp (VarWeb.Navigateur, "opera") == 0) {
			execlp (VarWeb.Navigateur,
				VarWeb.Navigateur,
				VarWeb.NameNavi,
				NULL);
		}
		else if (strcmp (VarWeb.Navigateur, "midori") == 0) {
			execlp (VarWeb.Navigateur,
				VarWeb.Navigateur,
				VarWeb.NameNavi,
				NULL);
		}
		else if( (strcmp (VarWeb.Navigateur, "firefox") == 0) ||
			  (strcmp (VarWeb.Navigateur, "iceweasel") == 0) ) {

			// -new-window URL
			// 	Open URL in a new window in an already running Firefox  process.
			// 	-new-tab URL Open URL in a new tab in an already running Firefox process.
			// EXEMPLE:
			// 	firefox -new-window -new-tab http:/www.google.fr
			execlp (VarWeb.Navigateur,
				VarWeb.Navigateur,
				"-new-window",
				"-new-tab",
				VarWeb.NameNavi,
				NULL);
		}
		else {
			gchar	Str [ 200 ];
			
			Str [ 0 ] = '\0';
			if( NULL != Config.StringParamNameNavigateur && *Config.StringParamNameNavigateur != '\0' ) {
				strncpy( Str, Config.StringParamNameNavigateur, 180 );
			}
			execlp (VarWeb.Navigateur,
				VarWeb.Navigateur,
				VarWeb.NameNavi,
				Str,
				NULL);
		}
		*/
		gchar	Str [ 200 ];
		
		Str [ 0 ] = '\0';
		if( NULL != Config.StringParamNameNavigateur && *Config.StringParamNameNavigateur != '\0' ) {
			strncpy( Str, Config.StringParamNameNavigateur, 180 );
		}
		execlp (VarWeb.Navigateur,
			VarWeb.Navigateur,
			VarWeb.NameNavi,
			Str,
			NULL);
		
		_exit (0);
	}
}
// 
// 
static void web_thread (void *arg)
{
	web_call_navigator ();
	pthread_exit(0);
}
// Fonction commune d'appel a un navigateur pour aller sur le site d'XCFA
// 
void web_goto_url (gchar *str_web)
{
	pthread_t   nmr_tid;
 	
	// PRINT_FUNC_LF();
	
	if (FALSE == extra_get_navigateur_is_ok ()) {
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_PRGEXTERNES);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_app_externes")), NOTEBOOK_OPTIONS_EXTRA);
		
		wind_info_init (
			WindMain,
			_("No web browser"),
			_("No Web access: no web browser"),
			  "\n",
			_("found in your system."),
			  "\n\n",
			_("Please install a web browser."),
			  "");
		return;
	}

	if (CdCurl_test_access_web () == FALSE) {
		
		wind_info_init (
			WindMain,
			_("A web connection is missing"),
			_("You need to enable web access"),
			  "");
		return;
	}
	
	web_remove ();
	VarWeb.Navigateur = g_strdup (extra_get_name_navigateur ());
	VarWeb.NameNavi   = g_strdup (str_web);
	
	pthread_create (&nmr_tid, NULL ,(void *)web_thread, (void *)NULL);
	sleep(5);
}
// 
// 
void web_goto_manpage (gchar *NameManPage)
{
	pthread_t	nmr_tid;
	gchar		*LineCommand = NULL;
	gchar		*PathNameHtml = NULL;
	FILE		*fp_html = NULL;
	FILE		*fp_popen = NULL;
	gchar 		buf_popen[ 2048 ];
	gchar		*Ptr = NULL;
	gboolean	bool_file_man_is_ok = FALSE;
	
	// PRINT_FUNC_LF();

	if (FALSE == extra_get_navigateur_is_ok ()) {
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_PRGEXTERNES);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_app_externes")), NOTEBOOK_OPTIONS_EXTRA);
		
		wind_info_init (
			WindMain,
			_("No web browser"),
			_("No Web access: no web browser"),
			  "\n",
			_("found in your system."),
			  "\n\n",
			_("Please install a web browser."),
			  "\n",
			  "");
		return;
	}

	if (libutils_find_file (NameManPage) == FALSE) {
		g_print("The application is missing, you must install [ %s ]\n", NameManPage);
		wind_info_init (
			WindMain,
			_("Application is missing."),
			_("Please install : "), "<b>", NameManPage, "</b>",
			  "\n",
			  "");
		return;
	}
		
	// INIT DOSSIER TEMPORAIRE
	if (NULL == VarWeb.TemporaryRep)  {
		VarWeb.TemporaryRep  = libutils_create_temporary_rep (Config.PathnameTMP, PATH_MANPAGE);
	}
	
	// INIT var
	web_remove ();
	VarWeb.Navigateur = g_strdup (extra_get_name_navigateur ());
	VarWeb.NameNavi   = g_strdup_printf ("%s/%s.html", VarWeb.TemporaryRep, NameManPage);
	
	// IF VarWeb.NameNavi NOT EXIST
	if (FALSE == libutils_test_file_exist (VarWeb.NameNavi)) {	
				
		LineCommand = g_strdup_printf ("man %s", NameManPage );
		// g_print("LineCommand = %s\n", LineCommand );
		PathNameHtml = g_strdup_printf ("%s/%s.html", VarWeb.TemporaryRep, NameManPage);
		fp_html = fopen (PathNameHtml, "w");
		
		g_fprintf (fp_html, "<html>\n");
		g_fprintf (fp_html, "<head>\n");
		g_fprintf (fp_html, "  <title>%s</title>\n", NameManPage);
		g_fprintf (fp_html, "</head>\n");
		g_fprintf (fp_html, "<body bgcolor=\"#ffffff\" text=\"#000000\">\n");
		g_fprintf (fp_html, "<pre>\n");
		
		if( (fp_popen = popen( LineCommand, "r" )) == NULL ) { 
			// perror( "popen error" );
			// exit( 1 );
			g_fprintf( fp_html, "\nERROR: FILE *popen(const char *command, const char *type);\n" );
			g_fprintf( fp_html, "\n" );
		}
		else {
			bool_file_man_is_ok = FALSE;
			while( fgets( buf_popen, sizeof(buf_popen)-1, fp_popen ) != NULL ) {
				bool_file_man_is_ok = TRUE;
				for( Ptr = buf_popen; *Ptr; Ptr++ ) {
					if( *Ptr >= 0 ) {
						if( *Ptr == '<' ) {
							g_fprintf( fp_html, "&lt;" );
						}
						else if( *Ptr == '>' ) {
							g_fprintf( fp_html, "&gt;" );
						}
						else {
							g_fprintf( fp_html, "%c", *Ptr );
						}
					}
				}
			}
			pclose( fp_popen );
			if( FALSE == bool_file_man_is_ok ) {
				g_fprintf( fp_html, "\n" );
				g_fprintf( fp_html, "Aucune entree de manuel pour aacplusenc\n" );
				g_fprintf( fp_html, "No manual entry for aacplusenc\n" );
				g_fprintf( fp_html, "\n" );
			}
		}
		g_fprintf( fp_html, "</pre>\n" );
		g_fprintf( fp_html, "</body>\n" );
		g_fprintf( fp_html, "</html>\n\n" );
		fclose( fp_html );
		
		g_free (PathNameHtml);	PathNameHtml = NULL;
	}
		
	pthread_create (&nmr_tid, NULL ,(void *)web_thread, (void *)NULL);
}

// 
// 
void web_remove_temporary_rep (void)
{
	web_remove ();
	// DELETTE TEMPORAY REP
	if (NULL != VarWeb.TemporaryRep)  {
		VarWeb.TemporaryRep  = libutils_remove_temporary_rep (VarWeb.TemporaryRep);
	}
}
// 
// 
void on_button_WebInformations_clicked (GtkButton *button, gpointer user_data)
{
	if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebCdparanoia_homepage")))) {
		web_goto_url ("http://www.xiph.org/paranoia/");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebCdparanoia_manpage")))) {
		web_goto_manpage ("cdparanoia");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebLame_homepage")))) {
		web_goto_url ("http://lame.sourceforge.net/index.php");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebLame_wikimp3")))) {
		web_goto_url ("http://fr.wikipedia.org/wiki/Mp3");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebLame_wikilame")))) {
		web_goto_url ("http://fr.wikipedia.org/wiki/LAME");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebLame_manpage")))) {
		web_goto_manpage ("lame");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebOggenc_wiki")))) {
		web_goto_url ("http://fr.wikipedia.org/wiki/Ogg_Vorbis");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebOggenc_manpage")))) {
		web_goto_manpage ("oggenc");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebFlac_homepage")))) {
		web_goto_url ("http://flac.sourceforge.net/");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebFlac_framasoft")))) {
		web_goto_url ("http://www.framasoft.net/article1510.html");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebFlac_manpage")))) {
		web_goto_manpage ("flac");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebMac_wiki")))) {
		web_goto_url ("http://en.wikipedia.org/wiki/Monkey's_audio");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WavPack_wiki")))) {
		web_goto_url ("http://fr.wikipedia.org/wiki/WavPack");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WavPack_manpage")))) {
		web_goto_manpage ("wavpack");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebMusepack_homepage")))) {
		web_goto_url ("http://www.musepack.net/");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_WebMusepack_framasoft")))) {
		web_goto_url ("http://www.framasoft.net/article2094.html");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_faac_homepage")))) {
		web_goto_url ("http://www.audiocoding.com/faac.html");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_faac_web_info")))) {
		web_goto_url ("http://freshmeat.net/projects/faac/");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_aacplusenc_homepage")))) {
		web_goto_url ("http://teknoraver.net/software/mp4tools/");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_faac_manpage")))) {
		web_goto_manpage ("faac");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_aacplusenc_web_info")))) {
		web_goto_url ("http://en.wikipedia.org/wiki/Aacplus");
	} else if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_aacplusenc_manpage")))) {
		web_goto_manpage ("aacplusenc");
	}
}
// 
// 
void on_button_see_xcfa_install_clicked (GtkButton *button, gpointer user_data)
{
	web_goto_url ("http://www.xcfa.tuxfamily.org/index.php?static2/xcfa");
	sleep(5);
	web_goto_url ("http://download.tuxfamily.org/xcfaudio/PlusPlus/");
}



