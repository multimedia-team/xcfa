 /*
 *  file      : configuser.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef configuser_h
#define configuser_h 1


typedef struct {
	gint        Bitrate_type_Lame;				// Bitrate Lame
	gint        If_abr;							// SI ABR est defini
	gint        If_cbr;							// SI CBR est defini
	gint        If_vbr;							// SI VBR est defini
	gint        If_vbr_new;						// SI VBR_NEW est defini
	gint        Mode_Lame;						// Mode Lame
	gint        Bitrate_Oggenc;					// Bitrate Oggenc
	gint        Managed_Oggenc;					// Managed Oggenc
	gint        Downmix_Oggenc;					// Downmix Oggenc
} RATE_MP3OGG;

typedef struct {
	gboolean	BoolConfigOk;					// Attendre la fin de la config.: Voir main.c
	gulong		UsesOfXcfa;						// 0 to G_MAXULONG ( 18446744073709551615 )
	gint		WinPos_X;						// 
	gint		WinPos_Y;						// 
	gint		WinWidth;						// 
	gint		WinHeight;						// 
	gint		Nice;							// 
	gchar		*PathLoadFileAll;				// 
	gchar		*PathLoadFileWav;				// 
	gchar		*PathLoadFileMp3Ogg;			// 
	gchar		*PathLoadFileTags;				// 
	gint		TabIndiceComboDestFile[4];		// 
	gchar		*PathDestinationFileAll;		// 
	gchar		*PathDestinationFileWav;		// 
	gchar		*PathDestinationFileMp3Ogg;		// 
	gchar		*PathDestinationFileTags;		// 
	gchar		*PathDestinationDVD;			// 
	gchar		*PathChoiceFileDVD;				// 
	gchar		*PathDestinationCD;				// 
	gchar		*PathChoiceFileCD;				// 
	gchar		*PathDestinationSplit;			// 
	gchar		*PathLoadSplit;					// 
	gchar		*PathnameTMP;					// 
	gchar		*PathStockeImagesPochette;		// 
	gchar		*PathDestFilePostScript;		// 
	gchar		*PathLoadImages;				// 
	gint		NotebookGeneral;				// 
	gint		NotebookExpanderCd;				// 
	gint		NotebookFile;					// 
	gint		NotebookOptions;				// 
	gint		NotebookAppExterns;				// 
	gint		ExtractCdWith;					// EXTRACT_WITH_CDPARANOIA = 0, EXTRACT_WITH_CDPARANOIA_EXPERT, EXTRACT_WITH_CDDA2WAV	
	
	gint		BitrateLameIndice;				// abr .. vbr_new
	gint		TabBitrateLame[4];				// [ abr | cbr | vbr | vbr_new ]
	gint		TabModeLame[4];					// [ ModeAbr | ModeCbr | ModeVbr | ModeVbrNew ]
	
	gint		BitrateOggenc;					// 
	gint		ManagedOggenc;					// 
	gint		DownmixOggenc;					// 
	
	gint		CompressionLevelFlac;			// 
	
	gint		CompressionLevelApeMac;			// 
	
	gint		CompressionWavpack;				// 
	gint		SoundWavpack;					// 
	gint		ModeHybrideWavpack;				// 
	gint		CorrectionFileWavpack;			// 
	gint		CompressionMaximumWavpack;		// 
	gint		SignatureMd5Wavpack;			// 
	gint		ExtraEncodingWavpack;			// 
	
	gint		QualityMppenc;					// 
	
	gint		ConteneurFacc;					// 
	gint		AbrVbrFacc;						// 
	gint		AbrFaccIndice;					// 
	gint		VbrFaccIndice;					// 
	
	gint		ChoiceMonoAacplusenc;			// 
	gint		ChoiceStereoAacplusenc;			// 
	
	gboolean	BoolArtistTag;					// 
	gboolean	BoolTitleTag;					// 
	gboolean	BoolAlbumTag;					// 
	gboolean	BoolNumerateTag;				// 
	gboolean	BoolGenreTag;					// 
	gboolean	BoolYearTag;					// 
	gboolean	BoolCommentTag;					// 
	gboolean	BoolEtatExpanderCd;				// 
	
	gchar		*StringExpanderLame;			// Lignes de saisie: options pour les geeks
	gchar		*StringExpanderOggenc;			// 
	gchar		*StringExpanderFlac;			// 
	gchar		*StringExpanderFaac;			// 
	gchar		*StringExpanderMppenc;			// 
	gchar		*StringExpanderMac;				// 
	gchar		*StringExpanderWavpack;			// 
	
	gint		 ServeurCddb;					// SERVER_CDDB_DEFAULT = 0 OR SERVER_CDDB_PARAM
	gchar		*StringNameFile_m3u_xspf;		// 
	gchar		*Templates_title_cdaudio;		// Format titres cdaudio
	gchar		*Templates_rep_cdaudio;			// Dossier de sauvegarde des fichiers cdaudio
	
	gchar		*StringNameNavigateur;			// 
	gchar		*StringParamNameNavigateur;		// 
	gchar		*StringNameLecteurPostScript;	// 
	gchar		*StringNameLecteurAudio;		// 
	gchar		*StringParamNameLecteurAudio;	// 
	
	gchar		*StringBoolFieldsIsVisible;		// FIELDS DVD,CD,FILES: FALSE or HIDE = 0, TRUE or SHOW = 1
	gchar		*StringPosFieldsName;			// POS NAME OF DVD,CD,FILES: 0 = LEFT, 1 = CENTER, 2 = RIGHT
	
	gint		 NumSelectComboBoxDvd;			// 
	gint		 NumSelectComboBoxCd;			// 
	
	gchar		*entry_cddp_server;				// 
	gchar		*entry_proxy_server;			// 
	gchar		*entry_proxy_port;				// 
	
	gchar		*NameImg;						// NEW
	gchar		*PathLoadImg;					// 
	gchar		*PathSaveImg;					// 
	gchar		*PathPochette;					// 
	
	gboolean	BoolCheckbuttonEndOfConvert;	// 
	gchar		*PathMusicFileEndOfConvert;		// 
	gchar		*FileMusicFileEndOfConvert;		// 
	
	gchar		*StringCommentCD;				// 
	
	gboolean	BoolLogCdparanoiaModeExpert;	// 
	
} VAR_CONFIG;

extern VAR_CONFIG Config;
extern VAR_CONFIG ConfigSaveToRest;

void	config_read(void);
void	config_save (void);
void	config_remove (void);

#endif

