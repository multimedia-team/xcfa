 /*
 *  file      : file_tags.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



// FILE_IS_FLAC
// FILE_IS_MP3
// FILE_IS_OGG
// FILE_IS_MPC

#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "dragNdrop.h"
#include "tags.h"
#include "file.h"
#include <taglib/tag_c.h>
#include "statusbar.h"
#include "popup.h"



VAR_FILE_TAGS var_file_tags;

enum
{
	COLUMN_FILETAGS_TYPE = 0,
	COLUMN_FILETAGS_TIME,
	COLUMN_FILETAGS_NAME,
	
	COLUMN_FILETAGS_POINTER_STRUCT,

	COLUMN_FILETAGS_NUM
};
enum
{
	NUM_TREE_FILETAGS_TYPE = 0,
	NUM_TREE_FILETAGS_TIME,
	NUM_TREE_FILETAGS_NAME,
	NUM_TREE_FILETAGS_ALL_COLUMN
};



#define	TAG_GENRE	0
#define	TAG_TRACK	1
#define	TAG_YEAR	2
#define	TAG_COMMENT	3
#define	TAG_ARTIST	4
#define	TAG_ALBUM	5
#define	TAG_TITLE	6




gboolean FileTags_bool_changed_OK = FALSE;



// 
// 
gboolean FileTags_get_line_is_selected (void)
{
	GtkTreeModel     *model = NULL;
	GList            *list = NULL;

	if (var_file_tags.Adr_TreeView == NULL) return (FALSE);
	if (var_file_tags.Adr_Line_Selected == NULL) return (FALSE);
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_tags.Adr_TreeView));
	list = gtk_tree_selection_get_selected_rows (var_file_tags.Adr_Line_Selected, &model);
	return (list ? TRUE : FALSE);
}
// 
// 
void FileTags_set_flag_buttons (void)
{
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_file")), FileTags_get_line_is_selected ());
}
// 
// 
DETAIL *FileTags_get_line_selected (void)
{
	GtkTreeIter       iter;
	GtkTreeModel     *model = NULL;
	GList            *begin_list = NULL;
	GList            *list = NULL;
	GtkTreePath      *path;
	DETAIL           *detail = NULL;

	if (NULL == var_file_tags.Adr_TreeView) return ((DETAIL *)detail);
	if (NULL == var_file_tags.Adr_Tree_Model) return ((DETAIL *)detail);
	if (NULL == var_file_tags.Adr_Line_Selected) return ((DETAIL *)detail);
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_tags.Adr_TreeView));
	if (NULL != (begin_list = gtk_tree_selection_get_selected_rows (var_file_tags.Adr_Line_Selected, &model))) {
		list = g_list_first (begin_list);
		while (list) {
			if (NULL != (path = list->data)) {
				gtk_tree_model_get_iter (model, &iter, path);
				gtk_tree_model_get (var_file_tags.Adr_Tree_Model, &iter, COLUMN_FILETAGS_POINTER_STRUCT, &detail, -1);
				if (NULL != (DETAIL *)detail) return ((DETAIL *)detail);
			}
			list = g_list_next (list);
		}
	}

	return ((DETAIL *)detail);
}
// 
// 
TAGS *FileTags_get_tag (DETAIL *detail)
{
	INFO_WAV	*infoWAV = NULL;
	INFO_SHN	*infoSHN = NULL;
	INFO_WMA	*infoWMA = NULL;
	INFO_RM		*infoRM  = NULL;
	INFO_DTS	*infoDTS = NULL;
	INFO_AIFF	*infoAIFF = NULL;
	INFO_APE	*infoAPE = NULL;
	INFO_FLAC	*infoFLAC = NULL;
	INFO_MP3	*infoMP3 = NULL;
	INFO_OGG	*infoOGG = NULL;
	INFO_M4A	*infoM4A = NULL;
	INFO_AAC	*infoAAC = NULL;
	INFO_MPC	*infoMPC = NULL;
	INFO_WAVPACK	*infoWAVPACK = NULL;
	INFO_AC3	*infoAC3 = NULL;
	TAGS		*tags = NULL;
	
	if (NULL == detail) {
		detail = FileTags_get_line_selected ();
		if (NULL == detail) return ((TAGS  *)tags);
	}
	switch (detail->type_infosong_file_is) {
	case FILE_IS_NONE :
	case FILE_IS_WAVPACK_MD5 :
	case FILE_TO_NORMALISE :
	case FILE_TO_NORMALISE_COLLECTIF :
	case FILE_TO_REPLAYGAIN :
		break;
		
	case FILE_IS_WAV :
		infoWAV = (INFO_WAV *)detail->info;
		tags = (TAGS  *)infoWAV->tags;
		break;
	case FILE_IS_SHN :
		infoSHN = (INFO_SHN *)detail->info;
		tags = (TAGS  *)infoSHN->tags;
		break;
	case FILE_IS_WMA :
		infoWMA = (INFO_WMA *)detail->info;
		tags = (TAGS  *)infoWMA->tags;
		break;
	case FILE_IS_RM :
		infoRM = (INFO_RM *)detail->info;
		tags = (TAGS  *)infoRM->tags;
		break;
	
	case FILE_IS_DTS :
		infoDTS = (INFO_DTS *)detail->info;
		tags = (TAGS  *)infoDTS->tags;
		break;

	case FILE_IS_AIFF :
		infoAIFF = (INFO_AIFF *)detail->info;
		// tags = (TAGS  *)infoDTS->tags;
		tags = (TAGS  *)infoAIFF->tags;
		break;
	
	case FILE_IS_APE :
		infoAPE = (INFO_APE *)detail->info;
		tags = (TAGS  *)infoAPE->tags;
		break;
	
	case FILE_IS_M4A :
		infoM4A = (INFO_M4A *)detail->info;
		tags = (TAGS  *)infoM4A->tags;
		break;
	case FILE_IS_VID_M4A :
		infoM4A = (INFO_M4A *)detail->info;
		tags = (TAGS  *)infoM4A->tags;
		break;
		
	case FILE_IS_AAC :
		infoAAC = (INFO_AAC *)detail->info;
		tags = (TAGS  *)infoAAC->tags;
		break;
		
	case FILE_IS_WAVPACK :
		infoWAVPACK = (INFO_WAVPACK *)detail->info;
		tags = (TAGS  *)infoWAVPACK->tags;
		break;
	
	case FILE_IS_AC3 :
		infoAC3 = (INFO_AC3 *)detail->info;
		tags = (TAGS  *)infoAC3->tags;
		break;
		
	// FICHIER GERES PAR LA LIBRARIE TagFile
	
	case FILE_IS_FLAC :
		infoFLAC = (INFO_FLAC *)detail->info;
		tags = (TAGS  *)infoFLAC->tags;
		break;
	case FILE_IS_MP3 :
		infoMP3 = (INFO_MP3 *)detail->info;
		tags = (TAGS  *)infoMP3->tags;
		break;
	case FILE_IS_OGG :
		infoOGG = (INFO_OGG *)detail->info;
		tags = (TAGS  *)infoOGG->tags;
		break;
	case FILE_IS_MPC :
		infoMPC = (INFO_MPC *)detail->info;
		tags = (TAGS  *)infoMPC->tags;
		break;
	}
	return ((TAGS  *)tags);
}
// 
// 
void FileTags_set_entry_tag_album (void)
{
	TAGS	*tags = FileTags_get_tag (NULL);
	// gchar	*ptr = NULL;
	gchar	*PtrStr = NULL;
	
	if (NULL == var_file_tags.Adr_entry_tag_album) return;
	FileTags_bool_changed_OK = FALSE;
	// ptr = g_strdup (tags ? tags->Album : "ééé");
	// PtrStr = utils_convert_string (tags->Album, TRUE);
	// PtrStr = g_strdup (tags->Album);
	PtrStr = utf8_convert_to_utf8 (tags->Album);
	gtk_entry_set_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_album), tags ? tags->Album ?  PtrStr : "" : "");
	g_free (PtrStr);
	PtrStr = NULL;
	FileTags_bool_changed_OK = TRUE;
}
// 
// 
void FileTags_set_entry_tag_title (void)
{
	TAGS	*tags = FileTags_get_tag (NULL);
	gchar	*PtrStr = NULL;

	if (NULL == var_file_tags.Adr_entry_tag_title) return;
	FileTags_bool_changed_OK = FALSE;
	// PtrStr = utils_convert_string (tags->Title, TRUE);
	// PtrStr = g_strdup (tags->Title);
	PtrStr = utf8_convert_to_utf8 (tags->Title);
	gtk_entry_set_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_title), tags ? tags->Title ?  PtrStr : "" : "");
	g_free (PtrStr);
	PtrStr = NULL;
	FileTags_bool_changed_OK = TRUE;
}
// 
// 
void FileTags_set_entry_tag_artist (void)
{
	TAGS	*tags = FileTags_get_tag (NULL);
	gchar	*PtrStr = NULL;

	if (NULL == var_file_tags.Adr_entry_tag_artist) return;
	FileTags_bool_changed_OK = FALSE;
	// PtrStr = utils_convert_string (tags->Artist, TRUE);
	// PtrStr = g_strdup (tags->Artist);
	PtrStr = utf8_convert_to_utf8 (tags->Artist);
	gtk_entry_set_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_artist), tags ? tags->Artist ? PtrStr : "" : "");
	g_free (PtrStr);
	PtrStr = NULL;
	FileTags_bool_changed_OK = TRUE;
}
// 
// 
void FileTags_set_entry_tag_comment (void)
{
	TAGS	*tags = FileTags_get_tag (NULL);
	gchar	*PtrStr = NULL;

	if (NULL == var_file_tags.Adr_entry_tag_comment) return;
	FileTags_bool_changed_OK = FALSE;
	// PtrStr = utils_convert_string (tags->Comment, TRUE);
	// PtrStr = g_strdup (tags->Comment);
	PtrStr = utf8_convert_to_utf8 (tags->Comment);
	gtk_entry_set_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_comment), tags ? tags->Comment ? PtrStr : "" : "");
	g_free (PtrStr);
	PtrStr = NULL;
	FileTags_bool_changed_OK = TRUE;
}
// 
// 
void FileTags_set_spinbutton_tag_year (void)
{
	TAGS  *tags = FileTags_get_tag (NULL);
	
	if (NULL == var_file_tags.Adr_spinbutton_tag_year) return;
	if (NULL == tags) return;

	FileTags_bool_changed_OK = FALSE;
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (var_file_tags.Adr_spinbutton_tag_year), (gdouble)tags->IntYear);
	FileTags_bool_changed_OK = TRUE;
}
// 
// 
void FileTags_set_spinbutton_tag_track (void)
{
	TAGS  *tags = FileTags_get_tag (NULL);
	
	if (NULL == var_file_tags.Adr_spinbutton_tag_track) return;
	if (NULL == tags) return;
	FileTags_bool_changed_OK = FALSE;
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (var_file_tags.Adr_spinbutton_tag_track), (gdouble)tags->IntNumber);
	FileTags_bool_changed_OK = TRUE;
}
void FileTags_set_combobox_tag_genre( void )
{
	TAGS  *tags = FileTags_get_tag (NULL);
	// tags->Genre = g_strdup( FilePopupGenre.name );
	// tags->IntGenre = tags_get_genre_by_value( FilePopupGenre.name );
	FileTags_bool_changed_OK = FALSE;
	gtk_entry_set_text( GTK_ENTRY(GLADE_GET_OBJECT("entry_tag_genre_file")), tags->Genre ? tags->Genre : "" );
	printf(">tags->Genre     = %s\n", tags->Genre );
	printf(">tags->IntGenre  = %d\n", tags->IntGenre );
	FileTags_bool_changed_OK = TRUE;
}

// CETTE FONCTION INDIQUE QU UNE MODIFICATION DE TAG A EU LIEU
// FLAG MODIFIE:         : LA COLONNE 'TYPE' EST EN ROUGE GRAS
// APRES UNE SAUVEGARDE  : COLONNE 'TYPE'  EST EN COULEUR NORMALE
// 
void FileTags_affiche_type (void)
{
	GtkTreeIter    iter;
	gboolean       valid;
	DETAIL        *detail = NULL;
	TAGS          *tags = NULL;

	// PRINT_FUNC_LF();

	if (NULL == var_file_tags.Adr_Tree_Model) return;

	valid = gtk_tree_model_get_iter_first (var_file_tags.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file_tags.Adr_Tree_Model, &iter, COLUMN_FILETAGS_POINTER_STRUCT, &detail, -1);
		
		if (detail && (detail->type_infosong_file_is == FILE_IS_FLAC ||
		    detail->type_infosong_file_is == FILE_IS_MP3 ||
		    detail->type_infosong_file_is == FILE_IS_OGG ||
		    detail->type_infosong_file_is == FILE_IS_MPC)) {
			
			if ((tags = (TAGS *)FileTags_get_tag (detail))) {

				gchar        *old_text = NULL;
				GtkTreeModel *model = var_file_tags.Adr_Tree_Model;

				gtk_tree_model_get (model, &iter, COLUMN_FILETAGS_TYPE, &old_text, -1);
				g_free (old_text);
				old_text = NULL;

				if (TRUE == tags->bool_save) {

					gtk_list_store_set (var_file_tags.Adr_List_Store,
						&iter,
						COLUMN_FILETAGS_TYPE,
						g_strdup_printf ("<span color=\"red\"><b>%s</b></span>",
								tags_get_str_type_file_is (detail->type_infosong_file_is)),
						-1);
				}
				else {
					gtk_list_store_set (var_file_tags.Adr_List_Store,
						&iter,
						COLUMN_FILETAGS_TYPE,
						g_strdup (tags_get_str_type_file_is (detail->type_infosong_file_is)),
						-1);
				}

			}
		}
		valid = gtk_tree_model_iter_next (var_file_tags.Adr_Tree_Model, &iter);
	}
}
// 
// 
void FileTags_set_label_tag_change (void)
{
	GList         *list = NULL;
	DETAIL        *detail = NULL;
	TAGS          *tags = NULL;
	gchar         *str = NULL;
	gint           changed = 0;
	
	if (NULL == var_file_tags.Adr_label_tag_change) return;
	tags = FileTags_get_tag (NULL);
	gtk_widget_set_sensitive (GTK_WIDGET (var_file_tags.Adr_table_tag), tags ? TRUE : FALSE);
	if (NULL != tags) {
		list = g_list_first (entetefile);
		while (list) {
			if (NULL != (detail = (DETAIL *)list->data)) {
				
				if (detail->type_infosong_file_is == FILE_IS_FLAC ||
				    detail->type_infosong_file_is == FILE_IS_MP3 ||
				    detail->type_infosong_file_is == FILE_IS_OGG ||
				    detail->type_infosong_file_is == FILE_IS_MPC) {
					
					if (NULL != (tags = (TAGS *)FileTags_get_tag (detail)) && TRUE == tags->bool_save) {
						changed ++;
					}
				}
			}
			list = g_list_next (list);
		}
	}

	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_tag_appliquer")), changed > 0 ? TRUE : FALSE);
	gtk_label_set_use_markup (GTK_LABEL (var_file_tags.Adr_label_tag_change), TRUE);
	str = g_strdup_printf ("<b>%d</b>   ", changed);
	gtk_label_set_markup (GTK_LABEL (var_file_tags.Adr_label_tag_change), str);
	g_free (str);
	str = NULL;
	
	FileTags_affiche_type  ();
}
// 
// 
void FileTags_changed_all_tags (gint TYPE_TAG)
{
	GtkTreeIter       iter;
	GtkTreeModel     *model = NULL;
	GList            *begin_list = NULL;
	GList            *list = NULL;
	GtkTreePath      *path;
	DETAIL           *detail = NULL;
	TAGS		 *tags = NULL;

	if (NULL == var_file_tags.Adr_TreeView) return;
	if (NULL == var_file_tags.Adr_Tree_Model) return;
	if (NULL == var_file_tags.Adr_Line_Selected) return;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_tags.Adr_TreeView));
	begin_list = gtk_tree_selection_get_selected_rows (var_file_tags.Adr_Line_Selected, &model);
	list = g_list_first (begin_list);
	while (list) {
		if (NULL != (path = list->data)) {
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_tags.Adr_Tree_Model, &iter, COLUMN_FILETAGS_POINTER_STRUCT, &detail, -1);
			
			if (NULL != (DETAIL *)detail) {

				switch (TYPE_TAG) {
				case TAG_GENRE :
					if (NULL != (tags = FileTags_get_tag (detail))) {
						if( TRUE == FilePopupGenre.BoolFromPopup ) {
							printf("TRUE == FilePopupGenre.BoolFromPopup\n");
							if( tags->Genre )
								g_free (tags->Genre);
							tags->Genre = NULL;
							tags->Genre = g_strdup( FilePopupGenre.name );
							tags->IntGenre = tags_get_genre_by_value( FilePopupGenre.name );
							gtk_entry_set_text( GTK_ENTRY(GLADE_GET_OBJECT("entry_tag_genre_file")), FilePopupGenre.name );
							if( TRUE == FileTags_bool_changed_OK )
								tags_set_flag_modification (tags, TRUE);
							FileTags_set_label_tag_change ();
						}
						else {
							printf("FALSE == FilePopupGenre.BoolFromPopup\n");
							if( tags->Genre )
								g_free (tags->Genre);
							tags->Genre = NULL;
							tags->Genre = g_strdup( (gchar *)gtk_entry_get_text( GTK_ENTRY(GLADE_GET_OBJECT("entry_tag_genre_file")) ));
							tags->IntGenre = tags_get_genre_by_value( tags->Genre );
							if( TRUE == FileTags_bool_changed_OK )
								tags_set_flag_modification (tags, TRUE);
							FileTags_set_label_tag_change ();
						}
					}
					break;
				case TAG_TRACK :
					if (NULL != (tags = FileTags_get_tag (detail))) {
						g_free (tags->Number);
						tags->Number = NULL;
						tags->IntNumber = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(var_file_tags.Adr_spinbutton_tag_track));
						tags->Number    = g_strdup_printf ("%d", tags->IntNumber);
						tags_set_flag_modification (tags, TRUE);
						FileTags_set_label_tag_change ();
					}
					break;
				case TAG_YEAR :
					if (NULL != (tags = FileTags_get_tag (detail))) {
						g_free (tags->Year);
						tags->Year = NULL;
						tags->IntYear = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(var_file_tags.Adr_spinbutton_tag_year));
						tags->Year    = g_strdup_printf ("%d", tags->IntYear);
						tags_set_flag_modification (tags, TRUE);
						FileTags_set_label_tag_change ();
					}
					break;
				case TAG_COMMENT :
					if (NULL != (tags = FileTags_get_tag (detail))) {
						g_free (tags->Comment);
						tags->Comment = NULL;
						tags->Comment = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_comment)));
						tags_set_flag_modification (tags, TRUE);
						FileTags_set_label_tag_change ();
					}
					break;
				case TAG_ARTIST :
					if (NULL != (tags = FileTags_get_tag (detail))) {
						g_free (tags->Artist);
						tags->Artist = NULL;
						tags->Artist = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_artist)));
						tags_set_flag_modification (tags, TRUE);
						FileTags_set_label_tag_change ();
					}
					break;
				case TAG_ALBUM :
					if (NULL != (tags = FileTags_get_tag (detail))) {
						g_free (tags->Album);
						tags->Album = NULL;
						tags->Album = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_album)));
						tags_set_flag_modification (tags, TRUE);
						FileTags_set_label_tag_change ();
					}
					break;
				case TAG_TITLE :
					if (NULL != (tags = FileTags_get_tag (detail))) {
						gchar	*str = NULL;
						gchar	*ptr = NULL;
												
						g_free (tags->Title);
						tags->Title = NULL;
						tags->Title = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_title)));
						
						// Suppression du caracteres '/' interdit si il existe
						str = g_strdup (tags->Title);
						if (strchr (str, '/')) {
							while ((ptr = strchr (str, '/'))) {
								strcpy (ptr, ptr+1);
							}
							gtk_entry_set_text (GTK_ENTRY (var_file_tags.Adr_entry_tag_title), str);
						}
						g_free (tags->Title);
						tags->Title = NULL;
						tags->Title = g_strdup (str);
						g_free (str);
						str = NULL;
						
						tags_set_flag_modification (tags, TRUE);
						FileTags_set_label_tag_change ();
					}
					break;
				}
			}
		}
		list = g_list_next (list);
	}
}
// 
// 
void on_entry_tag_title_file_realize (GtkWidget *widget, gpointer user_data)
{
	var_file_tags.Adr_entry_tag_title = widget;
}
// 
// 
void on_entry_tag_title_file_changed (GtkEditable *editable, gpointer user_data)
{
	if (NULL == var_file_tags.Adr_entry_tag_title) return;
	if (FALSE == FileTags_bool_changed_OK) return;
	FileTags_changed_all_tags (TAG_TITLE);
}
// 
// 
void on_entry_tag_album_file_realize (GtkWidget *widget, gpointer user_data)
{
	var_file_tags.Adr_entry_tag_album = widget;
}
// 
// 
void on_entry_tag_album_file_changed (GtkEditable *editable, gpointer user_data)
{
	if (NULL == var_file_tags.Adr_entry_tag_album) return;
	if (FALSE == FileTags_bool_changed_OK) return;
	FileTags_changed_all_tags (TAG_ALBUM);
}
// 
// 
void on_entry_tag_artist_file_realize (GtkWidget *widget, gpointer user_data)
{
	var_file_tags.Adr_entry_tag_artist = widget;
}
// 
// 
void on_entry_tag_artist_file_changed (GtkEditable *editable, gpointer user_data)
{
	if (NULL == var_file_tags.Adr_entry_tag_artist) return;
	if (FALSE == FileTags_bool_changed_OK) return;
	FileTags_changed_all_tags (TAG_ARTIST);
}
// 
// 
void on_entry_tag_comment_file_realize (GtkWidget *widget, gpointer user_data)
{
	var_file_tags.Adr_entry_tag_comment = widget;
}
// 
// 
void on_entry_tag_comment_file_changed (GtkEditable *editable, gpointer user_data)
{
	if (NULL == var_file_tags.Adr_entry_tag_comment) return;
	if (FALSE == FileTags_bool_changed_OK) return;
	FileTags_changed_all_tags (TAG_COMMENT);
}
// 
// 
void on_spinbutton_tag_year_file_realize (GtkWidget *widget, gpointer user_data)
{
	var_file_tags.Adr_spinbutton_tag_year = widget;
}
// 
// 
void on_spinbutton_tag_year_file_changed (GtkWidget *widget, gpointer user_data)
{
	if (NULL == var_file_tags.Adr_spinbutton_tag_year) return;
	if (FALSE == FileTags_bool_changed_OK) return;
	FileTags_changed_all_tags (TAG_YEAR);
}

#define _CONTACT_NAME_	0
void on_entry_tag_genre_file_changed( GtkEditable *editable, gpointer user_data )
{
	gchar	*PtrEntry = NULL;
	gint	NumEntry = -1;
	
	if( FALSE == FilePopupGenre.BoolFromPopup ) {
		PRINT_FUNC_LF();
		PtrEntry = (gchar *)gtk_entry_get_text( GTK_ENTRY(GLADE_GET_OBJECT("entry_tag_genre_file")));
		NumEntry = tags_get_genre_by_value( PtrEntry );
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print( "\tPtrEntry = %s\n", PtrEntry );
			g_print( "\tNumEntry = %d\n", NumEntry);
		}
		FileTags_changed_all_tags (TAG_GENRE);
		// cd_expander_set_genre( NumEntry, PtrEntry );
	}
}
/**
gboolean on_match_select_file(GtkEntryCompletion *widget, GtkTreeModel *model, GtkTreeIter *iter, gpointer user_data)
{  
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		GValue value = {0, };
		gtk_tree_model_get_value(model, iter, _CONTACT_NAME_, &value);
		g_print( "You have selected %s\n", g_value_get_string(&value) );
		g_value_unset(&value);
	}
	return FALSE;
}    
*/
void on_entry_tag_genre_file_realize( GtkWidget *widget, gpointer user_data )
{
	STRUCT_TAGS_FILE_MP3	*Contact;
	GtkEntryCompletion		*completion;
	GtkListStore			*model;
	GtkTreeIter				iter;
	
	// set up completion
	completion = gtk_entry_completion_new();
	gtk_entry_completion_set_text_column( completion, _CONTACT_NAME_ );
	gtk_entry_set_completion( GTK_ENTRY(widget), completion );
	// g_signal_connect( G_OBJECT(completion), "match-selected", G_CALLBACK(on_match_select_file), NULL );
	g_signal_connect( G_OBJECT(widget), "changed", G_CALLBACK(on_entry_tag_genre_file_changed), widget );
	model = gtk_list_store_new( 1, G_TYPE_STRING );
	for( Contact = StructTagsFileMp3; Contact && Contact->name; Contact++ ) {
		gtk_list_store_append( model, &iter );
		gtk_list_store_set( model, &iter, _CONTACT_NAME_, Contact->name, -1 );
	}
	gtk_entry_completion_set_model( completion, GTK_TREE_MODEL(model) );
}


void on_button_tag_call_popup_file_clicked( GtkButton *button, gpointer user_data )
{
	popup_menu_file();
}

// 
// 
void on_spinbutton_tag_track_file_realize (GtkWidget *widget, gpointer user_data)
{
	var_file_tags.Adr_spinbutton_tag_track = widget;
}
// 
// 
void on_spinbutton_tag_track_file_changed (GtkWidget *widget, gpointer user_data)
{
	if (NULL == var_file_tags.Adr_spinbutton_tag_track) return;
	if (FALSE == FileTags_bool_changed_OK) return;
	FileTags_changed_all_tags (TAG_TRACK);
}
// 
// 
void FileTags_appliquer_clicked (void)
{
	TAGS		*tags = FileTags_get_tag (NULL);
	TagLib_File	*file;
	TagLib_Tag	*tag;
	DETAIL		*detail = NULL;
	GList		*list = NULL;
	gchar		*Ptr = NULL;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			
			if (detail->type_infosong_file_is == FILE_IS_FLAC ||
			    detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_MPC) {
				
				if ((tags = (TAGS *)FileTags_get_tag (detail)) && tags->bool_save == TRUE) {
					
					/*
					g_print ("\n");
					g_print ("Album   = %s\n", tags->Album);
					g_print ("Artist  = %s\n", tags->Artist);
					g_print ("Title   = %s\n", tags->Title);
					g_print ("Comment = %s\n", tags->Comment);
					g_print ("Genre   = %s [%d]\n", tags->Genre, tags->IntGenre);
					g_print ("Year    = %s [%d]\n", tags->Year, tags->IntYear);
					g_print ("Number  = %s [%d]\n", tags->Number, tags->IntNumber);
					g_print ("\n");
					*/

					if ((file = taglib_file_new (detail->namefile))) {
						
						tag = taglib_file_tag(file);
						
						/*
						// Ptr = utils_convert_from_utf8 (tags->Title);
						Ptr = tags->Title;
						taglib_tag_set_title (tag, Ptr);
						// g_free (Ptr);	Ptr = NULL;

						// Ptr = utils_convert_from_utf8 (tags->Artist);
						Ptr = tags->Artist;// 
						taglib_tag_set_artist (tag, Ptr);
						// g_free (Ptr);	Ptr = NULL;

						// Ptr = utils_convert_from_utf8 (tags->Album);
						Ptr = tags->Album;
						taglib_tag_set_album (tag, Ptr);
						// g_free (Ptr);	Ptr = NULL;

						// Ptr = utils_convert_from_utf8 (tags->Comment);
						Ptr = tags->Comment;
						taglib_tag_set_comment (tag, Ptr);
						// g_free (Ptr);	Ptr = NULL;

						// Ptr = utils_convert_from_utf8 (tags->Genre);
						Ptr = tags->Genre;
						taglib_tag_set_genre (tag, Ptr);
						// g_free (Ptr);	Ptr = NULL;

						taglib_tag_set_year (tag, tags->IntYear);
						taglib_tag_set_track (tag, tags->IntNumber);
						*/
						
						// -----------------
						
						Ptr = utf8_convert_to_utf8 (tags->Title);
						taglib_tag_set_title (tag, Ptr);
						g_free (Ptr);	Ptr = NULL;

						Ptr = utf8_convert_to_utf8 (tags->Artist);
						taglib_tag_set_artist (tag, Ptr);
						g_free (Ptr);	Ptr = NULL;

						Ptr = utf8_convert_to_utf8 (tags->Album);
						taglib_tag_set_album (tag, Ptr);
						g_free (Ptr);	Ptr = NULL;

						Ptr = utf8_convert_to_utf8 (tags->Comment);
						taglib_tag_set_comment (tag, Ptr);
						g_free (Ptr);	Ptr = NULL;

						Ptr = utf8_convert_to_utf8 (tags->Genre);
						taglib_tag_set_genre (tag, Ptr);
						g_free (Ptr);	Ptr = NULL;

						taglib_tag_set_year (tag, tags->IntYear);
						taglib_tag_set_track (tag, tags->IntNumber);
						
						
						taglib_file_save(file);
						
						taglib_tag_free_strings();
						taglib_file_free (file);
					}
					tags->bool_save = FALSE;
				}
			}
		}
		list = g_list_next (list);
	}
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_tag_appliquer")), FALSE);
	gtk_label_set_use_markup (GTK_LABEL (var_file_tags.Adr_label_tag_change), TRUE);
	gtk_label_set_markup (GTK_LABEL (var_file_tags.Adr_label_tag_change), "<b>0</b>");

	FileTags_affiche_type ();
}
// 
// 
void on_label_tag_change_realize (GtkWidget *widget, gpointer user_data)
{
	var_file_tags.Adr_label_tag_change = widget;
}
// 
// 
void on_table_tag_realize (GtkWidget *widget, gpointer user_data)
{
	var_file_tags.Adr_table_tag = widget;
}
// MARQUER LES LIGNES POUR LA DESTRUCTION
// 
gboolean FileTags_del_file_clicked (void)
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*BeginList = NULL;
	GList		*list = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;
	gboolean	 BoolPrint = FALSE;
	
	// RECUP. LIGNES EN SELECTION POUR DESTRUCTION
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_tags.Adr_TreeView));
	if ((BeginList = gtk_tree_selection_get_selected_rows (var_file_tags.Adr_Line_Selected, &model))) {
		BoolPrint = TRUE;
		list = g_list_first (BeginList);
		while (list) {
			if (NULL != (path = list->data)) {
				gtk_tree_model_get_iter (model, &iter, path);				
				gtk_tree_model_get (var_file_tags.Adr_Tree_Model, &iter, COLUMN_FILETAGS_POINTER_STRUCT, &detail, -1);
				// MARQUER LA LIGNE DU GLIST A DETRUIRE AVANT LE REAFFICHAGE
				if (NULL != detail) detail->BoolRemove = TRUE;
			}
			list = g_list_next (list);
		}
		// gtk_tree_selection_unselect_all (var_file_tags.Adr_Line_Selected);
	}
	return (BoolPrint);
}
// FILE_IS_FLAC
// FILE_IS_MP3
// FILE_IS_OGG
// FILE_IS_MPC
// 
// 
void FileTags_affiche_glist( void )
{
	DETAIL		*detail = NULL;
	GList		*List = NULL;
	GtkTreeIter	iter;
	GtkAdjustment	*Adj = NULL;
	gdouble		AdjValue;
	gint		Line = 0;		// LIGNE EN COURS
	gchar		*NameDest = NULL;
	gint		NumLineSelected = -1;
	gboolean	BoolNumLineSelected = FALSE;
	
	// PRINT_FUNC_LF();
	
	// RECUP SELECTION
	// 
	NumLineSelected = libutils_get_first_line_is_selected( var_file_tags.Adr_Line_Selected, var_file_tags.Adr_Tree_Model );

	gtk_tree_selection_unselect_all (var_file_tags.Adr_Line_Selected);

	// DELETE TREEVIEW
	// 
	gtk_list_store_clear (GTK_LIST_STORE (var_file_tags.Adr_List_Store));
	
	// COORDONNEES POUR UN REAJUSTEMENT VISUEL DE LA PAGE
	// 
	Adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (var_file_tags.Adr_scroll));
	AdjValue = gtk_adjustment_get_value (Adj);
	
	// AFFICHAGE DE LA LISTE
	// 
	Line = 0;
	List = g_list_first (entetefile);
	while (List) {
		if (NULL != ((detail = (DETAIL *)List->data)) && FALSE == detail->BoolRemove) {

			if (FILE_IS_FLAC != detail->type_infosong_file_is &&
			    FILE_IS_MP3 != detail->type_infosong_file_is &&
			    FILE_IS_OGG != detail->type_infosong_file_is &&
			    FILE_IS_MPC != detail->type_infosong_file_is) {
				List = g_list_next (List);
				continue;
			}
			
			NameDest = libutils_get_name_without_ext_with_amp (detail->namefile);
			
			gtk_list_store_append (var_file_tags.Adr_List_Store, &iter);
			gtk_list_store_set (var_file_tags.Adr_List_Store, &iter,
						COLUMN_FILETAGS_TYPE,		tags_get_str_type_file_is (detail->type_infosong_file_is),
						COLUMN_FILETAGS_TIME,		file_get_time (detail),
						COLUMN_FILETAGS_NAME,		NameDest,
						// COLUMN_FILETAGS_COLOR,		&YellowColor,
						COLUMN_FILETAGS_POINTER_STRUCT,	detail,
						-1);
						
			g_free (NameDest);	NameDest = NULL;
			
			// AFFICHE LES EVENTUELLES LIGNES EN SELECTION
			// 
			if( NumLineSelected == Line ) {
				gtk_tree_selection_select_iter (var_file_tags.Adr_Line_Selected, &iter);
				BoolNumLineSelected = TRUE;
			}
			
			Line ++;
		}
		List = g_list_next (List);
	}
	
	// SUPPRESSON TABLEAU DES EVENTUELLES LIGNES EN SELECTION
	// 
	
	if( NumLineSelected == -1 ) {
		if (gtk_tree_model_get_iter_first (var_file_tags.Adr_Tree_Model, &iter)) {
			gtk_tree_selection_select_iter (var_file_tags.Adr_Line_Selected, &iter);
			BoolNumLineSelected = TRUE;
		}
	}

	if( NumLineSelected > 0 && NULL != entetefile && BoolNumLineSelected == FALSE ) {
		gtk_tree_selection_select_iter (var_file_tags.Adr_Line_Selected, &iter);
	}

	// REAJUSTEMENT DE LA LISTE
	// 
	gtk_adjustment_set_value (Adj, AdjValue);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (var_file_tags.Adr_scroll), Adj);

	FileTags_set_flag_buttons ();
	FileTags_set_label_tag_change ();
}
// 
// 
void FileTags_changed_selection_row (GtkTreeSelection *selection, gpointer data)
{
	var_file_tags.Adr_Line_Selected = selection;
	
	if (NOTEBOOK_FICHIERS == Config.NotebookGeneral && NOTEBOOK_FICHIERS_TAGS == Config.NotebookFile) {
		
		if (NULL != FileTags_get_line_selected ()) {
			FileTags_set_entry_tag_album ();
			FileTags_set_entry_tag_title ();
			FileTags_set_entry_tag_artist ();
			FileTags_set_entry_tag_comment ();
			FileTags_set_spinbutton_tag_year ();
			FileTags_set_spinbutton_tag_track ();
			FileTags_set_combobox_tag_genre ();
		}
		FileTags_set_label_tag_change ();
	}
	FileTags_set_flag_buttons ();
}
// 
// 
gboolean FileTags_key_press_event (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	if( TRUE == keys.BoolGDK_Control_A ) {	// CONTROL_A
		gtk_tree_selection_unselect_all (var_file_tags.Adr_Line_Selected);
		gtk_tree_selection_select_all (var_file_tags.Adr_Line_Selected);
	}
	if (keys.keyval == GDK_KEY_Delete) {
		GtkTreeIter   iter;
		if (gtk_tree_model_get_iter_first (var_file_tags.Adr_Tree_Model, &iter)) {
			on_file_button_del_file_clicked (NULL, NULL);
			return (FALSE);
		}
	}
	return (TRUE);
}
// AFFICHAGE DU NOM COMPLET DU FICHIER SI SURVOL PAR LE CURSEUR SOURIS DU CHAMPS 'Nom'
// 
gboolean FileTags_event (GtkWidget *treeview, GdkEvent *event, gpointer user_data)
{
	gint                x, y;
	GdkModifierType     state;
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)user_data;
	DETAIL             *detail = NULL;
	gint                Pos_X = 0, Pos_Y = 0;
	gint                i;
	gboolean            BoolSelectColNom = FALSE;
	
	// TODO : @Tetsumaki  http://forum.ubuntu-fr.org/viewtopic.php?pid=3889380#p3889380
	// return (FALSE);
	// PRINT_FUNC_LF();
		
	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &x, &y, &state );
	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &x, &y, &state);
	if (FALSE == gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					   x, y,
					   &path, &column, &Pos_X, &Pos_Y)) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "" );
		return (FALSE);
	}
	
	// RECUPERATION DE LA STRUCTURE POINTEE PAR LE CURSEUR SOURIS
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_file_tags.Adr_Tree_Model, &iter, COLUMN_FILETAGS_POINTER_STRUCT, &detail, -1);
	if (NULL == detail) return (FALSE);
	
	// DANS TOUS LES CAS, EFFACE LA BARRE DE TACHE
	StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "" );

	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_FILETAGS_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			switch ( i ) {
			case NUM_TREE_FILETAGS_NAME :		BoolSelectColNom		= TRUE;	break;
			default: 
				StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
				StatusBar_puts();
				return (FALSE);
			}
			// LA COLONNE ES TROUVEE ... SRTIE DE LA BOUCLE
			break;
		}
	}
	if (BoolSelectColNom) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, detail->namefile );
	}
	
	return (FALSE);
}
// 
// 
static void FileTags_drag_data_received(
					GtkWidget        *widget,
					GdkDragContext   *context,
					gint              x,
					gint              y,
					GtkSelectionData *data,
					guint             info,
					guint             time,
					gpointer          user_data)
{
	/* PRINT_FUNC_LF(); */
        /* Une copie ne peut aller vers elle meme !!! */
	if (gtk_drag_get_source_widget(context) != widget) {
		//dragndrop_list_drag_data (widget, (gchar *)data->data);
		dragndrop_list_drag_data( widget, (gchar*)gtk_selection_data_get_data( data ));
	}
}
// 
// 
static void FileTags_drag_data_drop (GtkWidget *widget,
					GdkDragContext *dc,
					GtkSelectionData *selection_data,
					guint info,
					guint t,
					gpointer data)
{
	GtkTreeIter       iter;
	GtkTreeModel     *model = NULL;
	GList            *begin_list = NULL;
	GList            *list = NULL;
	GtkTreePath      *path;
	DETAIL           *detail = NULL;
	gchar            *text = NULL;

	/* PRINT_FUNC_LF(); */
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(widget));
	begin_list = gtk_tree_selection_get_selected_rows (var_file_tags.Adr_Line_Selected, &model);
	list = g_list_first (begin_list);
	while (list) {
		if ((path = list->data)) {
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_tags.Adr_Tree_Model, &iter, COLUMN_FILETAGS_POINTER_STRUCT, &detail, -1);
			
			// DEBUG DRAG AND DROP
			// [ Tue, 03 May 2011 17:39:08 +0200 ]
			// XCFA-4.1.0
			// -----------------------------------------------------------
			// OLD CODE:
			// 	text = g_strdup( detail->namefile );
			// NEW_CODE:
			text = g_strdup_printf( "file://%s", detail->namefile );
			
			gdk_drag_status (dc, GDK_ACTION_COPY, t); 

			gtk_selection_data_set( selection_data,
						// GDK_SELECTION_TYPE_STRING,
						// selection_data->target,
						GDK_POINTER_TO_ATOM(gtk_selection_data_get_data( selection_data )),
						8,
						(guchar *)text,
						strlen( text )
						);
			g_free (text);
			text = NULL;
		}
		list = g_list_next (list);
	}
}
// 
// 
void FileTags_add_columns_scrolledwindow (GtkTreeView *treeview)
{
	GtkTreeModel      *model = gtk_tree_view_get_model (treeview);
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;
	
	// TODO : @Tetsumaki  http://forum.ubuntu-fr.org/viewtopic.php?pid=3889380#p3889380
	// SIGNAL : 'event'
	g_signal_connect(G_OBJECT(treeview),
			 "event",
                     	 (GCallback) FileTags_event,
			 model);

	// SIGNAL 'key-press-event'
	g_signal_connect(G_OBJECT(treeview),
			 "key-press-event",
                    	 (GCallback) FileTags_key_press_event,
			 model);
	
	// SIGNAL : Ligne actuellement selectionnee 'changed'
	var_file_tags.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	g_signal_connect(G_OBJECT(var_file_tags.Adr_Line_Selected),
			 "changed",
                   	 G_CALLBACK(FileTags_changed_selection_row),
                   	 "1");
	
	// Drag and drop support
	// SIGNAL : 'drag-data-received'
	gtk_drag_dest_set (GTK_WIDGET (treeview),
			   GTK_DEST_DEFAULT_MOTION |
			   GTK_DEST_DEFAULT_DROP,
			   drag_types, n_drag_types,
			   GDK_ACTION_COPY| GDK_ACTION_MOVE );
	g_signal_connect(G_OBJECT(treeview),
			 "drag-data-received",
			 G_CALLBACK(FileTags_drag_data_received),
			 NULL);

	gtk_drag_source_set(
			GTK_WIDGET(treeview),
			GDK_BUTTON1_MASK | GDK_BUTTON2_MASK | GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP,
			drag_types, n_drag_types,
			GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_DEFAULT
			);

	g_signal_connect(G_OBJECT(treeview),
      			"drag-data-get",
			 G_CALLBACK(FileTags_drag_data_drop),
			 treeview);
		
	// COLUMN_FILETAGS_TYPE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	// var_file_tags.Adr_ColumnFileWavNewHertz =
	var_file_tags.Adr_ColumnFileTagsType =
	column = gtk_tree_view_column_new_with_attributes (_("Type"),
						     renderer,
						     "markup", COLUMN_FILETAGS_TYPE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILETAGS_TIME
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	// var_file_tags.Adr_ColumnFileWavNewHertz =
	var_file_tags.Adr_ColumnFileTagsTime =
	column = gtk_tree_view_column_new_with_attributes (_("Time"),
						     renderer,
						     "markup", COLUMN_FILETAGS_TIME,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILETAGS_NAME
	var_file_tags.Renderer =
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	// var_file_tags.Adr_ColumnFileWavNewHertz =
	var_file_tags.Adr_ColumnFileTagsName =
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
						     renderer,
						     "markup", COLUMN_FILETAGS_NAME,
						     // "background-gdk", COLUMN_FILETAGS_COLOR,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
}
// 
// 
void on_scrolledwindow_tags_file_realize (GtkWidget *widget, gpointer user_data)
{
	GtkListStore *store;
	GtkTreeModel *model;
	GtkWidget    *treeview;
	
	var_file_tags.Adr_scroll = widget;
	
	
	var_file_tags.Adr_List_Store = store =
	gtk_list_store_new (	COLUMN_FILETAGS_NUM,	// TOTAL NUMBER
				G_TYPE_STRING,		// COLUMN_FILETAGS_TYPE
				G_TYPE_STRING,		// COLUMN_FILETAGS_TIME
				G_TYPE_STRING,		// COLUMN_FILETAGS_NAME
				// GDK_TYPE_COLOR,		// COLUMN_FILETAGS_COLOR
				G_TYPE_POINTER          // COLUMN_FILETAGS_POINTER_STRUCT
			   );
	var_file_tags.Adr_Tree_Model = model = GTK_TREE_MODEL (store);
	var_file_tags.Adr_TreeView =
	treeview = gtk_tree_view_new_with_model (model);
	// gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview)), GTK_SELECTION_MULTIPLE);	// GTK_SELECTION_BROWSE MULTIPLE
	g_object_unref (model);
	gtk_container_add (GTK_CONTAINER (widget), treeview);
	FileTags_add_columns_scrolledwindow (GTK_TREE_VIEW (treeview));
	gtk_widget_show_all (widget);
}









