 /*
 *  file      : cursor.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cursor.h"

/*
*---------------------------------------------------------------------------
* TYPE DE CURSEUR SOURIS
*---------------------------------------------------------------------------
*/
static GdkCursor *_cursor = NULL;
GdkCursorType     cursor_type = GDK_LEFT_PTR;




// 
// 
void cursor_set_clear (void)
{
	if (cursor_type != GDK_HAND2) {
		cursor_type = GDK_HAND2;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_hand (void)
{
	if (cursor_type != GDK_HAND2) {
		cursor_type = GDK_HAND2;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_saisie (void)
{
	if (cursor_type != GDK_XTERM) {
		cursor_type = GDK_XTERM;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_gauche_droite (void)
{
	if (cursor_type != GDK_SB_H_DOUBLE_ARROW) {
		cursor_type = GDK_SB_H_DOUBLE_ARROW;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_haut (void)
{
	if (cursor_type != GDK_TOP_SIDE) {
		cursor_type = GDK_TOP_SIDE;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_bas (void)
{
	if (cursor_type != GDK_BOTTOM_SIDE) {
		cursor_type = GDK_BOTTOM_SIDE;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_gauche (void)
{
	if (cursor_type != GDK_LEFT_SIDE) {
		cursor_type = GDK_LEFT_SIDE;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_droit (void)
{
	if (cursor_type != GDK_RIGHT_SIDE) {
		cursor_type = GDK_RIGHT_SIDE;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_haut_gauche (void)
{
	if (cursor_type != GDK_TOP_LEFT_CORNER) {
		cursor_type = GDK_TOP_LEFT_CORNER;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_haut_droit (void)
{
	if (cursor_type != GDK_TOP_RIGHT_CORNER) {
		cursor_type = GDK_TOP_RIGHT_CORNER;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_bas_gauche (void)
{
	if (cursor_type != GDK_BOTTOM_LEFT_CORNER) {
		cursor_type = GDK_BOTTOM_LEFT_CORNER;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_bas_droit (void)
{
	if (cursor_type != GDK_BOTTOM_RIGHT_CORNER) {
		cursor_type = GDK_BOTTOM_RIGHT_CORNER;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_move (void)
{
	if (cursor_type != GDK_FLEUR) {
		cursor_type = GDK_FLEUR;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_old (void)
{
	if (cursor_type != GDK_LEFT_PTR) {
		cursor_type = GDK_LEFT_PTR;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
void cursor_set_watch (void)
{
	if (cursor_type != GDK_WATCH) {
		cursor_type = GDK_WATCH;
		// _cursor = gdk_cursor_new (cursor_type);
		_cursor = gdk_cursor_new_for_display( gdk_display_get_default(), cursor_type );
		gdk_window_set_cursor (gtk_widget_get_window(WindMain), _cursor);
		gdk_flush ();
	}
}
// 
// 
gboolean cursor_get_watch (void)
{
	return (cursor_type == GDK_WATCH ? TRUE : FALSE);
}



/*
cursor.c:214:3: attention : ‘gdk_cursor_new’ is deprecated (declared at /usr/include/gtk-3.0/gdk/gdkcursor.h:223) [-Wdeprecated-declarations]


http://cpansearch.perl.org/src/KRYDE/Gtk2-Ex-WidgetCursor-15/devel/invisible-blank.c


#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

int
main (int argc, char **argv)
{
  GdkDisplay *display;
  GdkCursor *cursor;

  gtk_init (&argc, &argv);
  display = gdk_display_get_default();

  cursor = gdk_cursor_new_for_display (display, GDK_BLANK_CURSOR);
  printf ("%p\n", cursor);

  cursor = gdk_cursor_new_for_display (display, GDK_BLANK_CURSOR);
  printf ("%p\n", cursor);

  return 0;
}

*/
