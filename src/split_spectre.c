 /*
 *  file      : split_spectre.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <math.h>
#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "get_info.h"
#include "tags.h"
#include "split.h"




#define	SAMPLE_SHADES	3

extern VAR_SPLIT VarSplit;
extern ADJUST Adjust;


gint	MarkerOld = -1;


// 
// 
gint SplitSpectre_get_with (void)
{
	GtkAllocation	allocation;
	
	gtk_widget_get_allocation( VarSplit.AdrWidgetSpectre, &allocation );
	return( (allocation.width -1) * Adjust.mul );
}
// AFFICHAGE IMAGE CHRONO
// 
void SplitSpectre_draw_chrono ( GtkWidget *widget, cairo_t *p_cr )
{
	GtkAllocation	allocation;
	gint			ChronoX;
	gint			CursorX, CursorY;
	gint			H, M, S;
	gchar			*Str = NULL;
	gint			sec;
	gdouble			TimeSongSec;
	gdouble			Percent;

	if (NULL == widget) return;

	gtk_widget_get_allocation( widget, &allocation );
	
	cairo_scale( p_cr, 1.0, 1.0 );
	cairo_translate( p_cr, 0.0, 0.0 );
	
	// CLEAR  PREVIEW SURFACE
	cairo_set_source_rgb( p_cr, 0.4, 0.4, 0.4 );
	cairo_rectangle( p_cr,
			 0,
			 0,
			 allocation.width,
			 allocation.height
			 );			
	cairo_fill( p_cr );
	
#define  SPLIT_TIME_CHRONO_   80
	for (ChronoX = 2; ChronoX +60 <= VARSPLIT_CHRONO_W +80; ChronoX += SPLIT_TIME_CHRONO_) {
		if (ChronoX > 1) {
			// CRAYON_BLANC
			cairo_set_source_rgb( p_cr, 1.0, 1.0, 1.0 );
			// EPAISSEUR
			cairo_set_line_width( p_cr, 0.5 );
			// HAUT
			cairo_move_to( p_cr, ChronoX -2, 0.0 );
			// BAS
			cairo_rel_line_to( p_cr, 0.0, VARSPLIT_CHRONO_Y + VARSPLIT_CHRONO_H );
			cairo_stroke ( p_cr );
		
			// TEXTE
			CursorX = (gint)gtk_adjustment_get_value( VarSplit.AdjScroll );
			CursorX += ChronoX;
			CursorX -= 2;
			
			if (NULL != VarSplit.Tags) {
				TimeSongSec = VarSplit.sec_in_time;
			}
			else {
				TimeSongSec = 300;
			}
			Percent = ((gdouble)CursorX / (gdouble)SplitSpectre_get_with() ) * 100.0;
			sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
			// dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
			// hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
			H = (sec / 60) / 60;
			M = (sec / 60) % 60;
			S = sec % 60;
			Str = g_strdup_printf ("%02d:%02d:%02d", H, M, S);
			
			cairo_select_font_face( p_cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL );
			cairo_set_font_size( p_cr, 10 );
			cairo_move_to( p_cr, ChronoX +0,  13.);
			cairo_show_text( p_cr, Str );
			
			g_free( Str );
			Str = NULL;
		
		}
	}
	
	// REPAIR VERTICAL PARTIE HAUTE
	if( VarSplit.NbrSelecteurs > 0 ) {
		GdkModifierType		state;
		GdkDeviceManager	*manager = gdk_display_get_device_manager( gdk_display_get_default() );
		GdkDevice			*device  = gdk_device_manager_get_client_pointer( manager );
		gdk_window_get_device_position( gtk_widget_get_window(widget), device, &CursorX, &CursorY, &state );
		// gdk_window_get_pointer( widget->window, &CursorX, &CursorY, &state );
		
		cairo_set_line_width( p_cr, 2 );
		// CRAYON_ROUGE
		cairo_set_source_rgb( p_cr, 1.0, 0.0, 0.0 );
		cairo_move_to( p_cr, CursorX, 0.0 );
		cairo_rel_line_to( p_cr, 0.0, VARSPLIT_CHRONO_Y + VARSPLIT_CHRONO_H );
		cairo_stroke ( p_cr );
	}
}
// AFFICHAGE IMAGE TOP
// 
void SplitSpectre_draw_top ( GtkWidget *widget, cairo_t *p_cr )
{
	GtkAllocation	allocation;
	

	if (NULL == widget) return;
		
	gtk_widget_get_allocation( widget, &allocation );
	
	cairo_scale( p_cr, 1.0, 1.0 );
	cairo_translate( p_cr, 0.0, 0.0 );
	
	// CLEAR  PREVIEW SURFACE
	cairo_set_source_rgb( p_cr, 0.5, 0.5, 0.5 );
	cairo_rectangle( p_cr,
			 0,
			 0,
			 allocation.width,
			 allocation.height
			 );			
	cairo_fill( p_cr );
	if (VarSplit.NbrSelecteurs > 0) {
	
		gint			begin = 0;
		gint			Len;
		gint			Cpt;
		gint			SelBegin;
		gint			SelEnd;
		gint			i;
		GtkAllocation	allocation;
		gchar			*Str = NULL;
		
		gtk_widget_get_allocation( widget, &allocation );

		begin = (gint)gtk_adjustment_get_value( VarSplit.AdjScroll );
		Len = SplitSpectre_get_with () + begin;
	
		// DRAW ALL SELECTEURS
		for (Cpt = 0; Cpt < MAX_SELECTEURS_SPLIT; Cpt ++) {
			
			if (VarSplit.Selecteur [ Cpt ] . Nmr == -1) break;
			
			// FLECHE LIGNE DEBUT
			SelBegin = SplitSelector_get_pos_begin( Cpt );
			if (SelBegin >= begin && SelBegin <= Len) {
				
				cairo_set_source_rgb( p_cr, 0.0, 0.0, 0.0 );
				for (i = 0; i < 9; i++) {
					cairo_move_to( p_cr, SelBegin - begin + i, 0.0 + i );
					cairo_rel_line_to( p_cr, 0.0, allocation.height -(i*2) );
					cairo_stroke ( p_cr );
				}
				Str = g_strdup_printf("%d", Cpt );
				cairo_select_font_face( p_cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL );
				cairo_set_font_size( p_cr, 10 );
				cairo_move_to( p_cr, SelBegin +10,  13.);
				cairo_set_source_rgb( p_cr, 1.0, 1.0, 1.0 );
				cairo_show_text( p_cr, Str );
				g_free( Str );
				Str = NULL;			
			}
			// FLECHE LIGNE FIN
			SelEnd = SplitSelector_get_pos_end( Cpt );
			if (SelEnd > SelBegin && SelEnd > begin && SelEnd < Len) {
				cairo_set_source_rgb( p_cr, 0.0, 0.0, 0.0 );
				for (i = 0; i < 9; i++) {
					cairo_move_to( p_cr, SelEnd - begin - i, 0.0 + i  );
					cairo_rel_line_to( p_cr, 0.0, allocation.height -(i*2) );
					cairo_stroke ( p_cr );
				}
				Str = g_strdup_printf("%d", Cpt );
				cairo_select_font_face( p_cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL );
				cairo_set_font_size( p_cr, 10 );
				cairo_move_to( p_cr, SelEnd -24,  13.);
				cairo_set_source_rgb( p_cr, 1.0, 1.0, 1.0 );
				cairo_show_text( p_cr, Str );
				g_free( Str );
				Str = NULL;			
			}
		}
		// DRAW SELECTEUR ACTIF
		// FLECHE LIGNE DEBUT
		SelBegin = SplitSelector_get_pos_begin (VarSplit.SelecteurActif);
		if (SelBegin >= begin && SelBegin < Len) {
			cairo_set_source_rgb( p_cr, 244. / 255., 162. / 255., 87. / 255. );
			for (i = 0; i < 9; i++) {
				cairo_move_to( p_cr, SelBegin - begin + i, 0.0 + i  );
				cairo_rel_line_to( p_cr, 0.0, allocation.height -(i*2) );
				cairo_stroke ( p_cr );
			}
		}
		// FLECHE LIGNE FIN
		SelEnd = SplitSelector_get_pos_end (VarSplit.SelecteurActif);	
		if (SelEnd >= begin && SelEnd < Len) {
			cairo_set_source_rgb( p_cr, 0.0, 1.0, 0.0 );
			for (i = 0; i < 9; i++) {
				cairo_move_to( p_cr, SelEnd - begin - i, 0.0 + i  );
				cairo_rel_line_to( p_cr, 0.0, allocation.height -(i*2) );
				cairo_stroke ( p_cr );
			}
		}
			
			
	}
}
// 
// 
void SplitSpectre_remove (void)
{
	if (NULL != VarSplit.Tab) {
		g_free( VarSplit.Tab );
		VarSplit.Tab = NULL;
	}
	VarSplit.Tags = tagswav_remove_info (VarSplit.Tags);
}
// 
// PERCENT = ( VALUE_X / LONGUEUR_TOTALE ) * 100.0
// VALUE_X = ( LONGUEUR_TOTALE * PERCENT ) / 100.0
// 
// xavier a écrit : 
// Le marqueur de début doit coller le début de la forme d'onde. Celui de fin à la fin de la forme d'onde
// 
// From @Dzef:
// 	Sinon, Claude, pour le soucis de not'
// 	Christophe-coupeur-d'album-en-rondelles-sans-cue, la parade existe en partie
// 	dans le cas où les plages sont séparées par des "blancs" (ce qui n'est pas
// 	le cas des albums "live" par exemple) : il suffit de détecter le niveau
// 	audio et en dessous d'un seuil fixé (typiquement < ~-50 dB), hop, 1 plage !
// 	C'est peut être une option (un bouton) à cogiter pour une future mouture,
// 	non ? M'est avis qu'not' Christophe serait pas contre...					
// 
// PS:
// 	Et chose fut faite: MERCI Xavier  :-)
// 
void SplitSpectre_search_blank_pass (void)
{
	gint		IndicePoints = -1;
	gint		PointBegin = -1;
	gint		PointEnd = -1;
	gint		Indice;
	gdouble		PercentBegin;
	gdouble		PercentEnd;
	gint		ValueBegin;
	gint		ValueEnd;
	gint		MaxPoints = VarSplit.MaxPointsInTab;
	gboolean	BoolErr = FALSE;
	
	if (TRUE == VarSplit.BoolBlankWithCue) {
		PRINT("TRUE == VarSplit.BoolBlankWithCue");
		return;
	}
	
	PRINT_FUNC_LF();
	
	Indice = 0;
	do {
		if (VarSplit.Tab [ Indice ].Min == 0 && VarSplit.Tab [ Indice ].Max == 0) {
			
			if (-1 == PointBegin) {
				while (Indice < MaxPoints && VarSplit.Tab [ Indice ].Min == 0 && VarSplit.Tab [ Indice ].Max == 0) Indice ++;
				PointBegin = Indice;
				while (Indice < MaxPoints && (VarSplit.Tab [ Indice ].Min != 0 || VarSplit.Tab [ Indice ].Max != 0)) Indice ++;
				PointEnd = Indice;
				if (PointBegin > -1 && PointEnd - PointBegin > 13) {
					
					IndicePoints ++;
					if( TRUE == OptionsCommandLine.BoolVerboseMode )
						g_print ("PointBegin = %6d   PointEnd = %6d   Diff = %6d\n", PointBegin, PointEnd, PointEnd - PointBegin);
					
					PercentBegin = ((gdouble)PointBegin / (gdouble)MaxPoints) * 100.0;
					ValueBegin = ((gdouble)SplitSpectre_get_with() * (gdouble)PercentBegin) / 100.0;
					
					PercentEnd = ((gdouble)PointEnd / (gdouble)MaxPoints) * 100.0;
					ValueEnd = ((gdouble)SplitSpectre_get_with() * (gdouble)PercentEnd) / 100.0;
										
					VarSplit.Selecteur [ IndicePoints ] . Nmr          = IndicePoints;
					VarSplit.Selecteur [ IndicePoints ] . BeginPaint   = ValueBegin;
					VarSplit.Selecteur [ IndicePoints ] . PercentBegin = PercentBegin;
					VarSplit.Selecteur [ IndicePoints ] . EndPaint     = ValueEnd;
					VarSplit.Selecteur [ IndicePoints ] . PercentEnd   = PercentEnd;
				}
				PointBegin = -1;
				PointEnd = -1;
			}
		}
		Indice ++;
	} while (Indice < MaxPoints);
	
	if (-1 == IndicePoints) {
		IndicePoints = 0;
		VarSplit.Selecteur [ IndicePoints ] . Nmr          = IndicePoints;
		PercentEnd = ((gdouble)(MaxPoints - 1) / (gdouble)MaxPoints) * 100.0;
		ValueEnd = ((gdouble)SplitSpectre_get_with() * (gdouble)PercentEnd) / 100.0;
		VarSplit.Selecteur [ 0 ] . PercentEnd = PercentEnd;
		VarSplit.Selecteur [ 0 ] . EndPaint   = ValueEnd;
	}
	
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("\n");
		g_print ("IndicePoints = [ 0 .. %d ]\n", IndicePoints);
		g_print ("\n");
	}
	
	// DEBUT
	Indice = 0;
	while (Indice < MaxPoints && VarSplit.Tab [ Indice ].Min == 0 && VarSplit.Tab [ Indice ].Max == 0) Indice ++;
	PercentBegin = ((gdouble)Indice / (gdouble)MaxPoints) * 100.0;
	ValueBegin = ((gdouble)SplitSpectre_get_with() * (gdouble)PercentBegin) / 100.0;
	VarSplit.Selecteur [ 0 ] . PercentBegin = PercentBegin;
	VarSplit.Selecteur [ 0 ] . BeginPaint   = ValueBegin;
	
	// VERIFIE QUE LES POINTS NE SE CHEVAUCHENT PAS
	BoolErr = FALSE;
	for (IndicePoints = 0; VarSplit.Selecteur [ IndicePoints ] . Nmr != -1; IndicePoints ++) {
		if (IndicePoints > 0) {
			// if ((gint)VarSplit.Selecteur [ IndicePoints -1 ] . End >= (gint)VarSplit.Selecteur [ IndicePoints ] . Begin) {
				// g_print ("[ %02d ] ERREUR entre End %d et Begin %d  -->  RECTIFICATION\n", IndicePoints, IndicePoints -1, IndicePoints);
				// BoolErr = TRUE;
				// break;
			// }
			if ((gint)VarSplit.Selecteur [ IndicePoints ] . BeginPaint >= (gint)VarSplit.Selecteur [ IndicePoints ] . EndPaint) {
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("[ %02d ]ERREUR entre Begin %d et End %d  -->  RECTIFICATION\n", IndicePoints, IndicePoints, IndicePoints);
				BoolErr = TRUE;
				break;
			}
		}
	}
	
	// VERIFICATION DE LA COHERENCE DU TEMPS ENTRE DEUX POINTS BEGIN et END
	if (FALSE == BoolErr) {
		for (IndicePoints = 0; VarSplit.Selecteur [ IndicePoints ] . Nmr != -1; IndicePoints ++) {
			if (SplitSelector_get_diff_sec (VarSplit.Selecteur [ IndicePoints ] . PercentBegin, VarSplit.Selecteur [ IndicePoints ] . PercentEnd) <= 5) {
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("ERREUR Temps < a 5 secondes entre BEGIN %d et END %d  -->  RECTIFICATION\n", IndicePoints, IndicePoints);
				BoolErr = TRUE;
				break;
			}
		}
	}
	
	if (TRUE == BoolErr) {
		for (IndicePoints = 1; IndicePoints < MAX_SELECTEURS_SPLIT; IndicePoints ++) {
			VarSplit.Selecteur [ IndicePoints ] . Nmr = -1;
			VarSplit.Selecteur [ IndicePoints ] . BeginPaint = -1;
			VarSplit.Selecteur [ IndicePoints ] . EndPaint = -1;
			VarSplit.Selecteur [ IndicePoints ] . PercentBegin =
			VarSplit.Selecteur [ IndicePoints ] . PercentEnd = 0.0;
		}
		IndicePoints = 0;
		VarSplit.Selecteur [ IndicePoints ] . Nmr          = IndicePoints;
		PercentEnd = ((gdouble)(MaxPoints - 1) / (gdouble)MaxPoints) * 100.0;
		ValueEnd = ((gdouble)SplitSpectre_get_with() * (gdouble)PercentEnd) / 100.0;
		VarSplit.Selecteur [ 0 ] . PercentEnd = PercentEnd;
		VarSplit.Selecteur [ 0 ] . EndPaint   = ValueEnd;
	}
	
	// PRINT CONSOLE
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		for (IndicePoints = 0; VarSplit.Selecteur [ IndicePoints ] . Nmr != -1; IndicePoints ++) {
			g_print ("[ %2d ]   BEGIN = %5d  END = %5d  PERCENTBEGIN = %f  PERCENTEND = %f\n",
				VarSplit.Selecteur [ IndicePoints ] . Nmr,
				(gint)VarSplit.Selecteur [ IndicePoints ] . BeginPaint,
				(gint)VarSplit.Selecteur [ IndicePoints ] . EndPaint,
				VarSplit.Selecteur [ IndicePoints ] . PercentBegin,
				VarSplit.Selecteur [ IndicePoints ] . PercentEnd
				);
		}
		g_print ("\n");
	}
}
// GET NBR ALLOC
//
/**
gint get_alloc( gchar *p_PathNameFile )
{
	WAVE		WaveHeader;
	gint		Ret;
	gint		points;
	unsigned char	devbuf[ BLOCK_SIZE +10 ];
	gint		Indice;
	gint		ChannelNext;
	
	tagswav_read_file( p_PathNameFile, &WaveHeader );
	ChannelNext = (WaveHeader.FMT.NumChannels -1) * (WaveHeader.FMT.BitsPerSample / 8);
	points = 0;
	while ((Ret = fread (devbuf, 1, BLOCK_SIZE, WaveHeader.file)) > 0) {
		for (Indice = 0; Indice < Ret; Indice ++ ) {
			Indice += ChannelNext;
		}
		points ++;
	}
	tagswav_close_file( &WaveHeader );
	return( points );
}
*/
// LECTURE FICHIER
//
gboolean SplitSpectre_read_file_spectre (gchar *PathNameFile)
{
	FILE		*pFile = NULL;
	gboolean	found = FALSE;
	gint		Ret;
	gint		Indice;
	gint		ChannelNext;
	gint		min, max;
	gint		tmp = 0;
	gint		points;
	unsigned char	devbuf[ BLOCK_SIZE +10 ];
	WAVE		WaveHeader;
	TYPE_FILE_IS	TypeFileIs = FILE_IS_NONE;
	INFO_WAV		*Tags;

	PRINT_FUNC_LF();
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print("\t%s\n\n", PathNameFile);
	
	// VarSplit.MaxPointsInTab = get_alloc( PathNameFile );
	
	VarSplit.TypeFileIs = FILE_IS_NONE;
	if (FILE_IS_WAV != (TypeFileIs = GetInfo_file_is (PathNameFile))) {
		
		// DEL ALLOC MEMORY
		if (NULL != VarSplit.Tab) {
			g_free (VarSplit.Tab);	VarSplit.Tab = NULL;
			return (TRUE);
		}
	}
	
	if (FALSE == tagswav_read_file (PathNameFile, &WaveHeader)) {
		tagswav_close_file (&WaveHeader);
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			printf ("\nImpossible d'ouvrir le fichier : \"%s\"\n\n", PathNameFile);
		return FALSE;
	}
	pFile = WaveHeader.file;
	WaveHeader.file = NULL;
	
	if (pFile == NULL) {
		PRINT_FUNC_LF();
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("\nImpossible d'ouvrir le fichier : \"%s\"\n\n", PathNameFile);
		return FALSE;
	}
	VarSplit.nBitsPerSample = WaveHeader.FMT.BitsPerSample;
	
	switch (WaveHeader.FMT.BitsPerSample) {
	case 8 :
	case 16 :
	case 24 :
	case 32 :
		break;
	default :
		{
		fclose (pFile);
		
		PRINT_FUNC_LF();
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print("Seuls ces formats de bitrates : 8 16 24 et 32 sont pris en charge pour la lecture\n"
				"Format de bitrate [ %d ] non prit en charge\n", 
				WaveHeader.FMT.BitsPerSample);
			
		// DEL ALLOC MEMORY
		if (NULL != VarSplit.Tab) {
			g_free (VarSplit.Tab);	VarSplit.Tab = NULL;
		}
		return FALSE;
		}
	}
	VarSplit.nTotalChunckSize = WaveHeader.DATA.Subchunk2Size;
	found = TRUE;
		
	// DEL ALLOC MEMORY
	if (NULL != VarSplit.Tab) {
		g_free (VarSplit.Tab);	VarSplit.Tab = NULL;
	}

	
	if (found == TRUE) {
			
		VarSplit.TotalAllocation = sizeof(POINTS_FILE) * ((WaveHeader.DATA.Subchunk2Size / BLOCK_SIZE));
		// VarSplit.TotalAllocation = sizeof(POINTS_FILE) * VarSplit.MaxPointsInTab;
		VarSplit.Tab = (POINTS_FILE *)g_malloc0 (VarSplit.TotalAllocation + 10);
		
		points = 0;
		if (WaveHeader.FMT.BitsPerSample <= 0) WaveHeader.FMT.BitsPerSample = 8;

		// TRAITEMENT UNIQUES: 8, 16, 24 et 32 bits
		switch (WaveHeader.FMT.BitsPerSample) {
			case 8 :
			case 16 :
			case 24 :
			case 32 :
				// PRE - CALCUL

				ChannelNext = (WaveHeader.FMT.NumChannels -1) * (WaveHeader.FMT.BitsPerSample / 8);

				// LECTURE DES DATAS
				// #define BLOCK_SIZE	2352
				//
				while ((Ret = fread (devbuf, 1, BLOCK_SIZE, pFile)) > 0) {

					min = max = 0;

					// for (Indice = 0; Indice < Ret; Indice++) {
					for( Indice = 0; Indice < Ret; ) {

						if (WaveHeader.FMT.BitsPerSample == 8) {
							
							tmp = devbuf [ Indice ];
							tmp -= 128;
						}
						else if (WaveHeader.FMT.BitsPerSample == 16) {
							
							tmp = (gchar)devbuf [ Indice +1 ] << 8 | (gchar)devbuf [ Indice ];
							// Indice ++;
						}
						else if (WaveHeader.FMT.BitsPerSample == 24) {
							
							tmp  = (gchar)devbuf [ Indice +0 ] >> 16;
							tmp += (gchar)devbuf [ Indice +1 ] >> 8;
							tmp += (gchar)devbuf [ Indice +2 ];
							// Indice += 2;
						}
						else if (WaveHeader.FMT.BitsPerSample == 32) {
							
							tmp  = (gchar)devbuf [ Indice +0 ] >> 24;
							tmp += (gchar)devbuf [ Indice +1 ] >> 16;
							tmp += (gchar)devbuf [ Indice +2 ] >> 8;
							tmp += (gchar)devbuf [ Indice +3 ];
							// Indice += 2;
						}
						
						if( tmp > max )
							max = tmp;
						else if( tmp <= min )
							min = tmp;

						// skip over any extra channels
						Indice += ChannelNext;
					}
					VarSplit.Tab [ points ].Min = min;
					VarSplit.Tab [ points ].Max = max;
					
					points ++;
				}
				break;
			default :
				PRINT("Ne doit pas arriver ici  !!!!");
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("Ce format n'est pas pris en charge : %d\n", WaveHeader.FMT.BitsPerSample);
				fclose (pFile);
				return FALSE;
		}
	}
	fclose (pFile);
	
	VarSplit.MaxPointsInTab = points;
	
	// ALL DATAS
	// points * BLOCK_SIZE
	/*g_print( "points(%d) * BLOCK_SIZE(%d) = \n\t%d octets\n\t%d Ko\n\t%d Mo\n\n",
					points, 
					BLOCK_SIZE, 
					points * BLOCK_SIZE,
					(points * BLOCK_SIZE) / 1024, 
					((points * BLOCK_SIZE) / 1024) / 1024
					 );*/
	
	
	// TODO: MARQUAGE DES PASSAGES BLANC SOIT < ~-50 dB
	for (Indice = 0; Indice < points; Indice ++) {
		if (VarSplit.Tab [ Indice ].Min >= -1. && VarSplit.Tab [ Indice ].Max <= 1.) {
			VarSplit.Tab [ Indice ].Min =
				VarSplit.Tab [ Indice ].Max = 0.0;
		}
	}
	
	Tags = (INFO_WAV *)tagswav_get_info( PathNameFile );
	VarSplit.sec_in_time = Tags->SecTime;
	Tags = tagswav_remove_info( Tags );
	Tags = NULL;
	
	// RECHEFCHE DES PLAGES SELON L IDEE DE @Dzef  ;-)
	SplitSpectre_search_blank_pass ();

	VarSplit.BoolReadFileSpectre = TRUE;
	VarSplit.TypeFileIs = TypeFileIs;

	return (TRUE);
}
// TODO
// AFFICHAGE DU SPECTRE
// RAPIDITE DE REAFFICHAGE DE L'AMPLITUDE ACCRUE AVEC LES FONCTIONS gdk_draw_ (...)
// 13 juillet 2009
// 12 mars 2010
// 
void SplitSpectre_draw_lines( GtkWidget *widget, cairo_t *p_cr )
{
	GtkAllocation	allocation;
	gint			points;
	gint			y_min;
	gint			y_max;
	gint			xaxis;
	gint			scale;
	gint			k;
	gdouble			Percent;
	gdouble			SpectreGetWith;
	gdouble			MaxScale_0_y;
	gdouble			MinScale_0_y;
	gint			Begin;
	gint			save_begin;
	gint			End;
	gint			SelBegin = 0;
	gint			SelEnd = 0;
	gint			Len;
	gboolean 		bool_cairo_stroke = FALSE;
typedef struct {
	gdouble r;
	gdouble v;
	gdouble b;
} TAB_COLOR;
TAB_COLOR tab_color[ 4 ] = 
{
	{  23./255.,  55./255.,  93./255. },
	{  83./255., 142./255., 213./255. },
	{ 141./255., 180./255., 227./255. },
	{ 128./255., 128./255., 128./255. }
};

	if (NULL == widget) return;
	
	gtk_widget_get_allocation( widget, &allocation );
	cairo_scale( p_cr, 1.0, 1.0 );
	cairo_translate( p_cr, allocation.x, allocation.y );
	
	// CLEAR  PREVIEW SURFACE
	// CRAYON_GRIS
	cairo_set_source_rgb( p_cr, 1.0, 1.0, 1.0 );
	cairo_rectangle( p_cr,
			 0,
			 0,
			 allocation.width,
			 allocation.height
			 );			
	cairo_fill( p_cr );
	cairo_paint( p_cr );
	
	if( NULL == VarSplit.Tab ) return;
	
	SpectreGetWith = SplitSpectre_get_with();
	
	xaxis = (allocation.height -1) / 2;

	if (xaxis != 0) {
		if (VarSplit.nBitsPerSample == 16) {
			scale = SHRT_MAX  / (xaxis -1);
		} 
		else {
			scale = UCHAR_MAX / (xaxis -1);
		}
		if (scale == 0) {
			scale = 1;
		}
	} else {
		scale = 1;
	}
	
	// cairo_set_source_rgb( p_cr, 0.4, 0.4, 0.4 );
	// EPAISSEUR
	cairo_set_line_width( p_cr, 1 );

/**
	save_begin =
	Begin    = gtk_adjustment_get_value( VarSplit.AdjScroll );
	Len      = 
	End      = Begin + SpectreGetWith;
	SelBegin = SplitSelector_get_pos_begin( VarSplit.SelecteurActif );
	SelEnd   = SplitSelector_get_pos_end( VarSplit.SelecteurActif );
	
	for( points = 0; Begin < End; Begin ++, points ++ ) {
		
		if( points > VarSplit.MaxPointsInTab ) {
			// printf("------------------------------- BREAK \n");
			break;
		}
		Percent = ( (gdouble)Begin / SpectreGetWith ) * 100.;
		k = ( Percent * (gdouble)VarSplit.MaxPointsInTab ) / 100.;
		
		if( k >= VarSplit.MaxPointsInTab )
			k = VarSplit.MaxPointsInTab ;
		
		y_max = xaxis - VarSplit.Tab[ k ].Max / scale;
		y_min = xaxis + fabs( VarSplit.Tab[ k ].Min ) / scale;
		
		if (Begin >= SelBegin &&  Begin <= SelEnd) {
			cairo_set_source_rgb( p_cr, tab_color[ 0 ].r, tab_color[ 0 ].v, tab_color[ 0 ].b );
			MaxScale_0_y = y_max - (y_max - xaxis) * 0 / SAMPLE_SHADES;
			MinScale_0_y = y_min + (xaxis - y_min) * 0 / SAMPLE_SHADES;		
			cairo_move_to( p_cr, points, MinScale_0_y );
			cairo_rel_line_to( p_cr, 0.0, (MaxScale_0_y - MinScale_0_y) <= 0 ? MaxScale_0_y - MinScale_0_y : 1 );
			cairo_stroke ( p_cr );
			
			cairo_set_source_rgb( p_cr, tab_color[ 1 ].r, tab_color[ 1 ].v, tab_color[ 1 ].b );
			MaxScale_0_y = y_max - (y_max - xaxis) * 1 / SAMPLE_SHADES - 1;
			MinScale_0_y = y_min + (xaxis - y_min) * 1 / SAMPLE_SHADES - 1;			
			cairo_move_to( p_cr, points, MinScale_0_y );
			cairo_rel_line_to( p_cr, 0.0, (MaxScale_0_y - MinScale_0_y) <= 0 ? MaxScale_0_y - MinScale_0_y : 1 );
			cairo_stroke ( p_cr );
			
			cairo_set_source_rgb( p_cr, tab_color[ 2 ].r, tab_color[ 2 ].v, tab_color[ 2 ].b );
			MaxScale_0_y = y_max - (y_max - xaxis) * 2 / SAMPLE_SHADES - 1;
			MinScale_0_y = y_min + (xaxis - y_min) * 2 / SAMPLE_SHADES - 1;			
			cairo_move_to( p_cr, points, MinScale_0_y );
			cairo_rel_line_to( p_cr, 0.0, (MaxScale_0_y - MinScale_0_y) <= 0 ? MaxScale_0_y - MinScale_0_y : 1 );
			cairo_stroke ( p_cr );
		}
		else {
			cairo_set_source_rgb( p_cr, 128./255., 128./255., 128./255. );
			MaxScale_0_y = y_max - (y_max - xaxis) * 0 / SAMPLE_SHADES;
			MinScale_0_y = y_min + (xaxis - y_min) * 0 / SAMPLE_SHADES;		
			cairo_move_to( p_cr, points, MinScale_0_y );
			cairo_rel_line_to( p_cr, 0.0, (MaxScale_0_y - MinScale_0_y) <= 0 ? MaxScale_0_y - MinScale_0_y : 1 );
			cairo_stroke ( p_cr );
		}
	}

*/

	// 
	// SELECT
	// ... 0 / SAMPLE_SHADES
	// 
	save_begin =
	Begin    = gtk_adjustment_get_value( VarSplit.AdjScroll );
	Len      = 
	End      = Begin + SpectreGetWith;
	SelBegin = SplitSelector_get_pos_begin( VarSplit.SelecteurActif );
	SelEnd   = SplitSelector_get_pos_end( VarSplit.SelecteurActif );
	
	cairo_set_source_rgb( p_cr, tab_color[ 0 ].r, tab_color[ 0 ].v, tab_color[ 0 ].b );
	for( points = 0; Begin < End; Begin ++, points ++ ) {
		
		if( Begin >= SelBegin &&  Begin <= SelEnd ) {
			if( points > VarSplit.MaxPointsInTab ) {
				// printf("------------------------------- BREAK \n");
				// break;
			}
			Percent = ( (gdouble)Begin / SpectreGetWith ) * 100.;
			k = ( Percent * (gdouble)VarSplit.MaxPointsInTab ) / 100.;
			
			if( k >= VarSplit.MaxPointsInTab )
				k = VarSplit.MaxPointsInTab ;
			
			y_max = xaxis - VarSplit.Tab[ k ].Max / scale;
			y_min = xaxis + fabs( VarSplit.Tab[ k ].Min ) / scale;
		
			MaxScale_0_y = y_max - (y_max - xaxis) * 0 / SAMPLE_SHADES;
			MinScale_0_y = y_min + (xaxis - y_min) * 0 / SAMPLE_SHADES;		
			cairo_move_to( p_cr, points, MinScale_0_y );
			cairo_rel_line_to( p_cr, 0.0, (MaxScale_0_y - MinScale_0_y) <= 0 ? MaxScale_0_y - MinScale_0_y : 1 );
			bool_cairo_stroke = TRUE;
		}	
	}
	if( bool_cairo_stroke )
		cairo_stroke ( p_cr );
	// 
	// SELECT
	// ... 1 / SAMPLE_SHADES
	// 
	bool_cairo_stroke = FALSE;
	Begin = save_begin;
	cairo_set_source_rgb( p_cr, tab_color[ 1 ].r, tab_color[ 1 ].v, tab_color[ 1 ].b );
	for( points = 0; Begin < End; Begin ++, points ++ ) {
		
		if (Begin >= SelBegin &&  Begin <= SelEnd) {
			if( points > VarSplit.MaxPointsInTab ) {
				// printf("------------------------------- BREAK \n");
				// break;
			}
			Percent = ( (gdouble)Begin / SpectreGetWith ) * 100.;
			k = ( Percent * (gdouble)VarSplit.MaxPointsInTab ) / 100.;
			
			if( k >= VarSplit.MaxPointsInTab )
				k = VarSplit.MaxPointsInTab ;
			
			y_max = xaxis - VarSplit.Tab[ k ].Max / scale;
			y_min = xaxis + fabs( VarSplit.Tab[ k ].Min ) / scale;
		
			MaxScale_0_y = y_max - (y_max - xaxis) * 1 / SAMPLE_SHADES - 1;
			MinScale_0_y = y_min + (xaxis - y_min) * 1 / SAMPLE_SHADES - 1;			
			cairo_move_to( p_cr, points, MinScale_0_y );
			cairo_rel_line_to( p_cr, 0.0, (MaxScale_0_y - MinScale_0_y) <= 0 ? MaxScale_0_y - MinScale_0_y : 1 );
			bool_cairo_stroke = TRUE;
		}	
	}
	if( bool_cairo_stroke )
		cairo_stroke ( p_cr );
	// 
	// SELECT
	// ... 2 / SAMPLE_SHADES
	// 
	bool_cairo_stroke = FALSE;
	Begin = save_begin;
	cairo_set_source_rgb( p_cr, tab_color[ 2 ].r, tab_color[ 2 ].v, tab_color[ 2 ].b );
	for( points = 0; Begin < End; Begin ++, points ++ ) {
		
		if (Begin >= SelBegin &&  Begin <= SelEnd) {
			if( points > VarSplit.MaxPointsInTab ) {
				// printf("------------------------------- BREAK \n");
				// break;
			}
			Percent = ( (gdouble)Begin / SpectreGetWith ) * 100.;
			k = ( Percent * (gdouble)VarSplit.MaxPointsInTab ) / 100.;
			
			if( k >= VarSplit.MaxPointsInTab )
				k = VarSplit.MaxPointsInTab ;
			
			y_max = xaxis - VarSplit.Tab[ k ].Max / scale;
			y_min = xaxis + fabs( VarSplit.Tab[ k ].Min ) / scale;
		
			MaxScale_0_y = y_max - (y_max - xaxis) * 2 / SAMPLE_SHADES - 1;
			MinScale_0_y = y_min + (xaxis - y_min) * 2 / SAMPLE_SHADES - 1;			
			cairo_move_to( p_cr, points, MinScale_0_y );
			cairo_rel_line_to( p_cr, 0.0, (MaxScale_0_y - MinScale_0_y) <= 0 ? MaxScale_0_y - MinScale_0_y : 1 );
			bool_cairo_stroke = TRUE;
		}	
	}
	if( bool_cairo_stroke )
		cairo_stroke ( p_cr );
	// 
	// NO SELECT
	// ... 2 / SAMPLE_SHADES
	// 
	Begin = save_begin;
	cairo_set_source_rgb( p_cr, tab_color[ 3 ].r, tab_color[ 3 ].v, tab_color[ 3 ].b );
	for( points = 0; Begin < End; Begin ++, points ++ ) {
		
		if (Begin < SelBegin || Begin > SelEnd) {
			if( points > VarSplit.MaxPointsInTab ) {
				// printf("------------------------------- BREAK \n");
				// break;
			}
			Percent = ( (gdouble)Begin / SpectreGetWith ) * 100.;
			k = ( Percent * (gdouble)VarSplit.MaxPointsInTab ) / 100.;
			
			if( k >= VarSplit.MaxPointsInTab )
				k = VarSplit.MaxPointsInTab ;
			
			y_max = xaxis - VarSplit.Tab[ k ].Max / scale;
			y_min = xaxis + fabs( VarSplit.Tab[ k ].Min ) / scale;
		
			MaxScale_0_y = y_max - (y_max - xaxis) * 0 / SAMPLE_SHADES;
			MinScale_0_y = y_min + (xaxis - y_min) * 0 / SAMPLE_SHADES;		
			cairo_move_to( p_cr, points, MinScale_0_y );
			cairo_rel_line_to( p_cr, 0.0, (MaxScale_0_y - MinScale_0_y) <= 0 ? MaxScale_0_y - MinScale_0_y : 1 );
			bool_cairo_stroke = TRUE;
		}	
	}
	if( bool_cairo_stroke )
		cairo_stroke ( p_cr );

	// 	SelBegin = SplitSelector_get_pos_begin (VarSplit.SelecteurActif);
	// 	SelEnd   = SplitSelector_get_pos_end (VarSplit.SelecteurActif);
	
	// LIGNE SELECTEUR ACTIF DEBUT
	if (SelBegin >= save_begin && SelBegin < Len) {
		cairo_set_source_rgb( p_cr, 244. / 255., 162. / 255., 87. / 255. );
		cairo_move_to( p_cr, SelBegin - save_begin, 0.0 );
		cairo_rel_line_to( p_cr, 0.0, allocation.height );
		cairo_stroke ( p_cr );
	}
	
	// LIGNE SELECTEUR ACTIF FIN
	if (VarSplit.SelecteurActif == VarSplit.NbrSelecteurs -1) SelEnd --;
	if (SelEnd >= save_begin && SelEnd < Len) {
		cairo_set_source_rgb( p_cr, 0.0, 1.0, 0.0 );
		cairo_move_to( p_cr, SelEnd - save_begin, 0.0 );
		cairo_rel_line_to( p_cr, 0.0, allocation.height );
		cairo_stroke ( p_cr );
	}
	
	// AFFICHE LE CURSEUR AUDIO
	if (VarSplit.PercentActivePlay >= 0.0) {
		MarkerOld = (gint)(((gdouble)SplitSpectre_get_with() * VarSplit.PercentActivePlay) / 100.0);
		if( MarkerOld < save_begin ) MarkerOld = save_begin + 1;
		if( MarkerOld > Len-4 ) MarkerOld = Len - 4;
		if (MarkerOld >= save_begin && MarkerOld < Len ) {
			cairo_set_source_rgb( p_cr, 1.0, 0.0, 0.0 );
			cairo_move_to( p_cr, MarkerOld - save_begin, 0.0 );
			cairo_rel_line_to( p_cr, 0.0, allocation.height );
			cairo_stroke ( p_cr );
			MarkerOld -= 6;
			for( points = 0; points <= 12; points ++ ) {
				cairo_move_to( p_cr, points + (MarkerOld - save_begin ), allocation.height -12 );
				cairo_rel_line_to( p_cr, 0.0, 12 );
				cairo_stroke ( p_cr );
			}
		}
	}
}






















