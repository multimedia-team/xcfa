 /*
 *  file    : tags_dts.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>



/*
*---------------------------------------------------------------------------
* FILE IS DTS ?
*---------------------------------------------------------------------------
*/
/*
$ file *
	audio.dts:    data
*/

/*
*---------------------------------------------------------------------------
* REMOVE HEADER
*---------------------------------------------------------------------------
*/
INFO_DTS *tagsdts_remove_info (INFO_DTS *info)
{
	if (info) {
		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}
	return ((INFO_DTS *)NULL);
}

/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/
INFO_DTS *tagsdts_get_info (DETAIL *detail)
{
	INFO_DTS     *ptrinfo = NULL;

	ptrinfo = (INFO_DTS *)g_malloc0 (sizeof (INFO_DTS));
	if (ptrinfo == NULL) return (NULL);

	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);
	tags_set (detail->namefile, ptrinfo->tags);

	return ((INFO_DTS *)ptrinfo);
}











