 /*
 *  file      : configuser_new.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 /*
 * New Developpment File:
 * Fri, 15 Jul 2011 22:45:07 +0200
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
 

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "options.h"
#include "cd_audio.h"
#include "configuser.h"
#include "global.h"



typedef struct {
	gchar	**Larrbuf;
} CONF;

CONF	Conf = { NULL };

VAR_CONFIG ConfigSaveToRest;	// Pour les restitutions par defaut
VAR_CONFIG Config = {
	FALSE,		// BoolConfigOk
	1,			// gulong	UsesOfXcfa;	// 0 to G_MAXULONG ( 18446744073709551615 )
	0,			// WinPos_X
	0,			// WinPos_Y
	800,		// WinWidth
	520,		// WinHeight
	0,			// Nice
	NULL,		// PathLoadFileAll
	NULL,		// PathLoadFileWav
	NULL,		// PathLoadFileMp3Ogg
	NULL,		// PathLoadFileTags
	{0,0,0,0},	// TabIndiceComboDestFile
	NULL,		// PathDestinationFileAll
	NULL,		// PathDestinationFileWav
	NULL,		// PathDestinationFileMp3Ogg
	NULL,		// PathDestinationFileTags
	NULL,		// PathDestinationDVD
	NULL,		// PathChoiceFileDVD
	NULL,		// PathDestinationCD
	NULL,		// PathChoiceFileCD
	NULL,		// PathDestinationSplit
	NULL,		// PathLoadSplit
	NULL,		// PathnameTMP
	NULL,		// PathStockeImagesPochette
	NULL,		// PathDestFilePostScript
	NULL,		// PathLoadImages
	0,			// NotebookGeneral
	0,			// NotebookExpanderCd
	0,			// NotebookFile
	0,			// NotebookOptions
	0,			// NotebookAppExterns
	0,			// ExtractCdWith = EXTRACT_WITH_CDPARANOIA = 0
	1,			// BitrateLameIndice
	{0,8,0,0},	// TabBitrateLame
	{0,0,0,0},	// TabModeLame
	15,			// BitrateOggenc
	1,			// ManagedOggenc
	1,			// DownmixOggenc
	5,			// CompressionLevelFlac
	1,			// CompressionLevelApeMac
	0,			// CompressionWavpack
	1,			// SoundWavpack
	0,			// ModeHybrideWavpack
	0,			// CorrectionFileWavpack
	0,			// CompressionMaximumWavpack
	0,			// SignatureMd5Wavpack
	0,			// ExtraEncodingWavpack
	5,			// QualityMppenc
	0,			// ConteneurFacc
	0,			// AbrVbrFacc
	0,			// AbrFaccIndice
	6,			// VbrFaccIndice
	7,			// ChoiceMonoAacplusenc
	4,			// ChoiceStereoAacplusenc 
	TRUE,		// BoolArtistTag
	TRUE,		// BoolTitleTag
	TRUE,		// BoolAlbumTag
	TRUE,		// BoolNumerateTag
	TRUE,		// BoolGenreTag
	TRUE,		// BoolYearTag
	TRUE,		// BoolCommentTag
	TRUE,		// BoolEtatExpanderCd
	NULL,		// StringExpanderLame		Lignes de saisie: options pour les geeks
	NULL,		// StringExpanderOggenc
	NULL,		// StringExpanderFlac
	NULL,		// StringExpanderFaac
	NULL,		// StringExpanderMppenc
	NULL,		// StringExpanderMac
	NULL,		// StringExpanderWavpack
	0,			// ServeurCddb			SERVER_CDDB_DEFAULT = 0 OR SERVER_CDDB_PARAM
	NULL,		// StringNameFile_m3u_xspf
	NULL,		// Templates_title_cdaudio
	NULL,		// Templates_rep_cdaudio
	NULL,		// StringNameNavigateur
	NULL,		// StringParamNameNavigateur
	NULL,		// StringNameLecteurPostScript
	NULL,		// StringNameLecteurAudio
	NULL,		// StringParamNameLecteurAudio
	NULL,		// StringBoolFieldsIsVisible	FIELDS DVD,CD,FILES : FALSE or HIDE = 0, TRUE or SHOW = 1
	NULL,		// StringPosFieldsName		POS NAME OF DVD,CD,FILES: 0 = LEFT, 1 = CENTER, 2 = RIGHT
	0,			// NumSelectComboBoxDvd
	0,			// NumSelectComboBoxCd
	NULL,		// entry_cddp_server
	NULL,		// entry_proxy_server;
	NULL,		// entry_proxy_port
	NULL,		// NameImg
	NULL,		// PathLoadImg
	NULL,		// PathSaveImg;
	NULL,		// PathPochette
	FALSE,		// gboolean	BoolCheckbuttonEndOfConvert
	NULL,		// gchar	*PathMusicFileEndOfConvert
	NULL,		// gchar	*FileMusicFileEndOfConvert
	NULL,		// gchar	*StringCommentCD
	
	FALSE		// gboolean	BoolLogCdparanoiaModeExpert

};



// TRUE SI p_dest EST DANS p_src SINON FALSE
//
gboolean config_new_strcmp (gchar *p_src, gchar *p_dest)
{
	gint	len_aiguille = strlen (p_dest);
	gint	len_meule;
	gint	len;
	gchar	*PtrBeginSrc = NULL;
	gchar	*PtrEndSrc = NULL;
	gchar	*PtrSrc = NULL;
	gchar	*PtrDest = p_dest;
	
	for (PtrBeginSrc = p_src; *PtrBeginSrc == ' ' || *PtrBeginSrc == '\t'; PtrBeginSrc ++);
	for (PtrEndSrc = PtrBeginSrc; *PtrEndSrc != ' ' && *PtrEndSrc != '\t'; PtrEndSrc ++);
	len_meule = PtrEndSrc - PtrBeginSrc;
	
	if (len_meule != len_aiguille) return (FALSE);
	
	len = 0;
	for (PtrSrc = PtrBeginSrc; PtrSrc < PtrEndSrc; PtrSrc ++, PtrDest ++) {
		if (*PtrSrc != *PtrDest) return (FALSE);;
		len ++;
	}
	
	return (len_meule == len ? TRUE : FALSE);
}
// 
// 
void config_print_data (gchar *p_str)
{
	gint	len = strlen( p_str );
	g_print( "%s", p_str );
	while( len < 33 ) {
		g_print( " " );
		len ++;
	}
	g_print( "= " );
}
// RETOURNE LE NOM COMPLET DU FICHIER DE CONFIGURATION
// 
/*
gchar *OLD_config_get_pathname( void )
{
	// @Tetsumaki : http://forum.ubuntu-fr.org/viewtopic.php?pid=3890967#p3890967
	// return ((gchar *)g_strdup_printf ("%s/%s", getenv ("HOME"), ".config/xcfa_new/xcfa.conf"));
	return( (gchar *)g_strdup_printf( "%s/%s", getenv( "HOME"), ".config/xcfa/xcfa.conf" ));
}
gchar *config_get_pathname( void )
{
	const	gchar *Lenv = NULL;
	gchar	*Lpath = NULL;
	
	if( NULL != ( Lenv = g_getenv( "XDG_CONFIG_HOME" ))) {
		Lpath = g_strdup_printf("%s/%s/%s.conf", Lenv, PACKAGE, PACKAGE);
	}
	else if( NULL != ( Lenv = g_getenv( "HOME" ))) {
		Lpath = g_strdup_printf("%s/.config/%s/%s.conf", Lenv, PACKAGE, PACKAGE);
	}
	PRINT_FUNC_LF();
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print( "\t(gchar *)Lpath = %s\n", Lpath );
	return( (gchar *)Lpath );
}
*/
// RETOURNE UNE DEFINITION DU FICHIER DE CONFIGURATION
// 
gchar *config_get_target( gchar *p_buf )
{
	gchar	*Str = NULL;
	gchar	*Ptr = NULL;
	
	if( NULL != ( Ptr = strchr( p_buf, '=' ))) {
		Ptr ++;
		while( *Ptr && (*Ptr != ' ' && *Ptr != '\t' )) Ptr ++;
		Ptr ++;
		Str = g_strdup( Ptr );
		if( NULL != ( Ptr = strchr( Str, '\r' ))) *Ptr = '\0';
		if( NULL != ( Ptr = strchr( Str, '\n' ))) *Ptr = '\0';
	}
	return( (gchar *)Str );
}
// 	
// 	
gboolean config_get_bool( gchar *value )
{
	gchar *ptr = value;

	while( *ptr ) {
		*ptr = toupper( *ptr );
		ptr++;
	}
	return ( !strncmp( value, "TRUE", strlen( "TRUE" )) ? TRUE : FALSE );
}
// LECTURE BOOLEAN
// 
gboolean config_read_value_bool( gchar *p_Search, gboolean *p_ValueBool )
{
	gchar	*Ptr = NULL;
	gint	Cpt;
	
	for( Cpt = 0; Conf.Larrbuf[ Cpt ]; Cpt++ ) {
		
		// Si commentaire ou ligne vide alors ligne suivante
		if( *Conf.Larrbuf[ Cpt ] == '#' || *Conf.Larrbuf[ Cpt ] == '\0') continue;
		
		if( TRUE == config_new_strcmp( Conf.Larrbuf[ Cpt ], p_Search )) {
			if( NULL != ( Ptr = config_get_target( Conf.Larrbuf[ Cpt ] ))) {
				*p_ValueBool = config_get_bool( Ptr );
				g_free( Ptr );
				Ptr = NULL;
				if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
					config_print_data( p_Search );
					g_print( "%s\n", *p_ValueBool ? "TRUE" : "FALSE" );
				}
				*Conf.Larrbuf[ Cpt ] = '\0';
				return( TRUE );	
			}
		}
	}
	return( FALSE );	
}
// LECTURE NUMERIQUE INT
// 
gboolean config_read_value_atoi( gchar *p_Search, gint *p_ValueAtoi, gint p_Min, gint p_Max )
{
	gchar	*Ptr = NULL;
	gint	Cpt;
	
	for( Cpt = 0; Conf.Larrbuf[ Cpt ]; Cpt++ ) {
		
		// Si commentaire ou ligne vide alors ligne suivante
		if( *Conf.Larrbuf[ Cpt ] == '#' || *Conf.Larrbuf[ Cpt ] == '\0') continue;
		
		if( TRUE == config_new_strcmp( Conf.Larrbuf[ Cpt ], p_Search )) {
			if( NULL != ( Ptr = config_get_target( Conf.Larrbuf[ Cpt ] ))) {
				*p_ValueAtoi = atoi( Ptr );
				g_free( Ptr );
				Ptr = NULL;
				if( p_Min > -1 && *p_ValueAtoi < p_Min ) *p_ValueAtoi = p_Min;
				if( p_Max > -1 && *p_ValueAtoi > p_Max ) *p_ValueAtoi = p_Max;
				if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
					config_print_data( p_Search );
					g_print( "%d\n", *p_ValueAtoi ? *p_ValueAtoi : 0 );
				}
				*Conf.Larrbuf[ Cpt ] = '\0';
				return( TRUE );
			}
		}
	}
	return( FALSE );	
}
// LECTURE NUMERIQUE INT
// 
gboolean config_read_value_atol( gchar *p_Search, gulong *p_ValueAtoi, gint p_Min, gint p_Max )
{
	gchar	*Ptr = NULL;
	gint	Cpt;
	
	for( Cpt = 0; Conf.Larrbuf[ Cpt ]; Cpt++ ) {
		
		// Si commentaire ou ligne vide alors ligne suivante
		if( *Conf.Larrbuf[ Cpt ] == '#' || *Conf.Larrbuf[ Cpt ] == '\0') continue;
		
		if( TRUE == config_new_strcmp( Conf.Larrbuf[ Cpt ], p_Search )) {
			if( NULL != ( Ptr = config_get_target( Conf.Larrbuf[ Cpt ] ))) {
				*p_ValueAtoi = atol( Ptr );
				g_free( Ptr );
				Ptr = NULL;
				if( p_Min > -1 && *p_ValueAtoi < p_Min ) *p_ValueAtoi = p_Min;
				if( p_Max > -1 && *p_ValueAtoi > p_Max ) *p_ValueAtoi = p_Max;
				if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
					config_print_data( p_Search );
					g_print( "%lu / %lu  ;-)\n", *p_ValueAtoi ? *p_ValueAtoi : 0, G_MAXULONG );
				}
				*Conf.Larrbuf[ Cpt ] = '\0';
				return( TRUE );
			}
		}
	}
	return( FALSE );	
}
// LECTURE PATHNAME
// 
gboolean config_read_value_path( gchar *p_Search, gchar **p_ValuePath, gchar *p_default )
{
	gchar	*Ptr = NULL;
	gint	Cpt;
	
	for( Cpt = 0; Conf.Larrbuf[ Cpt ]; Cpt++ ) {
		
		// Si commentaire ou ligne vide alors ligne suivante
		if( *Conf.Larrbuf[ Cpt ] == '#' || *Conf.Larrbuf[ Cpt ] == '\0') continue;
		
		if( TRUE == config_new_strcmp( Conf.Larrbuf[ Cpt ], p_Search )) {
			if( NULL != ( Ptr = config_get_target( Conf.Larrbuf[ Cpt ] ))) {
				if( TRUE == g_file_test( Ptr, G_FILE_TEST_IS_DIR )) {
					*p_ValuePath = Ptr;
				} else {
					if( NULL == p_default )	*p_ValuePath = g_strdup( g_getenv( "HOME" ));
					else			*p_ValuePath = g_strdup( p_default );
					g_free( Ptr );
					Ptr = NULL;
				}
			}
			else {
				if( NULL == p_default )	*p_ValuePath = g_strdup( g_getenv( "HOME" ));
				else			*p_ValuePath = g_strdup( p_default );
			}
			if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
				config_print_data( p_Search );
				g_print( "%s\n", *p_ValuePath ? *p_ValuePath : "" );
			}
			*Conf.Larrbuf[ Cpt ] = '\0';
			return( TRUE );	
		}
	}
	return( FALSE );	
}
// 
// 
gboolean config_read_value_str( gchar *p_Search, gchar **p_ValueStr )
{
	gchar	*Ptr = NULL;
	gint	Cpt;
	
	for( Cpt = 0; Conf.Larrbuf[ Cpt ]; Cpt++ ) {
		
		// Si commentaire ou ligne vide alors ligne suivante
		if( *Conf.Larrbuf[ Cpt ] == '#' || *Conf.Larrbuf[ Cpt ] == '\0') continue;
		
		if( TRUE == config_new_strcmp( Conf.Larrbuf[ Cpt ], p_Search )) {
			if( NULL != ( Ptr = config_get_target( Conf.Larrbuf[ Cpt ]))) {
				*p_ValueStr = g_strdup( Ptr );
				g_free( Ptr );
				Ptr = NULL;
				if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
					config_print_data( p_Search );
					g_print( "%s\n", *p_ValueStr ? *p_ValueStr : "" );
				}
				*Conf.Larrbuf[ Cpt ] = '\0';
				return( TRUE );	
			}
		}
	}
	return( FALSE );	
}
// 
// CALL:
// 	config_read_value_tab( "HOURS_MONDAY", Config.HoursMonday, 4 );
// 
gboolean config_read_value_tab( gchar *p_Search, gint p_ValueTab[], gint p_max_tab )
{
	gchar	*Ptr = NULL;
	gint	Cpt;
	
	// IF NOT INIT IN TAB:
	// 	*(p_ValueTab +0) = 0;
	// 	*(p_ValueTab +1) = 0;
	// 	*(p_ValueTab +2) = 0;
	// 	*(p_ValueTab +3) = 0;
	
	for( Cpt = 0; Conf.Larrbuf[ Cpt ]; Cpt++ ) {
		
		// Si commentaire ou ligne vide alors ligne suivante
		if( *Conf.Larrbuf[ Cpt ] == '#' || *Conf.Larrbuf[ Cpt ] == '\0') continue;
		
		if( TRUE == config_new_strcmp( Conf.Larrbuf[ Cpt ], p_Search )) {

			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				config_print_data( p_Search );

			if( NULL != (Ptr = strchr( Conf.Larrbuf[ Cpt ], '=' ))) {
				
				gchar	**LarrbufLine = NULL;
				gint	Indice;
				
				// POINTEUR SUR LA PREMIERE OCCURANCE
				Ptr ++;
				while( *Ptr == ' ' || *Ptr == '\t' ) Ptr ++;
				
				// *Ptr IS VALID ?
				if( *Ptr != '\0' ) {
					
					// HOURS_MONDAY                  = 435 690 720 975
					// HOURS_SATURDAY                = 0 0 0 0
					LarrbufLine = g_strsplit( Ptr, " ", 0 );
				
					// INDICE: 0 .. p_max_tab
					for( Indice = 0; LarrbufLine[ Indice ] && Indice < p_max_tab; Indice ++ ) {
					
						*(p_ValueTab +Indice) = (gint)atoi( LarrbufLine[ Indice ] );
					
						if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
							g_print ("%04d (%02d:%02d)  ",
								*(p_ValueTab +Indice),
								*(p_ValueTab +Indice) / 60,
								*(p_ValueTab +Indice) % 60
								);
						}
					}
				
					g_strfreev( LarrbufLine );
					LarrbufLine = NULL;
				}
			}
			
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print( "\n" );
			
			*Conf.Larrbuf[ Cpt ] = '\0';
			return( TRUE );	
		}
	}
	return( FALSE );	
}
// 	
// 	
void config_close_conf_file( void )
{
	if( NULL != Conf.Larrbuf ) {
		g_strfreev( Conf.Larrbuf );
		Conf.Larrbuf = NULL;
	}
}
// 	
// 	
gboolean config_open_conf_file( gchar *p_PathNameConfig )
{
	size_t 		SizeFile;
	gchar		*Buffer = NULL;
	FILE		*fp = NULL;
	
	config_close_conf_file();
	if(( SizeFile = libutils_get_size_file( p_PathNameConfig )) > 0 ) {
		if( NULL != ( fp = fopen( p_PathNameConfig, "r" ))) {
			Buffer = (gchar *)g_malloc0( sizeof(gchar) * ( SizeFile +10 ));
			SizeFile = fread( Buffer, SizeFile, 1, fp );
			fclose( fp );
			Conf.Larrbuf = g_strsplit( Buffer, "\n", 0 );
			g_free( Buffer );
			Buffer = NULL;
			return( TRUE );
		}
	}
	return( FALSE );
}
// 
// 
void config_verif_path (gchar **p_ValuePath, gchar *p_default)
{
	if( NULL == *p_ValuePath ) {
		if( NULL == p_default )	*p_ValuePath = g_strdup( g_getenv( "HOME" ));
		else			*p_ValuePath = g_strdup( p_default );
	}
}
// 	
// 	
// LECTURE DU FICHIER DE CONFIGURATION
// 
void config_read( void )
{
	// gchar	*PathNameConfig = config_get_pathname ();	// HOME /.config/xcfa/xcfa.conf
	gchar	*PathNameConfig = xdg_search_config_xdg();		// HOME /.config/xcfa/xcfa.conf
	
	// Pour les restitutions par defaut
	g_memmove (&ConfigSaveToRest, &Config, sizeof (VAR_CONFIG));
	
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("\n------------------------------------------------------------------------\n" );
		g_print ("%s\n", PathNameConfig);
		g_print ("------------------------------------------------------------------------\n" );
	}
	
	// IF NOT EXIST CONFIG-FILE
	if( FALSE == libutils_test_file_exist( PathNameConfig )) {
		// GET PATHNAME
		gchar	*StrDir = g_strdup( PathNameConfig );
		// GET PATH
		gchar	*Ptr = strrchr( StrDir, '/' );
		
		// NOW StrDir IS PATH
		Ptr ++;
		*Ptr = '\0';
		// CREATION DU DOSSIER
		g_mkdir_with_parents (StrDir, 0700);
		// FREE MEM
		g_free( StrDir );	StrDir = Ptr = NULL;
		// CREATE CONFIG-FILE
		config_save ();
	}
	
	if( TRUE == config_open_conf_file( PathNameConfig )) {
		
		// READ ATOL
		// 
		config_read_value_atol( "USES_OF_XCFA",				&Config.UsesOfXcfa,       		 0,	-1 );
		if( Config.UsesOfXcfa < G_MAXULONG )
			Config.UsesOfXcfa ++;
		else	Config.UsesOfXcfa = 0;
		
		// READ ATOI
		// 
		config_read_value_atoi( "WIN_POS_X",				&Config.WinPos_X,       		 0,	-1 );
		config_read_value_atoi( "WIN_POS_Y",				&Config.WinPos_Y,       		 0,	-1 );
		config_read_value_atoi( "WIN_WIDTH",				&Config.WinWidth,        		-1,	-1 );
		config_read_value_atoi( "WIN_HEIGHT",				&Config.WinHeight,       		-1,	-1 );
		config_read_value_atoi( "NICE",					&Config.Nice,            		 0,	19 );
		config_read_value_atoi( "NOTEBOOK_GENERAL",			&Config.NotebookGeneral, 		-1,	-1 );
		config_read_value_atoi( "NOTEBOOK_EXPANDER_CD",			&Config.NotebookExpanderCd,		-1,	-1 );
		config_read_value_atoi( "NOTEBOOK_FILE",			&Config.NotebookFile,			-1,	-1 );
		config_read_value_atoi( "NOTEBOOK_OPTIONS",			&Config.NotebookOptions,  		-1,	-1 );
		config_read_value_atoi( "NOTEBOOK_APP_EXTERNS",			&Config.NotebookAppExterns,  		-1,	-1 );
		config_read_value_atoi( "EXTRACT_CD_WITH",			&Config.ExtractCdWith,    		EXTRACT_WITH_CDPARANOIA,  EXTRACT_WITH_CDDA2WAV );
		config_read_value_atoi( "OPTIONS_BITRATE_LAME",			&Config.BitrateLameIndice,		0,	3 );
		config_read_value_atoi( "OPTIONS_VALUE_ABR_LAME",   		&Config.TabBitrateLame[0],		0,	13 );
		config_read_value_atoi( "OPTIONS_VALUE_CBR_LAME",   		&Config.TabBitrateLame[1],		0,	14 );
		config_read_value_atoi( "OPTIONS_VALUE_VBR_LAME",   		&Config.TabBitrateLame[2],		0,	14 );
		config_read_value_atoi( "OPTIONS_VALUE_VBR_NEW_LAME",		&Config.TabBitrateLame[3],		0,	15 );
		config_read_value_atoi( "OPTIONS_MODE_ABR_LAME",   		&Config.TabModeLame[0],			0,	13 );
		config_read_value_atoi( "OPTIONS_MODE_CBR_LAME",   		&Config.TabModeLame[1],			0,	14 );
		config_read_value_atoi( "OPTIONS_MODE_VBR_LAME",   		&Config.TabModeLame[2],			0,	14 );
		config_read_value_atoi( "OPTIONS_MODE_VBR_NEW_LAME",		&Config.TabModeLame[3],			0,	15 );
		config_read_value_atoi( "OPTIONS_BITRATE_OGGENC",		&Config.BitrateOggenc,			0,	22 );
		config_read_value_atoi( "OPTIONS_MANAGED_OGGENC",		&Config.ManagedOggenc,			0,	1 );
		config_read_value_atoi( "OPTIONS_DOWNMIX_OGGENC",		&Config.DownmixOggenc,			0,	1 );
		config_read_value_atoi( "OPTIONS_LEVEL_FLAC",			&Config.CompressionLevelFlac,		0,	8 );
		config_read_value_atoi( "OPTIONS_LEVEL_APE_MAC",		&Config.CompressionLevelApeMac,		0,	4 );
		config_read_value_atoi( "OPTIONS_COMPRESSION_WAVPACK",		&Config.CompressionWavpack,		0,	3 );
		config_read_value_atoi( "OPTIONS_SOUND_WAVPACK",		&Config.SoundWavpack,			0,	2 );
		config_read_value_atoi( "OPTIONS_MODE_HYBRIDE_WAVPACK",		&Config.ModeHybrideWavpack,		0,	4 );
		config_read_value_atoi( "OPTIONS_CORRECTION_WAVPACK",		&Config.CorrectionFileWavpack,		0,	2 );
		config_read_value_atoi( "OPTIONS_COMPRESSION_MAXI_WAVPACK",	&Config.CompressionMaximumWavpack,	0,	2 );
		config_read_value_atoi( "OPTIONS_SIGNATURE_WAVPACK",		&Config.SignatureMd5Wavpack,		0,	2 );
		config_read_value_atoi( "OPTIONS_EXTRA_ENCODING_WAVPACK",	&Config.ExtraEncodingWavpack,		0,	6 );
		config_read_value_atoi( "OPTIONS_QUALITY_MPPENC",		&Config.QualityMppenc,			0,	6 );
		config_read_value_atoi( "OPTIONS_CONTENEUR_FAAC",		&Config.ConteneurFacc,			0,	2 );
		config_read_value_atoi( "OPTIONS_ABRVBR_FAAC",			&Config.AbrVbrFacc,			0,	2 );
		config_read_value_atoi( "OPTIONS_ABR_INDICE_FAAC",		&Config.AbrFaccIndice,			0,	5 );
		config_read_value_atoi( "OPTIONS_VBR_INDICE_FAAC",		&Config.VbrFaccIndice,			0,	13 );
		config_read_value_atoi( "OPTIONS_CHOICE_MONO_AACPLUSENC",	&Config.ChoiceMonoAacplusenc,		0,	7 );
		config_read_value_atoi( "OPTIONS_CHOICE_STEREO_AACPLUSENC",	&Config.ChoiceStereoAacplusenc,		0,	4 );
		config_read_value_atoi( "IND_COMBO_DEST_FILE_ALL",		&Config.TabIndiceComboDestFile[0],	0,	2 );
		config_read_value_atoi( "IND_COMBO_DEST_FILE_WAV",		&Config.TabIndiceComboDestFile[1],	0,	2 );
		config_read_value_atoi( "IND_COMBO_DEST_FILE_MP3OGG",		&Config.TabIndiceComboDestFile[2],	0,	2 );
		config_read_value_atoi( "IND_COMBO_DEST_FILE_TAGS",		&Config.TabIndiceComboDestFile[3],	0,	2 );
		config_read_value_atoi( "SERVER_FREE_CDDB",			&Config.ServeurCddb,			0,	2 );
		config_read_value_atoi( "NUM_SELECT_COMBOBOX_DVD",		&Config.NumSelectComboBoxDvd,		0,	10 );
		config_read_value_atoi( "NUM_SELECT_COMBOBOX_CD",		&Config.NumSelectComboBoxCd,		0,	10 );
		
		// READ BOOL
		// 
		config_read_value_bool( "TAGS_ARTISTE",				&Config.BoolArtistTag );
		config_read_value_bool( "TAGS_TITLE",				&Config.BoolTitleTag );
		config_read_value_bool( "TAGS_ALBUM",				&Config.BoolAlbumTag );
		config_read_value_bool( "TAGS_NUMERATE",			&Config.BoolNumerateTag );
		config_read_value_bool( "TAGS_GENRE",				&Config.BoolGenreTag );
		config_read_value_bool( "TAGS_YEAR",				&Config.BoolYearTag );
		config_read_value_bool( "TAGS_COMMENT",				&Config.BoolCommentTag );
		config_read_value_bool( "BOOL_ETAT_EXPANDER_CD",		&Config.BoolEtatExpanderCd );
		config_read_value_bool( "BOOL_PLAY_MUSIC_END_CONVERT",		&Config.BoolCheckbuttonEndOfConvert );
		config_read_value_bool( "BOOL_LOG_CDPARANOIA_MODE_EXPERT",	&Config.BoolLogCdparanoiaModeExpert);
		
		// READ PATH
		// 
		config_read_value_path( "PATH_DEST_FILE_ALL",			&Config.PathDestinationFileAll,		NULL );
		config_read_value_path( "PATH_DEST_FILE_WAV",			&Config.PathDestinationFileWav,		NULL );
		config_read_value_path( "PATH_DEST_FILE_MP3OGG",		&Config.PathDestinationFileMp3Ogg,	NULL );
		config_read_value_path( "PATH_DEST_FILE_TAGS",			&Config.PathDestinationFileTags,	NULL );
		config_read_value_path( "PATH_DEST_DVD",			&Config.PathDestinationDVD,		NULL );
		config_read_value_path( "PATH_CHOICE_FILE_DVD",			&Config.PathChoiceFileDVD,		NULL );
		config_read_value_path( "PATH_DEST_CD",				&Config.PathDestinationCD,		NULL );
		config_read_value_path( "PATH_CHOICE_FILE_CD",			&Config.PathChoiceFileCD,		NULL );
		config_read_value_path( "PATHNAME_TMP",				&Config.PathnameTMP,			"/tmp" );
		config_read_value_path( "PATHNAME_STOCKE_IMAGES_POCHETTE",	&Config.PathStockeImagesPochette,	NULL );
		config_read_value_path( "PATHNAME_DEST_FILE_POSTSCRIPT",	&Config.PathDestFilePostScript,		NULL );
		config_read_value_path( "PATHNAME_LOAD_IMAGES",			&Config.PathLoadImages,			NULL );
		config_read_value_path( "PATH_LOAD_FILE_ALL",			&Config.PathLoadFileAll,		NULL );
		config_read_value_path( "PATH_LOAD_FILE_WAV",			&Config.PathLoadFileWav,		NULL );
		config_read_value_path( "PATH_LOAD_FILE_MP3OGG",		&Config.PathLoadFileMp3Ogg,		NULL );
		config_read_value_path( "PATH_LOAD_FILE_TAGS",			&Config.PathLoadFileTags,		NULL );
		config_read_value_path( "PATH_DEST_SPLIT",			&Config.PathDestinationSplit,		NULL );
		config_read_value_path( "PATH_LOAD_SPLIT",			&Config.PathLoadSplit,			NULL );
		config_read_value_path( "PATH_LOAD_IMG",			&Config.PathLoadImg,			NULL );
		config_read_value_path( "PATH_SAVE_IMG",			&Config.PathSaveImg,			NULL );
		config_read_value_path( "PATH_MUSIC_END_CONVERT",		&Config.PathMusicFileEndOfConvert,	NULL );
		
		// READ STRING
		// 
		config_read_value_str( "STRING_EXPANDER_LAME",			&Config.StringExpanderLame );
		config_read_value_str( "STRING_EXPANDER_OGGENC",		&Config.StringExpanderOggenc );
		config_read_value_str( "STRING_EXPANDER_FLAC",			&Config.StringExpanderFlac );
		config_read_value_str( "STRING_EXPANDER_FAAC",			&Config.StringExpanderFaac );
		config_read_value_str( "STRING_EXPANDER_MPPENC",		&Config.StringExpanderMppenc );
		config_read_value_str( "STRING_EXPANDER_MAC",			&Config.StringExpanderMac );
		config_read_value_str( "STRING_EXPANDER_WAVPACK",		&Config.StringExpanderWavpack );
		config_read_value_str( "STRING_NAME_FILE_M3U_XSPF",		&Config.StringNameFile_m3u_xspf );
		config_read_value_str( "TEMPLATE_TITLE_CDAUDIO",		&Config.Templates_title_cdaudio );
		config_read_value_str( "TEMPLATE_REP_CDAUDIO",			&Config.Templates_rep_cdaudio );
		config_read_value_str( "STRING_NAME_NAVIGATEUR",		&Config.StringNameNavigateur );
		config_read_value_str( "STRING_PARAM_NAME_NAVIGATEUR",		&Config.StringParamNameNavigateur );
		config_read_value_str( "STRING_NAME_LECTEUR_POSTSCRIPT",	&Config.StringNameLecteurPostScript );
		config_read_value_str( "STRING_NAME_LECTEUR_AUDIO",		&Config.StringNameLecteurAudio );
		config_read_value_str( "STRING_PARAM_NAME_LECTEUR_AUDIO",	&Config.StringParamNameLecteurAudio );
		config_read_value_str( "STRING_BOOL_FIELD_IS_VISIBLE",		&Config.StringBoolFieldsIsVisible );
		config_read_value_str( "STRING_POS_FIELD_IS_NAME",		&Config.StringPosFieldsName );
		config_read_value_str( "STRING_ENTRY_CDDP_SERVER",		&Config.entry_cddp_server );
		config_read_value_str( "STRING_ENTRY_PROXY_SERVER",		&Config.entry_proxy_server );
		config_read_value_str( "STRING_ENTRY_PROXY_PORT",		&Config.entry_proxy_port );
		config_read_value_str( "NAME_IMG",				&Config.NameImg );
		config_read_value_str( "FILE_NAME_MUSIC_END_CONVERT",		&Config.FileMusicFileEndOfConvert );
		config_read_value_str( "STRING_COMMENT_CD",			&Config.StringCommentCD );
		
		config_close_conf_file();
		
		config_verif_path( &Config.PathDestinationFileAll,	NULL );
		config_verif_path( &Config.PathDestinationFileWav,	NULL );
		config_verif_path( &Config.PathDestinationFileMp3Ogg,	NULL );
		config_verif_path( &Config.PathDestinationFileTags,	NULL );
		config_verif_path( &Config.PathDestinationDVD,		NULL );
		config_verif_path( &Config.PathChoiceFileDVD,		NULL );
		config_verif_path( &Config.PathDestinationCD,		NULL );
		config_verif_path( &Config.PathChoiceFileCD,		NULL );
		config_verif_path( &Config.PathnameTMP,			"/tmp" );
		config_verif_path( &Config.PathStockeImagesPochette,	NULL );
		config_verif_path( &Config.PathDestFilePostScript,	NULL );
		config_verif_path( &Config.PathLoadImages,		NULL );
		config_verif_path( &Config.PathLoadFileAll,		NULL );
		config_verif_path( &Config.PathLoadFileWav,		NULL );
		config_verif_path( &Config.PathLoadFileMp3Ogg,		NULL );
		config_verif_path( &Config.PathLoadFileTags,		NULL );
		config_verif_path( &Config.PathDestinationSplit,	NULL );
		config_verif_path( &Config.PathLoadSplit,		NULL );
		config_verif_path( &Config.PathLoadImg,			NULL );
		config_verif_path( &Config.PathSaveImg,			NULL );
		config_verif_path( &Config.PathMusicFileEndOfConvert,	NULL );

	}
	if( NULL != PathNameConfig ) {
		g_free( PathNameConfig );
		PathNameConfig = NULL;
	}
}
//  SAUVEGARDE DU FICHIER DE CONFIGURATION
// 
void config_save( void )
{
	// gchar	*PathNameConfig = config_get_pathname();	// HOME /.config/xcfa/xcfa.conf
	gchar	*PathNameConfig = xdg_search_config_xdg();		// HOME /.config/xcfa/xcfa.conf
	FILE	*fp;
	gchar	*Ptr = NULL;
	
	if( NULL != ( fp = fopen( PathNameConfig, "w" ))) {
		fprintf( fp,   "############################################################################\n" );
		fprintf( fp,   "## Fichier de configuration pour : %s\n", PACKAGE_STRING);
		fprintf( fp,   "##--------------------------------------------------------------------------\n" );
		fprintf( fp,   "## File of configuration to : %s\n", PACKAGE_STRING);
		fprintf( fp,   "############################################################################\n" );
		fprintf( fp, "\n" );
		fprintf( fp, "USES_OF_XCFA                        = %lu\n",Config.UsesOfXcfa );
		fprintf( fp, "WIN_POS_X                           = %d\n", Config.WinPos_X );
		fprintf( fp, "WIN_POS_Y                           = %d\n", Config.WinPos_Y );
		fprintf( fp, "WIN_WIDTH                           = %d\n", Config.WinWidth );
		fprintf( fp, "WIN_HEIGHT                          = %d\n", Config.WinHeight );
		fprintf( fp, "NICE                                = %d\n", Config.Nice );
		fprintf( fp, "IND_COMBO_DEST_FILE_ALL             = %d\n", Config.TabIndiceComboDestFile[0] );
		fprintf( fp, "IND_COMBO_DEST_FILE_WAV             = %d\n", Config.TabIndiceComboDestFile[1] );
		fprintf( fp, "IND_COMBO_DEST_FILE_MP3OGG          = %d\n", Config.TabIndiceComboDestFile[2] );
		fprintf( fp, "IND_COMBO_DEST_FILE_TAGS            = %d\n", Config.TabIndiceComboDestFile[3] );
		fprintf( fp, "PATH_LOAD_FILE_ALL                  = %s\n", Config.PathLoadFileAll != NULL ? Config.PathLoadFileAll : "" );
		fprintf( fp, "PATH_LOAD_FILE_WAV                  = %s\n", Config.PathLoadFileWav != NULL ? Config.PathLoadFileWav : "" );
		fprintf( fp, "PATH_LOAD_FILE_MP3OGG               = %s\n", Config.PathLoadFileMp3Ogg != NULL ? Config.PathLoadFileMp3Ogg : "" );
		fprintf( fp, "PATH_LOAD_FILE_TAGS                 = %s\n", Config.PathLoadFileTags != NULL ? Config.PathLoadFileTags : "" );
		fprintf( fp, "PATH_DEST_FILE_ALL                  = %s\n", Config.PathDestinationFileAll != NULL ? Config.PathDestinationFileAll : "" );
		fprintf( fp, "PATH_DEST_FILE_WAV                  = %s\n", Config.PathDestinationFileWav != NULL ? Config.PathDestinationFileWav : "" );
		fprintf( fp, "PATH_DEST_FILE_MP3OGG               = %s\n", Config.PathDestinationFileMp3Ogg != NULL ? Config.PathDestinationFileMp3Ogg : "" );
		fprintf( fp, "PATH_DEST_FILE_TAGS                 = %s\n", Config.PathDestinationFileTags != NULL ? Config.PathDestinationFileTags : "" );
		fprintf( fp, "PATH_DEST_DVD                       = %s\n", Config.PathDestinationDVD != NULL ? Config.PathDestinationDVD : "" );
		fprintf( fp, "PATH_CHOICE_FILE_DVD                = %s\n", Config.PathChoiceFileDVD != NULL ? Config.PathChoiceFileDVD : "" );
		fprintf( fp, "PATH_DEST_CD                        = %s\n", Config.PathDestinationCD != NULL ? Config.PathDestinationCD : "" );
		fprintf( fp, "PATH_CHOICE_FILE_CD                 = %s\n", Config.PathChoiceFileCD != NULL ? Config.PathChoiceFileCD : "" );
		fprintf( fp, "PATH_DEST_SPLIT                     = %s\n", Config.PathDestinationSplit != NULL ? Config.PathDestinationSplit : "" );
		fprintf( fp, "PATH_LOAD_SPLIT                     = %s\n", Config.PathLoadSplit != NULL ? Config.PathLoadSplit : "" );
		fprintf( fp, "PATHNAME_TMP                        = %s\n", Config.PathnameTMP != NULL ? Config.PathnameTMP : "" );
		fprintf( fp, "PATHNAME_STOCKE_IMAGES_POCHETTE     = %s\n", Config.PathStockeImagesPochette != NULL ? Config.PathStockeImagesPochette : "" );
		fprintf( fp, "PATHNAME_DEST_FILE_POSTSCRIPT       = %s\n", Config.PathDestFilePostScript != NULL ? Config.PathDestFilePostScript : "" );
		fprintf( fp, "PATHNAME_LOAD_IMAGES                = %s\n", Config.PathLoadImages != NULL ? Config.PathLoadImages : "" );
		fprintf( fp, "NOTEBOOK_GENERAL                    = %d\n", Config.NotebookGeneral );
		fprintf( fp, "NOTEBOOK_EXPANDER_CD                = %d\n", Config.NotebookExpanderCd );
		fprintf( fp, "NOTEBOOK_FILE                       = %d\n", Config.NotebookFile );
		fprintf( fp, "NOTEBOOK_OPTIONS                    = %d\n", Config.NotebookOptions );
		fprintf( fp, "NOTEBOOK_APP_EXTERNS                = %d\n", Config.NotebookAppExterns );
		fprintf( fp, "EXTRACT_CD_WITH                     = %d\n", Config.ExtractCdWith );
		fprintf( fp, "OPTIONS_BITRATE_LAME                = %d\n", Config.BitrateLameIndice );
		fprintf( fp, "OPTIONS_VALUE_ABR_LAME              = %d\n", Config.TabBitrateLame[0] );
		fprintf( fp, "OPTIONS_VALUE_CBR_LAME              = %d\n", Config.TabBitrateLame[1] );
		fprintf( fp, "OPTIONS_VALUE_VBR_LAME              = %d\n", Config.TabBitrateLame[2] );
		fprintf( fp, "OPTIONS_VALUE_VBR_NEW_LAME          = %d\n", Config.TabBitrateLame[3] );
		fprintf( fp, "OPTIONS_MODE_ABR_LAME               = %d\n", Config.TabModeLame[0] );
		fprintf( fp, "OPTIONS_MODE_CBR_LAME               = %d\n", Config.TabModeLame[1] );
		fprintf( fp, "OPTIONS_MODE_VBR_LAME               = %d\n", Config.TabModeLame[2] );
		fprintf( fp, "OPTIONS_MODE_VBR_NEW_LAME           = %d\n", Config.TabModeLame[3] );
		fprintf( fp, "OPTIONS_BITRATE_OGGENC              = %d\n", Config.BitrateOggenc );
		fprintf( fp, "OPTIONS_MANAGED_OGGENC              = %d\n", Config.ManagedOggenc );
		fprintf( fp, "OPTIONS_DOWNMIX_OGGENC              = %d\n", Config.DownmixOggenc );
		fprintf( fp, "OPTIONS_LEVEL_FLAC                  = %d\n", Config.CompressionLevelFlac );
		fprintf( fp, "OPTIONS_LEVEL_APE_MAC               = %d\n", Config.CompressionLevelApeMac );
		fprintf( fp, "OPTIONS_COMPRESSION_WAVPACK         = %d\n", Config.CompressionWavpack );
		fprintf( fp, "OPTIONS_SOUND_WAVPACK               = %d\n", Config.SoundWavpack );
		fprintf( fp, "OPTIONS_MODE_HYBRIDE_WAVPACK        = %d\n", Config.ModeHybrideWavpack );
		fprintf( fp, "OPTIONS_CORRECTION_WAVPACK          = %d\n", Config.CorrectionFileWavpack );
		fprintf( fp, "OPTIONS_COMPRESSION_MAXI_WAVPACK    = %d\n", Config.CompressionMaximumWavpack );
		fprintf( fp, "OPTIONS_SIGNATURE_WAVPACK           = %d\n", Config.SignatureMd5Wavpack );
		fprintf( fp, "OPTIONS_EXTRA_ENCODING_WAVPACK      = %d\n", Config.ExtraEncodingWavpack );
		fprintf( fp, "OPTIONS_QUALITY_MPPENC              = %d\n", Config.QualityMppenc );
		fprintf( fp, "OPTIONS_CONTENEUR_FAAC              = %d\n", Config.ConteneurFacc );
		fprintf( fp, "OPTIONS_ABRVBR_FAAC                 = %d\n", Config.AbrVbrFacc );
		fprintf( fp, "OPTIONS_ABR_INDICE_FAAC             = %d\n", Config.AbrFaccIndice );
		fprintf( fp, "OPTIONS_VBR_INDICE_FAAC             = %d\n", Config.VbrFaccIndice );
		fprintf( fp, "OPTIONS_CHOICE_MONO_AACPLUSENC      = %d\n", Config.ChoiceMonoAacplusenc );
		fprintf( fp, "OPTIONS_CHOICE_STEREO_AACPLUSENC    = %d\n", Config.ChoiceStereoAacplusenc );
		fprintf( fp, "TAGS_ARTISTE                        = %s\n", Config.BoolArtistTag == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "TAGS_TITLE                          = %s\n", Config.BoolTitleTag == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "TAGS_ALBUM                          = %s\n", Config.BoolAlbumTag == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "TAGS_NUMERATE                       = %s\n", Config.BoolNumerateTag == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "TAGS_GENRE                          = %s\n", Config.BoolGenreTag == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "TAGS_YEAR                           = %s\n", Config.BoolYearTag == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "TAGS_COMMENT                        = %s\n", Config.BoolCommentTag == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "BOOL_ETAT_EXPANDER_CD               = %s\n", Config.BoolEtatExpanderCd == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "STRING_EXPANDER_LAME                = %s\n", Config.StringExpanderLame ? Config.StringExpanderLame : "" );
		fprintf( fp, "STRING_EXPANDER_OGGENC              = %s\n", Config.StringExpanderOggenc ? Config.StringExpanderOggenc : "" );
		fprintf( fp, "STRING_EXPANDER_FLAC                = %s\n", Config.StringExpanderFlac ? Config.StringExpanderFlac : "" );
		fprintf( fp, "STRING_EXPANDER_FAAC                = %s\n", Config.StringExpanderFaac ? Config.StringExpanderFaac : "" );
		fprintf( fp, "STRING_EXPANDER_MPPENC              = %s\n", Config.StringExpanderMppenc ? Config.StringExpanderMppenc : "" );
		fprintf( fp, "STRING_EXPANDER_MAC                 = %s\n", Config.StringExpanderMac ? Config.StringExpanderMac : "" );
		fprintf( fp, "STRING_EXPANDER_WAVPACK             = %s\n", Config.StringExpanderWavpack ? Config.StringExpanderWavpack : "" );
		fprintf( fp, "SERVER_FREE_CDDB                    = %d\n", Config.ServeurCddb );
		fprintf( fp, "STRING_ENTRY_CDDP_SERVER            = %s\n", Config.entry_cddp_server != NULL ? Config.entry_cddp_server : "freedb.org" );
		fprintf( fp, "STRING_ENTRY_PROXY_SERVER           = %s\n", Config.entry_proxy_server != NULL ? Config.entry_proxy_server : "10.0.0.1" );
		fprintf( fp, "STRING_ENTRY_PROXY_PORT             = %s\n", Config.entry_proxy_port != NULL ? Config.entry_proxy_port : "8080" );
		fprintf( fp, "STRING_NAME_FILE_M3U_XSPF           = %s\n", Config.StringNameFile_m3u_xspf ? Config.StringNameFile_m3u_xspf : "" );
		fprintf( fp, "TEMPLATE_TITLE_CDAUDIO              = " );
		Ptr = Config.Templates_title_cdaudio;
		if (Ptr == NULL) {
			fprintf( fp, "%%c %%d" );
		}
		else {
			while (Ptr && *Ptr) {
				if (*Ptr == '%')	fprintf( fp, "%%" );
				else			fprintf( fp, "%c", *Ptr );
				Ptr++;
			}
		}
		fprintf( fp,   "\n" );
		fprintf( fp, "TEMPLATE_REP_CDAUDIO                = " );
		Ptr = Config.Templates_rep_cdaudio;
		if( NULL == Ptr ) {
			fprintf( fp, "%%b" );
		}
		else {
			while( Ptr && *Ptr ) {
				if( *Ptr == '%')	fprintf( fp, "%%" );
				else			fprintf( fp, "%c", *Ptr );
				Ptr++;
			}
		}
		fprintf( fp, "\n" );
		fprintf( fp, "STRING_NAME_NAVIGATEUR              = %s\n", Config.StringNameNavigateur ? Config.StringNameNavigateur : "" );
		fprintf( fp, "STRING_PARAM_NAME_NAVIGATEUR        = %s\n", Config.StringParamNameNavigateur ? Config.StringParamNameNavigateur : "" );
		fprintf( fp, "STRING_NAME_LECTEUR_POSTSCRIPT      = %s\n", Config.StringNameLecteurPostScript ? Config.StringNameLecteurPostScript : "" );
		fprintf( fp, "STRING_NAME_LECTEUR_AUDIO           = %s\n", Config.StringNameLecteurAudio ? Config.StringNameLecteurAudio : "" );
		fprintf( fp, "STRING_PARAM_NAME_LECTEUR_AUDIO     = %s\n", Config.StringParamNameLecteurAudio ? Config.StringParamNameLecteurAudio : "" );
		fprintf( fp, "STRING_BOOL_FIELD_IS_VISIBLE        = %s\n", Config.StringBoolFieldsIsVisible ? Config.StringBoolFieldsIsVisible : "" );
		fprintf( fp, "STRING_POS_FIELD_IS_NAME            = %s\n", Config.StringPosFieldsName ? Config.StringPosFieldsName : "" );
		fprintf( fp, "NUM_SELECT_COMBOBOX_DVD             = %d\n", Config.NumSelectComboBoxDvd );
		fprintf( fp, "NUM_SELECT_COMBOBOX_CD              = %d\n", Config.NumSelectComboBoxCd );
		fprintf( fp, "NAME_IMG                            = %s\n", Config.NameImg != NULL ? Config.NameImg : "" );
		fprintf( fp, "PATH_LOAD_IMG                       = %s\n", Config.PathLoadImg != NULL ? Config.PathLoadImg : "" );
		fprintf( fp, "PATH_SAVE_IMG                       = %s\n", Config.PathSaveImg != NULL ? Config.PathSaveImg : "" );
		fprintf( fp, "BOOL_PLAY_MUSIC_END_CONVERT         = %s\n", Config.BoolCheckbuttonEndOfConvert == TRUE ? "TRUE" : "FALSE" );
		fprintf( fp, "PATH_MUSIC_END_CONVERT              = %s\n", Config.PathMusicFileEndOfConvert != NULL ? Config.PathMusicFileEndOfConvert : "" );
		fprintf( fp, "FILE_NAME_MUSIC_END_CONVERT         = %s\n", Config.FileMusicFileEndOfConvert != NULL ? Config.FileMusicFileEndOfConvert : "" );
		fprintf( fp, "STRING_COMMENT_CD                   = %s\n", Config.StringCommentCD  != NULL ? Config.StringCommentCD : "" );
		fprintf( fp, "BOOL_LOG_CDPARANOIA_MODE_EXPERT     = %s\n", Config.BoolLogCdparanoiaModeExpert == TRUE ? "TRUE" : "FALSE" );
		
		// TODO: RESERVED
		// g_sprintf( String, "%08lx", ret );
		// long atol( const char *nptr );
		// La fonction atol() convertit le début de la chaîne pointée par nptr en entier de type long
		// The atol() function converts the initial portion of the string pointed to by nptr to long
		
		fprintf( fp, "\n" );
		fclose( fp );
	}
	if( NULL != PathNameConfig ) {
		g_free( PathNameConfig );
		PathNameConfig = NULL;
	}
}
// LIBERATION MEMOIRE
// 
void config_remove( void )
{	
	g_free( Config.PathLoadFileAll );		Config.PathLoadFileAll = NULL;
	g_free( Config.PathLoadFileWav );		Config.PathLoadFileWav = NULL;
	g_free( Config.PathLoadFileMp3Ogg );		Config.PathLoadFileMp3Ogg = NULL;
	g_free( Config.PathLoadFileTags );		Config.PathLoadFileTags = NULL;
	g_free( Config.PathDestinationFileAll );	Config.PathDestinationFileAll = NULL;
	g_free( Config.PathDestinationFileWav );	Config.PathDestinationFileWav = NULL;
	g_free( Config.PathDestinationFileMp3Ogg );	Config.PathDestinationFileMp3Ogg = NULL;
	g_free( Config.PathDestinationFileTags );	Config.PathDestinationFileTags = NULL;
	g_free( Config.PathDestinationDVD );		Config.PathDestinationDVD = NULL;
	g_free( Config.PathChoiceFileDVD );		Config.PathChoiceFileDVD = NULL;
	g_free( Config.PathDestinationCD );		Config.PathDestinationCD = NULL;
	g_free( Config.PathChoiceFileCD );		Config.PathChoiceFileCD = NULL;
	g_free( Config.PathDestinationSplit );		Config.PathDestinationSplit = NULL;
	g_free( Config.PathLoadSplit );			Config.PathLoadSplit = NULL;
	g_free( Config.PathnameTMP );			Config.PathnameTMP = NULL;
	g_free( Config.PathStockeImagesPochette );	Config.PathStockeImagesPochette = NULL;
	g_free( Config.PathDestFilePostScript );	Config.PathDestFilePostScript = NULL;
	g_free( Config.PathLoadImages );		Config.PathLoadImages = NULL;
	g_free( Config.StringExpanderLame );		Config.StringExpanderLame = NULL;
	g_free( Config.StringExpanderOggenc );		Config.StringExpanderOggenc = NULL;
	g_free( Config.StringExpanderFlac );		Config.StringExpanderFlac = NULL;
	g_free( Config.StringExpanderFaac );		Config.StringExpanderFaac = NULL;
	g_free( Config.StringExpanderMppenc );		Config.StringExpanderMppenc = NULL;
	g_free( Config.StringExpanderMac );		Config.StringExpanderMac = NULL;
	g_free( Config.StringExpanderWavpack );		Config.StringExpanderWavpack = NULL;
	g_free( Config.StringNameFile_m3u_xspf );	Config.StringNameFile_m3u_xspf = NULL;
	g_free( Config.Templates_title_cdaudio );	Config.Templates_title_cdaudio = NULL;
	g_free( Config.Templates_rep_cdaudio );		Config.Templates_rep_cdaudio = NULL;
	g_free( Config.StringNameNavigateur );		Config.StringNameNavigateur = NULL;
	g_free( Config.StringParamNameNavigateur );	Config.StringParamNameNavigateur = NULL;
	g_free( Config.StringNameLecteurPostScript );	Config.StringNameLecteurPostScript = NULL;
	g_free( Config.StringNameLecteurAudio );	Config.StringNameLecteurAudio = NULL;
	g_free( Config.StringParamNameLecteurAudio );	Config.StringParamNameLecteurAudio = NULL;
	g_free( Config.StringBoolFieldsIsVisible );	Config.StringBoolFieldsIsVisible = NULL;
	g_free( Config.StringPosFieldsName );		Config.StringPosFieldsName = NULL;
	g_free( Config.entry_cddp_server );		Config.entry_cddp_server = NULL;
	g_free( Config.entry_proxy_server );		Config.entry_proxy_server = NULL;
	g_free( Config.entry_proxy_port );		Config.entry_proxy_port = NULL;
	g_free( Config.NameImg );			Config.NameImg = NULL;
	g_free( Config.PathLoadImg );			Config.PathLoadImg = NULL;
	g_free( Config.PathSaveImg );			Config.PathSaveImg = NULL;
	g_free( Config.PathMusicFileEndOfConvert );	Config.PathMusicFileEndOfConvert  = NULL;	
	g_free( Config.FileMusicFileEndOfConvert );	Config.FileMusicFileEndOfConvert = NULL;
	g_free( Config.StringCommentCD );		Config.StringCommentCD = NULL;
}



