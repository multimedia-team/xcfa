 /*
 *  file      : poche_web.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <cairo.h>
#include <cairo-pdf.h>
#include <cairo-ps.h>
#include <cairo-xlib.h>
#include <X11/Xlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "win_scan.h"
#include "win_info.h"
#include "configuser.h"
#include "poche.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

#define BUFFER_SIZE 1024


typedef struct {
	gboolean	EndThread;
	gboolean	EndTimeout;
	gchar		*name;
	guint		timeout;
	pthread_t	nmr_tid;
	guint		pocheweb_tt_glist;
	gchar		*name_index_html;
} POCHEWEB_EVENT;

POCHEWEB_EVENT pocheweb_event;


/*
*---------------------------------------------------------------------------
* FONCTIONS
*---------------------------------------------------------------------------
*/

/* Recuperation sur le web du fichier image et sauvegarde
*/
gchar *pocheweb_get_on_web_file_img (gchar *filename)
{
	gchar    *name_file = NULL;
	gint      num;
	gchar    *LineCommand = NULL;

	PRINT_FUNC_LF();

	// NEW NAME IF IMAGE EXIST
	num = 1;
	while (TRUE) {
		name_file = g_strdup_printf ("%s/%03d.jpg", Config.PathPochette, num);
		if (libutils_test_file_exist (name_file) == FALSE) {
			/*g_print ("NOUVEAU NOM: %s\n", name_file);*/
			break;
		}
		num ++;
		g_free (name_file);
		name_file = NULL;
	}
	
	// IMAGE IMPORT
	LineCommand = g_strdup_printf ("wget --user-agent=\"Mozilla 22.0\" -O %s %s", name_file, filename);
	system (LineCommand);
	g_free (LineCommand);
	LineCommand = NULL;
	
	// L'IMPORT EST IL CORRECT ?
	if (libutils_test_file_exist (name_file) == FALSE) {
		g_free(name_file);
		name_file = NULL;
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print( " ******************************BAD IMPORT ******************************\n");
	}
	
	// IMAGE FILE ?
	if( NULL != name_file ) {
		if( FALSE == FileIs_image( name_file )) {
			g_unlink (name_file);
			g_free (name_file);
			name_file = NULL;
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print( " ******************************BAD IMAGE ******************************\n");
		}
	}

	return ((gchar *)name_file);
}

// Le fichier NAME_INDEX_HTML est analyse:
// Tous les noms de fichiers seront passes en parametre a: pocheget_get_file (char *filename, int cpt)
// pour recuperation et seront sauvegardes
// 
void pocheweb_analyze_file_html (void)
{
	size_t	lenfile = 0;
	gchar	*buffer = NULL;
	gchar   *result = NULL;
	FILE    *fp;
	gchar   *PtrName = NULL;
	const	char *prefix = "/images?q=tbn:";
	char	*url_start = NULL;
	char	*url_end = NULL;
	
	PRINT_FUNC_LF();

	pocheweb_event.name_index_html = g_strdup_printf ("%s/index.html", Config.PathPochette);

	lenfile = libutils_get_size_file (pocheweb_event.name_index_html);
	if (lenfile <= 100) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("ERREUR TAILLE DE %s = %d\n", pocheweb_event.name_index_html, (gint)lenfile);
		g_free (pocheweb_event.name_index_html);
		pocheweb_event.name_index_html = NULL;
		return;
	}
	buffer = g_malloc0 (sizeof (char) * (lenfile + 1024));

	fp = fopen (pocheweb_event.name_index_html, "r");
	fread (buffer, 1, lenfile, fp);
	fclose (fp);

	url_start = buffer;
	do {
		url_start = strstr( url_start, prefix );
		if( NULL != url_start ) {
			url_end = strstr( url_start, "\"" );
			if( NULL != url_end ) {
				
				result = g_strnfill( (url_end - url_start) + 100, '\0' );
				strcpy( result, "www.google.com/" );
				strncat( result, url_start, url_end - url_start );
				
				// printf("result = www.google.com/%s\n", result );
				
				if ((PtrName = pocheweb_get_on_web_file_img (result))) {
					if (pocheweb_event.name) {
						g_free (pocheweb_event.name);
						pocheweb_event.name = NULL;
					}
					pocheweb_event.name = g_strdup( PtrName );
					pochedir_make_glist( PtrName );
					
					g_free( PtrName );
					PtrName = NULL;
				}
				
				url_start = url_end + 1;
				
				g_free( result );
				result = NULL;
				
				if (WindScan_close_request ()) {
					if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
						g_print ("DEMANDE D'ANNULATION DEPUIS: ");
						PRINT_FUNC_LF();
					}
					break;
				}
				
			}
			else break;
		}
	} while( NULL != url_start );

	g_free (buffer);
	buffer = NULL;
	g_unlink (pocheweb_event.name_index_html);
	g_free (pocheweb_event.name_index_html);
	pocheweb_event.name_index_html = NULL;
}

/* Recuperation du fichier *.html et sauvegarde
*/
gboolean pocheweb_recup_file_html (gchar *cd_name)
{
	gchar     *name = NULL;
	gchar     *ptrname = NULL;
	gboolean   bool_ret = FALSE;
	gchar     *PtrNew = NULL;
	gchar     *Ptr = NULL;
	gchar     *LineCommand = NULL;

	PRINT_FUNC_LF();

	/* Construction du path	*/
	Ptr = PtrNew = g_strdup_printf ("http://images.google.com/images?q=%s", cd_name);
	/* Chaine de transformation du nom */
	name = g_strnfill (2048, '\0');
	ptrname = name;
/*
http://images.google.com/images?q=Dire   Straits   Brothers   In   Arms+Dire   Straits   Brothers   in   arms
http://images.google.com/images?q=Dire%20Straits%20Brothers%20In%20Arms+Dire%20Straits%20Brothers%20in%20arms
*/

	while (*Ptr) {
		if (*Ptr == ' ') {
			*ptrname ++ = '%';
			*ptrname ++ = '2';
			*ptrname ++ = '0';
			Ptr ++;
		}
		else {
			*ptrname ++ = *Ptr ++;
		}
	}

	g_free (PtrNew);
	PtrNew = Ptr = NULL;
	
	pocheweb_event.name_index_html = g_strdup_printf ("%s/index.html", Config.PathPochette);
	LineCommand = g_strdup_printf ("wget --user-agent=\"Mozilla 22.0\" -O %s %s", pocheweb_event.name_index_html, name);
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print( "pocheweb_event.name_index_html = %s\n", pocheweb_event.name_index_html );	// /tmp/XcfaPochette/index.html
		g_print( "name                           = %s\n", name );				// http://images.google.com/images?q=
		g_print( "LineCommand=%s\n", LineCommand );	// wget --user-agent="Mozilla 22.0" -O /tmp/XcfaPochette/index.html http://images.google.com/images?q=
	}
	system (LineCommand);
	g_free (LineCommand);
	LineCommand = NULL;

	bool_ret = (libutils_test_file_exist (pocheweb_event.name_index_html) && libutils_get_size_file (pocheweb_event.name_index_html) > 0);
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print( "bool_ret = %s\n", TRUE == bool_ret ? "TRUE" : "FALSE" );
	}
	g_free (name);
	name = NULL;

	return (bool_ret);
}

static void pocheweb_thread (void *arg)
{
	gchar *ptr_cdname = NULL;

	ptr_cdname = pochetxt_get_ptr_entry_img_web();
	
	if (pocheweb_recup_file_html (ptr_cdname) == TRUE) {
		pocheweb_analyze_file_html ();
	}

	pocheweb_event.EndTimeout = TRUE;
	pocheweb_event.EndThread = TRUE;

	pthread_exit(0);
}

gboolean pocheweb_bool_file_add (void)
{
	return( pocheweb_event.pocheweb_tt_glist < g_list_length (view.glist ) ? TRUE : FALSE );
}

static gint pocheweb_timeout (gpointer data)
{
	if (pocheweb_event.name) {
		gchar	*Str = g_strdup_printf ("<b><i>%s</i></b>", pocheweb_event.name);
		WindScan_set_label (Str);
		g_free (pocheweb_event.name);
		pocheweb_event.name = NULL;
		g_free (Str);
		Str = NULL;
	}
	else if (pocheweb_event.EndThread == TRUE) {
		pochedir_table_add_images ();
		// pocheevnt_set_flag_buttons_recto ();
		// pocheevnt_set_flag_buttons_verso ();
		g_source_remove (pocheweb_event.timeout);
		
		WindScan_close ();

		if (pocheweb_bool_file_add () == FALSE) {
			wind_info_init (
				WindMain,
				_("No file found !"),
				_("To solve this problem :"),
				  "\n",
				  "-----------------------------------\n",
				_("Modify the search parameters and"),
				  "\n",
				_("start again."),
			  	  "");
		}
	}
	return (TRUE);
}

void pocheweb_get (void)
{
	gchar	*StrEntryWeb = pochetxt_get_ptr_entry_img_web();
	
	// PRINT_FUNC_LF();
	
	if( NULL == StrEntryWeb || '\0' == *StrEntryWeb ) {
		wind_info_init (
			WindMain,
			_("Search criteria away  !!"),
			_("To solve this problem :"),
			  "\n",
			  "-----------------------------------\n",
				_("Modify the search parameters and"),
				  "\n",
				_("start again."),
		  	  "");
		return;
	}
	pocheweb_event.pocheweb_tt_glist = g_list_length (view.glist);
	// WindScan_open ("Recherche des fichiers images", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Recherche des fichiers images",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Recherche des fichiers images ...</i></b>");
	pocheweb_event.name = g_strdup (_("[Search images files]"));
	pocheweb_event.name = NULL;
	pocheweb_event.EndThread = FALSE;
	pocheweb_event.EndTimeout = FALSE;
	pocheweb_event.timeout = g_timeout_add (10, pocheweb_timeout, 0);
	pthread_create (&pocheweb_event.nmr_tid, NULL ,(void *)pocheweb_thread, (void *)NULL);
}

