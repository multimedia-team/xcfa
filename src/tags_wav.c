 /*
 *  file    : tags_wav.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"



// 
// 
void tagswav_close_file( WAVE *WaveHeader )
{
	if (NULL != WaveHeader->file) {
		fclose(WaveHeader->file);  
		WaveHeader->file = NULL;
	}
}
// 
// 
void tagswav_init (WAVE *WaveHeader)
{
	WaveHeader->RIFF.ChunkID [ 0 ]     = '\0';
	WaveHeader->RIFF.ChunkSize         = 0;
	WaveHeader->RIFF.Format [ 0 ]      = '\0';
	WaveHeader->FMT.Subchunk1ID [ 0 ]  = '\0';
	WaveHeader->FMT.Subchunk1Size      = 0;
	WaveHeader->FMT.AudioFormat        = 0;
	WaveHeader->FMT.NumChannels        = 0;
	WaveHeader->FMT.SampleRate         = 0;
	WaveHeader->FMT.ByteRate           = 0;
	WaveHeader->FMT.Blockalign         = 0;
	WaveHeader->FMT.BitsPerSample      = 0;
	WaveHeader->DATA.Subchunk2ID [ 0 ] = '\0';
	WaveHeader->DATA.Subchunk2Size     = 0;
	WaveHeader->DATA.data              = NULL;
	WaveHeader->file                   = NULL;
	WaveHeader->TagWavIsFmtBext        = TAG_WAVE_IS_NONE;
}
// 
// 
void tagswav_print_header( gchar *p_PathNameFile,  WAVE	 *WaveHeader)
{
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		
		g_print ("\n" );
		g_print ("\t%s\n", p_PathNameFile );
		g_print ("\tChunkID        = %c%c%c%c\n", WaveHeader->RIFF.ChunkID [ 0 ], WaveHeader->RIFF.ChunkID [ 1 ], WaveHeader->RIFF.ChunkID [ 2 ], WaveHeader->RIFF.ChunkID [ 3 ] );
		g_print ("\tChunkSize      = %d\n", WaveHeader->RIFF.ChunkSize);
		g_print ("\tFormat         = %c%c%c%c\n", WaveHeader->RIFF.Format [ 0 ], WaveHeader->RIFF.Format [ 1 ], WaveHeader->RIFF.Format [ 2 ], WaveHeader->RIFF.Format [ 3 ] );
		
		g_print( "\t%s\n", (WaveHeader->TagWavIsFmtBext == TAG_WAVE_IS_FMT) ? "[ 'fmt ' ]" : "[ 'bext' | 'qlty' | 'levl' | 'link' | 'axml' ]" );

		g_print ("\tSubchunk1ID    = %c%c%c%c\n", WaveHeader->FMT.Subchunk1ID [ 0 ], WaveHeader->FMT.Subchunk1ID [ 1 ], WaveHeader->FMT.Subchunk1ID [ 2 ], WaveHeader->FMT.Subchunk1ID [ 3 ] );
		g_print ("\tSubchunk1Size  = %d\n", WaveHeader->FMT.Subchunk1Size);  
		g_print ("\tAudioFormat    = %d\n", WaveHeader->FMT.AudioFormat);  
		g_print ("\tNumChannels    = %d\n", WaveHeader->FMT.NumChannels);  
		g_print ("\tSampleRate     = %d\n", WaveHeader->FMT.SampleRate);  
		g_print ("\tByteRate       = %d\n", WaveHeader->FMT.ByteRate);  
		g_print ("\tBlockalign     = %d\n", WaveHeader->FMT.Blockalign);  
		g_print ("\tBitsPerSample  = %d\n", WaveHeader->FMT.BitsPerSample);
		g_print ("\tSubchunk2ID    = %c%c%c%c\n", WaveHeader->DATA.Subchunk2ID [ 0 ], WaveHeader->DATA.Subchunk2ID [ 1 ], WaveHeader->DATA.Subchunk2ID [ 2 ], WaveHeader->DATA.Subchunk2ID [ 3 ] );
		g_print ("\tSubchunk2Size  = %d\n", WaveHeader->DATA.Subchunk2Size);
		g_print ("\n");
	}
}
// 
// 
void tagswav_print( gchar *p_PathNameFile )
{
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		
		WAVE	WaveHeader;
	
		if (TRUE == tagswav_read_file( p_PathNameFile, &WaveHeader )) {
			tagswav_print_header( p_PathNameFile, &WaveHeader );
		}
		tagswav_close_file( &WaveHeader );
	}
}
// 
// 
gint tagswav_get_pos( gchar *p_PathNameFile, gchar p_1, gchar p_2, gchar p_3, gchar p_4 )
{
	FILE		*file = NULL;
 	unsigned char 	devbuf[ BLOCK_SIZE +10 ];
 	gint		n_read;
	gint		tt_read = 0;
	gint		cpt;
	
	if( NULL != (file = fopen( p_PathNameFile, "rb" ))) { 
	
		do {
			// LECTURE D UN BLOCK
			n_read = fread( devbuf, BLOCK_SIZE, 1, file );
		
			// RECHERCHE DE: data
			for( cpt = 0; cpt < BLOCK_SIZE; cpt ++ ) {
				if( devbuf [ cpt +0 ] == p_1 && devbuf [ cpt +1 ] == p_2 && devbuf [ cpt +2 ] == p_3 && devbuf [ cpt +3 ] == p_4 ) {
					return( tt_read + cpt );
				}
			}
		
			// VARIABLE TOTAL OCTECTS LU MOINS 8 AU CAS OU data SOIS COUPE EN FIN DE BLOCK
			tt_read += BLOCK_SIZE;
			tt_read -= 8;
		
			// POINTEUR AU DEBUT DU FICHIER
			rewind( file );
		
			// POINTEUR DE LECTURE SUR tt_read
			fseek( file, tt_read,  SEEK_CUR );
		
		} while( n_read > 0 );
	
		fclose( file );
	}
	
	return( -1 );
}
// 
// MODIF DU 02 MAI 2013 A LA DEMANDE DE CHRISTOPHE MARILLAT POUR TENIR COMPTE DE : Broadcast Wave Format
// VERSION DE TEST:
//	xcfa-4.3.4~beta0
//	xcfa-4.3.4~beta1
// 
/*

broadcast_audio_extension typedef struct {
  DWORD  ckID;                   	 // (broadcastextension)ckID=bext.
  DWORD  ckSize;                 	 // size of extension chunk
  BYTE   ckData[ckSize];         	 // data of the chunk
}


Quality_chunk typedef struct {
  DWORD          ckID;           	// (quality_chunk) ckID='qlty'
  DWORD          ckSize;         	// size of quality chunk
  BYTE           ckData[ckSize]; 	// data of the chunk
}


typedef struct peak_envelope
{
   CHAR           ckID[4];		// {'l','e','v','l'}
   DWORD          ckSize;		// size of chunk
   DWORD          dwVersion;		// version information
   DWORD          dwFormat;		// format of a peak point
					// 1 = unsigned char
					// 2 = unsigned short
   DWORD          dwPointsPerValue;	// 1 = only positive peak point
					// 2 = positive AND negative peak point
   DWORD          dwBlockSize;		// frames per value
   DWORD          dwPeakChannels;	// number of channels
   DWORD          dwNumPeakFrames;	// number of peak frames
   DWORD          dwPosPeakOfPeaks;	// audio sample frame index
					// or 0xFFFFFFFF if unknown
   DWORD          dwOffsetToPeaks;	// should usually be equal to the size of this header, but
                                          could also be higher
   CHAR           strTimestamp[28];	// ASCII: time stamp of the peak data
   CHAR           reserved[60];		// reserved set to 0x00
   CHAR           peak_envelope_data[]	// the peak point data
}
levl_chunk;


typedef struct link
{
    CHAR          CkID[4];		// 'link'
    DWORD         CkSize;		// size of chunk
    CHAR          XmlData[ ];		// text data in XML
}
link_chunk;


typedef struct axml
{
    CHAR        ckID[4];		// {'a','x','m','l'}
    DWORD       ckSize;			// size of chunk
    CHAR        xmlData[ ];		// text data in XML
}
axml_chunk;

*/
gboolean tagswav_read_file (gchar *wave_file, WAVE *WaveHeader)
{
/*
	gint	seek_bext = -1;
	gint	seek_qlty = -1;
	gint	seek_levl = -1;
	gint	seek_link = -1;
	gint	seek_axml = -1;
	gint	seek_fmt = -1;
	gint	seek_data = -1;
 	gint	n_read;
	
	tagswav_init (WaveHeader);
	
	if (NULL == (WaveHeader->file = fopen(wave_file, "rb"))) return (FALSE);
	
	seek_fmt  = tagswav_get_pos( wave_file, 'f', 'm', 't', ' ' );
	seek_data = tagswav_get_pos( wave_file, 'd', 'a', 't', 'a' );
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		seek_bext = tagswav_get_pos( wave_file, 'b', 'e', 'x', 't' );
		seek_qlty = tagswav_get_pos( wave_file, 'q', 'l', 't', 'y' );
		seek_levl = tagswav_get_pos( wave_file, 'l', 'e', 'v', 'l' );
		seek_link = tagswav_get_pos( wave_file, 'l', 'i', 'n', 'k' );
		seek_axml = tagswav_get_pos( wave_file, 'a', 'x', 'm', 'l' );
	}
	
	if( -1 == seek_fmt || -1 == seek_data ) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print("Entete incorrecte du fichier [ %s ]\n", wave_file );
			g_print ("\tseek_bext = %d\n", seek_bext );
			g_print ("\tseek_qlty = %d\n", seek_qlty );
			g_print ("\tseek_levl = %d\n", seek_levl );
			g_print ("\tseek_link = %d\n", seek_link );
			g_print ("\tseek_axml = %d\n", seek_axml );
			g_print ("\tseek_fmt  = %d\n", seek_fmt  );
			g_print ("\tseek_data = %d\n", seek_data );
		}
		tagswav_close_file( WaveHeader );
		return( FALSE );
	}
	
	// LECTURE ENTETE
	n_read = fread(&WaveHeader->RIFF.ChunkID, 4, 1, WaveHeader->file);		// RIFF
	n_read = fread(&WaveHeader->RIFF.ChunkSize, 4, 1, WaveHeader->file);		// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
	n_read = fread(&WaveHeader->RIFF.Format, 4, 1, WaveHeader->file);		// WAVE
	n_read = fread(&WaveHeader->FMT.Subchunk1ID, 4, 1, WaveHeader->file);		// 'fmt ' | 'bext'
	
	if( 0 == strncmp( WaveHeader->FMT.Subchunk1ID, "fmt ", 4 )) {
		
		WaveHeader->TagWavIsFmtBext = TAG_WAVE_IS_FMT;		
	}
	else {
		
		WaveHeader->TagWavIsFmtBext = TAG_WAVE_IS_BEXT;
		
		n_read = fseek( WaveHeader->file, seek_fmt + 4, SEEK_SET );
	}
	
	// FMT
	n_read = fread(&WaveHeader->FMT.Subchunk1Size, 4, 1, WaveHeader->file);		// taille en octet des données à suivre
	n_read = fread(&WaveHeader->FMT.AudioFormat, 2, 1, WaveHeader->file);		// format de compression (une valeur autre que 1 indique une compression) 
	n_read = fread(&WaveHeader->FMT.NumChannels, 2, 1, WaveHeader->file);		// nombre de canaux
	n_read = fread(&WaveHeader->FMT.SampleRate, 4, 1, WaveHeader->file);		// fréquence d'échantillonage (nombre d'échantillons par secondes) 
	n_read = fread(&WaveHeader->FMT.ByteRate, 4, 1, WaveHeader->file);		// nombre d'octects par secondes
	n_read = fread(&WaveHeader->FMT.Blockalign, 2, 1, WaveHeader->file);		// nombre d'octects pour coder un échantillon
	n_read = fread(&WaveHeader->FMT.BitsPerSample, 2, 1, WaveHeader->file);		// nombre de bits pour coder un échantillon
	
	// DATA
	n_read = fseek( WaveHeader->file, seek_data, SEEK_SET );
	n_read = fread( &WaveHeader->DATA.Subchunk2ID, 4, 1, WaveHeader->file );
	n_read = fread( &WaveHeader->DATA.Subchunk2Size, 4, 1, WaveHeader->file );
	
	tagswav_print_header( wave_file,  WaveHeader );
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("\tseek_bext = %d\n", seek_bext );
		g_print ("\tseek_qlty = %d\n", seek_qlty );
		g_print ("\tseek_levl = %d\n", seek_levl );
		g_print ("\tseek_link = %d\n", seek_link );
		g_print ("\tseek_axml = %d\n", seek_axml );
		g_print ("\tseek_fmt  = %d\n", seek_fmt  );
		g_print ("\tseek_data = %d\n", seek_data );
	}
	return( TRUE );
*/
	gint	seek_fmt = -1;
	gint	seek_data = -1;
 	gint	n_read;
	
	seek_fmt  = tagswav_get_pos( wave_file, 'f', 'm', 't', ' ' );
	seek_data = tagswav_get_pos( wave_file, 'd', 'a', 't', 'a' );
	if( -1 == seek_fmt || -1 == seek_data ) return( FALSE );
	
	if( NULL == ( WaveHeader->file = fopen( wave_file, "rb" ))) return (FALSE);
	// LECTURE ENTETE
	n_read = fread( &WaveHeader->RIFF.ChunkID, 4, 1, WaveHeader->file );		// RIFF
	n_read = fread( &WaveHeader->RIFF.ChunkSize, 4, 1, WaveHeader->file );		// taille du fichier entier en octets (sans compter les 8 octets de ce champ et le champ précédent
	n_read = fread( &WaveHeader->RIFF.Format, 4, 1, WaveHeader->file );		// WAVE
	n_read = fread( &WaveHeader->FMT.Subchunk1ID, 4, 1, WaveHeader->file );		// 'fmt ' | 'bext'
	
	if( 0 == strncmp( WaveHeader->FMT.Subchunk1ID, "fmt ", 4 )) {
		
		WaveHeader->TagWavIsFmtBext = TAG_WAVE_IS_FMT;		
	}
	else {
		WaveHeader->TagWavIsFmtBext = TAG_WAVE_IS_BEXT;
		
		n_read = fseek( WaveHeader->file, seek_fmt + 4, SEEK_SET );
	}
	
	// FMT
	n_read = fread( &WaveHeader->FMT.Subchunk1Size, 4, 1, WaveHeader->file );	// taille en octet des données à suivre
	n_read = fread( &WaveHeader->FMT.AudioFormat, 2, 1, WaveHeader->file );		// format de compression (une valeur autre que 1 indique une compression) 
	n_read = fread( &WaveHeader->FMT.NumChannels, 2, 1, WaveHeader->file );		// nombre de canaux
	n_read = fread( &WaveHeader->FMT.SampleRate, 4, 1, WaveHeader->file );		// fréquence d'échantillonage (nombre d'échantillons par secondes) 
	n_read = fread( &WaveHeader->FMT.ByteRate, 4, 1, WaveHeader->file );		// nombre d'octects par secondes
	n_read = fread( &WaveHeader->FMT.Blockalign, 2, 1, WaveHeader->file );		// nombre d'octects pour coder un échantillon
	n_read = fread( &WaveHeader->FMT.BitsPerSample, 2, 1, WaveHeader->file );	// nombre de bits pour coder un échantillon
	
	// DATA
	n_read = fseek( WaveHeader->file, seek_data, SEEK_SET );
	n_read = fread( &WaveHeader->DATA.Subchunk2ID, 4, 1, WaveHeader->file );
	n_read = fread( &WaveHeader->DATA.Subchunk2Size, 4, 1, WaveHeader->file );
	
	if( n_read );
	tagswav_print_header( wave_file,  WaveHeader );
	
	return( TRUE );

}
// 
// 
INFO_WAV *tagswav_remove_info (INFO_WAV *info)
{
	if( NULL != info ) {
		if( NULL != info->time )		{ g_free( info->time );		info->time = NULL;		}
		if( NULL != info->hertz )		{ g_free( info->hertz );	info->hertz = NULL;		}
		if( NULL != info->voie )		{ g_free( info->voie );		info->voie = NULL;		}
		if( NULL != info->bits )		{ g_free( info->bits );		info->bits = NULL;		}
		if( NULL != info->NewHertz )	{ g_free( info->NewHertz );	info->NewHertz = NULL;	}
		if( NULL != info->NewVoie )		{ g_free( info->NewVoie );	info->NewVoie = NULL;	}
		if( NULL != info->NewBits )		{ g_free( info->NewBits );	info->NewBits = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}
	return ((INFO_WAV *)NULL);
}
// 
// 
gboolean tagswav_file_is_mono (gchar *namefile)
{
	WAVE	WaveHeader;
	
	if (TRUE == tagswav_read_file (namefile, &WaveHeader)) {
		tagswav_close_file( &WaveHeader );
		return (WaveHeader.FMT.NumChannels == 1 ? TRUE : FALSE);
	}
	return (FALSE);
}
// 
// 
gboolean tagswav_file_GetBitrate (gchar *namefile, gint *Channels, gint *Hertz, gint *Bits)
{
	WAVE	WaveHeader;
	
	if (TRUE == tagswav_read_file (namefile, &WaveHeader)) {
		tagswav_close_file( &WaveHeader );
		if (Channels)	*Channels = WaveHeader.FMT.NumChannels;
		if (Hertz)	*Hertz    = WaveHeader.FMT.SampleRate;
		if (Bits)	*Bits     = WaveHeader.FMT.BitsPerSample;
		return ((WaveHeader.FMT.NumChannels != 2 || WaveHeader.FMT.SampleRate != 44100 || WaveHeader.FMT.BitsPerSample != 16) ? TRUE : FALSE);
	}
	return (FALSE);
}
// 
// 
INFO_WAV *tagswav_get_info (gchar *namefile)
{
	WAVE		WaveHeader;
	INFO_WAV	*ptrinfo = NULL;
	gint		m;
	gint		s;
	gint		sec;
	gboolean	BoolReadWavFile;
	
	// PRINT_FUNC_LF();
	
	ptrinfo = (INFO_WAV *)g_malloc0 (sizeof (INFO_WAV));
	if (NULL == ptrinfo) {
		g_print ("!---------------------------------------\n");
		PRINT_FUNC_LF();
		g_print ("!---------------------------------------\n");
		g_print ("! PTRINFO EST NULL  :(\n");
		g_print ("!---------------------------------------\n");
		return (NULL);
	}

	BoolReadWavFile = tagswav_read_file (namefile, &WaveHeader);
	tagswav_close_file( &WaveHeader );
	
	if (FALSE == BoolReadWavFile) {
		g_free (ptrinfo);
		PRINT("EXIT");
		ptrinfo = NULL;
		return (NULL);
	}
	if (WaveHeader.DATA.Subchunk2Size == 0 || WaveHeader.FMT.ByteRate == 0) {
		g_free (ptrinfo);
		ptrinfo = NULL;
		g_print ("! ---\n");
		g_print ("! MAUVAIS ENTETE DU FICHIER: %s\n", namefile);
		g_print ("! ---\n\n");
		return (NULL);
	}
	
	// calcul de la duree
	sec = WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate;
	ptrinfo->SecTime              = sec;

	s = sec % 60; sec /= 60;
	m = sec % 60; sec /= 60;
	if (sec > 0) ptrinfo->time = g_strdup_printf ("%02d:%02d:%02d", sec, m, s);
	else         ptrinfo->time = g_strdup_printf ("%02d:%02d", m, s);
	
	// 'fmt ', 'bext', 'qlty', 'levl', 'link','axml'
	ptrinfo->BoolBwf = WaveHeader.TagWavIsFmtBext == TAG_WAVE_IS_FMT ? FALSE : TRUE;
	
	// mode
	ptrinfo->voie    = g_strdup_printf ("%d", WaveHeader.FMT.NumChannels);
	ptrinfo->NewVoie = g_strdup_printf ("%d", WaveHeader.FMT.NumChannels);

	// freq
	ptrinfo->hertz    = g_strdup_printf ("%d", WaveHeader.FMT.SampleRate);
	ptrinfo->NewHertz = g_strdup_printf ("%d", WaveHeader.FMT.SampleRate);

	// format
	ptrinfo->bits    = g_strdup_printf ("%d", WaveHeader.FMT.BitsPerSample);
	ptrinfo->NewBits = g_strdup_printf ("%d", WaveHeader.FMT.BitsPerSample);
	
	ptrinfo->BoolConv= FALSE;

	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);
	tags_set (namefile, ptrinfo->tags);
	
	ptrinfo->LevelDbfs.level = -1;
	ptrinfo->LevelDbfs.NewLevel = -1;

	return (ptrinfo);
}
// 
// RETOUR DE LA DUREE D'ECOUTE EN SECONDES
// 		
gint tagswav_get_time_sec (gchar *namefile)
{
	WAVE	WaveHeader;
	
	// PRINT_FUNC_LF();
	
	if (TRUE == tagswav_read_file (namefile, &WaveHeader)) {
		tagswav_close_file( &WaveHeader );
		return (WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate);
	}
	return (0);
}
		
		





