 /*
 *  file      : poche_save.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>	// M_PI
#include <cairo.h>
#include <cairo-pdf.h>
#include <cairo-ps.h>
#include <cairo-xlib.h>
#include <X11/Xlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "win_info.h"
#include "configuser.h"
#include "statusbar.h"
#include "poche.h"




// SEE:
// http://en.literateprograms.org/Hello_World_%28C,_Cairo%29
// 
void pochesave_write_png( gchar *fname, gint width, gint height )
{
	cairo_surface_t *cs;

	cs = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, MM(290), MM(360) );
	pochedraw_paint( cs, NULL );
	cairo_surface_write_to_png( cs, fname );
	cairo_surface_destroy( cs );
}
// SEE:
// http://en.literateprograms.org/Hello_World_%28C,_Cairo%29
// 
void pochesave_write_pdf( gchar *fname, gint width, gint height )
{
	cairo_surface_t *cs;
	cairo_t			*cr = NULL;
	
	
	// cs = cairo_pdf_surface_create( fname, MM(280), MM(300) );
	// cs = cairo_pdf_surface_create( fname, width, height );
	cs = cairo_pdf_surface_create( fname, width, 816 );
	cr = pochedraw_paint( cs, NULL );
	// cairo_surface_flush( cs );
	cairo_surface_destroy( cs );
	cairo_show_page( cr );
	cairo_destroy( cr );
}
// SEE:
// http://en.literateprograms.org/Hello_World_%28C,_Cairo%29
// 
void pochesave_write_ps( gchar *fname, gint width, gint height )
{
	cairo_surface_t *cs;
	
	cs = cairo_ps_surface_create( fname, width, height );
	pochedraw_paint( cs, NULL );
	cairo_surface_destroy( cs );
}
// 
// PRINT WITH GIMP IS OK
// PRINT PS and PDF IS OK
// 
void on_button_save_clicked( GtkButton *button, gpointer user_data )
{
	gdouble		OldScale = view.scale;
	gboolean	BoolScaleAdjust = view.BoolScaleAdjust;
	gchar		*PathNameSaveFile = NULL;
	gchar		*NameSaveFile = pochetxt_get_ptr_entry_name_file_to_save();
	GtkAllocation	allocation;
	
	if( NULL == NameSaveFile || '\0' == *NameSaveFile ) {
		wind_info_init (
			WindMain,
			_("File name not found  !!"),
			_("To solve this problem :"),
			  "\n",
			  "-----------------------------------\n",
			_("Enter a file name without extension"),
		  	  "");
		return;
	}
	
	view.BoolScaleAdjust = FALSE;
	
	// 
	// PDF POSTSCRIPT
	// 
	StatusBar_set_mess( NOTEBOOK_POCHETTE,  _STATUSBAR_SIMPLE_, _("Saving to PDF") );
	view.scale = 10;
	poche_set_size_request();
	view.scale = 1.55;
	printf("view.scale = 1.55 \n" );
	do {
		poche_set_size_request();
		while (gtk_events_pending()) gtk_main_iteration();
		gtk_widget_get_allocation( view.AdrDrawingarea, &allocation );
		printf("	allocation.width = %d   allocation.height = %d\n", allocation.width, allocation.height );
	} while( allocation.width != 576 && allocation.height != 816 );
	
	// REDIRECTION VERS PDF
	view.BoolSaveToFile  = TRUE;

	PathNameSaveFile = g_strdup_printf( "%s/%s.pdf", Config.PathSaveImg, NameSaveFile );
	gtk_widget_get_allocation( view.AdrDrawingarea, &allocation );
	pochesave_write_pdf( PathNameSaveFile, allocation.width, allocation.height );
	g_free( PathNameSaveFile );
	PathNameSaveFile = NULL;
	
	/**
	// REDIRECTION VERS POSTSCRIPT
	PathNameSaveFile = g_strdup_printf( "%s/%s.ps", Config.PathSaveImg, NameSaveFile );
	pochesave_write_ps( PathNameSaveFile, allocation.width, allocation.height );
	g_free( PathNameSaveFile );
	PathNameSaveFile = NULL;
	view.BoolSaveToFile  = FALSE;
	
	// 
	// PNG
	// 
	// StatusBar_puts( _("Sauvegarde au format PNG") );
	StatusBar_set_mess( NOTEBOOK_POCHETTE,  _STATUSBAR_SIMPLE_, _("Sauvegarde au format PNG") );
	view.scale = 2.0;
	printf("view.scale = 2.0 \n" );
	do {
		poche_set_size_request();
		while (gtk_events_pending()) gtk_main_iteration();
		gtk_widget_get_allocation( view.AdrDrawingarea, &allocation );
		printf("	allocation.width = %d   allocation.height = %d\n", allocation.width, allocation.height );
	} while( allocation.width > 576 && allocation.height > 816 );
	
	PathNameSaveFile = g_strdup_printf( "%s/%s.png", Config.PathSaveImg, NameSaveFile );
	// REDIRECTION VERS PNG
	view.BoolSaveToFile  = TRUE;
	pochesave_write_png( PathNameSaveFile, allocation.width, allocation.height );
	g_free( PathNameSaveFile );
	PathNameSaveFile = NULL;
	*/
	
	view.BoolSaveToFile  = FALSE;
	view.scale           = OldScale;
	view.BoolScaleAdjust = BoolScaleAdjust;
	poche_set_size_request();
	gtk_widget_queue_draw( view.AdrDrawingarea );
	// StatusBar_puts( "" );
	StatusBar_set_mess( NOTEBOOK_POCHETTE,  _STATUSBAR_SIMPLE_, "" );
}






