 /*
 *  file      : poche_txt.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>	// M_PI
#include <cairo.h>
#include <cairo-pdf.h>
#include <cairo-ps.h>
#include <cairo-xlib.h>
#include <X11/Xlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "scan.h"
#include "cd_curl.h"
#include "poche.h"
// #include "cd_audio.h"




// 
// FONT BOLD
// 
gboolean pochetxt_get_is_bold( void )
{
	return( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(GLADE_GET_OBJECT("togglebutton_font_bold"))) );
}
void on_togglebutton_font_bold_clicked( GtkButton *button, gpointer user_data )
{
	IMAGE	*Image = poche_get_struct_selected_is_txt();
	
	if( NULL != Image ) {
		Image->BoolFontBold = pochetxt_get_is_bold();
		gtk_widget_queue_draw( view.AdrDrawingarea );
	}
}

// 
// FONT ITALIC
// 
gboolean pochetxt_get_is_italic( void )
{
	return( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(GLADE_GET_OBJECT("togglebutton_font_italic"))) );
}
void on_togglebutton_font_italic_clicked( GtkButton *button, gpointer user_data )
{
	IMAGE	*Image = poche_get_struct_selected_is_txt();
	
	if( NULL != Image ) {
		Image->BoolFontItalic = pochetxt_get_is_italic();
		gtk_widget_queue_draw( view.AdrDrawingarea );
	}
}
	
// 	
// NAME FILE TO PNG / PS / PDF
// 
gchar *pochetxt_get_ptr_entry_name_file_to_save( void )
{
	if( NULL != view.AdrEntryNameFileSave ) {
		return( (gchar *)gtk_entry_get_text (GTK_ENTRY (view.AdrEntryNameFileSave)));
	}
	return( NULL );
}
void on_entry_name_save_file_changed( GtkEditable *editable, gpointer user_data )
{
	if( NULL != view.AdrEntryNameFileSave ) {
		if( NULL != Config.NameImg ) {
			g_free( Config.NameImg );
			Config.NameImg = NULL;
		}
		Config.NameImg = g_strdup( pochetxt_get_ptr_entry_name_file_to_save() );
		poche_set_flag_buttons();
	}
}
void on_entry_name_save_file_realize( GtkWidget *widget, gpointer user_data )
{
	view.AdrEntryNameFileSave = widget;
	gtk_entry_set_text( GTK_ENTRY(widget), Config.NameImg );
}

// 
// NAME SEARCH ON WEB
// 
gchar *pochetxt_get_ptr_entry_img_web( void )
{
	if( NULL != view.AdrEntrySearchImgWeb ) {
		return( (gchar *)gtk_entry_get_text (GTK_ENTRY (view.AdrEntrySearchImgWeb)));
	}
	return( NULL );
}
void on_entry_img_web_changed( GtkEditable *editable, gpointer user_data )
{
	if( NULL != view.AdrEntrySearchImgWeb ) {
		poche_set_flag_buttons ();
	}
}
void on_entry_img_web_realize( GtkWidget *widget, gpointer user_data )
{
	view.AdrEntrySearchImgWeb = widget;
}

// 
// LE TITRE S'AFFICHE DE FACON PERPENDICULARE SUR LES COTES DE LA POCHETTE
// 
void on_entry_titre_changed( GtkEditable *editable, gpointer user_data )
{
	if( NULL != (gchar *)gtk_entry_get_text (GTK_ENTRY (editable))) {
		poche_set_texte_title( (gchar *)gtk_entry_get_text (GTK_ENTRY (editable)) );
	}
}
void on_entry_titre_realize( GtkWidget *widget, gpointer user_data )
{
	view.AdrEntryTitleCD = widget;
}

// 
// INSERTION D UNE NOUVELLE BOITE DE TEXTE
// 
void on_button_text_add_clicked( GtkButton *button, gpointer user_data )
{
	IMAGE		*Image = NULL;
	
	Image = poche_add_to_glist( NULL, 100, 100, TRUE, _TYPE_TEXT_ );
	poche_set_selected_flag_image( Image );
	gtk_widget_queue_draw( view.AdrDrawingarea );
	pochetxt_set_text_to_textview( Image );
	// DEBUG CDDB_CURL
	pochetxt_set_combobox_choice( Image->PosCombobox );
	poche_set_flag_buttons();
}

// 
// IMPORTATION DU TITRE DU CD
// 
void on_button_import_title_cd_clicked( GtkButton *button, gpointer user_data )
{
	gchar	*PtrTitleCd = CdCurl_get_title_cd();
	
	if( NULL != PtrTitleCd ) {
		gtk_entry_set_text( GTK_ENTRY(view.AdrEntryTitleCD), PtrTitleCd );
	}
}

// 
// IMPORTATION DEPUIS LE WEB SELON LE CRITERE DE RECHERCHE
// 
void on_button_import_img_web_clicked( GtkButton *button, gpointer user_data )
{
	pocheweb_get();
}

// 
// IMPORTATION DES DATAS DU CD
// 
void pochetxt_set_text_from_cd( gchar *p_buffer, gint p_PosCombobox )
{
	IMAGE	*Image = poche_get_struct_selected_is_txt();
	
	if( NULL != Image ) {
		if( NULL != Image->Texte ) {
			g_free( Image->Texte );
			Image->Texte = NULL;
		}
		Image->PosCombobox = p_PosCombobox;
		/*Image->Texte = g_strdup(
					"01 california ][ 05:01\n"
					"02 XXL ][ 04:28\n"
					"03 l'instant X ][ 04:45\n"
					"04 comme j'ai mal ][ 03:57\n"
					"05 rever ][ 05:25\n"
					"06 l'âme-stram-gram ][ 04:26\n"
					"07 je te rends ton amour ][ 05:13\n"
					"08 souviens toi du jour ][ 05:01\n"
					"09 optimistique moi ][ 04:23\n"
					"10 innamoramento ][ 05:15\n"
					"11 l'histoire dune fee , c'est... ][ 05:02\n"
					);*/
		Image->Texte = g_strdup( p_buffer );
		pochetxt_set_text_to_textview( Image );
		gtk_widget_queue_draw( view.AdrDrawingarea );
	}
}

// 
// CHOIX DE LA FONTE
// 
void on_button_change_font_clicked( GtkButton *button, gpointer user_data )
{
	IMAGE	*Image = NULL;
	
	if( NULL != ( Image = (IMAGE *)poche_get_struct_selected_is_txt())) {
	
		GtkResponseType	result;
		GtkWidget	*dialog = NULL;
		gchar		*Ptr = NULL;
		gchar		*Font = g_strdup_printf( "%s %d", Image->FontName, Image->SizeFontName );
	
		dialog = gtk_font_chooser_dialog_new( "Select Font", GTK_WINDOW(WindMain) );
		gtk_font_chooser_set_font( GTK_FONT_CHOOSER (dialog), Font );
		g_free( Font );
		Font = NULL;
		result = gtk_dialog_run(GTK_DIALOG(dialog));
		if (result == GTK_RESPONSE_OK || result == GTK_RESPONSE_APPLY) {

			if( NULL != Image->FontName ) {
				g_free( Image->FontName );
				Image->FontName = NULL;
			}
			Image->FontName  = g_strdup( gtk_font_chooser_get_font( GTK_FONT_CHOOSER (dialog) ));
			
			// LE NOM DE LA POLICE: FontName
			Ptr = Image->FontName;
			while( *Ptr ++ );
			Ptr --;
			while( *Ptr != ' ' ) Ptr --;
			Ptr ++;
			
			// LA TAILLE DE LA POLICE: SizeFont
			Image->SizeFontName = atoi( Ptr );
			Ptr --;
			*Ptr = '\0';
			
			// FORCER AFFICHAGE
			gtk_widget_queue_draw( view.AdrDrawingarea );
		}
		gtk_widget_destroy(dialog);
	}
}

// 
// COPY BOX TEXT IN TEXTVIEW
// 
void pochetxt_set_text_to_textview( IMAGE *p_Image )
{
	if( _TYPE_TEXT_ == p_Image->TypeImage ) {
		
		GtkTextBuffer	*Buf = NULL;
		GtkTextIter	Start;
		GtkTextIter	End;
		
		view.BoolAccessChanged = FALSE;
		
		// ACQUISITION BUFFER TEXTVIEW
		Buf = gtk_text_view_get_buffer( GTK_TEXT_VIEW(view.AdrTextview));
		gtk_text_buffer_get_bounds( Buf, &Start, &End );
		
		// DESTROY BUFFER TEXTE
		gtk_text_buffer_delete( Buf, &Start, &End );
		
		// AFFICHAGE
		if( NULL == p_Image->Texte )
			gtk_text_buffer_set_text( Buf, "", -1 );
		else	gtk_text_buffer_set_text( Buf, p_Image->Texte, -1 );
		
		// NEW OPTION COMBOBOX
		// DEBUG CDDB_CURL
		pochetxt_set_combobox_choice( p_Image->PosCombobox );
		
		view.BoolAccessChanged = TRUE;
	}
}

// 
// TEXTVIEW
// 
void pochetxt_texte_changed_in_textview (GtkTextBuffer *textbuffer, gpointer user_data)
{
	IMAGE		*Image = NULL;
	
	// SI _TYPE_TEXT_ EN SELECTION
	if( TRUE == view.BoolAccessChanged && NULL != ( Image = poche_get_struct_selected_is_txt())) {
		
		GtkTextBuffer	*Buf = NULL;
		gchar		*PtrTexte = NULL;
		GtkTextIter	Start;
		GtkTextIter	End;
		
		// ACQUISITION BUFFER TEXTVIEW
		Buf = gtk_text_view_get_buffer( GTK_TEXT_VIEW(view.AdrTextview));
		gtk_text_buffer_get_bounds( Buf, &Start, &End );
		
		// ACQUISITION DU TEXTE
		PtrTexte = gtk_text_buffer_get_text (Buf, &Start, &End, FALSE);
		
		// DESTRUCTION BUFFER ACTUEL DE LA BOITE
		if( NULL != Image->Texte ) {
			g_free( Image->Texte );
			Image->Texte = NULL;
		}
		// INSERTION DU NOUVEA TEXTE
		Image->Texte = g_strdup( PtrTexte );
		
		// FORCER AFFICHAGE
		gtk_widget_queue_draw( view.AdrDrawingarea );
	
		g_free( PtrTexte );
		PtrTexte = NULL;
	}
}
void on_textview_poche_realize( GtkWidget *widget, gpointer user_data )
{
	view.AdrTextview = widget;
	g_signal_connect((gpointer) gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget)), "changed", G_CALLBACK (pochetxt_texte_changed_in_textview), NULL);
}


// 
// CALLBACKS
// 

// 
// LES OPTIONS > 0 APPELLES UNE LECTURE SI BESOIN
// 
void on_combobox_choice_get_cd_realize( GtkWidget *widget, gpointer user_data )
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Aucun" );
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Titre" );
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Titre - Temps" );
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "Artiste - Titre - Temps" );
	gtk_combo_box_set_active(GTK_COMBO_BOX (widget), 0 );
	view.AdrComboBoxChoice = GTK_COMBO_BOX (widget);
}
void on_combobox_choice_get_cd_changed( GtkComboBox *combobox, gpointer user_data )
{
	if( NULL != view.AdrComboBoxChoice && TRUE == view.BoolAccessChanged && NULL != view.Buffer_none ) {
	
		gint	NbrGet = gtk_combo_box_get_active (GTK_COMBO_BOX (view.AdrComboBoxChoice));
		
		switch( NbrGet ) {
		case _CDDB_BUFFER_NONE_ :
			pochetxt_set_text_from_cd( view.Buffer_none, NbrGet );
			break;
		case _CDDB_BUFFER_TITLE_ : 
			pochetxt_set_text_from_cd( view.Buffer_title, NbrGet );
			break;
		case _CDDB_BUFFER_TITLE_TIME_ : 
			pochetxt_set_text_from_cd( view.Buffer_title_time, NbrGet );
			break;
		case _CDDB_BUFFER_ARTIST_TITLE_TIME_ : 
			pochetxt_set_text_from_cd( view.Buffer_artist_title_time, NbrGet );
			break;
		}
	}
}
// 
//
void pochetxt_set_combobox_choice( gint p_activate )
{
	if( p_activate > -1 ) {
		view.BoolAccessChanged = FALSE;
		gtk_combo_box_set_active( GTK_COMBO_BOX (view.AdrComboBoxChoice), p_activate );
		view.BoolAccessChanged = TRUE;
	}
}








