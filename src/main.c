 /*
 *  file      : main.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
// /usr/include/gtk-2.0/gdk/gdkkeysyms.h
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/types.h>
#include <locale.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "options.h"
#include "configuser.h"
#include "file.h"
#include "notify_send.h"
#include "parse.h"
#include "scan.h"
#include "cd_audio.h"
#include "dvd.h"
#include "web.h"
#include "poche.h"
#include "win_treeview.h"
#include "get_info.h"
#include "mplayer.h"
#include "win_about.h"
#include "split.h"
#include "alsa_play.h"
#include "win_info.h"
#include "statusbar.h"
#include "cd_curl.h"

// FIXME
// TODO

GtkBuilder	*GtkXcfaProjet = NULL;
GtkWidget	*WindMain = NULL;
HOST_CONF	 HostConf;
GtkWidget	*AdrLabelStatusbarGlobal = NULL;


KEYS keys = {
	FALSE,		// gboolean	BoolGDKPress
	FALSE,		// gboolean	BoolGDK_KEY_Control_L_R
	FALSE,		// gboolean	BoolGDK_Control_A
	0,		// guint	keyval
};


//
//
void on_label_statusbar_global_realize (GtkWidget *widget, gpointer user_data)
{
	AdrLabelStatusbarGlobal = widget;
}
// -RECUPERATION DES DATAS
// -SAUVEGARDE DES DATAS
//
void remove_memory_apply (void)
{
	static gboolean BoolDoEnterRemove = FALSE;

	if (TRUE == BoolDoEnterRemove) return;
	BoolDoEnterRemove = TRUE;

	VERBOSE ("\n--------------------------------------------------\n");
	PRINT_FUNC_LF();
	VERBOSE ("--------------------------------------------------\n");

	VERBOSE("AlsaPlay_stop ()\n")
	AlsaPlay_stop ();

	VERBOSE ("split_set_stop ()\n");
	split_set_stop ();

	VERBOSE ("Recuperation position et taille de la fenetre\n");
	gdk_window_get_root_origin( gtk_widget_get_window(WindMain), &Config.WinPos_X, &Config.WinPos_Y );
	Config.WinWidth  = gdk_window_get_width( gtk_widget_get_window(WindMain) );
	Config.WinHeight = gdk_window_get_height( gtk_widget_get_window(WindMain) ) ;

	VERBOSE ("Sauvegarde l'etat de l'expander CD\n");
	Config.BoolEtatExpanderCd = gtk_expander_get_expanded (GTK_EXPANDER (var_cd.Adr_Expander));

	VERBOSE ("Sauvegarde de la configuration\n");
	config_save ();

	VERBOSE ("config_remove ()\n");
	config_remove ();

	VERBOSE ("fileanalyze_remove_entetefile ()\n");
	fileanalyze_remove_entetefile ();

	VERBOSE ("dvdaudio_remove_GtkTree ()\n");
	dvd_remove_GtkTree ();
	VERBOSE ("dvdread_remove_list ()\n");
	dvdread_remove_list ();

	VERBOSE ("cdaudio_deallocate_glist ()\n");
	cdaudio_deallocate_glist ();

	VERBOSE ("NotifySend_remove ()\n");
	NotifySend_remove ();

	VERBOSE ("Parse_remove (PARSE_TYPE_STOCKAGE_CD)\n");
	Parse_remove (PARSE_TYPE_STOCKAGE_CD, TRUE);
	VERBOSE ("Parse_remove (PARSE_TYPE_TITLE_CD)\n");
	Parse_remove (PARSE_TYPE_TITLE_CD, TRUE);

	VERBOSE ("poche_remove_view()\n");
	poche_remove_view();

	VERBOSE ("poche_remove_ListImage()\n");
	poche_remove_ListImage();

	VERBOSE ("scan_remove_glist_media ()\n");
	scan_remove_glist_media ();

	VERBOSE ("web_remove_temporary_rep ()\n");
	web_remove_temporary_rep ();

	VERBOSE ("CdCurl_remove_struct_all ()\n");
	CdCurl_remove_struct_all();

	VERBOSE ("mplayer_remove_list_args ()\n");
	mplayer_remove_list_args ();

	VERBOSE ("SplitSpectre_remove ()\n");
	SplitSpectre_remove ();

	VERBOSE ("AlsaPlay_remove ()\n");
	AlsaPlay_remove ();

	if (TRUE == libutils_test_file_exist (SPLIT_FILE_TMP_WAV)) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("g_unlink (%s)\n", SPLIT_FILE_TMP_WAV);
		g_unlink (SPLIT_FILE_TMP_WAV);
	}
	VERBOSE ("StatusBar_remove ()\n");
	StatusBar_remove();

	Config.PathPochette = libutils_remove_temporary_rep( Config.PathPochette );

	if( NULL != Config.PathLoadImg ) {
		g_free( Config.PathLoadImg );
		Config.PathLoadImg = NULL;
	}
	if( Config.PathSaveImg ) {
		g_free( Config.PathSaveImg );
		Config.PathSaveImg = NULL;
	}

	g_print ("\n");
}
//
//
void on_notebook_general_switch_page( GtkNotebook *notebook, gpointer page, guint page_num, gpointer user_data )
{
	if (TRUE == Config.BoolConfigOk) {
		Config.NotebookGeneral = page_num;

		if (NOTEBOOK_SPLIT == Config.NotebookGeneral) {
			split_set_name_file ();
		}
		else {
			StatusBar_puts();
		}
	}
}
//
//
void on_notebook_expander_cd_switch_page( GtkNotebook *notebook, gpointer page, guint page_num, gpointer user_data )
{
	if (TRUE == Config.BoolConfigOk)
		Config.NotebookExpanderCd = page_num;
}
//
//
void on_notebook_in_file_switch_page( GtkNotebook *notebook, gpointer page, guint page_num, gpointer user_data )
{
	if (TRUE == Config.BoolConfigOk) {
		Config.NotebookFile = page_num;
		switch (Config.NotebookFile) {
		case NOTEBOOK_FICHIERS_CONVERSION :
			gtk_combo_box_text_remove( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), 2 );
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileAll);
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_CONVERSION ]);
			break;
		case NOTEBOOK_FICHIERS_WAV :
			gtk_combo_box_text_remove( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), 2 );
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileWav);
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_WAV ]);
			break;
		case NOTEBOOK_FICHIERS_MP3OGG :
			gtk_combo_box_text_remove( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), 2 );
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileMp3Ogg);
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_MP3OGG ]);
			break;
		case NOTEBOOK_FICHIERS_TAGS :
			gtk_combo_box_text_remove( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), 2 );
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileTags);
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_TAGS ]);
			break;
		}
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "" );
		StatusBar_puts();
	}
}
//
//
void on_notebook_options_switch_page( GtkNotebook *notebook, gpointer page, guint page_num, gpointer user_data )
{
	if (TRUE == Config.BoolConfigOk)
		Config.NotebookOptions = page_num;
}
//
//
void on_notebook_app_externes_switch_page( GtkNotebook *notebook, gpointer page, guint page_num, gpointer user_data )
{
	if (TRUE == Config.BoolConfigOk)
		Config.NotebookAppExterns = page_num;
}
//
//
gboolean on_wind_main_key_press_event( GtkWidget *widget, GdkEventKey *kevent, gpointer user_data )
{
	keys.BoolGDKPress = TRUE;
	keys.keyval       = kevent->keyval;

	if( kevent->keyval == GDK_KEY_Control_L || kevent->keyval == GDK_KEY_Control_R ) {
		keys.BoolGDK_Control_L_R = TRUE;
	}
	else if( kevent->state == 20 || kevent->state == 21 ) {
		if( kevent->keyval == 65 || kevent->keyval == 97 ) {
			keys.BoolGDK_Control_A = TRUE;
		}
	}

	if( NOTEBOOK_POCHETTE == Config.NotebookGeneral ) {
		if( TRUE == keys.BoolGDK_Control_L_R ) {
			pochedir_set_ctrl( TRUE );
		}
		else if( kevent->keyval == GDK_KEY_Delete) {
			// poche_remove_image();
		}
	}
	else if( NOTEBOOK_SPLIT == Config.NotebookGeneral ) {
		if( kevent->keyval == GDK_KEY_Delete ) {
			SplitSelector_cut ();
		}
		else if( kevent->keyval == GDK_KEY_space ) {
			// Une action sur la barre espace agit sur sur le play/pause
			split_play ();
		}
	}

	return FALSE;
}
//
//
gboolean on_wind_main_key_release_event( GtkWidget *widget, GdkEventKey *kevent, gpointer user_data )
{
	keys.BoolGDKPress        = FALSE;
	keys.BoolGDK_Control_L_R = FALSE;
	keys.BoolGDK_Control_A   = FALSE;
	keys.keyval              = 0;

	if( NOTEBOOK_POCHETTE == Config.NotebookGeneral ) {
		pochedir_set_ctrl( FALSE );
	}

	return FALSE;
}

gboolean on_wind_main_window_state_event( GtkWidget *widget, GdkEventWindowState *event, gpointer ser_data )
{
	/*
	typedef enum
	{
	  GDK_WINDOW_STATE_WITHDRAWN  = 1 << 0,
	  GDK_WINDOW_STATE_ICONIFIED  = 1 << 1,
	  GDK_WINDOW_STATE_MAXIMIZED  = 1 << 2,
	  GDK_WINDOW_STATE_STICKY     = 1 << 3,
	  GDK_WINDOW_STATE_FULLSCREEN = 1 << 4,
	  GDK_WINDOW_STATE_ABOVE      = 1 << 5,
	  GDK_WINDOW_STATE_BELOW      = 1 << 6
	} GdkWindowState;
	*/
	if( GDK_WINDOW_STATE_MAXIMIZED == event->new_window_state ) {
		gdk_window_maximize( gtk_widget_get_window(WindMain) );
	}
	return FALSE;
}

//
//
gboolean on_wind_main_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	remove_memory_apply ();
	gtk_main_quit ();
	return FALSE;
}
//
//
gboolean on_wind_main_destroy_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	remove_memory_apply ();
	gtk_main_quit ();
	return FALSE;
}
//
//
void on_quitter1_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	remove_memory_apply ();
	gtk_main_quit ();
}
//
//
void on_a_propos1_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	gchar	*StrAboutXcfa = g_strdup_printf ("About XCFA  -  %s", VERSION);
	// WinAbout_open (StrAboutXcfa);
	wind_about_init( WindMain );
	g_free (StrAboutXcfa);
	StrAboutXcfa = NULL;
}
//
//
void on_show_fields_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	wintreeview_init( WindMain );
}
/* Install un 'handler' sur une mauvaise allocation memoire
*  --
*  entree : int sig :
*  retour : -
*/
void segfault_handler(int sig)
{
	remove_memory_apply ();

	g_print ("\n=========================================================\n");
	g_print ("=  Segmentation fault\n");
	g_print ("=  Adressage memoire invalide\n");
	g_print ("=-------------------------------------------------------=\n");
	g_print ("=      You've probably found a bug in XCFA\n");
	g_print ("=      Vous avez probablement trouve un bug dans XCFA\n");
	g_print ("=========================================================\n\n");

	exit (EXIT_FAILURE);
}

//
//
void AnalyseCommandLine_set_argv( gint argc, gchar *argv[] )
{
	gint	 i;

	if( optind < argc ) {

		for( i = optind; i < argc; i++ ) {
			// VERBOSE MODE
			if( 0 == strcmp( argv[i], "--verbose" )) {
				OptionsCommandLine.BoolVerboseMode = TRUE;
				// g_print( "\nVERBOSE MODE:\n");
				// g_print( "	argv[ %d ] = %s\n", i, argv[ i ] );
				// g_print( "	BoolVerboseMode = %s\n", OptionsCommandLine.BoolVerboseMode ? "TRUE" : "FALSE" );
				// g_print( "\n" );
			}
			// VERSION MODE
			else if( 0 == strcmp( argv[i], "--version" )) {
				OptionsCommandLine.BoolVersionMode = TRUE;
				// g_print( "\nVERSION MODE:\n");
				// g_print( "	argv[ %d ] = %s\n", i, argv[ i ] );
				// g_print( "	OptionsCommandLine.BoolVersionMode = %s\n", OptionsCommandLine.BoolVersionMode ? "TRUE" : "FALSE" );
				// g_print( "\n" );
			}
			// HELP MODE
			else if( 0 == strcmp( argv[i], "--help" ) || 0 == strcmp( argv[i], "-h" )) {
				OptionsCommandLine.BoolHelpMode = TRUE;
			}
		}
	}
}
/* gint main (gint argc, gchar *argv[], gchar **envp)
 *
 * argc  designe le nombre d'arguments transmis au moment du lancement de l'executable
 * argv  designe le vecteur contenant les differents arguments
 * envp  designe le vecteur contenant les informations sur l'environnement
 */
gint main (gint argc, gchar *argv[], gchar **envp)
{
	GError	*p_err = NULL;
	gchar	*CurrentDir = g_get_current_dir ();
	gchar	*PathCurrentDir = NULL;
	gchar	*PathShareProjet = NULL;
	gchar	*Path = NULL;
	gchar	*PackageVersion = NULL;

	/* Init type system as soon as possible */
	// g_type_init ();
	g_set_prgname ("xcfa");

	PRINT_FUNC_LF();
	g_print ("\n");
	g_print ("***************************\n");
	g_print ("%s %s\n", PACKAGE, VERSION);
	g_print ("***************************\n");
	g_print ("%s %s compiled at %s %s\n", PACKAGE_NAME, VERSION, __DATE__, __TIME__);
	g_print ("Compiler: gcc %s\n", __VERSION__);
	g_print ("***************************\n");
	g_print ("Gtk version  = %d.%d.%d\n", GTK_MAJOR_VERSION, GTK_MICRO_VERSION, GTK_MINOR_VERSION);
	g_print ("Glib version = %d.%d.%d\n", GLIB_MAJOR_VERSION, GLIB_MICRO_VERSION, GLIB_MINOR_VERSION);
	g_print ("***************************\n\n");

	// ANALYSE UNIQUEMENT LES OPTIONS
	AnalyseCommandLine_set_argv( argc, &argv[0] );

	if( TRUE == OptionsCommandLine.BoolVersionMode || TRUE == OptionsCommandLine.BoolHelpMode ) {
		if( TRUE == OptionsCommandLine.BoolHelpMode ) {
			g_print( "HELP MODE:\n");
			g_print( "	--help -h	Displays this help section and exits\n" );
			g_print( "	--version	Displays the version of the program and exits\n" );
			g_print( "	--verbose	Activate verbose mode\n" );
		}
		g_print( "\n" );
		g_unlink( SPLIT_FILE_TMP_WAV );
		return( EXIT_SUCCESS );
	}

#ifdef ENABLE_NLS
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("ENABLE_NLS OK\n");
		g_print ("\tGETTEXT_PACKAGE    = %s\n", GETTEXT_PACKAGE);
		g_print ("\tLOCALE_DIR         = %s\n", LOCALE_DIR);
	}

	setlocale( LC_ALL, "" );
	bindtextdomain (GETTEXT_PACKAGE, LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#else
	// g_error ("ENABLE_NLS IS NOT OK\n\n");
#endif

	GetInfo_cpu_print ();

	// IF Segmentation fault
	signal (SIGSEGV, segfault_handler);

	config_read();

	setlocale( LC_ALL, "" );
	gtk_init( &argc, &argv );

	if (NULL == (GtkXcfaProjet = gtk_builder_new ())) {
		return (EXIT_FAILURE);
	}
	gtk_builder_set_translation_domain (GtkXcfaProjet, NULL);

	PathCurrentDir = g_strdup_printf ("%s/glade/%s", CurrentDir, PACKAGE_NAME_GLADE);
	g_free (CurrentDir);
	CurrentDir = NULL;
	// PathShareProjet = g_strdup_printf ("%s/%s/glade/%s", DATA_DIR, PACKAGE_NAME, PACKAGE_NAME_GLADE);
	PathShareProjet = xdg_search_data_xdg( PACKAGE_NAME_GLADE );

	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print("\n");
		g_print("!---\n");
		g_print("! File XML: [ %s ] %s\n", PathCurrentDir != NULL ? PathCurrentDir : PACKAGE_NAME_GLADE, g_file_test (PathCurrentDir, G_FILE_TEST_EXISTS) ? "Found" : "Not Found !!!" );
		g_print("!---\n");
		g_print("! File XML: [ %s ] %s\n", PathShareProjet != NULL ? PathShareProjet : PACKAGE_NAME_GLADE, g_file_test (PathShareProjet, G_FILE_TEST_EXISTS) ? "Found" : "Not Found !!!" );
		g_print("!---\n");
	}

	if (FALSE == g_file_test (PathCurrentDir, G_FILE_TEST_EXISTS) && FALSE == g_file_test (PathShareProjet, G_FILE_TEST_EXISTS)) {
		g_free (PathCurrentDir);
		PathCurrentDir = NULL;
		g_free (PathShareProjet);
		PathShareProjet = NULL;

		return (EXIT_FAILURE);
	}
	else if (TRUE == g_file_test (PathCurrentDir, G_FILE_TEST_EXISTS)) {

		Path = PathCurrentDir;
	}
	else if (TRUE == g_file_test (PathShareProjet, G_FILE_TEST_EXISTS)) {

		Path = PathShareProjet;
	}
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print("\nPATHNAME FILE %s: %s\n\n", PACKAGE_NAME_GLADE, Path);
	if (gtk_builder_add_from_file (GtkXcfaProjet, Path, &p_err) <= 0) {
		g_warning ("Couldn't load builder file: %s", p_err->message);
		g_error_free (p_err);

		g_free (PathCurrentDir);
		PathCurrentDir = NULL;
		g_free (PathShareProjet);
		PathShareProjet = NULL;
		Path = NULL;

		return (EXIT_FAILURE);
	}
	g_free (PathCurrentDir);
	PathCurrentDir = NULL;
	g_free (PathShareProjet);
	PathShareProjet = NULL;
	Path = NULL;

	gtk_builder_set_translation_domain  (GtkXcfaProjet, setlocale( LC_ALL, "" ) );
	gtk_builder_connect_signals (GtkXcfaProjet, NULL);
	WindMain = GTK_WIDGET (GLADE_GET_OBJECT("wind_main"));

	// REDIMENSIONNER LA FENETRE
	gtk_window_resize(GTK_WINDOW(WindMain), Config.WinWidth, Config.WinHeight);
	// PLACER LA FENETRE
	gtk_window_move(GTK_WINDOW(WindMain), Config.WinPos_X, Config.WinPos_Y);

	PackageVersion = g_strdup_printf ("X Convert File Audio  -  %s", VERSION);
	gtk_window_set_title (GTK_WINDOW (WindMain), PackageVersion);
	g_free (PackageVersion);
	PackageVersion = NULL;

	libutils_set_default_icone_to_win (WindMain);

	gtk_widget_show (WindMain);

	// NOTEBOOK
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_PRGEXTERNES);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_app_externes")), NOTEBOOK_OPTIONS_EXTRA);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_app_externes")), NOTEBOOK_OPTIONS_PRGEXTERNES);

	// OPTIONS
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_OPTIONS);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_EXPORT_TAGS);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_AACPLUSENC);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_FAAC);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_MUSEPACK);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_WAVPACK);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_MAC);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_FLAC);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_OGGENC);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_LAME);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_CD_AUDIO);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), NOTEBOOK_OPTIONS_GENERAL);

	// POCHETTE
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_POCHETTE);

	// SPLIT
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_SPLIT);

	// FICHIERS
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_FICHIERS);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_in_file")), NOTEBOOK_FICHIERS_TAGS);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_in_file")), NOTEBOOK_FICHIERS_MP3OGG);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_in_file")), NOTEBOOK_FICHIERS_WAV);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_in_file")), NOTEBOOK_FICHIERS_CONVERSION);

	// CD-AUDIO
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_CD_AUDIO);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_expander_cd")), NOTEBOOK_CD_AUDIO_CUE);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_expander_cd")), NOTEBOOK_CD_AUDIO_TITRE_CD);
		gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_expander_cd")), NOTEBOOK_CD_AUDIO_TAGS);

	// DVD-AUDIO
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), NOTEBOOK_DVD_AUDIO);

	var_options.ColorLineCommand = COLOR_INIT;
	options_set_all_interne ();
	var_options.ColorLineCommand = COLOR_NONE;

	Config.BoolConfigOk = TRUE;

	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_general")), Config.NotebookGeneral);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_expander_cd")), Config.NotebookExpanderCd);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_in_file")), Config.NotebookFile);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_options")), Config.NotebookOptions);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (GLADE_GET_OBJECT("notebook_app_externes")), Config.NotebookAppExterns);
	if (NOTEBOOK_SPLIT == Config.NotebookGeneral) {
		split_set_name_file ();
	}

	cdaudio_set_flag_buttons ();
	file_set_flag_buttons ();
	FileWav_set_flag_buttons ();
	FileMp3Ogg_set_flag_buttons ();
	FileTags_set_flag_buttons ();
	gtk_widget_set_sensitive (GTK_WIDGET (var_file_tags.Adr_table_tag), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_tag_appliquer")), FALSE);

	gtk_widget_hide( GTK_WIDGET (GLADE_GET_OBJECT("frame_discid")));
	
	// TODO: re-encoding unit [ poche_web.c ] function [ void pocheweb_analyze_file_html( void ) ] -> OK
	// gtk_widget_hide( GTK_WIDGET (GLADE_GET_OBJECT("frame_import_img_web")));

	wintreeview_set_etat_fields ();
	wintreeview_radiobutton_set_PosFieldsName ();

	poche_set_flag_buttons();

	// ADMINISTRATOR MODE ?
	if( 0 == getuid() ) {
		wind_info_init (
			WindMain,
			_("ADMINISTRATOR MODE"),
			  "\n",
			_("   You should not use   "),
			  "\n",
			_("  XCFA under ROOT    !"),
			  "\n",
			"");
	}

	xdg_print_list_config_path();
	xdg_print_list_data_path();

	gtk_main ();

	exit(EXIT_SUCCESS);
}


