 /*
 *  file      : scan.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef scan_h
#define scan_h 1


typedef struct {

	gchar *Full_Name;
	gchar *Device;
	gint   Num;
	gint   line;
	gint   type;		// 0 = IDE, 1 = SCSI

} MEDIA;

typedef enum {
	_CD_ = 0,			// 
	_DVD_				// 
} TYPE_READER;

void		scan_remove_glist_media (void);
void		scan_open_peri_cd (void);
void		scan_close_peri_cd (void);
gint		scan_get_nbr_readers_detected (void);
gchar		*scan_get_text_combo_cd (TYPE_READER type_reader);
GList		*scan_get_glist (void);
void		scan_set_bool_scan (gboolean p_flag);
void		scan_eject_media (TYPE_READER TypeReadder);


#endif
