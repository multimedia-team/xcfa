 /*
 *  file      : fileselect.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef fileselect_h
#define fileselect_h 1


typedef enum {

	_PATH_CHOICE_DESTINATION_ = 0,
	_PATH_LOAD_SPLIT_FILE_,
	_PATH_LOAD_FILE_MUSIC_,
	_PATH_LOAD_ONE_FILE_,
	_PATH_LOAD_FILE_ALL_,
	_PATH_LOAD_FILE_WAV_,
	_PATH_LOAD_FILE_MP3OGG_,
	_PATH_LOAD_FILE_TAGS_,
	_PATH_IMPORT_IMAGES_,
	_PATH_STOCKE_IMAGES_POCHETTE_,
	_PATH_DEST_FILE_POSTSCRIPT_,

	_NB_PATH_                       /* Do Not Use This Var, It's reserved */

} TYPE_FILESELECTION;


void	 fileselect_create (TYPE_FILESELECTION Choice, gchar *Path, void *Func_Extern);
void	 fileselect_clear_glist (void);
GList	*fileselect_set_glist_from_str (gchar *p_str);
GList   *fileselect_get_glist (void);


#endif
