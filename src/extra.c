 /*
 *  file      : extra.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "extra.h"




typedef struct {
	GtkWidget	*Adr_entry_navigateur;
	GtkWidget	*Adr_entry_param_navigateur;
	GtkWidget	*Adr_entry_audio;
	GtkWidget	*Adr_entry_param_audio;
	
	gboolean	BoolNavigateurOk;
	gboolean	BoolAudioOk;
} VAR_EXTRA;

VAR_EXTRA VarExtra;




// ENTRY: NAVIGATEUR
// 
void on_entry_navigateur_extra_realize (GtkWidget *widget, gpointer user_data)
{
	VarExtra.Adr_entry_navigateur = widget;
	if (NULL == Config.StringNameNavigateur) {
		Config.StringNameNavigateur = g_strdup ("iceweasel");
	}
	else if ('\0' == *Config.StringNameNavigateur) {
		g_free (Config.StringNameNavigateur);
		Config.StringNameNavigateur = NULL;
		Config.StringNameNavigateur = g_strdup ("iceweasel");
	}
	gtk_entry_set_text (GTK_ENTRY (VarExtra.Adr_entry_navigateur), Config.StringNameNavigateur);
}
// 
// 
void on_entry_navigateur_extra_changed (GtkEditable *editable, gpointer user_data)
{
	if (NULL != VarExtra.Adr_entry_navigateur) {
		
		gchar	*Ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY (VarExtra.Adr_entry_navigateur));
		
		if (TRUE == libutils_find_file (Ptr)) {
			gtk_label_set_markup (GTK_LABEL (GLADE_GET_OBJECT("label_navigateur_extra")), "<span color=\"forestgreen\"><b>Found</b></span>");
			VarExtra.BoolNavigateurOk = TRUE;
		}
		else {
			gtk_label_set_markup (GTK_LABEL (GLADE_GET_OBJECT("label_navigateur_extra")), "<span color=\"red\"><b><i>NOT Found</i></b></span>");
			VarExtra.BoolNavigateurOk = FALSE;
		}
		if (NULL != Config.StringNameNavigateur) {
			g_free (Config.StringNameNavigateur);
			Config.StringNameNavigateur = NULL;
		}
		Config.StringNameNavigateur = g_strdup (Ptr);
	}
}
// 
// 
void on_entry_param_navigateur_extra_realize (GtkWidget *widget, gpointer user_data)
{
	VarExtra.Adr_entry_param_navigateur = widget;
	if (NULL == Config.StringParamNameNavigateur) {
		Config.StringParamNameNavigateur = g_strdup ("");
	}
	gtk_entry_set_text (GTK_ENTRY (VarExtra.Adr_entry_param_navigateur),Config.StringParamNameNavigateur);
}
// 
// 
void on_entry_param_navigateur_extra_changed (GtkEditable *editable, gpointer user_data)
{
	if (NULL != VarExtra.Adr_entry_param_navigateur) {
		if (NULL != Config.StringParamNameNavigateur) {
			g_free (Config.StringParamNameNavigateur);
			Config.StringParamNameNavigateur = NULL;
		}
		Config.StringParamNameNavigateur = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY (VarExtra.Adr_entry_param_navigateur)));
	}
}
// 
// 
gchar *extra_get_name_navigateur (void)
{
	return ((gchar *)gtk_entry_get_text (GTK_ENTRY (VarExtra.Adr_entry_navigateur)));
}
// 
// 
gboolean extra_get_navigateur_is_ok (void)
{
	return (VarExtra.BoolNavigateurOk);
}


// ENTRY: LECTEUR AUDIO
// 
void on_entry_lecteur_audio_extra_realize (GtkWidget *widget, gpointer user_data)
{
	VarExtra.Adr_entry_audio = widget;
	if (NULL == Config.StringNameLecteurAudio) {
		Config.StringNameLecteurAudio = g_strdup ("totem");
	}
	else if ('\0' == *Config.StringNameLecteurAudio) {
		g_free (Config.StringNameLecteurAudio);
		Config.StringNameLecteurAudio = NULL;
		Config.StringNameLecteurAudio = g_strdup ("totem");
	}
	gtk_entry_set_text (GTK_ENTRY (VarExtra.Adr_entry_audio),Config.StringNameLecteurAudio);
}
// 
// 
void on_entry_lecteur_audio_extra_changed (GtkEditable *editable, gpointer user_data)
{
	if (NULL != VarExtra.Adr_entry_audio) {
		
		gchar	*Ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY (VarExtra.Adr_entry_audio));
		
		if (TRUE == libutils_find_file (Ptr)) {
			gtk_label_set_markup (GTK_LABEL (GLADE_GET_OBJECT("label_lecteur_audio_extra")), "<span color=\"forestgreen\"><b>Found</b></span>");
			VarExtra.BoolAudioOk = TRUE;
		}
		else {
			gtk_label_set_markup (GTK_LABEL (GLADE_GET_OBJECT("label_lecteur_audio_extra")), "<span color=\"red\"><b><i>NOT Found</i></b></span>");
			VarExtra.BoolAudioOk = FALSE;
		}
		if (NULL != Config.StringNameLecteurAudio) {
			g_free (Config.StringNameLecteurAudio);
			Config.StringNameLecteurAudio = NULL;
		}
		Config.StringNameLecteurAudio = g_strdup (Ptr);
	}
}
// 
// 
void on_entry_param_lecteur_audio_extra_realize (GtkWidget *widget, gpointer user_data)
{
	VarExtra.Adr_entry_param_audio = widget;
	if (NULL == Config.StringParamNameLecteurAudio) {
		Config.StringParamNameLecteurAudio = g_strdup ("");
	}
	gtk_entry_set_text (GTK_ENTRY (VarExtra.Adr_entry_param_audio),Config.StringParamNameLecteurAudio);
}
// 
// 
void on_entry_param_lecteur_audio_extra_changed (GtkEditable *editable, gpointer user_data)
{
	if (NULL != VarExtra.Adr_entry_param_audio) {
		if (NULL != Config.StringParamNameLecteurAudio) {
			g_free (Config.StringParamNameLecteurAudio);
			Config.StringParamNameLecteurAudio = NULL;
		}
		Config.StringParamNameLecteurAudio = g_strdup ((gchar *)gtk_entry_get_text (GTK_ENTRY (VarExtra.Adr_entry_param_audio)));
	}
}
// 
// 
gchar *extra_get_name_lecteur_audio (void)
{
	return ((gchar *)gtk_entry_get_text (GTK_ENTRY (VarExtra.Adr_entry_audio)));
}
// 
// 
gchar *extra_get_param_name_lecteur_audio (void)
{
	return ((gchar *)gtk_entry_get_text (GTK_ENTRY (VarExtra.Adr_entry_param_audio)));
}
// 
// 
gboolean extra_get_param_name_lecteur_audio_is_ok (void)
{
	gchar	*Ptr = (gchar *)gtk_entry_get_text (GTK_ENTRY (VarExtra.Adr_entry_param_audio));
	return (('\0' == *Ptr) ? FALSE : TRUE);
}
// 
// 
gboolean extra_get_lecteur_audio_is_ok (void)
{
	return (VarExtra.BoolAudioOk);
}


