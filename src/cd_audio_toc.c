 /*
 *  file      : cd_audio_toc.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "file.h"
#include "cd_audio_extract.h"
#include "configuser.h"
#include "scan.h"
#include "cd_cue.h"
#include "win_info.h"
#include "win_scan.h"
#include "cd_audio.h"
#include "cd_curl.h"
#include "statusbar.h"
#include "prg_init.h"
#include "cd_curl.h"
#include "options.h"




/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/



/* Activation du thread de lecture du catalogue du cd-audio et
*  des datas WEB si l'utilisateur est connecte
*  --
*  entree : -
*  retour : -
*/
void *cdaudiotoc_thread_do_read_toc_cd (void *data)
{
	var_cd.bool_thread_read_toc_cd = TRUE;
	EnteteCD.Bool_Read_Infos_cd = CdCurl_get_info_cd();
	var_cd.bool_thread_read_toc_cd = FALSE;
	pthread_exit (0);
}
/* Mise a jour des informations du cd-audio
*  --
*  entree :
*      gpointer data :
*  retour :
*      TRUE
*/
static gint cdaudiotoc_timeout_do_read_toc_cd (gpointer data)
{
	if (var_cd.bool_thread_read_toc_cd == FALSE) {

		// FIN du timeout Update_cdaudio_do_read_toc_cd
		g_source_remove (var_cd.handler_timeout_read_toc_cd);

		cdaudio_put_label_duree ("");
		cdaudio_put_label_titre ("");
		if (EnteteCD.Bool_Read_Infos_cd == TRUE) {
			if (EnteteCD.GList_Audio_cd != NULL) {
				cdaudio_affiche_glist_audio ();
				cdaudio_set_flag_buttons ();
				cdaudio_put_label_duree (EnteteCD.StrDureeCd);
				cdaudio_put_label_titre (EnteteCD.TitleCD != NULL ? EnteteCD.TitleCD : "");

				OptionsCd_set_entry_and_label();
				
				/* TO DEBUG : A VERIFIER DES QUE CONNECTION WEB EST OK

				g_print ("\n");
				g_print ("EnteteCD.TitleCD            : %s\n", EnteteCD.TitleCD);
				g_print ("EnteteCD.Title              : %s\n", EnteteCD.Title);
				g_print ("EnteteCD.Artiste            : %s\n", EnteteCD.Artiste);
				g_print ("EnteteCD.StrDureeCd           : %s\n", EnteteCD.StrDureeCd);
				g_print ("EnteteCD.Num_ID_CD          : %lu\n",EnteteCD.Num_ID_CD);
				g_print ("EnteteCD.Str_ID_CD          : %s\n", EnteteCD.Str_ID_CD);
				g_print ("EnteteCD.TotalTracks        : %d\n", EnteteCD.TotalTracks);
				g_print ("EnteteCD.NumGenre           : %d\n", EnteteCD.NumGenre);
				g_print ("EnteteCD.StrGenre           : %s\n", EnteteCD.StrGenre);

				g_print ("GENRE                       : %s\n", cddb_disc_db_genre(EnteteCD.NumGenre));

				g_print ("EnteteCD.NumYear            : %d\n", EnteteCD.NumYear);
				g_print ("EnteteCD.StrYear            : %s\n", EnteteCD.StrYear);

				g_print ("EnteteCD.Bool_Read_Infos_cd : %s\n", EnteteCD.Bool_Read_Infos_cd ? "TRUE" : "FALSE");
				g_print ("EnteteCD.Message            : %s\n", EnteteCD.Message);
				*/
			}
		}
		CdCurl_set_list_discid();
		StatusBar_set_mess( NOTEBOOK_CD_AUDIO, _STATUSBAR_SIMPLE_, EnteteCD.Message);
		StatusBar_puts();
		var_cd.bool_timeout_read_toc_cd = FALSE;
		cd_audio_bool_access_discid( TRUE );
		WindScan_close ();
	}
	return (TRUE);
}
/* Lecture du catalogue du cd-audio
*  --
*  entree : -
*  retour : -
*/
void cdaudiotoc_reffresh_list_cd (void)
{
	pthread_t nmr_tid;
	
	if (var_cd.Adr_Combo_Box == NULL) return;
	
	if( FALSE == PrgInit.bool_cdda2wav ) {
	
		PRINT_FUNC_LF();
		if( FALSE == libutils_find_file( "icedax" ) && FALSE == libutils_find_file( "cdda2wav" )) {
			wind_info_init (
				WindMain,
				_("cdda2wav and icedax NOT FOUND"),
				_("Please, install packages [ icedax, cdda2wav ]"),
			  	"");
		}
		else if( FALSE == libutils_find_file( "icedax" )) {
			wind_info_init (
				WindMain,
				_("icedax NOT FOUND"),
				_("Please, install package [ icedax ]"),
			  	"");
		}
		else if( FALSE == libutils_find_file( "cdda2wav" )) {
			wind_info_init (
				WindMain,
				_("cdda2wav NOT FOUND"),
				_("Please, install package [ cdda2wav ]"),
			  	"");
		}
		return;
	}
	if( FALSE == PrgInit.bool_cdparanoia ) {
		PRINT_FUNC_LF();
		wind_info_init (
			WindMain,
			_("cdparanoia NOT FOUND"),
			_("Please, install package [ cdparanoia ]"),
		  	"");
		return;
	}
	if( FALSE == PrgInit.bool_cd_discid ) {
		wind_info_init (
			WindMain,
			_("cd-discid NOT FOUND"),
			_("Please, install packages [ cd-discid ]"),
		  	"");
		return;
	}
	
	if( SERVER_PROXY_PARAM == Config.ServeurCddb ) {
	
		gboolean BoolAccess = TRUE;
		
		if( '\0' == *Config.entry_proxy_server ) BoolAccess = FALSE;
		if( '\0' == *Config.entry_proxy_port ) BoolAccess = FALSE;
		if( FALSE == BoolAccess ) {
			wind_info_init (
				WindMain,
				_("ERROR datas proxy"),
				_("Please complete all fields proxy"),
			  	"");
			return;
		}
	}
	
	// WindScan_open ("Scan CD", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Scan CD",
		WINDSCAN_PULSE
		);
	WindScan_show_cancel( FALSE );
	WindScan_set_label ("<b><i>Scanning CD ...</i></b>");
	WindScan_set_pulse ();

	EnteteCD.NameCD_Device = scan_get_text_combo_cd (_CD_);

	cdaudio_deallocate_glist_context ();
	cdaudio_deallocate_glist ();
	cdcue_remove_base_ioctl ();
	gtk_list_store_clear (var_cd.Adr_List_Store);

	cdaudio_set_flag_buttons ();
	
	// ACTIVATION de l'update qui attends la fin du thread cd audio
	var_cd.bool_thread_read_toc_cd = TRUE;
	var_cd.bool_timeout_read_toc_cd = TRUE;
	var_cd.handler_timeout_read_toc_cd = g_timeout_add (50, cdaudiotoc_timeout_do_read_toc_cd, 0);
	pthread_create (&nmr_tid, NULL ,(void *)cdaudiotoc_thread_do_read_toc_cd, (void *)NULL);
}
// 
// 
void cdaudiotoc_button_eject_cd_clicked (void)
{
	if (FALSE == libutils_find_file ("eject")) {
		PRINT_FUNC_LF();
		wind_info_init (
			WindMain,
			"eject NOT FOUND !",
			"Please, install package [ eject ]",
		  	"");
		return;
	}	
	cdaudio_deallocate_glist_context ();
	cdaudio_deallocate_glist ();
	cdcue_remove_base_ioctl ();
	gtk_list_store_clear (var_cd.Adr_List_Store);
	cdaudio_set_flag_buttons ();
	scan_eject_media (_CD_);
	
	libcombo_remove_options( GTK_COMBO_BOX(var_cd.Adr_combobox_discid_cd));
	gtk_combo_box_set_active( GTK_COMBO_BOX(var_cd.Adr_combobox_discid_cd), 0 );
	while (gtk_events_pending()) gtk_main_iteration();
	gtk_widget_hide( GTK_WIDGET (GLADE_GET_OBJECT("frame_discid")));
	while (gtk_events_pending()) gtk_main_iteration();
}



