 /*
 *  file      : statusbar.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <sys/time.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "statusbar.h"



typedef struct {

	guint		handler_timeout_conv;
	gboolean	bool_timer_activate;
	gint		NoteBook_which;
	STATUS_BAR_MESS StatusMess;
	struct		timeval start;
	struct		timeval stop;
	
} VAR_STATUSBAR;

VAR_STATUSBAR VarStatusBar = {
			0,	// handler_timeout_conv
			FALSE,	// bool_timer_activate
			0,	// NoteBook_which
			0	// StatusMess
			};



// 
// 
static gint StatusBar_timeout( gpointer data )
{
	gettimeofday( &VarStatusBar.stop, NULL );
	
	if( VarStatusBar.stop.tv_sec - VarStatusBar.start.tv_sec > 3 ) {
		
		g_source_remove( VarStatusBar.handler_timeout_conv );
		
		if( _STATUSBAR_ERR_ == VarStatusBar.StatusMess ) {
			g_free( StatusBar [ VarStatusBar.NoteBook_which ] . MessError );	StatusBar [ VarStatusBar.NoteBook_which ] . MessError = NULL;
		}
		else if( _STATUSBAR_WARN_ == VarStatusBar.StatusMess ) {
			g_free( StatusBar [ VarStatusBar.NoteBook_which ] . MessWarn );		StatusBar [ VarStatusBar.NoteBook_which ] . MessWarn = NULL;
		}
		StatusBar_puts();
		
		VarStatusBar.bool_timer_activate = FALSE;
	}
	return (TRUE);
}
// 
// 
void StatusBar_timer( gint p_which, STATUS_BAR_MESS p_StatusMess )
{
	if( FALSE == VarStatusBar.bool_timer_activate ) {
		VarStatusBar.NoteBook_which      = p_which;
		VarStatusBar.StatusMess          = p_StatusMess;
		VarStatusBar.bool_timer_activate = TRUE;
		gettimeofday( &VarStatusBar.start, NULL );
		VarStatusBar.handler_timeout_conv = g_timeout_add( 2, StatusBar_timeout, 0 );
	}
}
// 
// 
void StatusBar_remove( void )
{
	gint	i;
	
	for( i = NOTEBOOK_DVD_AUDIO; i <= NOTEBOOK_PRGEXTERNES; i ++ ) {
		if( NULL != StatusBar [ i ] . Mess ) {
			g_free( StatusBar [ i ] . Mess );	StatusBar [ i ] . Mess = NULL;
		}
		if( NULL != StatusBar [ i ] . MessWarn ) {
			g_free( StatusBar [ i ] . MessWarn );	StatusBar [ i ] . MessWarn = NULL;
		}
		if( NULL != StatusBar [ i ] . MessError ) {
			g_free( StatusBar [ i ] . MessError );	StatusBar [ i ] . MessError = NULL;
		}
	}
}
// 
// 
void StatusBar_set_mess(
			gint		p_which,	// NOTEBOOK_DVD_AUDIO .. NOTEBOOK_PRGEXTERNES
			STATUS_BAR_MESS	p_StatusMess,	// _STATUSBAR_SIMPLE_ || _STATUSBAR_WARN_ || _STATUSBAR_ERR_
			gchar		*p_mess		// Message
			)
{
	gchar	*Ptr = NULL;
	
	// FREE MEMORY STRING
	switch( p_StatusMess ) {
	case _STATUSBAR_SIMPLE_ :	Ptr = StatusBar [ p_which ] . Mess;		break;
	case _STATUSBAR_WARN_ :		Ptr = StatusBar [ p_which ] . MessWarn;		break;
	case _STATUSBAR_ERR_ :		Ptr = StatusBar [ p_which ] . MessError;	break;
	default :			return;
	}
	if( NULL != Ptr ) {
		g_free( Ptr );	Ptr = NULL;
	}
	
	// SET STRING
	switch( p_StatusMess ) {
	case _STATUSBAR_SIMPLE_ :	StatusBar [ p_which ] . Mess      = g_strdup( p_mess );	break;
	case _STATUSBAR_WARN_ :		StatusBar [ p_which ] . MessWarn  = g_strdup( p_mess );	break;
	case _STATUSBAR_ERR_ :		StatusBar [ p_which ] . MessError = g_strdup( p_mess );	break;
	}
}
// 
// 
gchar *StatusBar_get_mess( gint p_which )
{
	gchar	*Str = NULL;
	
	// _STATUSBAR_ERR_ : RED COLOR
	if( NULL != StatusBar [ p_which ] . MessError ) {
		Str = g_strdup_printf( "<span color=\"red\"><b>%s</b></span>", StatusBar [ p_which ] . MessError );
		return( Str );
	}
	
	// _STATUSBAR_WARN_ : RED COLOR
	else if( NULL != StatusBar [ p_which ] . MessWarn ) {
		Str = g_strdup_printf( "<span color=\"red\"><b>%s</b></span>", StatusBar [ p_which ] . MessWarn );
		StatusBar_timer( p_which, _STATUSBAR_WARN_ );
		return( Str );
	}
	// _STATUSBAR_SIMPLE_ : BLACK COLOR
	else if( NULL != StatusBar [ p_which ] . Mess ) {
		Str = g_strdup_printf( "<span color=\"black\"><b>%s</b></span>", StatusBar [ p_which ] . Mess );
		return( Str );
	}
	return( g_strdup( " " ));
}
// 
// 
void StatusBar_puts( void )
{
	static	gboolean Bool_puts_statusbar_global = FALSE;
	
	if( NULL != AdrLabelStatusbarGlobal && FALSE == Bool_puts_statusbar_global ) {
		
		gchar *NewName = NULL;
		
		// NO ACESS IN PROC
		Bool_puts_statusbar_global = TRUE;
		
		if( NULL != StatusBar_get_mess( Config.NotebookGeneral ))
			NewName = utf8_eperluette_name( StatusBar_get_mess( Config.NotebookGeneral ));
		else	NewName = g_strdup( " " );
		
		// TODO : @Tetsumaki	http://forum.ubuntu-fr.org/viewtopic.php?pid=3889380#p3889380
		// TODO : bug avec:	while (gtk_events_pending()) gtk_main_iteration(); 
		
		gtk_label_set_justify (GTK_LABEL (AdrLabelStatusbarGlobal), GTK_JUSTIFY_CENTER);
		gtk_label_set_use_markup (GTK_LABEL (AdrLabelStatusbarGlobal), TRUE);
		gtk_label_set_markup (GTK_LABEL (AdrLabelStatusbarGlobal), NewName);

		g_free (NewName);	NewName = NULL;
		
		// ACESS IN PROC IS OK
		Bool_puts_statusbar_global = FALSE;
	}
}









