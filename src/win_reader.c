 /*
 *  file      : win_reader.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "mplayer.h"
#include "win_reader.h"



typedef struct {

	GtkWidget		*AdrScale;
	GtkAdjustment	*AdrAdjust;
	GtkWidget		*AdrHscale;
	gboolean		BoolValueChange;
	
} VAR_WINDREADER;

VAR_WINDREADER VarWindReader = {
	NULL,
	NULL,
	NULL,
	FALSE
};


GtkBuilder	*GtkBuilderProjet_wind_reader = NULL;
GtkWidget	*WindMain_wind_reader = NULL;



void WinReader_set_value (gdouble p_value)
{
	gtk_adjustment_set_value (GTK_ADJUSTMENT (VarWindReader.AdrAdjust), p_value);
}
// HSCALE VALUR-CHANGE
// 
gboolean WinReader_value_change (GtkRange *range, GtkScrollType scroll, gdouble value, gpointer user_data)
{
	if (value < 0.0) value = 0.0;
	if (value > 100.0) value = 100.0;

	if (GDK_BUTTON_RELEASE == VarMplayer.Button) {
		mplayer_fifo_seek_with_hundr (value);
		VarMplayer.Button = -1;
	}
	VarMplayer.PercentTempsActuel = value;
	VarWindReader.BoolValueChange = TRUE;
	
	WinReader_set_value (value);
	
	return FALSE;
}
// HSCALE EVENT
// 
gboolean WinReader_event (GtkWidget *treeview, GdkEvent *event, gpointer user_data)
{
	if (GDK_BUTTON_PRESS == event->type) {
		VarMplayer.Button = GDK_BUTTON_PRESS;
	}
	if (GDK_BUTTON_RELEASE == event->type) {
		VarMplayer.Button = GDK_BUTTON_RELEASE;
	}
	return (FALSE);
}
// HSCALE REALISE
// 
void on_hbox_WindReader_realize (GtkWidget *widget, gpointer user_data)
{
	VarWindReader.AdrAdjust = gtk_adjustment_new(
									0.0,	// gdouble value,
									0.0,	// gdouble lower,
									100.0,	// gdouble upper,
									0.001,	// gdouble step_increment,
									1.0,	// gdouble page_increment,
									0.0		// gdouble page_size
									);
	// VarWindReader.AdrAdjust = gtk_adjustment_new (0, 0, 100.0, 0.001, 1, 0);
	VarWindReader.AdrHscale = gtk_scale_new( GTK_ORIENTATION_HORIZONTAL, GTK_ADJUSTMENT(VarWindReader.AdrAdjust) );
	gtk_widget_show (VarWindReader.AdrHscale);
	gtk_scale_set_draw_value (GTK_SCALE (VarWindReader.AdrHscale), FALSE);
	gtk_box_pack_start (GTK_BOX (widget), VarWindReader.AdrHscale, TRUE, TRUE, 0);
	// GTK_WIDGET_UNSET_FLAGS (widget, GTK_CAN_FOCUS);
	gtk_scale_set_value_pos (GTK_SCALE (VarWindReader.AdrHscale), GTK_POS_LEFT);
	g_signal_connect ((gpointer) VarWindReader.AdrHscale, "change_value", G_CALLBACK (WinReader_value_change), NULL);
	g_signal_connect ((gpointer) VarWindReader.AdrHscale, "event",G_CALLBACK (WinReader_event), NULL);
}
// DELETE EVENT
// 
gboolean on_WindReader_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	mplayer_fifo_quit ();
	gtk_widget_destroy( WindMain_wind_reader );
	WindMain_wind_reader = NULL;
	GtkBuilderProjet_wind_reader = NULL;
	return TRUE;
}
// DESTROY EVENT
// 
gboolean on_WindReader_destroy_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	mplayer_fifo_quit ();
	gtk_widget_destroy( WindMain_wind_reader );
	WindMain_wind_reader = NULL;
	GtkBuilderProjet_wind_reader = NULL;
	return TRUE;
}
// PAUSE
// 
void on_button_pause_WindReader_clicked (GtkButton *button, gpointer user_data)
{
	mplayer_fifo_pause ();
	WinReader_set_lecture ();
}
// LECTURE
// 
void on_button_lecture_WindReader_clicked (GtkButton *button, gpointer user_data)
{
	if (TRUE == VarWindReader.BoolValueChange) {
		VarMplayer.Button = -1;
		VarWindReader.BoolValueChange = FALSE;
		mplayer_fifo_seek_with_hundr (VarMplayer.PercentTempsActuel);
	}
	else {
		mplayer_fifo_seek (VarMplayer.PercentTempsActuel);
	}
	WinReader_set_pause ();
}
// QUITTER
// 
void on_button_quitter_WindReader_clicked (GtkButton *button, gpointer user_data)
{
	VarMplayer.BoolIsPause = FALSE;
	mplayer_fifo_seek (VarMplayer.PercentTempsActuel);
	mplayer_fifo_quit ();
	gtk_widget_destroy( WindMain_wind_reader );
	WindMain_wind_reader = NULL;
	GtkBuilderProjet_wind_reader = NULL;
}
// WIN-READER SET-TITLE
// 
void WinReader_set_title (gchar *title)
{
	if (NULL != title) gtk_window_set_title (GTK_WINDOW (WindMain_wind_reader), title);
}
// WIN-READER SHOW-PAUSE & HIDE-LECTURE
// 
void WinReader_set_pause (void)
{
	gtk_widget_hide (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_reader, "button_lecture_WindReader")));
	gtk_widget_show (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_reader, "button_pause_WindReader")));
}
// WIN-READER HIDE-PAUSE & SHOW-LECTURE
// 
void WinReader_set_lecture (void)
{

	gtk_widget_hide (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_reader, "button_pause_WindReader")));
	gtk_widget_show (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_reader, "button_lecture_WindReader")));

}
// WIN-READER CLOSE
// 
void WinReader_close (void)
{
	WinReader_set_lecture ();
	gtk_widget_destroy( WindMain_wind_reader );
	WindMain_wind_reader = NULL;
	GtkBuilderProjet_wind_reader = NULL;
}
// WIN-READER IS OK
// 
gboolean WinReader_is_close (void)
{
	return (WindMain_wind_reader ? FALSE : TRUE);
}
// WIN-READER OPEN
// 
gboolean wind_reader_init( GtkWidget *FromWindMain )
{
	gchar	*PackageVersion = NULL;
	// GtkWidget	*button_quitter_WindReader = NULL;
	// GtkWidget	*button_lecture_WindReader = NULL;
	// GtkWidget	*button_pause_WindReader = NULL;
	// GtkWidget	*hbox_WindReader = NULL;

	if( NULL == (GtkBuilderProjet_wind_reader = Builder_open( "xcfa_win_reader.glade", GtkBuilderProjet_wind_reader ))) {
		return( EXIT_FAILURE );
	}
	// Add language support from glade file
	gtk_builder_set_translation_domain(  GtkBuilderProjet_wind_reader, "WindReader" );
	gtk_builder_connect_signals( GtkBuilderProjet_wind_reader, NULL );

	WindMain_wind_reader = GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_reader, "WindReader" ));
	// button_quitter_WindReader=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_reader, "button_quitter_WindReader" ));
	// button_lecture_WindReader=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_reader, "button_lecture_WindReader" ));
	// button_pause_WindReader=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_reader, "button_pause_WindReader" ));
	// hbox_WindReader=GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_reader, "hbox_WindReader" ));

	if( NULL != FromWindMain ) {
		gtk_window_set_transient_for (GTK_WINDOW(WindMain_wind_reader), GTK_WINDOW(FromWindMain));
		gtk_window_set_modal (GTK_WINDOW(WindMain_wind_reader), TRUE);
	}
	// resize window
	// gtk_window_resize(GTK_WINDOW(WindMain_wind_reader), Config.WinWidth, Config.WinHeight);
	// place the window
	// gtk_window_move(GTK_WINDOW(WindMain_wind_reader), Config.WinPos_X, Config.WinPos_Y);
	// icon apply
	libutils_set_default_icone_to_win( WindMain_wind_reader );

	PackageVersion = g_strdup_printf( "%s  -  %s", PACKAGE_NAME, VERSION );
	gtk_window_set_title( GTK_WINDOW( WindMain_wind_reader ), PackageVersion );
	g_free( PackageVersion );
	PackageVersion = NULL;

	// place your initialization code here

	gtk_widget_show_all( WindMain_wind_reader );
	
	VarWindReader.BoolValueChange = FALSE;
	
	if (LIST_MPLAYER_FROM_DVD == VarMplayer.ListPlayFrom)
			gtk_widget_hide (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_reader, "hbox_WindReader")));
	else	gtk_widget_show (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_reader, "hbox_WindReader")));


	return( TRUE );
}















