 /*
 *  file      : file_mp3ogg_conv.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "options.h"
#include "notify_send.h"
#include "configuser.h"
#include "conv.h"
#include "prg_init.h"
#include "win_scan.h"
#include "win_info.h"
#include "popup.h"
#include "file.h"





gboolean	BoolErrMplayer = FALSE;
gboolean	BoolErrSox = FALSE;

// 
// 
static void filemp3oggconv_thread (void *arg)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	PARAM_FILELC	param_filelc;
	CONV_FIC_MP3OGG	*PConvMp3Ogg;
	gint		pos;
	gchar		**PtrTabArgs = NULL;
	
	g_print ("\n");
	g_print ("!-------------------------------------------------!\n");
	g_print ("! CONVERSION(S) DEPUIS L'ONGLET 'ANALYZE MP3-OGG' !\n");
	g_print ("!-------------------------------------------------!\n");
	g_print ("\n");

	conv.bool_thread_conv  = TRUE;

	list = g_list_first (entetefile);
	while( NULL != list && FALSE == conv.bool_stop ) {

		if (NULL != (detail = (DETAIL *)list->data)) {

			if (FILE_IS_OGG != detail->type_infosong_file_is && FILE_IS_MP3 != detail->type_infosong_file_is) {
				list = g_list_next(list);
				continue;
			}
			if( FALSE == detail->BoolChanged ) {
				list = g_list_next(list);
				continue;
			}
			PConvMp3Ogg = detail->PConvMp3Ogg;
			
			if (FILE_IS_MP3 == detail->type_infosong_file_is) {

				INFO_MP3 *InfoMp3 = (INFO_MP3 *)detail->info;

				/* MPLAYER_MP3_TO_WAV
				*  MPLAYER_WAV_TO_WAV
				*  SOURCE MP3 -> DESTINATION WAV
				*  ------------------------------------
				*/
				if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
					g_print ("SOURCE MP3<%s>\n", detail->namefile);
					g_print ("DEST   WAV<%s>\n", PConvMp3Ogg->destwav);
				}
				
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
				PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
				PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
				PtrTabArgs [ pos++ ] = g_strdup ("-novideo");
				PtrTabArgs [ pos++ ] = g_strdup_printf ("%s", detail->namefile);
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup ("pcm");
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", PConvMp3Ogg->destwav);
				PtrTabArgs [ pos++ ] = g_strdup ("-af");
				PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
				PtrTabArgs [ pos++ ] = g_strdup ("-srate");
				PtrTabArgs [ pos++ ] = g_strdup ("44100");
				PtrTabArgs [ pos++ ] = NULL;
				conv_to_convert( PtrTabArgs, FALSE, MPLAYER_WAV_TO_WAV, "MPLAYER_WAV_TO_WAV");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
				if( TRUE == libutils_test_file_exist( PConvMp3Ogg->destwav )) {
					
					/* SOX wav TO soxwav
					*  RENAME soxwav TO wav
					*/
					if (InfoMp3->mpeg_is != MPEG_1) {

						if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
							g_print ("SOURCE MP3<%s>\n", PConvMp3Ogg->destwav);
							g_print ("DEST   WAV<%s>\n", PConvMp3Ogg->destsox);
						}
						PtrTabArgs = conv_with_sox_get_param( PConvMp3Ogg->destwav, PConvMp3Ogg->destsox, "44100", "2", "16" );
						conv_to_convert( PtrTabArgs, FALSE, SOX_WAV_TO_WAV, "SOX_WAV_TO_WAV");
						PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
						conv_copy_src_to_dest (PConvMp3Ogg->destsox, PConvMp3Ogg->destwav);
					
						// conv.encode_completed ++;
					}
					
					if( TRUE == libutils_test_file_exist( PConvMp3Ogg->destwav ) && libutils_get_size_file( PConvMp3Ogg->destwav ) > 0 ) {
						/* lame SOURCE WAV -> DESTINATION MP3
						*  ----------------------------------
						*/
						if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
							g_print ("SOURCE WAV%s>\n", PConvMp3Ogg->destwav);
							g_print ("DEST   MP3<%s>\n", PConvMp3Ogg->destmp3);
						}
						param_filelc.type_conv            = LAME_WAV_TO_MP3;
						param_filelc.With_CommandLineUser = FALSE;
						param_filelc.filesrc              = PConvMp3Ogg->destwav;
						param_filelc.filedest             = PConvMp3Ogg->destmp3;
						param_filelc.tags                 = InfoMp3->tags;
						param_filelc.cdrom                = NULL;
						param_filelc.num_track            = NULL;
						param_filelc.BoolSetBitrate       = TRUE;
						param_filelc.PtrStrBitrate        = popup_get_param_mp3( detail->Mp3_Debit, detail->Mp3_Mode );

						PtrTabArgs = filelc_get_command_line (&param_filelc);
						conv_to_convert( PtrTabArgs, FALSE, LAME_WAV_TO_MP3, "LAME_WAV_TO_MP3");
						PtrTabArgs = filelc_RemoveTab( PtrTabArgs );

						/* copy SOURCE MP3 -> DESTINATION MP3
						*  ----------------------------------
						*/
						if (FALSE == conv.bool_stop) {
							conv_copy_src_to_dest (PConvMp3Ogg->destmp3, PConvMp3Ogg->destfile);
						}
					}
					else {
						BoolErrSox = TRUE;
					}
				}
				else {
					BoolErrMplayer = TRUE;
				}
			}
			else if (FILE_IS_OGG == detail->type_infosong_file_is) {

				INFO_OGG *InfoOgg = (INFO_OGG *)detail->info;

				/* ogg123 SOURCE OGG -> DESTINATION WAV
				*  ------------------------------------
				*/
				if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
					g_print ("SOURCE MP3<%s>\n", detail->namefile);
					g_print ("DEST   WAV<%s>\n", PConvMp3Ogg->destwav);
				}
				PtrTabArgs = filelc_AllocTabArgs();
				pos = 3;
				PtrTabArgs [ pos++ ] = g_strdup ("mplayer");
				PtrTabArgs [ pos++ ] = g_strdup ("-nojoystick");
				PtrTabArgs [ pos++ ] = g_strdup ("-nolirc");
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup ("pcm");
				PtrTabArgs [ pos++ ] = g_strdup_printf ("%s", detail->namefile);
				PtrTabArgs [ pos++ ] = g_strdup ("-ao");
				PtrTabArgs [ pos++ ] = g_strdup_printf ("pcm:file=%s", PConvMp3Ogg->destwav);
				PtrTabArgs [ pos++ ] = g_strdup ("-af");
				PtrTabArgs [ pos++ ] = g_strdup ("channels=2");
				PtrTabArgs [ pos++ ] = g_strdup ("-srate");
				PtrTabArgs [ pos++ ] = g_strdup ("44100");
				PtrTabArgs [ pos++ ] = NULL;
				conv_to_convert( PtrTabArgs, param_filelc.With_CommandLineUser, MPLAYER_OGG_TO_WAV, "MPLAYER_OGG_TO_WAV");
				PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
								
				if( TRUE == libutils_test_file_exist( PConvMp3Ogg->destwav ) && libutils_get_size_file( PConvMp3Ogg->destwav ) > 0 ) {
					
					/* oggenc SOURCE WAV -> DESTINATION OGG
					*  ----------------------------------
					*/
					if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
						g_print ("SOURCE WAV%s>\n", PConvMp3Ogg->destwav);
						g_print ("DEST   MP3<%s>\n", PConvMp3Ogg->destogg);
					}
					param_filelc.type_conv            = OGGENC_WAV_TO_OGG;
					param_filelc.With_CommandLineUser = FALSE;
					param_filelc.filesrc              = PConvMp3Ogg->destwav;
					param_filelc.filedest             = PConvMp3Ogg->destogg;
					param_filelc.tags                 = InfoOgg->tags;
					param_filelc.cdrom                = NULL;
					param_filelc.num_track            = NULL;
					param_filelc.BoolSetBitrate       = TRUE;
					param_filelc.PtrStrBitrate        = popup_get_param_ogg( detail->Ogg_Debit, detail->Ogg_Managed, detail->Ogg_Downmix );

					PtrTabArgs = filelc_get_command_line (&param_filelc);
					conv_to_convert( PtrTabArgs, FALSE, OGGENC_WAV_TO_OGG, "OGGENC_WAV_TO_OGG");
					PtrTabArgs = filelc_RemoveTab( PtrTabArgs );
				
					/* copy SOURCE OGG -> DESTINATION MP3
					*  ----------------------------------
					*/
					if (FALSE == conv.bool_stop) {
						conv_copy_src_to_dest (PConvMp3Ogg->destogg, PConvMp3Ogg->destfile);
					}
				}
				else {
					BoolErrMplayer = TRUE;
				}
			}

			g_unlink (PConvMp3Ogg->destwav);
			g_unlink (PConvMp3Ogg->destmp3);
			g_unlink (PConvMp3Ogg->destogg);
			g_unlink (PConvMp3Ogg->destsox);
		}
		list = g_list_next(list);
	}

	// close(conv.tube_conv [ 0 ]);
	conv.bool_thread_conv = FALSE;
	pthread_exit(0);
}
// 
// 
static gint filemp3oggconv_timeout (gpointer data)
{
	if (TRUE == conv.bool_percent_conv) {
		gchar	*Str = NULL;
		
		// DEBUG
		if( conv.total_percent > 1.0 ) conv.total_percent = 1.0;
		Str = g_strdup_printf ("%d%%", (int)(conv.total_percent * 100));
		WindScan_set_progress (Str, conv.total_percent);
		g_free (Str);
		Str = NULL;
		conv.bool_percent_conv = FALSE;
	}
	
	else if( FALSE == conv.bool_thread_conv ) {
		
		GList           *list = NULL;
		DETAIL          *detail = NULL;
		CONV_FIC_MP3OGG	*PConvMp3Ogg;

		FileMp3Ogg_change_parameters ();
		
		// FREE STRUCT CONV_FIC_MP3OGG
		list = g_list_first (entetefile);
		while (list) {
			if (NULL != (detail = (DETAIL *)list->data)) {
				if (FILE_IS_OGG == detail->type_infosong_file_is || FILE_IS_MP3 == detail->type_infosong_file_is) {
					
					if( NULL != (PConvMp3Ogg = detail->PConvMp3Ogg)) {
					
						g_free (PConvMp3Ogg->destwav);  PConvMp3Ogg->destwav = NULL;
						g_free (PConvMp3Ogg->destmp3);  PConvMp3Ogg->destmp3 = NULL;
						g_free (PConvMp3Ogg->destogg);  PConvMp3Ogg->destogg = NULL;
						g_free (PConvMp3Ogg->destsox);  PConvMp3Ogg->destsox = NULL;
						g_free (PConvMp3Ogg->destfile); PConvMp3Ogg->destfile = NULL;
					
						g_free (PConvMp3Ogg);
						PConvMp3Ogg = detail->PConvMp3Ogg = NULL;
					}
				}
				if( FILE_IS_OGG == detail->type_infosong_file_is )
				{
					detail->BoolChanged = ( -1 != detail->Ogg_Debit || -1 != detail->Ogg_Managed || -1 != detail->Ogg_Downmix );
				}
				if( FILE_IS_MP3 == detail->type_infosong_file_is )
				{
					detail->BoolChanged = ( -1 != detail->Mp3_Debit || -1 != detail->Mp3_Mode );
				}
			}
			list = g_list_next(list);
		}

		// Delete temporary rep
		if (NULL != conv.TmpRep)  {
			conv.TmpRep  = libutils_remove_temporary_rep (conv.TmpRep);
		}

		g_source_remove (conv.handler_timeout_conv);
		WindScan_close ();
		FileMp3Ogg_set_flag_buttons ();

		if (FALSE == conv.bool_stop) {
			NotifySend_msg (_("XCFA: Bitrate change"), _("Ok"), conv.bool_stop);
		} else {
			NotifySend_msg (_("XCFA: Bitrate change"), _("Stop by user"), conv.bool_stop);
		}
		if( TRUE == BoolErrMplayer ) {
			wind_info_init (
				WindMain,
				_("Error from MPLAYER !"),
				_("You can enable XCFA command line with:         "),
				  "\n",
				_("$ xcfa -verbose"),
				  "\n",
				_("to see the type of error returned by mplayer."),
			  	"");
		
		}
		else if( TRUE == BoolErrSox ) {
			wind_info_init (
				WindMain,
				_("Error from SOX !"),
				_("You can enable XCFA command line with:         "),
				  "\n",
				_("$ xcfa -verbose"),
				  "\n",
				_("to see the type of error returned by sox."),
			  	"");
		
		}
	}

	return (TRUE);
}
// 
// 
void filemp3oggconv_set_flags_before_conversion (void)
{
	GList           *list = NULL;
	DETAIL          *detail = NULL;
	CONV_FIC_MP3OGG	*PConvMp3Ogg;

	// Create tmp rep
	if( NULL == conv.TmpRep )
		conv.TmpRep  = libutils_create_temporary_rep (Config.PathnameTMP, PATH_TMP_XCFA_AUDIOFILEMP3OGG);


	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (FILE_IS_OGG == detail->type_infosong_file_is || FILE_IS_MP3 == detail->type_infosong_file_is) {
					
				// CREATION STRUCTURE
				detail->PConvMp3Ogg = (CONV_FIC_MP3OGG *)g_malloc0 (sizeof (CONV_FIC_MP3OGG));
				PConvMp3Ogg = detail->PConvMp3Ogg;
				
				if( TRUE == detail->BoolChanged ) {

					conv.total_convert += 3;

					if (FILE_IS_MP3 == detail->type_infosong_file_is) {
						INFO_MP3 *info = (INFO_MP3 *)detail->info;
						if (MPEG_1 != info && info->mpeg_is != MPEG_1) conv.total_convert ++;
					}
					
					if (FILE_IS_OGG == detail->type_infosong_file_is) conv.total_convert ++;
					
					PConvMp3Ogg->destwav  = g_strdup_printf ("%s/tmp.wav", conv.TmpRep);
					PConvMp3Ogg->destsox  = g_strdup_printf ("%s/tmp_sox.wav", conv.TmpRep);
					PConvMp3Ogg->destmp3  = g_strdup_printf ("%s/tmp.mp3", conv.TmpRep);
					PConvMp3Ogg->destogg  = g_strdup_printf ("%s/tmp.ogg", conv.TmpRep);

					if (FILE_IS_OGG == detail->type_infosong_file_is) {
						PConvMp3Ogg->destfile = file_get_pathname_dest (detail, "ogg");
					} else {
						PConvMp3Ogg->destfile = file_get_pathname_dest (detail, "mp3");
					}
				}
			}
		}
		list = g_list_next(list);
	}
}
// 
// 
gboolean filemp3oggconv_is_regul_ok (void) {
	GList           *list = NULL;
	DETAIL          *detail = NULL;
	gboolean         bool_mp3 = FALSE;
	gboolean         bool_ogg = FALSE;
	gchar           *str = NULL;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if( TRUE == detail->BoolChanged ) {
				if (FILE_IS_MP3 == detail->type_infosong_file_is)	bool_mp3 = TRUE;
				else if (FILE_IS_OGG == detail->type_infosong_file_is)	bool_ogg = TRUE;
			}
		}
		list = g_list_next(list);
	}

	if (bool_mp3 && bool_ogg) {
		if (FALSE == PrgInit.bool_mplayer || FALSE == PrgInit.bool_lame || FALSE == PrgInit.bool_oggenc)
		str = g_strdup_printf (_("   WARNING : you need to install   %s%s%s"),
					FALSE == PrgInit.bool_mplayer ? " \n   MPLAYER" : "",
					FALSE == PrgInit.bool_lame    ? " \n   LAME"    : "",
					FALSE == PrgInit.bool_oggenc  ? " \n   OGGENC"  : ""
					);
	}
	else if (bool_mp3) {
		if (FALSE == PrgInit.bool_mplayer || FALSE == PrgInit.bool_lame)
		str = g_strdup_printf (_("   WARNING : you need to install   %s%s"),
					FALSE == PrgInit.bool_mplayer ? " \n   MPLAYER" : "",
					FALSE == PrgInit.bool_lame    ? " \n   LAME"    : ""
					);
	}
	else if (bool_ogg) {
		if (FALSE == PrgInit.bool_oggenc)
		str = g_strdup_printf (_("   WARNING : you need to install   %s"),
					FALSE == PrgInit.bool_oggenc ? " \n   OGGENC"  : ""
					);
	}
	if (NULL != str) {
		wind_info_init (
			WindMain,
			_("Package NOT FOUND !"),
			str,
		  	"");
		g_free (str);
		str = NULL;
		return (FALSE);
	}

	return (TRUE);
}
// 
// 
void filemp3oggconv_apply_regul_mp3ogg_by_conv (void)
{
	pthread_t nmr_tid;

	// PRINT_FUNC_LF();

	if (TRUE == filemp3oggconv_is_regul_ok ()) {
		
		// WindScan_open ("Conversions bitrate", WINDSCAN_PULSE);
		wind_scan_init(
			WindMain,
			"Conversions bitrate",
			WINDSCAN_PULSE
			);
		WindScan_set_label ("<b><i>Conversions bitrate ...</i></b>");
	
		BoolErrMplayer = FALSE;
		BoolErrSox     = FALSE;
		
		conv_reset_struct (WindScan_close_request);
		filemp3oggconv_set_flags_before_conversion ();
		conv.bool_thread_conv = TRUE;
 		pthread_create (&nmr_tid, NULL ,(void *)filemp3oggconv_thread, (void *)NULL);
 		conv.handler_timeout_conv = g_timeout_add (100, filemp3oggconv_timeout, 0);
	}
}




