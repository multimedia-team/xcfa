 /*
 *  file    : tags_ape.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "get_info.h"
#include "tags.h"
#include <taglib/tag_c.h>


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

/*****************************************************************************************
APE header that all APE files have in common (old and new)
*****************************************************************************************
struct APE_COMMON_HEADER
{
    char cID[4];                            should equal 'MAC '
    uint16 nVersion;                        version number * 1000 (3.81 = 3810)
};

*****************************************************************************************
APE header structure for old APE files (3.97 and earlier)
*****************************************************************************************
struct APE_HEADER_OLD
{
    char cID[4];                            should equal 'MAC '
    uint16 nVersion;                        version number * 1000 (3.81 = 3810)
    uint16 nCompressionLevel;               the compression level
    uint16 nFormatFlags;                    any format flags (for future use)
    uint16 nChannels;                       the number of channels (1 or 2)
    uint32 nSampleRate;                     the sample rate (typically 44100)
    uint32 nHeaderBytes;                    the bytes after the MAC header that compose the WAV header
    uint32 nTerminatingBytes;               the bytes after that raw data (for extended info)
    uint32 nTotalFrames;                    the number of frames in the file
    uint32 nFinalFrameBlocks;               the number of samples in the final frame
};
*****************************************************************************************/



/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/

INFO_APE *tagsape_remove_info (INFO_APE *info)
{
	if (info) {
		if (NULL != info->time)		{ g_free (info->time); info->time = NULL;	}
		if (NULL != info->size)		{ g_free (info->size);info->size = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}
	return ((INFO_APE *)NULL);
}

INFO_APE *tagsape_get_info (DETAIL *detail)
{
	INFO_APE	*ptrinfo = NULL;
	SHNTAG		*ShnTag = GetInfo_shntool (detail->namefile);
	gint		m;
	gint		s;
	gint		sec;

	ptrinfo = (INFO_APE *)g_malloc0 (sizeof (INFO_APE));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);
	
	ptrinfo->size     = g_strdup (ShnTag->size);
	ptrinfo->SecTime =
	sec = ShnTag->SecTime;
	s = sec % 60; sec /= 60;
	m = sec % 60; sec /= 60;
	if (sec > 0) ptrinfo->time = g_strdup_printf ("%02d:%02d:%02d", sec, m, s);
	else         ptrinfo->time = g_strdup_printf ("%02d:%02d", m, s);

	/*
	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);
	tags_set (namefile, ptrinfo->tags);
	*/
	
	ShnTag = GetInfo_free_shntool (ShnTag);
	
	return (ptrinfo);
}
