 /*
 *  file      : play_file.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "extra.h"
#include "win_info.h"
#include "play_file.h"



typedef struct {
	pthread_t	 nmr_tid;
	gchar		*Playeur;
	gchar		*Param;
	gchar		*PathNameFile;
} VAR_PLAYFILE ;

VAR_PLAYFILE PlayFile = {
		0,		// 
		NULL,		// 
		NULL,		// 
		NULL		// 
		};



// 
// 
// 
void PlayFile_call (void)
{
	pid_t  pid;

	if ((pid = fork ()) == 0) {
		if (NULL != PlayFile.Param && '\0' != *PlayFile.Param) {
			execlp (PlayFile.Playeur,
				PlayFile.Playeur,
				PlayFile.Param,
				PlayFile.PathNameFile,
				NULL);
		}
		else {
			execlp (PlayFile.Playeur,
				PlayFile.Playeur,
				PlayFile.PathNameFile,
				NULL);
		}
		_exit (0);
	}
}
// 
// 
static void PlayFile_thread (void *arg)
{
	PlayFile_call ();
	pthread_exit(0);
}
// 
// 
void PlayFile_play (gchar *PathNameFile)
{
	PRINT_FUNC_LF();
	
	if (FALSE == extra_get_lecteur_audio_is_ok ()) {
		wind_info_init (
			WindMain,
			_("NO audio player present"),
			_("Please install an audio player."),
			  "");
		return;
	}
	
	if (PlayFile.Playeur != NULL) {
		g_free (PlayFile.Playeur);	PlayFile.Playeur = NULL;
	}
	if (PlayFile.Param != NULL) {
		g_free (PlayFile.Param);	PlayFile.Param = NULL;
	}
	if (PlayFile.PathNameFile != NULL) {
		g_free (PlayFile.PathNameFile);	PlayFile.PathNameFile = NULL;
	}
	
	PlayFile.Playeur      = g_strdup (extra_get_name_lecteur_audio ());
	PlayFile.Param        = g_strdup (extra_get_param_name_lecteur_audio ());
	PlayFile.PathNameFile = g_strdup (PathNameFile);
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("\tFILE    : %s\n", PathNameFile);
		g_print ("\tPLAYEUR : %s\n", PlayFile.Playeur);
		g_print ("\tPARAM   : %s\n", PlayFile.Param);
	}
	pthread_create (&PlayFile.nmr_tid, NULL ,(void *)PlayFile_thread, (void *)NULL);
}
















