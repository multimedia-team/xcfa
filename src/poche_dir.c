 /*
 *  file      : poche_dir.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <gdk/gdkkeysyms.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/types.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cursor.h"
#include "win_scan.h"
#include "dragNdrop.h"
#include "popup.h"
#include "statusbar.h"
#include "fileselect.h"
#include "configuser.h"
#include "poche.h"



extern int lstat(const char *file_name, struct stat *buf);


typedef struct {
	
	GList			*ListReceive;		// COPIE DU GSLIST POUR RECHERCHE DES FICHIERS IMAGES UNIQUEMENT QUI SERONT INSCRIT DANS:
	GList			*ListFiles;		// LE NOM DU FICHIER
	
	// RECHERCHE RECURSIVE DES FICHIERS
	// INDIQUE LE NOMBRE TOTAL DANS: VarAnalyze.RecTotalFile
	// 
	gchar			*RecPtrDir;		// POUR WINDSCAN
	gchar			*RecName;		// 
	gint			RecTotalFile;		// 
	gint			RecActivate;		// 
	double			percent;		// 
	
	gboolean		BoolInThread;
	gboolean		BoolSetPercent;
	guint			HandlerTimeout;
	
	gboolean		CancelClicked;
	gboolean		EndThread;
	gboolean		EndTimeout;
	gboolean		Action;
	gchar			*name;
	guint			timeout;
	pthread_t		nmr_tid;
	
	gboolean		BoolIsEnterNotify;	// 
	gboolean		BoolIsCtrl;		// 
	
} VAR_POCHEDIR;

VAR_POCHEDIR	VarPocheDir;



// 
// 
void pochedir_remove_GlistPochette( void )
{
	GList		*list = NULL;
	GLIST_POCHETTE	*GlistPochette = NULL;
        gint		NbrFree = 0;
	
	list = g_list_first( view.glist );
	while (list) {
		if (NULL != (GlistPochette = (GLIST_POCHETTE *)list->data)) {
			if( NULL != GlistPochette->name_img ) {
				g_free( GlistPochette->name_img );
				GlistPochette->name_img = NULL;
			}
			if( NULL != GlistPochette->name_png ) {
				g_free( GlistPochette->name_png );
				GlistPochette->name_png = NULL;
			}
			// GlistPochette->togglebutton
			// if( NULL != GlistPochette->image ) {
			// 	g_object_unref( GlistPochette->image );
			// 	GlistPochette->image = NULL;
			// }
			// if( NULL != GlistPochette->label ) {
			// 	g_free( GlistPochette->label );
			// 	GlistPochette->label = NULL;
			// }
			
			g_free( GlistPochette );
			GlistPochette = list->data = NULL;
			NbrFree ++;
		}
		list = g_list_next (list);
	}
	g_list_free( view.glist );
	view.glist = NULL;
	g_print( "\tRemove = %d\n", NbrFree );
}
// 
// 
void pochedir_make_glist (gchar *filename)
{
	GLIST_POCHETTE	*new = NULL;
	
	// Allocation memoire
	if( NULL == ( new = ( GLIST_POCHETTE * )g_malloc0( sizeof(GLIST_POCHETTE)))) {
		PRINT_FUNC_LF();
		g_print ("\tERREUR D'ALLOCATION\n");
		return;
	}
	// init
	new->name_img         = g_strdup (filename);
	new->BoolStructRemove = FALSE;
	
	// Stocke
	view.glist = g_list_append (view.glist, new);
}
// 
// 
void pochedir_set_in_list (void)
{
	GList		*list = NULL;
	gchar		*PtrName = NULL;
	
	list = g_list_first (VarPocheDir.ListFiles);
	while (list) {
		if (NULL != (PtrName = (gchar *)list->data)) {
			pochedir_make_glist (PtrName);
			VarPocheDir.RecActivate ++;
			VarPocheDir.BoolSetPercent = TRUE;
		}
		list = g_list_next (list);
	}
}
// 
// 
gboolean pochedir_is_dupply (gchar *filename)
{
	GList		*list = NULL;
	GLIST_POCHETTE	*gl = NULL;
	
	return (FALSE);
	list = g_list_first (view.glist);
	while (list) {
		if ((gl = (GLIST_POCHETTE *)list->data)) {
			if (strcmp (gl->name_img, filename) == 0) {
				return (TRUE);
			}
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
// VERIFIE LA VALIDITE DU FICHIER
// 
void pochedir_verif_validity_files (void)
{
	GList		*List = NULL;
	gchar		*Ptr = NULL;	
	
	List = g_list_first (VarPocheDir.ListFiles);
	while (List) {
		
		if (WindScan_close_request () == TRUE) break;
		
		if (NULL != (Ptr = (gchar *)List->data)) {
			if (TRUE == pochedir_is_dupply (Ptr)) {
				g_free (Ptr);
				Ptr = List->data = NULL;
				List = g_list_next (List);
				continue;
			}

			if( FALSE == FileIs_image( Ptr )) {
				g_free (Ptr);
				Ptr = List->data = NULL;
				List = g_list_next (List);
				continue;
			}
			VarPocheDir.RecTotalFile ++;
		}
		List = g_list_next (List);
	}
	VarPocheDir.BoolSetPercent = TRUE;
}
// RECHERCHE RECURSIVE DES FICHIERS
// INDIQUE LE NOMBRE TOTAL DANS: VarPocheDir.RecTotalFile
void pochedir_recherche_recursive_ (gchar *Directory)
{
	DIR    *dp;
	struct  dirent *entry;
	struct  stat statbuf;
	
	if (NULL == (dp = opendir (Directory))) {
		if (TRUE == libutils_test_file_exist (Directory) && libutils_get_size_file (Directory) > 100) {
			VarPocheDir.ListFiles = g_list_append (VarPocheDir.ListFiles, g_strdup (Directory));
			VarPocheDir.RecTotalFile ++;
		}
		return;
	}
		
	chdir (Directory);
	while ((entry = readdir (dp)) != NULL) {
		
		if (WindScan_close_request () == TRUE) break;

		lstat (entry->d_name, &statbuf);
		
		if (S_ISDIR (statbuf.st_mode)) {
			if (strcmp (".", entry->d_name) == 0 || strcmp ("..", entry->d_name) == 0) continue;
			pochedir_recherche_recursive_ (entry->d_name);
		}
		else {
			// VarPocheDir.RecPtrDir = g_get_current_dir ();
			// VarPocheDir.RecPtrDir = get_current_dir_name();
			
			// Comme  une  extension  du  standard POSIX.1-2001, la version  Linux (libc4, libc5, glibc) de getcwd() alloue le
			// tampon dynamiquement avec malloc(3), si buf est NULL. Dans ce cas, le tampon alloué a la longueur size à moins
			// que size soit égal à zéro, auquel cas buf est alloué avec la taille nécessaire.
			// L'appelant doit libérer avec free(3) le tampon renvoyé.
			if ((VarPocheDir.RecPtrDir = getcwd(VarPocheDir.RecPtrDir, 0)) != NULL) {
							
				VarPocheDir.RecName = g_strdup_printf ("%s/%s", VarPocheDir.RecPtrDir, entry->d_name);
				free (VarPocheDir.RecPtrDir);
				VarPocheDir.RecPtrDir = NULL;

				// FICHIER VALIDE ?
				if( TRUE == FileIs_image( VarPocheDir.RecName )) {
					
					// ALIMENTE LA LISTE AVEC LE NOM COMPLET DE FICHIER: PATH + NAMEFILE					
					VarPocheDir.ListFiles = g_list_append (VarPocheDir.ListFiles, VarPocheDir.RecName);
					VarPocheDir.RecTotalFile ++;
				}
				else {
					g_free (VarPocheDir.RecName);
					VarPocheDir.RecName = NULL;
				}
			}
		}
	}
	chdir ("..");
	closedir (dp);
}
// 
// 
static void pochedir_thread_ (void *arg)
{
	gchar		*Ptr = NULL;
	GList		*List = NULL;
	
	VarPocheDir.BoolInThread = TRUE;
	
	//PRINT_FUNC_LF();
	// CONSTRUIT UNE LISTE DE TOUS LES FICHIERS SANS LES DOSSIERS
	List = g_list_first (VarPocheDir.ListReceive);
	while (List) {
		if ((Ptr = (gchar *)List->data) != NULL) {
			// RECHERCHE RECURSIVE DES FICHIERS
			pochedir_recherche_recursive_ (Ptr);
		}
		List = g_list_next (List);
	}
	// SUPPRESISON DE LA COPIE DU GSLIST
	VarPocheDir.ListReceive = libutils_remove_glist (VarPocheDir.ListReceive);

	// VERIFIE LA VALIDITE DU FICHIER
	VarPocheDir.RecActivate = 0;
	VarPocheDir.RecTotalFile = 0;
	pochedir_verif_validity_files ();
	
	// ALIMENTATION DE LA LISTE
	pochedir_set_in_list ();
	
	// REMOVE COPIE DU GLIST
	VarPocheDir.ListFiles = libutils_remove_glist (VarPocheDir.ListFiles);

	VarPocheDir.BoolInThread = FALSE;
	VarPocheDir.BoolSetPercent = FALSE;
	g_print("END THREAD\n");

	pthread_exit(0);
}
// 
// 
void pochedir_togglebutton_clicked (GtkButton *button, gpointer user_data)
{
	GtkToggleButton *W = (GtkToggleButton *)user_data;
	
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(W), FALSE );
}
// 
// 
static void file_drag_data_drop( GtkWidget *widget,
				 GdkDragContext *dc,
				 GtkSelectionData *selection_data,
				 guint info,
				 guint t,
				 gpointer data )
{
	gchar		*Str = (gpointer)data;
	gdouble		x, y;
	gint		X, Y;
	GdkModifierType	state;
	IMAGE		*Image = NULL;
	cairo_t		*cr;
	
	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( gtk_widget_get_window(view.AdrDrawingarea), device, &X, &Y, &state );
	// gdk_window_get_pointer( view.AdrDrawingarea->window, &X, &Y, &state ); 	
	cr = gdk_cairo_create( gtk_widget_get_window(view.AdrDrawingarea) );
	cairo_scale (cr, view.scale, view.scale);
	cairo_translate (cr, view.x0, view.y0);
	x = X;
	y = Y;
	cairo_device_to_user (cr, &x, &y);
	cairo_destroy( cr );
	
	Image = poche_add_to_glist( Str, x, y, TRUE, _TYPE_IMAGE_ );
	poche_set_selected_flag_image( Image );
	gtk_widget_queue_draw( view.AdrDrawingarea );
}
// 
// 
gboolean pochedir_stock_button_press_event( GtkWidget *widget, GdkEventButton *event, gpointer user_data )
{
	GLIST_POCHETTE	*GlistPochette = (GLIST_POCHETTE *)user_data;
	gboolean	bool_click_droit = (event->button == 3);	
	guint		state = event->state;
 	
	// IMAGE A DESSUS AVEC: CTRL + click
	if( state & GDK_CONTROL_MASK ) {
	
		// PRINT("REMOVE IMG WITH: CRTL + CLICK");
		// g_print( "\tIMAGE A SUPPRIMER DE LA LISTE:\n" );
		// g_print( "\t%s\n", GlistPochette->name_img );
		pochedir_destroy_image( GlistPochette );
	}
	else if( TRUE == bool_click_droit ) {
		// PRINT("REMOVE IMG WITH: POPUP");
		// g_print( "IMAGE A SUPPRIMER DE LA LISTE:\n" );
		// g_print( "\t%s\n", GlistPochette->name_img );
		popup_viewport( GlistPochette );
	}
	return( FALSE );
}
// 
// 
void pochedir_destroy_image( GLIST_POCHETTE *gl )
{
	if( NULL != gl->name_img ) {
		g_free( gl->name_img );
		gl->name_img = NULL;
	}
	if( NULL != gl->name_png ) {
		g_free( gl->name_png );
		gl->name_png = NULL;
	}
	if( NULL != gl->togglebutton ) {
		gtk_widget_destroy( gl->togglebutton );
		gl->togglebutton = NULL;
	}
	gl->BoolStructRemove = TRUE;

	StatusBar_set_mess( NOTEBOOK_POCHETTE,  _STATUSBAR_SIMPLE_, "" );
	StatusBar_puts();
	VarPocheDir.BoolIsEnterNotify = FALSE;
	cursor_set_old();
}
// 
// 
void pochedir_set_cursor( void )
{
	if( TRUE == VarPocheDir.BoolIsCtrl && TRUE == VarPocheDir.BoolIsEnterNotify )
		cursor_set_clear();
	else	cursor_set_old();
}
// 
// 
void pochedir_set_ctrl( gboolean p_Bool )
{
	VarPocheDir.BoolIsCtrl = p_Bool;
	pochedir_set_cursor();
}
// 
// 
static gboolean pochedir_enter_notify( GtkWidget *widget, GdkEventCrossing *event, gpointer user_data )
{
	// StatusBar_puts( "<b>Utilisez le glisser / deplacer     -     <i>Effacer une image avec Ctrl + Click</i></b>" );
	StatusBar_set_mess( NOTEBOOK_POCHETTE,  _STATUSBAR_SIMPLE_, _("<b>Utilisez le glisser / deplacer     -     <i>Effacer une image avec Ctrl + Click</i></b>") );
	StatusBar_puts();
	VarPocheDir.BoolIsEnterNotify = TRUE;
	pochedir_set_cursor();
	return FALSE;
}
// 
// 
static gboolean pochedir_leave_notify( GtkWidget *widget, GdkEventCrossing *event, gpointer user_data )
{
	// StatusBar_puts( "" );
	StatusBar_set_mess( NOTEBOOK_POCHETTE,  _STATUSBAR_SIMPLE_, "" );
	StatusBar_puts();
	VarPocheDir.BoolIsEnterNotify = FALSE;
	pochedir_set_cursor();
	return FALSE;
}
// 
// 
void pochedir_table_add_images( void )
{
	GLIST_POCHETTE *gl = NULL;
	GList          *list = NULL;
	gint            row_table = 0;
	GdkPixbuf	*Pixbuf = NULL;
	
	PRINT_FUNC_LF();
	
	// DESTROY WIDGETS
	
	if( NULL != view.Adr_table ) {
		list = g_list_first (view.glist);
		while (list) {
			if ((gl = (GLIST_POCHETTE *)list->data)) {
			
				// NE DETRUIRE QUE LE WIDGET
				if( NULL != gl->togglebutton ) {
					gtk_widget_destroy( gl->togglebutton );
					gl->togglebutton = NULL;
				}
			}
			list = g_list_next (list);
		}
		gtk_widget_destroy( view.Adr_table );
		view.Adr_table = NULL;
	}
	
	// CONSTRUCTION TABLE
	
	// total_row_table = g_list_length (view.glist);

	// view.Adr_table = gtk_grid_new (1, total_row_table, FALSE);
	view.Adr_table = gtk_grid_new();
	// gtk_table_resize(
	// 			view.Adr_table, 	// GtkTable *table,
	// 			total_row_table,	// guint rows,
	// 			1					// guint columns
	// 			);
                  
	gtk_widget_show (view.Adr_table);
	gtk_container_add (GTK_CONTAINER (view.Adr_viewport_image_preview), view.Adr_table);

	list = g_list_first (view.glist);
	while (list) {
		if ((gl = (GLIST_POCHETTE *)list->data)) {
			
			if( TRUE == gl->BoolStructRemove ) {
				list = g_list_next (list);
				continue;
			}
			
			// BUTTON
			gl->togglebutton = gtk_toggle_button_new ();
			gtk_widget_show (gl->togglebutton);
			// gtk_table_attach(
			// 			GTK_TABLE (view.Adr_table),
			// 			gl->togglebutton,
			// 			row_table, row_table +1, 0, 1,
			// 			(GtkAttachOptions) (GTK_FILL),
			// 			(GtkAttachOptions) (0), 0, 0
			// 			);
			gtk_grid_attach(
					GTK_GRID(view.Adr_table),	// GtkGrid *grid,
					gl->togglebutton,				// GtkWidget *child,
					row_table,							// gint left,
					0,							// gint top,
					1,					// gint width,
					1					// gint height
					);
			gtk_widget_set_size_request (gl->togglebutton, SIZE_IMAGE_VIEWPORT, SIZE_IMAGE_VIEWPORT);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gl->togglebutton), FALSE);
			gtk_container_set_border_width (GTK_CONTAINER (gl->togglebutton), 5);

			// IMAGE
			gl->image = gtk_image_new ();
			gtk_widget_show (gl->image);
			gtk_container_add (GTK_CONTAINER (gl->togglebutton), gl->image);
			
			Pixbuf = gdk_pixbuf_new_from_file_at_scale( gl->name_img, SIZE_IMAGE_VIEWPORT, SIZE_IMAGE_VIEWPORT, TRUE, NULL );
			gtk_image_set_from_pixbuf( GTK_IMAGE(gl->image), Pixbuf );
			
			gtk_drag_source_set(
					GTK_WIDGET(gl->togglebutton),
					GDK_BUTTON1_MASK | GDK_BUTTON2_MASK | GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP,
					drag_types,
					n_drag_types,
					GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_DEFAULT
					);
			
			// SIGNALS
			g_signal_connect( G_OBJECT(gl->togglebutton), "drag-data-get",      G_CALLBACK(file_drag_data_drop),               gl->name_img);	
			g_signal_connect( G_OBJECT(gl->togglebutton), "clicked",            G_CALLBACK (pochedir_togglebutton_clicked),    gl->togglebutton);
			g_signal_connect( G_OBJECT(gl->togglebutton), "button-press-event", G_CALLBACK(pochedir_stock_button_press_event), gl);
			g_signal_connect( G_OBJECT(gl->togglebutton), "enter-notify-event", G_CALLBACK (pochedir_enter_notify),            NULL);
			g_signal_connect( G_OBJECT(gl->togglebutton), "leave-notify-event", G_CALLBACK (pochedir_leave_notify),            NULL);
			
			gl->BoolStructRemove = FALSE;
			
			row_table ++;
		}
		list = g_list_next (list);
	}
	gtk_widget_show_all (view.Adr_table);
}
// 
// 
static gint pochedir_timeout_ (gpointer data)
{
	VarPocheDir.CancelClicked = WindScan_close_request ();
	
	if (TRUE == VarPocheDir.BoolSetPercent) {
		gchar	*Str = NULL;
		
		VarPocheDir.BoolSetPercent = FALSE;
		VarPocheDir.percent = (double)VarPocheDir.RecActivate  / (double)VarPocheDir.RecTotalFile;
		Str = g_strdup_printf ("%d%%", (int)(VarPocheDir.percent*100));
		WindScan_set_progress (Str, VarPocheDir.percent);
		g_free (Str);
		Str = NULL;
	}
	else if (FALSE == VarPocheDir.BoolInThread) {
		g_source_remove (VarPocheDir.HandlerTimeout);
		pochedir_table_add_images ();
		WindScan_close ();
		g_print("END TIMEOUT\n");
	}
	return (TRUE);
}
// 
// 
void pochedir_add_img_file (GSList *p_list)
{
	gchar		*Ptr = NULL;
	GSList		*gs_List = p_list;
	pthread_t	nmr_tid_1;
	
	// WindScan_open ("Files scan", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Files scan",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Scan directory, verify and load ...</i></b>");

	// PRINT_FUNC_LF();
	
	// COPIE DE LA GSlist TRANSMISE PAR LE FILESELECT ET QUI DEVRA ETRE SUPRIMEE
	while (gs_List) {
		if ((Ptr = (gchar *)gs_List->data) != NULL) {
			VarPocheDir.ListReceive = g_list_append (VarPocheDir.ListReceive, g_strdup (Ptr));
		}
		gs_List = g_slist_next (gs_List);
	}
	
	VarPocheDir.RecActivate = 0;
	VarPocheDir.RecTotalFile = 0;
	
	// CALL THREAD
	VarPocheDir.BoolInThread = TRUE;
	VarPocheDir.BoolSetPercent = FALSE;
	
	g_print("DEBUT TIMEOUT\n");
	VarPocheDir.HandlerTimeout = g_timeout_add (100, pochedir_timeout_, 0);
	g_print("DEBUT THREAD\n");
	pthread_create (&nmr_tid_1, NULL ,(void *)pochedir_thread_, (void *)NULL);

}
// 
// 
void pochedir_load_file_clicked( gchar *path )
{
	if( NULL != Config.PathSaveImg ) {
		g_free( Config.PathSaveImg );
		Config.PathSaveImg = NULL;
	}
	Config.PathSaveImg = g_strdup( path );
	gtk_button_set_label( GTK_BUTTON(GLADE_GET_OBJECT("button_stock_img")), Config.PathSaveImg );
}
// 
// 
void on_button_stock_img_clicked( GtkButton *button, gpointer user_data )
{
	fileselect_create( _PATH_CHOICE_DESTINATION_, Config.PathSaveImg, pochedir_load_file_clicked );
}




