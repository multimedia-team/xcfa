 /*
 *  file      : get_info.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/utsname.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "prg_init.h"
#include "file.h"
#include "configuser.h"
#include "get_info.h"





#define GET_MAX_CARS 1024


typedef struct {

	pid_t		code_fork;		/* Num Code Fork	*/
	int		signal_numchildren;	/* the signal handler	*/
	int		tube [ 2 ];		/* for pipe		*/
	gboolean	bool_percent;

} VAR_GET;

VAR_GET VarGet;


/*
*---------------------------------------------------------------------------
* SIGNAL
*---------------------------------------------------------------------------
*/
void GetInfo_sigchld (gint signum)
{
        gint status;

        wait(&status);

	/* PRINT_FUNC(); */
        /* if there are still children waiting
        *  re-install the signal handler
	*/
	VarGet.signal_numchildren --;
        if (VarGet.signal_numchildren > 0)
        {
                /* re-install the signal handler */
                signal (SIGCHLD, GetInfo_sigchld);
        }
}
//
//
gint GetInfo_exec_with_output (gchar **args, pid_t *p, gint Streams)
{
	VarGet.signal_numchildren = 0;

	if (pipe (VarGet.tube) != 0)
	{
		fprintf (stderr, "error: pipe\n");
		exit (1);
	}
	if ((*p = fork()) == 0)
	{
		dup2 (VarGet.tube [ 1 ], Streams); /* STDOUT_FILENO STDERR_FILENO */
		close (VarGet.tube [ 1 ]);
		execvp (args[0], args);
		fprintf (stderr, "error: exec");
		exit (2);
	}
	VarGet.signal_numchildren ++;
	signal (SIGCHLD, GetInfo_sigchld);
	close (VarGet.tube [ 1 ]);
	return (VarGet.tube [ 0 ]);
}


/*
*---------------------------------------------------------------------------
* FUNCTION
*---------------------------------------------------------------------------
*/
extern float roundf(float x);

// RETOURNE, EN KiloOctet LA PLACE DISPONIBLE DU DOSSIER TEMPORAIRE
//
glong GetInfo_level_df (void)
{
	gint   pos = 0;
	gchar *args [ 20 ];
	gint   fd;
	gint   size = -1;
	gchar  buf [ 1024 + 10 ];
	gchar *Path = g_strdup_printf ("%s/", Config.PathnameTMP);
	glong  Size = 0;
	gchar *Ptr = NULL;

	args[pos++] = "df";
	args[pos++] = "--block-size=K";
	args[pos++] = Path;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!  NORMALISE -n %s\n", args[ 0 ]);
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= 1024) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= 1024\n", pos);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		if (strstr (buf, "/dev/")) {
			/*
			buf=/dev/sda5            19686772K  4080996K 14605736K  22% /
			*/
			/* g_print("buf=%s\n", buf); */
			Ptr = strchr (buf, 'K');
			Ptr ++;
			Ptr = strchr (Ptr, 'K');
			Ptr ++;
			while (*Ptr == ' ') Ptr ++;
			/* g_print("Ptr  = '%s'\n", Ptr); */
			Size = (glong)atol (Ptr);
			/* g_print("Size = %lu\n", Size); */
		}

	} while (size != 0);

	g_free (Path);
	Path = NULL;

	close(fd);
	VarGet.code_fork = -1;
	return (Size);
}
//
//
gboolean GetInfo_level_bool_percent (void)
{
	return (VarGet.bool_percent);
}
//
//
gint GetInfo_level_get_from (TYPE_FILE_IS TypeFileIs, gchar *file)
{
	gint   pos = 0;
	gchar *args [ 20 ];
	gint   fd;
	gint   size = -1;
	gchar  buf [ 1024 + 10 ];
	gint   arrondit = -1;

	if (TypeFileIs == FILE_IS_WAV || TypeFileIs == FILE_IS_MP3) {
		/*
		normalize-audio -n ./kill_bill.wav
		*/
		args[pos++] = prginit_get_name (NMR_normalize);
		args[pos++] = "-n";
		args[pos++] = file;
		args[pos++] = NULL;
	}
	else if (TypeFileIs == FILE_IS_OGG) {
		/*
		normalize-ogg -n ./kill_bill.ogg
		*/
		args[pos++] = "normalize-ogg";
		args[pos++] = "-n";
		args[pos++] = file;
		args[pos++] = NULL;
	}

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!  NORMALISE -n %s\n", args[ 0 ]);
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= 1024) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= 1024\n", pos);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		pos ++;
		buf[ pos ] = '\0';

		VarGet.bool_percent = TRUE;

		if (strstr (buf, "dBFS")) {
			arrondit = (gint)roundf(atof(buf));
		}

	} while (size != 0);

	close(fd);
	VarGet.code_fork = -1;
	return (arrondit);
}
//
//
GString *GetInfo_file (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);

	args[pos++] = "file";
	args[pos++] = "-b";
	args[pos++] = "-z";
	args[pos++] = "-n";
	args[pos++] = file;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!             F I L E               !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);

	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		/*
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
		*/
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';
		g_string_append_printf (gstr, "%s", buf);

	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
gchar *GetInfo_cpu (void)
{
	gchar	 buf [ GET_MAX_CARS + 10 ];
	FILE	*fp;
	gchar	*Ptr = NULL;

	fp = fopen ("/proc/cpuinfo", "r");
	while (fgets (buf,  GET_MAX_CARS, fp) != NULL) {
		// g_print("BUF = %s", buf);
		Ptr = buf;
		if (*(Ptr + 0) == 'm' &&
		    *(Ptr + 1) == 'o' &&
		    *(Ptr + 2) == 'd' &&
		    *(Ptr + 3) == 'e' &&
		    *(Ptr + 4) == 'l' &&
		    *(Ptr + 5) == ' ' &&
		    *(Ptr + 6) == 'n' &&
		    *(Ptr + 7) == 'a' &&
		    *(Ptr + 8) == 'm' &&
		    *(Ptr + 9) == 'e'

		) {
			// g_print("---------BUF = %s", buf);

			Ptr = buf;
			while (*Ptr ++)
				if (*Ptr == '\n') *Ptr = '\0';
			for ( Ptr = buf; *Ptr != ':'; Ptr ++ );
			Ptr ++;
			while (*Ptr == ' ') Ptr ++;
			// g_print("---------BUF = %s", Ptr);

			fclose (fp);
			return (g_strdup (Ptr));
		}
	}
	fclose (fp);
	return (NULL);
}
//
//
gchar *GetInfo_cpu_str (void)
{
	struct	utsname utsname;
	gchar	*ptruname = NULL;
	gchar	*ModelName = NULL;

	ModelName = GetInfo_cpu ();

	HostConf.NbCpu   = sysconf(_SC_NPROCESSORS_ONLN);
	HostConf.TypeCpu = 8 * sizeof(void*);
	if (uname (&utsname) == 0) {

		strcpy (HostConf.Machine, utsname.machine);
		if (strcmp (HostConf.Machine, "x86_64") == 0)
			HostConf.BoolCpuIs64Bits = TRUE;
		else	HostConf.BoolCpuIs64Bits = FALSE;
		ptruname = g_strdup_printf (
				"<span foreground=\"#0000FF\"><b>Informations machine:</b></span>\n\n"
				"sysname\n\t<b> %s</b>\n"
				"nodename\n\t<b> %s</b>\n"
				"release\n\t<b> %s</b>\n"
				"version\n\t<b> %s</b>\n"
				"machine\n\t<b> %s</b>\n"
				"\t<b> %d CPU (%d bits)</b>\n"
				"\t<b> %s</b>\n",
			utsname.sysname, utsname.nodename, utsname.release, utsname.version, utsname.machine,
			HostConf.NbCpu, HostConf.TypeCpu,
			ModelName);
	}
	g_free (ModelName);
	ModelName = NULL;
	return (ptruname);
}
//
//
void GetInfo_cpu_print (void)
{
	struct	utsname utsname;
	gchar	*ptruname = NULL;
	gchar	*ModelName = NULL;

	ModelName = GetInfo_cpu ();
	HostConf.NbCpu   = sysconf(_SC_NPROCESSORS_ONLN);
	// g_print ("_SC_NPROCESSORS_CONF : Le nombre de processeurs configure                           = %d\n", (gint)sysconf(_SC_NPROCESSORS_CONF));
	// g_print ("_SC_NPROCESSORS_ONLN : Le nombre de processeurs actuellement en ligne (disponibles) = %d\n", (gint)sysconf(_SC_NPROCESSORS_ONLN));
	HostConf.TypeCpu = 8 * sizeof(void*);
	if (uname (&utsname) == 0) {

		strcpy (HostConf.Machine, utsname.machine);
		if (strcmp (HostConf.Machine, "x86_64") == 0)
			HostConf.BoolCpuIs64Bits = TRUE;
		else	HostConf.BoolCpuIs64Bits = FALSE;

		ptruname = g_strdup_printf (
				"\nINFORMATIONS MACHINE:\n\n"
				"sysname\t\t = %s\n"
				"nodename\t = %s\n"
				"release\t\t = %s\n"
				"version\t\t = %s\n"
				"machine\t\t = %s\n"
				"       \t\t = %d CPU (%d bits)\n"
				"       \t\t = %s\n",
			utsname.sysname, utsname.nodename, utsname.release, utsname.version, utsname.machine,
			HostConf.NbCpu, HostConf.TypeCpu,
			ModelName);
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print("%s",ptruname);
			g_print ("HostConf.NbCpu   = %d CPU\n", HostConf.NbCpu);
			g_print ("HostConf.TypeCpu = %d Bits\n", HostConf.TypeCpu);
			g_print ("HostConf.Machine = %s\n", HostConf.Machine);
			g_print ("\n");
		}
	}
	g_free (ModelName);
	ModelName = NULL;
	g_free (ptruname);
	ptruname = NULL;
}
//
//
GString *GetInfo_faad (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);

	args[pos++] = "faad";
	args[pos++] = "-i";
	args[pos++] = file;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!             F A A D               !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		/*
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
		*/
		buf[pos ++ ] = '\n';
		buf[pos] = '\0';
		g_string_append_printf (gstr, "%s", buf);

	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
GString *GetInfo_checkmp3 (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = NULL;

	if (FALSE == PrgInit.bool_checkmp3) {
		PRINT_FUNC_LF();
		g_print ("\tPrgInit.bool_checkmp3 = %s\n", PrgInit.bool_checkmp3 ? "TRUE" : "FALSE");
		g_print ("\tIL FAUT INSTALLER checkmp3 OU mp3check SUIVANT VOTRE DISTRIBUTION !\n\n");
		return (NULL);
	}
	gstr = g_string_new (NULL);

	// args[pos++] = PrgInit.name_checkmp3;
	args[pos++] = prginit_get_name (NMR_checkmp3);
	args[pos++] = "-a";
	args[pos++] = "-v";

	// TODO: @Tetsumaki
	// http://forum.ubuntu-fr.org/viewtopic.php?pid=3889239#p3889239
	// OPTION -i n'existe pas sous ARCHLINUX !!!
	// args[pos++] = "-i";

	args[pos++] = file;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!           C H E C K M P 3         !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
GString *GetInfo_ogginfo (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);

	args[pos++] = "ogginfo";
	args[pos++] = file;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!           O G G I N F O           !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
GString *GetInfo_mediainfo (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);

	args[pos++] = "dvd+rw-mediainfo";
	args[pos++] = file;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!        DVD+RW-MEDIAINFO           !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/
	// STDIN_FILENO
	// STDOUT_FILENO
	// STDERR_FILENO
	fd = GetInfo_exec_with_output( args, &VarGet.code_fork, STDOUT_FILENO );
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
GString *GetInfo_cdplay (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);

	args[pos++] = "cdplay";
	args[pos++] = "-d";
	args[pos++] = file;
	args[pos++] = "info";
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!           C D P L A Y             !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
GString *GetInfo_which (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);

	args[pos++] = "which";
	args[pos++] = file;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!             W H I C H             !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
void GetInfo_eject (gchar *StrDevice)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;

	args[pos++] = "eject";
	// args[pos++] = "--cdrom";
	args[pos++] = StrDevice;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!              E J E C T            !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
	} while (size != 0);

	close(fd);
}
//
//
GString *GetInfo_cdparanoia( gchar *p_device )
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);

	args[pos++] = "cdparanoia";
	args[pos++] = "-Q";
	args[pos++] = "-d";
	args[pos++] = p_device;
	args[pos++] = NULL;
	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!       C D P A R A N O I A         !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/
	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
GString *GetInfo_metaflac (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);

	args[pos++] = "metaflac";
	args[pos++] = "--show-total-samples";
	args[pos++] = "--show-sample-rate";
	args[pos++] = file;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!         M E T A F L A C           !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);
	return (gstr);
}
//
//
gboolean GetInfo_wget (void)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	gboolean bool_access_ok = TRUE;

	// PRINT_FUNC_LF();

	args[pos++] = "wget";
	args[pos++] = "--user-agent=\"Mozilla 22.0\"";
	args[pos++] = "--directory-prefix=/tmp/";
	args[pos++] = "http://www.google.fr/index.html";
	args[pos++] = NULL;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!             W G E T               !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDERR_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));

		if (NULL != strstr (buf, "Not Found")) {
			bool_access_ok = FALSE;
		}

	} while (size != 0);

	close(fd);

	if( TRUE == libutils_test_file_exist( "/tmp/index.html" )) {
		g_unlink ("/tmp/index.html");
		bool_access_ok = TRUE;
	}
	else {
		bool_access_ok = FALSE;
	}
	return (bool_access_ok);
}
//
//
SHNTAG *GetInfo_free_shntool (SHNTAG *ShnTag)
{
	if (NULL != ShnTag) {
		if (NULL != ShnTag->time)	{ g_free (ShnTag->time);	ShnTag->time = NULL; }
		if (NULL != ShnTag->size)	{ g_free (ShnTag->size);	ShnTag->size = NULL; }

		g_free (ShnTag);
		ShnTag = NULL;
	}

	return ((SHNTAG *)NULL);

}

gchar *GetInfo_cd_discid( gchar *p_device )
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);
	gchar	*StrCdDiscId = NULL;

	args[pos++] = "cd-discid";
	args[pos++] = p_device;
	args[pos++] = NULL;

	/*
	g_print( "\n" );
	g_print ("!-----------------------------------!\n");
	g_print ("!        C D - D I S C I D          !\n");
	g_print ("!-----------------------------------!\n");
	for( pos = 0; args[ pos ] != NULL; pos++ )
		g_print( "args [ %02d ] = '%s'\n", pos, args [ pos ] );
	g_print( "\n" );
	*/
	fd = GetInfo_exec_with_output( args, &VarGet.code_fork, STDOUT_FILENO );
	do {
		pos = -1;
		do {
			pos++;
			if( pos >= GET_MAX_CARS ) {
				PRINT_FUNC_LF();
				g_print( "pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS );
				pos --;
			}
			size = read( fd, &buf[pos], 1 );

		} while(( buf[pos] != '\b' ) && ( buf[pos] != '\r' ) && ( buf[pos] != '\n' ) && ( size > 0 ));
		if( pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf( gstr, "%s", buf );
		}
	} while( size != 0 );
	close( fd );

	if( '\0' != *gstr->str )
		StrCdDiscId = g_strdup( gstr->str );
	g_string_free( gstr, TRUE );
	gstr = NULL;

	return( StrCdDiscId );
}

SHNTAG *GetInfo_shntool (gchar *file)
{
	gint     fd;
	gint     pos = 0;
	gchar   *args [ 10 ];
	gchar    buf [ GET_MAX_CARS + 10 ];
	gint     size = 0;
	GString *gstr = g_string_new (NULL);
	SHNTAG	*ShnTag = NULL;
	gchar	*Ptr = NULL;

	guint	 Min;
	guint	 Sec;
	guint	 Hundr;

	args[pos++] = "shntool";
	args[pos++] = "info";
	args[pos++] = file;
	args[pos++] = NULL;

	/*
	g_print ("!-----------------------------------!\n");
	g_print ("!          S H N T O O L            !\n");
	g_print ("!-----------------------------------!\n");
	for (pos = 0; args[ pos ] != NULL; pos++)
		g_print ("args [ %02d ] = '%s'\n", pos, args [ pos ]);
	g_print ("\n");
	*/

	fd = GetInfo_exec_with_output (args, &VarGet.code_fork, STDOUT_FILENO);
	do {
		pos = -1;
		do {
			pos++;
			if (pos >= GET_MAX_CARS) {
				PRINT_FUNC_LF();
				g_print ("pos(%d) >= GET_MAX_CARS(%d)\n", pos, GET_MAX_CARS);
				pos --;
			}
			size = read(fd, &buf[pos], 1);

		} while ((buf[pos] != '\b') && (buf[pos] != '\r') && (buf[pos] != '\n') && (size > 0));
		if (pos > 0 ) {
			buf[pos ++ ] = '\n';
			buf[pos] = '\0';
			g_string_append_printf (gstr, "%s", buf);
		}
	} while (size != 0);

	close(fd);

	ShnTag = g_malloc0 (sizeof (SHNTAG));

	if ((Ptr = strstr (gstr->str, "Length:")) != NULL) {
		Ptr = strchr (Ptr, ':');
		Ptr ++;
		while (*Ptr == ' ') Ptr ++;

		Min = atoi (Ptr);

		Ptr = strchr (Ptr, ':');
		Ptr ++;
		Sec = atoi (Ptr);

		Ptr = strchr (Ptr, '.');
		Ptr ++;
		Hundr = atoi (Ptr);

		ShnTag->SecTime = (gdouble)(Min * 60) + (gdouble)(Sec) + (gdouble)(Hundr / 10.0);

		ShnTag->time = g_strdup_printf ("%02d:%02d", Min, Sec);

		if ((Ptr = strchr (ShnTag->time, '.')) != NULL) {
			*Ptr = '\0';
		}
	}

	ShnTag->size = g_strdup_printf ("%d Ko", (gint)libutils_get_size_file (file) / 1024);

	g_string_free (gstr, TRUE);

	return ((SHNTAG *)ShnTag);
}
//
// RECONNAISSANCE ENTETE FICHIER:
// 	FILE_IS_M4A
// 	FILE_IS_VID_M4A
// 	FILE_IS_WAVPACK
// 	FILE_IS_WAVPACK_MD5
// 	FILE_IS_WAV
// 	FILE_IS_SHN
// 	FILE_IS_RM
// 	FILE_IS_OGG
// 	FILE_IS_MPC
// 	FILE_IS_AC3
// 	FILE_IS_MP3
// 	FILE_IS_FLAC
// 	FILE_IS_APE
// 	FILE_IS_AIFF
// 	FILE_IS_DTS
// SI COMMANDE file IS OK TEST DE:
// 	FILE_IS_AAC
// 	FILE_IS_WMA
//
TYPE_FILE_IS GetInfo_file_is( gchar *PathName )
{
	GString		*gstr = NULL;
	TYPE_FILE_IS	TypeFileRet = FILE_IS_NONE;

	gstr = GetInfo_file (PathName);
	// g_print( "gstr =\n%s\n", gstr->str );
	// ABANDON SI LA COMMANDE file NE TROUVE PAS LE TYPE DE FICHIER
	if( 0 == strcmp( gstr->str, "data" )) {
		TypeFileRet = FILE_IS_NONE;
		PRINT("ERROR: LA COMMANDE file NE TROUVE RIEN :/");
	}
	// UNE PETITE CHANCE SE PRESENTE ICI POUR AAC
	else {
		// FILE_IS_AAC
		// 19 Waka Waka (This Time for Africa).m4a:          ISO Media, MPEG v4 system, iTunes AAC-LC
		// 4 Non Blondes - Whats Going On.m4a:               ISO Media, MPEG v4 system, version 2
		if( TRUE == FileIs_vidm4a( PathName )) {
			TypeFileRet = FILE_IS_VID_M4A;
		}
		else if( strstr (gstr->str, "AAC" ) || strstr (gstr->str, "MPEG ADTS, AAC," )) {
			TypeFileRet = FILE_IS_AAC;
		}
		// FILE_IS_WMA
		else if (strstr (gstr->str, "Microsoft ASF")) {
			TypeFileRet = FILE_IS_WMA;
		}
		// FILE_IS_VID_M4A
		// WCC_RADIO_EP042.m4a:			ISO Media, MPEG v4 system, iTunes AAC-LC
		//
		// FILE_IS_M4A
		// 21 The Academy.m4a:			ISO Media, MPEG v4 system, version 2
		// 4 Non Blondes - Whats Going On.m4a:	ISO Media, MPEG v4 system, version 2
		// audio.m4a:				ISO Media, MPEG v4 system, version 2
		//
		// FILE_IS_AAC
		// 01_King_Porter_Stomp.aac:		MPEG ADTS, AAC, v4 LC, 22.05 kHz, stereo
		// aif.aac:				MPEG ADTS, AAC, v4 LC, 22.05 kHz, stereo
		// audio.aac:				MPEG ADTS, AAC, v2 LC, 48 kHz, stereo
	}
	if( FILE_IS_VID_M4A == TypeFileRet || FILE_IS_AAC == TypeFileRet || FILE_IS_WMA == TypeFileRet ) {
		g_string_free (gstr, TRUE);
		gstr = NULL;
		return( TypeFileRet );
	}

	// FILE_IS_VID_M4A
	if( TRUE == FileIs_vidm4a( PathName )) {
		TypeFileRet = FILE_IS_VID_M4A;
	}
	// FILE_IS_M4A
	else if( TRUE == FileIs_m4a( PathName )) {
		TypeFileRet = FILE_IS_M4A;
	}
	// FILE_IS_WAVPACK
	// FILE_IS_WAVPACK_MD5
	else if( TRUE == FileIs_wavpack( PathName )) {
		TypeFileRet = FILE_IS_WAVPACK;
	}
	// FILE_IS_WAV
	else if( TRUE == FileIs_wav( PathName )) {
		TypeFileRet = FILE_IS_WAV;
	}
	// FILE_IS_SHN
	else if( TRUE == FileIs_shn( PathName )) {
		TypeFileRet = FILE_IS_SHN;
	}
	// FILE_IS_RM
	else if( TRUE == FileIs_rm( PathName )) {
		TypeFileRet = FILE_IS_RM;
	}
	// FILE_IS_OGG
	else if( TRUE == FileIs_ogg( PathName )) {
		TypeFileRet = FILE_IS_OGG;
	}
	// FILE_IS_MPC
	else if( TRUE == FileIs_mpc( PathName )) {
		TypeFileRet = FILE_IS_MPC;
	}
	// FILE_IS_AC3
	else if( TRUE == FileIs_ac3( PathName )) {
		TypeFileRet = FILE_IS_AC3;
	}
	// FILE_IS_FLAC
	else if( TRUE == FileIs_flac( PathName )) {
		TypeFileRet = FILE_IS_FLAC;
	}
	// FILE_IS_APE
	else if( TRUE == FileIs_ape( PathName )) {
		TypeFileRet = FILE_IS_APE;
	}
	// FILE_IS_DTS
	else if( TRUE == FileIs_dts( PathName )) {
		TypeFileRet = FILE_IS_DTS;
	}
	// FILE_IS_AIFF
	else if( TRUE == FileIs_aiff( PathName )) {
		TypeFileRet = FILE_IS_AIFF;
	}
	// FILE_IS_MP3
	else if( TRUE == FileIs_mp3( PathName )) {
		TypeFileRet = FILE_IS_MP3;
	}
	// FILE_IS_MP3
	// Without tag id3 :/
	else if( strstr (gstr->str, "MPEG ADTS, layer III" )) {
		TypeFileRet = FILE_IS_MP3;
	}

	g_string_free (gstr, TRUE);
	gstr = NULL;

	return( TypeFileRet );
}





