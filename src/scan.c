 /*
 *  file      : scan_cd.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cd_audio.h"
#include "configuser.h"
#include "proc.h"
#include "dvd.h"
#include "get_info.h"
#include "win_info.h"
#include "scan.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/




typedef struct {
	GList        *Media_List;		// NULL
	gint          TotalReadersDetected;	// 0
	gboolean      bool_scan_cd_dvd;		// TRUE

} VAR_SCANCD;

VAR_SCANCD var_scancd = {NULL, 0, TRUE};



/* Idee de code reprise depuis : graveman-0.3.12-4
*  2005 - 11 - 11
*/
typedef enum { _NONE_, IDE, SCSI } TP;
typedef struct {
	TP        tp;
	gchar    *detectline;
	gchar    *useline;
	gchar    *info;
	gboolean  tst;
} Tsearchdrive;

// definition de tous les type de bus
// 
Tsearchdrive listesearchdrives[] = {
	/* 
	 *  10 - LINUX_IDE : pure ide devices with linux
	 */
	{ IDE, "dev=/dev/hda",   "/dev/hda",  "hda", FALSE },
	{ IDE, "dev=/dev/hdb",   "/dev/hdb",  "hdb", FALSE },
	{ IDE, "dev=/dev/hdc",   "/dev/hdc",  "hdc", FALSE },
	{ IDE, "dev=/dev/hdd",   "/dev/hdd",  "hdd", FALSE },
	{ IDE, "dev=/dev/hde",   "/dev/hde",  "hde", FALSE },
	{ IDE, "dev=/dev/hdf",   "/dev/hdf",  "hdf", FALSE },
	{ IDE, "dev=/dev/hdg",   "/dev/hdg",  "hdg", FALSE },
	{ IDE, "dev=/dev/hdh",   "/dev/hdh",  "hdh", FALSE },
	{ IDE, "dev=/dev/hdi",   "/dev/hdi",  "hdi", FALSE },
	{ IDE, "dev=/dev/hdj",   "/dev/hdj",  "hdj", FALSE },
	/* 
	 *  28 - LINUX_SCSI : SCSI alias with linux
	 */
	{ SCSI, "dev=/dev/scd0", "/dev/scd0", "scd0", FALSE },
	{ SCSI, "dev=/dev/scd1", "/dev/scd1", "scd1", FALSE },
	{ SCSI, "dev=/dev/scd2", "/dev/scd2", "scd2", FALSE },
	{ SCSI, "dev=/dev/scd3", "/dev/scd3", "scd3", FALSE },
	{ SCSI, "dev=/dev/scd4", "/dev/scd4", "scd4", FALSE },
	{ SCSI, "dev=/dev/scd5", "/dev/scd5", "scd5", FALSE },
	{ SCSI, "dev=/dev/scd6", "/dev/scd6", "scd6", FALSE },
	{ SCSI, "dev=/dev/sr0",  "/dev/sr0",  "sr0", FALSE },
	{ SCSI, "dev=/dev/sr1",  "/dev/sr1",  "sr1", FALSE },
	{ SCSI, "dev=/dev/sr2",  "/dev/sr2",  "sr2", FALSE },
	{ SCSI, "dev=/dev/sr3",  "/dev/sr3",  "sr3", FALSE },
	{ SCSI, "dev=/dev/sr4",  "/dev/sr4",  "sr4", FALSE },
	{ SCSI, "dev=/dev/sr5",  "/dev/sr5",  "sr5", FALSE },
	{ SCSI, "dev=/dev/sr6",  "/dev/sr6",  "sr6", FALSE },
	{ SCSI, "dev=/dev/sg0",  "/dev/sg0",  "sg0", FALSE },
	{ SCSI, "dev=/dev/sg1",  "/dev/sg1",  "sg1", FALSE },
	{ SCSI, "dev=/dev/sg2",  "/dev/sg2",  "sg2", FALSE },
	{ SCSI, "dev=/dev/sg3",  "/dev/sg3",  "sg3", FALSE },
	{ SCSI, "dev=/dev/sg4",  "/dev/sg4",  "sg4", FALSE },
	{ SCSI, "dev=/dev/sg5",  "/dev/sg5",  "sg5", FALSE },
	{ SCSI, "dev=/dev/sg6",  "/dev/sg6",  "sg6", FALSE },
	{ SCSI, "dev=/dev/sga",  "/dev/sga",  "sga", FALSE },
	{ SCSI, "dev=/dev/sgb",  "/dev/sgb",  "sgb", FALSE },
	{ SCSI, "dev=/dev/sgc",  "/dev/sgc",  "sgc", FALSE },
	{ SCSI, "dev=/dev/sgd",  "/dev/sgd",  "sgd", FALSE },
	{ SCSI, "dev=/dev/sge",  "/dev/sge",  "sge", FALSE },
	{ SCSI, "dev=/dev/sgf",  "/dev/sgf",  "sgf", FALSE },
	{ SCSI, "dev=/dev/sgg",  "/dev/sgg",  "sgg", FALSE },
	/* 
	 *  The end of list
	 */
	{ _NONE_, NULL, NULL, NULL, FALSE}
};

/*
*---------------------------------------------------------------------------
* FUNCTIONS
*---------------------------------------------------------------------------
*/

void scan_set_bool_scan (gboolean p_flag)
{
	var_scancd.bool_scan_cd_dvd = p_flag;
}

/*
** Return total readers in computer
*/
gint scan_get_nbr_readers_detected (void)
{
	/* PRINT_FUNC_LF(); */
	return (var_scancd.TotalReadersDetected);
}


/*
** Renvoie le numero du lecteur en selection
*/
gint scan_get_num_config_cd_is_selected (void)
{
	/* PRINT_FUNC_LF(); */
	return (0);
}


/*
** Ouverture du lecteur en selection
*/
void scan_open_peri_cd (void)
{
	/* PRINT_FUNC_LF(); */
}


/*
** Fermeture du lecteur en selection
*/
void scan_close_peri_cd (void)
{
	/* PRINT_FUNC_LF(); */
}


/*
** Nouvelle selection de lecteur CD ou DVD
*/
void scan_set_sel_reader_cd (gint column, gint row)
{
	/* PRINT_FUNC_LF(); */
}


/*
** Affiche 'NameDevice' et 'NameLink' dans 'Adr_ClistConfigCD'
*/
void scan_print_name_cdrom (gchar *NameDevice, gchar *NameLink)
{
	/* PRINT_FUNC_LF(); */
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\tNameDevice=%s  NameLink=%s\n", NameDevice, NameLink);
}


/* Detruit la GList MEDIA
*/
void scan_remove_glist_media (void)
{
	GList   *list = NULL;
	MEDIA   *Media = NULL;
	gint	 NbList = 0;

	/* PRINT_FUNC_LF(); */
	list = g_list_first (var_scancd.Media_List);
	while (list) {
		if (NULL != (Media = (MEDIA *)list->data)) {
			g_free (Media->Full_Name);
			g_free (Media->Device);
			g_free (Media);
			Media = NULL;
			list->data = NULL;
			
			NbList ++;
		}
		list = g_list_next (list);
	}
	g_list_free (var_scancd.Media_List);
	var_scancd.Media_List = NULL;
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\tRemove: %d\n", NbList);
}

// 
// 
GList *scan_scan_drives_cd_dvd (void)
{
	GString      *gstr = NULL;	
	gchar        *Lout = NULL;
	Tsearchdrive *Lcurentry;
	gchar        *Ptr = NULL;
	MEDIA        *Media = NULL;
	GList        *List = NULL;
	GList        *cd_list = NULL;
	gchar         str [256];
	gint          i;
	gint          pass;

	// PRINT_FUNC_LF();
	
	if (var_scancd.bool_scan_cd_dvd == FALSE) {
		g_print ("En activant cette option [xcfa --no_cd_dvd] en ligne de commande, vous activez la non reconnaissance des lecteurs cd - dvd\n\n");
		return ((GList *)NULL);
	}
	
	/* test is 'dvd+rw-tools package' present ? */
	if (libutils_find_file ("dvd+rw-mediainfo") == FALSE) {
		wind_info_init (
			WindMain,
			_("PACKAGE dvd+rw-tools is missing"),
			_("The package 'imagemagick' is missing"),
			  "\n",
			_("on your system !"),
			  "\n\n",
			_("Please install it and resume"),
			  "\n",
			_("'Dvd + rw-mediainfo' to identify the"),
			  "\n",
			_("reader of cd / dvd."),
			  "");
			return (NULL);
	}
	
	/* Init a FALSE */
	for (Lcurentry = listesearchdrives; Lcurentry->detectline; Lcurentry++)
		Lcurentry->tst = FALSE;
	
	/* Prend dans </proc/sys/dev/cdrom/info> tous les cd.dvd installes de l'ordi et
	*  active la variable de recherche dans 'Lcurentry'
	*/
	List = proc_get_proc_init_cdrominfo ();
	if (List) {
		cd_list = g_list_first (List);
		while (cd_list) {
			if ((gchar *)cd_list->data) {
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("%s\n", (gchar *)cd_list->data);
				for (Lcurentry = listesearchdrives; Lcurentry->detectline; Lcurentry++) {
					/*Lcurentry->tst = FALSE;*/
					if (0 == strncmp((gchar *)cd_list->data, Lcurentry->info, strlen((gchar *)cd_list->data))) {
						Lcurentry->tst = TRUE;
						break;
					}
				}
			}
			cd_list = g_list_next(cd_list);
		}
	}
	else {
		/* Le fichier </proc/sys/dev/cdrom/info> n'existe pas: toutes
		*  les entrees 'Lcurentry->useline' seront testees
		*/
		for (Lcurentry = listesearchdrives; Lcurentry->detectline; Lcurentry++)
			Lcurentry->tst = TRUE;
	}
	List = proc_remove_glist_cdrominfo (List);

	/* l'analyse des medias commence ici */
	var_scancd.TotalReadersDetected = 0;
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("\nAnalyse devices in : ");
		g_print ("%s::%s(line=%d) \n", __FILE__, __FUNCTION__, __LINE__);
	}
	
	if( TRUE == g_file_test( "/dev", G_FILE_TEST_IS_DIR )) {
		for (pass = 0, Lcurentry = listesearchdrives; Lcurentry->detectline; pass++, Lcurentry++) {

			/* Entree en test ? */
			if (Lcurentry->tst == FALSE) continue;
			
			if( FALSE == libutils_access_mode( Lcurentry->useline )) continue;
			
			/* TODO
			*  TODO : 'dvd+rw-mediainfo' FONCTIONNE T-IL AUSSI AVEC LES LECTEURS DE CD ?
			*  TODO : IL FAUDRAIT AUSSI FAIRE UN TEST AVEC DES LECTEURS / GRAVEURS DE CD
			*  TODO
			*/		
			gstr = GetInfo_mediainfo( Lcurentry->useline );
			Lout = gstr->str;

			if (*Lout) {
				if ((Ptr = strstr (Lout, "INQUIRY:"))) {
					/* sur mon ordi :
					INQUIRY:                [_NEC    ][DVD_RW ND-3520A ][1.04]
					INQUIRY:                [_NEC    ][DVD_RW ND-3520A ][3.05]  -- Apres un flash bios
					INQUIRY:                [_NEC    ][DVD_RW ND-3520A ][3.06]  -- Apres un flash bios
					INQUIRY:                [DVDRW   ][IDE 16X         ][A07R]
					INQUIRY:                [DVDRW   ][IDE 16X         ][A190]  -- Apres un flash bios
					*/
					/* remplacer tous les '[' et les ']' par ' ' */
					if (strchr (Ptr, '[') && strchr (Ptr, ']')) {
						Ptr = strchr (Ptr, '[');
						i = 0;
						while (*Ptr != '\n') str [ i++ ] = *Ptr++;
						str [ i ] = '\0';
						while ((Ptr = strchr (str, '['))) *Ptr = ' ';
						while ((Ptr = strchr (str, ']'))) *Ptr = ' ';

						/* supprimer les espaces de debut */
						Ptr = str;
						while (*Ptr == ' ') Ptr ++;
						strcpy (str, Ptr);

						/* supprimer les espaces de fin */
						Ptr = str;
						while (*Ptr) Ptr ++;
						Ptr --;
						while (*Ptr == ' ') Ptr --;
						Ptr ++;
						*Ptr = '\0';

						/* supprimer les doubles espaces */
						Ptr = str;
						while (*Ptr) {
							if ((*Ptr==' ') && (*(Ptr+1)==' ')) {
								strcpy (Ptr, Ptr+1);
								Ptr = str;
								continue;
							}
							Ptr ++;
						}
						if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
							g_print ("\tline[ %d ] %s   ", pass, Lcurentry->tp == IDE ? "IDE" : "SCSI");
							g_print ("%s\t'%s'\n", Lcurentry->useline, str);
						}
						if ((Media = (MEDIA *)g_malloc0 (sizeof (MEDIA)))) {
							Media->Full_Name = g_strdup (str);
							Media->Device    = g_strdup (Lcurentry->useline);
							Media->Num       = var_scancd.TotalReadersDetected;
							var_scancd.TotalReadersDetected ++;
						
							Media->line      =  pass;
							Media->type      = Lcurentry->tp == IDE ? 0 : 1;
						
							List = g_list_append (List, Media);
						}
					}
					else {
						PRINT_FUNC_LF();
						if( TRUE == OptionsCommandLine.BoolVerboseMode )
							g_print ("La ligne renvoyee par: dvd+rw-mediainfo est inconnue\n");
					}
				}
			}

			g_string_free (gstr, TRUE);
			Lout = NULL;
		}
	}
	g_print ("\n");

	return (List);
}

// Retourne un pointeur sur le media actif
// 
gchar *scan_get_text_combo_cd (TYPE_READER type_reader)
{
	gint   Nmr_Combo_Actif = -1;
	GList *List = NULL;
	MEDIA *Media = NULL;

	/* PRINT_FUNC_LF(); */

	if (type_reader == _CD_) {
		Nmr_Combo_Actif = gtk_combo_box_get_active (var_cd.Adr_Combo_Box);
	}
	else if (type_reader == _DVD_) {
		Nmr_Combo_Actif = gtk_combo_box_get_active (var_dvd.Adr_ComboBox_Reader);
		
		if( dvd_bool_read_dvd_from_directory () == TRUE ) {
			return( (gchar *)var_dvd.from.path );
		}
	}
	
	List = g_list_first (var_scancd.Media_List);
	while (List) {
		if ((Media = (MEDIA *)List->data)) {
			if (Media->Num == Nmr_Combo_Actif) return ((gchar *)Media->Device);
		}
		List = g_list_next(List);
	}

	return ((gchar *)NULL);
}

// 
// 
GList *scan_get_glist (void)
{
	if (var_scancd.Media_List == NULL)
		var_scancd.Media_List = scan_scan_drives_cd_dvd ();
	
	return ( (GList *)var_scancd.Media_List);
}

// 
// 
void scan_eject_media (TYPE_READER TypeReadder)
{
	GetInfo_eject ( scan_get_text_combo_cd (TypeReadder));

}




