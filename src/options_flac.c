 /*
 *  file      : options_flac.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "options.h"






static gchar *CompLevel[] = {"-0","-1","-2","-3","-4","-5","-6","-7","-8"};

gchar *optionsFlac_get_compression_level_flac (void)
{
	// return ((gchar *)CompLevel [ gtk_combo_box_get_active ( GTK_COMBO_BOX (var_options.Adr_Widget_flac_compression)) ]);
	return ((gchar *)CompLevel [ Config.CompressionLevelFlac ]);
}



void on_combobox_flac_compression_realize (GtkWidget *widget, gpointer user_data)
{
 	var_options.Adr_Widget_flac_compression =  GTK_COMBO_BOX (widget);

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("0  (Fast)"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget),   "1");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget),   "2");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget),   "3");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget),   "4");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget),   "5");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget),   "6");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget),   "7");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("8  (Low)"));

	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.CompressionLevelFlac);
}
void on_combobox_flac_compression_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_flac_compression) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_flac_compression))) >= 0)
			Config.CompressionLevelFlac = ind;
		OptionsInternal_set_datas_interne (COLOR_FLAC_TAUX_COMPRESSION, var_options.Adr_label_flac_flac, FLAC_WAV_TO_FLAC);
	}
}













