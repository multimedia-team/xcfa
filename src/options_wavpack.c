 /*
 *  file      : options_wavpack.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "options.h"



static gchar *str_val_preset_compression[] = { "", "-f", "-h", "-hh" };
static gchar *str_val_preset_sound[] = { "-j0", "-j1" };
static gchar *str_val_preset_hybride[] = {"", "-b320", "-b384", "-b448"};
static gchar *str_val_preset_md5[] = {"", "-m"};
static gchar *str_val_preset_encoding[] = {"", "-x1", "-x2", "-x3", "-x4", "-x5", "-x6"};
static gchar *str_val_preset_correction[] = {"",  "-c"};
static gchar *str_val_preset_maximum_compression[] = {"", "-cc"};
gchar *optionsWavpack_get_wavpack_compression (void)
{
	return ( (gchar *)str_val_preset_compression[ gtk_combo_box_get_active (var_options.Adr_Widget_wavpack) ]);
}
gchar *optionsWavpack_get_wavpack_sound (void)
{
	return ( (gchar *)str_val_preset_sound[ gtk_combo_box_get_active (var_options.Adr_Widget_wavpack_sound) ]);
}
gchar *optionsWavpac_get_wavpack_hybride (void)
{
	if (var_options.Adr_Widget_wavpack_mode_hybride)
		return ( (gchar *)str_val_preset_hybride[ gtk_combo_box_get_active (var_options.Adr_Widget_wavpack_mode_hybride) ]);
	return ("");
}
gchar *optionsWavpack_get_wavpack_signature_md5 (void)
{
	return ( (gchar *)str_val_preset_md5[ gtk_combo_box_get_active (var_options.Adr_Widget_wavpack_signature_md5) ]);
}
gchar *optionsWavpack_get_wavpack_extra_encoding (void)
{
	return ( (gchar *)str_val_preset_encoding[ gtk_combo_box_get_active (var_options.Adr_Widget_wavpack_extra_encoding) ]);
}
gchar *optionsWavpack_get_wavpack_correction_file (void)
{
	return ( (gchar *)str_val_preset_correction[ gtk_combo_box_get_active (var_options.Adr_Widget_wavpack_correction_file) ]);
}
gchar *optionsWavpack_get_wavpack_maximum_compression (void)
{
	return ( (gchar *)str_val_preset_maximum_compression[ gtk_combo_box_get_active (var_options.Adr_Widget_wavpack_maximum_compression) ]);
}




void on_combobox_wavpack_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_wavpack = GTK_COMBO_BOX (widget);

	/*
	-f   Mode rapide de compression
	-h   Ume bonne qualite de compression dans tous les modes mais plus lent
	-hh  La meilleure qualite de compression dans tous les modes mais la plus lente
	*/
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Standard"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Fast (Faster encode and decode)"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("High quality (Better compression ratio)"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Very high quality (Best compression but slowest)"));
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.CompressionWavpack);
}
void on_combobox_wavpack_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_wavpack) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack))) >= 0)
			Config.CompressionWavpack = ind;
		
		OptionsInternal_set_datas_interne (COLOR_WAVPACK_COMPRESSION, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	}
}
void on_combobox_wavpack_sound_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_wavpack_sound = GTK_COMBO_BOX (widget);

	/*
	-jn  Joint-stereo override (0 = left/right, 1 = mid/side)
	----------------------
	-j0  Joint-stereo override (0 = left/right)
	-j1  Joint-stereo override (1 = mid/side)
	*/
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("stereo left/right"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("stereo mid/side"));
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.SoundWavpack);
}
void on_combobox_wavpack_sound_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_wavpack_sound) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_sound))) >= 0)
			Config.SoundWavpack = ind;
		
		OptionsInternal_set_datas_interne (COLOR_WAVPACK_SOUND, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	}
}
void on_combobox_wavpack_mode_hybride_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("No"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "320 kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "384 kbit/s");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "448 kbit/s");
	
	var_options.Adr_Widget_wavpack_mode_hybride = GTK_COMBO_BOX (widget);
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.ModeHybrideWavpack);
}
void on_combobox_wavpack_mode_hybride_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_wavpack_mode_hybride) {
		gboolean Bool = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_mode_hybride)) == 0 ? FALSE : TRUE;
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_mode_hybride))) >= 0)
			Config.ModeHybrideWavpack = ind;
			
		if (var_options.Adr_Widget_wavpack_correction_file)
			gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("combobox_wavpack_fichier_de_correction")), Bool);
	
		if (var_options.Adr_Widget_wavpack_maximum_compression)
			gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("combobox_wavpack_compression_maximum")), Bool);
	
		OptionsInternal_set_datas_interne (COLOR_WAVPACK_MODE_HYBRIDE, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	}
}
void on_combobox_wavpack_fichier_de_correction_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_wavpack_correction_file = GTK_COMBO_BOX (widget);

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("No"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Yes"));
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.CorrectionFileWavpack);
	
	on_combobox_wavpack_mode_hybride_changed (NULL,NULL);
}
void on_combobox_wavpack_fichier_de_correction_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_wavpack_correction_file) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_correction_file))) >= 0)
			Config.CorrectionFileWavpack = ind;
		
		OptionsInternal_set_datas_interne (COLOR_WAVPACK_FICHIER_CORRECTION, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	}
}
void on_combobox_wavpack_compression_maximum_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_wavpack_maximum_compression = GTK_COMBO_BOX (widget);

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("No"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Yes"));
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.CompressionMaximumWavpack);
	
	on_combobox_wavpack_mode_hybride_changed (NULL,NULL);
}
void on_combobox_wavpack_compression_maximum_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_wavpack_maximum_compression) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_maximum_compression))) >= 0)
			Config.CompressionMaximumWavpack = ind;
		
		OptionsInternal_set_datas_interne (COLOR_WAVPACK_COMPRESSION_MAXIMUM, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	}
}
void on_combobox_wavpack_signature_md5_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_wavpack_signature_md5 = GTK_COMBO_BOX (widget);

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("No"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Yes"));
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.SignatureMd5Wavpack);
}
void on_combobox_wavpack_signature_md5_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_wavpack_signature_md5) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_signature_md5))) >= 0)
			Config.SignatureMd5Wavpack = ind;
		
		OptionsInternal_set_datas_interne (COLOR_WAVPACK_SIGNATURE_MD5, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	}
}
void on_combobox_wavpack_extra_encoding_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_Widget_wavpack_extra_encoding = GTK_COMBO_BOX (widget);

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget),  _("No"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("1 (fast / rapide)"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "2");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "3");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "4");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "5");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("6 (very slow / tres lent)"));
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.ExtraEncodingWavpack);
}
void on_combobox_wavpack_extra_encoding_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_wavpack_extra_encoding) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_wavpack_extra_encoding))) >= 0)
			Config.ExtraEncodingWavpack = ind;
		
		OptionsInternal_set_datas_interne (COLOR_WAVPACK_EXTRA_ENCODING, var_options.Adr_label_wavpack_wv, WAVPACK_WAV_TO_WAVPACK);
	}
}

