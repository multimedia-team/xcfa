/*
 *  file      : parse.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * ==================================================================================
 * vendredi 22 janvier 2010
 * Fri, 22 Jan 2010
 * ==================================================================================
 * 
 * Un patch pour la fonction %u avec pour caractere unique avait ete code par
 * jerome AT jolimont POINT fr : Jerome Lafrechoux
 * J'ai recode cette fonction, trop restrictive, en ameliorant la fonction tel que:
 *   %u(SOURCE=DESTINATION)
 *   SOURCE
 *     peut etre un ou une suite de caracteres
 *   DESTINATION
 *     est un caractere unique pour remplacer la SOURCE
 * 
 * Par exemple:
 *   %u(éêè=e)
 * Les caracteres SOURCE [ éêè ] seront remplaces par DESTINATION [ e ]
 * La fonction %u() peut figurer plusieurs fois en entree:
 *   %u(àâã=a)%u(éêè=e)%u( =_)
 * etc, ...
 * 
 * ==================================================================================
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "cd_audio.h"
#include "parse.h"


	/*
	%a  -  artiste
	%b  -  titre de l'album
	%c  -  numero de la piste
	%d  -  titre de la chanson
	%e  -  annee
	%g  -  genre
	ptr_template = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_stockage_cdaudio));


	%a  -  artiste
	%b  -  titre de l'album
	%c  -  numero de la piste
	%d  -  titre de la chanson
	%e  -  annee
	%g  -  genre
	%f  -  creation d'un fichier *.m3u et *.xspf
	%u  -  remplacement des caracteres speciaux
	ptr_template = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_new_titre_cdaudio));
	*/
	
typedef enum {
	CD_TAG_ARTIST = 0,			// 
	CD_TAG_ALBUM,				// 
	CD_TAG_INT_NUMBER,			// 
	CD_TAG_TITLE,				// 
	CD_TAG_YEAR,				// 
	CD_TAG_GENRE,				// 
	CD_TAG_FILE_M3U_XSPF,			// BoolCreateFileM
	CD_TAG_CAR_REPLACE,			// StringReplace
	CD_TAG_TETSUMAKI_LOWER,			// TOUTES LES LETTRES EN MINUSCULE SAUF LA PREMIERE ANSI QUE LES CARACTERES PRECEDES PAR '%'
	CD_TAG_WITHOUT_ACCENT,			// Suppression des accents dans les noms de fichier
	CD_TAG_DEFAULT				// StringDefault
} CD_TYPE_TAG;


typedef struct {
	CD_TYPE_TAG	 CdTypeTag;		// CD_TYPE_TAG
	gboolean	 BoolCreateFileM3uXspf;	// CD_TAG_FILE_M3U_XSPF
	gchar		*StringReplaceOld;	// CD_TAG_CAR_REPLACE
	gchar		*StringReplaceNew;	// CD_TAG_CAR_REPLACE		
	gchar		*StringDefault;		// CD_TAG_DEFAULT
} PARSE;


typedef struct {
	GList	*ListParseStockCd;		// Liste PARSE de Preference->CD->Dossier de stockage
	GList	*ListParseTitleCd;		// Liste PARSE de CD->Arrangement des titres du CD
} VAR_PARSE;

VAR_PARSE VarParse = { NULL, NULL };



/*
//
// DEBUGING PARSE
//
void Parse_debug (PARSE_TYPE p_ParseType)
{
	GList	*list = NULL;
	PARSE	*Parse = NULL;
	gchar	*Str[] = {"DD_TAG_ARTIST", "CD_TAG_ALBUM", "CD_TAG_INT_NUMBER", "CD_TAG_TITLE", "CD_TAG_YEAR", "CD_TAG_GENRE", "CD_TAG_FILE_M3U_XSPF", "CD_TAG_CAR_REPLACE", "CD_TAG_DEFAULT" };
		
	if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
		list = g_list_first (VarParse.ListParseStockCd);
	} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
		list = g_list_first (VarParse.ListParseTitleCd);
	}
	while(  list ) {
		if( (Parse = (PARSE *)list->data) != NULL ) {
			g_print ("Str [ Parse->CdTypeTag ] = %s\n", Str [ Parse->CdTypeTag ]);
			if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
				if( Parse->CdTypeTag == CD_TAG_CAR_REPLACE ) {
					g_print("Parse->StringReplaceOld = %s ", Parse->StringReplaceOld);
					g_print("Parse->StringReplaceNe = %s\n", Parse->StringReplaceNew);
				}
			}
		}
		list = g_list_next (list);
	}
}
//
// DEBUGING PARSE
//
void Parse_debug_print_cd (PARSE_TYPE p_ParseType)
{
	gint	 Num_cell = 0;
	gchar	*Str = NULL;
	
	for (Num_cell = 0; Num_cell < EnteteCD.TotalTracks; Num_cell ++ ) {
		Str = Parse_get_line (p_ParseType, Num_cell);
		g_print ("Num_cell = %02d  Str = %s\n", Num_cell, Str);
		g_free (Str);
		Str = NULL;
	}
}
*/

// 
// SUPRESSION DES STRUCTURES ET DES GLIST
//
void Parse_remove( PARSE_TYPE p_ParseType, gboolean b_see )
{
	GList		*list = NULL;
	PARSE		*Parse = NULL;

	if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
		list = g_list_first (VarParse.ListParseStockCd);
	} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
		list = g_list_first (VarParse.ListParseTitleCd);
	}
	while( list ) {
		if( (Parse = (PARSE *)list->data) != NULL ) {
			switch (Parse->CdTypeTag ) {
			case CD_TAG_INT_NUMBER :
			case CD_TAG_TITLE :
			case CD_TAG_YEAR :
			case CD_TAG_GENRE :
			case CD_TAG_FILE_M3U_XSPF :
			case CD_TAG_TETSUMAKI_LOWER :
			case CD_TAG_WITHOUT_ACCENT :
				break;
			case CD_TAG_CAR_REPLACE :
				if( NULL != Parse->StringReplaceOld ) {
					g_free (Parse->StringReplaceOld);
					Parse->StringReplaceOld = NULL;
				}
				if( NULL != Parse->StringReplaceNew ) {
					g_free (Parse->StringReplaceNew);
					Parse->StringReplaceNew = NULL;
				}
				break;
			case CD_TAG_DEFAULT :
			case CD_TAG_ARTIST :
			case CD_TAG_ALBUM :
				if( NULL != Parse->StringDefault ) {
					g_free (Parse->StringDefault);
					Parse->StringDefault = NULL;
				}
				break;
			}
			
			g_free (Parse);
			Parse = NULL;
			list->data = NULL;
		}
		list = g_list_next (list);
	}
	
	if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
		g_list_free (VarParse.ListParseStockCd);
		VarParse.ListParseStockCd = NULL;
	} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
		g_list_free (VarParse.ListParseTitleCd);
		VarParse.ListParseTitleCd = NULL;
	}
	
	if( TRUE == b_see && TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print( "\tRemove struct PARSE: ok\n" );
	
}
// 
// ALLOCATION ET STOCKAGE DE TOUS LES TYPES SAUF: CD_TAG_CAR_REPLACE
// 
void Parse_allocate (PARSE_TYPE p_ParseType, CD_TYPE_TAG p_CdTypeTag, gchar *p_Str)
{
	PARSE	*New = NULL;
	
	New = (PARSE *)g_malloc0 (sizeof(PARSE));
	New->CdTypeTag = p_CdTypeTag;
	
	if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
		if( p_CdTypeTag == CD_TAG_FILE_M3U_XSPF ) {
			New->BoolCreateFileM3uXspf = TRUE;
		}
	}
	if( p_CdTypeTag == CD_TAG_DEFAULT && p_Str != NULL ) {
		New->StringDefault = g_strdup_printf ("%s", p_Str);
	}
	if( p_CdTypeTag == CD_TAG_ARTIST && p_Str != NULL ) {
		New->StringDefault = g_strdup_printf ("%s", p_Str);
	}
	if( p_CdTypeTag == CD_TAG_ALBUM && p_Str != NULL ) {
		New->StringDefault = g_strdup_printf ("%s", p_Str);
	}
	
	// REFERENCE LE POINTEUR DE STRUCTURE DANS LE GLIST
	if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
		VarParse.ListParseStockCd = g_list_append (VarParse.ListParseStockCd, New);
	} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
		VarParse.ListParseTitleCd = g_list_append (VarParse.ListParseTitleCd, New);
	}
}
// 
// ALLOCATION ET STOCKAGE DU TYPE: CD_TAG_CAR_REPLACE
// 
void Parse_allocate_function (PARSE_TYPE p_ParseType, CD_TYPE_TAG p_CdTypeTag, gchar *p_Str)
{
	gchar	*Ptr = p_Str;
	gchar	*PtrEnd = p_Str;
	gchar	*PtrEqual = NULL;
	PARSE	*New = NULL;
	gchar	Str [ 10 ];
		
	// LE CARACTERE DE REMPLACEMENT
	while(  PtrEnd && *PtrEnd) PtrEnd ++;
	PtrEnd --;
	if( *PtrEnd == ')' ) {
		*PtrEnd = '\0';
		PtrEnd --;
	}
	while(  PtrEnd && *PtrEnd && *PtrEnd != '=') PtrEnd --;
	PtrEnd ++;
	
	Ptr = p_Str;
	
	for (PtrEqual = PtrEnd; *PtrEqual != '='; PtrEqual --);

	// LE CARACTERE A REMPLACER
	while(  Ptr < PtrEqual ) {
		
		if( Ptr == PtrEqual) break;
		
		if( *Ptr < 0 ) {
			Str [ 0 ] = *Ptr;
			Str [ 1 ] = *(Ptr +1);
			Str [ 2 ] = '\0';
		} else {
			Str [ 0 ] = *Ptr;
			Str [ 1 ] = '\0';
		}
		
		// NOUVELLE STRUCTURE
		New = (PARSE *)g_malloc0 (sizeof(PARSE));
		New->CdTypeTag = p_CdTypeTag;
		
		//
		New->StringReplaceOld = g_strdup_printf ("%s", Str);
		New->StringReplaceNew = g_strdup_printf ("%s", PtrEnd);

		// STOCKE LA STRUCTURE DANS LA LISTE
		if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
			VarParse.ListParseStockCd = g_list_append (VarParse.ListParseStockCd, New);
		} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
			VarParse.ListParseTitleCd = g_list_append (VarParse.ListParseTitleCd, New);
		}
		
		if( *Ptr < 0)
			Ptr += 2;
		else	Ptr ++;
	}
}
// 
// SUPPRESSION ET PARSE COMPLET
// 
void Parse_entry (PARSE_TYPE p_ParseType)
{
	gchar		*PtrTemplate = NULL;
	gchar		*BeginPtrTemplate = NULL;
	gchar		*NewPtrTemplate = NULL;
	gchar		 Str [ 10 ];
	gboolean	 BoolTagOk = FALSE;
	gboolean	 BoolSet = FALSE;
	
	// PRINT_FUNC_LF();
	
	// QUELLE LIGNE ANALYSER ?
	if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
		PtrTemplate = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_stockage_cdaudio));
	} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
		PtrTemplate = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_new_titre_cdaudio));
	} else {
		PRINT("ERREUR PARAMETRE");
		return;
	}
	BeginPtrTemplate = PtrTemplate;
	NewPtrTemplate   = g_strdup( PtrTemplate );
	
	// REMOVE LISTE
	Parse_remove (p_ParseType, FALSE);
	
	// PARSE
	while(  PtrTemplate && *PtrTemplate != '\0' ) {
		if( *PtrTemplate == '%' ) {
			
			if( *(PtrTemplate +1) == 'a' ) {
				// CD_TAG_CAR_REPLACE
				gchar	*Ptr = (PtrTemplate +2);
				gchar	*PtrBegin = NULL;
				gchar	*PtrEnd = NULL;
				gchar	*String = NULL;
				
				// LA PARENTEZE OUVRE LA FONCTION
				if( *Ptr >= 0 && *Ptr == '=' ) {
					Ptr ++;
					
					if( *Ptr == '"' ) {
						Ptr ++;
					
						PtrEnd = PtrBegin = Ptr;
						while(  *Ptr ) {
							if( *Ptr == '"' ) {
								PtrEnd = Ptr;
								break;
							}
							Ptr ++;
						}
					
						// SI CHANE VIDE OU FIN DE FONCTION
						if( *PtrEnd != '"') break;
						if( *PtrBegin == '"')break;
						
						// PREPARE LA CHAINE POUR ANALYSE
						String = g_strnfill ((PtrEnd - PtrBegin) *2, '\0');
						strncpy(String, PtrBegin, PtrEnd - PtrBegin);
						Parse_allocate (p_ParseType, CD_TAG_ARTIST, String);
						g_free (String);
						String = NULL;
						PtrTemplate = (PtrEnd +1);
					}
					else {
						PtrTemplate += 2;
					}
				} else {
					Parse_allocate (p_ParseType, CD_TAG_ARTIST, NULL);
					PtrTemplate += 2;
					BoolTagOk = TRUE;
				}
			}
			else if( *(PtrTemplate +1) == 'b' ) {
				// CD_TAG_CAR_REPLACE
				gchar	*Ptr = (PtrTemplate +2);
				gchar	*PtrBegin = NULL;
				gchar	*PtrEnd = NULL;
				gchar	*String = NULL;
				
				// LA PARENTEZE OUVRE LA FONCTION
				if( *Ptr >= 0 && *Ptr == '=' ) {
					Ptr ++;
					
					if( *Ptr == '"' ) {
						Ptr ++;
					
						PtrEnd = PtrBegin = Ptr;
						while(  *Ptr ) {
							if( *Ptr == '"' ) {
								PtrEnd = Ptr;
								break;
							}
							Ptr ++;
						}
					
						// SI CHANE VIDE OU FIN DE FONCTION
						if( *PtrEnd != '"') break;
						if( *PtrBegin == '"')break;

						// PREPARE LA CHAINE POUR ANALYSE
						String = g_strnfill ((PtrEnd - PtrBegin) *2, '\0');
						strncpy(String, PtrBegin, PtrEnd - PtrBegin);
						Parse_allocate (p_ParseType, CD_TAG_ALBUM, String);
						g_free (String);
						String = NULL;
						PtrTemplate = (PtrEnd +1);
					}
					else {
						PtrTemplate += 2;
					}
				} else {
					Parse_allocate (p_ParseType, CD_TAG_ALBUM, NULL);
					PtrTemplate += 2;
					BoolTagOk = TRUE;
				}
			}
			else if( *(PtrTemplate +1) == 'c' ) {
				Parse_allocate (p_ParseType, CD_TAG_INT_NUMBER, NULL);
				PtrTemplate += 2;
				BoolTagOk = TRUE;
			}
			else if( *(PtrTemplate +1) == 'd' ) {
				Parse_allocate (p_ParseType, CD_TAG_TITLE, NULL);
				PtrTemplate += 2;
				BoolTagOk = TRUE;
			}
			else if( *(PtrTemplate +1) == 'e' ) {
				Parse_allocate (p_ParseType, CD_TAG_YEAR, NULL);
				PtrTemplate += 2;
			}
			else if( *(PtrTemplate +1) == 'f' ) {
				Parse_allocate (p_ParseType, CD_TAG_FILE_M3U_XSPF, NULL);
				PtrTemplate += 2;
			}
			else if( *(PtrTemplate +1) == 'g' ) {
				Parse_allocate (p_ParseType, CD_TAG_GENRE, NULL);
				PtrTemplate += 2;
				BoolTagOk = TRUE;
			}
			else if( *(PtrTemplate +1) == 'u' ) {
				// CD_TAG_CAR_REPLACE
				gchar	*Ptr = (PtrTemplate +2);
				gchar	*PtrBegin = NULL;
				gchar	*PtrEnd = NULL;
				gchar	*String = NULL;
				
				// LA PARENTEZE OUVRE LA FONCTION
				if( *Ptr >= 0 && *Ptr == '(' ) {
					PtrEnd = PtrBegin = Ptr;
					Ptr ++;
					if( *Ptr < 0) Ptr ++;
					// PASSER LE CARACTERE APRES LA PARENTEZE
					if( *Ptr) Ptr ++;
					// SI PAS FIN DE CHAINE
					while(  *Ptr && *(Ptr +1) != '\0' && *(Ptr +2) != '\0' ) {
						// SI FIN DE FONCTION TROUVEE
						if( *(Ptr +1) < 0 ) {
							if( *Ptr == '=' && *(Ptr +3) == ')' ) {
								PtrEnd = Ptr +3;
								break;
							}
						}
						else {
							if( *Ptr == '=' && *(Ptr +2) == ')' ) {
								PtrEnd = Ptr +2;
								break;
							}
						}
						Ptr ++;
					}
					// SI FIN DE FONCTION ABSENTE ALORS QUIT L'ANALIZE DE FIN DE CHAINE
					if( *PtrEnd != ')') break;
					// PREPARE LA CHAINE POUR ANALYSE
					String = g_strnfill ((PtrEnd - PtrBegin) *2, '\0');
					strncpy(String, PtrBegin +1, PtrEnd - PtrBegin);
					// g_print("String = %s\n", String);
					Parse_allocate_function (p_ParseType, CD_TAG_CAR_REPLACE, String);
					g_free (String);
					String = NULL;
					PtrTemplate = (PtrEnd +1);
				} else break;
			}
			// Tetsumaki UPPER
			// else if( *(PtrTemplate +1) == 'T' && *(PtrTemplate +2) == 'u' ) {
			// 	PtrTemplate += 3;
			// 	g_print("Tetsumaki UPPER : %%Tu\n");				
			// }
			// Tetsumaki LOWER
			else if( *(PtrTemplate +1) == 'T' && *(PtrTemplate +2) == 'l' ) {
				PtrTemplate += 3;
				Parse_allocate (p_ParseType, CD_TAG_TETSUMAKI_LOWER, NULL);
			}
			
			// CD_TAG_WITHOUT_ACCENT
			// 
			else if( *( PtrTemplate +1 ) == 'n' && *( PtrTemplate +2 ) == 'a'  ) {
				Parse_allocate (p_ParseType, CD_TAG_WITHOUT_ACCENT, NULL);
				PtrTemplate += 3;
				BoolTagOk = TRUE;
			}
			
			else {
				Str [ 0 ] = *PtrTemplate;
				Str [ 1 ] = '\0';
				Parse_allocate (p_ParseType, CD_TAG_DEFAULT, Str);
				PtrTemplate ++;
			}
		}
		else {
			if( *PtrTemplate < 0 ) {
				Str [ 0 ] = *PtrTemplate;
				Str [ 1 ] = *(PtrTemplate +1);
				Str [ 2 ] = '\0';
				PtrTemplate += 2;
			} else {
				Str [ 0 ] = *PtrTemplate;
				Str [ 1 ] = '\0';
				PtrTemplate ++;
			}
			Parse_allocate (p_ParseType, CD_TAG_DEFAULT, Str);

			if( *Str == '/' ) {
				gchar *Ptr = PtrTemplate;
				while( Ptr > BeginPtrTemplate && *Ptr != '/' ) Ptr --;
				/*
				if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
					if( Ptr > BeginPtrTemplate ) {
						if( *(Ptr -1) == '/' ) {
							g_print( "DOUBLON  PARSE_TYPE_STOCKAGE_CD\n" );
							BoolSet = TRUE;
							*( NewPtrTemplate + (gint)(Ptr - BeginPtrTemplate) ) = '_';
						}
					}
				} else 
				*/
				if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
					*( NewPtrTemplate + (gint)(Ptr - BeginPtrTemplate) ) = '_';
					BoolSet = TRUE;
				}
			}
		}
	}
	
	if( BoolTagOk == FALSE ) {
		// SI LISTE VIDE -> CREATION D'UN MINIMUM AVEC LE TITRE
		// 
		// Fwd: Bug#673641: xcfa: Wrong file directory
		// http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=673641
		// SOLVED
		
		// Parse_allocate (p_ParseType, CD_TAG_TITLE, NULL);
	}
	
	if( TRUE == BoolSet ) {
		if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
			gtk_entry_set_text( GTK_ENTRY(var_cd.Adr_entry_stockage_cdaudio), NewPtrTemplate );
		} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
			gtk_entry_set_text( GTK_ENTRY(var_cd.Adr_entry_new_titre_cdaudio), NewPtrTemplate );
		}
	}
	
	// Parse_debug (p_ParseType);
}
// 
// RECUPERATION DU PARSE DE LA LIGNE: p_Num_cell(0 .. n-1) 
// 
gchar *Parse_get_line (PARSE_TYPE p_ParseType, gint p_Num_cell)
{
	CD_AUDIO	*Audio = NULL;
	GList		*ListCD = NULL;
	GList		*ListParse = NULL;
	PARSE		*Parse = NULL;
	GString		*gstr = NULL;
	GString		*gstrNew = NULL;
	gchar		*Ptr = NULL;
	gchar		*Str = NULL;
	
	ListCD = g_list_nth (EnteteCD.GList_Audio_cd, p_Num_cell);
	if( NULL != (Audio = (CD_AUDIO *)ListCD->data) ) {
	
		gstr = g_string_new (NULL);
		
		if( NULL == Audio->tags ) {
			g_string_append_printf (gstr, "Track_%02d", p_Num_cell +1);
			Str = g_strdup (gstr->str);
			g_string_free (gstr, TRUE);
			gstr = NULL;
			return (Str);
		}
		
		// QUELLE LIGNE ANALISER ?
		if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
			ListParse = g_list_first (VarParse.ListParseStockCd);
		} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
			ListParse = g_list_first (VarParse.ListParseTitleCd);
		}
		// LISTE PARSE
		while(  ListParse ) {
			if( NULL != (Parse = (PARSE *)ListParse->data) ) {
				switch (Parse->CdTypeTag ) {
				case CD_TAG_ARTIST :
					// g_print("CD_TAG_ARTIST\n");
					// g_print("%s\n", Audio->tags->Artist);
					if( NULL != Parse->StringDefault ) {
						g_string_append_printf (gstr, "%s", Parse->StringDefault);
					}
					else {
						g_string_append_printf (gstr, "%s", Audio->tags->Artist);
					}
					break;
				case CD_TAG_ALBUM :
					// g_print("CD_TAG_ALBUM\n");
					// g_print("%s\n", Audio->tags->Album);
					if( NULL != Parse->StringDefault ) {
						g_string_append_printf (gstr, "%s", Parse->StringDefault);
					}
					else {
						g_string_append_printf (gstr, "%s", Audio->tags->Album);
					}
					break;
				case CD_TAG_INT_NUMBER :
					// g_print("CD_TAG_INT_NUMBER\n");
					// g_print("%02d\n", Audio->tags->IntNumber);
					g_string_append_printf (gstr, "%02d", Audio->tags->IntNumber);
					break;
				case CD_TAG_TITLE :
					// g_print("CD_TAG_TITLE\n");
					// g_print("'%s'\n", Audio->tags->Title);
					g_string_append_printf (gstr, "%s", Audio->tags->Title);
					break;
				case CD_TAG_YEAR :
					// g_print("CD_TAG_YEAR\n");
					// g_print("%s\n", Audio->tags->Year);
					g_string_append_printf (gstr, "%s", Audio->tags->Year);
					break;
				case CD_TAG_GENRE :
					// g_print("CD_TAG_GENRE\n");
					// g_print("%s\n", Audio->tags->Genre);
					g_string_append_printf (gstr, "%s", Audio->tags->Genre);
					break;
				case CD_TAG_FILE_M3U_XSPF :
					// g_print("CD_TAG_FILE_M3U_XSPF\n");
					// g_print("BoolCreateFileM3uXspf = %s\n", Parse->BoolCreateFileM3uXspf ? "TRUE" : "FALSE");
					var_cd.Bool_create_file_m3u = FALSE;
					var_cd.Bool_create_file_m3u = Parse->BoolCreateFileM3uXspf;
					break;
				case CD_TAG_CAR_REPLACE :
					// LES REMPLACEMENT DE CARACTERES SE FONT A LA FIN ...
					// g_print("CD_TAG_REPLACE\n");
					// g_print("%s ==> %s\n", Parse->StringReplaceOld,  Parse->StringReplaceNew);
					break;
				case CD_TAG_TETSUMAKI_LOWER :
				case CD_TAG_WITHOUT_ACCENT :
					break;
				case CD_TAG_DEFAULT :
					// g_print("CD_TAG_DEFAULT\n");
					// g_print("%s\n", Parse->StringDefault);
					g_string_append_printf (gstr, "%s", Parse->StringDefault);
					break;
				}
			}
			// STRUCTURE SUIVANTE
			ListParse = g_list_next (ListParse);
		}
		
		
		// QUELLE LIGNE ANALISER ?
		if( p_ParseType == PARSE_TYPE_STOCKAGE_CD ) {
			ListParse = g_list_first (VarParse.ListParseStockCd);
		} else if( p_ParseType == PARSE_TYPE_TITLE_CD ) {
			ListParse = g_list_first (VarParse.ListParseTitleCd);
		}
		
		// LISTE PARSE: REMPLACEMENT DE CARACTERES
		while(  ListParse ) {
			if( NULL != (Parse = (PARSE *)ListParse->data) ) {
				if( CD_TAG_CAR_REPLACE == Parse->CdTypeTag ) {
					gstrNew = g_string_new (NULL);
					Ptr = gstr->str;
					while(  *Ptr ) {
						// SI LES CARACTERES A COMPARER SONT DU MEME TYPE
						if( *Parse->StringReplaceOld < 0 && *(Parse->StringReplaceOld +1) < 0 ) {
							if( *Ptr < 0 && *(Ptr +1) < 0 ) {
								// REMPLACEMENT POSSIBLE ?
								if( *Parse->StringReplaceOld == *Ptr && *(Parse->StringReplaceOld +1) == *(Ptr +1) ) {
									g_string_append_printf (gstrNew, "%s", Parse->StringReplaceNew);
									Ptr += 2;
									continue;
								}
							}
						// SI LES CARACTERES A COMPARER SONT DU MEME TYPE
						} else if( *Parse->StringReplaceOld > 0 ) {
							if( *Ptr > 0 ) {
								// REMPLACEMENT POSSIBLE ?
								if( *Parse->StringReplaceOld == *Ptr ) {
									g_string_append_printf (gstrNew, "%s", Parse->StringReplaceNew);
									Ptr ++;
									continue;
								}
							}
						}
						g_string_append_printf (gstrNew, "%c", *Ptr);	
						Ptr ++;
					}
					g_string_free (gstr, TRUE);
					gstr = NULL;
					gstr = g_string_new (NULL);
					g_string_append_printf (gstr, "%s", gstrNew->str);
					g_string_free (gstrNew, TRUE);
					gstrNew = NULL;
				}
				
				// DEMANDE DE @Tetsumaki
				// http://forum.ubuntu-fr.org/viewtopic.php?pid=3973950#p3973950
				// TOUTES LES LETTRES EN MINUSCULE SAUF LA PREMIERE ANSI QUE LES CARACTERES PRECEDES PAR '%'
				else if( CD_TAG_TETSUMAKI_LOWER == Parse->CdTypeTag ) {
					
					gboolean	BoolAlphaOk = FALSE;
					
					Ptr = gstr->str;
					while(  *Ptr ) {
						if( '%' == *Ptr ) {
							strcpy (Ptr, Ptr + 1);
						}
						else {
							*Ptr = g_ascii_tolower (*Ptr);
						
							if( FALSE == BoolAlphaOk ) {
								if( TRUE == g_ascii_isalpha (*Ptr) ) {
									*Ptr = g_ascii_toupper (*Ptr);
									BoolAlphaOk = TRUE;
								}
							}

						}
						Ptr ++;
					}
				}
				
				// CD_TAG_WITHOUT_ACCENT
				// 
				// A la demande de @mdos, fonction permettant de supprimer
				// tous les accents depuis un nom pour un futur fichier
				// 
				else if( CD_TAG_WITHOUT_ACCENT == Parse->CdTypeTag ) {
					
					gchar	*New = NULL;
					
					Ptr = gstr->str;
					
					while(  *Ptr ) {

						if( *Ptr < 0  ) {
							
							// g_print ("OLD = %s\n", gstr->str);
							New = utf8_removediacritics( gstr->str, -1 );
							// g_print ("NEW = %s\n", New );
							g_string_free (gstr, TRUE);
							gstr = g_string_new (NULL);
							// g_string_append_printf( gstr, New );
							g_string_append( gstr, New );
							g_free( New );
							New = NULL;
							
							break;
						}
						Ptr ++;
					}
				}
			}
			// STRUCTURE SUIVANTE
			ListParse = g_list_next (ListParse);
		}
		
		// ERRADICATION DE SLASH
		// DEUX CARACTERES SLASH ( // ) NE PEUVENT ETRE COTE A COTE
		Ptr = gstr->str;
		while(  *Ptr ) {
			if( '/' == *Ptr && '/' == *(Ptr +1) ) {
				*Ptr = '_';
			}
			Ptr ++;
		}
	}
	Str = g_strdup (gstr->str);
	g_string_free (gstr, TRUE);
	gstr = NULL;
	return (Str);
}

	








