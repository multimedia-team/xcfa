 /*
 *  file      : file.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <stdio.h>
#include <stdlib.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "fileselect.h"
#include "tags.h"
#include "prg_init.h"
#include "dragNdrop.h"
#include "win_info.h"
#include "get_info.h"
#include "play_file.h"
#include "popup.h"
#include "file.h"
#include "statusbar.h"



VAR_FILE var_file;

enum
{
	COLUMN_FILE_PLAY = 0,
	COLUMN_FILE_TRASH,
	COLUMN_FILE_TYPE,
	COLUMN_FILE_WAV,
	COLUMN_FILE_FLAC,
	COLUMN_FILE_APE,
	COLUMN_FILE_WAVPACK,
	COLUMN_FILE_OGG,
	COLUMN_FILE_M4A,
	COLUMN_FILE_AAC,
	COLUMN_FILE_MPC,
	COLUMN_FILE_MP3,
	COLUMN_FILE_TIME,
	COLUMN_FILE_NORMALIZE,
	COLUMN_FILE_REPLAYGAIN,
	COLUMN_FILE_NAME,
	COLUMN_FILE_EDITABLE,

	COLUMN_FILE_POINTER_STRUCT,

	COLUMN_FILE_NUM
};
enum
{
	NUM_TREE_FILE_Play = 0,
	NUM_TREE_FILE_Trash,
	NUM_TREE_FILE_Type,
	NUM_TREE_FILE_Wav,
	NUM_TREE_FILE_Flac,
	NUM_TREE_FILE_Ape,
	NUM_TREE_FILE_WavP,
	NUM_TREE_FILE_Ogg,
	NUM_TREE_FILE_M4a,
	NUM_TREE_FILE_Aac,
	NUM_TREE_FILE_Mpc,
	NUM_TREE_FILE_Mp3,
	NUM_TREE_FILE_Time,
	NUM_TREE_FILE_Normalise,
	NUM_TREE_FILE_ReplayGain,
	NUM_TREE_FILE_Name,

	NUM_TREE_FILE_ALL_COLUMN
};


typedef struct {
	glong	SizeTmpKo;
	gint	NbrFile;
	glong	SizeFileKo;
} SIZE_TMP;





//
//
gboolean file_get_bool_ReplaygainApply (void)
{
	GList   *list = NULL;
	DETAIL  *detail = NULL;

	list = g_list_first (entetefile);
	while (list) {
		if ((detail = (DETAIL *)list->data)) {
			if (detail->type_infosong_file_is == FILE_IS_OGG ||
				detail->type_infosong_file_is == FILE_IS_MP3 ||
				detail->type_infosong_file_is == FILE_IS_FLAC ||
				detail->type_infosong_file_is == FILE_IS_WAVPACK) {

				if (detail->Etat_ReplayGain != RPG_EFFACER && detail->Etat_ReplayGain != RPG_ATTENTE) {
				// if (detail->Etat_ReplayGain != RPG_ATTENTE) {
					return (TRUE);
				}
			}
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
//
//
gboolean file_get_bool_is_conversion (void)
{
	GList   *list = NULL;
	DETAIL  *detail = NULL;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (detail->EtatSelection_Flac > ETAT_ATTENTE_EXIST ||
			    detail->EtatSelection_Wav > ETAT_ATTENTE_EXIST ||
			    detail->EtatSelection_Mp3 > ETAT_ATTENTE_EXIST ||
			    detail->EtatSelection_Ogg > ETAT_ATTENTE_EXIST ||
			    detail->EtatSelection_M4a > ETAT_ATTENTE_EXIST ||
			    detail->EtatSelection_Aac > ETAT_ATTENTE_EXIST ||
			    detail->EtatSelection_Mpc > ETAT_ATTENTE_EXIST ||
			    detail->EtatSelection_Ape > ETAT_ATTENTE_EXIST ||
			    detail->EtatSelection_WavPack > ETAT_ATTENTE_EXIST) return (TRUE);
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
//
//
void file_get_etat (guint *total, guint *total_select)
{
	gboolean     valid;
	GtkTreeIter  iter;
	DETAIL      *detail = NULL;

	*total        = 0;
	*total_select = 0;

	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		(*total) ++;

		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if (NULL != detail) {
			if (detail->EtatSelection_Flac > ETAT_ATTENTE_EXIST)	(*total_select) ++;
			if (detail->EtatSelection_Wav > ETAT_ATTENTE_EXIST)	(*total_select) ++;
			if (detail->EtatSelection_Mp3 > ETAT_ATTENTE_EXIST)	(*total_select) ++;
			if (detail->EtatSelection_Ogg > ETAT_ATTENTE_EXIST)	(*total_select) ++;
			if (detail->EtatSelection_M4a > ETAT_ATTENTE_EXIST)	(*total_select) ++;
			if (detail->EtatSelection_Aac > ETAT_ATTENTE_EXIST)	(*total_select) ++;
			if (detail->EtatSelection_Mpc > ETAT_ATTENTE_EXIST)	(*total_select) ++;
			if (detail->EtatSelection_Ape > ETAT_ATTENTE_EXIST)	(*total_select) ++;
			if (detail->EtatSelection_WavPack > ETAT_ATTENTE_EXIST) (*total_select) ++;

			if (detail->Etat_Normalise == NORM_RMS_FIX) {

				gint Level = 0;
				gint NewLevel = 0;

				if (detail->type_infosong_file_is == FILE_IS_WAV) {
					INFO_WAV *info = (INFO_WAV *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				else if (detail->type_infosong_file_is == FILE_IS_OGG) {
					INFO_OGG *info = (INFO_OGG *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				else if (detail->type_infosong_file_is == FILE_IS_MP3) {
					INFO_MP3 *info = (INFO_MP3 *)detail->info;
					Level    = info->LevelDbfs.level;
					NewLevel = info->LevelDbfs.NewLevel;
				}
				if (Level != NewLevel) (*total_select) ++;
			}
			else if (detail->Etat_Normalise > NORM_RMS_FIX) {
				(*total_select) ++;
			}

			if (detail->Etat_ReplayGain > RPG_ATTENTE) (*total_select) ++;
		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}
}
//
//
gboolean file_get_line_is_selected  (void)
{
	GtkTreeModel     *model = NULL;
	GList            *list = NULL;

	if (var_file.Adr_TreeView == NULL) return (FALSE);
	if (var_file.Adr_Line_Selected == NULL) return (FALSE);
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file.Adr_TreeView));
	list = gtk_tree_selection_get_selected_rows (var_file.Adr_Line_Selected, &model);
	return (list ? TRUE : FALSE);
}
//
//
gboolean file_get_etat_normalise (void)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {

			if (detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_WAV) {

				if (detail->Etat_Normalise > NORM_READY_FOR_SELECT) return (TRUE);
			}
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
//
//
gboolean file_get_etat_replaygain (void)
{
	GList   *list = NULL;
	DETAIL  *detail = NULL;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (detail->type_infosong_file_is == FILE_IS_OGG ||
				detail->type_infosong_file_is == FILE_IS_MP3 ||
				detail->type_infosong_file_is == FILE_IS_FLAC ||
				detail->type_infosong_file_is == FILE_IS_WAVPACK) {

				if (detail->Etat_ReplayGain != RPG_ATTENTE) return (TRUE);
			}
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
//
//
void file_set_flag_buttons (void)
{
	gchar	*Ptr = NULL;
	guint	total;
	guint	total_select;
	gboolean      BoolButtonsAudio [ 5 ];
	
	// SUPPRESSION
	BoolButtonsAudio [ 0 ] = file_get_line_is_selected  ();
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_file")),    BoolButtonsAudio [ 0 ]);
	// SELECTION PAR TYPE
	BoolButtonsAudio [ 1 ] = (NULL == entetefile)  ? FALSE : TRUE;
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("eventbox_combobox_select_type")),    BoolButtonsAudio [ 1 ]);
	//
	BoolButtonsAudio [ 2 ] = file_get_etat_normalise ();
	gtk_widget_set_sensitive (GTK_WIDGET (GTK_COMBO_BOX (var_file.AdrComboboxNormalise)), BoolButtonsAudio [ 2 ]);
	switch (gtk_combo_box_get_active (GTK_COMBO_BOX (var_file.AdrComboboxNormalise))) {
	case 0:
	case 1:
		gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), FALSE);
		break;
	case 2:
	case 3:
		if (gtk_widget_is_sensitive(GTK_WIDGET(var_file.AdrComboboxNormalise)) == TRUE) {
			gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), TRUE);
		}
		else {
			gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), FALSE);
		}
		break;
	}
	//
	BoolButtonsAudio [ 3 ] = file_get_etat_normalise () ||
				 file_get_etat_replaygain () ||
				 file_get_bool_is_conversion ();
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_conv_file")),    BoolButtonsAudio [ 3 ]);

	file_get_etat (&total, &total_select);
	Ptr = g_strdup_printf (_("<b>Total files: %d,  Selections: %d</b>"), total, total_select);
	if (NULL != var_file.Adr_Label_Number) {
		gtk_label_set_use_markup (GTK_LABEL (var_file.Adr_Label_Number), TRUE);
		gtk_label_set_markup (GTK_LABEL (var_file.Adr_Label_Number), Ptr);
	}
	g_free (Ptr);
	Ptr = NULL;
}
//
//
gboolean file_peak_get_size_is_ok (ETAT_NORMALISE p_EtatNormalise, SIZE_TMP *SizeTmp)
{
	GtkTreeIter	 iter;
	gboolean	 valid;
	DETAIL		*detail = NULL;
	glong		 AllSize = 0;
	glong		 SizeRepTmp;

	if (SizeTmp != NULL) {
		SizeTmp->SizeTmpKo   = 0;
		SizeTmp->NbrFile     = 0;
		SizeTmp->SizeFileKo  = 0;
	}
	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if ((NULL != detail) &&
		    (detail->type_infosong_file_is == FILE_IS_MP3 ||
		    detail->type_infosong_file_is == FILE_IS_OGG)) {

			if (detail->Etat_Normalise == p_EtatNormalise) {

				AllSize += (glong)atol (file_get_size (detail));

				if (SizeTmp != NULL) {
					SizeTmp->NbrFile ++;
					SizeTmp->SizeFileKo = AllSize;
				}
			}
		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}

	if (AllSize > 0) {
		/* 12 est l'indice de multiplication permettant de determiner la taille vers le format WAV
		*/
		AllSize = AllSize * 12;
		if (SizeTmp != NULL) {
			SizeTmp->SizeFileKo *= 12;
			SizeTmp->SizeFileKo /= 1024;
		}

		/*
		g_print ("\tOCTET  %lu\n", AllSize);
		g_print ("\tKO     %lu\n", AllSize / 1024);
		g_print ("\tMEGA   %lu\n", (AllSize / 1024) / 1024);
		g_print ("\tGIGA   %lu\n", ((AllSize / 1024) / 1024) / 1024);
		g_print ("\n");

		df --block-size=K /tmp

		Sys. de fich.  1K-blocs   Occupé    Disponible  Capacité Monté sur
		/dev/sda5      19686772K  4080984K  14605748K   22%      /

		AllSize        = taille en octets
		AllSize / 1024 = taille en KiloOctet
		*/

		SizeRepTmp = GetInfo_level_df ();
		if (SizeTmp != NULL) {
			SizeTmp->SizeTmpKo   = SizeRepTmp;
		}
		/*g_print("\tSizeRepTmp = %lu\n", SizeRepTmp);*/
		/*g_print("\tAllSize / 1024 = %lu\n", AllSize / 1024);*/
		if (AllSize / 1024 >= SizeRepTmp) {
			return (FALSE);
		}

	}
	return (TRUE);
}
//
//
void on_button_apply_file_clicked (GtkButton *button, gpointer user_data)
{
	SIZE_TMP SizeTmp;

	/*if (GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_conv_file"))) == button) {
		g_print(">NOTEBOOK_FICHIERS_CONVERSION :\n");
	}
	else if (GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_apply_wav"))) == button) {
		g_print(">NOTEBOOK_FICHIERS_WAV :\n");
	}
	else if (GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_applique_file_mp3ogg"))) == button) {
		g_print(">NOTEBOOK_FICHIERS_MP3OGG :\n");

	}
	else if (GTK_BUTTON (GTK_WIDGET (GLADE_GET_OBJECT("button_tag_appliquer"))) == button) {
		g_print(">NOTEBOOK_FICHIERS_TAGS :\n");
	}*/
	if (NOTEBOOK_FICHIERS == Config.NotebookGeneral) {
		switch (Config.NotebookFile) {
		case NOTEBOOK_FICHIERS_CONVERSION :
			// g_print("NOTEBOOK_FICHIERS_CONVERSION :\n");
			if (file_peak_get_size_is_ok (NORM_PEAK_ALBUM, &SizeTmp) == FALSE) {

				gchar *Str1 = NULL;
				gchar *Str2 = NULL;

				Str1 = g_strdup_printf ("<b>   %s/  = %lu Ko</b>", Config.PathnameTMP, SizeTmp.SizeTmpKo);
				Str2 = g_strdup_printf ("<b>   %lu Ko en selection</b>", SizeTmp.SizeFileKo);

				wind_info_init (
					WindMain,
					_("TEMPORARY CAPACITY TOO LOW"),
					_("WARNING: Not enough space available in the temp folder."),
					  "\n",
					  Str1,
					  "\n",
					  Str2,
					  "\n\n",
					_("Please deselect PeakGroup."),
					"");

				g_free (Str1);
				Str1 = NULL;
				g_free (Str2);
				Str2 = NULL;
				return;
			}
			else {
				fileaction_choice ();
			}
			break;
		case NOTEBOOK_FICHIERS_WAV :
			// g_print("NOTEBOOK_FICHIERS_WAV :\n");
			filewavconv_apply ();
			break;
		case NOTEBOOK_FICHIERS_MP3OGG :
			// g_print("NOTEBOOK_FICHIERS_MP3OGG :\n");
			filemp3oggconv_apply_regul_mp3ogg_by_conv ();
			break;
		case NOTEBOOK_FICHIERS_TAGS :
			// g_print("NOTEBOOK_FICHIERS_TAGS :\n");
			FileTags_appliquer_clicked ();
			break;
		}

	}
}
//
//
gchar *file_get_pathname_dest (DETAIL *detail, gchar *NewExt)
{
	gchar	*PathName = NULL;
	gchar	*Dummy = NULL;
	gchar	*Ptr = NULL;

	switch (gtk_combo_box_get_active (var_file.Adr_combobox_DestFile)) {
	// CONVERSION VERS LA SOURCE
	case 0 :
		Dummy = g_strdup (detail->namefile);
		if (NULL != (Ptr = strrchr (Dummy, '/'))) *Ptr = '\0';
		PathName = g_strdup_printf ("%s/%s.%s", Dummy, detail->NameDest, NewExt);
		g_free (Dummy);
		Ptr = Dummy = NULL;
		break;

	// CHOIX D UNE NOUVELLE DESTINATION
	case 1 :
		break;
	// CONVERSION VERS UNE NOUVELLE DESTINATION
	case 2 :
		switch (Config.NotebookFile) {
		case NOTEBOOK_FICHIERS_CONVERSION:
			PathName = g_strdup_printf ("%s/%s.%s", Config.PathDestinationFileAll, detail->NameDest, NewExt);
			break;
		case NOTEBOOK_FICHIERS_WAV :
			PathName = g_strdup_printf ("%s/%s.%s", Config.PathDestinationFileWav, detail->NameDest, NewExt);
			break;
		case NOTEBOOK_FICHIERS_MP3OGG :
			PathName = g_strdup_printf ("%s/%s.%s", Config.PathDestinationFileMp3Ogg, detail->NameDest, NewExt);
			break;
		}
		break;
	}
	return ((gchar *)PathName);
}
// RENVOIE TRUE SI LE FICHIER EXISTE SINON FALSE
//
gboolean file_test_with_new_ext_exist (DETAIL *detail, gchar *NewExt)
{
	gchar		*NewPathName = file_get_pathname_dest (detail, NewExt);
	gboolean	BoolFileExist;

	BoolFileExist = libutils_test_file_exist (NewPathName);

	g_free (NewPathName);
	NewPathName = NULL;

	return (BoolFileExist);
}
//
//
void on_label_number_files_realize (GtkWidget *widget, gpointer user_data)
{
	var_file.Adr_Label_Number = widget;
}
//
//
gint file_get_scan (void)
{
	DETAIL         *detail = NULL;
	GtkTreeIter     iter;
	gboolean        valid;
	gint		compteur = 0;

	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if (NULL != detail) {
			if(detail->type_infosong_file_is == FILE_IS_WAV ||
			    detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG) {

				if (detail->Etat_Scan == ETAT_SCAN_DEMANDE) compteur ++;
			}
		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}
	return (compteur);
}
//
//
gchar *file_get_size (DETAIL *detail)
{
	gchar *ptr = NULL;

	if (detail->type_infosong_file_is == FILE_IS_MP3) {
		INFO_MP3 *info = (INFO_MP3 *)detail->info;
		ptr = info->size;
	}
	else if (detail->type_infosong_file_is == FILE_IS_OGG) {
		INFO_OGG *info = (INFO_OGG *)detail->info;
		ptr = info->size;
	}
	return (ptr);
}
//
//
gchar *file_get_time (DETAIL *detail)
{
	gchar *ptr = NULL;

	if (detail->type_infosong_file_is == FILE_IS_FLAC) {
		INFO_FLAC *info = (INFO_FLAC *)detail->info;
		ptr = info->time;
	}
	else if (detail->type_infosong_file_is == FILE_IS_WAV) {
		INFO_WAV *info = (INFO_WAV *)detail->info;
		ptr = info->time;
	}
	else if (detail->type_infosong_file_is == FILE_IS_MP3) {
		INFO_MP3 *info = (INFO_MP3 *)detail->info;
		ptr = info->time;
	}
	else if (detail->type_infosong_file_is == FILE_IS_OGG) {
		INFO_OGG *info = (INFO_OGG *)detail->info;
		ptr = info->time;
	}
	else if (detail->type_infosong_file_is == FILE_IS_M4A) {
		INFO_M4A *info = (INFO_M4A *)detail->info;
		ptr = info->time;
	}
	else if (detail->type_infosong_file_is == FILE_IS_VID_M4A) {
		INFO_M4A *info = (INFO_M4A *)detail->info;
		ptr = info->time;
	}
	else if (detail->type_infosong_file_is == FILE_IS_AAC) {
		INFO_AAC *info = (INFO_AAC *)detail->info;
		if ((INFO_AAC *)info)
			ptr = info->time;
	}
	/* Infos obtenues via SHNTOOL
	*/
	else if (detail->type_infosong_file_is == FILE_IS_APE) {
		INFO_APE *info = (INFO_APE *)detail->info;
		ptr = info->time;
	}
	else if (detail->type_infosong_file_is == FILE_IS_SHN) {
		INFO_SHN *info = (INFO_SHN *)detail->info;
		ptr = info->time;
	}
	else if (detail->type_infosong_file_is == FILE_IS_WAVPACK) {
		INFO_WAVPACK *info = (INFO_WAVPACK *)detail->info;
		ptr = info->time;
	}
	return ((gchar *)ptr);
}
//
//
typedef struct {
	TYPE_FILE_IS	From;
	TYPE_FILE_IS	To;
	gchar		*Conv1;
	gboolean	*Bool1;
	gchar		*Conv2;
	gboolean	*Bool2;
} TST_FROM;

TST_FROM TstFrom[] = {

{FILE_IS_AC3,		FILE_IS_AC3,			"NORMALISE",				&PrgInit.bool_a52dec,		NULL,		NULL },
{FILE_IS_AC3,		FILE_IS_WAV,			"NORMALISE",				&PrgInit.bool_a52dec,		NULL,		NULL },
{FILE_IS_AC3,		FILE_IS_FLAC,			"NORMALISE",				&PrgInit.bool_a52dec, 		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_AC3,		FILE_IS_APE,			"MPLAYER", 				&PrgInit.bool_a52dec, 		"MAC",		&PrgInit.bool_ape },
{FILE_IS_AC3,		FILE_IS_WAVPACK,		"MPLAYER", 				&PrgInit.bool_a52dec,		"WAVPACK",	&PrgInit.bool_wavpack },
{FILE_IS_AC3,		FILE_IS_OGG,			"MPLAYER", 				&PrgInit.bool_a52dec, 		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_AC3,		FILE_IS_M4A,			"FAAC", 				&PrgInit.bool_a52dec, 		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_AC3,		FILE_IS_AAC,			"AACPLUSENC", 				&PrgInit.bool_a52dec, 		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_AC3,		FILE_IS_MPC,			"MPLAYER", 				&PrgInit.bool_a52dec, 		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_AC3,		FILE_IS_MP3,			"MPLAYER", 				&PrgInit.bool_a52dec,		"LAME",		&PrgInit.bool_lame },

{FILE_IS_WMA,		FILE_IS_WMA,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_WMA,		FILE_IS_WAV,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_WMA,		FILE_IS_MP3,			"MPLAYER",				&PrgInit.bool_mplayer,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_WMA,		FILE_IS_OGG,			"MPLAYER",				&PrgInit.bool_mplayer, 		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_WMA,		FILE_IS_FLAC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_WMA,		FILE_IS_M4A,			"MPLAYER",				&PrgInit.bool_mplayer, 		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_WMA,		FILE_IS_AAC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_WMA,		FILE_IS_MPC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_WMA,		FILE_IS_APE,			"MPLAYER",				&PrgInit.bool_mplayer, 		"MAC",		&PrgInit.bool_ape },
{FILE_IS_WMA,		FILE_IS_WAVPACK,		"MPLAYER",				&PrgInit.bool_mplayer,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_RM,		FILE_IS_RM,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_RM,		FILE_IS_WAV,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_RM,		FILE_IS_MP3,			"MPLAYER",				&PrgInit.bool_mplayer,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_RM,		FILE_IS_OGG,			"MPLAYER",				&PrgInit.bool_mplayer, 		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_RM,		FILE_IS_FLAC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_RM,		FILE_IS_M4A,			"MPLAYER",				&PrgInit.bool_mplayer, 		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_RM,		FILE_IS_AAC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_RM,		FILE_IS_MPC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_RM,		FILE_IS_APE,			"MPLAYER",				&PrgInit.bool_mplayer, 		"MAC",		&PrgInit.bool_ape },
{FILE_IS_RM,		FILE_IS_WAVPACK,		"MPLAYER",				&PrgInit.bool_mplayer,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_DTS,		FILE_IS_DTS,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_DTS,		FILE_IS_WAV,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_DTS,		FILE_IS_MP3,			"MPLAYER",				&PrgInit.bool_mplayer,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_DTS,		FILE_IS_OGG,			"MPLAYER",				&PrgInit.bool_mplayer, 		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_DTS,		FILE_IS_FLAC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_DTS,		FILE_IS_M4A,			"MPLAYER",				&PrgInit.bool_mplayer, 		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_DTS,		FILE_IS_AAC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_DTS,		FILE_IS_MPC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_DTS,		FILE_IS_APE,			"MPLAYER",				&PrgInit.bool_mplayer, 		"MAC",		&PrgInit.bool_ape },
{FILE_IS_DTS,		FILE_IS_WAVPACK,		"MPLAYER",				&PrgInit.bool_mplayer,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_AIFF,		FILE_IS_AIFF,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_AIFF,		FILE_IS_WAV,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_AIFF,		FILE_IS_MP3,			"MPLAYER",				&PrgInit.bool_mplayer,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_AIFF,		FILE_IS_OGG,			"MPLAYER",				&PrgInit.bool_mplayer, 		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_AIFF,		FILE_IS_FLAC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_AIFF,		FILE_IS_M4A,			"MPLAYER",				&PrgInit.bool_mplayer, 		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_AIFF,		FILE_IS_AAC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_AIFF,		FILE_IS_MPC,			"MPLAYER",				&PrgInit.bool_mplayer, 		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_AIFF,		FILE_IS_APE,			"MPLAYER",				&PrgInit.bool_mplayer, 		"MAC",		&PrgInit.bool_ape },
{FILE_IS_AIFF,		FILE_IS_WAVPACK,		"MPLAYER",				&PrgInit.bool_mplayer,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_WAV,		FILE_TO_NORMALISE,		"NORMALISE",				&PrgInit.bool_normalize,	NULL,		NULL },
{FILE_IS_WAV,		FILE_TO_NORMALISE_COLLECTIF,	"NORMALISE",				&PrgInit.bool_normalize,	NULL,		NULL },
{FILE_IS_WAV,		FILE_IS_MP3,			"MPLAYER", 				&PrgInit.bool_mplayer,		"LAME", 	&PrgInit.bool_lame },
{FILE_IS_WAV,		FILE_IS_OGG,			"MPLAYER", 				&PrgInit.bool_mplayer,		"OGGENC", 	&PrgInit.bool_oggenc },
{FILE_IS_WAV,		FILE_IS_FLAC,			"MPLAYER", 				&PrgInit.bool_mplayer,		"FLAC", 	&PrgInit.bool_flac },
{FILE_IS_WAV,		FILE_IS_M4A,			"FAAC", 				&PrgInit.bool_faac,		NULL,		NULL },
{FILE_IS_WAV,		FILE_IS_AAC,			"AACPLUSENC", 				&PrgInit.bool_aacplusenc,	NULL,		NULL },
{FILE_IS_WAV,		FILE_IS_MPC,			"MPLAYER", 				&PrgInit.bool_mplayer,		"MPPENC", 	&PrgInit.bool_mppenc },
{FILE_IS_WAV,		FILE_IS_APE,			"MPLAYER", 				&PrgInit.bool_mplayer,		"MAC", 		&PrgInit.bool_ape },
{FILE_IS_WAV,		FILE_IS_WAVPACK,		"MPLAYER", 				&PrgInit.bool_mplayer,		"WAVPACK", 	&PrgInit.bool_wavpack },

{FILE_IS_FLAC,		FILE_TO_REPLAYGAIN,		"METAFLAC", 				&PrgInit.bool_flac,		NULL,		NULL },
{FILE_IS_FLAC,		FILE_IS_WAV,			"FLAC",					&PrgInit.bool_flac,		NULL,		NULL },
{FILE_IS_FLAC,		FILE_IS_MP3,			"FLAC", 				&PrgInit.bool_flac,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_FLAC,		FILE_IS_OGG,			"OGGENC", 				&PrgInit.bool_oggenc,		NULL,		NULL },
{FILE_IS_FLAC,		FILE_IS_M4A,			"FLAC", 				&PrgInit.bool_flac,		"FAAC", 	&PrgInit.bool_faac },
{FILE_IS_FLAC,		FILE_IS_AAC,			"FLAC",					&PrgInit.bool_flac,		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_FLAC,		FILE_IS_MPC,			"FLAC",					&PrgInit.bool_flac,		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_FLAC,		FILE_IS_APE,			"FLAC",					&PrgInit.bool_flac,		"MAC",		&PrgInit.bool_ape },
{FILE_IS_FLAC,		FILE_IS_WAVPACK,		"FLAC",					&PrgInit.bool_flac,		"WAVPACK",	&PrgInit.bool_wavpack },
{FILE_IS_MP3,		FILE_TO_REPLAYGAIN,		"MP3GAIN", 				&PrgInit.bool_mp3gain,		NULL,		NULL },

{FILE_IS_MP3,		FILE_TO_NORMALISE,		"NORMALISE",				&PrgInit.bool_normalize,	NULL,		NULL },
{FILE_IS_MP3,		FILE_TO_NORMALISE_COLLECTIF,	"NORMALISE",				&PrgInit.bool_normalize,	NULL,		NULL },
{FILE_IS_MP3,		FILE_IS_MP3,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_MP3,		FILE_IS_WAV,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_MP3,		FILE_IS_OGG,			"MPLAYER",				&PrgInit.bool_mplayer,		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_MP3,		FILE_IS_FLAC,			"MPLAYER",				&PrgInit.bool_mplayer,		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_MP3,		FILE_IS_M4A,			"MPLAYER",				&PrgInit.bool_mplayer,		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_MP3,		FILE_IS_AAC,			"MPLAYER",				&PrgInit.bool_mplayer,		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_MP3,		FILE_IS_MPC,			"MPLAYER",				&PrgInit.bool_mplayer,		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_MP3,		FILE_IS_APE,			"MPLAYER",				&PrgInit.bool_mplayer,		"MAC",		&PrgInit.bool_ape },
{FILE_IS_MP3,		FILE_IS_WAVPACK,		"MPLAYER",				&PrgInit.bool_mplayer,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_OGG,		FILE_TO_REPLAYGAIN,		"VORBISGAIN",				&PrgInit.bool_vorbisgain,	NULL,		NULL },
{FILE_IS_OGG,		FILE_TO_NORMALISE,		"NORMALISE",				&PrgInit.bool_normalize,	NULL,		NULL },
{FILE_IS_OGG,		FILE_TO_NORMALISE_COLLECTIF,	"NORMALISE",				&PrgInit.bool_normalize,	NULL,		NULL },
{FILE_IS_OGG,		FILE_IS_OGG,			"OGGENC",				&PrgInit.bool_oggenc,		NULL,		NULL },
{FILE_IS_OGG,		FILE_IS_WAV,			"OGGENC",				&PrgInit.bool_oggenc,		NULL,		NULL },
{FILE_IS_OGG,		FILE_IS_MP3,			"OGGENC",				&PrgInit.bool_oggenc,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_OGG,		FILE_IS_FLAC,			"OGGENC",				&PrgInit.bool_oggenc,		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_OGG,		FILE_IS_M4A,			"OGGENC",				&PrgInit.bool_oggenc,		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_OGG,		FILE_IS_AAC,			"OGGENC",				&PrgInit.bool_oggenc,		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_OGG,		FILE_IS_MPC,			"OGGENC",				&PrgInit.bool_oggenc,		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_OGG,		FILE_IS_APE,			"OGGENC",				&PrgInit.bool_oggenc,		"MAC",		&PrgInit.bool_ape },
{FILE_IS_OGG,		FILE_IS_WAVPACK,		"OGGENC",				&PrgInit.bool_oggenc,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_SHN,		FILE_IS_WAV,			"SHORTEN",				&PrgInit.bool_shorten,		NULL,		NULL },
{FILE_IS_SHN,		FILE_IS_MP3,			"SHORTEN",				&PrgInit.bool_shorten,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_SHN,		FILE_IS_OGG,			"SHORTEN",				&PrgInit.bool_shorten,		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_SHN,		FILE_IS_FLAC,			"SHORTEN",				&PrgInit.bool_shorten,		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_SHN,		FILE_IS_M4A,			"SHORTEN",				&PrgInit.bool_shorten,		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_SHN,		FILE_IS_AAC,			"SHORTEN",				&PrgInit.bool_shorten,		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_SHN,		FILE_IS_MPC,			"SHORTEN",				&PrgInit.bool_shorten,		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_SHN,		FILE_IS_APE,			"SHORTEN",				&PrgInit.bool_shorten,		"MAC",		&PrgInit.bool_ape },
{FILE_IS_SHN,		FILE_IS_WAVPACK,		"SHORTEN",				&PrgInit.bool_shorten,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_M4A,		FILE_IS_M4A,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_M4A,		FILE_IS_AAC,			"MPLAYER",				&PrgInit.bool_mplayer,		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_M4A,		FILE_IS_WAV,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_M4A,		FILE_IS_MP3,			"MPLAYER",				&PrgInit.bool_mplayer,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_M4A,		FILE_IS_OGG,			"MPLAYER",				&PrgInit.bool_mplayer,		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_M4A,		FILE_IS_FLAC,			"MPLAYER",				&PrgInit.bool_mplayer,		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_M4A,		FILE_IS_MPC,			"MPLAYER",				&PrgInit.bool_mplayer,		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_M4A,		FILE_IS_APE,			"MPLAYER",				&PrgInit.bool_mplayer,		"MAC",		&PrgInit.bool_ape },
{FILE_IS_M4A,		FILE_IS_WAVPACK,		"MPLAYER",				&PrgInit.bool_mplayer,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_MPC,		FILE_IS_MPC,			"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_MPC,		FILE_IS_WAV,			"MPPDEC",				&PrgInit.bool_mpc123_mppdec,	NULL,		NULL },
{FILE_IS_MPC,		FILE_IS_MP3,			"MPPDEC",				&PrgInit.bool_mpc123_mppdec,	"LAME",		&PrgInit.bool_lame },
{FILE_IS_MPC,		FILE_IS_OGG,			"MPPDEC",				&PrgInit.bool_mpc123_mppdec,	"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_MPC,		FILE_IS_FLAC,			"MPPDEC",				&PrgInit.bool_mpc123_mppdec,	"FLAC",		&PrgInit.bool_flac },
{FILE_IS_MPC,		FILE_IS_M4A,			"MPPDEC",				&PrgInit.bool_mpc123_mppdec,	"FAAC",		&PrgInit.bool_faac },
{FILE_IS_MPC,		FILE_IS_AAC,			"MPPDEC",				&PrgInit.bool_mpc123_mppdec,	"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_MPC,		FILE_IS_APE,			"MPPDEC",				&PrgInit.bool_mpc123_mppdec,	"MAC",		&PrgInit.bool_ape },
{FILE_IS_MPC,		FILE_IS_WAVPACK,		"MPPDEC",				&PrgInit.bool_mpc123_mppdec,	"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_APE,		FILE_IS_WAV,			"MAC",					&PrgInit.bool_ape,		NULL,		NULL },
{FILE_IS_APE,		FILE_IS_MP3,			"MAC",					&PrgInit.bool_ape,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_APE,		FILE_IS_OGG,			"MAC",					&PrgInit.bool_ape,		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_APE,		FILE_IS_FLAC,			"MAC",					&PrgInit.bool_ape,		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_APE,		FILE_IS_M4A,			"MAC",					&PrgInit.bool_ape,		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_APE,		FILE_IS_AAC,			"MAC",					&PrgInit.bool_ape,		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_APE,		FILE_IS_MPC,			"MAC",					&PrgInit.bool_ape,		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_APE,		FILE_IS_WAVPACK,		"MAC",					&PrgInit.bool_ape,		"WAVPACK",	&PrgInit.bool_wavpack },

{FILE_IS_WAVPACK,	FILE_TO_REPLAYGAIN,		"WVGAIN",				&PrgInit.bool_wavpack,		NULL,		NULL },
{FILE_IS_WAVPACK,	FILE_IS_WAVPACK,		"MPLAYER",				&PrgInit.bool_mplayer,		NULL,		NULL },
{FILE_IS_WAVPACK,	FILE_IS_WAV,			"WVUNPACK",				&PrgInit.bool_wavpack,		NULL,		NULL },
{FILE_IS_WAVPACK,	FILE_IS_MP3,			"WVUNPACK",				&PrgInit.bool_wavpack,		"LAME",		&PrgInit.bool_lame },
{FILE_IS_WAVPACK,	FILE_IS_OGG,			"WVUNPACK",				&PrgInit.bool_wavpack,		"OGGENC",	&PrgInit.bool_oggenc },
{FILE_IS_WAVPACK,	FILE_IS_FLAC,			"WVUNPACK",				&PrgInit.bool_wavpack,		"FLAC",		&PrgInit.bool_flac },
{FILE_IS_WAVPACK,	FILE_IS_M4A,			"WVUNPACK",				&PrgInit.bool_wavpack,		"FAAC",		&PrgInit.bool_faac },
{FILE_IS_WAVPACK,	FILE_IS_AAC,			"WVUNPACK",				&PrgInit.bool_wavpack,		"AACPLUSENC",	&PrgInit.bool_aacplusenc },
{FILE_IS_WAVPACK,	FILE_IS_MPC,			"WVUNPACK",				&PrgInit.bool_wavpack,		"MPPENC",	&PrgInit.bool_mppenc },
{FILE_IS_WAVPACK,	FILE_IS_APE,			"WVUNPACK",				&PrgInit.bool_wavpack,		"MAC",		&PrgInit.bool_ape },

{-1}
};

gboolean file_bool_from_to (DETAIL *detail, TYPE_FILE_IS to)
{
	gint	     Cpt;
	TYPE_FILE_IS from = detail->type_infosong_file_is;

	for (Cpt = 0; TstFrom [ Cpt ].From != -1; Cpt ++) {
		if (from == TstFrom [ Cpt ].From && to == TstFrom [ Cpt ].To) {
			if (TstFrom [ Cpt ].Conv2 != NULL) {
				return (*TstFrom [ Cpt ].Bool1 & *TstFrom [ Cpt ].Bool2);
			}
			else {
				return (*TstFrom [ Cpt ].Bool1);
			}
		}
	}
	return TRUE;
}
// BASCULE DE L ETAT DU FLAG SUR L ETAT SUIVANT
//
ETAT_SELECTION file_get_next_flag (DETAIL *detail, TYPE_FILE_IS type)
{
	ETAT_SELECTION	etat = ETAT_ATTENTE;
	gboolean	BoolFileExist = FALSE;
	gboolean	BoolOpExpertExist = FALSE;

	if (detail->type_infosong_file_is == type) return (ETAT_PRG_NONE);
	if (FALSE == file_bool_from_to (detail, type)) return (ETAT_PRG_ABSENT);

	if (type == FILE_IS_WAV) {
		etat = detail->EtatSelection_Wav;
		BoolFileExist = file_test_with_new_ext_exist (detail, "wav");
	}
	else if (type == FILE_IS_FLAC) {
		etat = detail->EtatSelection_Flac;
		BoolFileExist = file_test_with_new_ext_exist (detail, "flac");
		BoolOpExpertExist = Config.StringExpanderFlac != NULL && '\0' != *Config.StringExpanderFlac ? TRUE : FALSE;
	}
	else if (type == FILE_IS_APE) {
		etat = detail->EtatSelection_Ape;
		BoolFileExist = file_test_with_new_ext_exist (detail, "ape");
		BoolOpExpertExist = Config.StringExpanderMac != NULL && '\0' != *Config.StringExpanderMac ? TRUE : FALSE;
	}
	else if (type == FILE_IS_WAVPACK) {
		etat = detail->EtatSelection_WavPack;
		BoolFileExist = file_test_with_new_ext_exist (detail, "wv");
		BoolOpExpertExist = Config.StringExpanderWavpack != NULL && '\0' != *Config.StringExpanderWavpack ? TRUE : FALSE;
	}
	else if (type == FILE_IS_OGG) {
		etat = detail->EtatSelection_Ogg;
		BoolFileExist = file_test_with_new_ext_exist (detail, "ogg");
		BoolOpExpertExist = Config.StringExpanderOggenc != NULL && '\0' != *Config.StringExpanderOggenc ? TRUE : FALSE;
	}
	else if (type == FILE_IS_M4A) {
		etat = detail->EtatSelection_M4a;
		BoolFileExist = file_test_with_new_ext_exist (detail, "m4a");
		BoolOpExpertExist = Config.StringExpanderFaac != NULL && '\0' != *Config.StringExpanderFaac ? TRUE : FALSE;
	}
	else if (type == FILE_IS_AAC) {
		etat = detail->EtatSelection_Aac;
		BoolFileExist = file_test_with_new_ext_exist (detail, "aac");
	}
	else if (type == FILE_IS_MPC) {
		etat = detail->EtatSelection_Mpc;
		BoolFileExist = file_test_with_new_ext_exist (detail, "mpc");
		BoolOpExpertExist = Config.StringExpanderMppenc != NULL && '\0' != *Config.StringExpanderMppenc ? TRUE : FALSE;
	}
	else if (type == FILE_IS_MP3) {
		etat = detail->EtatSelection_Mp3;
		BoolFileExist = file_test_with_new_ext_exist (detail, "mp3");
		BoolOpExpertExist = Config.StringExpanderLame != NULL && '\0' != *Config.StringExpanderLame ? TRUE : FALSE;
	}

	switch (etat) {
	case ETAT_PRG_NONE :
		if (type == FILE_IS_FLAC) {
			return (fileanalyze_exist (detail, "flac") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		else if (type == FILE_IS_WAV) {
			return (fileanalyze_exist (detail, "wav") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		else if (type == FILE_IS_MP3) {
			return (fileanalyze_exist (detail, "mp3") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		else if (type == FILE_IS_OGG) {
			return (fileanalyze_exist (detail, "ogg") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		else if (type == FILE_IS_M4A) {
			return (fileanalyze_exist (detail, "m4a") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		else if (type == FILE_IS_AAC) {
			return (fileanalyze_exist (detail, "aac") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		else if (type == FILE_IS_MPC) {
			return (fileanalyze_exist (detail, "mpc") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		else if (type == FILE_IS_APE) {
			return (fileanalyze_exist (detail, "ape") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		else if (type == FILE_IS_WAVPACK) {
			return (fileanalyze_exist (detail, "wv") ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
		}
		return (ETAT_PRG_NONE);
	case ETAT_PRG_ABSENT :
		return( ETAT_PRG_ABSENT );
		break;
	case ETAT_ATTENTE :
	case ETAT_ATTENTE_EXIST :
		return (TRUE == BoolFileExist ? ETAT_SELECT_EXIST : ETAT_SELECT);
	case ETAT_SELECT :
	case ETAT_SELECT_EXIST :
		if (TRUE == BoolOpExpertExist)
			return (TRUE == BoolFileExist ? ETAT_SELECT_EXPERT_EXIST : ETAT_SELECT_EXPERT);
		else	return (TRUE == BoolFileExist ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
	case ETAT_SELECT_EXPERT :
	case ETAT_SELECT_EXPERT_EXIST :
		return (TRUE == BoolFileExist ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE);
	}

	return (etat);
}
// RENVOIE L ETAT ACTUEL SOUS FORME D UN PIXBUF
//
GdkPixbuf *file_get_pixbuf (DETAIL *detail, TYPE_FILE_IS type)
{
	ETAT_SELECTION	*etat = NULL;
	gboolean	BoolFileExist = FALSE;

	if( detail->type_infosong_file_is == type ) return ((GdkPixbuf *)NULL);
	if( FILE_IS_VID_M4A == detail->type_infosong_file_is && type == FILE_IS_M4A ) return ((GdkPixbuf *)NULL);

	if (FALSE == file_bool_from_to (detail, type)) return (var_file.Pixbuf_NotInstall);

	if (detail->type_infosong_file_is != type) {

		if (type == FILE_IS_WAV)          { etat = &detail->EtatSelection_Wav;		BoolFileExist =  file_test_with_new_ext_exist (detail, "wav"); }
		else if (type == FILE_IS_FLAC)    { etat = &detail->EtatSelection_Flac;		BoolFileExist =  file_test_with_new_ext_exist (detail, "flac"); }
		else if (type == FILE_IS_APE)     { etat = &detail->EtatSelection_Ape;		BoolFileExist =  file_test_with_new_ext_exist (detail, "ape"); }
		else if (type == FILE_IS_WAVPACK) { etat = &detail->EtatSelection_WavPack;	BoolFileExist =  file_test_with_new_ext_exist (detail, "wv"); }
		else if (type == FILE_IS_OGG)     { etat = &detail->EtatSelection_Ogg;		BoolFileExist =  file_test_with_new_ext_exist (detail, "ogg"); }
		else if (type == FILE_IS_M4A)     { etat = &detail->EtatSelection_M4a;		BoolFileExist =  file_test_with_new_ext_exist (detail, "m4a"); }
		else if (type == FILE_IS_AAC)     { etat = &detail->EtatSelection_Aac;		BoolFileExist =  file_test_with_new_ext_exist (detail, "aac"); }
		else if (type == FILE_IS_MPC)     { etat = &detail->EtatSelection_Mpc;		BoolFileExist =  file_test_with_new_ext_exist (detail, "mpc"); }
		else if (type == FILE_IS_MP3)     { etat = &detail->EtatSelection_Mp3;		BoolFileExist =  file_test_with_new_ext_exist (detail, "mp3"); }

		switch (*etat) {
		case ETAT_PRG_NONE :
			*etat = ETAT_PRG_NONE;
			return (NULL);
			break;
		case ETAT_PRG_ABSENT :
			*etat = ETAT_PRG_ABSENT;
			return (var_file.Pixbuf_NotInstall);
			break;
		case ETAT_ATTENTE :
		case ETAT_ATTENTE_EXIST :
			*etat = TRUE == BoolFileExist ? ETAT_ATTENTE_EXIST : ETAT_ATTENTE;
			return (TRUE == BoolFileExist ? var_file.Pixbuf_Coche_exist : var_file.Pixbuf_Coche);
			break;
		case ETAT_SELECT :
		case ETAT_SELECT_EXIST :
			*etat = TRUE == BoolFileExist ? ETAT_SELECT_EXIST : ETAT_SELECT;
			return (TRUE == BoolFileExist ? var_file.Pixbuf_Selected_exist : var_file.Pixbuf_Selected);
			break;
		case ETAT_SELECT_EXPERT :
		case ETAT_SELECT_EXPERT_EXIST :
			*etat = TRUE == BoolFileExist ? ETAT_SELECT_EXPERT_EXIST : ETAT_SELECT_EXPERT;
			return (TRUE == BoolFileExist ? var_file.Pixbuf_Selected_expert_exist : var_file.Pixbuf_Selected_expert);
			break;
		}
	}
	return ((GdkPixbuf *)NULL);
}
//
//
gchar *file_get_str_level_normalise (DETAIL *detail)
{
	gint		 level = 0;
	gint		 NewLevel = 0;
	gchar		*Str = NULL;

	if (detail->type_infosong_file_is == FILE_IS_WAV) {
		INFO_WAV *info = (INFO_WAV *)detail->info;

		level          = info->LevelDbfs.level;
		NewLevel       = info->LevelDbfs.NewLevel;
	}
	else if (detail->type_infosong_file_is == FILE_IS_MP3) {
		INFO_MP3 *info = (INFO_MP3 *)detail->info;

		level          = info->LevelDbfs.level;
		NewLevel       = info->LevelDbfs.NewLevel;
	}
	else if (detail->type_infosong_file_is == FILE_IS_OGG) {
		INFO_OGG *info = (INFO_OGG *)detail->info;

		level          = info->LevelDbfs.level;
		NewLevel       = info->LevelDbfs.NewLevel;
	}
	else {
		detail->Etat_Normalise = NORM_NONE;
	}

	switch (detail->Etat_Normalise) {

	case NORM_NONE :
		Str = g_strdup ("");
		break;

	case NORM_READY_FOR_SELECT :
		Str = g_strdup ("<span font_desc=\"Courier bold 9\"><span color=\"black\"><b>[ ]</b></span></span>");
		break;

	case NORM_PEAK_ALBUM :
		Str = g_strdup ("<span font_desc=\"Courier bold 9\"><span color=\"black\"><b>PEAK/ALBUM</b></span></span>");
		break;

	case NORM_PEAK :
		Str = g_strdup_printf ("<span font_desc=\"Courier bold 9\"><span color=\"black\"><b>PEAK</b></span></span>");
		break;

	case NORM_RMS_MIX_ALBUM :
		if (detail->Etat_Scan == ETAT_SCAN_NONE) {
			detail->Etat_Scan = ETAT_SCAN_DEMANDE;
			FileScanDB_action (TRUE);
		}
		if (detail->LevelMix < level) {
			Str = g_strdup_printf ("<span font_desc=\"Courier bold 9\"><span color=\"black\"><b>RMS/ALBUM:%02d:%02d dBFS</b></span></span>", level, detail->LevelMix);
		}
		else if (detail->LevelMix == level) {
			Str = g_strdup_printf ("<span font_desc=\"Courier bold 9\"><span color=\"forestgreen\"><b>RMS/ALBUM:%02d:%02d dBFS</b></span></span>", level, detail->LevelMix);
		}
		else if (detail->LevelMix > level) {
			Str = g_strdup_printf ("<span font_desc=\"Courier bold 9\"><span color=\"gold\"><b>RMS/ALBUM:%02d:%02d dBFS</b></span></span>", level, detail->LevelMix);
		}
		else {
			Str = g_strdup ("");
		}
		break;

	case NORM_RMS_FIX :
		if (detail->Etat_Scan == ETAT_SCAN_NONE) {
			detail->Etat_Scan = ETAT_SCAN_DEMANDE;
			FileScanDB_action (TRUE);
		}

		if (NewLevel < level) {
			Str = g_strdup_printf ("<span font_desc=\"Courier bold 9\"><span color=\"red\"><b>RMS:%02d:%02d dBFS</b></span></span>", level, NewLevel);
		}
		else if (NewLevel == level) {
			Str = g_strdup_printf ("<span font_desc=\"Courier bold 9\"><span color=\"forestgreen\"><b>RMS:%02d:%02d dBFS</b></span></span>", level, NewLevel);
		}
		else if (NewLevel > level){
			Str = g_strdup_printf ("<span font_desc=\"Courier bold 9\"><span color=\"orange\"><b>RMS:%02d:%02d dBFS</b></span></span>", level, NewLevel);
		}
		else {
			Str = g_strdup ("");
		}
		break;
	}

	return (Str);
}
// CETTE FONCTION RETOURNE UN TYPE DE PIXBUF REFLETANT L ETA DU REPLAGAIN
//
GdkPixbuf *file_get_pixbuf_replaygain (DETAIL *detail, gboolean p_bool_next)
{
	GdkPixbuf *PixBuf = NULL;

	switch (detail->type_infosong_file_is) {
	case FILE_IS_NONE :
	case FILE_IS_WAV :
	case FILE_IS_M4A :
	case FILE_IS_VID_M4A :
	case FILE_IS_AAC :
	case FILE_IS_SHN :
	case FILE_IS_WMA :
	case FILE_IS_RM :
	case FILE_IS_DTS :
	case FILE_IS_AIFF :
	case FILE_IS_APE :
	case FILE_IS_WAVPACK_MD5 :
	case FILE_IS_AC3 :
	case FILE_TO_NORMALISE :
	case FILE_TO_NORMALISE_COLLECTIF :
	case FILE_TO_REPLAYGAIN :
	/*
	Remarque de @Shankarius:
	je me demande si le support du ReplayGain pour MusePack est vraiment souhaitable du
	fait qu'il ne se trouve pas dans les depots, de l'impossibilite de supprimer les tags
	et de la doc plus que limitee le concernant...
	*/
	case FILE_IS_MPC :
		break;
	case FILE_IS_MP3 :
	case FILE_IS_OGG :
	case FILE_IS_WAVPACK :
		if (detail->type_infosong_file_is == FILE_IS_MP3 && ! PrgInit.bool_mp3gain) return (var_file.Pixbuf_NotInstall);
		if (detail->type_infosong_file_is == FILE_IS_OGG && ! PrgInit.bool_vorbisgain) return (var_file.Pixbuf_NotInstall);
		if (detail->type_infosong_file_is == FILE_IS_WAVPACK && ! PrgInit.bool_wavpack) return (var_file.Pixbuf_NotInstall);

		switch (detail->Etat_ReplayGain) {
		case RPG_NONE :
			return (NULL);
		case RPG_ATTENTE :
			if (p_bool_next) {
				detail->Etat_ReplayGain = RPG_PISTE;
				return (var_file.Pixbuf_rpg_piste);
			}
			return (var_file.Pixbuf_rpg_wait);
		case RPG_PISTE :
			if (p_bool_next) {
				detail->Etat_ReplayGain = RPG_ALBUM;
				return (var_file.Pixbuf_rpg_album);
			}
			return (var_file.Pixbuf_rpg_piste);
		case RPG_ALBUM :
			if (p_bool_next) {
				detail->Etat_ReplayGain = RPG_EFFACER;
				return (var_file.Pixbuf_rpg_effacer);
			}
			return (var_file.Pixbuf_rpg_album);
		case RPG_EFFACER :
			if (p_bool_next) {
				detail->Etat_ReplayGain = RPG_ATTENTE;
				return (var_file.Pixbuf_rpg_wait);
			}
			return (var_file.Pixbuf_rpg_effacer);
		}
		break;
	case FILE_IS_FLAC :
		if (FALSE == PrgInit.bool_flac) return (var_file.Pixbuf_NotInstall);

		switch (detail->Etat_ReplayGain) {
		case RPG_NONE :
			return (NULL);
		case RPG_ATTENTE :
			if (p_bool_next) {
				detail->Etat_ReplayGain = RPG_ALBUM;
				return (var_file.Pixbuf_rpg_album);
			}
			return (var_file.Pixbuf_rpg_wait);
		case RPG_PISTE :
			detail->Etat_ReplayGain = RPG_ATTENTE;
			return (var_file.Pixbuf_rpg_wait);
		case RPG_ALBUM :
			if (p_bool_next) {
				detail->Etat_ReplayGain = RPG_EFFACER;
				return (var_file.Pixbuf_rpg_effacer);
			}
			return (var_file.Pixbuf_rpg_album);
		case RPG_EFFACER :
			if (p_bool_next) {
				detail->Etat_ReplayGain = RPG_ATTENTE;
				return (var_file.Pixbuf_rpg_wait);
			}
			return (var_file.Pixbuf_rpg_effacer);
		}
	}

	return ((GdkPixbuf *)PixBuf);
}
//
//
GdkPixbuf *file_get_pixbuf_trash (DETAIL *detail)
{
	if (FILE_TRASH_NONE == detail->EtatTrash) {

		return ((GdkPixbuf *)var_file.Pixbuf_NoTrash);
	}
	else if (FILE_TRASH_OK == detail->EtatTrash) {

		return ((GdkPixbuf *)var_file.Pixbuf_Trash);
	}

	return ((GdkPixbuf *)NULL);
}
//
//
gchar *file_get_name_to_treview( DETAIL *detail )
{
	gchar		*NameSrc = libutils_get_name_without_ext( detail->namefile );
	gchar		*NameEperluette = utf8_eperluette_name( detail->NameDest );
	gboolean	BoolIdem = FALSE;

	BoolIdem = ( strlen( detail->NameDest ) == strlen( NameSrc ) && 0 == strcmp( detail->NameDest, NameSrc )) ? TRUE : FALSE;
	g_free (NameSrc);	NameSrc = NULL;

	if( TRUE == BoolIdem )
		NameSrc = g_strdup( NameEperluette );
	else	NameSrc = g_strdup_printf( "<span color=\"red\"><b><i>%s</i></b></span>", NameEperluette );

	g_free( NameEperluette );	NameEperluette = NULL;

	return( (gchar *)NameSrc );
}
//
//
void file_make_combobox_select_type (void);
void file_affiche_glist (void)
{
	DETAIL		*detail = NULL;
	GList		*List = NULL;
	GtkTreeIter	iter;
	GtkAdjustment	*Adj = NULL;
	gdouble		AdjValue;
	gint		Line = 0;		// LIGNE EN COURS
	gchar		*NameDest = NULL;
	gint		NumLineSelected = -1;
	gboolean	BoolNumLineSelected = FALSE;

	// RECUP SELECTION
	//
	NumLineSelected = libutils_get_first_line_is_selected( var_file.Adr_Line_Selected, var_file.Adr_Tree_Model );

	gtk_tree_selection_unselect_all (var_file.Adr_Line_Selected);

	// DELETE TREEVIEW
	//
	gtk_list_store_clear (GTK_LIST_STORE (var_file.Adr_List_Store));

	// COORDONNEES POUR UN REAJUSTEMENT VISUEL DE LA PAGE
	//
	Adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (var_file.Adr_scroll));
	AdjValue = gtk_adjustment_get_value (Adj);

	// AFFICHAGE DE LA LISTE
	//
	// Cpt = 0;
	Line = 0;
	List = g_list_first (entetefile);
	while (List) {
		if (NULL != ((detail = (DETAIL *)List->data)) && FALSE == detail->BoolRemove) {

			NameDest = file_get_name_to_treview (detail);

			gtk_list_store_append (var_file.Adr_List_Store, &iter);
			gtk_list_store_set (var_file.Adr_List_Store, &iter,
						COLUMN_FILE_PLAY,		var_file.Pixbuf_FileStop,
						COLUMN_FILE_TRASH,		file_get_pixbuf_trash (detail),
						COLUMN_FILE_TYPE,		tags_get_str_type_file_is (detail->type_infosong_file_is),

						COLUMN_FILE_WAV,		file_get_pixbuf (detail, FILE_IS_WAV),
						COLUMN_FILE_FLAC,		file_get_pixbuf (detail, FILE_IS_FLAC),
						COLUMN_FILE_APE,		file_get_pixbuf (detail, FILE_IS_APE),
						COLUMN_FILE_WAVPACK,		file_get_pixbuf (detail, FILE_IS_WAVPACK),
						COLUMN_FILE_OGG,		file_get_pixbuf (detail, FILE_IS_OGG),
						COLUMN_FILE_M4A,		file_get_pixbuf (detail, FILE_IS_M4A),
						COLUMN_FILE_AAC,		file_get_pixbuf (detail, FILE_IS_AAC),
						COLUMN_FILE_MPC,		file_get_pixbuf (detail, FILE_IS_MPC),
						COLUMN_FILE_MP3,		file_get_pixbuf (detail, FILE_IS_MP3),

						COLUMN_FILE_TIME,		file_get_time (detail),
						COLUMN_FILE_NORMALIZE,		file_get_str_level_normalise (detail),
						COLUMN_FILE_REPLAYGAIN,		file_get_pixbuf_replaygain (detail, FALSE),
						COLUMN_FILE_NAME,		NameDest,
						COLUMN_FILE_EDITABLE,		TRUE,
						//COLUMN_FILE_COLOR,		&YellowColor,
						COLUMN_FILE_POINTER_STRUCT,	detail,
						-1);

			// AFFICHE LES EVENTUELLES LIGNES EN SELECTION
			//
			if( NumLineSelected == Line ) {
				gtk_tree_selection_select_iter (var_file.Adr_Line_Selected, &iter);
				BoolNumLineSelected = TRUE;
			}

			g_free (NameDest);	NameDest = NULL;

			Line ++;
		}
		List = g_list_next (List);
	}

	// SUPPRESSON TABLEAU DES EVENTUELLES LIGNES EN SELECTION
	//
	if( NumLineSelected == -1 ) {
		if (gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter)) {
			gtk_tree_selection_select_iter (var_file.Adr_Line_Selected, &iter);
			BoolNumLineSelected = TRUE;
		}
	}
	if( NumLineSelected > 0 && NULL != entetefile && BoolNumLineSelected == FALSE ) {
		gtk_tree_selection_select_iter (var_file.Adr_Line_Selected, &iter);
	}
	// REAJUSTEMENT DE LA LISTE
	//
	gtk_adjustment_set_value (Adj, AdjValue);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (var_file.Adr_scroll), Adj);

	file_make_combobox_select_type ();
	file_pixbuf_update_glist();
	file_set_flag_buttons ();
}
//
//
void file_maj_file_load (gchar *path)
{
	switch (Config.NotebookFile) {
	case NOTEBOOK_FICHIERS_CONVERSION :
		g_free (Config.PathLoadFileAll);
		Config.PathLoadFileAll = NULL;
		Config.PathLoadFileAll = g_strdup (path);
		break;
	case NOTEBOOK_FICHIERS_WAV :
		g_free (Config.PathLoadFileWav);
		Config.PathLoadFileWav = NULL;
		Config.PathLoadFileWav = g_strdup (path);
		break;
	case NOTEBOOK_FICHIERS_MP3OGG :
		g_free (Config.PathLoadFileMp3Ogg);
		Config.PathLoadFileMp3Ogg = NULL;
		Config.PathLoadFileMp3Ogg = g_strdup (path);
		break;
	case NOTEBOOK_FICHIERS_TAGS :
		g_free (Config.PathLoadFileTags);
		Config.PathLoadFileTags = NULL;
		Config.PathLoadFileTags = g_strdup (path);
		break;
	}
}
//
//
void on_button_import_file_clicked (GtkButton *button, gpointer user_data)
{
	switch (Config.NotebookFile) {
	case NOTEBOOK_FICHIERS_CONVERSION :
		fileselect_create (_PATH_LOAD_FILE_ALL_, Config.PathLoadFileAll, file_maj_file_load);
		break;
	case NOTEBOOK_FICHIERS_WAV :
		fileselect_create (_PATH_LOAD_FILE_WAV_, Config.PathLoadFileWav, file_maj_file_load);
		break;
	case NOTEBOOK_FICHIERS_MP3OGG :
		fileselect_create (_PATH_LOAD_FILE_MP3OGG_, Config.PathLoadFileMp3Ogg, file_maj_file_load);
		break;
	case NOTEBOOK_FICHIERS_TAGS :
		fileselect_create (_PATH_LOAD_FILE_TAGS_, Config.PathLoadFileTags, file_maj_file_load);
		break;
	}
}
// SUPPRESSION DES LIGNES DANS LE GLIST entetefile DONT CIBLE N EXISTE PLUS
//
void file_verify_before_conversion (void)
{
	GList     *list = NULL;
	DETAIL    *detail = NULL;
	gint       NbrFileRemove = 0;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			//
			// @Shankarius : Suppression d'un fichier qui n'existe plus avant conversion
			//
			if (FALSE == libutils_test_file_exist (detail->namefile)) {
				g_print ("@Shankarius: NOT EXIST: DEL IN LISTE -> %s\n", detail->namefile);
				detail->BoolRemove = TRUE;
				NbrFileRemove ++;
			}
		}
		list = g_list_next(list);
	}
	if (NbrFileRemove > 0) {
		file_affiche_glist ();
		FileWav_affiche_glist ();
	}
}
// MARQUER LES LIGNES POUR LA DESTRUCTION
//
void on_file_button_del_file_clicked (GtkButton *button, gpointer user_data)
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*BeginList = NULL;
	GList		*list = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;
	gboolean	 BoolPrint = FALSE;
	gboolean	valid;

	if (NOTEBOOK_FICHIERS == Config.NotebookGeneral) {
		if (NOTEBOOK_FICHIERS_CONVERSION == Config.NotebookFile) {
			// RECUP. LIGNES EN SELECTION POUR DESTRUCTION
			model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file.Adr_TreeView));
			if ((BeginList = gtk_tree_selection_get_selected_rows (var_file.Adr_Line_Selected, &model))) {
				BoolPrint = TRUE;
				list = g_list_first (BeginList);
				while (list) {
					if ((path = list->data)) {
						gtk_tree_model_get_iter (model, &iter, path);
						gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
						// MARQUER LA LIGNE DU GLIST A DETRUIRE AVANT LE REAFFICHAGE
						if (NULL != detail) detail->BoolRemove = TRUE;
					}
					list = g_list_next (list);
				}
				// gtk_tree_selection_unselect_all (var_file.Adr_Line_Selected);
			}
		}
		else if (NOTEBOOK_FICHIERS_WAV == Config.NotebookFile) {
			BoolPrint = FileWav_del_file_clicked ();
		}
		else if (NOTEBOOK_FICHIERS_MP3OGG == Config.NotebookFile) {
			BoolPrint = FileMp3Ogg_del_file_clicked ();
		}
		else if (NOTEBOOK_FICHIERS_TAGS == Config.NotebookFile) {
			BoolPrint = FileTags_del_file_clicked ();
		}

		// LA SUPPRESSION SE FAIT DEPUIS LA FONCTION D AFFICHAGE
		if (TRUE == BoolPrint) {
			file_affiche_glist ();
			FileWav_affiche_glist ();
			FileMp3Ogg_affiche_glist ();
			FileTags_affiche_glist ();

			// LIST DESTROY with detail->BoolRemove
			fileanalyze_remove_entetefile_detail();

			// MAKE NEW LIST
			valid = gtk_tree_model_get_iter_first( var_file.Adr_Tree_Model, &iter );
			while (valid) {
				gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);

				if( NULL != detail )
					entetefile = g_list_append( entetefile, detail );

				valid = gtk_tree_model_iter_next( var_file.Adr_Tree_Model, &iter );
			}
		}
	}
}
//
//
void on_combobox_dest_file_realize (GtkWidget *widget, gpointer user_data)
{
	var_file.Adr_combobox_DestFile = GTK_COMBO_BOX (widget);
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), _("Conversion(s) to source"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), _("Select a new destination"));
	switch (Config.NotebookFile) {
	case NOTEBOOK_FICHIERS_CONVERSION :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileAll);
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_CONVERSION ]);
		break;
	case NOTEBOOK_FICHIERS_WAV :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileWav);
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_WAV ]);
		break;
	case NOTEBOOK_FICHIERS_MP3OGG :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileMp3Ogg);
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_MP3OGG ]);
		break;
	case NOTEBOOK_FICHIERS_TAGS :
		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileTags);
		gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_TAGS ]);
		break;
	}
}
//
//
void file_make_combobox_select_type (void)
{
	GList	*list = NULL;
	DETAIL	*detail = NULL;
	gint	bool_flac = 0;
	gint	bool_mp3 = 0;
	gint	bool_ogg = 0;
	gint	bool_wav = 0;
	gint	bool_m4a = 0;
	gint	bool_aac = 0;
	gint	bool_shn = 0;
	gint	bool_wma = 0;
	gint	bool_rm = 0;
	gint	bool_dts = 0;
	gint	bool_aiff = 0;
	gint	bool_mpc = 0;
	gint	bool_ape = 0;
	gint	bool_wavpack = 0;
	gint	bool_val = 0;
	gchar	Str [ 20 ];

	if (NULL == GTK_COMBO_BOX (var_file.Adr_combobox_select_type)) return;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (detail->type_infosong_file_is == FILE_IS_FLAC)		bool_flac ++;
			else if (detail->type_infosong_file_is == FILE_IS_WAV)		bool_wav ++;
			else if (detail->type_infosong_file_is == FILE_IS_MP3)		bool_mp3 ++;
			else if (detail->type_infosong_file_is == FILE_IS_OGG)		bool_ogg ++;
			else if (detail->type_infosong_file_is == FILE_IS_M4A)		bool_m4a ++;
			else if (detail->type_infosong_file_is == FILE_IS_VID_M4A)	bool_m4a ++;
			else if (detail->type_infosong_file_is == FILE_IS_AAC)		bool_aac ++;
			else if (detail->type_infosong_file_is == FILE_IS_SHN)		bool_shn ++;
			else if (detail->type_infosong_file_is == FILE_IS_WMA)		bool_wma ++;
			else if (detail->type_infosong_file_is == FILE_IS_RM)		bool_rm ++;
			else if (detail->type_infosong_file_is == FILE_IS_DTS)		bool_dts ++;
			else if (detail->type_infosong_file_is == FILE_IS_AIFF)		bool_aiff ++;
			else if (detail->type_infosong_file_is == FILE_IS_MPC)		bool_mpc ++;
			else if (detail->type_infosong_file_is == FILE_IS_APE)		bool_ape ++;
			else if (detail->type_infosong_file_is == FILE_IS_WAVPACK)	bool_wavpack ++;
		}
		list = g_list_next (list);
	}

	libcombo_remove_options (GTK_COMBO_BOX (var_file.Adr_combobox_select_type));

	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), _("Select type: --"));
	if (bool_flac > 0)    { g_sprintf (Str, "%03d  FLAC", bool_flac);	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_wav > 0)     { g_sprintf (Str, "%03d  WAV", bool_wav);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_mp3 > 0)     { g_sprintf (Str, "%03d  MP3", bool_mp3);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_ogg > 0)     { g_sprintf (Str, "%03d  OGG", bool_ogg);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_m4a > 0)     { g_sprintf (Str, "%03d  M4A", bool_m4a);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_aac > 0)     { g_sprintf (Str, "%03d  AAC", bool_aac);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_shn > 0)     { g_sprintf (Str, "%03d  SHN", bool_shn);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_wma > 0)     { g_sprintf (Str, "%03d  WMA", bool_wma);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_rm > 0)      { g_sprintf (Str, "%03d  RM",  bool_rm);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_dts > 0)     { g_sprintf (Str, "%03d  DTS", bool_dts);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_aiff > 0)    { g_sprintf (Str, "%03d  AIF", bool_aiff);	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_mpc > 0)     { g_sprintf (Str, "%03d  MPC", bool_mpc);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_ape > 0)     { g_sprintf (Str, "%03d  APE", bool_ape);		gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	if (bool_wavpack > 0) { g_sprintf (Str, "%03d  WAVPACK", bool_wavpack);	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_select_type), Str);	}
	gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_select_type), 0);

	bool_val = bool_flac + bool_wav + bool_mp3 + bool_ogg + bool_m4a + bool_aac + bool_shn + bool_wma + bool_rm + bool_dts + bool_aiff + bool_mpc + bool_ape + bool_wavpack;
	gtk_widget_set_sensitive (GTK_WIDGET (var_file.Adr_combobox_select_type), (bool_val > 0) ? TRUE : FALSE);
}
//
//
void on_combobox_select_type_realize (GtkWidget *widget, gpointer user_data)
{
	// gtk_combo_box_append_text (GTK_COMBO_BOX (widget), _("Select type: --"));
	// gtk_combo_box_set_active (GTK_COMBO_BOX (widget), 0);
	var_file.Adr_combobox_select_type = GTK_COMBO_BOX (widget);
	file_make_combobox_select_type ();
}
//
//
void on_combobox_select_type_changed (GtkComboBox *combobox, gpointer user_data)
{
	gchar         *ptr = NULL;
	gint           nmr = -1;
	TYPE_FILE_IS   type_file_is = FILE_IS_NONE;
	gboolean       valid;
	GtkTreeIter    iter;
	DETAIL        *detail = NULL;

	if (var_file.Adr_combobox_select_type != NULL) {
		if ((nmr = gtk_combo_box_get_active (GTK_COMBO_BOX (var_file.Adr_combobox_select_type)) > 0)) {

			ptr = gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT (var_file.Adr_combobox_select_type));
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_select_type), 0);
			if (NULL != strstr (ptr, "FLAC"))     type_file_is = FILE_IS_FLAC;
			if (NULL != strstr (ptr, "WAV"))      type_file_is = FILE_IS_WAV;
			if (NULL != strstr (ptr, "MP3"))      type_file_is = FILE_IS_MP3;
			if (NULL != strstr (ptr, "OGG"))      type_file_is = FILE_IS_OGG;
			if (NULL != strstr (ptr, "M4A"))      type_file_is = FILE_IS_M4A;
			// if (NULL != strstr (ptr, "M4A"))      type_file_is = FILE_IS_VID_M4A;
			if (NULL != strstr (ptr, "AAC"))      type_file_is = FILE_IS_AAC;
			if (NULL != strstr (ptr, "SHN"))      type_file_is = FILE_IS_SHN;
			if (NULL != strstr (ptr, "WMA"))      type_file_is = FILE_IS_WMA;
			if (NULL != strstr (ptr, "RM"))       type_file_is = FILE_IS_RM;
			if (NULL != strstr (ptr, "DTS"))      type_file_is = FILE_IS_DTS;
			if (NULL != strstr (ptr, "AIF"))      type_file_is = FILE_IS_AIFF;
			if (NULL != strstr (ptr, "MPC"))      type_file_is = FILE_IS_MPC;
			if (NULL != strstr (ptr, "APE"))      type_file_is = FILE_IS_APE;
			if (NULL != strstr (ptr, "WAVPACK"))  type_file_is = FILE_IS_WAVPACK;

			if (type_file_is == FILE_IS_NONE) return;

			gtk_tree_selection_unselect_all (var_file.Adr_Line_Selected);

			valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
			while (valid) {
				gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
				if (detail->type_infosong_file_is == type_file_is) {
					gtk_tree_selection_select_iter (var_file.Adr_Line_Selected, &iter);
				}
				else if( FILE_IS_VID_M4A == detail->type_infosong_file_is && FILE_IS_M4A == type_file_is ) {
					gtk_tree_selection_select_iter (var_file.Adr_Line_Selected, &iter);
				}
				valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
			}
		}
	}
}
//
//
void on_combobox_normalise_file_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Peak/album"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Peak"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Mix (scan)Rms/album"));
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), _("Fix (scan)Rms"));
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), 0);
	var_file.AdrComboboxNormalise = GTK_COMBO_BOX (widget);
}
//
//
void on_combobox_normalise_file_changed (GtkComboBox *combobox, gpointer user_data)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	gboolean	 BoolScanACtion = FALSE;
	gint		 FicLevelMix;

	if (var_file.AdrComboboxNormalise == NULL) return;

	FicLevelMix = (gint)gtk_spin_button_get_value (var_file.AdrSpinbuttonNormalise);

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {

			if (detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_WAV) {

				if (detail->Etat_Normalise > NORM_READY_FOR_SELECT) {
					switch (gtk_combo_box_get_active (GTK_COMBO_BOX (var_file.AdrComboboxNormalise))) {
					case 0 :
						detail->Etat_Normalise = NORM_PEAK_ALBUM;
						gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), FALSE);
						break;
					case 1 :
						detail->Etat_Normalise = NORM_PEAK;
						gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), FALSE);
						break;
					case 2 :
						detail->LevelMix = FicLevelMix;
						detail->Etat_Normalise = NORM_RMS_MIX_ALBUM;
						if (detail->Etat_Scan == ETAT_SCAN_NONE) {
							detail->Etat_Scan = ETAT_SCAN_DEMANDE;
							BoolScanACtion = TRUE;
						}
						gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), TRUE);
						break;
					case 3 :
						if (detail->type_infosong_file_is == FILE_IS_WAV) {
							INFO_WAV *info           = (INFO_WAV *)detail->info;
							info->LevelDbfs.NewLevel = FicLevelMix;
						}
						else if (detail->type_infosong_file_is == FILE_IS_MP3) {
							INFO_MP3 *info           = (INFO_MP3 *)detail->info;
							info->LevelDbfs.NewLevel = FicLevelMix;
						}
						else if (detail->type_infosong_file_is == FILE_IS_OGG) {
							INFO_OGG *info           = (INFO_OGG *)detail->info;
							info->LevelDbfs.NewLevel = FicLevelMix;
						}
						detail->Etat_Normalise = NORM_RMS_FIX;
						if (detail->Etat_Scan == ETAT_SCAN_NONE) {
							detail->Etat_Scan = ETAT_SCAN_DEMANDE;
							BoolScanACtion = TRUE;
						}
						gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), TRUE);
						break;
					}
				}
			}
		}
		list = g_list_next (list);
	}

	if (BoolScanACtion == TRUE) FileScanDB_action (TRUE);

	file_pixbuf_update_glist ();
}
//
//
void on_spinbutton_choix_niveau_file_realize (GtkWidget *widget, gpointer user_data)
{
	var_file.AdrSpinbuttonNormalise = GTK_SPIN_BUTTON (widget);
	gtk_spin_button_set_value (var_file.AdrSpinbuttonNormalise, -10);
}
//
//
void on_spinbutton_choix_niveau_file_value_changed (GtkSpinButton *spinbutton, gpointer user_data)
{
	gint		 FicLevelMix = (gint)gtk_spin_button_get_value (var_file.AdrSpinbuttonNormalise);
	GList		*list = NULL;
	DETAIL		*detail = NULL;

	if (var_file.AdrSpinbuttonNormalise == NULL) return;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {

			if (detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_WAV) {

				switch (gtk_combo_box_get_active (GTK_COMBO_BOX (var_file.AdrComboboxNormalise))) {
				case 0 :
				case 1 :
					break;
				case 2 :
					detail->LevelMix = FicLevelMix;
					break;
				case 3 :
					if (detail->type_infosong_file_is == FILE_IS_WAV) {
						INFO_WAV *info           = (INFO_WAV *)detail->info;
						info->LevelDbfs.NewLevel = FicLevelMix;
					}
					else if (detail->type_infosong_file_is == FILE_IS_MP3) {
						INFO_MP3 *info           = (INFO_MP3 *)detail->info;
						info->LevelDbfs.NewLevel = FicLevelMix;
					}
					else if (detail->type_infosong_file_is == FILE_IS_OGG) {
						INFO_OGG *info           = (INFO_OGG *)detail->info;
						info->LevelDbfs.NewLevel = FicLevelMix;
					}
					break;
				}
			}
		}
		list = g_list_next (list);
	}
	file_pixbuf_update_glist ();
}
//
//
void file_maj_destination (gchar *path)
{
	if (TRUE == libutils_test_write (path)) {
		switch (Config.NotebookFile) {
		case NOTEBOOK_FICHIERS_CONVERSION :
			g_free (Config.PathDestinationFileAll);
			Config.PathDestinationFileAll = NULL;
			Config.PathDestinationFileAll = g_strdup (path);
			gtk_combo_box_text_remove (GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), 2 );
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileAll);
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_CONVERSION ]);
			break;
		case NOTEBOOK_FICHIERS_WAV :
			g_free (Config.PathDestinationFileWav);
			Config.PathDestinationFileWav = NULL;
			Config.PathDestinationFileWav = g_strdup (path);
			gtk_combo_box_text_remove (GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), 2);
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileWav);
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_WAV ]);
			break;
		case NOTEBOOK_FICHIERS_MP3OGG :
			g_free (Config.PathDestinationFileMp3Ogg);
			Config.PathDestinationFileMp3Ogg = NULL;
			Config.PathDestinationFileMp3Ogg = g_strdup (path);
			gtk_combo_box_text_remove (GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), 2);
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileMp3Ogg);
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_MP3OGG ]);
			break;
		case NOTEBOOK_FICHIERS_TAGS :
			g_free (Config.PathDestinationFileTags);
			Config.PathDestinationFileTags = NULL;
			Config.PathDestinationFileTags = g_strdup (path);
			gtk_combo_box_text_remove (GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), 2);
			gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(var_file.Adr_combobox_DestFile), Config.PathDestinationFileTags);
			gtk_combo_box_set_active (GTK_COMBO_BOX (var_file.Adr_combobox_DestFile), Config.TabIndiceComboDestFile[ NOTEBOOK_FICHIERS_TAGS ]);
			break;
		}
		file_pixbuf_update_glist ();
	}
}
//
//
void on_combobox_dest_file_changed (GtkComboBox *combobox, gpointer user_data)
{
	// file_combobox_dest_file_changed ();
	if (NULL != var_file.Adr_combobox_DestFile) {

		switch (gtk_combo_box_get_active (var_file.Adr_combobox_DestFile)) {
		// CONVERSIONS VERS LA SOURCE
		case 0 :
			Config.TabIndiceComboDestFile[ Config.NotebookFile ] = 0;
			file_pixbuf_update_glist ();
			break;
		// CHOIX D UNE NOUVELLE DESTINATION
		case 1 :
			Config.TabIndiceComboDestFile[ Config.NotebookFile ] = 2;
			switch (Config.NotebookFile) {
			case NOTEBOOK_FICHIERS_CONVERSION :
				fileselect_create (_PATH_CHOICE_DESTINATION_, Config.PathDestinationFileAll, file_maj_destination);
				break;
			case NOTEBOOK_FICHIERS_WAV :
				fileselect_create (_PATH_CHOICE_DESTINATION_, Config.PathDestinationFileWav, file_maj_destination);
				break;
			case NOTEBOOK_FICHIERS_MP3OGG :
				fileselect_create (_PATH_CHOICE_DESTINATION_, Config.PathDestinationFileMp3Ogg, file_maj_destination);
				break;
			case NOTEBOOK_FICHIERS_TAGS :
				fileselect_create (_PATH_CHOICE_DESTINATION_, Config.PathDestinationFileTags, file_maj_destination);
				break;
			}
			break;
		// APPLIQUER LA DESTINATION
		case 2 :
			Config.TabIndiceComboDestFile[ Config.NotebookFile ] = 2;
			file_pixbuf_update_glist ();
			break;
		}
	}
}
// INIT TREVIEW
//
static void file_cell_edited (GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text, gpointer data)
{
	GtkTreeModel *model = (GtkTreeModel *)data;
	GtkTreePath  *path = gtk_tree_path_new_from_string (path_string);
	GtkTreeIter   iter;
	gint          column = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cell), "column"));

	gtk_tree_model_get_iter (model, &iter, path);

	if (column == COLUMN_FILE_NAME) {
		gchar	*old_text;
		DETAIL	*detail = NULL;
		gchar	*StrVerif = NULL;
		gchar	*ptr = NULL;

		StrVerif = g_strdup (new_text );
		if (NULL != strchr (StrVerif, '/')) {
			while (NULL != (ptr = strchr (StrVerif, '/'))) {
				strcpy (ptr, ptr+1);
			}
		}

		gtk_tree_model_get (model, &iter, COLUMN_FILE_NAME, &old_text, -1);
		g_free (old_text);	old_text = NULL;

		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if(NULL != detail) {

			// STOCKE LE NOM DE FICHIER ORIGINAL
			if (NULL != detail->NameDest) {
				g_free (detail->NameDest);	detail->NameDest = NULL;
			}

			// LE NOM D'ORIGINE
			if( 0 == strlen( StrVerif )) {
				// POUR L'AFFICHAGE
				g_free( StrVerif );	StrVerif = NULL;
				StrVerif = libutils_get_name_without_ext_with_amp( detail->namefile );
				// POUR LA DESTINATION
				detail->NameDest = libutils_get_name_without_ext( detail->namefile );
			}
			// LE NOUVEAU NOM
			else {
				// POUR LA DESTINATION
				detail->NameDest = g_strdup( StrVerif );
				// POUR L'AFFICHAGE
				g_free( StrVerif );	StrVerif = NULL;
				StrVerif = file_get_name_to_treview ( detail );
			}
		}

		// actualise le nom
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_NAME,
					StrVerif,
					-1);

		// actualise FLAC WAV MP3 OGG, ...
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_FLAC,
					file_get_pixbuf (detail, FILE_IS_FLAC),
					-1);
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_WAV,
					file_get_pixbuf (detail, FILE_IS_WAV),
					-1);
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_MP3,
					file_get_pixbuf (detail, FILE_IS_MP3),
					-1);
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_OGG,
					file_get_pixbuf (detail, FILE_IS_OGG),
					-1);
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_M4A,
					file_get_pixbuf (detail, FILE_IS_M4A),
					-1);
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_MPC,
					file_get_pixbuf (detail, FILE_IS_MPC),
					-1);
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_APE,
					file_get_pixbuf (detail, FILE_IS_APE),
					-1);
		gtk_list_store_set (GTK_LIST_STORE (model),
					&iter,
					COLUMN_FILE_WAVPACK,
					file_get_pixbuf (detail, FILE_IS_WAVPACK),
					-1);

		g_free (StrVerif);	StrVerif = NULL;
	}
	gtk_tree_path_free (path);
	file_pixbuf_update_glist();
}
// SELECTION DES COLONNES TypeFileIs DONT LES LIGNES SONT EN SELECTION
//
void file_select_from_click_column (TYPE_FILE_IS TypeFileIs)
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*BeginList = NULL;
	GList		*list = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;
	gboolean	 valid;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file.Adr_TreeView));

	// RECUP. DE LA LIGNE EN SELECTION ET VALIDATION
	if (NULL != (BeginList = gtk_tree_selection_get_selected_rows (var_file.Adr_Line_Selected, &model))) {
		list = g_list_first (BeginList);
		while (list) {
			if ((path = list->data)) {
				gtk_tree_model_get_iter (model, &iter, path);
				gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);

				if (TypeFileIs == FILE_IS_WAV) {
					detail->EtatSelection_Wav = file_get_next_flag (detail, FILE_IS_WAV);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_WAV,
								file_get_pixbuf (detail, FILE_IS_WAV),
								-1);
				}
				else if (TypeFileIs == FILE_IS_FLAC) {
					detail->EtatSelection_Flac = file_get_next_flag (detail, FILE_IS_FLAC);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_FLAC,
								file_get_pixbuf (detail, FILE_IS_FLAC),
								-1);
				}
				else if (TypeFileIs == FILE_IS_APE) {
					detail->EtatSelection_Ape = file_get_next_flag (detail, FILE_IS_APE);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_APE,
								file_get_pixbuf (detail, FILE_IS_APE),
								-1);
				}
				else if (TypeFileIs == FILE_IS_WAVPACK) {
					detail->EtatSelection_WavPack = file_get_next_flag (detail, FILE_IS_WAVPACK);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_WAVPACK,
								file_get_pixbuf (detail, FILE_IS_WAVPACK),
								-1);
				}
				else if (TypeFileIs == FILE_IS_OGG) {
					detail->EtatSelection_Ogg = file_get_next_flag (detail, FILE_IS_OGG);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_OGG,
								file_get_pixbuf (detail, FILE_IS_OGG),
								-1);
				}
				else if (TypeFileIs == FILE_IS_M4A) {
					detail->EtatSelection_M4a = file_get_next_flag (detail, FILE_IS_M4A);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_M4A,
								file_get_pixbuf (detail, FILE_IS_M4A),
								-1);
				}
				else if (TypeFileIs == FILE_IS_AAC) {
					detail->EtatSelection_Aac = file_get_next_flag (detail, FILE_IS_AAC);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_AAC,
								file_get_pixbuf (detail, FILE_IS_AAC),
								-1);
				}
				else if (TypeFileIs == FILE_IS_MPC) {
					detail->EtatSelection_Mpc = file_get_next_flag (detail, FILE_IS_MPC);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_MPC,
								file_get_pixbuf (detail, FILE_IS_MPC),
								-1);
				}
				else if (TypeFileIs == FILE_IS_MP3) {
					detail->EtatSelection_Mp3 = file_get_next_flag (detail, FILE_IS_MP3);
					gtk_list_store_set (var_file.Adr_List_Store,
								&iter,
								COLUMN_FILE_MP3,
								file_get_pixbuf (detail, FILE_IS_MP3),
								-1);
				}
			}
			list = g_list_next (list);
		}
	}
	// SINON SUPPRESSION DES LIGNES EN SELECTION
	else {
		valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
		while (valid) {
			gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
			if (TypeFileIs == FILE_IS_WAV) {
				detail->EtatSelection_Wav = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_WAV,
							file_get_pixbuf (detail, FILE_IS_WAV),
							-1);
			}
			else if (TypeFileIs == FILE_IS_FLAC) {
				detail->EtatSelection_Flac = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_FLAC,
							file_get_pixbuf (detail, FILE_IS_FLAC),
							-1);
			}
			else if (TypeFileIs == FILE_IS_APE) {
				detail->EtatSelection_Ape = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_APE,
							file_get_pixbuf (detail, FILE_IS_APE),
							-1);
			}
			else if (TypeFileIs == FILE_IS_WAVPACK) {
				detail->EtatSelection_WavPack = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_WAVPACK,
							file_get_pixbuf (detail, FILE_IS_WAVPACK),
							-1);
			}
			else if (TypeFileIs == FILE_IS_OGG) {
				detail->EtatSelection_Ogg = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_OGG,
							file_get_pixbuf (detail, FILE_IS_OGG),
							-1);
			}
			else if (TypeFileIs == FILE_IS_M4A) {
				detail->EtatSelection_M4a = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_M4A,
							file_get_pixbuf (detail, FILE_IS_M4A),
							-1);
			}
			else if (TypeFileIs == FILE_IS_AAC) {
				detail->EtatSelection_Aac = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_AAC,
							file_get_pixbuf (detail, FILE_IS_AAC),
							-1);
			}
			else if (TypeFileIs == FILE_IS_MPC) {
				detail->EtatSelection_Mpc = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_MPC,
							file_get_pixbuf (detail, FILE_IS_MPC),
							-1);
			}
			else if (TypeFileIs == FILE_IS_MP3) {
				detail->EtatSelection_Mp3 = ETAT_ATTENTE;
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_MP3,
							file_get_pixbuf (detail, FILE_IS_MP3),
							-1);
			}
			valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
		}
	}
	file_set_flag_buttons ();
}
//
//
void file_pixbuf_update_glist (void)
{
	GtkTreeIter    iter;
	gboolean       valid;
	DETAIL        *detail = NULL;
	GdkPixbuf     *pixbuf = NULL;

	if (var_file.Adr_Tree_Model == NULL) return;

	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if (NULL != detail) {

			// pixbuf  = file_get_pixbuf_play (detail, detail->EtatPlay);
			// gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_PLAY, pixbuf, -1);

			pixbuf = file_get_pixbuf_trash (detail);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_TRASH, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_FLAC);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_FLAC, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_WAV);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_WAV, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_MP3);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_MP3, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_OGG);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_OGG, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_M4A);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_M4A, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_AAC);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_AAC, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_MPC);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_MPC, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_APE);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_APE, pixbuf, -1);

			pixbuf = file_get_pixbuf (detail, FILE_IS_WAVPACK);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_WAVPACK, pixbuf, -1);

			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_NORMALIZE, file_get_str_level_normalise (detail), -1);

			pixbuf = file_get_pixbuf_replaygain (detail, FALSE);
			gtk_list_store_set (
					var_file.Adr_List_Store,
					&iter,
					COLUMN_FILE_REPLAYGAIN,
					pixbuf,
					-1);
		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}

	file_set_flag_buttons ();
}
//
//
void file_set_etat_column_normalise (void)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	gboolean	 BoolScanACtion = FALSE;
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*BeginList = NULL;
	GtkTreePath	*path;
	gboolean	valid;
	gint		NbrNormalise = 0;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file.Adr_TreeView));

	// RECUP. DES LIGNES EN SELECTION POUR LA SELECTION
	//
	if (NULL != (BeginList = gtk_tree_selection_get_selected_rows (var_file.Adr_Line_Selected, &model))) {
		list = g_list_first (BeginList);
		while (list) {
			if ((path = list->data)) {
				gtk_tree_model_get_iter (model, &iter, path);
				gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
				if (NULL != detail) {
					if (detail->type_infosong_file_is == FILE_IS_MP3 ||
					    detail->type_infosong_file_is == FILE_IS_OGG ||
					    detail->type_infosong_file_is == FILE_IS_WAV) {

						if (NbrNormalise ++ >= MAX_ARG_CONV) break;

						if (detail->Etat_Normalise > NORM_READY_FOR_SELECT) {

							detail->Etat_Normalise = NORM_READY_FOR_SELECT;
							detail->Etat_Scan = ETAT_SCAN_NONE;
						}
						else {
							switch (gtk_combo_box_get_active (GTK_COMBO_BOX (var_file.AdrComboboxNormalise))) {
							case 0 :
								detail->Etat_Normalise = NORM_PEAK_ALBUM;
								break;
							case 1 :
								detail->Etat_Normalise = NORM_PEAK;
								break;
							case 2 :
								detail->Etat_Normalise = NORM_RMS_MIX_ALBUM;
								if (detail->Etat_Scan == ETAT_SCAN_NONE) {
									detail->Etat_Scan = ETAT_SCAN_DEMANDE;
									BoolScanACtion = TRUE;
								}
								gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), TRUE);
								break;
							case 3 :
								detail->Etat_Normalise = NORM_RMS_FIX;
								if (detail->Etat_Scan == ETAT_SCAN_NONE) {
									detail->Etat_Scan = ETAT_SCAN_DEMANDE;
									BoolScanACtion = TRUE;
								}
								gtk_widget_set_sensitive (GTK_WIDGET (GTK_SPIN_BUTTON (var_file.AdrSpinbuttonNormalise)), TRUE);
								break;
							}
						}
					}
				}
			}
			list = g_list_next (list);
		}
	}
	// SINON SUPPRESSION DES SELECTIONS
	//
	else {
		valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
		while (valid) {
			gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
			detail->Etat_Normalise = NORM_READY_FOR_SELECT;
			detail->Etat_Scan = ETAT_SCAN_NONE;
			valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
		}
	}
	if (BoolScanACtion == TRUE) FileScanDB_action (TRUE);
	file_pixbuf_update_glist ();
}
//
//
void file_set_etat_column_replaygain (void)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*BeginList = NULL;
	GtkTreePath	*path;
	gboolean	 valid;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file.Adr_TreeView));

	// RECUP. DES LIGNES EN SELECTION POUR LA SELECTION
	//
	if (NULL != (BeginList = gtk_tree_selection_get_selected_rows (var_file.Adr_Line_Selected, &model))) {
		list = g_list_first (BeginList);
		while (list) {
			if ((path = list->data)) {
				gtk_tree_model_get_iter (model, &iter, path);
				gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
				if (NULL != detail) {
					file_get_pixbuf_replaygain (detail, TRUE);
				}
			}
			list = g_list_next (list);
		}
	}
	// SINON SUPPRESSION DES SELECTIONS
	//
	else {
		valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
		while (valid) {
			gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
			detail->Etat_ReplayGain = RPG_ATTENTE;
			valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
		}
	}
	file_pixbuf_update_glist ();
}
//
//
void file_set_etat_column_trash (void)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*BeginList = NULL;
	GtkTreePath	*path;
	gboolean	 valid;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file.Adr_TreeView));

	// RECUP. DES LIGNES EN SELECTION POUR LA SELECTION
	//
	if (NULL != (BeginList = gtk_tree_selection_get_selected_rows (var_file.Adr_Line_Selected, &model))) {
		list = g_list_first (BeginList);
		while (list) {
			if ((path = list->data)) {
				gtk_tree_model_get_iter (model, &iter, path);
				gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
				if (NULL != detail) {
					detail->EtatTrash = FILE_TRASH_OK;
				}
			}
			list = g_list_next (list);
		}
	}
	// SINON SUPPRESSION DES SELECTIONS
	//
	else {
		valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
		while (valid) {
			gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
			detail->EtatTrash = FILE_TRASH_NONE;
			valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
		}
	}
	file_pixbuf_update_glist ();
}
//
//
gboolean file_ok_to_normalise (void)
{
	GList		*list = NULL;
	DETAIL		*detail = NULL;
	gint		Nbr = 0;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {

			if (detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_WAV) {

				if (detail->Etat_Normalise > NORM_READY_FOR_SELECT) Nbr ++;
			}
		}
		list = g_list_next (list);
	}
	return (Nbr < MAX_ARG_CONV ? TRUE : FALSE);
}
//
//
void file_selected_column (GtkTreeViewColumn *treeviewcolumn, gpointer user_data)
{
	gint		NumColonne = GPOINTER_TO_INT(user_data);
	TYPE_FILE_IS	TypeFileIs = FILE_IS_NONE;

	switch (NumColonne) {
	case -1 : file_set_etat_column_trash ();	return;
	case  0 : TypeFileIs = FILE_IS_WAV;		break;
	case  1 : TypeFileIs = FILE_IS_FLAC;		break;
	case  2 : TypeFileIs = FILE_IS_APE;		break;
	case  3 : TypeFileIs = FILE_IS_WAVPACK;		break;
	case  4 : TypeFileIs = FILE_IS_OGG;		break;
	case  5 : TypeFileIs = FILE_IS_M4A;		break;
	case  6 : TypeFileIs = FILE_IS_AAC;		break;
	case  7 : TypeFileIs = FILE_IS_MPC;		break;
	case  8 : TypeFileIs = FILE_IS_MP3;		break;
	case  9 : file_set_etat_column_normalise ();	return;
	case 10 : file_set_etat_column_replaygain ();	return;
	}
	file_select_from_click_column (TypeFileIs);
}
//
//
gboolean file_event_click_mouse (GtkWidget *treeview, GdkEventButton *event, gpointer data)
{
	gboolean            BoolFlac = FALSE;
	gboolean            BoolWav = FALSE;
	gboolean            BoolMp3 = FALSE;
	gboolean            BoolOgg = FALSE;
	gboolean            BoolM4a = FALSE;
	gboolean            BoolAac = FALSE;
	gboolean            BoolShn = FALSE;
	gboolean            BoolWma = FALSE;
	gboolean            BoolMpc = FALSE;
	gboolean            BoolApe = FALSE;
	gboolean            BoolWavPack = FALSE;

	gboolean            BoolSelectColPlay = FALSE;
	gboolean            BoolSelectColTrash = FALSE;
	gboolean            BoolSelectColType = FALSE;
	gboolean            BoolSelectColFlac = FALSE;
	gboolean            BoolSelectColWav = FALSE;
	gboolean            BoolSelectColMp3 = FALSE;
	gboolean            BoolSelectColOgg = FALSE;
	gboolean            BoolSelectColM4a = FALSE;
	gboolean            BoolSelectColAac = FALSE;
	gboolean            BoolSelectColMpc = FALSE;
	gboolean            BoolSelectColApe = FALSE;
	gboolean            BoolSelectColWavP = FALSE;
	gboolean            BoolSelectColTime = FALSE;
	gboolean            BoolSelectColNorm = FALSE;
	gboolean            BoolSelectColReplayGain = FALSE;
	gboolean            BoolSelectColNom = FALSE;

	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	gint                i;

	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)data;
	// GdkPixbuf          *Pixbuf = NULL;
	DETAIL             *detail = NULL;
	gint                Pos_X = 0, Pos_Y = 0;
	gboolean            bool_key_Control = (keys.keyval == GDK_KEY_Control_L || keys.keyval == GDK_KEY_Control_R);
	gboolean            bool_key_Shift   = (keys.keyval == GDK_KEY_Shift_L || keys.keyval == GDK_KEY_Shift_R);
	gboolean            bool_key_Release = (bool_key_Control == FALSE &&  bool_key_Shift == FALSE);
	gboolean            bool_click_gauche = (event->button == 1);
	gboolean            bool_click_droit = (event->button == 3);

	/* Single clicks only */
	if (event->type != GDK_BUTTON_PRESS) return (FALSE);

	/*
	if (event->type == GDK_2BUTTON_PRESS) {
		g_print ("event->type == GDK_2BUTTON_PRESS\n");
	}
	g_print ("event->button = %d\n", event->button);
	event->button = 1 = GAUCHE
	event->button = 2 = CENTRE
	event->button = 3 = DROITE
	*/

	/* Si pas de selection a cet endroit retour */
	if (FALSE == gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					  (gint)event->x, (gint)event->y,
					   &path, &column, &Pos_X, &Pos_Y)) return (FALSE);

	// RECCPERATION DE LA STRUCTURE
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
	if (NULL == detail) return (FALSE);

	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_FILE_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			/* g_print ("\tNUM IS: %d\n", i); */
			switch ( i ) {
			case NUM_TREE_FILE_Play :		BoolSelectColPlay		= TRUE;	break;
			case NUM_TREE_FILE_Trash :		BoolSelectColTrash		= TRUE;	break;
			case NUM_TREE_FILE_Type :		BoolSelectColType		= TRUE;	break;
			case NUM_TREE_FILE_Wav :		BoolSelectColWav		= detail->type_infosong_file_is == FILE_IS_WAV ? FALSE : TRUE; break;
			case NUM_TREE_FILE_Flac :		BoolSelectColFlac		= detail->type_infosong_file_is == FILE_IS_FLAC ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Ape :		BoolSelectColApe		= detail->type_infosong_file_is == FILE_IS_APE ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_WavP :		BoolSelectColWavP		= detail->type_infosong_file_is == FILE_IS_WAVPACK ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Ogg :		BoolSelectColOgg		= detail->type_infosong_file_is == FILE_IS_OGG ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_M4a :		BoolSelectColM4a		= detail->type_infosong_file_is == FILE_IS_M4A ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Aac :		BoolSelectColAac		= detail->type_infosong_file_is == FILE_IS_AAC ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Mpc :		BoolSelectColMpc		= detail->type_infosong_file_is == FILE_IS_MPC ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Mp3 :		BoolSelectColMp3		= detail->type_infosong_file_is == FILE_IS_MP3 ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Time :		BoolSelectColTime		= TRUE;	break;
			case NUM_TREE_FILE_Normalise :		BoolSelectColNorm		= TRUE;	break;
			case NUM_TREE_FILE_ReplayGain :		BoolSelectColReplayGain		= TRUE;	break;
			case NUM_TREE_FILE_Name :		BoolSelectColNom		= TRUE;	break;
			default: return (FALSE);
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}
	if( BoolSelectColNom );
	if( BoolSelectColTime );
	if( BoolSelectColType );

	BoolSelectColNorm &= (detail->type_infosong_file_is == FILE_IS_WAV ||
			      detail->type_infosong_file_is == FILE_IS_OGG ||
			      detail->type_infosong_file_is == FILE_IS_MP3);

	if (FALSE == BoolSelectColNorm && FALSE == BoolSelectColReplayGain) {
		if (Pos_X < 18 || Pos_X > 30) return (FALSE);
		if (Pos_Y < 6 || Pos_Y > 18) return (FALSE);
	}

	if (bool_click_droit &&
	   (BoolSelectColFlac ||
	    BoolSelectColWav ||
	    BoolSelectColMp3 ||
	    BoolSelectColOgg ||
	    BoolSelectColM4a ||
	    BoolSelectColAac ||
	    BoolSelectColMpc ||
	    BoolSelectColApe ||
	    BoolSelectColWavP)) {

	    	/*GdkPixbuf    *Pixbuf = NULL;
	    	TYPE_FILE_IS  TypeFileIs;

		if (BoolSelectColFlac)      {Pixbuf = file_get_pixbuf (detail, FILE_IS_FLAC);	TypeFileIs = FILE_IS_FLAC;	}
		else if (BoolSelectColWav)  {Pixbuf = file_get_pixbuf (detail, FILE_IS_WAV);	TypeFileIs = FILE_IS_WAV;	}
		else if (BoolSelectColMp3)  {Pixbuf = file_get_pixbuf (detail, FILE_IS_MP3);	TypeFileIs = FILE_IS_MP3;	}
		else if (BoolSelectColOgg)  {Pixbuf = file_get_pixbuf (detail, FILE_IS_OGG);	TypeFileIs = FILE_IS_OGG;	}
		else if (BoolSelectColM4a)  {Pixbuf = file_get_pixbuf (detail, FILE_IS_M4A);	TypeFileIs = FILE_IS_M4A;	}
		else if (BoolSelectColAac)  {Pixbuf = file_get_pixbuf (detail, FILE_IS_AAC);	TypeFileIs = FILE_IS_AAC;	}
		else if (BoolSelectColMpc)  {Pixbuf = file_get_pixbuf (detail, FILE_IS_MPC);	TypeFileIs = FILE_IS_MPC;	}
		else if (BoolSelectColApe)  {Pixbuf = file_get_pixbuf (detail, FILE_IS_APE);	TypeFileIs = FILE_IS_APE;	}
		else if (BoolSelectColWavP) {Pixbuf = file_get_pixbuf (detail, FILE_IS_WAVPACK);	TypeFileIs = FILE_IS_WAVPACK;	}

		if (Pixbuf != NULL && Pixbuf != var_file.Pixbuf_NotInstall) popup_file (detail, TypeFileIs);*/

	    	TYPE_FILE_IS  TypeFileIs = FILE_IS_NONE;

		if (BoolSelectColFlac)		TypeFileIs = FILE_IS_FLAC;
		else if (BoolSelectColWav)	TypeFileIs = FILE_IS_WAV;
		else if (BoolSelectColMp3)	TypeFileIs = FILE_IS_MP3;
		else if (BoolSelectColOgg)	TypeFileIs = FILE_IS_OGG;
		else if (BoolSelectColM4a)	TypeFileIs = FILE_IS_M4A;
		else if (BoolSelectColAac)	TypeFileIs = FILE_IS_AAC;
		else if (BoolSelectColMpc)	TypeFileIs = FILE_IS_MPC;
		else if (BoolSelectColApe)	TypeFileIs = FILE_IS_APE;
		else if (BoolSelectColWavP)	TypeFileIs = FILE_IS_WAVPACK;

		popup_file (detail, TypeFileIs);
		return (FALSE);
	}
	else if (bool_click_droit && BoolSelectColReplayGain) {
		if (detail->type_infosong_file_is == FILE_IS_FLAC ||
		    detail->type_infosong_file_is == FILE_IS_MP3 ||
		    detail->type_infosong_file_is == FILE_IS_WAVPACK ||
		    detail->type_infosong_file_is == FILE_IS_OGG) {

	    		/*GdkPixbuf *Pixbuf = file_get_pixbuf_replaygain (detail, FALSE);

			if (Pixbuf != NULL && Pixbuf != var_file.Pixbuf_NotInstall) popup_file_ReplayGain (detail);*/
			popup_file_ReplayGain (detail);
		}
		return (FALSE);
	}
	else if (bool_click_droit && BoolSelectColTrash) {
		popup_trash ();
	}


	if (detail->type_infosong_file_is != FILE_IS_FLAC && BoolSelectColFlac) {
		BoolFlac = file_bool_from_to (detail, FILE_IS_FLAC);
	}
	else if (detail->type_infosong_file_is != FILE_IS_WAV && BoolSelectColWav) {
		BoolWav  = file_bool_from_to (detail, FILE_IS_WAV);
	}
	else if (detail->type_infosong_file_is != FILE_IS_MP3 && BoolSelectColMp3) {
		BoolMp3  = file_bool_from_to (detail, FILE_IS_MP3);
	}
	else if (detail->type_infosong_file_is != FILE_IS_OGG && BoolSelectColOgg) {
		BoolOgg  = file_bool_from_to (detail, FILE_IS_OGG);
	}
	else if (detail->type_infosong_file_is != FILE_IS_M4A && BoolSelectColM4a) {
		BoolM4a  = file_bool_from_to (detail, FILE_IS_M4A);
	}
	else if (detail->type_infosong_file_is != FILE_IS_AAC && BoolSelectColAac) {
		BoolAac  = file_bool_from_to (detail, FILE_IS_AAC);
	}
	else if (detail->type_infosong_file_is != FILE_IS_MPC && BoolSelectColMpc) {
		BoolMpc  = file_bool_from_to (detail, FILE_IS_MPC);
	}
	else if (detail->type_infosong_file_is != FILE_IS_APE && BoolSelectColApe) {
		BoolApe  = file_bool_from_to (detail, FILE_IS_APE);
	}
	else if (detail->type_infosong_file_is != FILE_IS_WAVPACK && BoolSelectColWavP) {
		BoolWavPack  = file_bool_from_to (detail, FILE_IS_WAVPACK);
	}

	if (BoolFlac || BoolWav || BoolMp3 || BoolOgg || BoolShn || BoolM4a || BoolAac || BoolWma || BoolMpc || BoolApe || BoolWavPack) {
		if (TRUE == bool_key_Control && TRUE == bool_click_gauche) {
			PRINT("TRUE == bool_key_Control && TRUE == bool_click_gauche");
		}
		else if (TRUE == bool_key_Release && TRUE == bool_click_gauche) {

			if (TRUE == BoolWav) {
				detail->EtatSelection_Wav = file_get_next_flag (detail, FILE_IS_WAV);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_WAV,
							file_get_pixbuf (detail, FILE_IS_WAV),
							-1);
			}
			else if (TRUE == BoolFlac) {
				detail->EtatSelection_Flac = file_get_next_flag (detail, FILE_IS_FLAC);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_FLAC,
							file_get_pixbuf (detail, FILE_IS_FLAC),
							-1);
			}
			else if (TRUE == BoolApe) {
				detail->EtatSelection_Ape = file_get_next_flag (detail, FILE_IS_APE);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_APE,
							file_get_pixbuf (detail, FILE_IS_APE),
							-1);
			}
			else if (TRUE == BoolWavPack) {
				detail->EtatSelection_WavPack = file_get_next_flag (detail, FILE_IS_WAVPACK);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_WAVPACK,
							file_get_pixbuf (detail, FILE_IS_WAVPACK),
							-1);
			}
			else if (TRUE == BoolOgg) {
				detail->EtatSelection_Ogg = file_get_next_flag (detail, FILE_IS_OGG);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_OGG,
							file_get_pixbuf (detail, FILE_IS_OGG),
							-1);
			}
			else if (TRUE == BoolM4a) {
				detail->EtatSelection_M4a = file_get_next_flag (detail, FILE_IS_M4A);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_M4A,
							file_get_pixbuf (detail, FILE_IS_M4A),
							-1);
			}
			else if (TRUE == BoolAac) {
				detail->EtatSelection_Aac = file_get_next_flag (detail, FILE_IS_AAC);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_AAC,
							file_get_pixbuf (detail, FILE_IS_AAC),
							-1);
			}
			else if (TRUE == BoolMpc) {
				detail->EtatSelection_Mpc = file_get_next_flag (detail, FILE_IS_MPC);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_MPC,
							file_get_pixbuf (detail, FILE_IS_MPC),
							-1);
			}
			else if (TRUE == BoolMp3) {
				detail->EtatSelection_Mp3 = file_get_next_flag (detail, FILE_IS_MP3);
				gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_MP3,
							file_get_pixbuf (detail, FILE_IS_MP3),
							-1);
			}

		}
	}
	else if (TRUE == BoolSelectColNorm) {


			/* Une seule selection par l'utilisateur */
			if (TRUE == bool_key_Release && TRUE == bool_click_gauche) {

				gtk_widget_set_sensitive (GTK_WIDGET (GTK_COMBO_BOX (var_file.AdrComboboxNormalise)), TRUE);

				if (file_ok_to_normalise () && detail->Etat_Normalise == NORM_READY_FOR_SELECT) {

					switch (gtk_combo_box_get_active (GTK_COMBO_BOX (var_file.AdrComboboxNormalise))) {
					case 0 :
						detail->Etat_Normalise = NORM_PEAK_ALBUM;
						break;
					case 1 :
						detail->Etat_Normalise = NORM_PEAK;
						break;
					case 2 :
						detail->Etat_Normalise = NORM_RMS_MIX_ALBUM;
						break;
					case 3 :
						detail->Etat_Normalise = NORM_RMS_FIX;
						break;
					}
				}
				else if (detail->Etat_Normalise > NORM_READY_FOR_SELECT) {
					detail->Etat_Normalise = NORM_READY_FOR_SELECT;
					detail->Etat_Scan = ETAT_SCAN_NONE;
				}

				gtk_list_store_set (
						var_file.Adr_List_Store,
						&iter,
						COLUMN_FILE_NORMALIZE,
						file_get_str_level_normalise (detail),
						-1);
			}

	}
	else if (TRUE == BoolSelectColReplayGain) {

		if (TRUE == bool_key_Release && TRUE == bool_click_gauche) {

			gtk_list_store_set (
					var_file.Adr_List_Store,
					&iter,
					COLUMN_FILE_REPLAYGAIN,
					file_get_pixbuf_replaygain (detail, TRUE),
					-1);
		}
	}
	else if (TRUE == BoolSelectColTrash) {

		if (TRUE == bool_key_Release && TRUE == bool_click_gauche) {
			detail->EtatTrash = (detail->EtatTrash == FILE_TRASH_NONE) ? FILE_TRASH_OK : FILE_TRASH_NONE;
			gtk_list_store_set (
					var_file.Adr_List_Store,
					&iter,
					COLUMN_FILE_TRASH,
					file_get_pixbuf_trash (detail),
					-1);
		}
	}
	else if (TRUE == BoolSelectColPlay) {
		PlayFile_play (detail->namefile);
	}

	file_set_flag_buttons ();

	return (FALSE);
}
//
//
void file_changed_selection_row (GtkTreeSelection *selection, gpointer data)
{
	var_file.Adr_Line_Selected = selection;
	file_set_flag_buttons ();
}
//
//
gboolean file_key_press_event (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	if( TRUE == keys.BoolGDK_Control_A ) {	// CONTROL_A
		gtk_tree_selection_unselect_all (var_file.Adr_Line_Selected);
		gtk_tree_selection_select_all (var_file.Adr_Line_Selected);
	}
	if (keys.keyval == GDK_KEY_Delete) {
		GtkTreeIter   iter;
		if (gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter)) {
			on_file_button_del_file_clicked (NULL, NULL);
			return (FALSE);
		}
	}
	return (TRUE);
}
//
// Drag import
//
static void file_drag_data_received(
					GtkWidget        *widget,
					GdkDragContext   *context,
					gint              x,
					gint              y,
					GtkSelectionData *data,
					guint             info,
					guint             time,
					gpointer          user_data)
{

	// Une copie ne peut aller vers elle meme !!!
	if (gtk_drag_get_source_widget(context) != widget) {
		// dragndrop_list_drag_data (widget, (gchar *)data->data);
		dragndrop_list_drag_data( widget, (gchar*)gtk_selection_data_get_data( data ));
	}
}
//
// DRag export
//
static void file_drag_data_drop(
					GtkWidget *widget,
					GdkDragContext *dc,
					GtkSelectionData *selection_data,
					guint info,
					guint t,
					gpointer data )
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*begin_list = NULL;
	GList		*list = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;
	gchar		*text = NULL;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(widget));
	begin_list = gtk_tree_selection_get_selected_rows (var_file.Adr_Line_Selected, &model);
	list = g_list_first (begin_list);
	while (list) {
		if( NULL != ( path = list->data )) {
			gtk_tree_model_get_iter( model, &iter, path );
			gtk_tree_model_get( var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1 );

			// DEBUG DRAG AND DROP
			// [ Tue, 03 May 2011 17:39:08 +0200 ]
			// XCFA-4.1.0
			// -----------------------------------------------------------
			// OLD CODE:
			// 	text = g_strdup( detail->namefile );
			// NEW_CODE:
			text = g_strdup_printf( "file://%s", detail->namefile );

			gdk_drag_status (dc, GDK_ACTION_COPY, t);

			/*
			gtk_selection_data_set(
						GtkSelectionData *selection_data,
						GdkAtom type,
						gint format,
						const guchar *data,
						gint length);
			*/
			gtk_selection_data_set(
						selection_data,
						// GDK_SELECTION_TYPE_STRING,
						// selection_data->target,
						GDK_POINTER_TO_ATOM(gtk_selection_data_get_data( selection_data )),
						8,
						(guchar *)text,
						strlen( text )
						);
			g_free( text );
			text = NULL;
		}
		list = g_list_next( list );
	}
}
//
//
void file_set_help (DETAIL *detail, TYPE_FILE_IS TypeFileIs)
{
	ETAT_SELECTION	etat = ETAT_ATTENTE;

	etat = file_get_next_flag( detail, TypeFileIs );

	switch( etat ) {
	case ETAT_PRG_NONE :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "" );
		break;
	case ETAT_PRG_ABSENT :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("The external ripping program is missing"));
		break;
	case ETAT_ATTENTE :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Waiting for selection."));
		break;
	case ETAT_ATTENTE_EXIST :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Waiting for Selection. File exists."));
		break;
	case ETAT_SELECT :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Selected files"));
		break;
	case ETAT_SELECT_EXIST :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Selected files. File exists."));
		break;
	case ETAT_SELECT_EXPERT :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Selection - expert mode."));
		break;
	case ETAT_SELECT_EXPERT_EXIST :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right click = Menu) / Selection - expert mode. File exists."));
		break;
	}
}
//
//
void file_set_normalise (ETAT_NORMALISE p_EtatNormalise)
{
	if (FALSE == file_ok_to_normalise ()) {
		gchar	*Str = g_strdup_printf (_("MAX FILES REACHED = %d"), MAX_ARG_CONV);
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, Str );
		g_free (Str);	Str = NULL;
	}
	else {
		switch (p_EtatNormalise) {
		case NORM_NONE  :
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "VOIR LA FONCTION: file_set_normalise()");
			break;
		case NORM_READY_FOR_SELECT :
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("Normalise: Waiting for selection."));
			break;
		case NORM_PEAK_ALBUM :
			if (FALSE == file_peak_get_size_is_ok (NORM_PEAK_ALBUM, NULL)) {
				StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("WARNING: Not enough space available in the temp folder."));
			}
			else {
				StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("PEAK-ALBUM: Maximum volume amplification for a group of files according to the deviations in level between them"));
			}			
			break;
		case NORM_PEAK :
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("PEAK: Maximum volume amplification of each file."));
			break;
		case NORM_RMS_MIX_ALBUM :
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("RMS-ALBUM: Adjusting the average volume for a group of files according to the average level of deviation between them."));
			break;
		case NORM_RMS_FIX :
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("RMS: Average volume amplification of each file."));
			break;
		}
	}
}
//
//
void file_set_replaygain (ETAT_REPLAYGAIN p_EtatReplayGain)
{
	switch (p_EtatReplayGain) {
	case RPG_NONE :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "" );
		break;
	case RPG_ATTENTE :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right Click = Menu) / ReplayGain: Waiting for selection."));
		break;
	case RPG_PISTE :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right Click = Menu) / Track mode."));
		break;
	case RPG_ALBUM :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right Click = Menu) / Album mode."));
		break;
	case RPG_EFFACER :
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right Click = Menu) / Erase mode."));
		break;
	}
}
// AFFICHAGE DU NOM COMPLET DU FICHIER SI SURVOL PAR LE CURSEUR SOURIS DU CHAMPS 'Nom'
//
gboolean file_event (GtkWidget *treeview, GdkEvent *event, gpointer user_data)
{
	gint                x, y;
	GdkModifierType     state;
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)user_data;
	DETAIL             *detail = NULL;
	gint                Pos_X = 0, Pos_Y = 0;
	gint                i;
	gboolean            BoolSelectColPlay = FALSE;
	gboolean            BoolSelectColTrash = FALSE;
	gboolean            BoolSelectColType = FALSE;
	gboolean            BoolSelectColFlac = FALSE;
	gboolean            BoolSelectColWav = FALSE;
	gboolean            BoolSelectColMp3 = FALSE;
	gboolean            BoolSelectColOgg = FALSE;
	gboolean            BoolSelectColM4a = FALSE;
	gboolean            BoolSelectColAac = FALSE;
	gboolean            BoolSelectColMpc = FALSE;
	gboolean            BoolSelectColApe = FALSE;
	gboolean            BoolSelectColWavP = FALSE;
	gboolean            BoolSelectColTime = FALSE;
	gboolean            BoolSelectColNorm = FALSE;
	gboolean            BoolSelectColReplayGain = FALSE;
	gboolean            BoolSelectColNom = FALSE;

	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &x, &y, &state );
	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &x, &y, &state);
	if (FALSE == gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					   x, y,
					   &path, &column, &Pos_X, &Pos_Y)) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, " ");
		return (FALSE);
	}

	// RECUPERATION DE LA STRUCTURE POINTEE PAR LE CURSEUR SOURIS
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
	if (NULL == detail) return (FALSE);

	// SORTIE SI LE WIDGET SOUS LE CURSEUR NE CORRESPOND PAS AU TREEVIEW
	if (gtk_get_event_widget (event) != var_file.Adr_TreeView) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
		return (FALSE);
	}

	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_FILE_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);

		if (ColumnDum == column) {
			switch ( i ) {
			case NUM_TREE_FILE_Play :		BoolSelectColPlay	= TRUE;	break;
			case NUM_TREE_FILE_Trash :		BoolSelectColTrash	= TRUE;	break;
			case NUM_TREE_FILE_Type :		BoolSelectColType	= TRUE;	break;
			case NUM_TREE_FILE_Wav :		BoolSelectColWav	= detail->type_infosong_file_is == FILE_IS_WAV ? FALSE : TRUE; break;
			case NUM_TREE_FILE_Flac :		BoolSelectColFlac	= detail->type_infosong_file_is == FILE_IS_FLAC ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Ape :		BoolSelectColApe	= detail->type_infosong_file_is == FILE_IS_APE ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_WavP :		BoolSelectColWavP	= detail->type_infosong_file_is == FILE_IS_WAVPACK ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Ogg :		BoolSelectColOgg	= detail->type_infosong_file_is == FILE_IS_OGG ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_M4a :		BoolSelectColM4a	= detail->type_infosong_file_is == FILE_IS_M4A ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Aac :		BoolSelectColAac	= detail->type_infosong_file_is == FILE_IS_AAC ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Mpc :		BoolSelectColMpc	= detail->type_infosong_file_is == FILE_IS_MPC ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Mp3 :		BoolSelectColMp3	= detail->type_infosong_file_is == FILE_IS_MP3 ? FALSE : TRUE;	break;
			case NUM_TREE_FILE_Time :		BoolSelectColTime	= TRUE;	break;
			case NUM_TREE_FILE_Normalise :		BoolSelectColNorm	= TRUE;	break;
			case NUM_TREE_FILE_ReplayGain :		BoolSelectColReplayGain	= TRUE;	break;
			case NUM_TREE_FILE_Name :		BoolSelectColNom	= TRUE;	break;
			default:
				StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
				StatusBar_puts();
				return (FALSE);
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}

	BoolSelectColNorm &= (detail->type_infosong_file_is == FILE_IS_WAV ||
			      detail->type_infosong_file_is == FILE_IS_OGG ||
			      detail->type_infosong_file_is == FILE_IS_MP3);

	if (BoolSelectColFlac || BoolSelectColWav || BoolSelectColMp3 || BoolSelectColOgg || BoolSelectColM4a ||
	    BoolSelectColAac || BoolSelectColMpc || BoolSelectColApe || BoolSelectColWavP) {
		if ((Pos_X < 18 || Pos_X > 30) || (Pos_Y < 6 || Pos_Y > 18)) {
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
			return (FALSE);
		}
	}

	if (TRUE == BoolSelectColPlay) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
	}
	else if (TRUE == BoolSelectColTrash) {
		switch (detail->EtatTrash) {
		case FILE_TRASH_NONE :
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Click Droit = Menu) / Click here to send the processed file to the trash"));
			break;
		case FILE_TRASH_OK :
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right Click = Menu) / This file will be sent to the trash after processing"));
			break;
		case FILE_TRASH_VERIF_OK :
			StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
			break;
		}
	}
	else if (TRUE == BoolSelectColType) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
	}
	else if (TRUE == BoolSelectColWav) {
		file_set_help (detail, FILE_IS_WAV);
	}
	else if (TRUE == BoolSelectColFlac) {
		file_set_help (detail, FILE_IS_FLAC);
	}
	else if (TRUE == BoolSelectColApe) {
		file_set_help (detail, FILE_IS_APE);
	}
	else if (TRUE == BoolSelectColWavP) {
		file_set_help (detail, FILE_IS_WAVPACK);
	}
	else if (TRUE == BoolSelectColOgg) {
		file_set_help (detail, FILE_IS_OGG);
	}
	else if (TRUE == BoolSelectColM4a) {
		file_set_help (detail, FILE_IS_M4A);
	}
	else if (TRUE == BoolSelectColAac) {
		file_set_help (detail, FILE_IS_AAC);
	}
	else if (TRUE == BoolSelectColMpc) {
		file_set_help (detail, FILE_IS_MPC);
	}
	else if (TRUE == BoolSelectColMp3) {
		file_set_help (detail, FILE_IS_MP3);
	}
	else if (TRUE == BoolSelectColTime) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
	}
	else if (TRUE == BoolSelectColNorm) {
		file_set_normalise (detail->Etat_Normalise);
	}
	else if (TRUE == BoolSelectColReplayGain) {
		file_set_replaygain (detail->Etat_ReplayGain);
	}
	else if (TRUE == BoolSelectColNom) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, detail->namefile);
	}
	else {
		// StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");
	}

	StatusBar_puts();

	return (FALSE);
}
//
//
static void file_add_columns_scrolledwindow (GtkTreeView *treeview)
{
	GtkTreeModel      *model = gtk_tree_view_get_model (treeview);
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;

	// SIGNAL : 'event'
	g_signal_connect(G_OBJECT(treeview),
			 "event",
                    	 (GCallback) file_event,
			 model);

	// SIGNAL 'key-press-event'
	g_signal_connect(G_OBJECT(treeview),
			"key-press-event",
			(GCallback) file_key_press_event,
			model);

	// SIGNAL : 'Gestion click click'
	g_signal_connect(G_OBJECT(treeview),
			 "button-press-event",
                    	 (GCallback) file_event_click_mouse,
			 model);

	// SIGNAL : Ligne actuellement selectionnee 'changed'
	var_file.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	g_signal_connect(G_OBJECT(var_file.Adr_Line_Selected),
			 "changed",
                   	 G_CALLBACK(file_changed_selection_row),
                   	 "1");

	// Drag and drop support
	// SIGNAL : 'drag-data-received'
	gtk_drag_dest_set (GTK_WIDGET (treeview),
			   // GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP | GTK_DEST_DEFAULT_HIGHLIGHT,
			   GTK_DEST_DEFAULT_ALL,
			   drag_types,
			   n_drag_types,
			   GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_DEFAULT
			   );
	g_signal_connect(G_OBJECT(treeview),
			 "drag-data-received",
			 G_CALLBACK(file_drag_data_received),
			 NULL);

	gtk_drag_source_set(
			GTK_WIDGET(treeview),
			GDK_BUTTON1_MASK | GDK_BUTTON2_MASK | GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP,
			drag_types,
			n_drag_types,
			GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_DEFAULT
			);

	g_signal_connect(G_OBJECT(treeview),
      			"drag-data-get",
			 G_CALLBACK(file_drag_data_drop),
			 NULL);

	// COLUMN_FILE_PLAY
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFilePlay =
	column = gtk_tree_view_column_new_with_attributes( "Play",
						     renderer,
						     "pixbuf", COLUMN_FILE_PLAY,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column), GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);

	// COLUMN_FILE_TRASH
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileTrash =
	column = gtk_tree_view_column_new_with_attributes (_("Trash"),
						     renderer,
						     "pixbuf", COLUMN_FILE_TRASH,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(-1));


	// COLUMN_FILE_TYPE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileType =
	column = gtk_tree_view_column_new_with_attributes (_("Type"),
						     renderer,
						     "markup", COLUMN_FILE_TYPE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 55);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	// TRIS
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_FILE_TYPE);
	gtk_tree_view_append_column (treeview, column);

	/*
	@Patachon, @Shankarius, @Dzef

	Je plaide aussi pour la réunion logique des formats lossless puis des autres. Je propose :

	    lossless     |       perte      | destruction forte
	<================>
	                 <============================>
					    <=========>
	Wav | Flac | Ape | WPack | Ogg | Aac | Mpc | Mp3

	Je conviens qu'il n'est pas facile de classer, car certains formats sont
	capables d'être utilisés en lossless ou en moindre qualité. Ce n'est
	donc pas vraiment linéaire. Mais c'est une piste...
	*/

	// COLUMN_FILE_WAV
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileWav =
	column = gtk_tree_view_column_new_with_attributes ("Wav",
						     renderer,
						     "pixbuf", COLUMN_FILE_WAV,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(0));

	// COLUMN_FILE_FLAC
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileFlac =
	column = gtk_tree_view_column_new_with_attributes ("Flac",
						     renderer,
						     "pixbuf", COLUMN_FILE_FLAC,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(1));

	// COLUMN_FILE_APE
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileApe =
	column = gtk_tree_view_column_new_with_attributes ("Ape",
						     renderer,
						     "pixbuf", COLUMN_FILE_APE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(2));

	// COLUMN_FILE_WAVPACK
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileWavPack =
	column = gtk_tree_view_column_new_with_attributes ("WavP",
						     renderer,
						     "pixbuf", COLUMN_FILE_WAVPACK,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(3));

	// COLUMN_FILE_OGG
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileOgg =
	column = gtk_tree_view_column_new_with_attributes ("Ogg",
						     renderer,
						     "pixbuf", COLUMN_FILE_OGG,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(4));

	// COLUMN_FILE_M4A
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileM4a =
	column = gtk_tree_view_column_new_with_attributes ("M4a",
						     renderer,
						     "pixbuf", COLUMN_FILE_M4A,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(5));

	// COLUMN_FILE_AAC
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileAac =
	column = gtk_tree_view_column_new_with_attributes ("Aac",
						     renderer,
						     "pixbuf", COLUMN_FILE_AAC,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(6));

	// COLUMN_FILE_MPC
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileMpc =
	column = gtk_tree_view_column_new_with_attributes ("Mpc",
						     renderer,
						     "pixbuf", COLUMN_FILE_MPC,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(7));

	// COLUMN_FILE_MP3
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileMp3 =
	column = gtk_tree_view_column_new_with_attributes ("Mp3",
						     renderer,
						     "pixbuf", COLUMN_FILE_MP3,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(8));

	// COLUMN_FILE_TIME
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileTime =
	column = gtk_tree_view_column_new_with_attributes (_("Time"),
						     renderer,
						     /*"pixbuf", COLUMN_FILE_TYPE,*/
						     "text", COLUMN_FILE_TIME,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 70);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);

	// COLUMN_FILE_NORMALIZE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer,
			"xalign", 0.5,
			NULL);
	var_file.Adr_ColumnFileNormalize =
	column = gtk_tree_view_column_new_with_attributes (_("Normalise"),
						     renderer,
						     "markup", COLUMN_FILE_NORMALIZE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 170);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(9));

	// COLUMN_FILE_REPLAYGAIN
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file.Adr_ColumnFileReplayGain =
	column = gtk_tree_view_column_new_with_attributes( "ReplayGain",
						     renderer,
						     "pixbuf", COLUMN_FILE_REPLAYGAIN,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 80);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	g_signal_connect (G_OBJECT(GTK_TREE_VIEW_COLUMN (column)),
			"clicked",
			G_CALLBACK (file_selected_column),
			GINT_TO_POINTER(10));

	// COLUMN_FILE_NAME
	// COLUMN_FILE_EDITABLE
	var_file.Renderer =
	renderer = gtk_cell_renderer_text_new ();
	g_signal_connect (renderer, "edited", G_CALLBACK (file_cell_edited), model);
	g_object_set (renderer, "xalign", 0.5, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint *)COLUMN_FILE_NAME);
	var_file.Adr_ColumnFileName =
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
							renderer,
							"markup", COLUMN_FILE_NAME,
							"editable", COLUMN_FILE_EDITABLE,
						    //    "background-gdk", COLUMN_FILE_COLOR,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 200);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	// TRIS
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_FILE_NAME);
	gtk_tree_view_append_column (treeview, column);
}
//
//
void on_file_scrolledwindow_file_realize (GtkWidget *widget, gpointer user_data)
{
	GtkListStore *store;
	GtkTreeModel *model;
	GtkWidget    *treeview;

	var_file.Adr_scroll = widget;

	var_file.Pixbuf_Coche			= libutils_init_pixbufs ("coche.png");
	var_file.Pixbuf_Selected		= libutils_init_pixbufs ("selected.png");
	var_file.Pixbuf_Coche_exist		= libutils_init_pixbufs ("coche_exist.png");
	var_file.Pixbuf_Selected_exist		= libutils_init_pixbufs ("selected_exist.png");
	var_file.Pixbuf_Selected_expert		= libutils_init_pixbufs ("selected_expert.png");
	var_file.Pixbuf_Selected_expert_exist	= libutils_init_pixbufs ("selected_expert_exist.png");
	var_file.Pixbuf_FilePlay		= libutils_init_pixbufs ("sol.png");
	var_file.Pixbuf_FileStop		= libutils_init_pixbufs ("no_play.png");
	var_file.Pixbuf_NotInstall		= libutils_init_pixbufs ("not_install.png");
	var_file.Pixbuf_rpg_piste		= libutils_init_pixbufs ("rpg_piste.png");
	var_file.Pixbuf_rpg_album		= libutils_init_pixbufs ("rpg_album.png");
	var_file.Pixbuf_rpg_effacer		= libutils_init_pixbufs ("rpg_effacer.png");
	var_file.Pixbuf_rpg_wait		= libutils_init_pixbufs ("norm_rpg_wait.png");
	var_file.Pixbuf_norm_fix		= libutils_init_pixbufs ("norm_fix.png");
	var_file.Pixbuf_norm_mix		= libutils_init_pixbufs ("norm_mix.png");
	var_file.Pixbuf_norm_peak		= libutils_init_pixbufs ("norm_peak.png");
	var_file.Pixbuf_norm_wait		= libutils_init_pixbufs ("norm_rpg_wait.png");
	var_file.Pixbuf_Normalize_Coche		= libutils_init_pixbufs ("normalize2.png");
	var_file.Pixbuf_NoTrash			= libutils_init_pixbufs ("coche.png");
	var_file.Pixbuf_Trash			= libutils_init_pixbufs ("trash.png");

	var_file.Adr_List_Store = store =
	gtk_list_store_new (	COLUMN_FILE_NUM,	/* TOTAL NUMBER			*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_PLAY		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_TRASH		*/
				G_TYPE_STRING,	        /* COLUMN_FILE_TYPE		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_FLAC		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_WAV		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_MP3		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_OGG		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_M4A		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_AAC		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_MPC		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_APE		*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_WAVPACK		*/
				G_TYPE_STRING,		/* COLUMN_FILE_TIME		*/
				G_TYPE_STRING,		/* COLUMN_FILE_NORMALIZE	*/
				GDK_TYPE_PIXBUF,	/* COLUMN_FILE_REPLAYGAIN	*/
				G_TYPE_STRING,		/* COLUMN_FILE_NAME		*/
				G_TYPE_BOOLEAN,         /* COLUMN_FILE_EDITABLE		*/
				//GDK_TYPE_COLOR,		/* COLUMN_FILE_COLOR		*/
				G_TYPE_POINTER          /* COLUMN_FILE_POINTER_STRUCT	*/
			   );
	var_file.Adr_Tree_Model = model = GTK_TREE_MODEL (store);
	var_file.Adr_TreeView =
	treeview = gtk_tree_view_new_with_model (model);
	// gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview)), GTK_SELECTION_MULTIPLE);	// GTK_SELECTION_BROWSE MULTIPLE
	g_object_unref (model);
	gtk_container_add (GTK_CONTAINER (widget), treeview);
	file_add_columns_scrolledwindow (GTK_TREE_VIEW (treeview));
	gtk_widget_show_all (widget);
}
//
//
void file_from_popup_select_verticaly (ETAT_SELECTION EtatSelection, TYPE_FILE_IS type_file_is)
{
	DETAIL        *detail = NULL;
	GtkTreeIter    iter;
	gboolean       valid;
	GdkPixbuf     *Pixbuf = NULL;

	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if (NULL != detail) {

			if (type_file_is == FILE_IS_FLAC) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Flac = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Flac = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Flac = ETAT_SELECT_EXIST;
				detail->EtatSelection_Flac = file_get_next_flag (detail, FILE_IS_FLAC);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_FLAC);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_FLAC, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_WAV) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Wav = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Wav = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Wav = ETAT_SELECT_EXIST;
				detail->EtatSelection_Wav = file_get_next_flag (detail, FILE_IS_WAV);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_WAV);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_WAV, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_MP3) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Mp3 = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Mp3 = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Mp3 = ETAT_SELECT_EXIST;
				detail->EtatSelection_Mp3 = file_get_next_flag (detail, FILE_IS_MP3);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_MP3);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_MP3, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_OGG) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Ogg = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Ogg = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Ogg = ETAT_SELECT_EXIST;
				detail->EtatSelection_Ogg = file_get_next_flag (detail, FILE_IS_OGG);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_OGG);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_OGG, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_M4A) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_M4a = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_M4a = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_M4a = ETAT_SELECT_EXIST;
				detail->EtatSelection_M4a = file_get_next_flag (detail, FILE_IS_M4A);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_M4A);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_M4A, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_AAC) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Aac = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Aac = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Aac = ETAT_SELECT_EXIST;
				detail->EtatSelection_Aac = file_get_next_flag (detail, FILE_IS_AAC);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_AAC);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_AAC, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_MPC) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Mpc = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Mpc = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Mpc = ETAT_SELECT_EXIST;
				detail->EtatSelection_Mpc = file_get_next_flag (detail, FILE_IS_MPC);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_MPC);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_MPC, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_APE) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Ape = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Ape = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Ape = ETAT_SELECT_EXIST;
				detail->EtatSelection_Ape = file_get_next_flag (detail, FILE_IS_APE);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_APE);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_APE, Pixbuf, -1);
			}
			else if (type_file_is == FILE_IS_WAVPACK) {
				if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_WavPack = ETAT_PRG_NONE;
				else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_WavPack = ETAT_ATTENTE_EXIST;
				else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_WavPack = ETAT_SELECT_EXIST;
				detail->EtatSelection_WavPack = file_get_next_flag (detail, FILE_IS_WAVPACK);
				Pixbuf = file_get_pixbuf (detail, FILE_IS_WAVPACK);
				gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_WAVPACK, Pixbuf, -1);
			}
		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}
	file_set_flag_buttons ();
}
//
//
void file_from_popup_select_horizontaly (DETAIL *p_detail, ETAT_SELECTION EtatSelection)
{
	DETAIL        *detail = NULL;
	GtkTreeIter    iter;
	gboolean       valid;
	GdkPixbuf     *Pixbuf = NULL;

	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if (NULL != detail && p_detail == detail) {

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Wav = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Wav = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Wav = ETAT_SELECT_EXIST;

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Flac = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Flac = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Flac = ETAT_SELECT_EXIST;

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Ape = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Ape = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Ape = ETAT_SELECT_EXIST;

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_WavPack = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_WavPack = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_WavPack = ETAT_SELECT_EXIST;

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Ogg = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Ogg = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Ogg = ETAT_SELECT_EXIST;

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_M4a = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_M4a = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_M4a = ETAT_SELECT_EXIST;

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Aac = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Aac = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Aac = ETAT_SELECT_EXIST;

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Mpc = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Mpc = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Mpc = ETAT_SELECT_EXIST;

			if (EtatSelection == ETAT_PRG_NONE)		detail->EtatSelection_Mp3 = ETAT_PRG_NONE;
			else if (EtatSelection == ETAT_SELECT)		detail->EtatSelection_Mp3 = ETAT_ATTENTE_EXIST;
			else if (EtatSelection == ETAT_SELECT_EXPERT)	detail->EtatSelection_Mp3 = ETAT_SELECT_EXIST;

			detail->EtatSelection_Flac    = file_get_next_flag (detail, FILE_IS_FLAC);
			detail->EtatSelection_Wav     = file_get_next_flag (detail, FILE_IS_WAV);
			detail->EtatSelection_Mp3     = file_get_next_flag (detail, FILE_IS_MP3);
			detail->EtatSelection_Ogg     = file_get_next_flag (detail, FILE_IS_OGG);
			detail->EtatSelection_M4a     = file_get_next_flag (detail, FILE_IS_M4A);
			detail->EtatSelection_Aac     = file_get_next_flag (detail, FILE_IS_AAC);
			detail->EtatSelection_Mpc     = file_get_next_flag (detail, FILE_IS_MPC);
			detail->EtatSelection_Ape     = file_get_next_flag (detail, FILE_IS_APE);
			detail->EtatSelection_WavPack = file_get_next_flag (detail, FILE_IS_WAVPACK);

			Pixbuf = file_get_pixbuf (detail, FILE_IS_FLAC);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_FLAC,
							Pixbuf,
							-1);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_WAV);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_WAV,
							Pixbuf,
							-1);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_MP3);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_MP3,
							Pixbuf,
							-1);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_OGG);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_OGG,
							Pixbuf,
							-1);

			Pixbuf = file_get_pixbuf (detail, FILE_IS_M4A);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_M4A,
							Pixbuf,
							-1);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_AAC);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_AAC,
							Pixbuf,
							-1);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_MPC);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_MPC,
							Pixbuf,
							-1);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_APE);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_APE,
							Pixbuf,
							-1);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_WAVPACK);
			gtk_list_store_set (var_file.Adr_List_Store,
							&iter,
							COLUMN_FILE_WAVPACK,
							Pixbuf,
							-1);

			file_set_flag_buttons ();
			return;
		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}
}
//
//
void file_from_popup_clear (void)
{
	DETAIL        *detail = NULL;
	GtkTreeIter    iter;
	gboolean       valid;
	GdkPixbuf     *Pixbuf = NULL;

	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if (NULL != detail) {

			detail->EtatSelection_Wav = ETAT_SELECT_EXPERT;
			detail->EtatSelection_Wav = file_get_next_flag (detail, FILE_IS_WAV);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_WAV);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_WAV, Pixbuf, -1);

			detail->EtatSelection_Flac = ETAT_SELECT_EXPERT;
			detail->EtatSelection_Flac = file_get_next_flag (detail, FILE_IS_FLAC);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_FLAC);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_FLAC, Pixbuf, -1);

			detail->EtatSelection_Ape = ETAT_SELECT_EXPERT;
			detail->EtatSelection_Ape = file_get_next_flag (detail, FILE_IS_APE);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_APE);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_APE, Pixbuf, -1);

			detail->EtatSelection_WavPack = ETAT_SELECT_EXPERT;
			detail->EtatSelection_WavPack = file_get_next_flag (detail, FILE_IS_WAVPACK);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_WAVPACK);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_WAVPACK, Pixbuf, -1);

			detail->EtatSelection_Ogg = ETAT_SELECT_EXPERT;
			detail->EtatSelection_Ogg = file_get_next_flag (detail, FILE_IS_OGG);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_OGG);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_OGG, Pixbuf, -1);

			detail->EtatSelection_M4a = ETAT_SELECT_EXPERT;
			detail->EtatSelection_M4a = file_get_next_flag (detail, FILE_IS_M4A);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_M4A);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_M4A, Pixbuf, -1);

			detail->EtatSelection_Aac = ETAT_SELECT_EXPERT;
			detail->EtatSelection_Aac = file_get_next_flag (detail, FILE_IS_AAC);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_AAC);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_AAC, Pixbuf, -1);

			detail->EtatSelection_Mpc = ETAT_SELECT_EXPERT;
			detail->EtatSelection_Mpc = file_get_next_flag (detail, FILE_IS_MPC);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_MPC);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_MPC, Pixbuf, -1);

			detail->EtatSelection_Mp3 = ETAT_SELECT_EXPERT;
			detail->EtatSelection_Mp3 = file_get_next_flag (detail, FILE_IS_MP3);
			Pixbuf = file_get_pixbuf (detail, FILE_IS_MP3);
			gtk_list_store_set (var_file.Adr_List_Store, &iter, COLUMN_FILE_MP3, Pixbuf, -1);
		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}
	file_set_flag_buttons ();
}
//
//
void file_from_popup_replaygain (ETAT_REPLAYGAIN EtatReplayGain)
{
	GtkTreeIter    iter;
	gboolean       valid;
	DETAIL        *detail = NULL;

	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if (NULL != detail) {
			if (detail->type_infosong_file_is == FILE_IS_FLAC ||
			    detail->type_infosong_file_is == FILE_IS_MP3 ||
			    detail->type_infosong_file_is == FILE_IS_OGG ||
			    detail->type_infosong_file_is == FILE_IS_WAVPACK) {

				detail->Etat_ReplayGain = EtatReplayGain;

				gtk_list_store_set (
						var_file.Adr_List_Store,
						&iter,
						COLUMN_FILE_REPLAYGAIN,
						file_get_pixbuf_replaygain (detail, FALSE),
						-1);
			}
		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}
	file_set_flag_buttons ();
}
//
//
void file_from_popup_trash (gboolean BoolSelect)
{
	GtkTreeIter	iter;
	gboolean	valid;
	DETAIL		*detail = NULL;

	valid = gtk_tree_model_get_iter_first (var_file.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file.Adr_Tree_Model, &iter, COLUMN_FILE_POINTER_STRUCT, &detail, -1);
		if (NULL != detail) {

			if (BoolSelect == TRUE)
				detail->EtatTrash = FILE_TRASH_OK;
			else	detail->EtatTrash = FILE_TRASH_NONE;

			gtk_list_store_set (
					var_file.Adr_List_Store,
					&iter,
					COLUMN_FILE_TRASH,
					file_get_pixbuf_trash (detail),
					-1);

		}
		valid = gtk_tree_model_iter_next (var_file.Adr_Tree_Model, &iter);
	}
	file_set_flag_buttons ();
}
//
//
void file_from_popup (TYPE_SET_FROM_POPUP_FILE TypeSetFromPopup, DETAIL *detail, TYPE_FILE_IS TypeFileIs)
{
	switch (TypeSetFromPopup) {

	// SELECTION ou DESELECTION POUR LES ICONES DE CONVERSIONS

	case FILE_CONV_DESELECT_ALL :			// Deselection globale
		file_from_popup_clear ();
		break;
	case FILE_CONV_DESELECT_V :			// Deselection verticale
		file_from_popup_select_verticaly (ETAT_PRG_NONE, TypeFileIs);
		break;
	case FILE_CONV_DESELECT_H :			// Deselection horizontale
		file_from_popup_select_horizontaly (detail, ETAT_PRG_NONE);
		break;
	case FILE_CONV_SELECT_V :			// Selection verticale
		file_from_popup_select_verticaly (ETAT_SELECT, TypeFileIs);
		break;
	case FILE_CONV_SELECT_EXPERT_V :		// Selection Expert verticale
		file_from_popup_select_verticaly (ETAT_SELECT_EXPERT, TypeFileIs);
		break;
	case FILE_CONV_SELECT_H :			// Selection horizontale
		file_from_popup_select_horizontaly (detail, ETAT_SELECT);
		break;
	case FILE_CONV_SELECT_EXPERT_H :		// Selection Expert horizontale
		file_from_popup_select_horizontaly (detail, ETAT_SELECT_EXPERT);
		break;

	// SELECTION ou DESELECTION POUR LES ICONES DE REPLAYGAIN

	case FILE_REPLAYGAIN_DESELECT_V :		// Deselection verticale
		file_from_popup_replaygain (RPG_ATTENTE);
		break;
	case FILE_REPLAYGAIN_SELECT_PISTE :		// Selection PISTE
		file_from_popup_replaygain (RPG_PISTE);
		break;
	case FILE_REPLAYGAIN_SELECT_ALBUM :		// Selection ALBUM
		file_from_popup_replaygain (RPG_ALBUM);
		break;
	case FILE_REPLAYGAIN_SELECT_NETTOYER :		// Selection NETTOYER
		file_from_popup_replaygain (RPG_EFFACER);
		break;

	// SELECTION ou DESELECTION POUR TRASH

	case FILE_TRASH_DESELECT_V :			// Deselection verticale
		file_from_popup_trash (FALSE);
		break;
	case FILE_TRASH_SELECT_V :			// Selection verticale
		file_from_popup_trash (TRUE);
		break;

	// SELECTION ou DESELECTION POUR FREQUENCES VOIE BITS

	case FILEWAV_FREQUENCY_CELL_HERTZ :
	case FILEWAV_TRACK_CELL :
	case FILEWAV_QUANTIFICATION_CELL :
		break;
	}
}






