 /*
 *  file      : split_cue.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include "conv.h"
#include "configuser.h"
#include "win_scan.h"
#include "win_info.h"
#include "prg_init.h"
#include "split.h"




extern VAR_SPLIT VarSplit;


//
//
//
//
float SplitCue_get_time_in_sec (gchar *p_str)
{
	gchar		*Ptr = NULL;
	guint		 Min, Sec, Hundr;
	float		 SecondesTempsActuel = 0;

	// MINUTES
	if ((Ptr = strchr (p_str, ':')) != NULL) {
		while (*Ptr != ' ') Ptr --;
		if (*Ptr == ' ') Ptr ++;
		Min = atoi (Ptr);

		// SECONDES
		if ((Ptr = strchr (p_str, ':')) != NULL) {
			Ptr ++;
			Sec = atoi (Ptr);

			// CENTIEMES
			if ((Ptr = strrchr (p_str, ':')) != NULL) {
				Ptr ++;
				Hundr = atoi (Ptr);

				// Temps Total en secondes
				SecondesTempsActuel = ((float)Min * 60.0) + (float)Sec + ((float)Hundr / 100.0);
			}
		}
	}

	return (SecondesTempsActuel);
}
//
// TEST LA COHERENCE DU FICHIER CUE
// LECTURE DES INFOS DU FICHIER
// RETOURNE LE NOM DU FICHIER DE MUSIQUE ASSOCIE
//
gchar *SplitCue_read_cue_file (gchar *p_pathname)
{
	FILE		*fp;
#define MAX_CARS_SPLIT_CUE_FILE 255
	gchar		buf   [ MAX_CARS_SPLIT_CUE_FILE + 4 ];
	gchar		*PathNameSong = NULL;
	gchar		*StrNameSong = NULL;
	gchar		*Ptr = NULL;
	gchar		*Path = NULL;
	gint		IndicePoints = -1;
	gint		FileTimeSec;
	gboolean	BoolResultOk;

	//
	// TEST LA COHERENCE DU FICHIER CUE
	//
	if (NULL == (fp = fopen (p_pathname, "r"))) {
		wind_info_init (
			WindMain,
			_("Misreading !"),
			  "");
		return (NULL);
	}

	// PERFORMER "Rita Mitsouko / Rita Mitsouko"
	// TITLE "Rita Mitsouko / Rita Mitsouko"
	// FILE "XCFA_CUE.wav" WAVE
	//
	// PERFORMER "José de Divina"
	// TITLE "Ibiza World Tour @ Space"
	// FILE "Ibiza1.mp3" WAVE
	//
	// REM GENRE Jazz
	// REM DATE 1966
	// REM DISCID 120F4902
	// REM COMMENT "ExactAudioCopy v0.95b4"
	// PERFORMER "John Coltrane"
	// TITLE "Live in Japan (CD1)"
	// FILE "un.flac" WAVE

	// REM Cue file written by K3b 1.0.5
	// PERFORMER "Rita Mitsouko"
	// TITLE "Rita Mitsouko"
	// FILE "01 - Restez Avec Moi.wav" WAVE

	while (fgets (buf, MAX_CARS_SPLIT_CUE_FILE, fp) != NULL) {

		// Si debut de ligne ok
		if (NULL != (Ptr = strstr (buf, "FILE \""))) {

			// StrNameSong contiendra le nom du fichier delimite par les signes "
			Ptr = strchr (buf, '"');
			Ptr ++;
			StrNameSong = g_strdup (Ptr);
			if ((Ptr = strrchr (StrNameSong, '"')) == NULL) {
				fclose (fp);
				g_free (StrNameSong);	StrNameSong = NULL;
				return (NULL);
			}
			*Ptr = '\0';

			// Path contiendra le chemin
			Path = g_strdup (p_pathname);
			if ((Ptr = strrchr (Path, '/')) != NULL) {
				*Ptr = '\0';
			}

			// PathNameSong contiendra: chemin + nom du fichier musical
			PathNameSong = g_strdup_printf ("%s/%s", Path, StrNameSong);
			g_free (Path);		Path = NULL;
			g_free (StrNameSong);	StrNameSong = NULL;
			break;
		}
	}
	fclose (fp);

	// LES TESTS
	if (NULL == PathNameSong) {
		wind_info_init (
			WindMain,
			_("Error"),
			_("No music file associated with the cue-file"),
			  "");
		return (NULL);
	}
	if (FALSE == FileIs_g_str_has_suffix (PathNameSong, ".WAV")) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("====\n%s\n----\n%s\t%s\n\t\tFILE NOT IN: .wav\n====\n", p_pathname, buf, PathNameSong);
		wind_info_init (
			WindMain,
			_("Error"),
			_("No WAV file in the cue-file"),
			  "");
		g_free (PathNameSong);	PathNameSong = NULL;
		return (NULL);
	}
	if (FALSE == libutils_test_file_exist (PathNameSong)) {
		wind_info_init (
			WindMain,
			_("Error"),
			_("No music file in the folder associated"),
			  "\n",
			_("of cue-file"),
			  "");
		return (NULL);
	}

	//
	// LECTURE DES INFOS DU FICHIER
	//
	FileTimeSec = tagswav_get_time_sec (PathNameSong);

	if( NULL != ( fp = fopen( p_pathname, "r" )) ) {

		while (fgets (buf, MAX_CARS_SPLIT_CUE_FILE, fp) != NULL) {

			if (strstr (buf, "TRACK ")) {
				// g_print("TRACK ->%s", buf);
			}

			// DEBUT

			else if (strstr (buf, "INDEX 01")) {

				IndicePoints ++;

				VarSplit.Selecteur [ IndicePoints ] . Nmr             = IndicePoints;
				VarSplit.Selecteur [ IndicePoints ] . BeginPaint      = SplitCue_get_time_in_sec (buf);
				VarSplit.Selecteur [ IndicePoints ] . PercentBegin    = ((gdouble)VarSplit.Selecteur [ IndicePoints ] . BeginPaint / (gdouble)FileTimeSec) * 100.0;
			}

			// FIN

			else if (strstr (buf, "INDEX 00")) {

				// SOLVED Sun, 03 Feb 2013 22:17:05 +0100
				if( -1 == IndicePoints ) IndicePoints = 0;

				VarSplit.Selecteur [ IndicePoints ] . Nmr             = IndicePoints;
				VarSplit.Selecteur [ IndicePoints ] . EndPaint        = SplitCue_get_time_in_sec (buf);
				VarSplit.Selecteur [ IndicePoints ] . PercentEnd      = ((gdouble)VarSplit.Selecteur [ IndicePoints ] . EndPaint / (gdouble)FileTimeSec) * 100.0;
			}

			else if (strstr (buf, "TITLE ")) {
				// g_print("TITLE ->%s", buf);
			}

		}
		fclose (fp);
	}

	// AJUSTER LE DEBUT
	VarSplit.Selecteur [ 0 ] . BeginPaint         = 1;

	// AJUSTER LA FIN
	VarSplit.Selecteur [ IndicePoints ] . PercentEnd    = 99.99;
	VarSplit.Selecteur [ IndicePoints ] . EndPaint      = FileTimeSec;

	// TESTER LA COHERENCE DES DATAS
	BoolResultOk = TRUE;
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\n");
	for (IndicePoints = 0; VarSplit.Selecteur [ IndicePoints ] . Nmr != -1; IndicePoints ++) {
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("[ %2d ]   TIME = %05d  BEGIN = %5d  END = %5d  PERCENTBEGIN = %f  PERCENTEND = %f\n",
				VarSplit.Selecteur [ IndicePoints ] . Nmr,
				FileTimeSec,
				(gint)VarSplit.Selecteur [ IndicePoints ] . BeginPaint,
				(gint)VarSplit.Selecteur [ IndicePoints ] . EndPaint,
				VarSplit.Selecteur [ IndicePoints ] . PercentBegin,
				VarSplit.Selecteur [ IndicePoints ] . PercentEnd
				);
		if ((gint)VarSplit.Selecteur [ IndicePoints ] . BeginPaint > (gint)VarSplit.Selecteur [ IndicePoints ] . EndPaint) {
			BoolResultOk = FALSE;
			if( TRUE == OptionsCommandLine.BoolVerboseMode )
				g_print ("\t--- ERREUR [ 1 ] DE DATAS DANS LE FICHIER CUE !!! ---\n");
		}
		if (VarSplit.Selecteur [ IndicePoints +1 ] . Nmr != -1) {
			if ((gint)VarSplit.Selecteur [ IndicePoints ] . EndPaint > (gint)VarSplit.Selecteur [ IndicePoints +1 ] . BeginPaint) {
				BoolResultOk = FALSE;
				if( TRUE == OptionsCommandLine.BoolVerboseMode )
					g_print ("\t--- [ 2 ] ERREUR DE DATAS DANS LE FICHIER CUE !!! ---\n");
			}
		}
	}
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\n");

	// ERREUR DE DATAS DANS LE FICHIER CUE !!!
	if (FALSE == BoolResultOk) {
		if (NULL != PathNameSong) {
			g_free (PathNameSong);
			PathNameSong = NULL;

			wind_info_init (
				WindMain,
				_("Data errors in the CUE file"),
				_("The solution is to import the file"),
				  "\n",
				_("music that will be recognized and pre-cutting"),
				  "\n",
				_("in the split module."),
				  "\n\n",
				_("We will just adjust to the beaches"),
				  "\n",
				_("generate a new cue-file or cuts"),
				  "\n",
				_("corresponding to the beaches."),
				  "");
		}
	}

	//
	// RETOURNE LE NOM DU FICHIER DE MUSIQUE ASSOCIE
	//
	return ((gchar *)PathNameSong);
}
//
//
//
void on_button_gen_cue_split_clicked (GtkButton *button, gpointer user_data)
{
	gint		IndicePoints;
	gdouble		Percent;
	// gint		H;
	gint		M, S, C;
	gint		hundr;
	gint		sec;
	gdouble		dsec;
	gdouble		TimeSongSec;
	gchar		*Ptr = NULL;
	gchar		*PathNameFileCue = NULL;
	gchar		*NameFileWav = NULL;
	gchar		*PathFileCue = NULL;
	gchar		*NameFileCue = NULL;
	gint		NumFileCue;
	FILE		*fp;

	PRINT_FUNC_LF();
	TimeSongSec = VarSplit.Tags->SecTime;

	for (IndicePoints = 0; VarSplit.Selecteur [ IndicePoints ] . Nmr != -1; IndicePoints ++) {

		Percent = VarSplit.Selecteur [ IndicePoints ] . PercentBegin;
		sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
		dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
		hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
		// H = (sec / 60) / 60;
		M = (sec / 60) % 60;
		S = sec % 60;
		C = hundr;

		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print ("[ %02d ] ", IndicePoints);
			g_print ("%02d:%02d:%02d   ", M, S, C);
		}

		Percent = VarSplit.Selecteur [ IndicePoints ] . PercentEnd;
		sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
		dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
		hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
		// H = (sec / 60) / 60;
		M = (sec / 60) % 60;
		S = sec % 60;
		C = hundr;

		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print ("%02d:%02d:%02d", M, S, C);
			g_print ("\n");
		}
	}

	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\n");

	// NOM DU CUE
	/*PathNameFileCue = g_strdup (VarSplit.PathNameFile);
	Ptr = strrchr (PathNameFileCue, '.');
	Ptr ++;
	*Ptr ++ = 'c';
	*Ptr ++ = 'u';
	*Ptr ++ = 'e';
	*Ptr ++ = '\0';*/
	Ptr = strrchr (VarSplit.PathNameFileReal, '/');
	PathNameFileCue = g_strdup_printf ("%s%s", Config.PathDestinationSplit, Ptr);
	Ptr = strrchr (PathNameFileCue, '.');
	Ptr ++;
	*Ptr ++ = 'c';
	*Ptr ++ = 'u';
	*Ptr ++ = 'e';
	*Ptr ++ = '\0';

	// NOM DU WAV
	Ptr = strrchr (VarSplit.PathNameFile, '/');
	Ptr ++;
	NameFileWav = g_strdup (Ptr);

	// DEBUG
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		g_print ("PathNameFileCue = %s\n", PathNameFileCue);
		g_print ("NameFileWav     = %s\n", NameFileWav);
	}
	// CHANGER LE NOM DU FICHIER CUE SI IL EXIST
	if (TRUE == libutils_test_file_exist (PathNameFileCue)) {
		PRINT("PathNameFileCue  EXIST");

		// PATH CUE FILE
		PathFileCue = g_strdup (PathNameFileCue);
		Ptr = strrchr (PathFileCue, '/');
		Ptr ++;
		*Ptr = '\0';

		// NAME CUE FILE
		Ptr = strrchr (PathNameFileCue, '/');
		Ptr ++;
		NameFileCue = g_strdup (Ptr);
		NumFileCue = 1;
		while (TRUE == libutils_test_file_exist (PathNameFileCue)) {
			g_free (PathNameFileCue);	PathNameFileCue = NULL;
			PathNameFileCue = g_strdup_printf ("%s%02d_%s", PathFileCue, NumFileCue, NameFileCue);
			NumFileCue ++;
		}
		g_free (PathFileCue);		PathFileCue = NULL;
		g_free (NameFileCue);		NameFileCue = NULL;
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("NOUVEAU PathNameFileCue = %s\n", PathNameFileCue);
	}

	fp = fopen (PathNameFileCue, "w");

	// PRODUCTION DU FICHIER CUE
	if( TRUE == OptionsCommandLine.BoolVerboseMode )
		g_print ("\n");
	fprintf (fp,   "REM Cue file written by Xcfa\n\n");
	fprintf (fp,   "PERFORMER \"%s\"\n", "CHANTEUR");
	fprintf (fp,   "TITLE \"%s\"\n", "TITRE DISQUE");
	fprintf (fp,   "FILE \"%s\" WAVE\n", NameFileWav);

	// DUREE TOTALE EXPRIMEE EN SECONDES
	TimeSongSec = VarSplit.Tags->SecTime;

	for (IndicePoints = 0; VarSplit.Selecteur [ IndicePoints ] . Nmr != -1; IndicePoints ++) {

		fprintf (fp,   "  TRACK %02d AUDIO\n", IndicePoints +1);
		fprintf (fp,   "    PERFORMER \"\"\n");
		fprintf (fp,   "    TITLE \"Track_%02d\"\n", IndicePoints +1);
		if (IndicePoints > 0) {

			// INDEX DE FIN

			Percent = VarSplit.Selecteur [ IndicePoints -1 ] . PercentEnd;
			sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
			dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
			hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
			// H = (sec / 60) / 60;
			M = (sec / 60) % 60;
			S = sec % 60;
			C = hundr;

			fprintf (fp,   "    INDEX 00 %02d:%02d:%02d\n", M, S, C);
		}

		// INDEX DE DEBUT

		Percent = VarSplit.Selecteur [ IndicePoints ] . PercentBegin;
		sec = (gint) (((gdouble)TimeSongSec * (gdouble)Percent) / 100.0);
		dsec  = ((gdouble)TimeSongSec *(gdouble)Percent) / 100.0;
		hundr = ((gdouble)dsec - (gdouble)sec) * 100.0;
		// H = (sec / 60) / 60;
		M = (sec / 60) % 60;
		S = sec % 60;
		C = hundr;

		fprintf (fp,   "    INDEX 01 %02d:%02d:%02d\n", M, S, C);
	}
	fprintf (fp,   "\n");

	fclose (fp);

	g_free (PathNameFileCue);	PathNameFileCue = NULL;
	g_free (NameFileWav);		NameFileWav = NULL;
}
//
//
//
void SplitCue_thread (void)
{
	SplitWav_extract();

	PRINT("FIN THREAD SPLIT EXTRACT");
	conv.bool_thread_conv = FALSE;
	pthread_exit(0);
}
//
//
//
static gint SplitCue_timeout (gpointer data)
{
	if (TRUE == conv.bool_thread_conv) {

	}
	else {
		PRINT("FIN TIMEOUT SPLIT EXTRACT");
		g_source_remove (conv.handler_timeout_conv);

		WindScan_close ();
	}
	return (TRUE);
}
//
//
//
void on_button_action_split_clicked (GtkButton *button, gpointer user_data)
{
	pthread_t  nmr_tid;

	PRINT_FUNC_LF();

	// WindScan_open ("Split file", WINDSCAN_PULSE);
	wind_scan_init(
		WindMain,
		"Split file",
		WINDSCAN_PULSE
		);
	WindScan_set_label ("<b><i>Split file ...</i></b>");
	conv_reset_struct (WindScan_close_request);

	conv.bool_thread_conv = TRUE;
	PRINT("DEBUT THREAD SPLIT EXTRACT");
	pthread_create (&nmr_tid, NULL ,(void *)SplitCue_thread, (void *)NULL);
	PRINT("DEBUT TIMEOUT SPLIT EXTRACT");
	conv.handler_timeout_conv = g_timeout_add (300, SplitCue_timeout, 0);
}

























