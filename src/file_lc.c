 /*
 *  file      : file_lc.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
 

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "conv.h"
#include "prg_init.h"
#include "options.h"
#include "tags.h"
#include "cd_cue.h"
#include "file.h"




// 
// Allocate Tab Args
// 
gchar **filelc_AllocTabArgs( void )
{
	gchar	**PtrTab = (gchar **)g_malloc0( sizeof(gchar **) * 50 );

	PtrTab [ 0 ] = g_strdup( "nice" );
	PtrTab [ 1 ] = g_strdup( "-n" );
	PtrTab [ 2 ] = g_strdup_printf( "%d", Config.Nice );

	return( (gchar **)PtrTab );
}
// 
// Remove Tab Args
// 
gchar **filelc_RemoveTab( gchar **p_PtrTabArgs )
{
	gint	i;
	
	for( i = 0; p_PtrTabArgs[ i ] != NULL; i ++ ) {
		g_free( p_PtrTabArgs[ i ] );
		p_PtrTabArgs[ i ] = NULL;
	}
	g_free( p_PtrTabArgs );
	p_PtrTabArgs = NULL;
	
	return( (gchar **)NULL );
}
// 
// 
gint filelc_analyse_line( gchar **p_PtrTabArgs, gchar *entry, gint *pos, TAGS *tags )
{
	gchar     **Larrbuf = NULL;
	gchar      *ptr = entry;
	GString    *gstr = NULL;
	gboolean    bool_tag = FALSE;
	gboolean    bool_open = FALSE;
	gint        i;

	while (ptr && *ptr) {
		if (*ptr == '\"') {
			if (bool_open == TRUE) {
				p_PtrTabArgs[(*pos) ++ ] = g_strdup (gstr->str);
				g_string_free (gstr, TRUE);
				gstr = NULL;
				ptr ++;
				bool_open = FALSE;
				continue;
			} else {
				ptr ++;
				bool_open = TRUE;
				continue;
			}
		}

		if (bool_open == TRUE) {
			if (gstr == NULL) gstr = g_string_new (NULL);
			g_string_append_printf (gstr, "%c", *ptr);
			ptr ++;
			continue;
		}

		if (*ptr == ' ') {
			if (gstr != NULL && *gstr->str) {
				p_PtrTabArgs[(*pos) ++ ] = g_strdup (gstr->str);
				
				g_string_free (gstr, TRUE);
				gstr = NULL;
			}
			ptr ++;
			continue;
		}
		else if (*ptr != '%') {
			if (gstr == NULL) gstr = g_string_new (NULL);
			g_string_append_printf (gstr, "%c", *ptr);
			ptr ++;
			continue;
		}

		if (*ptr == '%') {
			ptr ++;
			if ( *ptr == 'b' || *ptr == 'a' || *ptr == 't' || *ptr == 'l' || *ptr == 'N' || *ptr == 'G' || *ptr == 'c' || *ptr == 'd') {
				if (gstr == NULL) gstr = g_string_new (NULL);
			}

			/* OGGENC_WAV_TO_OGG | OGGENC_FLAC_TO_OGG */
			if (*ptr == 'b' && (*(ptr+1) == ' ' || *(ptr+1) == '\0')) {
				p_PtrTabArgs[(*pos) ++ ] = g_strdup_printf ("%s", optionsOggenc_get_val_bitrate_oggenc ());
				
				ptr ++;
			}
			/* LAME_WAV_TO_MP3 */
			else if (*ptr == 'b' && *(ptr+1) == 'l' && (*(ptr+2) == ' ' || *(ptr+2) == '\0')) {
				Larrbuf = g_strsplit (optionsLame_get_str_val_bitrate_abr_vbr_lame (), " ", 0);
				for (i=0; Larrbuf[i]; i++) {
					p_PtrTabArgs[(*pos) ++ ] = g_strdup (Larrbuf[i]);
					
				}
				g_strfreev(Larrbuf);
				ptr ++;
				ptr ++;
			} else if (*ptr == 'a' && (*(ptr+1) == ' ' || *(ptr+1) == '\0')) {
				bool_tag = TRUE == Config.BoolArtistTag && NULL != tags && NULL != tags->Artist && '\0' != *tags->Artist;
				g_string_append_printf (gstr, "%s", bool_tag ? tags->Artist : "*");
				ptr ++;
			} else if (*ptr == 't' && (*(ptr+1) == ' ' || *(ptr+1) == '\0')) {
				bool_tag = TRUE == Config.BoolTitleTag && NULL != tags && NULL != tags->Title && '\0' != *tags->Title;
				g_string_append_printf (gstr, "%s", bool_tag ? tags->Title : "*");
				ptr ++;
			} else if (*ptr == 'l' && (*(ptr+1) == ' ' || *(ptr+1) == '\0')) {
				bool_tag = TRUE == Config.BoolAlbumTag && NULL != tags && NULL != tags->Album && '\0' != *tags->Album;
				g_string_append_printf (gstr, "%s", bool_tag ? tags->Album : "Album");
				ptr ++;
			} else if (*ptr == 'N' && (*(ptr+1) == ' ' || *(ptr+1) == '\0')) {
				bool_tag = TRUE == Config.BoolNumerateTag && NULL != tags && NULL != tags->Number && '\0' != *tags->Number;
				g_string_append_printf (gstr, "%s", bool_tag ? tags->Number : "1");
				ptr ++;
			} else if (*ptr == 'G' && (*(ptr+1) == ' ' || *(ptr+1) == '\0')) {
				bool_tag = TRUE == Config.BoolGenreTag && NULL != tags && NULL != tags->Genre && '\0' != *tags->Genre;
				g_string_append_printf (gstr, "%s", bool_tag ? tags->Genre : "100");
				ptr ++;
			} else if (*ptr == 'c' && (*(ptr+1) == ' ' || *(ptr+1) == '\0')) {
				bool_tag = TRUE == Config.BoolCommentTag && NULL != tags && NULL != tags->Comment && '\0' != *tags->Comment;
				g_string_append_printf (gstr, "%s", bool_tag ? tags->Comment : "Comment");
				ptr ++;
			} else if (*ptr == 'd' && (*(ptr+1) == ' ' || *(ptr+1) == '\0')) {
				bool_tag = TRUE == Config.BoolYearTag && NULL != tags && NULL != tags->Year && '\0' != *tags->Year;
				g_string_append_printf (gstr, "%s", bool_tag ? tags->Year : "2007");
				ptr ++;
			}
			else {
				while (ptr && *ptr != ' ') ptr ++;
			}
		}
	}
	if (gstr != NULL && *gstr->str) {
		p_PtrTabArgs[(*pos) ++ ] = g_strdup (gstr->str);
		
		g_string_free (gstr, TRUE);
		gstr = NULL;
	}
	return( *pos );
}
gchar **filelc_analyse_command_line( PARAM_FILELC *param_filelc, gchar *entry )
{
	gint	pos = 0;
	gchar	**PtrTabArgs = NULL;
	
	if (param_filelc->type_conv == LAME_WAV_TO_MP3) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3; 
		PtrTabArgs[ pos++ ] = g_strdup ("lame");
		pos = filelc_analyse_line( PtrTabArgs, entry, &pos, param_filelc->tags );
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup ("-o");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == OGGENC_WAV_TO_OGG || param_filelc->type_conv == OGGENC_FLAC_TO_OGG) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("oggenc");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		pos = filelc_analyse_line (PtrTabArgs, entry, &pos, param_filelc->tags);
		PtrTabArgs[ pos++ ] = g_strdup ("-o");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == FLAC_WAV_TO_FLAC) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("flac");
		pos = filelc_analyse_line (PtrTabArgs, entry, &pos, param_filelc->tags);
		PtrTabArgs[ pos++ ] = g_strdup ("-f");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup ("-o");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == FAAC_WAV_TO_M4A) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("faac");
		pos = filelc_analyse_line (PtrTabArgs, entry, &pos, param_filelc->tags);
		PtrTabArgs[ pos++ ] = g_strdup ("-o");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == MPPENC_WAV_TO_MPC) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup (prginit_get_name (NMR_musepack_tools_mppenc));
		pos = filelc_analyse_line (PtrTabArgs, entry, &pos, param_filelc->tags);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == MAC_WAV_TO_APE || param_filelc->type_conv == MAC_APE_TO_WAV) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup (prginit_get_name (NMR_mac));
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == WAVPACK_WAV_TO_WAVPACK) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("wavpack");
		pos = filelc_analyse_line (PtrTabArgs, entry, &pos, param_filelc->tags);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup ("-o");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == CDPARANOIA_CD_TO_WAV) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("cdparanoia");	
		PtrTabArgs[ pos++ ] = g_strdup ("-e");
		PtrTabArgs[ pos++ ] = g_strdup ("-d");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->cdrom);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->num_track);
		pos = filelc_analyse_line (PtrTabArgs, entry, &pos, param_filelc->tags);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == CDDA2WAV_CD_TO_WAV) {
		
		PtrTabArgs = filelc_AllocTabArgs();
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("cdda2wav");
		PtrTabArgs[ pos++ ] = g_strdup ("-D");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->cdrom);
		PtrTabArgs[ pos++ ] = g_strdup ("-O");
		PtrTabArgs[ pos++ ] = g_strdup ("wav");
		PtrTabArgs[ pos++ ] = g_strdup ("-t");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->num_track);
		PtrTabArgs[ pos++ ] = NULL;
	}
	
	return( (gchar **)PtrTabArgs );
}
// 
// 
gboolean filelc_bool_add_line_command (TYPE_CONV p_type_conv, gchar **entry)
{
	GtkWidget *wd = NULL;
	
	switch (p_type_conv) {
	
	case FLAC_WAV_TO_FLAC :		wd = var_options.Adr_entry_flac_flac;		break;
	case LAME_WAV_TO_MP3 :		wd = var_options.Adr_entry_lame_mp3;		break;
	case OGGENC_WAV_TO_OGG :	wd = var_options.Adr_entry_oggenc_ogg;		break;
	case FAAC_WAV_TO_M4A :		wd = var_options.Adr_entry_faac_m4a;		break;
	case MPPENC_WAV_TO_MPC :	wd = var_options.Adr_entry_musepack_mpc;	break;
	case MAC_WAV_TO_APE :		wd = var_options.Adr_entry_mac_ape;		break;
	case WAVPACK_WAV_TO_WAVPACK :	wd = var_options.Adr_entry_wavpack_wv;		break;
	
	case CDPARANOIA_CD_TO_WAV_EXPERT :
	case CDPARANOIA_CD_TO_WAV :
	case CDDA2WAV_CD_TO_WAV :	return (FALSE);
	
	default :			return (FALSE);
	}
	if (wd == NULL) return (FALSE);
	
	*entry = (gchar *)gtk_entry_get_text (GTK_ENTRY(wd));

	return ((entry != NULL && **entry != '\0' && **entry != ' ') ? TRUE : FALSE);
}
// 
// 
gchar **filelc_get_command_line( PARAM_FILELC *param_filelc )
{
	gint	pos = 0;
	gchar	*entry = NULL;
	gchar	**PtrTabArgs = NULL;
	
	// OPTIONS LIGNE DE SAISIE UTILISATEUR
	if (TRUE == param_filelc->With_CommandLineUser &&
	    TRUE == filelc_bool_add_line_command (param_filelc->type_conv, &entry)) {
		
		PtrTabArgs = filelc_analyse_command_line( param_filelc, entry );
		return( (gchar **)PtrTabArgs );
	}
	
	param_filelc->With_CommandLineUser = FALSE;
	
	PtrTabArgs = filelc_AllocTabArgs();
	
	if (param_filelc->type_conv == LAME_WAV_TO_MP3) {
		
		gchar	**Larrbuf = NULL;
		gint	  i;
		
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup ("lame");
		PtrTabArgs [ pos++ ] = g_strdup ("-h");
		PtrTabArgs [ pos++ ] = g_strdup ("--nohist");
		PtrTabArgs [ pos++ ] = g_strdup ("--noreplaygain");
		
		if (param_filelc->PtrStrBitrate != NULL) {
			Larrbuf = g_strsplit (param_filelc->PtrStrBitrate, " ", 0);
			for (i=0; Larrbuf[i]; i++) {
				PtrTabArgs [ pos++ ] = g_strdup (Larrbuf[i]);
			}
			g_strfreev(Larrbuf);
			g_free (param_filelc->PtrStrBitrate);
			param_filelc->PtrStrBitrate = NULL;
		}
		
		
		if (Config.BoolArtistTag || Config.BoolTitleTag || Config.BoolAlbumTag ||
		    Config.BoolNumerateTag || Config.BoolGenreTag || Config.BoolYearTag || Config.BoolCommentTag) {
			PtrTabArgs [ pos++ ] = g_strdup ("--add-id3v2");
		}
		
		if (Config.BoolArtistTag && param_filelc->tags && param_filelc->tags->Artist && *param_filelc->tags->Artist) {
			PtrTabArgs [ pos++ ] = g_strdup ("--ta");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Artist);
		}
		
		if (Config.BoolTitleTag && param_filelc->tags && param_filelc->tags->Title && *param_filelc->tags->Title) {
			PtrTabArgs [ pos++ ] = g_strdup ("--tt");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Title);
		}
		
		if (Config.BoolAlbumTag && param_filelc->tags && param_filelc->tags->Album && *param_filelc->tags->Album) {
			PtrTabArgs [ pos++ ] = g_strdup ("--tl");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Album);
		}
		
		if (Config.BoolNumerateTag && param_filelc->tags && param_filelc->tags->Number && *param_filelc->tags->Number) {
			PtrTabArgs [ pos++ ] = g_strdup ("--tn");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Number);
		}
		
		if (Config.BoolGenreTag && param_filelc->tags && param_filelc->tags->Genre && *param_filelc->tags->Genre) {
			

/**
NAME
       lame - create mp3 audio files

SYNOPSIS
       lame [options] <infile> <outfile>

DESCRIPTION
       LAME  is  a  program which can be used to create compressed audio files.
       (Lame ain't an MP3 encoder).
       These audio files can be played back by popular MP3 players such as mpg123 or madplay.
       To read from stdin, use "-" for <infile>.
       To write to stdout, use "-" for <outfile>.

OPTIONS
      ID3 tag options:

      Input options:
      --tg genre
              audio/song genre (name or number in list)

*/
			/**
			gint value = 147;
			
			if (param_filelc->tags->bool_tag_cd == FALSE) {
				value = tags_get_genre_by_value (param_filelc->tags->Genre);
			} else {
				value = param_filelc->tags->IntGenre;
			}
			if (value < 0 || value > 147) value = 147;
			// New = g_list_append (New, g_strdup ("--tg"));
			// New = g_list_append (New, g_strdup_printf ("%d", value));
			PtrTabArgs [ pos++ ] = g_strdup ("--tg");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("%d", value);
			*/
			PtrTabArgs [ pos++ ] = g_strdup ("--tg");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("%s", param_filelc->tags->Genre);
			
			
		}
		
		if (Config.BoolYearTag && param_filelc->tags && param_filelc->tags->Year && *param_filelc->tags->Year) {
			PtrTabArgs [ pos++ ] = g_strdup ("--ty");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Year);
		}
		
		if (Config.BoolCommentTag && param_filelc->tags && param_filelc->tags->Comment && *param_filelc->tags->Comment) {
			PtrTabArgs [ pos++ ] = g_strdup ("--tc");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Comment);
		}
		
		PtrTabArgs [ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs [ pos++ ] = g_strdup ("-o");
		PtrTabArgs [ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs [ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == OGGENC_WAV_TO_OGG || param_filelc->type_conv == OGGENC_FLAC_TO_OGG) {

		gchar	**Larrbuf = NULL;
		gint	  i;
		
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("oggenc");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		
		if (param_filelc->PtrStrBitrate != NULL) {
			Larrbuf = g_strsplit (param_filelc->PtrStrBitrate, " ", 0);
			for (i=0; Larrbuf[i]; i++) {
				PtrTabArgs[ pos++ ] = g_strdup (Larrbuf[i]);
			}
			g_strfreev(Larrbuf);
			g_free (param_filelc->PtrStrBitrate);
			param_filelc->PtrStrBitrate = NULL;
		}

		if (Config.BoolArtistTag && param_filelc->tags && param_filelc->tags->Artist && *param_filelc->tags->Artist) {
			PtrTabArgs[ pos++ ] = g_strdup ("-a");
			PtrTabArgs[ pos++ ] = g_strdup ("-a");
		}
		if (Config.BoolTitleTag && param_filelc->tags && param_filelc->tags->Title && *param_filelc->tags->Title) {
			PtrTabArgs[ pos++ ] = g_strdup ("-t");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Title);
		}
		if (Config.BoolAlbumTag && param_filelc->tags && param_filelc->tags->Album && *param_filelc->tags->Album) {
			PtrTabArgs[ pos++ ] = g_strdup ("-l");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Album);
		}
		if (Config.BoolNumerateTag && param_filelc->tags && param_filelc->tags->Number && *param_filelc->tags->Number) {
			PtrTabArgs[ pos++ ] = g_strdup ("-N");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Number);
		}
		if (Config.BoolGenreTag && param_filelc->tags && param_filelc->tags->Genre && *param_filelc->tags->Genre) {
			PtrTabArgs[ pos++ ] = g_strdup ("-G");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Genre);
		}
		if (Config.BoolYearTag && param_filelc->tags && param_filelc->tags->Year && *param_filelc->tags->Year) {
			PtrTabArgs[ pos++ ] = g_strdup ("--date");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Year);
		}
		if (Config.BoolCommentTag && param_filelc->tags && param_filelc->tags->Comment && *param_filelc->tags->Comment) {
			PtrTabArgs[ pos++ ] = g_strdup ("-c");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Comment);
		}

		PtrTabArgs[ pos++ ] = g_strdup ("-o");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == FLAC_WAV_TO_FLAC || param_filelc->type_conv == FLAC_FLAC_TO_WAV) {
		
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("flac");
		PtrTabArgs[ pos++ ] = g_strdup_printf ("%s", optionsFlac_get_compression_level_flac ());
		
		if (Config.BoolArtistTag && param_filelc->tags && param_filelc->tags->Artist && *param_filelc->tags->Artist) {
			PtrTabArgs[ pos++ ] = g_strdup ("-T");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("artist=%s",param_filelc->tags->Artist);
		}
		if (Config.BoolTitleTag && param_filelc->tags && param_filelc->tags->Title && *param_filelc->tags->Title) {
			PtrTabArgs[ pos++ ] = g_strdup ("-T");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("title=%s", param_filelc->tags->Title);
		}
		if (Config.BoolAlbumTag && param_filelc->tags && param_filelc->tags->Album && *param_filelc->tags->Album) {
			PtrTabArgs[ pos++ ] = g_strdup ("-T");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("album=%s", param_filelc->tags->Album);
		}
		if (Config.BoolNumerateTag && param_filelc->tags && param_filelc->tags->IntNumber > -1) {
			PtrTabArgs[ pos++ ] = g_strdup ("-T");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("tracknumber=%d", param_filelc->tags->IntNumber);
		}
		if (Config.BoolGenreTag && param_filelc->tags && param_filelc->tags->Genre && *param_filelc->tags->Genre) {
			PtrTabArgs[ pos++ ] = g_strdup ("-T");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("genre=%s", param_filelc->tags->Genre);
		}
		if (Config.BoolYearTag && param_filelc->tags && param_filelc->tags->Year && *param_filelc->tags->Year) {
			PtrTabArgs[ pos++ ] = g_strdup ("-T");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("date=%s", param_filelc->tags->Year);
		}
		if (Config.BoolCommentTag && param_filelc->tags && param_filelc->tags->Comment && *param_filelc->tags->Comment) {
			PtrTabArgs[ pos++ ] = g_strdup ("-T");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("Description=%s",param_filelc->tags->Comment);
		}
		
		if (param_filelc->type_conv == FLAC_FLAC_TO_WAV) PtrTabArgs [ pos++ ] = g_strdup ("-d");
		
		PtrTabArgs[ pos++ ] = g_strdup ("-f");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup ("-o");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == FAAC_WAV_TO_M4A) {

		gchar *Ptr = NULL;
		
		pos = 3;
		PtrTabArgs [ pos++ ] = g_strdup ("faac");
		
		// filelc_analyse_line (entry, New, param_filelc->tags);
		// -----------------------------------------------------
		
		if ('-' == *OptionsFaac_get_faac_conteneur ()) {
			PtrTabArgs [ pos++ ] = g_strdup (OptionsFaac_get_faac_conteneur ());
		}
		
		Ptr = OptionsFaac_get_faac_set_choice_vbr_abr ();
		if ( *(Ptr + 1) == 'b') {
			PtrTabArgs [ pos++ ] = g_strdup ("-b");
		}
		else {
			PtrTabArgs [ pos++ ] = g_strdup ("-q");
		}
		PtrTabArgs [ pos++ ] = g_strdup_printf ("%s", (Ptr +3));
		
		
		if (Config.BoolArtistTag && param_filelc->tags && param_filelc->tags->Artist && *param_filelc->tags->Artist) {
			PtrTabArgs [ pos++ ] = g_strdup ("--artist");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Artist);
		}
		if (Config.BoolTitleTag && param_filelc->tags && param_filelc->tags->Title && *param_filelc->tags->Title) {
			PtrTabArgs [ pos++ ] = g_strdup ("--title");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Title);
		}
		if (Config.BoolAlbumTag && param_filelc->tags && param_filelc->tags->Album && *param_filelc->tags->Album) {
			PtrTabArgs [ pos++ ] = g_strdup ("--album");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Album);
		}
		if (Config.BoolGenreTag && param_filelc->tags && param_filelc->tags->Genre && *param_filelc->tags->Genre) {
			PtrTabArgs [ pos++ ] = g_strdup ("--genre");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Genre);
		}
		if (Config.BoolYearTag && param_filelc->tags && param_filelc->tags->Year && *param_filelc->tags->Year) {
			PtrTabArgs [ pos++ ] = g_strdup ("--year");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Year);
		}
		if (Config.BoolNumerateTag && param_filelc->tags && param_filelc->tags->IntNumber > -1) {
			PtrTabArgs [ pos++ ] = g_strdup ("--track");
			PtrTabArgs [ pos++ ] = g_strdup_printf ("%d", param_filelc->tags->IntNumber);
		}
		if (Config.BoolCommentTag && param_filelc->tags && param_filelc->tags->Comment && *param_filelc->tags->Comment) {
			PtrTabArgs [ pos++ ] = g_strdup ("--comment");
			PtrTabArgs [ pos++ ] = g_strdup (param_filelc->tags->Comment);
		}

		// -----------------
		
		PtrTabArgs [ pos++ ] = g_strdup ("-o");
		PtrTabArgs [ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs [ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs [ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == MPPENC_WAV_TO_MPC) {
		
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup (prginit_get_name (NMR_musepack_tools_mppenc));
		PtrTabArgs[ pos++ ] = g_strdup ("--verbose");
		PtrTabArgs[ pos++ ] = g_strdup ("--overwrite");
		PtrTabArgs[ pos++ ] = g_strdup (optionsMusepack_get_quality_mppenc ());

		if (Config.BoolArtistTag && param_filelc->tags && param_filelc->tags->Artist && *param_filelc->tags->Artist) {
			PtrTabArgs[ pos++ ] = g_strdup ("--artist");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Artist);
		}
		if (Config.BoolTitleTag && param_filelc->tags && param_filelc->tags->Title && *param_filelc->tags->Title) {
			PtrTabArgs[ pos++ ] = g_strdup ("--title");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Title);
		}
		if (Config.BoolAlbumTag && param_filelc->tags && param_filelc->tags->Album && *param_filelc->tags->Album) {
			PtrTabArgs[ pos++ ] = g_strdup ("--album");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Album);
		}
		if (Config.BoolGenreTag && param_filelc->tags && param_filelc->tags->Genre && *param_filelc->tags->Genre) {
			PtrTabArgs[ pos++ ] = g_strdup ("--genre");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Genre);
		}
		if (Config.BoolYearTag && param_filelc->tags && param_filelc->tags->Year && *param_filelc->tags->Year) {
			PtrTabArgs[ pos++ ] = g_strdup ("--year");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Year);
		}
		if (Config.BoolNumerateTag && param_filelc->tags && param_filelc->tags->Number && *param_filelc->tags->Number) {
			PtrTabArgs[ pos++ ] = g_strdup ("--track");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Number);
		}
		if (Config.BoolCommentTag && param_filelc->tags && param_filelc->tags->Comment && *param_filelc->tags->Comment) {
			PtrTabArgs[ pos++ ] = g_strdup ("--comment");
			PtrTabArgs[ pos++ ] = g_strdup (param_filelc->tags->Comment);
		}
		
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == MAC_WAV_TO_APE || param_filelc->type_conv == MAC_APE_TO_WAV) {
		
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup (prginit_get_name (NMR_mac));	
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		
		if (param_filelc->type_conv == MAC_APE_TO_WAV) {
			PtrTabArgs[ pos++ ] = g_strdup ("-d");
		}
		else if (param_filelc->type_conv == MAC_WAV_TO_APE) {
			PtrTabArgs[ pos++ ] = g_strdup (optionsApe_get_compression_level_ape ());
		}
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == WAVPACK_WAV_TO_WAVPACK) {
		
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("wavpack");
		PtrTabArgs[ pos++ ] = g_strdup ("-y");
				
		if (*optionsWavpack_get_wavpack_compression () != '\0')
			PtrTabArgs[ pos++ ] = g_strdup (optionsWavpack_get_wavpack_compression ());
		PtrTabArgs[ pos++ ] = g_strdup (optionsWavpack_get_wavpack_sound ());
		
		if (*optionsWavpac_get_wavpack_hybride () != '\0') {
			PtrTabArgs[ pos++ ] = g_strdup (optionsWavpac_get_wavpack_hybride ());
			if (*optionsWavpack_get_wavpack_correction_file () != '\0') {
				PtrTabArgs[ pos++ ] = g_strdup (optionsWavpack_get_wavpack_correction_file ());
			}
			if (*optionsWavpack_get_wavpack_maximum_compression () != '\0') {
				PtrTabArgs[ pos++ ] = g_strdup (optionsWavpack_get_wavpack_maximum_compression ());
			}
		}
		if (*optionsWavpack_get_wavpack_signature_md5 () != '\0') {
			PtrTabArgs[ pos++ ] = g_strdup (optionsWavpack_get_wavpack_signature_md5 ());
		}
		if (*optionsWavpack_get_wavpack_extra_encoding () != '\0') {
			PtrTabArgs[ pos++ ] = g_strdup (optionsWavpack_get_wavpack_extra_encoding ());
		}

		if (Config.BoolArtistTag && param_filelc->tags && param_filelc->tags->Artist && *param_filelc->tags->Artist) {
			PtrTabArgs[ pos++ ] = g_strdup ("-w");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("Artist=%s", param_filelc->tags->Artist);
		}
		if (Config.BoolTitleTag && param_filelc->tags && param_filelc->tags->Title && *param_filelc->tags->Title) {
			PtrTabArgs[ pos++ ] = g_strdup ("-w");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("Title=%s", param_filelc->tags->Title);
		}
		if (Config.BoolAlbumTag && param_filelc->tags && param_filelc->tags->Album && *param_filelc->tags->Album) {
			PtrTabArgs[ pos++ ] = g_strdup ("-w");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("Album=%s", param_filelc->tags->Album);
		}
		if (Config.BoolGenreTag && param_filelc->tags && param_filelc->tags->Genre && *param_filelc->tags->Genre) {
			PtrTabArgs[ pos++ ] = g_strdup ("-w");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("Genre=%s", param_filelc->tags->Genre);
		}
		if (Config.BoolYearTag && param_filelc->tags && param_filelc->tags->Year && *param_filelc->tags->Year) {
			PtrTabArgs[ pos++ ] = g_strdup ("-w");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("Year=%s", param_filelc->tags->Year);
		}
		if (Config.BoolNumerateTag && param_filelc->tags && param_filelc->tags->Number && *param_filelc->tags->Number) {
			PtrTabArgs[ pos++ ] = g_strdup ("-w");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("Track=%s", param_filelc->tags->Number);
		}
		if (Config.BoolCommentTag && param_filelc->tags &&param_filelc->tags->Comment && *param_filelc->tags->Comment) {
			PtrTabArgs[ pos++ ] = g_strdup ("-w");
			PtrTabArgs[ pos++ ] = g_strdup_printf ("Comment=%s", param_filelc->tags->Comment);
		}
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup ("-o");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == CDPARANOIA_CD_TO_WAV) {
		
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("cdparanoia");
		PtrTabArgs[ pos++ ] = g_strdup ("-e");
		PtrTabArgs[ pos++ ] = g_strdup ("-d");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->cdrom);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->num_track);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == CDDA2WAV_CD_TO_WAV) {
		
		// cdda2wav -D /dev/hda -O wav -t 7
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("cdda2wav");
		PtrTabArgs[ pos++ ] = g_strdup ("-D");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->cdrom);
		PtrTabArgs[ pos++ ] = g_strdup ("-O");
		PtrTabArgs[ pos++ ] = g_strdup ("wav");
		PtrTabArgs[ pos++ ] = g_strdup ("-t");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->num_track);
		PtrTabArgs[ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == AACPLUSENC_WAV_TO_AAC) {
		
		pos = 3;
		PtrTabArgs[ pos++ ] = g_strdup ("aacplusenc");
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filesrc);
		PtrTabArgs[ pos++ ] = g_strdup (param_filelc->filedest);
		
		if( NULL != param_filelc->tags && TRUE == param_filelc->tags->bool_tag_cd ) {
			PtrTabArgs[ pos++ ] = g_strdup_printf ("%d", optionsaacplusenc_get_bitrate_stereo ());
		}
		else {
			if (tagswav_file_is_mono (param_filelc->filesrc) == TRUE) {
				PtrTabArgs[ pos++ ] = g_strdup_printf ("%d", optionsaacplusenc_get_bitrate_mono ());
			} else {
				PtrTabArgs[ pos++ ] = g_strdup_printf ("%d", optionsaacplusenc_get_bitrate_stereo ());
			}
		}
		PtrTabArgs[ pos++ ] = NULL;
	}
	
	return( (gchar **)PtrTabArgs );
}
// 
// 
void filelc_get_command_line_extract_elem (gint Elem, gchar *Dev, gchar *Dest)
{
	gint	pos = 0;
	
	for (pos = 0; conv.ArgExtract [ pos ] != NULL; pos ++) {
		g_free (conv.ArgExtract [ pos ]);
		conv.ArgExtract [ pos ] = NULL;
	}
	pos = 0;
	conv.ArgExtract [ pos++ ] = g_strdup ("nice");
	conv.ArgExtract [ pos++ ] = g_strdup ("-n");
	conv.ArgExtract [ pos++ ] = g_strdup_printf ("%d", Config.Nice);
	conv.ArgExtract [ pos++ ] = g_strdup ("cdparanoia");
	conv.ArgExtract [ pos++ ] = g_strdup_printf ("[.%d]-[.0]", Elem);
	conv.ArgExtract [ pos++ ] = g_strdup ("-e");
	conv.ArgExtract [ pos++ ] = g_strdup ("-d");
	conv.ArgExtract [ pos++ ] = g_strdup (Dev);
	conv.ArgExtract [ pos++ ] = g_strdup ("-O");
	conv.ArgExtract [ pos++ ] = g_strdup ("0");
	conv.ArgExtract [ pos++ ] = g_strdup (Dest);
	conv.ArgExtract [ pos++ ] = NULL;
}
// 
// 
void filelc_get_command_line_extract (PARAM_FILELC *param_filelc)
{
	gint	pos = 0;
	
	for (pos = 0; conv.ArgExtract [ pos ] != NULL; pos ++) {
		g_free (conv.ArgExtract [ pos ]);
		conv.ArgExtract [ pos ] = NULL;
	}
	pos = 0;
	if (param_filelc->type_conv == CDPARANOIA_CD_TO_WAV) {
		conv.ArgExtract [ pos++ ] = g_strdup ("nice");
		conv.ArgExtract [ pos++ ] = g_strdup ("-n");
		conv.ArgExtract [ pos++ ] = g_strdup_printf ("%d", Config.Nice);
		conv.ArgExtract [ pos++ ] = g_strdup ("cdparanoia");
		conv.ArgExtract [ pos++ ] = g_strdup ("-e");
		conv.ArgExtract [ pos++ ] = g_strdup ("-d");
		conv.ArgExtract [ pos++ ] = g_strdup (param_filelc->cdrom);
		conv.ArgExtract [ pos++ ] = g_strdup (param_filelc->num_track);
		conv.ArgExtract [ pos++ ] = g_strdup (param_filelc->filedest);
		conv.ArgExtract [ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == CDPARANOIA_CD_TO_WAV_EXPERT) {
		
		gint NumTrack = atoi(param_filelc->num_track) -1;
		
		conv.ArgExtract [ pos++ ] = g_strdup ("nice");
		conv.ArgExtract [ pos++ ] = g_strdup ("-n");
		conv.ArgExtract [ pos++ ] = g_strdup_printf ("%d", Config.Nice);
		conv.ArgExtract [ pos++ ] = g_strdup ("cdparanoia");
		conv.ArgExtract [ pos++ ] = g_strdup_printf ("[.%d]-[.%d]",
							BaseIoctl.Datas[ NumTrack ].begin < 44
							? 0
							: BaseIoctl.Datas[ NumTrack ].begin,
							NumTrack == 0
							? (BaseIoctl.Datas[ NumTrack ].length + BaseIoctl.Datas[ NumTrack ].begin) -1
							: BaseIoctl.Datas[ NumTrack ].length -1
							);
		conv.ArgExtract [ pos++ ] = g_strdup ("-e");
		conv.ArgExtract [ pos++ ] = g_strdup ("-d");
		conv.ArgExtract [ pos++ ] = g_strdup (param_filelc->cdrom);
		conv.ArgExtract [ pos++ ] = g_strdup ("-O");
		conv.ArgExtract [ pos++ ] = g_strdup ("0");
		conv.ArgExtract [ pos++ ] = g_strdup (param_filelc->filedest);
		conv.ArgExtract [ pos++ ] = NULL;
	}
	else if (param_filelc->type_conv == CDDA2WAV_CD_TO_WAV) {
		conv.ArgExtract [ pos++ ] = g_strdup ("nice");
		conv.ArgExtract [ pos++ ] = g_strdup ("-n");
		conv.ArgExtract [ pos++ ] = g_strdup_printf ("%d", Config.Nice);
		conv.ArgExtract [ pos++ ] = g_strdup (prginit_get_name (NMR_icedax));
		conv.ArgExtract [ pos++ ] = g_strdup ("-D");
		conv.ArgExtract [ pos++ ] = g_strdup (param_filelc->cdrom);
		conv.ArgExtract [ pos++ ] = g_strdup ("-O");
		conv.ArgExtract [ pos++ ] = g_strdup ("wav");
		conv.ArgExtract [ pos++ ] = g_strdup ("-t");
		conv.ArgExtract [ pos++ ] = g_strdup (param_filelc->num_track);
		conv.ArgExtract [ pos++ ] = g_strdup ("--no-infofile");
		conv.ArgExtract [ pos++ ] = NULL;
	}
}
// 
// 
GList *filelc_remove_glist (GList *New)
{
	GList *list = NULL;
	gchar *ptr = NULL;

	if (New != NULL) {
		list = g_list_first (New);
		while (list) {
			if ((ptr = (gchar*)list->data)) {
				g_free (ptr);
				ptr = list->data = NULL;
			}
			list = g_list_next(list);
		}
		g_list_free (New);
		New = NULL;
	}
	return ((GList *)NULL);
}



