 /*
 *  file      : file_wav.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "dragNdrop.h"
#include "fileselect.h"
#include "file.h"
#include "popup.h"
#include "statusbar.h"




VAR_FILE_WAV var_file_wav;

enum
{
	COLUMN_FILEWAV_HERTZ = 0,
	COLUMN_FILEWAV_NEW_HERTZ,
	COLUMN_FILEWAV_VOIE,
	COLUMN_FILEWAV_NEW_VOIE,
	COLUMN_FILEWAV_BITS,
	COLUMN_FILEWAV_NEW_BITS,
	COLUMN_FILEWAV_NAME,
	
	COLUMN_FILEWAV_POINTER_STRUCT,

	COLUMN_FILE_NUM
};
enum
{
	NUM_TREE_FILEWAV_Frequence = 0,
	NUM_TREE_FILEWAV_FREQUENCE,
	NUM_TREE_FILEWAV_Piste,
	NUM_TREE_FILEWAV_PISTE,
	NUM_TREE_FILEWAV_Quantification,
	NUM_TREE_FILEWAV_QUANTIFICATION,
	NUM_TREE_FILEWAV_Nom,
	NUM_TREE_FILEWAV_ALL_COLUMN
};





// 
// 
gboolean FileWav_get_line_is_selected (void)
{
	GtkTreeModel     *model = NULL;
	GList            *list = NULL;

	if (var_file_wav.Adr_TreeView == NULL) return (FALSE);
	if (var_file_wav.Adr_Line_Selected == NULL) return (FALSE);
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_wav.Adr_TreeView));
	list = gtk_tree_selection_get_selected_rows (var_file_wav.Adr_Line_Selected, &model);
	return (list ? TRUE : FALSE);
}
// 
// 
gboolean FileWav_get_bool_conversion (void)
{
	GList		*List = NULL;
	INFO_WAV	*info = NULL;
	DETAIL           *detail = NULL;
	
	List = g_list_first (entetefile);
	while (List) {
		if (NULL != (detail = (DETAIL *)List->data)) {
			if (FILE_IS_WAV == detail->type_infosong_file_is) {
				if (NULL != (info = (INFO_WAV *)detail->info) && TRUE == info->BoolConv) return (TRUE);
			}
		}
		List = g_list_next (List);
	}
	return (FALSE);

}
// CHANGEMENT DES PARAMETRES: freq, voie, bits SI PARAM <> NULL
// 
void FileWav_set_flag_buttons (void)
{
	gboolean      BoolButtonsAudio [ 3 ];

	// LECTURE
	BoolButtonsAudio [ 0 ] = 
	BoolButtonsAudio [ 1 ] = FileWav_get_line_is_selected ();
	BoolButtonsAudio [ 2 ] = FileWav_get_bool_conversion ();

	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_del_file")),		BoolButtonsAudio [ 0 ]);
	// gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("spinbutton_frequence_wav")),	BoolButtonsAudio [ 1 ]);
	// gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("combobox_voie_wav")),		BoolButtonsAudio [ 1 ]);
	// gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("combobox_bit_wav")),		BoolButtonsAudio [ 1 ]);
	gtk_widget_set_sensitive (GTK_WIDGET (GLADE_GET_OBJECT("button_apply_wav")),		BoolButtonsAudio [ 2 ]);
}
// CHANGEMENT DES PARAMETRES: freq, voie, bits SI PARAM <> NULL
// 
void FileWav_change_parameters (void)
{
	GtkTreeIter       iter;
	DETAIL           *detail = NULL;
	INFO_WAV	*info = NULL;
	gchar		*NewHertz = NULL;
	gchar		*NewVoie = NULL;
	gchar		*NewBits = NULL;
	gboolean	valid;
	
	valid = gtk_tree_model_get_iter_first (var_file_wav.Adr_Tree_Model, &iter);
	while (valid) {
		gtk_tree_model_get (var_file_wav.Adr_Tree_Model, &iter, COLUMN_FILEWAV_POINTER_STRUCT, &detail, -1);
		if (NULL != detail) {
			if (FILE_IS_WAV == detail->type_infosong_file_is) {
				
				info = (INFO_WAV *)detail->info;

				// Et mettre le contenu a jour
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_HERTZ, info->hertz, -1);
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_VOIE, info->voie, -1);
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_BITS, info->bits, -1);

				// Et mettre le contenu a jour
				NewHertz = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewHertz);
				NewVoie  = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewVoie);
				NewBits  = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewBits);
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_NEW_HERTZ, NewHertz, -1);
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_NEW_VOIE, NewVoie, -1);
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_NEW_BITS, NewBits, -1);
				g_free (NewHertz);	NewHertz = NULL;
				g_free (NewVoie);	NewVoie = NULL;
				g_free (NewBits);	NewBits = NULL;
			}
			valid = gtk_tree_model_iter_next (var_file_wav.Adr_Tree_Model, &iter);
		}
	}
}
// MARQUER LES LIGNES POUR LA DESTRUCTION
// 
gboolean FileWav_del_file_clicked (void)
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GList		*BeginList = NULL;
	GList		*list = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;
	gboolean	 BoolPrint = FALSE;
	
	// RECUP. LIGNES EN SELECTION POUR DESTRUCTION
	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_wav.Adr_TreeView));
	if ((BeginList = gtk_tree_selection_get_selected_rows (var_file_wav.Adr_Line_Selected, &model))) {
		BoolPrint = TRUE;
		list = g_list_first (BeginList);
		while (list) {
			if ((path = list->data)) {
				gtk_tree_model_get_iter (model, &iter, path);				
				gtk_tree_model_get (var_file_wav.Adr_Tree_Model, &iter, COLUMN_FILEWAV_POINTER_STRUCT, &detail, -1);
				// MARQUER LA LIGNE DU GLIST A DETRUIRE AVANT LE REAFFICHAGE
				if (NULL != detail) detail->BoolRemove = TRUE;
			}
			list = g_list_next (list);
		}
		// gtk_tree_selection_unselect_all (var_file_wav.Adr_Line_Selected);
	}
	return (BoolPrint);
}
// 
// 
void FileWav_affiche_glist (void)
{
	DETAIL		*detail = NULL;
	GList		*List = NULL;
	GtkTreeIter	iter;
	GtkAdjustment	*Adj = NULL;
	gdouble		AdjValue;
	gint		Line = 0;		// LIGNE EN COURS
	INFO_WAV	*info = NULL;
	gchar		*NewHertz = NULL;
	gchar		*NewVoie = NULL;
	gchar		*NewBits = NULL;
	gchar		*NameDest = NULL;
	gint		NumLineSelected = -1;
	gboolean	BoolNumLineSelected = FALSE;
	
	// RECUP SELECTION
	// 
	NumLineSelected = libutils_get_first_line_is_selected( var_file_wav.Adr_Line_Selected, var_file_wav.Adr_Tree_Model );
	
	gtk_tree_selection_unselect_all (var_file_wav.Adr_Line_Selected);

	// DELETE TREEVIEW
	// 
	gtk_list_store_clear (GTK_LIST_STORE (var_file_wav.Adr_List_Store));
	
	// COORDONNEES POUR UN REAJUSTEMENT VISUEL DE LA PAGE
	// 
	Adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (var_file_wav.Adr_scroll));
	AdjValue = gtk_adjustment_get_value (Adj);
	
	// AFFICHAGE DE LA LISTE
	// 
	Line = 0;
	List = g_list_first (entetefile);
	while (List) {
		if (NULL != ((detail = (DETAIL *)List->data)) && FALSE == detail->BoolRemove) {
			if (FILE_IS_WAV != detail->type_infosong_file_is) {
				List = g_list_next (List);
				continue;
			}
			if (NULL == (info = (INFO_WAV *)detail->info)) {
				List = g_list_next (List);
				continue;
			}
			NewHertz = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewHertz);
			NewVoie  = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewVoie);
			NewBits  = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewBits);
			
			NameDest = libutils_get_name_without_ext_with_amp (detail->namefile);

			gtk_list_store_append (var_file_wav.Adr_List_Store, &iter);
			gtk_list_store_set (var_file_wav.Adr_List_Store, &iter,
						COLUMN_FILEWAV_HERTZ,		info->hertz,
						COLUMN_FILEWAV_NEW_HERTZ,	NewHertz,
						COLUMN_FILEWAV_VOIE,		info->voie,
						COLUMN_FILEWAV_NEW_VOIE,	NewVoie,
						COLUMN_FILEWAV_BITS,		info->bits,
						COLUMN_FILEWAV_NEW_BITS,	NewBits,
						COLUMN_FILEWAV_NAME,		NameDest,
						// COLUMN_FILEWAV_COLOR,		&YellowColor,
						COLUMN_FILEWAV_POINTER_STRUCT,	detail,
						-1);
									
			g_free (NameDest);	NameDest = NULL;
			g_free (NewHertz);	NewHertz = NULL;
			g_free (NewVoie);	NewVoie = NULL;
			g_free (NewBits);	NewBits = NULL;
			
			// AFFICHE LES EVENTUELLES LIGNES EN SELECTION
			// 
			if( NumLineSelected == Line ) {
				gtk_tree_selection_select_iter (var_file_wav.Adr_Line_Selected, &iter);
				BoolNumLineSelected = TRUE;
			}
			
			Line ++;
		}
		List = g_list_next (List);
	}
	
	// SUPPRESSON TABLEAU DES EVENTUELLES LIGNES EN SELECTION
	// 
	if( NumLineSelected == -1 ) {
		if (gtk_tree_model_get_iter_first (var_file_wav.Adr_Tree_Model, &iter)) {
			gtk_tree_selection_select_iter (var_file_wav.Adr_Line_Selected, &iter);
			BoolNumLineSelected = TRUE;
		}
	}
	if( NumLineSelected > 0 && NULL != entetefile && BoolNumLineSelected == FALSE ) {
		gtk_tree_selection_select_iter (var_file_wav.Adr_Line_Selected, &iter);
	}

	// REAJUSTEMENT DE LA LISTE
	// 
	gtk_adjustment_set_value (Adj, AdjValue);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (var_file_wav.Adr_scroll), Adj);

	FileWav_set_flag_buttons ();
}
// CHANGEMENT DES PARAMETRES: freq, voie, ou bits SI <> DE NULL
// 
void FileWav_set_change_parameters_is_selected (gchar *freq, gchar *voie, gchar *bits)
{
	GtkTreeIter	iter;
	GtkTreeModel	*model = NULL;
	GtkTreePath	*path;
	DETAIL		*detail = NULL;
	GList		*list = NULL;
	gchar		*Str = NULL;
	INFO_WAV	*info = NULL;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(var_file_wav.Adr_TreeView));
	list = gtk_tree_selection_get_selected_rows (var_file_wav.Adr_Line_Selected, &model);
	list = g_list_first (list);
	while (list) {
		if (NULL != (path = list->data)) {
			/* prend le numero de la structure
			*/
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_wav.Adr_Tree_Model, &iter, COLUMN_FILEWAV_POINTER_STRUCT, &detail, -1);
			if (NULL == detail) {
				list = g_list_next (list);
				continue;
			}
			if (NULL == (info = (INFO_WAV *)detail->info)) {
				list = g_list_next (list);
				continue;
			}
						
			/* Et mettre le contenu a jour
			*/
			if (NULL != freq) {
				if (info->NewHertz) {
					g_free (info->NewHertz);
					info->NewHertz = NULL;
				}
				// VALEURS PAR DEFAUT
				if (0 == strcmp (freq, "-1")) {
					info->NewHertz = g_strdup (info->hertz);
				} else {
					info->NewHertz = g_strdup (freq);
				}

				if (strcmp (info->hertz, info->NewHertz) == 0)
					Str = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewHertz);
				else	Str = g_strdup_printf ("<span color=\"red\"><b>%s</b></span>", info->NewHertz);
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_NEW_HERTZ, Str, -1);
				g_free (Str);	Str = NULL;
			}
			if (NULL != voie) {
				if (info->NewVoie) {
					g_free (info->NewVoie);
					info->NewVoie = NULL;
				}
				// VALEURS PAR DEFAUT
				if (0 == strcmp (voie, "-1")) {
					info->NewVoie = g_strdup (info->voie);
				} else {
					info->NewVoie = g_strdup (voie);
				}

				if (0 == strcmp (info->voie, info->NewVoie))
					Str = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewVoie);
				else	Str = g_strdup_printf ("<span color=\"red\"><b>%s</b></span>", info->NewVoie);
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_NEW_VOIE, Str, -1);
				g_free (Str);	Str = NULL;
			}
			if (NULL != bits) {
				if (info->NewBits) {
					g_free (info->NewBits);
					info->NewBits = NULL;
				}
				// VALEURS PAR DEFAUT
				if (0 == strcmp (bits, "-1")) {
					info->NewBits = g_strdup (info->bits);
				} else {
					info->NewBits = g_strdup (bits);
				}

				if (strcmp (info->bits, info->NewBits) == 0)
					Str = g_strdup_printf ("<span color=\"forestgreen\"><b>%s</b></span>", info->NewBits);
				else	Str = g_strdup_printf ("<span color=\"red\"><b>%s</b></span>", info->NewBits);
				gtk_list_store_set (var_file_wav.Adr_List_Store, &iter, COLUMN_FILEWAV_NEW_BITS, Str, -1);
				g_free (Str);	Str = NULL;
			}
			
			info->BoolConv = FALSE;
			if ( atoi (info->hertz) != atoi (info->NewHertz) ||
			     atoi (info->voie) != atoi (info->NewVoie) ||
			     atoi (info->bits) != atoi (info->NewBits)) {
			
				info->BoolConv = TRUE;
			}
		}
		list = g_list_next (list);
	}
	FileWav_set_flag_buttons ();
}
// 
// 
void FileWav_changed_selection_row (GtkTreeSelection *selection, gpointer data)
{
	var_file_wav.Adr_Line_Selected = selection;
	FileWav_set_flag_buttons ();
}
// AFFICHAGE DU NOM COMPLET DU FICHIER SI SURVOL PAR LE CURSEUR SOURIS DU CHAMPS 'Nom'
// 
gboolean FileWav_event (GtkWidget *treeview, GdkEvent *event, gpointer user_data)
{
	gint                x, y;
	GdkModifierType     state;
	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)user_data;
	DETAIL             *detail = NULL;
	gint                Pos_X = 0, Pos_Y = 0;
	gint                i;
	gboolean            BoolSelectColNom = FALSE;
	gboolean            BoolSelectColFREQUENCE = FALSE;
	gboolean            BoolSelectColPISTE = FALSE;
	gboolean            BoolSelectColQUANTIFICATION = FALSE;
	
	/* Si pas de selection a cet endroit retour */
	GdkDeviceManager *manager = gdk_display_get_device_manager( gdk_display_get_default() );
	GdkDevice		 *device = gdk_device_manager_get_client_pointer( manager );
	gdk_window_get_device_position( ((GdkEventButton*)event)->window, device, &x, &y, &state );
	// gdk_window_get_pointer (((GdkEventButton*)event)->window, &x, &y, &state);
	if (FALSE == gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					   x, y,
					   &path, &column, &Pos_X, &Pos_Y)) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, " ");
		StatusBar_puts( );
		return (FALSE);
	}
	
	// RECUPERATION DE LA STRUCTURE POINTEE PAR LE CURSEUR SOURIS
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_file_wav.Adr_Tree_Model, &iter, COLUMN_FILEWAV_POINTER_STRUCT, &detail, -1);
	if (NULL == detail) {
		return (FALSE);
	}
	
	// DANS TOUS LES CAS, EFFACE LA BARRE DE TACHE
	StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, "");

	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_FILEWAV_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			switch ( i ) {
			case NUM_TREE_FILEWAV_FREQUENCE :	BoolSelectColFREQUENCE		= TRUE;	break;
			case NUM_TREE_FILEWAV_PISTE :		BoolSelectColPISTE		= TRUE;	break;
			case NUM_TREE_FILEWAV_QUANTIFICATION :	BoolSelectColQUANTIFICATION	= TRUE;	break;
			case NUM_TREE_FILEWAV_Nom :		BoolSelectColNom		= TRUE;	break;
			// default: return (FALSE);
			default: break;
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}
	if (TRUE == BoolSelectColFREQUENCE) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right Click = Menu) / Select frequency"));
	}
	else if (TRUE == BoolSelectColPISTE) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right Click = Menu) / Select number of tracks (channels)"));
	}
	else if (TRUE == BoolSelectColQUANTIFICATION) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, _("(Right Click = Menu) / Encoding selection in number of bits"));
	}
	else if (TRUE == BoolSelectColNom) {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, detail->namefile);
	}
	else {
		StatusBar_set_mess( NOTEBOOK_FICHIERS, _STATUSBAR_SIMPLE_, " ");
	}
	StatusBar_puts( );
	
	return (FALSE);
}
// 
// 
static void FileWav_drag_data_received(
					GtkWidget        *widget,
					GdkDragContext   *context,
					gint              x,
					gint              y,
					GtkSelectionData *data,
					guint             info,
					guint             time,
					gpointer          user_data)
{
        /* Une copie ne peut aller vers elle meme !!! */
	if (gtk_drag_get_source_widget(context) != widget) {
		// dragndrop_list_drag_data (widget, (gchar *)data->data);
		dragndrop_list_drag_data( widget, (gchar*)gtk_selection_data_get_data( data ));
	}
}
// 
// 
static void FileWav_drag_data_drop (GtkWidget *widget,
					GdkDragContext *dc,
					GtkSelectionData *selection_data,
					guint info,
					guint t,
					gpointer data)
{
	GtkTreeIter       iter;
	GtkTreeModel     *model = NULL;
	GList            *begin_list = NULL;
	GList            *list = NULL;
	GtkTreePath      *path;
	DETAIL           *detail = NULL;
	gchar            *text = NULL;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW(widget));
	begin_list = gtk_tree_selection_get_selected_rows (var_file_wav.Adr_Line_Selected, &model);
	list = g_list_first (begin_list);
	while (list) {
		if ((path = list->data)) {
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (var_file_wav.Adr_Tree_Model, &iter, COLUMN_FILEWAV_POINTER_STRUCT, &detail, -1);
			
			// DEBUG DRAG AND DROP
			// [ Tue, 03 May 2011 17:39:08 +0200 ]
			// XCFA-4.1.0
			// -----------------------------------------------------------
			// OLD CODE:
			// 	text = g_strdup( detail->namefile );
			// NEW_CODE:
			text = g_strdup_printf( "file://%s", detail->namefile );
			
			gdk_drag_status (dc, GDK_ACTION_COPY, t); 
			
			gtk_selection_data_set( selection_data,
						// GDK_SELECTION_TYPE_STRING,
						// selection_data->target,
						GDK_POINTER_TO_ATOM(gtk_selection_data_get_data( selection_data )),
						8,
						(guchar *)text,
						strlen( text )
						);
			g_free (text);
			text = NULL;
		}
		list = g_list_next (list);
	}
}
// 
// 
gboolean FileWav_key_press_event (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	if( TRUE == keys.BoolGDK_Control_A ) {	// CONTROL_A
		gtk_tree_selection_unselect_all (var_file_wav.Adr_Line_Selected);
		gtk_tree_selection_select_all (var_file_wav.Adr_Line_Selected);
	}
	if (keys.keyval == GDK_KEY_Delete) {
		GtkTreeIter   iter;
		if (gtk_tree_model_get_iter_first (var_file_wav.Adr_Tree_Model, &iter)) {
			on_file_button_del_file_clicked (NULL, NULL);
			return (FALSE);
		}
	}
	return (TRUE);
}
// 
// 
gboolean FileWav_event_click_mouse (GtkWidget *treeview, GdkEventButton *event, gpointer data)
{
	gboolean            BoolSelectColFREQUENCE = FALSE;
	gboolean            BoolSelectColPISTE = FALSE;
	gboolean            BoolSelectColQUANTIFICATION = FALSE;

	GtkTreePath        *path;
	GtkTreeViewColumn  *column;
	GtkTreeViewColumn  *ColumnDum;
	gint                i;
	
	GtkTreeIter         iter;
	GtkTreeModel       *model = (GtkTreeModel *)data;
	// GdkPixbuf          *Pixbuf = NULL;
	DETAIL             *detail = NULL;
	gint                Pos_X = 0, Pos_Y = 0;
	// gboolean            bool_key_Control = (keys.keyval == GDK_KEY_Control_L || keys.keyval == GDK_KEY_Control_R);
	// gboolean            bool_key_Shift   = (keys.keyval == GDK_KEY_Shift_L || keys.keyval == GDK_KEY_Shift_R);
	// gboolean            bool_key_Release = (bool_key_Control == FALSE &&  bool_key_Shift == FALSE);
	// gboolean            bool_click_gauche = (event->button == 1);
	gboolean            bool_click_droit = (event->button == 3);	
	
	/* Single clicks only */
	if (event->type != GDK_BUTTON_PRESS) return (FALSE);

	/* Si pas de selection a cet endroit retour */
	if (!gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(treeview),
					  (gint)event->x, (gint)event->y,
					   &path, &column, &Pos_X, &Pos_Y)) return (FALSE);

	// RECCPERATION DE LA STRUCTURE
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (var_file_wav.Adr_Tree_Model, &iter, COLUMN_FILEWAV_POINTER_STRUCT, &detail, -1);
	if (NULL == detail) return (FALSE);
	
	/* IDEE POUR REMPLACER LES COMPARAISON PAR NOMS. EXEMPLES:
	 * 	PLAY	= 0
	 * 	TRASH	= 1
	 *	TYPE	= 2
	 * 	etc ...
	 * NOTA:
	 * 	CET ALGO PERMET DE RENOMMER AISEMENT LES ENTETES DE COLONNES DANS TOUTES LES LANGUES: FR, EN, DE, ...
	 */
	for (i = 0; i < NUM_TREE_FILEWAV_ALL_COLUMN; i ++) {
		ColumnDum = gtk_tree_view_get_column (GTK_TREE_VIEW(treeview), i);
		if (ColumnDum == column) {
			/* g_print ("\tNUM IS: %d\n", i); */
			switch ( i ) {
			case NUM_TREE_FILEWAV_FREQUENCE :	BoolSelectColFREQUENCE		= TRUE;	break;
			case NUM_TREE_FILEWAV_PISTE :		BoolSelectColPISTE		= TRUE;	break;
			case NUM_TREE_FILEWAV_QUANTIFICATION :	BoolSelectColQUANTIFICATION	= TRUE;	break;
			default: return (FALSE);
			}
			/* La colonne est trouvee ... sortie de la boucle */
			break;
		}
	}
	
	if (bool_click_droit && BoolSelectColFREQUENCE) {
		
		popup_file_wav_frequence (detail);

		// AUTORISE LE POPUP SUR UNE SELECTION MULTIPLE
		var_file_wav.Adr_Line_Selected = gtk_tree_view_get_selection( GTK_TREE_VIEW(treeview) );
		gtk_tree_selection_select_path( var_file_wav.Adr_Line_Selected, path );
		return TRUE;
	}
	else if (bool_click_droit && BoolSelectColPISTE) {
		
		popup_file_wav_piste (detail);

		// AUTORISE LE POPUP SUR UNE SELECTION MULTIPLE
		var_file_wav.Adr_Line_Selected = gtk_tree_view_get_selection( GTK_TREE_VIEW(treeview) );
		gtk_tree_selection_select_path( var_file_wav.Adr_Line_Selected, path );
		return TRUE;
	}
	else if (bool_click_droit && BoolSelectColQUANTIFICATION) {
		
		popup_file_wav_quantification (detail);

		// AUTORISE LE POPUP SUR UNE SELECTION MULTIPLE
		var_file_wav.Adr_Line_Selected = gtk_tree_view_get_selection( GTK_TREE_VIEW(treeview) );
		gtk_tree_selection_select_path( var_file_wav.Adr_Line_Selected, path );
		return TRUE;
	}

	return (FALSE);
}
// 
// 
static void FileWav_add_columns_scrolledwindow (GtkTreeView *treeview)
{
	GtkTreeModel      *model = gtk_tree_view_get_model (treeview);
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;

	// SIGNAL : 'event'
	g_signal_connect(G_OBJECT(treeview),
			 "event",
                    	 (GCallback) FileWav_event,
			 model);

	// SIGNAL : 'Gestion click click'
	g_signal_connect(G_OBJECT(treeview),
			 "button-press-event",
                    	 (GCallback) FileWav_event_click_mouse,
			 model);
	
	// SIGNAL : Ligne actuellement selectionnee 'changed'
	var_file_wav.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	g_signal_connect(G_OBJECT(var_file_wav.Adr_Line_Selected),
			 "changed",
                   	 G_CALLBACK(FileWav_changed_selection_row),
                   	 "1");
	
	// SIGNAL 'key-press-event'
	g_signal_connect(G_OBJECT(treeview),
			"key-press-event",
			(GCallback) FileWav_key_press_event,
			model);
	
	// Drag and drop support
	// SIGNAL : 'drag-data-received'
	gtk_drag_dest_set (GTK_WIDGET (treeview),
			   GTK_DEST_DEFAULT_MOTION |
			   GTK_DEST_DEFAULT_DROP,
			   drag_types, n_drag_types,
			   GDK_ACTION_COPY| GDK_ACTION_MOVE );
	g_signal_connect(G_OBJECT(treeview),
			 "drag-data-received",
			 G_CALLBACK(FileWav_drag_data_received),
			 NULL);

	gtk_drag_source_set(
			GTK_WIDGET(treeview),
			GDK_BUTTON1_MASK | GDK_BUTTON2_MASK | GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP,
			drag_types, n_drag_types,
			GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_DEFAULT
			);

	g_signal_connect(G_OBJECT(treeview),
      			"drag-data-get",
			 G_CALLBACK(FileWav_drag_data_drop),
			 treeview);
	
	// COLUMN_FILEWAV_HERTZ
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_wav.Adr_ColumnFileWavHertz =
	column = gtk_tree_view_column_new_with_attributes (_("Frequency"),
						     renderer,
						     "text", COLUMN_FILEWAV_HERTZ,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 100);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	/* TRIS */
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_FILEWAV_HERTZ);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEWAV_NEW_HERTZ
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_wav.Adr_ColumnFileWavNewHertz =
	column = gtk_tree_view_column_new_with_attributes (_("FREQUENCY"),
						     renderer,
						     "markup", COLUMN_FILEWAV_NEW_HERTZ,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 90);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEWAV_VOIE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_wav.Adr_ColumnFileWavVoie =
	column = gtk_tree_view_column_new_with_attributes (_("Track"),
						     renderer,
						     "text", COLUMN_FILEWAV_VOIE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 100);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	/* TRIS */
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_FILEWAV_VOIE);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEWAV_NEW_VOIE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_wav.Adr_ColumnFileWavNewVoie =
	column = gtk_tree_view_column_new_with_attributes (_("TRACK"),
						     renderer,
						     "markup", COLUMN_FILEWAV_NEW_VOIE,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 100);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEWAV_BITS
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_wav.Adr_ColumnFileWavBits =
	column = gtk_tree_view_column_new_with_attributes (_("Quantification"),
						     renderer,
						     "text", COLUMN_FILEWAV_BITS,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 120);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	/* TRIS */
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_FILEWAV_BITS);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEWAV_NEW_BITS
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_wav.Adr_ColumnFileWavNewBits =
	column = gtk_tree_view_column_new_with_attributes (_("QUANTIFICATION"),
						     renderer,
						     "markup", COLUMN_FILEWAV_NEW_BITS,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 115);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// COLUMN_FILEWAV_NAME
	var_file_wav.Renderer =
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	var_file_wav.Adr_ColumnFileWavName =
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
						     renderer,
						     "markup", COLUMN_FILEWAV_NAME,
						     // "background-gdk", COLUMN_FILEWAV_COLOR,
						     NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
}
// 
// 
void on_scrolledwindow_file_wav_realize (GtkWidget *widget, gpointer user_data)
{
	GtkListStore *store;
	GtkTreeModel *model;
	GtkWidget    *treeview;
	
	var_file_wav.Adr_scroll = widget;
	
	var_file_wav.Pixbuf_NotInstall             = libutils_init_pixbufs ("not_install.png");
	
	var_file_wav.Adr_List_Store = store =
	gtk_list_store_new (	COLUMN_FILE_NUM,	/* TOTAL NUMBER				*/
				G_TYPE_STRING,		/* COLUMN_FILEWAV_HERTZ			*/
				G_TYPE_STRING,		/* COLUMN_FILEWAV_NEW_HERTZ		*/
				G_TYPE_STRING,		/* COLUMN_FILEWAV_VOIE			*/
				G_TYPE_STRING,		/* COLUMN_FILEWAV_NEW_VOIE		*/
				G_TYPE_STRING,		/* COLUMN_FILEWAV_BITS			*/
				G_TYPE_STRING,		/* COLUMN_FILEWAV_NEW_BITS		*/
				G_TYPE_STRING,		/* COLUMN_FILEWAV_NAME			*/
				// GDK_TYPE_COLOR,		/* COLUMN_FILEWAV_COLOR			*/
				G_TYPE_POINTER          /* COLUMN_FILEWAV_POINTER_STRUCT	*/
			   );
	var_file_wav.Adr_Tree_Model = model = GTK_TREE_MODEL (store);
	var_file_wav.Adr_TreeView =
	treeview = gtk_tree_view_new_with_model (model);
	// gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview)), GTK_SELECTION_MULTIPLE);	// GTK_SELECTION_BROWSE MULTIPLE
	g_object_unref (model);
	gtk_container_add (GTK_CONTAINER (widget), treeview);
	FileWav_add_columns_scrolledwindow (GTK_TREE_VIEW (treeview));
	gtk_widget_show_all (widget);
}
// 
// 
void FileWav_from_popup_set_data (gchar *StrFreq, gchar *StrVoie, gchar *StrBits)
{
	// TRAITEMENT DE LA CELLULE POINTEE PAR LA STRUCTURE DETAIL
		
	// VALEUR PAR DEFAUT SUR LA CELLULE POINTEE PAR LA STRUCTURE DETAIL
	// 	EX: -1, NULL, NULL
	// ASSIGNEMENT DU POINTEUR NON NULL SUR LA CELLULE POINTEE PAR LA STRUCTURE DETAIL
	// 	EX: 44100, NULL, NULL
	// 
	FileWav_set_change_parameters_is_selected (StrFreq, StrVoie, StrBits);
}
// 
// 
void FileWav_from_popup (TYPE_SET_FROM_POPUP_FILE TypeSetFromPopup, DETAIL *detail, gint freq, gint voie, gint bits)
{
	gchar *StrFreq = NULL;
	gchar *StrVoie = NULL;
	gchar *StrBits = NULL;

	if (freq >= -1) StrFreq = g_strdup_printf ("%d", freq);
	if (voie >= -1) StrVoie = g_strdup_printf ("%d", voie);
	if (bits >= -1) StrBits = g_strdup_printf ("%d", bits);
	
	switch (TypeSetFromPopup) {
	
	// SELECTION ou DESELECTION POUR LES ICONES DE CONVERSIONS
	
	case FILE_CONV_DESELECT_ALL :			// Deselection globale
	case FILE_CONV_DESELECT_V :			// Deselection verticale
	case FILE_CONV_DESELECT_H :			// Deselection horizontale
	case FILE_CONV_SELECT_V :			// Selection verticale
	case FILE_CONV_SELECT_EXPERT_V :		// Selection Expert verticale
	case FILE_CONV_SELECT_H :			// Selection horizontale
	case FILE_CONV_SELECT_EXPERT_H :		// Selection Expert horizontale
	
	// SELECTION ou DESELECTION POUR LES ICONES DE REPLAYGAIN
	
	case FILE_REPLAYGAIN_DESELECT_V :		// Deselection verticale
	case FILE_REPLAYGAIN_SELECT_PISTE :		// Selection PISTE
	case FILE_REPLAYGAIN_SELECT_ALBUM :		// Selection ALBUM
	case FILE_REPLAYGAIN_SELECT_NETTOYER :		// Selection NETTOYER
	
	// SELECTION ou DESELECTION POUR TRASH
	
	case FILE_TRASH_DESELECT_V :			// Deselection verticale
	case FILE_TRASH_SELECT_V :			// Selection verticale
		break;
	
	// SELECTION ou DESELECTION POUR FREQUENCES VOIE BITS 
	
	case FILEWAV_FREQUENCY_CELL_HERTZ :		// Frequence Cellule ou valeur d origine
		FileWav_from_popup_set_data (StrFreq, StrVoie, StrBits);
		break;
	case FILEWAV_TRACK_CELL :			// Voie Cellule ou valeur d origine
		FileWav_from_popup_set_data (StrFreq, StrVoie, StrBits);
		break;
	case FILEWAV_QUANTIFICATION_CELL :		// Bits Cellule ou valeur d origine
		FileWav_from_popup_set_data (StrFreq, StrVoie, StrBits);
		break;
	}
	
	if (NULL != StrFreq)	{ g_free (StrFreq);	StrFreq = NULL; }
	if (NULL != StrVoie)	{ g_free (StrVoie);	StrVoie = NULL; }
	if (NULL != StrBits)	{ g_free (StrBits);	StrBits = NULL; }
}








