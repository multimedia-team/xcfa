 /*
 *  file      : options_cd.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "cd_audio.h"
#include "parse.h"
#include "options.h"




void on_radiobutton_choice_extract_with_clicked (GtkButton *button, gpointer user_data)
{
	if (GTK_BUTTON (var_options.Adr_radiobutton_cdparanoia_mode_expert) == button)		Config.ExtractCdWith = EXTRACT_WITH_CDPARANOIA_EXPERT;
	else if (GTK_BUTTON (var_options.Adr_radiobutton_extract_with_cdda2wav) == button)	Config.ExtractCdWith = EXTRACT_WITH_CDDA2WAV;
	else if (GTK_BUTTON (var_options.Adr_radiobutton_extract_with_cdparanoia) == button)	Config.ExtractCdWith = EXTRACT_WITH_CDPARANOIA;
	else if (GTK_BUTTON (var_options.Adr_radiobutton_cdparanoia_mode_2) == button)		Config.ExtractCdWith = EXTRACT_WITH_CDPARANOIA_MODE_2;
}
void on_radiobutton_extract_with_cdparanoia_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_radiobutton_extract_with_cdparanoia = widget;
	if (Config.ExtractCdWith == EXTRACT_WITH_CDPARANOIA)
		gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_extract_with_cdparanoia), TRUE);
}
void on_radiobutton_extract_with_cdda2wav_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_radiobutton_extract_with_cdda2wav = widget;
	if (Config.ExtractCdWith == EXTRACT_WITH_CDDA2WAV)
		gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_extract_with_cdda2wav), TRUE);
}
void on_radiobutton_cdparanoia_mode_expert_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_radiobutton_cdparanoia_mode_expert = widget;
	if (Config.ExtractCdWith == EXTRACT_WITH_CDPARANOIA_EXPERT)
		gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_cdparanoia_mode_expert), TRUE);
}
void on_radiobutton_cdparanoia_mode_2_realize (GtkWidget *widget, gpointer user_data)
{
	var_options.Adr_radiobutton_cdparanoia_mode_2 = widget;
	if (Config.ExtractCdWith == EXTRACT_WITH_CDPARANOIA_MODE_2)
		gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (var_options.Adr_radiobutton_cdparanoia_mode_2), TRUE);
}
// 
// 
void OptionsCd_set_entry_and_label( void )
{
	gchar		*PtrEntryStockageCdAudio = NULL;
	gchar		*Ptr = NULL;
	gchar		*Dummy = NULL;
	CD_AUDIO	*Audio = NULL;
	GString		*gstr = NULL;
	
	// VERIFICATION SI PARAMS entry AND label
	if (NULL == var_cd.Adr_entry_stockage_cdaudio || NULL == var_cd.Adr_label_stockage_cdaudio) return;
	
	// RECUP POINTEUR PARAM entry
	PtrEntryStockageCdAudio = (gchar *)gtk_entry_get_text (GTK_ENTRY (var_cd.Adr_entry_stockage_cdaudio));
	
	// SAVE CONFIG
	if (NULL != Config.Templates_rep_cdaudio) {
		g_free (Config.Templates_rep_cdaudio);
		Config.Templates_rep_cdaudio = NULL;
	}
	Config.Templates_rep_cdaudio = g_strdup( PtrEntryStockageCdAudio );
		
	// SI CATALOGUE CD NON LU -> MESS ERREUR ET RETOUR
	if( NULL == ( Audio = cdaudio_get_line_selected_for_extract()) ) {
		gstr = g_string_new (NULL);
		// PATH
		g_string_append_printf (gstr,
				"<b>%s/<span color=\"blue\">",
				(gchar *)Config.PathDestinationCD
				);
		// CONVERSION UTF8 POUR LE MARKUP
		Ptr = utf8_eperluette_name( PtrEntryStockageCdAudio );
		g_string_append_printf (gstr,
				"%s"
				"</span></b>\n"
				"<span color=\"red\">",
				Ptr
				);
		g_string_append_printf (gstr,
				_("To assess the templates, you must enable a reading cdaudio")
				);
		g_string_append_printf (gstr,
				"</span>"
				);
		// AFFICHAGE
		gtk_label_set_markup (GTK_LABEL (var_cd.Adr_label_stockage_cdaudio), gstr->str);
		
		// FREE MEMORY
		g_string_free (gstr, TRUE);
		g_free( Ptr );
		Ptr = NULL;
		// bool_entree_cd_verif_car = TRUE;
		
		// ET RETOUR
		return;
	}
	
	// CONFIGURE label POUR LE MARKUP
	gtk_label_set_markup (GTK_LABEL (var_cd.Adr_label_stockage_cdaudio), "");
	gstr = g_string_new (NULL);
	g_string_append_printf (gstr, "<b>%s/<span color=\"blue\">", (gchar *)Config.PathDestinationCD);
	Parse_entry( PARSE_TYPE_STOCKAGE_CD );
	Dummy = Parse_get_line( PARSE_TYPE_STOCKAGE_CD, Audio->Num_Track -1 );
	Ptr = utf8_eperluette_name( Dummy );
	
	g_string_append_printf (gstr, "%s", Ptr );
	g_string_append_printf (gstr, "</span></b>" );
	
	gtk_label_set_markup (GTK_LABEL (var_cd.Adr_label_stockage_cdaudio), gstr->str);
	g_free( Ptr );
	Ptr = NULL;
	g_free( Dummy );
	Dummy = NULL;
}
// 
void on_entry_stockage_cdaudio_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.bool_enter_parse = TRUE;
	var_cd.Adr_entry_stockage_cdaudio = widget;
	if( NULL != Config.Templates_rep_cdaudio ) {
		gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_stockage_cdaudio), Config.Templates_rep_cdaudio);
		OptionsCd_set_entry_and_label();
	}
}
// 
// Saisie  PREFERENCES -> CD -> DOSSIER DE STOCKAGE
// 
void on_entry_stockage_cdaudio_changed (GtkEditable *editable, gpointer user_data)
{
	if( FALSE == var_cd.bool_enter_parse ) return;
	var_cd.bool_enter_parse = FALSE;
	OptionsCd_set_entry_and_label();
	var_cd.bool_enter_parse = TRUE;
	
	// PARSING
	//		ARRANGEMENT DES TITRES DU CD		DOSSIER DE STOCKAGE
	//		-----------------------------------------------------------
	//	%a	ARTISTE					ARTISTE
	//	%b	TITRE DE L ALBUM			TITRE DE L ALBUM
	//	%c	NUMERO DE LA PISTE			NUMERO DE LA PISTE
	//	%d	TITRE DE LA CHANSON			TITRE DE LA CHANSON
	//	%e	ANNEE					ANNEE
	//	%g	GENRE					GENRE
	//	%f	CREATION DES FICHIERS m3u et xpm	
	//	%na	NO ACCENT				
	//	%u	REMPLACEMENT DE CARACTERES		
	//	%Tl	TETSUMAKI LOWER				
}
// 
// 
void on_label_stockage_cdaudio_exemple_realize (GtkWidget *widget, gpointer user_data)
{
	var_cd.Adr_label_stockage_cdaudio = widget;
	if (var_cd.Adr_entry_stockage_cdaudio) {
		if( NULL != Config.Templates_rep_cdaudio ) {
			gtk_entry_set_text (GTK_ENTRY (var_cd.Adr_entry_stockage_cdaudio), Config.Templates_rep_cdaudio);
			OptionsCd_set_entry_and_label();
		}
	}
}


// 
// 
void on_checkbutton_log_cdparanoia_mode_expert_realize( GtkWidget *widget, gpointer user_data )
{
	var_cd.Adr_checkbutton_log_cdparanoia_mode_expert = widget;
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON(widget), Config.BoolLogCdparanoiaModeExpert );
}
// 
// 
void on_checkbutton_log_cdparanoia_mode_expert_clicked( GtkButton *button, gpointer user_data )
{
	Config.BoolLogCdparanoiaModeExpert = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(var_cd.Adr_checkbutton_log_cdparanoia_mode_expert)) ? TRUE : FALSE;
}
// 
// 
gboolean OptionsCd_get_save_log_mode_expert( void )
{
	return( Config.BoolLogCdparanoiaModeExpert );
}








