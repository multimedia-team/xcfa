 /*
 *  file      : file.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef file_h
#define file_h 1

typedef struct {
	gboolean		 bool_tag_cd;						// TRUE is CD-AUDIO else is FILE
	gboolean		 bool_save;							// TRUE -> save
	gchar			*Album;								// %b
	gchar			*Artist;							// %a
	gchar			*Title;								// %d
	gchar			*Number;							// %c
	gint			 IntNumber;							//
	gchar			*Genre;								// %g
	gint			 IntGenre;							//
	gchar			*Year;								// %e
	gint			 IntYear;							//
	gchar			*Comment;							//
	gchar			*Description;						//
} TAGS;

//
// ---------------------------------------------------------------------------
//  I N F O _
// ---------------------------------------------------------------------------
//

typedef struct {
	gint			 level;								// Niveau en dBFS
	gint			 NewLevel;							// Nouveau niveau en dBFS
} LEVEL_DBFS;

typedef struct {
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	gchar			*hertz;								//
	TAGS			*tags;								// Tags
} INFO_M4A;

typedef struct {
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	gchar			*hertz;								//
	TAGS			*tags;								// Tags
} INFO_AAC;

typedef struct {
	gboolean		 BoolBwf;							// 'fmt ', 'bext', 'qlty', 'levl', 'link','axml'
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	gchar			*hertz;								// Taux d'échantillonnage en hertz( qualité du son)
	gchar			*voie;								// 1, 2, 4, 6
	gchar			*bits;								// 8, 16, ,24, 32 ou 64
	gchar			*NewHertz;							// Nouvelle valeur
	gchar			*NewVoie;							// Nouvelle valeur
	gchar			*NewBits;							// Nouvelle valeur
	gboolean		 BoolConv;							// TRUE si nouvelles valeurs differenetes de anciennes valeurs
	TAGS			*tags;								// Tags
	LEVEL_DBFS		 LevelDbfs;							//
} INFO_WAV;

typedef enum {											// type:     bitrate:
	NONE_MPEG = -1,
	MPEG_1 = 0,											// MPV_1     [ 32 40 48 56 64 80 96 112 128 160 192 224 256 320 ]
	MPEG_2,												// MPV_2     [  8 16 24 32 40 48 56  64  80  96 112 128 144 160 ]
	MPEG_25												// MPV_25    [  8 16 24 32 40 48 56  64  80  96 112 128 144 160 ]
} MPEG_IS;

typedef struct {
	gchar			*bitrate;							//
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	MPEG_IS			 mpeg_is;							//
	gchar			*size;								//
	TAGS			*tags;								// Tags
	LEVEL_DBFS		 LevelDbfs;							//
} INFO_MP3;

typedef struct {
	gchar			*Channels;							//
	gchar			*Rate;								//
	gchar			*Nominal_bitrate;					//
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	gchar			*size;								//
	TAGS			*tags;								// Tags
	LEVEL_DBFS		 LevelDbfs;							// Datas du dBFS
} INFO_OGG;

typedef struct {
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	TAGS			*tags;								// Tags
} INFO_FLAC;

typedef struct {
	TAGS			*tags;								// Tags
} INFO_WMA;

typedef struct {
	TAGS			*tags;								// Tags
} INFO_RM;

typedef struct {
	TAGS			*tags;								// Tags
} INFO_DTS;

typedef struct {
	TAGS			*tags;								// Tags
} INFO_AIFF;

typedef struct {
	gchar			*namefile;							// Nom complet du fichier
	TAGS			*tags;								// Tags
} INFO_MPC;

typedef struct {
	TAGS			*tags;								// Tags
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	gchar			*size;								//
} INFO_SHN;

typedef struct {
	TAGS			*tags;								// Tags
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	gchar			*size;								//
} INFO_APE;

typedef struct {
	TAGS			*tags;								// Tags
	gchar			*time;								// Temps d'ecoute
	guint			 SecTime;							// Temps d'ecoute en secondes
	gchar			*size;								//
} INFO_WAVPACK;

typedef struct {
	TAGS			*tags;								// Tags
} INFO_AC3;

/*
	M P 3

	TYPE:	ABR
	DEBIT:	32 40 48 56 64 80 96 112 128 160 192 224 320
	MODE:	Defaut / Stereo / Join Stereo / Forced Join Stereo / Duo Channels / Mono

	TYPE:	CBR
	DEBIT:	32 40 48 56 64 80 96 112 128 160 192 224 320
	MODE:	Defaut / Stereo / Join Stereo / Forced Join Stereo / Duo Channels / Mono

	TYPE:	VBR
	DEBIT:	preset medium / preset standard / preset extreme / preset fast standard / preset fast extreme / V0 V1 V2 V3 V4 V5 V6 V7 V8 V9
	MODE:	Defaut / Stereo / Join Stereo / Forced Join Stereo / Duo Channels / Mono

	TYPE:	VBR-NEW
	DEBIT:	NONE / preset medium / preset standard / preset extreme / preset fast standard / preset fast extreme / V0 V1 V2 V3 V4 V5 V6 V7 V8 V9
	MODE:	Defaut / Stereo / Join Stereo / Forced Join Stereo / Duo Channels / Mono

	O G G

	DEBIT:		45  kbit/s / 64  kbit/s / 80  kbit/s / 96  kbit/s / 112  kbit/s / 128  kbit/s / 160  kbit/s / 192  kbit/s / 224  kbit/s / 256  kbit/s / 320  kbit/s /
			Qualite -1 -Moins bonne qualite / Qualite  0 / Qualite  1 / Qualite  2 / Qualite  3 -Defaut / Qualite  4 / Qualite  5 / Qualite  6 / Qualite  7 / Qualite  8 / Qualite  9 / Qualite  10 -Meilleure qualite
	MANAGED:	Oui / Non
	DOWNMIX:	Mono force / Stereo
*/

typedef struct {
	gboolean		 Bool_Conv;							// TRUE si conversion
	gchar			*destwav;							// Temporaire: Pathname
	gchar			*destsox;							// Temporaire: Pathname
	gchar			*destmp3;							// Temporaire: Pathname
	gchar			*destogg;							// Temporaire: Pathname
	gchar			*destfile;							// Destination du fichier apres rectification
	gchar			*Bitrate;							// Le bitrate actuel
} CONV_FIC_MP3OGG;

//
// ---------------------------------------------------------------------------
//  F I C    W A V
// ---------------------------------------------------------------------------
//

typedef struct {
	gchar			*Path;								// Path du fichier source
	gchar			*TmpSrc;							// $TMP/original
	gchar			*TmpDest;							// $TMP/modifie
	gchar			*TmpMplayer;						// $TMP/
	gchar			*NameDest;							// Copie vers la destination
} CONV_FIC_WAV;

//
// ---------------------------------------------------------------------------
//  F I C
// ---------------------------------------------------------------------------
//

typedef enum {
	FILE_IS_NONE = 0,
	FILE_IS_FLAC,
	FILE_IS_WAV,
	// FILE_IS_BFW,										// BROADCAST WAVE FORMAT
	FILE_IS_MP3,
	FILE_IS_OGG,
	/* Les fichiers au format AAC, portant l'extension
		.mp4( pour MPEG-4),
	   	.m4a( pour MPEG-4 Audio)
	   	.m4p( pour MPEG-4 Protégé),
	   sont globalement plus petits que les fichiers au format MP3. */
	FILE_IS_M4A,
	FILE_IS_VID_M4A,
	FILE_IS_AAC,
	FILE_IS_SHN,
	FILE_IS_WMA,
	FILE_IS_RM,
	FILE_IS_DTS,
	FILE_IS_AIFF,
	FILE_IS_MPC,
	FILE_IS_APE,
	FILE_IS_WAVPACK,
	FILE_IS_WAVPACK_MD5,
	FILE_IS_AC3,
	FILE_TO_NORMALISE,
	FILE_TO_NORMALISE_COLLECTIF,
	FILE_TO_REPLAYGAIN
} TYPE_FILE_IS;

typedef enum {
	FILE_NONE = 0,
	FILE_WAITING,
	FILE_CONV_OK
} ETAT_CONV_FILE;

typedef enum {
	FILE_ATTENTE = 0,
	FILE_SELECTED,
	FILE_SELECTED_LC
} ETAT_CONVFILE;

typedef enum {
	VERIF_FILE_NONE = 0,
	VERIF_FILE_BAD,
	VERIF_FILE_OK
} VERIF_FILE;

typedef enum {
	ETAT_SCAN_NONE = 0,
	ETAT_SCAN_DEMANDE,
	ETAT_SCAN_OK
} ETAT_SCAN;

/*
	Intitulé	Correspond à	Valeurs		Fichiers
	PEAK/ALBUM					Choix
	PEAK						Choix
	(scan)RMS/ALBUM	Mix		-6 à -22	Tous ou aucun
	(scan)RMS	Fix		-6 à -24	Tous ou aucun

PEAK/ALBUM
    Amplification maximale du volume pour un groupe de fichiers en respectant les écarts de niveau entre chacun d'eux.
PEAK
    Amplification maximale du volume pour chaque de fichier
RMS/ALBUM
    Ajustement du volume moyen pour un groupe de fichiers en respectant les écarts de niveau moyen entre chacun d'eux
RMS
    Ajustement du volume moyen de chaque de fichier
*/
typedef enum {
	NORM_NONE = 0,
	NORM_READY_FOR_SELECT,								// Pret a la selection wav,mp3, ogg
	NORM_PEAK_ALBUM,									// NORM_PEAK_ALBUM
	NORM_PEAK,											// NORM_PEAK
	NORM_RMS_MIX_ALBUM,									//( scan) NORM_RMS_MIX_ALBUM
	NORM_RMS_FIX										//( scan) NORM_RMS_FIX
} ETAT_NORMALISE;

typedef enum {
	RPG_NONE = 0,										// Attente de selection
	RPG_ATTENTE,										// Attente de selection
	RPG_PISTE,											// Mode PISTE
	RPG_ALBUM,											// Mode ALBUM
	RPG_EFFACER											// Mode EFFACER
} ETAT_REPLAYGAIN;

typedef enum {
	ETAT_PRG_NONE = 0,
	ETAT_PRG_ABSENT,
	ETAT_ATTENTE,
	ETAT_ATTENTE_EXIST,
	ETAT_SELECT,
	ETAT_SELECT_EXIST,
	ETAT_SELECT_EXPERT,
	ETAT_SELECT_EXPERT_EXIST
} ETAT_SELECTION;

typedef enum {
	FILE_ETAT_PLAY_NONE = 0,
	FILE_ETAT_PLAY_PRG_ABSENT,
	FILE_ETAT_PLAY_ATTENTE,
	FILE_ETAT_PLAY
} ETAT_PLAY_FILE;

typedef enum {
	FILE_TRASH_NONE = 0,
	FILE_TRASH_OK,
	FILE_TRASH_VERIF_OK
} ETAT_TRASH_FILE;

enum {
	ETAT_FROM_WAV_TO_FLAC = 0,
	ETAT_FROM_WAV_TO_APE,
	ETAT_BOOL_WAVPACK,
	ETAT_FROM_WAV_TO_OGG,
	ETAT_FROM_WAV_TO_M4A,
	ETAT_FROM_WAV_TO_AAC,
	ETAT_FROM_WAV_TO_MPC,
	ETAT_FROM_WAV_TO_MP3,
	ETAT_BOOL_TOTAL
};


typedef struct {
	ETAT_PLAY_FILE		 EtatPlay;						//
	gchar			*name_dest;							// Nom du fichier de destination sans le path ni l'extension
	gchar			*dest_flac;							//
	gchar			*dest_wav;							//
	gchar			*dest_mp3;							//
	gchar			*dest_ogg;							//
	gchar			*dest_m4a;							//
	gchar			*dest_aac;							//
	gchar			*dest_mpc;							//
	gchar			*dest_ape;							//
	gchar			*dest_wavpack;						//
	gchar			*dest_wavpack_md5;					//
	gchar			*tmp_flac;							//
	gchar			*tmp_wav;							//
	gchar			*tmp_sox;							// wav to wav
	gchar			*tmp_sox_24;						// wav to wav
	gchar			*tmp_mp3;							//
	gchar			*tmp_ogg;							//
	gchar			*tmp_m4a;							//
	gchar			*tmp_aac;							//
	gchar			*tmp_shn;							//
	gchar			*tmp_wma;							//
	gchar			*tmp_rm;							//
	gchar			*tmp_dts;							//
	gchar			*tmp_aiff;							//
	gchar			*tmp_mpc;							//
	gchar			*tmp_ape;							//
	gchar			*tmp_wavpack;						//
	gchar			*tmp_wavpack_md5;					//
	gchar			*tmp_ac3;							//
	gboolean		 BoolEtatConv [ ETAT_BOOL_TOTAL ];
	ETAT_CONV_FILE		 Etat_Flac;						//
	ETAT_CONV_FILE		 Etat_Wav;						//
	ETAT_CONV_FILE		 Etat_Mp3;						//
	ETAT_CONV_FILE		 Etat_Ogg;						//
	ETAT_CONV_FILE		 Etat_m4a;						//
	ETAT_CONV_FILE		 Etat_aac;						//
	ETAT_CONV_FILE		 Etat_Mpc;						//
	ETAT_CONV_FILE		 Etat_Ape;						//
	ETAT_CONV_FILE		 Etat_wavpack;					//
	gboolean		 BoolNewString;						//

} CONV_FIC;

typedef enum {
	STRUCT_NO_REMOVE = 0,								//
	STRUCT_ALL_REMOVE,									// Remove in treeview ALL FILE
	STRUCT_WAV_REMOVE_WAV,								// Remove in treeview WAV
	STRUCT_MP3OGG_REMOVE,								// Remove in treeview MP3OGG
	STRUCT_SPLIT_REMOVE									// Remove in treeview SPLIT
} TYPE_REMOVE_STRUCT;


//
// ---------------------------------------------------------------------------
//  F I C
// ---------------------------------------------------------------------------
//


typedef struct {
	gchar			*namefile;							// Nom complet du fichier
	gchar			*NameFileCopyFromNormalizate;		//
	gchar			*NameDest;							// Nom de la destination
	TYPE_FILE_IS		 type_infosong_file_is;			// Type du fichier
	void 			*info;								// En fonction du type de fichier pointeur sur INFO_xxx
	ETAT_TRASH_FILE		 EtatTrash;						// Etat pour la poubelle
	ETAT_SELECTION		 EtatSelection_Wav;				// Etat de la selection WAV
	ETAT_SELECTION		 EtatSelection_Flac;			// Etat de la selection FLAC
	ETAT_SELECTION		 EtatSelection_Ape;				// Etat de la selection APE
	ETAT_SELECTION		 EtatSelection_WavPack;			// Etat de la selection WAVPACK
	ETAT_SELECTION		 EtatSelection_Ogg;				// Etat de la selection OGG
	ETAT_SELECTION		 EtatSelection_M4a;				// Etat de la selection M4A
	ETAT_SELECTION		 EtatSelection_Aac;				// Etat de la selection AAC
	ETAT_SELECTION		 EtatSelection_Mpc;				// Etat de la selection MPC
	ETAT_SELECTION		 EtatSelection_Mp3;				// Etat de la selection MP3
	ETAT_NORMALISE		 Etat_Normalise;				// Etat de la selection de normalisation
	ETAT_SCAN		 Etat_Scan;							// Etat de la selection du scan
	gint			 LevelMix;							// Niveau du MIX
	ETAT_REPLAYGAIN		 Etat_ReplayGain;				// Etat de la selection du gain
	CONV_FIC		*PConv;								// Pointeur pour conversion de tous les fichiers
	CONV_FIC_WAV		*PConvWav;						// Pointeur pour conversion WAV TO WAV
	CONV_FIC_MP3OGG		*PConvMp3Ogg;					// Pointeur pour modification bitrate mp3 et ogg
	TYPE_REMOVE_STRUCT	 BoolRemove;					// TRUE si suppression de la liste sinon FALSE

	// MP3: DEBIT MODE
	gint			Mp3_Debit;							//
	gint			Mp3_Mode;							//
	// OGG: DEBIT MANAGED DOWNMIX
	gint			Ogg_Debit;							//
	gint			Ogg_Managed;						//
	gint			Ogg_Downmix;						//
	gboolean		BoolChanged;						//
} DETAIL;

extern GList *entetefile;								// pointeur sur DETAIL

//
// ---------------------------------------------------------------------------
//  FILE.C
// ---------------------------------------------------------------------------
//
typedef struct {
	GtkComboBox		*AdrComboboxNormalise;				// Adresse
	GtkSpinButton		*AdrSpinbuttonNormalise;		// Adresse
	GtkWidget		*Adr_Label_Number;					// Adresse
	GtkWidget		*Adr_scroll;						// Adresse
	GtkListStore		*Adr_List_Store;				// Adresse
	GtkTreeModel		*Adr_Tree_Model;				// Adresse
	GtkTreeSelection	*Adr_Line_Selected;				// Adresse
	GtkWidget		*Adr_TreeView;						// Adresse
	GtkComboBox		*Adr_combobox_DestFile;				// Adresse
	GtkComboBox		*Adr_combobox_select_type;			// Adresse
	GtkComboBox		*Adr_combobox_normalise;			// Adresse
	GdkPixbuf		*Pixbuf_Trash;						// trash.png
	GdkPixbuf		*Pixbuf_NoTrash;					// coche.png
	GdkPixbuf		*Pixbuf_FilePlay;					// play.png
	GdkPixbuf		*Pixbuf_FileStop;					// sol.png
	GdkPixbuf		*Pixbuf_Coche;						// coche.png
	GdkPixbuf		*Pixbuf_Coche_exist;				// coche_exist.png
	GdkPixbuf		*Pixbuf_Selected;					// selected.png
	GdkPixbuf		*Pixbuf_Selected_exist;				// selected_exist.png
	GdkPixbuf		*Pixbuf_Selected_expert;			// selected_expert.png
	GdkPixbuf		*Pixbuf_Selected_expert_exist;		// selected_expert_exist.png
	GdkPixbuf		*Pixbuf_Normalize_Coche;			// normalize2.png
	GdkPixbuf		*Pixbuf_NotInstall;					// not_install.png
	GtkCellRenderer		*Renderer;
	GtkTreeViewColumn	*Adr_ColumnFilePlay;
	GtkTreeViewColumn	*Adr_ColumnFileTrash;
	GtkTreeViewColumn	*Adr_ColumnFileType;
	GtkTreeViewColumn	*Adr_ColumnFileFlac;
	GtkTreeViewColumn	*Adr_ColumnFileWav;
	GtkTreeViewColumn	*Adr_ColumnFileMp3;
	GtkTreeViewColumn	*Adr_ColumnFileOgg;
	GtkTreeViewColumn	*Adr_ColumnFileM4a;
	GtkTreeViewColumn	*Adr_ColumnFileAac;
	GtkTreeViewColumn	*Adr_ColumnFileMpc;
	GtkTreeViewColumn	*Adr_ColumnFileApe;
	GtkTreeViewColumn	*Adr_ColumnFileWavPack;
	GtkTreeViewColumn	*Adr_ColumnFileTime;
	GtkTreeViewColumn	*Adr_ColumnFileNormalize;
	GtkTreeViewColumn	*Adr_ColumnFileReplayGain;
	GtkTreeViewColumn	*Adr_ColumnFileName;
	gint			 PageNum;
	GdkPixbuf		*Pixbuf_rpg_piste;					// rpg_piste.png
	GdkPixbuf		*Pixbuf_rpg_album;					// rpg_album.png
	GdkPixbuf		*Pixbuf_rpg_effacer;				// rpg_effacer.png
	GdkPixbuf		*Pixbuf_rpg_wait;					// rpg_wait.png

	GdkPixbuf		*Pixbuf_norm_fix;					// norm_fix.png
	GdkPixbuf		*Pixbuf_norm_mix;					// norm_mix.png
	GdkPixbuf		*Pixbuf_norm_peak;					// norm_peak.png
	GdkPixbuf		*Pixbuf_norm_wait;					// norm_wait.png

} VAR_FILE;

extern VAR_FILE var_file;

typedef struct {
	GtkWidget		*Adr_scroll;						// Adresse
	GtkListStore		*Adr_List_Store;				// Adresse
	GtkTreeModel		*Adr_Tree_Model;				// Adresse
	GtkTreeSelection	*Adr_Line_Selected;				// Adresse
	GtkWidget		*Adr_TreeView;						// Adresse
	GtkWidget		*Adr_Entry_Dest;					// Adresse
	GdkPixbuf		*Pixbuf_NotInstall;					// not_install.png
	GtkCellRenderer		*Renderer;
	GtkTreeViewColumn	*Adr_ColumnFileWavHertz;
	GtkTreeViewColumn	*Adr_ColumnFileWavVoie;
	GtkTreeViewColumn	*Adr_ColumnFileWavBits;
	GtkTreeViewColumn	*Adr_ColumnFileWavNewHertz;
	GtkTreeViewColumn	*Adr_ColumnFileWavNewVoie;
	GtkTreeViewColumn	*Adr_ColumnFileWavNewBits;
	GtkTreeViewColumn	*Adr_ColumnFileWavName;

} VAR_FILE_WAV;

extern VAR_FILE_WAV var_file_wav;

typedef struct {
	GtkWidget		*Adr_scroll;						// Adresse
	GtkListStore		*Adr_List_Store;				// Adresse
	GtkTreeModel		*Adr_Tree_Model;				// Adresse
	GtkTreeSelection	*Adr_Line_Selected;				// Adresse
	GtkWidget		*Adr_TreeView;						// Adresse
	GtkWidget		*Adr_Entry_Dest;					// Adresse
	GtkWidget		*Adr_Button_Dest;					// Adresse
	GtkComboBox		*Adr_combobox_DestFile;				// Adresse
	GdkPixbuf		*Pixbuf_NotInstall;					// not_install.png
	GtkCellRenderer		*Renderer;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggType;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggBitrate;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggNewBitrate;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggSize;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggTime;
	GtkTreeViewColumn	*Adr_ColumnFileMp3OggName;
} VAR_FILE_MP3OGG;

extern VAR_FILE_MP3OGG var_file_mp3ogg;

typedef struct {
	GtkWidget		*Adr_scroll;						// Adresse
	GtkListStore		*Adr_List_Store;				// Adresse
	GtkTreeModel		*Adr_Tree_Model;				// Adresse
	GtkTreeSelection	*Adr_Line_Selected;				// Adresse
	GtkWidget		*Adr_TreeView;						// Adresse
	GtkWidget		*Adr_entry_tag_album;
	GtkWidget		*Adr_entry_tag_title;
	GtkWidget		*Adr_entry_tag_artist;
	GtkWidget		*Adr_entry_tag_comment;
	GtkWidget		*Adr_spinbutton_tag_year;
	GtkWidget		*Adr_spinbutton_tag_track;
	GtkWidget		*Adr_label_tag_change;
	GtkWidget		*Adr_table_tag;
	GtkCellRenderer		*Renderer;
	GtkTreeViewColumn	*Adr_ColumnFileTagsType;
	GtkTreeViewColumn	*Adr_ColumnFileTagsTime;
	GtkTreeViewColumn	*Adr_ColumnFileTagsName;
} VAR_FILE_TAGS;

extern VAR_FILE_TAGS var_file_tags;


typedef enum {
	// SELECTION ou DESELECTION POUR LES CONVERSIONS
	FILE_CONV_DESELECT_ALL = 0,
	FILE_CONV_DESELECT_V,
	FILE_CONV_DESELECT_H,
	FILE_CONV_SELECT_V,
	FILE_CONV_SELECT_EXPERT_V,
	FILE_CONV_SELECT_H,
	FILE_CONV_SELECT_EXPERT_H,
	// SELECTION ou DESELECTION POUR LE REPLAYGAIN
	FILE_REPLAYGAIN_DESELECT_V,
	FILE_REPLAYGAIN_SELECT_PISTE,
	FILE_REPLAYGAIN_SELECT_ALBUM,
	FILE_REPLAYGAIN_SELECT_NETTOYER,
	// SELECTION ou DESELECTION POUR TRASH
	FILE_TRASH_DESELECT_V,
	FILE_TRASH_SELECT_V,
	// SELECTION ou DESELECTION POUR TRASH
	FILEWAV_FREQUENCY_CELL_HERTZ,
	FILEWAV_TRACK_CELL,
	FILEWAV_QUANTIFICATION_CELL
} TYPE_SET_FROM_POPUP_FILE;

//
// ---------------------------------------------------------------------------
//  FILE.C
// ---------------------------------------------------------------------------
//
gchar		*file_get_time( DETAIL *detail );
void		file_scrolledwindow_file_realize( GtkWidget *widget );
void		file_set_flag_buttons( void );
void		file_button_del_file_clicked( void );
void		file_pixbuf_update_glist( void );
void		file_affiche_glist( void );
gboolean	file_get_bool_ReplaygainApply( void );
gboolean	file_get_bool_ReplaygainClear_file( void );
gboolean	file_get_bool_normalize_file( void );
gboolean	file_get_bool_is_conversion( void );
gint		file_get_scan( void );
void		file_verify_before_conversion( void );
gchar		*file_get_pathname_dest( DETAIL *detail, gchar *NewExt );
gchar		*file_get_size( DETAIL *detail );
void		on_file_button_del_file_clicked( GtkButton *button, gpointer user_data );
void		file_from_popup( TYPE_SET_FROM_POPUP_FILE TypeSetFromPopup, DETAIL *detail, TYPE_FILE_IS TypeFileIs );
//
// ---------------------------------------------------------------------------
//  FILE_SCAN_DB.C
// ---------------------------------------------------------------------------
//
void		FileScanDB_action( gboolean BoolAllScan );
//
// ---------------------------------------------------------------------------
//  FILE_ANALYZE.C
// ---------------------------------------------------------------------------
//
#include "fileselect.h"

void		fileanalyze_remove_entetefile_detail( void );
void		fileanalyze_remove_entetefile( void );
void		fileanalyze_add_file_to_treeview( TYPE_FILESELECTION p_TypeFileselection, GSList *p_list );
gboolean	fileanalyze_exist( DETAIL *detail, gchar *NewExt );
//
// ---------------------------------------------------------------------------
//  FILE_CONV.C
// ---------------------------------------------------------------------------
//
void		fileconv_action( void );
//
// ---------------------------------------------------------------------------
//  FILE_MP3OGG.C
// ---------------------------------------------------------------------------
//
void		FileMp3Ogg_set_flag_buttons( void );
void		FileMp3Ogg_affiche_glist( void );
gboolean	FileMp3Ogg_del_file_clicked( void );
void		FileMp3Ogg_change_parameters( void );
// void		FileMp3Ogg_update_newbitrate( TYPE_FILE_IS p_TypeFileIs );
void		FileMp3Ogg_update_newbitrate( TYPE_FILE_IS p_TypeFileIs, gint p_debit, gint p_mode_managed, gint p_downmix );
//
// ---------------------------------------------------------------------------
//  FILE_MP3OGG_CONV.C
// ---------------------------------------------------------------------------
//
void		filemp3oggconv_apply_regul_mp3ogg_by_conv( void );
//
// ---------------------------------------------------------------------------
//  FILE_WAV.C
// ---------------------------------------------------------------------------
//
void		FileWav_affiche_glist( void );
void		FileWav_change_parameters( void );
void		FileWav_set_flag_buttons( void );
void		FileWav_from_popup( TYPE_SET_FROM_POPUP_FILE TypeSetFromPopup, DETAIL *detail, gint freq, gint voie, gint bits );
//
// ---------------------------------------------------------------------------
//  FILE_TAGS.C
// ---------------------------------------------------------------------------
//
void		FileTags_affiche_glist( void );
gboolean	FileWav_del_file_clicked( void );
gboolean	FileTags_del_file_clicked( void );
void		FileTags_appliquer_clicked( void );
void		FileTags_set_flag_buttons( void );
void 		FileTags_changed_all_tags (gint TYPE_TAG);
//
// ---------------------------------------------------------------------------
//  FILE_WAV_CONV.C
// ---------------------------------------------------------------------------
//
void		filewavconv_apply( void );
//
// ---------------------------------------------------------------------------
//  FILE_ACTION.C
// ---------------------------------------------------------------------------
//
typedef enum {
	TYPE_REPLAYGAINCLEAR = 0,
	TYPE_NORMALISE,
	TYPE_CONVERSION,
	TYPE_REPLAYGAINAPPLY,
	TYPE_TRASH
} FILEACTION_TYPE;

void		fileaction_choice( void );
void		fileaction_set_end( FILEACTION_TYPE FileActionType );
//
// ---------------------------------------------------------------------------
//  FILE_LC.C
// ---------------------------------------------------------------------------
//
#include "conv.h"

typedef struct {
	TYPE_CONV	 type_conv;
	gboolean	 With_CommandLineUser;
	gchar		*filesrc;
	gchar		*filedest;
	TAGS		*tags;
	gchar		*cdrom;
	gchar		*num_track;
	gboolean	 BoolSetBitrate;
	gchar		*PtrStrBitrate;
} PARAM_FILELC;

//
// Allocate Tab Args
//
gchar		**filelc_AllocTabArgs( void );
gchar		**filelc_RemoveTab( gchar **p_PtrTabArgs );
// gint		filelc_get_command_line_remove( void );
// gint		filelc_get_command_line_nice( void );
gchar		**filelc_get_command_line( PARAM_FILELC *param_filelc );
void		filelc_get_command_line_extract( PARAM_FILELC *param_filelc );
GList		*filelc_remove_glist( GList *New );
void		filelc_get_command_line_extract_elem( gint Elem, gchar *Dev, gchar *Dest );
//
// ---------------------------------------------------------------------------
//  FILE_TRASH.C
// ---------------------------------------------------------------------------
//
void		filetrash_action( void );
gboolean	filetrash_ok( void );


#endif


