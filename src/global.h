 /*
 *  file      : global.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef global_h
#define global_h 1

#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gi18n.h>


extern	GtkBuilder	*GtkXcfaProjet;
extern	GtkWidget	*WindMain;
extern	GtkWidget	*AdrLabelStatusbarGlobal;

#ifdef ENABLE_NLS
	#include <libintl.h>
	#define gettext_noop(x) (x)
	#define _(String) gettext (String)
#endif

#define GLADE_GET_OBJECT(OBJ) gtk_builder_get_object (GtkXcfaProjet, OBJ)

GtkBuilder *Builder_open( gchar *p_name_glade, GtkBuilder *p_GtkBuilder );

// 
// ---------------------------------------------------------------------------
//  NOTEBOOK
// ---------------------------------------------------------------------------
// 
// notebook_general
#define NOTEBOOK_DVD_AUDIO		0
#define NOTEBOOK_CD_AUDIO		1
	// notebook_expander_cd
	#define NOTEBOOK_CD_AUDIO_TAGS			0
	#define NOTEBOOK_CD_AUDIO_TITRE_CD		1
	#define NOTEBOOK_CD_AUDIO_CUE			2
#define NOTEBOOK_FICHIERS		2
	// notebook_in_file
	#define NOTEBOOK_FICHIERS_CONVERSION	0
	#define NOTEBOOK_FICHIERS_WAV			1
	#define NOTEBOOK_FICHIERS_MP3OGG		2	
	#define NOTEBOOK_FICHIERS_TAGS			3
#define NOTEBOOK_SPLIT			3
#define NOTEBOOK_POCHETTE		4
#define NOTEBOOK_OPTIONS		5
	// notebook_options
	#define NOTEBOOK_OPTIONS_GENERAL		0
	#define NOTEBOOK_OPTIONS_CD_AUDIO		1
	#define NOTEBOOK_OPTIONS_LAME			2
	#define NOTEBOOK_OPTIONS_OGGENC			3
	#define NOTEBOOK_OPTIONS_FLAC			4
	#define NOTEBOOK_OPTIONS_MAC			5
	#define NOTEBOOK_OPTIONS_WAVPACK		6
	#define NOTEBOOK_OPTIONS_MUSEPACK		7
	#define NOTEBOOK_OPTIONS_FAAC			8
	#define NOTEBOOK_OPTIONS_AACPLUSENC		9
	#define NOTEBOOK_OPTIONS_EXPORT_TAGS	10
#define NOTEBOOK_PRGEXTERNES	6
	// notebook_app_externes
	#define NOTEBOOK_OPTIONS_PRGEXTERNES	0
	#define NOTEBOOK_OPTIONS_EXTRA			1

// 
// ---------------------------------------------------------------------------
//  DEBUG
// ---------------------------------------------------------------------------
// 
#define PRINT_FUNC_LF() if( TRUE == OptionsCommandLine.BoolVerboseMode ) g_print ("%s :: %s (line = %d)\n", __FILE__, __FUNCTION__, __LINE__)
#define PRINTINT(str,num) g_print ("%s = %d\n", str, num)
#define PRINTBOOL(str,bool) g_print ("%s = %s\n", str, bool == TRUE ? "TRUE" : "FALSE")
#define PRINT(str) if( TRUE == OptionsCommandLine.BoolVerboseMode ) g_print ("%s :: %s(line = %d)\n\t%s\n", __FILE__, __FUNCTION__, __LINE__, str)
#define	VERBOSE(str) if( TRUE == OptionsCommandLine.BoolVerboseMode ) g_print( "%s", str );

// 
// ---------------------------------------------------------------------------
//  SUPPORT
// ---------------------------------------------------------------------------
// 
#ifndef GLADE_HOOKUP_OBJECT
#define GLADE_HOOKUP_OBJECT(component,widget,name) \
  g_object_set_data_full(G_OBJECT(component), name, \
    g_object_ref(widget), (GDestroyNotify) g_object_unref)
#endif

#ifndef GLADE_HOOKUP_OBJECT_NO_REF
#define GLADE_HOOKUP_OBJECT_NO_REF(component,widget,name) \
  g_object_set_data (G_OBJECT (component), name, widget)
#endif


// 
// ---------------------------------------------------------------------------
//  ALSA_PLAY.C
//  SPLIT_SPECTRE.C
// ---------------------------------------------------------------------------
// 
#define BLOCK_SIZE	2352

// /usr/include/gtk-2.0/gdk/gdkkeysyms.h
typedef struct {
	gboolean	BoolGDKPress;			//
	gboolean	BoolGDK_Control_L_R;	// 
	gboolean	BoolGDK_Control_A;		// 
	guint		keyval;					// 
} KEYS;

extern KEYS keys;

typedef struct {
	gchar		Machine [ 100 ];
	gint		NbCpu;
	gint		TypeCpu;
	gboolean	BoolCpuIs64Bits;
	
} HOST_CONF;

extern HOST_CONF HostConf;




// 
// ---------------------------------------------------------------------------
//  Dossier temporaire
// ---------------------------------------------------------------------------
// 

#define PATH_TMP_XCFA_CD_NORMALISE_PEAK_ALBUM	"xcfa_peak_album"
#define PATH_TMP_XCFA_WAVSPLIT			"xcfa_wavsplit"
#define PATH_TMP_XCFA_AUDIOCD			"xcfa_cdaudio"
#define PATH_TMP_XCFA_AUDIOFILE			"xcfa_audiofile"
#define PATH_TMP_XCFA_AUDIOFILEWAVCONV		"xcfa_audiofilewavconv"
#define PATH_TMP_XCFA_AUDIOFILEMP3OGG		"xcfa_audiofilemp3ogg"
#define PATH_TMP_XCFA_AUDIOFILENORMALYSE	"xcfa_audiofilenormalyse"
#define PATH_TMP_XCFA_CDDBTEST			"xcfa_cddbtest"
#define PATH_SAVE_POCHETTE_IMG			"XcfaPochette"
#define PATH_MANPAGE				"XcfaManpage"


#endif

