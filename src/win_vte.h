 /*
 * file      : win_vte.h
 * project   : XCFA
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 * xcfa - Creation d'une base de programmation en langage C de type GNU avec les autotools
 * GNU General Public License
 * 
 * This file is part of XCFA.
 * 
 * XCFA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 * 
 * XCFA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __win_vte_h__
#define __win_vte_h__ 1


void		WinVte_window_write_va( gchar *p_text, ... );
void		WinVte_reset( void );
void		WinVte_realize( GtkWidget *widget );
gboolean	WinVte_is_ok( void );


#endif


