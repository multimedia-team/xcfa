 /*
 *  file      : cd_audio.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef cd_audio_h
#define cd_audio_h 1

#include "tags.h"

typedef enum {
	TYPE_CUE_NONE = 0,
	TYPE_CUE_CD,
	TYPE_CUE_FILE
} TYPE_CUE;


typedef enum {
	EXTRACT_WITH_CDPARANOIA = 0,
	EXTRACT_WITH_CDPARANOIA_MODE_2,
	EXTRACT_WITH_CDPARANOIA_EXPERT,
	EXTRACT_WITH_CDDA2WAV
} EXTRACT_WITH;


typedef struct {
	GtkWidget		*Adr_checkbutton_log_cdparanoia_mode_expert;
	GtkComboBox		*Adr_combobox_discid_cd;			// 
	GtkComboBox		*Adr_combobox_normalise_cd;			// 
	GtkComboBox		*Adr_Combo_Box;						// 
	GtkComboBox		*Adr_combobox_serveur;				// 
	GtkWidget		*Adr_button_applique_change;		// 
	GtkWidget		*Adr_Button_Cancel_cddb;			// 
	GtkWidget		*Adr_Button_Raffraichir;			// 
	GtkWidget		*Adr_Label_Duree;					// 
	GtkWidget		*Adr_Label_Titre;					// 
	GtkWidget		*Adr_Label_Statusbar;				// 
	GtkButton		*Adr_Button_Destination;			// 
	GtkWidget		*Adr_Progressbar;					// 
	GtkWidget		*Adr_scroll;						// 
	GtkListStore		*Adr_List_Store;				// 
	GtkTreeModel		*Adr_Tree_Model;				// 
	GtkTreeSelection	*Adr_Line_Selected;				// 
	GtkWidget		*Adr_TreeView;						// 
	GdkPixbuf		*Pixbuf_Coche;						// coche.png
	GdkPixbuf		*Pixbuf_Coche_exist;				// coche_exist.png
	GdkPixbuf		*Pixbuf_Selected;					// selected.png
	GdkPixbuf		*Pixbuf_Selected_exist;				// selected_exist.png
	GdkPixbuf		*Pixbuf_Selected_expert;			// selected_expert.png
	GdkPixbuf		*Pixbuf_Selected_expert_exist;		// selected_expert_exist.png
	GdkPixbuf		*Pixbuf_CdPlay;						// play.png
	GdkPixbuf		*Pixbuf_CdStop;						// sol.png
	GdkPixbuf		*Pixbuf_Normalize;					//  normalize1.png
	GdkPixbuf		*Pixbuf_Normalize_Coche;			//  normalize2.png
	GdkPixbuf		*Pixbuf_NotInstall;					// not_install.png
	gboolean		 Bool_create_file_m3u;				// 
	gchar			*Pathname_m3u;						// 
	gchar			*Pathname_xspf;						// 
	GtkWidget		*Adr_Expander;						// 
	GtkWidget		*Adr_notebook;						// 
	GtkWidget		*Adr_entry_tag_titre_album;			// 
	GtkWidget		*Adr_entry_tag_nom_artiste;			// 
	GtkWidget		*Adr_spinbutton_tag_annee;			// 
	GtkWidget		*Adr_entry_new_titre_cdaudio;		// 
	GtkWidget		*Adr_entry_tag_titre_chanson;		// 
	GtkWidget		*Adr_entry_tag_titre_fichier;		// 
	GtkWidget		*Adr_entry_tag_commentaire;			// 
	GtkWidget		*Adr_spinbutton_tag_piste;			// 
	GtkWidget		*Adr_entry_stockage_cdaudio;		// 
	GtkWidget		*Adr_label_stockage_cdaudio;		// 
	GtkWidget		*Adr_entry_name_file_cue;			// 
	TYPE_CUE		 TypeCreateCue;						// TYPE_CUE_NONE | TYPE_CUE_CD | TYPE_CUE_FILE
	GtkTreeViewColumn	*Adr_Column_Play;
	GtkTreeViewColumn	*Adr_Column_Flac;
	GtkTreeViewColumn	*Adr_Column_Wav;
	GtkTreeViewColumn	*Adr_Column_Mp3;
	GtkTreeViewColumn	*Adr_Column_Ogg;
	GtkTreeViewColumn	*Adr_Column_M4a;
	GtkTreeViewColumn	*Adr_Column_Aac;
	GtkTreeViewColumn	*Adr_Column_Mpc;
	GtkTreeViewColumn	*Adr_Column_Ape;
	GtkTreeViewColumn	*Adr_Column_Wavpack;
	GtkTreeViewColumn	*Adr_Column_Num;
	GtkTreeViewColumn	*Adr_Column_Time;
	GtkTreeViewColumn	*Adr_Column_Normalise;
	GtkCellRenderer		*Renderer;
	GtkTreeViewColumn	*Adr_Column_Nom;
	guint			 handler_timeout_read_toc_cd;
	gboolean		 bool_thread_read_toc_cd;
	gboolean		 bool_timeout_read_toc_cd;
	gboolean		 bool_enter_parse;
} VAR_CD;

extern VAR_CD var_cd;

typedef enum {											// Pour extraction / conversion */
	ETAT_CONV_CD_NONE,									// Aucune demande
	ETAT_CONV_CD_WAITING,								// En attente
	ETAT_CONV_CD_CONV_OK								// Fait
} ETAT_CONV_CD;

typedef enum {
	CD_ETAT_NONE = 0,
	CD_ETAT_PRG_ABSENT,
	CD_ETAT_ATTENTE,
	CD_ETAT_ATTENTE_EXIST,
	CD_ETAT_SELECT,
	CD_ETAT_SELECT_EXPERT,
	CD_ETAT_SELECT_EXIST,
	CD_ETAT_SELECT_EXPERT_EXIST
} ETAT_SELECTION_CD;


typedef enum {
	CD_ETAT_PLAY_NONE = 0,
	CD_ETAT_PLAY_PRG_ABSENT,
	CD_ETAT_PLAY_ATTENTE,
	CD_ETAT_PLAY
} ETAT_PLAY_CD;


typedef enum {
	CD_NORM_PEAK_NONE = 0,
	CD_NORM_PEAK,										// INDIVIDUEL   ajustement maximal du volume
	CD_NORM_PEAK_WAITING								// COLLECTIF 	ajustement maximal du volume
} ETAT_NORMALISE_CD;


typedef enum {
	NUM_WAV = 0,
	NUM_WAV_EXPERT_1,
	NUM_WAV_EXPERT_2,
	NUM_FLAC,
	NUM_APE,
	NUM_WAVPACK,
	NUM_OGG,
	NUM_M4A,
	NUM_AAC,
	NUM_MPC,
	NUM_MP3,
	NUM_TOTAL
} NUM_TYPE;

typedef struct {
	gchar			*Arg [ 100 ];
	gboolean	 	 WithCommandLineUser;
	size_t			 Size;
} LIST_CONV;

typedef struct {
	gchar			*Name1;
	gchar			*Name2;
} EXTRACT_EXPERT;

typedef struct {
	LIST_CONV		*ListConv;
	ETAT_PLAY_CD		 EtatPlay;						// 
	gboolean		 Bool_Delette_Flac;					// Le fichier provisoir doit etre efface
	gboolean		 Bool_Delette_Wav;					// 
	gboolean		 Bool_Delette_Mp3;					// 
	gboolean		 Bool_Delette_Ogg;					// 
	gboolean		 Bool_Delette_M4a;					// 
	gboolean		 Bool_Delette_Aac;					// 
	gboolean		 Bool_Delette_Mpc;					// 
	gboolean		 Bool_Delette_Ape;					// 
	gboolean		 Bool_Delette_WavPack;				// 
	ETAT_SELECTION_CD	 EtatSelection_Flac;			// 
	ETAT_SELECTION_CD	 EtatSelection_Wav;				// 
	ETAT_SELECTION_CD	 EtatSelection_Mp3;				// 
	ETAT_SELECTION_CD	 EtatSelection_Ogg;				// 
	ETAT_SELECTION_CD	 EtatSelection_M4a;				// 
	ETAT_SELECTION_CD	 EtatSelection_Aac;				// 
	ETAT_SELECTION_CD	 EtatSelection_Mpc;				// 
	ETAT_SELECTION_CD	 EtatSelection_Ape;				// 
	ETAT_SELECTION_CD	 EtatSelection_WavPack;			// 
	gint			 Num_Track;							// Numero de piste
	gchar			*Str_Track;							// 
	gchar			*NameSong;							// Le nom du futur fichier
	gchar			*Duree;								// La duree d'ecoute
	ETAT_CONV_CD		 Etat_Flac;						// Pour extraction / conversion
	ETAT_CONV_CD		 Etat_Wav;						// 
	ETAT_CONV_CD		 Etat_Mp3;						// 
	ETAT_CONV_CD		 Etat_Ogg;						// 
	ETAT_CONV_CD		 Etat_m4a;						// 
	ETAT_CONV_CD		 Etat_Aac;						// 
	ETAT_CONV_CD		 Etat_Mpc;						// 
	ETAT_CONV_CD		 Etat_Ape;						// 
	ETAT_CONV_CD		 Etat_WavPack;					// 
	ETAT_CONV_CD		 Etat_Sox;
	gboolean		 EtatNormalise;
	ETAT_NORMALISE_CD	 EtatPeak;
	gchar			*PathName_Dest_Flac;				// Emplacement du fichier
	gchar			*PathName_Dest_Wav;					// 
	gchar			*PathName_Dest_Mp3;					// 
	gchar			*PathName_Dest_Ogg;					// 
	gchar			*PathName_Dest_M4a;					// 
	gchar			*PathName_Dest_Aac;					// 
	gchar			*PathName_Dest_Mpc;					// 
	gchar			*PathName_Dest_Ape;					// 
	gchar			*PathName_Dest_WavPack;				// 
	gchar			*PathName_Dest_Tmp;
	TAGS			*tags;								// Tags
	gboolean		 BoolM3uForWav;
	EXTRACT_EXPERT	ExtractExpert;
	gboolean		 EtatBoolWavpack;
} CD_AUDIO;

typedef struct {										// UN ENTETE POUR LE CD
	gchar			*TitleCD;							// Titre integral du CD
	gchar			*Title;								// Titre du CD
	gchar			*Artiste;							// Nom de l'artiste
	gboolean		 BoolMultiArtiste;					// 
	gchar			*StrDureeCd;						// Duree totale du cd
	gint			 TotalTracks;						// Nombre total de pistes trouvees
	gint			 NumGenre;							// Genre en numerique
	gchar			*StrGenre;							// Genre en chaine
	gint			 NumYear;							// Annee du cd en numerique
	gchar			*StrYear;							// Annee du cd en chaine
	GList			*GList_Audio_cd;					// UNE STRUCTURE 'AUDIO' PAR PISTE
	gboolean		 Bool_Read_Infos_cd;				// TRUE or FALSE
	gchar			*Message;							// Message pour l'utilisateur
	gchar			*NameCD_Device;						// Pointeur sur le device en selection 
	gchar			 Str_Nice [ 10 ];					// Priorite systeme
	gchar			 Str_Bitrate_Lame [ 10 ];			// Bitrate pour Lame
	gchar			 Str_Bitrate_oggenc [ 10 ]; 		// Bitrate pour Oggenc
} ENTETE_CD;

extern ENTETE_CD EnteteCD;


typedef enum {
	// SELECTION ou DESELECTION POUR LES CONVERSIONS
	CD_CONV_DESELECT_ALL = 0,
	CD_CONV_DESELECT_V,
	CD_CONV_DESELECT_H,
	CD_CONV_SELECT_V,
	CD_CONV_SELECT_EXPERT_V,
	CD_CONV_SELECT_H,
	CD_CONV_SELECT_EXPERT_H,
	// SELECTION ou DESELECTION POUR LA NORMALISATION
	CD_REPLAYGAIN_SELECT_V,
	CD_REPLAYGAIN_DESELECT_V
} TYPE_SET_FROM_POPUP_CD;

/*
*---------------------------------------------------------------------------
* CD_AUDIO.C
*---------------------------------------------------------------------------
*/
void		cdaudio_deallocate_glist (void);
gchar		*cdaudio_get_result_destination (void);
void		cdaudio_set_flag_buttons (void);
void		cdaudio_affiche_glist_audio (void);
void		cdaudio_update_glist (void);
void		cdaudio_set_titre_chanson (void);
CD_AUDIO	*cdaudio_get_line_selected (void);
void		cdaudio_deallocate_glist_context (void);
gboolean	cdaudio_get_bool_is_wav_extract_to_cue (void);
void		cdaudio_put_label_titre (gchar *Messag);
void		cdaudio_put_label_duree (gchar *Messag);
void		cdaudio_from_popup (TYPE_SET_FROM_POPUP_CD TypeSetFromPopup, CD_AUDIO *Audio, TYPE_FILE_IS TypeFileIs);
void		cd_audio_bool_access_discid( gboolean p_bool_access );
CD_AUDIO	*cdaudio_get_line_selected_for_extract( void );

/*
*---------------------------------------------------------------------------
* CD_EXPANDER.C
*---------------------------------------------------------------------------
*/
void		cdexpander_set_entry_tag_titre_album (void);
void		cdexpander_set_entry_tag_nom_artiste (void);
void		cdexpander_set_spinbutton_tag_annee (void);
void		cdexpander_set_entry_tag_titre_chanson (void);
void		cdexpander_set_new_genre (void);
void		cdexpander_set_entry_tag_commentaire (void);
void		cdexpander_set_spinbutton_tag_piste (void);
void		cdexpander_set_entry_tag_titre_fichier_m3u (void);
void		cdexpander_set_sensitive_notebook (void);
void		cd_expander_set_genre( gint p_num, gchar *p_name );

/*
*---------------------------------------------------------------------------
* CD_AUDIO_TOC
*---------------------------------------------------------------------------
*/
void		cdaudiotoc_reffresh_list_cd (void);
void		cdaudiotoc_button_eject_cd_clicked (void);


#endif






