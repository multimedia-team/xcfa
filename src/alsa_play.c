 /*
 *  file      : alsa_play.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "split.h"
#include "tags.h"
#include "alsa_audio.h"
#include "alsa_play.h"



// 
// PREMIERE UTILISAION D' ALSA DEPUIS: 3.7.2~beta3
// 



// 
// 
void AlsaPlay_remove (void)
{
	if (NULL != VarAlsa.buffer) {
		g_free (VarAlsa.buffer);
		VarAlsa.buffer = NULL;
	}
}
// 
// 
gdouble		AlsaPlay_TRet;
gboolean	AlsaPlay_BoolPrintPercent = FALSE;

// 
// 
static void AlsaPlay_thread (void *arg)
{
	gdouble	Ret;
	gdouble	PercentBegin = -1.;
	
	VarAlsa.BoolThreadEnd = FALSE;
	
	AlsaPlay_remove ();
	
	VarAlsa.buffer = g_malloc0 (sizeof(char) * VarAlsa.SizeBuffer);
	Ret = 0.0;
	AlsaPlay_TRet = 0.0;
	
	while ((Ret = fread (VarAlsa.buffer,  1, VarAlsa.SizeBuffer, VarAlsa.pFile)) > 0) {
				
		if (VarAlsa.StopPlay == TRUE) break;
				
		AlsaPlay_TRet +=  Ret;
		
		VarAlsa.Percent = (float)(AlsaPlay_TRet / (float)VarAlsa.TotalChunckSize) * 100.0;
		// Sec = (int)(((float)VarAlsa.Sec * VarAlsa.Percent) / 100.0);

		if (VarAlsa.Percent >= VarAlsa.PercentBegin && VarAlsa.Percent <= VarAlsa.PercentEnd) {
			
			// CALCUL DU POURCENTAGE REEL DE LECTURE
			
			if (PercentBegin < 0.0) PercentBegin = VarAlsa.Percent;
			if (PercentBegin >= 0.0) {
				VarAlsa.PercentSel = VarAlsa.Percent;
				AlsaPlay_BoolPrintPercent = TRUE;
			}
			
			AlsaAudio_write (VarAlsa.SizeBuffer);
		}
	}
	
	VarAlsa.BoolThreadEnd = TRUE;
	PRINT("FIN THREAD ALSA");
	pthread_exit(0);
}
// 
// 
void AlsaPlay_Pause (void)
{
	VarAlsa.PauseAlsa = TRUE;
}
// 
// 
void AlsaPlay_fseek (gdouble p_NewPosInPercent)
{
	// void rewind(VarAlsa.pFile);
	// La fonction rewind() place l’indicateur de position du flux pointé par stream au début du fichier.
	// C’est l’équivalent de:
	// 	(void) fseek(VarAlsa.pFile, 0L, SEEK_SET)
	// 
	if (NULL != VarAlsa.pFile) {
		AlsaPlay_BoolPrintPercent = FALSE;
		rewind (VarAlsa.pFile);
		VarAlsa.PauseAlsa = FALSE;
		AlsaPlay_TRet = 0.0;
		VarAlsa.PercentBegin = p_NewPosInPercent;
	}
}
// 
// 
static gint AlsaPlay_timeout (gpointer data)
{
	if (VarAlsa.BoolThreadEnd == TRUE) {
		
		fclose (VarAlsa.pFile);
		VarAlsa.pFile = NULL;
		AlsaAudio_close_device ();
		g_source_remove (VarAlsa.HandlerTimeout);

		if (NULL != VarAlsa.FuncSetValueTime) (*VarAlsa.FuncSetValueTime) (VarAlsa.PercentSel);
		if (NULL != VarAlsa.FuncIconeStop)    (*VarAlsa.FuncIconeStop) ();

		PRINT("FIN TIMEOUT ALSA");
	}
	else {
		if( TRUE == AlsaPlay_BoolPrintPercent ) {
			AlsaPlay_BoolPrintPercent = FALSE;
			if (NULL != VarAlsa.FuncSetValueTime && VarAlsa.PercentSel > 0.0 && FALSE == VarAlsa.PauseAlsa) (*VarAlsa.FuncSetValueTime) (VarAlsa.PercentSel);
		}
	}

	return (TRUE);
}
// 
// 
gboolean AlsaPlay_is_play (void)
{
	return ((VarAlsa.pFile == NULL) ? FALSE : TRUE);
}
// 
// 
void AlsaPlay_stop (void)
{
	VarAlsa.StopPlay = TRUE;
}
// 
// 
void AlsaPlay_song (gchar *NameFile, gdouble PBegin, gdouble PEnd, void *p_FuncSetValueTime, void *p_FuncIconeStop)
{
	pthread_t	nmr_tid;
	gboolean	BoolOpenDevice = FALSE;
	WAVE		WaveHeader;
	
	if (FALSE == tagswav_read_file (NameFile, &WaveHeader)) {
		tagswav_close_file (&WaveHeader);
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("\nImpossible d'ouvrir le fichier : \"%s\"\n\n", NameFile);
		return;
	}
	
	// OUVERTURE FICHIER ET TRANSMISSION DU HANDLE
	VarAlsa.pFile = WaveHeader.file;
	WaveHeader.file = NULL;
	
	switch (WaveHeader.FMT.BitsPerSample) {
	case 8 :
	case 16 :
	case 24 :
	case 32 :
		break;
	default :
		fclose (VarAlsa.pFile);
		PRINT_FUNC_LF();
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print("Seuls ces formats de bitrates : 8 16 24 et 32 sont pris en charge pour la lecture\n"
				"Format de bitrate [ %d ] non prit en charge\n", 
				WaveHeader.FMT.BitsPerSample);
			g_print("NO MUSIC :/\n");
		}
		return;
	}
	
	// DUREE EN SECONDES DU FICHIER
	VarAlsa.Sec          = WaveHeader.DATA.Subchunk2Size / WaveHeader.FMT.ByteRate;
	VarAlsa.PercentBegin = PBegin;
	VarAlsa.PercentEnd   = PEnd;
	
	// TAILLE DES DATA AUDIO EN OCTETS
	VarAlsa.TotalChunckSize = WaveHeader.DATA.Subchunk2Size;
	
	// TEST OUVERTURE DEVICE
	BoolOpenDevice = AlsaAudio_open_device(
			"default",							// const char *audio_dev,		default
			WaveHeader.FMT.BitsPerSample,		// int p_bitsPerSample,			8, 16, 24, ...
			WaveHeader.FMT.NumChannels,			// int p_channels,			1, 2, 4, ...
			WaveHeader.FMT.SampleRate,			// unsigned int p_samplesPerSec		44100, ...
			&VarAlsa.SizeBuffer					// unsigned int *p_bufferSize,		Taille buffer retournee
			);
	
	if (BoolOpenDevice == TRUE) {
		VarAlsa.PauseAlsa        = FALSE;
		VarAlsa.FuncSetValueTime = p_FuncSetValueTime;
		VarAlsa.FuncIconeStop    = p_FuncIconeStop;
		VarAlsa.StopPlay         = FALSE;
		VarAlsa.BoolThreadEnd    = FALSE;
		VarAlsa.PercentSel       = 0.0;
		PRINT("DEBUT THREAD ALSA");
		if( TRUE == OptionsCommandLine.BoolVerboseMode )
			g_print ("\t%s\n", NameFile);
		pthread_create (&nmr_tid, NULL ,(void *)AlsaPlay_thread, (void *)NULL);
		PRINT("DEBUT TIMEOUT ALSA");
		VarAlsa.HandlerTimeout = g_timeout_add (100, AlsaPlay_timeout, 0);
	}
	else {
		fclose (VarAlsa.pFile);
		AlsaAudio_close_device ();
		PRINT_FUNC_LF();
		if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
			g_print("Format de bitrate [ %d ] non prit en charge\n", WaveHeader.FMT.BitsPerSample);
			g_print("NO MUSIC :/\n");
		}
	}
}
















