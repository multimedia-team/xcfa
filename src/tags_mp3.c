 /*
 *  file    : tags_mp3.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>
#include "get_info.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

typedef struct  {
	gchar	magic[ 3 ];
	gchar	songname[ 30 ];
	gchar	artist[ 30 ];
	gchar	album[ 30 ];
	gchar	year[ 4 ];
	gchar	note[ 28 ];
	unsigned char	nnull;
	unsigned char	track;
	unsigned char	style;
} ID3TAG;


/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/
INFO_MP3 *tagsmp3_remove_info (INFO_MP3 *info)
{
	if (NULL != info) {
		if (NULL != info->bitrate)	{ g_free (info->bitrate);	info->bitrate = NULL;	}
		if (NULL != info->time)		{ g_free (info->time);		info->time = NULL;	}
		if (NULL != info->size)		{ g_free (info->size);		info->size = NULL;	}

		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}

	return ((INFO_MP3 *)NULL);
}
/*
MPEG-1   layer III sample frequencies (kHz):  32  48  44.1
bitrates (kbit/s): 32 40 48 56 64 80 96 112 128 160 192 224 256 320

MPEG-2   layer III sample frequencies (kHz):  16  24  22.05
bitrates (kbit/s):  8 16 24 32 40 48 56 64 80 96 112 128 144 160

MPEG-2.5 layer III sample frequencies (kHz):   8  12  11.025
bitrates (kbit/s):  8 16 24 32 40 48 56 64 80 96 112 128 144 160
-1
0
1
2
*/
gint tagsmp3_type_mpeg (gchar *namefile)
{
	gchar        *Lout = NULL;
	gchar        *ptr = NULL;
#define MAX_STR 256
	static gchar  str [ MAX_STR + 4 ];
	gint          cpt = 0;
	gchar        *term[] = {"MPV_1", "MPV_2", "MPV_25"};
	gint          i;
	GString      *gstr = NULL;
	gint          Ret = NONE_MPEG;	// NONE_MPEG = -1

	if (NULL == (gstr = GetInfo_checkmp3 (namefile))) {
		return (-1);
	}

	Lout = gstr->str;
	for (i = 0; i < 3; i ++) {
		if ((ptr = strstr (Lout, term[ i ]))) {

			/* Init la chaine de stockage */
			for (cpt=0; cpt < MAX_STR; cpt++) str [ cpt ] = '\0';

			/* Passe l'intro */
			while (*ptr != ' ') ptr ++;
			while (*ptr == ' ') ptr ++;

			/* Copie */
			cpt = 0;
			while (*ptr != '\n') {
				if (cpt > MAX_STR) break;
				str [ cpt ++ ] = *ptr ++;
			}
			str [ cpt ] = '\0';

			switch ( i ) {
			case 0 :
				// g_print ("----------------------------MPV_1  str=%s\n", str);
				if (str[0] == '1') Ret = MPEG_1;	// MPEG_1 = 0
				break;
			case 1 :
				// g_print ("----------------------------MPV_2  str=%s\n", str);
				if (str[0] == '1') Ret = MPEG_2;	// MPEG_2 = 1
				break;
			case 2 :
				// g_print ("----------------------------MPV_25 str=%s\n", str);
				if (str[0] == '1') Ret =  MPEG_25;	// MPEG_25 = 2
				break;
			}
			if (Ret > -1) break;
		}
	}
	g_string_free (gstr, TRUE);
	Lout = NULL;

	return (Ret);
}

void tagsmp3_print( gchar *p_PathNameFile, INFO_MP3 *ptrinfo )
{
	if( TRUE == OptionsCommandLine.BoolVerboseMode && NULL != ptrinfo ) {
		g_print( "\n");
		g_print ("\t%s\n", p_PathNameFile );
		g_print( "\tptrinfo->tags->Title    = %s\n", ptrinfo->tags->Title );
		g_print( "\tptrinfo->tags->Artist   = %s\n", ptrinfo->tags->Artist );
		g_print( "\tptrinfo->tags->Album    = %s\n", ptrinfo->tags->Album );
		g_print( "\tptrinfo->tags->IntYear  = %d\n", ptrinfo->tags->IntYear );
		g_print( "\tptrinfo->tags->Year     = %s\n", ptrinfo->tags->Year );
		g_print( "\tptrinfo->tags->Comment  = %s\n", ptrinfo->tags->Comment );
		g_print( "\tptrinfo->tags->IntNumber= %d\n", ptrinfo->tags->IntNumber );
		g_print( "\tptrinfo->tags->Number   = %s\n", ptrinfo->tags->Number );
		g_print( "\tptrinfo->tags->Genre    = %s\n", ptrinfo->tags->Genre );
		g_print( "\tptrinfo->tags->IntGenre = %d\n", ptrinfo->tags->IntGenre );
		g_print( "\tptrinfo->time           = %s\n", ptrinfo->time );
		g_print( "\tptrinfo->bitrate        = %s\n", ptrinfo->bitrate );
		g_print( "\tptrinfo->mpeg_is        = %d\n", ptrinfo->mpeg_is );
		g_print( "\tptrinfo->size           = %s\n", ptrinfo->size );
	}
}

INFO_MP3 *tagsmp3_get_info (DETAIL *detail)
{
	INFO_MP3     *ptrinfo = NULL;
	TagLib_File  *file;
	TagLib_Tag   *tag;
	const TagLib_AudioProperties *properties;
	gint		m;
	gint		s;
	gint		sec;

	/* PRINT_FUNC_LF();*/

	ptrinfo = (INFO_MP3 *)g_malloc0 (sizeof (INFO_MP3));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);

	if ((file = taglib_file_new (detail->namefile))) {

		taglib_set_strings_unicode(FALSE);
		tag = taglib_file_tag(file);
		properties = taglib_file_audioproperties(file);

		ptrinfo->tags->Title     = g_strdup (taglib_tag_title(tag));
		// ptrinfo->tags->Title     = utils_convert_string (taglib_tag_title(tag), TRUE);


		ptrinfo->tags->Artist    = g_strdup (taglib_tag_artist(tag));
		// ptrinfo->tags->Artist    = utils_convert_string (taglib_tag_artist(tag), TRUE);


		ptrinfo->tags->Album     = g_strdup (taglib_tag_album(tag));
		// ptrinfo->tags->Album     = utils_convert_string (taglib_tag_album(tag), TRUE);


		ptrinfo->tags->IntYear   = taglib_tag_year(tag);
		ptrinfo->tags->Year      = g_strdup_printf ("%d", ptrinfo->tags->IntYear);
		ptrinfo->tags->Comment   = g_strdup (taglib_tag_comment(tag));
		// ptrinfo->tags->Comment   = utils_convert_string (taglib_tag_comment(tag), TRUE);

		ptrinfo->tags->IntNumber = taglib_tag_track(tag);
		ptrinfo->tags->Number    = g_strdup_printf ("%d", ptrinfo->tags->IntNumber);
		ptrinfo->tags->Genre     = g_strdup (taglib_tag_genre(tag));
		// ptrinfo->tags->Genre     = utils_convert_string (taglib_tag_genre(tag), TRUE);


		ptrinfo->tags->IntGenre  = tags_get_genre_by_value (ptrinfo->tags->Genre);

		ptrinfo->SecTime =
		sec = taglib_audioproperties_length(properties);
		s = sec % 60; sec /= 60;
		m = sec % 60; sec /= 60;
		if (sec > 0) ptrinfo->time = g_strdup_printf ("%02d:%02d:%02d", sec, m, s);
		else         ptrinfo->time = g_strdup_printf ("%02d:%02d", m, s);

		ptrinfo->bitrate  = g_strdup_printf ("%d", taglib_audioproperties_bitrate(properties));
		ptrinfo->mpeg_is  = tagsmp3_type_mpeg (detail->namefile);
		ptrinfo->size     = g_strdup_printf ("%d Ko", (gint)libutils_get_size_file (detail->namefile) / 1024);

		taglib_tag_free_strings();
		taglib_file_free (file);
	}
	/*
	ptrinfo->level = level_get_from (FILE_IS_MP3, namefile);
	g_print ("%s\t%d\n", namefile, ptrinfo->level);
	*/
	ptrinfo->LevelDbfs.level = -1;
	ptrinfo->LevelDbfs.NewLevel = -1;

	tagsmp3_print( detail->namefile, ptrinfo );

	return (ptrinfo);
}






