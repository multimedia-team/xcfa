 /*
 *  file      : proc.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "proc.h"



/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/
#define DRIVE_NAME "drive name:"


/*
*---------------------------------------------------------------------------
* FUNCTIONS
*---------------------------------------------------------------------------
*/

/* Detruit la GList
*/
GList *proc_remove_glist_cdrominfo (GList *list)
{
	GList *LList = list;
	
	PRINT_FUNC_LF();

	list = g_list_first (list);
	while (list) {
		if ((gchar *)list->data) {
			g_free ((gchar *)list->data);
			list->data = NULL;
		}
		/*list = g_list_remove (list, list->data);*/
		list = g_list_next(list);
	}
	g_list_free (LList);
	LList = NULL;
	return ((GList *)NULL);
}

/* Verification visuelle des entree du GList
*/
void proc_print_glist_cdrominfo (GList *list)
{
	GList *p_list = NULL;

	/*PRINT_FUNC_LF();*/

	p_list = g_list_first (list);
	while (p_list) {
		if (p_list->data) {
			g_print ("%s\n", (gchar *)p_list->data);
		}
		p_list = g_list_next(p_list);
	}
}

/* Recup des info depuis le fichier '/proc/sys/dev/cdrom/info' et
*  retour dans un GList.
*  Si le fichier n'existe pas, retour NULL
*/
GList *proc_get_proc_init_cdrominfo (void)
{
	GList     *List = NULL;
	GError    *Aerror = NULL;
	gchar     *Lcontent = NULL;
	gchar    **Larrbuf = NULL;
	gchar     *p = NULL;		/* Pointer begin DRIVE_NAME in Larrbuf */
	gchar     *s_proc = NULL;	/* String DRIVE_NAME */
	gchar     *s_alloc = NULL;	/* String to GList */
	gchar     *ps = NULL;		/* Pointer begin s_proc */
	gint       i;			/* Counter */

	PRINT_FUNC_LF();

	if (FALSE == g_file_get_contents("/proc/sys/dev/cdrom/info", &Lcontent, NULL, &Aerror)) {
		g_print ("\t---------------------------------------------------\n");
		g_print ("\tLE FICHIER: </proc/sys/dev/cdrom/info> N'EXISTE PAS\n");
		g_print ("\t---------------------------------------------------\n");
		return (NULL);
	}
	Larrbuf = g_strsplit(Lcontent, "\n", 0);

	/* TEST TO DEBUG
	*
	g_free (Larrbuf[2]);
	Larrbuf[2] = g_strdup ("drive name:               hdd	hdc scd4	 sr0");
	*/
	for (i=0; Larrbuf[i]; i++) {
		if (0 == strncmp(Larrbuf[i], DRIVE_NAME, strlen(DRIVE_NAME))) {

			s_proc = g_strdup (Larrbuf[i]);
			/*g_print ("Larrbuf[%02d] = %s\n", i, s_proc);*/

			p = Larrbuf[i] + strlen(DRIVE_NAME);
			while (*p) {
				if (*p == ' ' || *p == '\t') {
					p ++;
					continue;
				}
				strcpy (s_proc, p);
				ps = s_proc;
				while ((*ps >= 'a' && *ps <= 'z') || (*ps >= '0' && *ps <= '9')) ps++;
				*ps = '\0';
				// g_print ("1>%s\n", s_proc);

				s_alloc = g_strdup (s_proc);
				List = g_list_append (List, s_alloc);
				while (*p && ((*p >= 'a' && *p <= 'z') || (*p >= '0' && *p <= '9'))) p++;

			}
			g_free (s_proc);
			s_proc = NULL;
		}
	}
	g_strfreev(Larrbuf);
	g_free(Lcontent);
	g_free (s_proc);
	return (List);
}

