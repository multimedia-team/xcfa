 /*
 *  file    : tags.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifndef tags_h
#define tags_h 1


#include "file.h"


typedef enum {
	TAG_WAVE_IS_NONE = 0,
	TAG_WAVE_IS_FMT,
	TAG_WAVE_IS_BEXT
} TAG_WAV_IS_FMT_BEXT;


typedef struct {  

	struct {  
		char	ChunkID[4];		//(  4 octets) : contient les lettres "RIFF" pour indiquer que le fichier est codé selon la norme RIFF  
		guint32 ChunkSize;		//(  4 octets) : taille du fichier entier en octets( sans compter les 8 octets de ce champ et le champ précédent CunkID  
		char	Format[4];		//(  4 octets) : correspond au format du fichier donc ici, contient les lettres "WAVE" car fichier est au format wave  
	} RIFF;  
	
	TAG_WAV_IS_FMT_BEXT	TagWavIsFmtBext;
	
	struct {  
		char	Subchunk1ID[4];	//(  4 octets) : contient les lettres "fmt " pour indiquer les données à suivre décrivent le format des données audio  
		guint32 Subchunk1Size;	//(  4 octets) : taille en octet des données à suivre( qui suivent cette variable) 16 Pour un fichier PCM  
		short	AudioFormat;	//(  2 octets) : format de compression( une valeur autre que 1 indique une compression)  
		short	NumChannels;	//(  2 octets) : nombre de canaux
								// 	1 pour mono,
								// 	2 pour stéréo
								// 	3 pour gauche, droit et centre
								// 	4 pour face gauche, face droit, arrière gauche, arrière droit
								// 	5 pour gauche, centre, droit, surround( ambiant)
								// 	6 pour centre gauche, gauche, centre, centre droit, droit, surround( ambiant)
		guint32 SampleRate;		//(  4 octets) : fréquence d'échantillonage, ex 44100, 44800( nombre d'échantillons par secondes)  
		guint32 ByteRate;		//(  4 octets) : nombre d'octects par secondes  
		short	Blockalign;		//(  2 octets) : nombre d'octects pour coder un échantillon  
		short	BitsPerSample;	//(  2 octets) : nombre de bits pour coder un échantillon  
	} FMT;  

	struct {  
		char	Subchunk2ID[4];	//(  4 octets) : contient les lettres "data" pour indiquer que les données à suivre sont les données audio( les échantillons et)  
		guint32 Subchunk2Size;	//(  4 octets) : taille des données audio( nombre total d'octets codant les données audio)  
		short	*data;			//(  4 octets) : données audio... les échantillons  
								//     DATAS[] : [Octets du Sample 1 du Canal 1] [Octets du Sample 1 du Canal 2] [Octets du Sample 2 du Canal 1] [Octets du Sample 2 du Canal 2]
	} DATA;  
	
	FILE	*file;				// Handle du fichier si different de NULL
} WAVE;

// 
// ---------------------------------------------------------------------------
//  TAGS_WAV.C
// ---------------------------------------------------------------------------
// 
INFO_WAV	*tagswav_remove_info( INFO_WAV *info );
INFO_WAV	*tagswav_get_info( gchar *namefile );
gboolean	tagswav_file_is_mono( gchar *namefile );
gboolean	tagswav_file_GetBitrate( gchar *namefile, gint *Channels, gint *Hertz, gint *Bits );
gint		tagswav_get_time_sec( gchar *namefile );
gboolean	tagswav_read_file( gchar *wave_file, WAVE *WaveHeader );
void		tagswav_close_file( WAVE *WaveHeader );
void		tagswav_print( gchar *p_PathNameFile );
// 
// ---------------------------------------------------------------------------
//  TAGS.C
// ---------------------------------------------------------------------------
// 
typedef struct {
	gint   num;
	gchar *name;
} STRUCT_TAGS_FILE_MP3;
// 
// 
extern	STRUCT_TAGS_FILE_MP3 StructTagsFileMp3 [];

TAGS		*tags_alloc( gboolean bool_tag_cd );
TAGS		*tags_remove( TAGS *tags );
TAGS		*tags_set( gchar *filename, TAGS *tags );
void		tags_set_flag_modification( TAGS *tags, gboolean p_flag );
gboolean	tags_is_modified( TAGS *tags );
gchar		*tags_get_time_wav( gchar *namefile );
gchar		*tags_get_time( gchar *namefile );
gint		tags_get_genre_by_value( gchar *p_name );
gchar		*tags_get_str_type_file_is( TYPE_FILE_IS type );
void		tags_set_elements_combobox( GtkWidget *widget );
gint		tags_get_elements_combobox( gint num );
gint		tags_get_num_combobox( gint value );
gchar		*tags_get_genre_by_name( gint value );
// 
// ---------------------------------------------------------------------------
//  TAGS_FLAC.C
// ---------------------------------------------------------------------------
// 
INFO_FLAC	*tagsflac_remove_info( INFO_FLAC *info );
INFO_FLAC	*tagsflac_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_MP3.C
// ---------------------------------------------------------------------------
// 
INFO_MP3	*tagsmp3_remove_info( INFO_MP3 *info );
INFO_MP3	*tagsmp3_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_OGG.C
// ---------------------------------------------------------------------------
// 
INFO_OGG	*tagsogg_remove_info( INFO_OGG *info );
INFO_OGG	*tagsogg_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_M4A_.C
// ---------------------------------------------------------------------------
// 
INFO_M4A	*tagsm4a_remove_info( INFO_M4A *info );
INFO_M4A	*tagsm4a_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_AAC_.C
// ---------------------------------------------------------------------------
// 
INFO_AAC	*tagsaac_remove_info( INFO_AAC *info );
// 
// ---------------------------------------------------------------------------
//  TAGS_SHN.C
// ---------------------------------------------------------------------------
// 
INFO_SHN	*tagsshn_remove_info( INFO_SHN *info );
INFO_SHN	*tagsshn_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_WMA.C
// ---------------------------------------------------------------------------
// 
INFO_WMA	*tagswma_remove_info( INFO_WMA *info );
INFO_WMA	*tagswma_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_MPC.C
// ---------------------------------------------------------------------------
// 
INFO_MPC	*tagsmpc_remove_info( INFO_MPC *info );
INFO_MPC	*tagsmpc_get_info( DETAIL *detai );
// 
// ---------------------------------------------------------------------------
//  TAGS_APE.C
// ---------------------------------------------------------------------------
// 
INFO_APE	*tagsape_remove_info( INFO_APE *info );
INFO_APE	*tagsape_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_WAVPACK.C
// ---------------------------------------------------------------------------
// 
INFO_WAVPACK	*tagswavpack_remove_info( INFO_WAVPACK *info );
INFO_WAVPACK	*tagswavpack_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_RM.C
// ---------------------------------------------------------------------------
// 
INFO_RM		*tagsrm_remove_info( INFO_RM *info );
INFO_RM		*tagsrm_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_DTS.C
// ---------------------------------------------------------------------------
// 
INFO_DTS	*tagsdts_remove_info( INFO_DTS *info );
INFO_DTS	*tagsdts_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_AIFF.C
// ---------------------------------------------------------------------------
// 
INFO_AIFF	*tagsaiff_remove_info( INFO_AIFF *info );
INFO_AIFF	*tagsaiff_get_info( DETAIL *detail );
// 
// ---------------------------------------------------------------------------
//  TAGS_AC3.C
// ---------------------------------------------------------------------------
// 
INFO_AC3	*tagsac3_remove_info( INFO_AC3 *info );
INFO_AC3	*tagsac3_get_info( DETAIL *detail );


#endif


