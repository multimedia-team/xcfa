 /*
 *  file      : prg_init.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "options.h"
#include "cd_audio.h"
#include "prg_init.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

enum
{
	NUM_TREE_Nom = 0,
	NUM_TREE_Status,
	NUM_TREE_Action,
	NUM_TREE_ALL_COLUMN
};

enum
{
	PRGEXT_NAME = 0,
	PRGEXT_STATUS,
	PRGEXT_PAQUAGE,
	PRGEXT_COMMENT,
	PRGEXT_NUM_COLUMN,
	PRGEXT_TOTAL_COLUMN
};


VAR_PRGEXTERN var_prgextern;

PRGINIT PrgInit;


TABLEAU_PRG_EXTERN TableauPrgExtern [ NMR_MAX_TABLEAU ] = {

{"a52dec",							// PACKAGEDEBIAN_64
"a52dec",							// PACKAGEDEBIAN_32
"a52dec",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_a52dec,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" liba52-0.7.4-dev",						// PAQUAGE
gettext_noop(" Decode ATSC A/52 audio streams")},		// DESCRIPTION

{"aacplusenc",							// PACKAGEDEBIAN_64
"aacplusenc",							// PACKAGEDEBIAN_32
"aacplusenc",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_aacplusenc,					// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" aacplusenc",							// PAQUAGE
gettext_noop(" AAC+ encoder")},					// DESCRIPTION

{"cdparanoia",							// PACKAGEDEBIAN_64
"cdparanoia",							// PACKAGEDEBIAN_32
"cdparanoia",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_cdparanoia,					// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" cdparanoia",							// PAQUAGE
gettext_noop(" Extracteur cd audio")},				// DESCRIPTION

{"cd-discid",							// PACKAGEDEBIAN_64
"cd-discid",							// PACKAGEDEBIAN_32
"cd-discid",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_cd_discid,					// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" cd-discid",							// PAQUAGE
gettext_noop(" Read CD and get CDDB discid information")},	// DESCRIPTION

{"checkmp3",							// PACKAGEDEBIAN_64
"checkmp3",							// PACKAGEDEBIAN_32
"mp3check",							// NAME1
"checkmp3",							// NAME2
"mp3_check",							// NAME3
&PrgInit.bool_checkmp3,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" checkmp3",							// PAQUAGE
gettext_noop(" Cherche des renseignements sur les formats mp3")},		// DESCRIPTION

{"faac",							// PACKAGEDEBIAN_64
"faac",								// PACKAGEDEBIAN_32
"faac",								// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_faac,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" faac",							// PAQUAGE
gettext_noop(" Audio Codeur freeware")},			// DESCRIPTION

{"faad",							// PACKAGEDEBIAN_64
"faad",								// PACKAGEDEBIAN_32
"faad",								// NAME1
"faad2",							// NAME2
"",								// NAME3
&PrgInit.bool_faad,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" faad",							// PAQUAGE
gettext_noop(" MPEG-4 AAC decodeur")},				// DESCRIPTION

{"flac",							// PACKAGEDEBIAN_64
"flac",								// PACKAGEDEBIAN_32
"flac",								// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_flac,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" flac",							// PAQUAGE
gettext_noop(" Conversion wav : flac")},			// DESCRIPTION

{"icedax",							// PACKAGEDEBIAN_64
"icedax",							// PACKAGEDEBIAN_32
"icedax",							// NAME1
"cdda2wav",							// NAME2
"",								// NAME3
&PrgInit.bool_cdda2wav,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" icedax",							// PAQUAGE
gettext_noop(" Extracteur cd audio")},				// DESCRIPTION

{"lame",							// PACKAGEDEBIAN_64
"lame",								// PACKAGEDEBIAN_32
"lame",								// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_lame,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" lame",							// PAQUAGE
gettext_noop(" Conversion wav : mp3")},				// DESCRIPTION

{"lsdvd",							// PACKAGEDEBIAN_64
"lsdvd",							// PACKAGEDEBIAN_32
"lsdvd",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_lsdvd,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" lsdvd",							// PAQUAGE
gettext_noop(" Cherche les informations d'un dvd")},		// DESCRIPTION

// ADD
{"monkeys-audio",						// PACKAGEDEBIAN_64
"monkeys-audio",						// PACKAGEDEBIAN_32
"mac",								// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_ape,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" monkeys-audio",						// PAQUAGE
gettext_noop(" Monkey's Audio Console Front End : APE")},	// DESCRIPTION

// ADD TO amd64 and i386
{"musepack-tools",						// PACKAGEDEBIAN_64
"musepack-tools",						// PACKAGEDEBIAN_32
"mpcdec",							// NAME1
"mppdec",							// NAME2
"mpc123",							// NAME3
&PrgInit.bool_mpc123_mppdec,					// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" musepack-tools",						// PAQUAGE
gettext_noop(" MusePack commandline utilities")},		// DESCRIPTION

{"musepack-tools",						// PACKAGEDEBIAN_64
"musepack-tools",						// PACKAGEDEBIAN_32
"mpcenc",							// NAME1
"mppenc",							// NAME2
"",								// NAME3
&PrgInit.bool_mppenc,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" musepack-tools",						// PAQUAGE
gettext_noop(" MusePack commandline utilities")},		// DESCRIPTION

{"mplayer",							// PACKAGEDEBIAN_64
"mplayer",							// PACKAGEDEBIAN_32
"mplayer",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_mplayer,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" mplayer",							// PAQUAGE
gettext_noop(" Lecteur et extracteur")},			// DESCRIPTION

{"mp3gain",							// PACKAGEDEBIAN_64
"mp3gain",							// PACKAGEDEBIAN_32
"mp3gain",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_mp3gain,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" mp3gain",							// PAQUAGE
gettext_noop(" Normaliseur de fichier mp3")},			// DESCRIPTION

{"normalize-audio",						// PACKAGEDEBIAN_64
"normalize-audio",						// PACKAGEDEBIAN_32
"normalize-audio",						// NAME1
"normalize",							// NAME2
"",								// NAME3
&PrgInit.bool_normalize,					// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" normalize-audio",						// PAQUAGE
gettext_noop(" Normaliseur de fichier wav")},			// DESCRIPTION

{"libnotify-bin",						// PACKAGEDEBIAN_64
"libnotify-bin",						// PACKAGEDEBIAN_32
"libnotify-bin",						// NAME1
"notify-send",							// NAME2
"",								// NAME3
&PrgInit.bool_notify_send,					// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" libnotify-bin",						// PAQUAGE
gettext_noop(" A program to send desktop notifications")},	// DESCRIPTION

{"shorten",							// PACKAGEDEBIAN_64
"shorten",							// PACKAGEDEBIAN_32
"shorten",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_shorten,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" shorten",							// PAQUAGE
gettext_noop(" Forte compression au format wave")},		// DESCRIPTION

{"shntool",							// PACKAGEDEBIAN_64
"shntool",							// PACKAGEDEBIAN_32
"shntool",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_shntool,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" shntool",							// PAQUAGE
gettext_noop(" Decoupage de fichiers sans decodage")},		// DESCRIPTION

{"sox",								// PACKAGEDEBIAN_64
"sox",								// PACKAGEDEBIAN_32
"sox",								// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_sox,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" sox",								// PAQUAGE
gettext_noop(" Transformation universelle de fichiers son")},	// DESCRIPTION

{"vorbis-tools",						// PACKAGEDEBIAN_64
"vorbis-tools",							// PACKAGEDEBIAN_32
"oggenc",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_oggenc,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" Vorbis-tools",						// PAQUAGE
gettext_noop(" Conversion wav : ogg")},				// DESCRIPTION

{"vorbisgain",							// PACKAGEDEBIAN_64
"vorbisgain",							// PACKAGEDEBIAN_32
"vorbisgain",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_vorbisgain,					// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" vorbisgain",							// PAQUAGE
gettext_noop(" Normaliseur de fichier ogg")},			// DESCRIPTION

{"wavpack",							// PACKAGEDEBIAN_64
"wavpack",							// PACKAGEDEBIAN_32
"wavpack",							// NAME1
"",								// NAME2
"",								// NAME3
&PrgInit.bool_wavpack,						// FOUND
NULL,								// NAME TREE-VIEW
NULL,								// PIXBUF TREE-VIEW
" wavpack",							// PAQUAGE
gettext_noop(" WAVPACK  Hybrid Lossless Audio Compressor")}	// DESCRIPTION

};



// 
// CHERCHE LA DISTRIBUTION EN RAPPORT AVEC LE PROGRAMME D' INSTALLATION
// 
void on_label_type_package_realize (GtkWidget *widget, gpointer user_data)
{
	gchar *New_Str = NULL;
	
	var_options.Adr_label_type_paquege = widget;
	
	if (libutils_find_file ("apt-get") == TRUE || libutils_find_file ("aptitude") == TRUE) {
		if (widget != NULL) {
			New_Str = g_strdup_printf (_(" <b>... Packets: deb</b> "));
		}
		var_options.Type_Package = _DEBIAN_;
	}
	else if (libutils_find_file ("slackpkg") == TRUE || libutils_find_file ("installpkg") == TRUE) {
		if (widget != NULL) {
			New_Str = g_strdup_printf (_(" <b>... Packets: tgz</b> "));
		}
		var_options.Type_Package = _TGZ_;
	}
	else if (libutils_find_file ("pacman-g2") == TRUE) {
		if (widget != NULL) {
			New_Str = g_strdup_printf (_(" <b>... Packets: fpm</b> "));
		}
		var_options.Type_Package = _FPM_;
	}
	else if (libutils_find_file ("rpm") == TRUE || libutils_find_file ("urpmi") == TRUE  || libutils_find_file ("zypper") == TRUE || libutils_find_file ("yum") == TRUE) {
		if (widget != NULL) {
			New_Str = g_strdup_printf (_(" <b>... Packets: rpm</b> "));
		}
		var_options.Type_Package = _RPM_;
	}
	else if (libutils_find_file ("yaourt") == TRUE || libutils_find_file ("pacman") == TRUE) {
		if (widget != NULL) {
			New_Str = g_strdup_printf (_(" <b>... Packets: pkg.tar.xz</b> "));
		}
		var_options.Type_Package = _ARCHLINUX_;
	}
	else {
		if (widget != NULL) {
			New_Str = g_strdup_printf (_(" <b>... Packets: ?</b> "));
		}
		var_options.Type_Package = _PACKAGE_NOT_FOUND_;
	}
	
	if (widget != NULL && New_Str != NULL) {
		gtk_label_set_markup (GTK_LABEL (var_options.Adr_label_type_paquege), New_Str);
		g_free (New_Str);
		New_Str = NULL;
	}
}
// GET INFOS FROM STRUCT
// 
gchar *prginit_get_name (TYPE_PROGINIT TypeEnum)
{
	return ((gchar *)TableauPrgExtern [ TypeEnum ] . PtrName);
}
// 
// 
GdkPixbuf *prginit_get_pixbuf (TYPE_PROGINIT TypeEnum)
{
	return ((GdkPixbuf *)TableauPrgExtern [ TypeEnum ] . Pixbuf);
}
// 
// 
gchar *prginit_get_Paquage (TYPE_PROGINIT TypeEnum)
{
	return ((gchar *)TableauPrgExtern [ TypeEnum ] . Paquage);
}
// 
// 
gchar *prginit_get_Description (TYPE_PROGINIT TypeEnum)
{
	return (gettext((gchar *)TableauPrgExtern [ TypeEnum ] . Description));
}
// 
// 
gboolean prginit_elem_is_present (TYPE_PROGINIT TypeEnum)
{
	return ((gboolean)*TableauPrgExtern [ TypeEnum ] . BoolFound);
}
// 
// 
void prginit_affiche_glist (void)
{
	gchar		*Name;
	gchar		*Paquage;
	GtkAdjustment	*Adj = NULL;
	gdouble		 AdjValue;
	GtkTreeIter	 iter;
	gint		 index;
	
	// EFFACER LE CONTENU DU TREEVIEW
	
	gtk_list_store_clear (GTK_LIST_STORE (var_prgextern.Adr_List_Store));
	
	// AJUSTEMENT DE LA LISTE
	
	Adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (var_prgextern.Adr_scroll));
	AdjValue = gtk_adjustment_get_value (Adj);
	gtk_adjustment_set_value (Adj, AdjValue);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (var_prgextern.Adr_scroll), Adj);
	
	for (index = 0; index < NMR_MAX_TABLEAU; index ++) {
	
		gtk_list_store_append (var_prgextern.Adr_List_Store, &iter);
		
		if (prginit_elem_is_present (index) == TRUE) {
			Name    = g_strdup_printf ("<span color=\"black\"><b>%s</b></span>", prginit_get_name (index));
			Paquage = g_strdup_printf ("<span color=\"black\"><b><i>%s</i></b></span>", prginit_get_Paquage (index));
		}
		else {
			Name    = g_strdup_printf ("<span color=\"red\"><b>%s</b></span>", prginit_get_name (index));
			Paquage = g_strdup_printf ("<span color=\"red\"><b><i>%s</i></b></span>", prginit_get_Paquage (index));
		}
		
		gtk_list_store_set (var_prgextern.Adr_List_Store, &iter,
					PRGEXT_NAME,		Name,
					PRGEXT_STATUS,		prginit_get_pixbuf (index),
					PRGEXT_PAQUAGE,		Paquage,
					PRGEXT_COMMENT,		prginit_get_Description (index),
					PRGEXT_NUM_COLUMN,	index,	
					-1);

		g_free (Name);		Name = NULL;
		g_free (Paquage);	Paquage = NULL;
	}
}
// 
// 
void prginit_print_info (void)
{	
	PRINT_FUNC_LF();
	
	if( TRUE == OptionsCommandLine.BoolVerboseMode ) {
		gint	index;
		
		g_print ("!------------------------------\n");
		g_print ("! INSTALL      NAME\n");
		g_print ("!------------------------------\n");
		for (index = 0; index < NMR_MAX_TABLEAU; index ++)
			g_print("!  %s   %s\n", (*TableauPrgExtern [ index ] . BoolFound == TRUE) ? "Yes      " : " NO  --> ", TableauPrgExtern [ index ] . PtrName);
		g_print ("!------------------------------\n\n");
	}
}
// 
// 
void prginit_scan (void)
{
	gint	index;
	
	for (index = 0; index < NMR_MAX_TABLEAU; index ++) {
		
		// INIT VAR
		
		*TableauPrgExtern [ index ] . BoolFound = FALSE;
		TableauPrgExtern [ index ] . PtrName    = NULL;

		// CHERCHE LE NOM APPROPRIE SUIVANT LA DISTRIBUTION UTILISEE
		
		*TableauPrgExtern [ index ] . BoolFound = libutils_find_file (TableauPrgExtern [ index ] . Name1);
		TableauPrgExtern [ index ] . PtrName = TableauPrgExtern [ index ] .Name1;
		
		if (*TableauPrgExtern [ index ] . BoolFound == FALSE) {
			if (*TableauPrgExtern [ index ] . Name2 != '\0') {
				if ((*TableauPrgExtern [ index ] . BoolFound = libutils_find_file (TableauPrgExtern [ index ] . Name2)) == TRUE) {
					TableauPrgExtern [ index ] . PtrName = TableauPrgExtern [ index ] .Name2;
				}
			}
		}

		if (*TableauPrgExtern [ index ] . BoolFound == FALSE) {
			if (*TableauPrgExtern [ index ] . Name3 != '\0') {
				if ((*TableauPrgExtern [ index ] . BoolFound = libutils_find_file (TableauPrgExtern [ index ] . Name3)) == TRUE) {
					TableauPrgExtern [ index ] . PtrName = TableauPrgExtern [ index ] .Name3;
				}
			}
		}
				
		// FIXER LE PIXBUF SI PAS EN SELECTION
		
		if (*TableauPrgExtern [ index ] . BoolFound == TRUE)
			TableauPrgExtern [ index ] . Pixbuf = var_prgextern.Pixbuf_Ok;
		else	TableauPrgExtern [ index ] . Pixbuf = var_prgextern.Pixbuf_Not_Ok;
	}
	
	prginit_affiche_glist ();
}
// 
// 
gboolean prginit_get_etat (void)
{
	prginit_scan ();
	
	var_prgextern.Bool_Goto_Page_Options =
		PrgInit.bool_a52dec &
		PrgInit.bool_cdparanoia &
		PrgInit.bool_cdda2wav &
		PrgInit.bool_flac &
		PrgInit.bool_lame &
		PrgInit.bool_oggenc &
		PrgInit.bool_sox &
		PrgInit.bool_normalize &
		PrgInit.bool_checkmp3 &
		PrgInit.bool_faad &
		PrgInit.bool_faac &
		PrgInit.bool_mplayer &
		PrgInit.bool_shorten &
		PrgInit.bool_wavpack &
		PrgInit.bool_ape &
		PrgInit.bool_lsdvd &
		PrgInit.bool_vorbisgain &
		PrgInit.bool_mp3gain &
		PrgInit.bool_shntool &
		PrgInit.bool_aacplusenc;
	
	return (var_prgextern.Bool_Goto_Page_Options);

}
// UPDATE DU TREEVIEW
// 
void on_button_scan_without_deb_clicked (GtkButton *button, gpointer user_data)
{
	if (var_prgextern.Adr_Tree_Model == NULL) return;
	prginit_scan ();
	prginit_affiche_glist ();
	//
	cdaudio_update_glist ();
	cdaudio_affiche_glist_audio ();
	cdaudio_set_flag_buttons ();
	//
	file_pixbuf_update_glist ();
	file_affiche_glist ();
	file_set_flag_buttons ();
}
// 
// 
gboolean prginit_key_press_event (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	return (TRUE);
}
// CREAATION DU SCROLLWINDOW ET DES EVENEMENTS ASSOCIES
// 
static void prginit_add_columns (GtkTreeView *treeview)
{
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel      *model = gtk_tree_view_get_model (treeview);

	// SIGNAL 'key-press-event'
	g_signal_connect(G_OBJECT(treeview),
			"key-press-event",
			(GCallback) prginit_key_press_event,
			model);
	
	// SIGNAL 'changed'
	var_prgextern.Adr_Line_Selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	gtk_tree_selection_set_mode (var_prgextern.Adr_Line_Selected, GTK_SELECTION_BROWSE);

	// PRGEXT_NAME
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.0, NULL);
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
						     renderer,
						     "markup", PRGEXT_NAME,
						     NULL);
	// gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
	// 			   GTK_TREE_VIEW_COLUMN_FIXED);
	// gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 130);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);

	// PRGEXT_STATUS
	renderer = gtk_cell_renderer_pixbuf_new ();
	g_object_set (renderer, "xalign", 0.5, NULL);
	column = gtk_tree_view_column_new_with_attributes (
							_("Status"),
							renderer,
							"pixbuf", PRGEXT_STATUS,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	// PRGEXT_PAQUAGE
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.0, NULL);
	column = gtk_tree_view_column_new_with_attributes (
							_("Packets"),
							renderer,
							"markup", PRGEXT_PAQUAGE,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 140);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
	
	
	// PRGEXT_COMMENT
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "xalign", 0.0, NULL);
	column = gtk_tree_view_column_new_with_attributes (
							_("Action"),
							renderer,
							"markup", PRGEXT_COMMENT,
							NULL);
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 160);
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (treeview, column);
}
// INITIALISATION DU SCROLLWINDOW
// 
void on_scrolledwindow_applications_externes_realize (GtkWidget *widget, gpointer user_data)
{
	GtkTreeModel *model;
	GtkWidget    *treeview;
	GtkListStore *store;

	var_prgextern.Pixbuf_Ok        = libutils_init_pixbufs ("prg-ok.png");
	var_prgextern.Pixbuf_Not_Ok    = libutils_init_pixbufs ("dbfs-no.png");

	var_prgextern.Adr_scroll = widget;
	var_prgextern.Adr_List_Store =
	store = gtk_list_store_new (PRGEXT_TOTAL_COLUMN,	// Nombre total de colonnes
				    G_TYPE_STRING,		// Name
				    GDK_TYPE_PIXBUF,		// Status
				    G_TYPE_STRING,		// Paquage
				    G_TYPE_STRING,		// Commentaire
				    G_TYPE_INT
				    );
	var_prgextern.Adr_Tree_Model = model = GTK_TREE_MODEL (store);
	treeview = gtk_tree_view_new_with_model (model);
	// gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);
	g_object_unref (model);
	gtk_container_add (GTK_CONTAINER (widget), treeview);
	prginit_add_columns (GTK_TREE_VIEW (treeview));
	prginit_scan ();
	prginit_print_info ();
	gtk_widget_show_all (widget);
}




