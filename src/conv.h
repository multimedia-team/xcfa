 /*
 *  file      : conv.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifndef conv_h
#define conv_h 1

#include <sys/types.h>
#include <signal.h>

typedef enum {
	_EXTRACT_ = 0,									// 
	_CONV_											// 
} TYPE_OPER;

typedef enum {										// Les types de conversions possibles
	NONE_CONV = 0,									// 
	FLAC_FLAC_TO_WAV,								// 
	FLAC_WAV_TO_FLAC,								// 
	LAME_WAV_TO_MP3,								// 
	LAME_FLAC_TO_MP3,								// 
	OGGENC_WAV_TO_OGG,								// 
	OGGENC_FLAC_TO_OGG,								// 
	OGG123_OGG_TO_WAV,								// 
	MPG321_MP3_TO_WAV,								// 
	SOX_WAV_TO_WAV,									// 
	SHORTEN_SHN_TO_WAV,								// 
	FAAD_M4A_TO_WAV,								// 
	FAAC_WAV_TO_M4A,								// 
	MPLAYER_WAV_TO_WAV,								// 
	MPLAYER_WMA_TO_WAV,								// 
	MPLAYER_AUDIO_TO_WAV,							// 
	MPLAYER_M4A_TO_WAV,								// 
	MPLAYER_OGG_TO_WAV,								// 
	MPLAYER_MP3_TO_WAV,								// 
	MPLAYER_MPC_TO_WAV,								// 
	MPLAYER_RM_TO_WAV,								// 
	MPLAYER_DTS_TO_WAV,								// 
	MPLAYER_AIFF_TO_WAV,							// 
	COPY_FILE,										// 
	NORMALISE_CALCUL,								// 
	NORMALISE_APPLIQUE,								// 
	NORMALISE_EXEC,									// 
	NORMALISE_GET_LEVEL,							// 
	MPPDEC_MPC_TO_WAV,								// 
	MPPENC_WAV_TO_MPC,								// 
	MAC_APE_TO_WAV,									// 
	MAC_WAV_TO_APE,									// 
	WAVPACK_WAV_TO_WAVPACK,							// 
	WVUNPACK_WAVPACK_TO_WAV,						// 
	CDPARANOIA_CD_TO_WAV,							// 
	CDPARANOIA_CD_TO_WAV_EXPERT,					// 
	CDPARANOIA_CD_TO_WAV_EXPERT_SEGMENT,			// 
	CDDA2WAV_CD_TO_WAV,								// 
	REPLAYGAIN,										// 
	LSDVD,											// 
	SPLIT,											// 
	CDDB_TOOL,										// 
	ICEDAX,											// 
	AACPLUSENC_WAV_TO_AAC,							// 
	WAVSPLIT_EXTRACT,								// 
	A52DEC_AC3_TO_WAV
	
} TYPE_CONV;

#define	MAX_ARG_CONV 200

typedef struct {
	gboolean	BoolIsExtract;						// 
	gboolean	BoolIsConvert;						// 
	gboolean	BoolIsCopy;							// 
	gboolean	BoolIsNormalise;					// 
	gboolean	BoolIsReplaygain;					// 
	gboolean	BoolIsa52dec;						// 
	
	GtkWidget	*Adr_label_info_wind;				// Adresse label information
	GtkWidget	*Adr_label_extract;					// Adresse label extraction
	GtkWidget	*Adr_progressbar_extract;			// Adresse Progressbar extraction
	GtkWidget	*Adr_label_conversion;				// Adresse label conversion
	GtkWidget	*Adr_progressbar_conversion;		// Adresse Progressbar conversion
	GtkWidget	*Adr_label_total;					// Adresse label total
	GtkWidget	*Adr_progressbar_total;				// Adresse Progressbar total
	
	double		extract_percent;					// Pourcentage actuel de progession
	gint		total_rip;							// Nombre total de fichiers a extraires
	gint		rip_completed;						// Nombre de fichiers extraits

	double		conversion_percent;					// Pourcentage actuel de progession
	gdouble		TimeMplayer;						// Pourcentage actuel de progession en temps

	gint		total_convert;						// Nombre total de fichiers a convertir
	gint		encode_completed;					// Nombre de fichiers convertis
	double		total_percent;						// Pourcentage TOTAL actuel de progession

	gboolean	BoolPutTextview;					// 
	GList		*ListPutTextview;					// 
	
	gboolean	bool_percent_conv;					// TRUE : print(conversion_percent, total_percent)
	gboolean	bool_percent_extract;				// TRUE : print(conversion_percent, total_percent)
		
	gboolean	bool_stop;							// TRUE : stop thread and timeout

	gchar		*TmpRep;							// Le repertoire temporaire d'extractions/conversions

	pid_t		code_fork_conv;						// Num Code Fork
	pid_t		code_fork_extract;					// Num Code Fork

	guint		handler_timeout_conv;				// Numerate timeout
	gboolean	bool_thread_conv;					// TRUE : thread in
	gboolean	bool_thread_extract;				// TRUE : thread in

	int		signal_numchildren_conv;				// the signal handler
	int		signal_numchildren_extract;				// the signal handler
	int		tube_conv [ 2 ];						// for pipe
	int		tube_extract [ 2 ];						// for pipe
	
	gboolean	Bool_MAJ_select;					// TRUE

	gchar		*ArgExtract [ MAX_ARG_CONV + 20 ];	// 
	
	double		value_PEAK_RMS_GROUP_ARGS;			// 
	
	gboolean	(*Func_request_stop) (void);		// 

} CONV;

extern CONV conv;

void		conv_inc_rip_completed (void);
void		conv_inc_encode_completed (void);
void		conv_stop_conversion (void);
void		conv_reset_struct (void *p_func);

gboolean	conv_to_convert( gchar **p_TabArgs, gboolean WithParameter, TYPE_CONV type_conv, gchar *info );

gboolean  	conv_exec_extract (gboolean WithParameter, TYPE_CONV type_conv, gchar *info);
gboolean  	conv_copy_src_to_dest (gchar *filesrc, gchar *filedest);
gchar 		**conv_with_sox_get_param (gchar *filesrc, gchar *filedest, gchar *frequence, gchar *voie, gchar *bits);
gchar		**conv_with_sox_float_get_param (gchar *filesrc, gchar *filedest);
gchar		**conv_lsdvd_read_get_param( gchar *p_PathDvd );
gchar		**conv_RemoveTab( gchar **p_PtrTabArgs );
gchar		**conv_AllocTabArgs( gint *p_tab );

#endif


