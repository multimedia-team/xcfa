 /*
 *  file    : tags_ogg.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "tags.h"
#include <taglib/tag_c.h>


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/


/*
*---------------------------------------------------------------------------
* GET HEADER
*---------------------------------------------------------------------------
*/
INFO_OGG *tagsogg_remove_info (INFO_OGG *info)
{
	if (info) {
		if (NULL != info->Channels)		{ g_free (info->Channels);		info->Channels = NULL;		}
		if (NULL != info->Rate)			{ g_free (info->Rate);			info->Rate = NULL;		}
		if (NULL != info->Nominal_bitrate)	{ g_free (info->Nominal_bitrate);	info->Nominal_bitrate = NULL;	}
		if (NULL != info->time)			{ g_free (info->time);			info->time = NULL;		}
		if (NULL != info->size)			{ g_free (info->size);			info->size = NULL;		}

		info->tags = (TAGS *)tags_remove (info->tags);

		g_free (info);
		info = NULL;
	}
	return ((INFO_OGG *)NULL);
}

INFO_OGG *tagsogg_get_info (DETAIL *detail)
{
	TagLib_File  *file;
	TagLib_Tag   *tag;
	INFO_OGG     *ptrinfo = NULL;
	const TagLib_AudioProperties *properties;
	gint		m;
	gint		s;
	gint		sec;

	ptrinfo = (INFO_OGG *)g_malloc0 (sizeof (INFO_OGG));
	if (ptrinfo == NULL) return (NULL);
	ptrinfo->tags = (TAGS *)tags_alloc (FALSE);
	
	if ((file = taglib_file_new (detail->namefile))) {
	
		taglib_set_strings_unicode(FALSE);
		tag = taglib_file_tag(file);
		properties = taglib_file_audioproperties(file);
		
		ptrinfo->tags->Title     = g_strdup (taglib_tag_title(tag));
		ptrinfo->tags->Artist    = g_strdup (taglib_tag_artist(tag));
		ptrinfo->tags->Album     = g_strdup (taglib_tag_album(tag));
		ptrinfo->tags->IntYear   = taglib_tag_year(tag);
		ptrinfo->tags->Year      = g_strdup_printf ("%d", ptrinfo->tags->IntYear);
		ptrinfo->tags->Comment   = g_strdup (taglib_tag_comment(tag));
		ptrinfo->tags->IntNumber = taglib_tag_track(tag);
		ptrinfo->tags->Number    = g_strdup_printf ("%d", ptrinfo->tags->IntNumber);
		ptrinfo->tags->Genre     = g_strdup (taglib_tag_genre(tag));
		ptrinfo->tags->IntGenre  = tags_get_genre_by_value (ptrinfo->tags->Genre);

		ptrinfo->Channels        = g_strdup_printf ("%d", taglib_audioproperties_channels (properties));
		ptrinfo->Rate            = g_strdup_printf ("%d", taglib_audioproperties_samplerate (properties));
		ptrinfo->Nominal_bitrate = g_strdup_printf ("%d", taglib_audioproperties_bitrate (properties));

		ptrinfo->SecTime =
		sec = taglib_audioproperties_length(properties);
		s = sec % 60; sec /= 60;
		m = sec % 60; sec /= 60;
		if (sec > 0) ptrinfo->time = g_strdup_printf ("%02d:%02d:%02d", sec, m, s);
		else         ptrinfo->time = g_strdup_printf ("%02d:%02d", m, s);
		
		ptrinfo->size            = g_strdup_printf ("%d Ko", (gint)libutils_get_size_file (detail->namefile) / 1024);
			
		taglib_tag_free_strings();
		taglib_file_free (file);
	}
	/*
	ptrinfo->level = level_get_from (FILE_IS_OGG, namefile);
g_print ("%s\t%d\n", namefile, ptrinfo->level);
	*/
	ptrinfo->LevelDbfs.level = -1;
	ptrinfo->LevelDbfs.NewLevel = -1;

	return (ptrinfo);
}

