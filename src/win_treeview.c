 /*
 *  file      : win_treeview.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "dvd.h"
#include "cd_audio.h"
#include "file.h"
#include "configuser.h"
#include "win_treeview.h"



GtkBuilder	*GtkBuilderProjet_wind_treeview = NULL;
GtkWidget	*WindMain_wind_treeview = NULL;



#define MAX_DATAS_STRUCT_RADIOBUTTONS 6

typedef struct {
	gchar			 	*NameLeft;
	gchar			 	*NameCenter;
	gchar			 	*NameRight;
	GtkCellRenderer		**AdrRenderer;
	GtkTreeViewColumn	**AdrTreeViewColumn;
	gint			  	Pos;
} TAB_POSNAME;

TAB_POSNAME TabPosName [ MAX_DATAS_STRUCT_RADIOBUTTONS ] = {
	{"radiobutton_nameleft_dvd", "radiobutton_namecenter_dvd", "radiobutton_nameright_dvd",
	&var_dvd.Renderer, &var_dvd.Adr_Column_Nom,
	1},
	{"radiobutton_nameleft_cd", "radiobutton_namecenter_cd", "radiobutton_nameright_cd",
	&var_cd.Renderer, &var_cd.Adr_Column_Nom,
	1},
	{"radiobutton_nameleft_allfile", "radiobutton_namecenter_allfile", "radiobutton_nameright_allfile",
	&var_file.Renderer, &var_file.Adr_ColumnFileName,
	1},
	{"radiobutton_nameleft_wav", "radiobutton_namecenter_wav", "radiobutton_nameright_wav",
	&var_file_wav.Renderer, &var_file_wav.Adr_ColumnFileWavName,
	1},
	{"radiobutton_nameleft_mp3ogg", "radiobutton_namecenter_mp3ogg", "radiobutton_nameright_mp3ogg",
	&var_file_mp3ogg.Renderer, &var_file_mp3ogg.Adr_ColumnFileMp3OggName,
	1},
	{"radiobutton_nameleft_tags", "radiobutton_namecenter_tags", "radiobutton_nameright_tags",
	&var_file_tags.Renderer, &var_file_tags.Adr_ColumnFileTagsName,
	1}
	} ;




#define POS_STRUCT_DVD			00
#define POS_STRUCT_CD			07
#define POS_STRUCT_CONVERSIONS		21
#define POS_STRUCT_WAV			37
#define POS_STRUCT_MP3OGG		44
#define POS_STRUCT_TAGS			50
#define MAX_DATAS_STRUCT_TREEVIEW	53

typedef struct {

	GtkWidget	*AdrWin;
	gboolean	IsShow;
	gboolean	BoolNoEnter;
	gint		PageNotebook;

} VAR_TREEVIEW;

VAR_TREEVIEW VarTreeView = {
	NULL,
	FALSE,
	TRUE,
	0
};

typedef struct {
	gchar			 *Name;
	gboolean		  BoolIsVisible;
	GtkTreeViewColumn	**AdrTreeViewColumn;
} TAB_SHOWHIDE;

TAB_SHOWHIDE TabShowHide [ MAX_DATAS_STRUCT_TREEVIEW ] = {
// 	
// 	Name								BoolIsVisible: DVD, CD, FILE, WAV, MP3OGG, TAGS
// 										|     AdrTreeViewColumn
// 	  POS_STRUCT_DVD = 0            	|     |                
	{"checkbutton_titre_dvd",			TRUE,  &var_dvd.Adr_Column_Titres},
	{"checkbutton_play_dvd",			TRUE,  &var_dvd.Adr_Column_Play},
	{"checkbutton_temps_dvd",			TRUE,  &var_dvd.Adr_Column_Temps},
	{"checkbutton_format_dvd",			TRUE,  &var_dvd.Adr_Column_Format},
	{"checkbutton_choix_dvd",			TRUE,  &var_dvd.Adr_Column_Choix},
	{"checkbutton_normalise_dvd",		TRUE,  &var_dvd.Adr_Column_Normalise},
	{"checkbutton_nom_dvd",				TRUE,  &var_dvd.Adr_Column_Nom},
	// POS_STRUCT_CD = 7
	{"checkbutton_play_cd",				TRUE, &var_cd.Adr_Column_Play},
	{"checkbutton_wav_cd",				TRUE, &var_cd.Adr_Column_Wav},
	{"checkbutton_flac_cd",				TRUE, &var_cd.Adr_Column_Flac},
	{"checkbutton_ape_cd",				TRUE, &var_cd.Adr_Column_Ape},
	{"checkbutton_wavpack_cd",			TRUE, &var_cd.Adr_Column_Wavpack},
	{"checkbutton_ogg_cd",				TRUE, &var_cd.Adr_Column_Ogg},
	{"checkbutton_m4a_cd",				TRUE, &var_cd.Adr_Column_M4a},
	{"checkbutton_aac_cd",				TRUE, &var_cd.Adr_Column_Aac},
	{"checkbutton_musepack_cd",			TRUE, &var_cd.Adr_Column_Mpc},
	{"checkbutton_mp3_cd",				TRUE, &var_cd.Adr_Column_Mp3},
	{"checkbutton_num_cd",				TRUE, &var_cd.Adr_Column_Num},
	{"checkbutton_time_cd",				TRUE, &var_cd.Adr_Column_Time},
	{"checkbutton_normalise_cd",		TRUE, &var_cd.Adr_Column_Normalise},
	{"checkbutton_nom_cd",				TRUE, &var_cd.Adr_Column_Nom},
	// POS_STRUCT_CONVERSIONS = 21
	{"checkbutton_play_allfile",		TRUE, &var_file.Adr_ColumnFilePlay},
	{"checkbutton_trash_allfile",		TRUE, &var_file.Adr_ColumnFileTrash},
	{"checkbutton_type_allfile",		TRUE, &var_file.Adr_ColumnFileType},
	{"checkbutton_wav_allfile",			TRUE, &var_file.Adr_ColumnFileWav},
	{"checkbutton_flac_allfile",		TRUE, &var_file.Adr_ColumnFileFlac},
	{"checkbutton_ape_allfile",			TRUE, &var_file.Adr_ColumnFileApe},
	{"checkbutton_wavpack_allfile",		TRUE, &var_file.Adr_ColumnFileWavPack},
	{"checkbutton_ogg_allfile",			TRUE, &var_file.Adr_ColumnFileOgg},
	{"checkbutton_m4a_allfile",			TRUE, &var_file.Adr_ColumnFileM4a},
	{"checkbutton_aac_allfile",			TRUE, &var_file.Adr_ColumnFileAac},
	{"checkbutton_musepack_allfile",	TRUE, &var_file.Adr_ColumnFileMpc},
	{"checkbutton_mp3_allfile",			TRUE, &var_file.Adr_ColumnFileMp3},
	{"checkbutton_time_allfile",		TRUE, &var_file.Adr_ColumnFileTime},
	{"checkbutton_normalise_allfile",	TRUE, &var_file.Adr_ColumnFileNormalize},
	{"checkbutton_replaygain_allfile",	TRUE, &var_file.Adr_ColumnFileReplayGain},
	{"checkbutton_nom_allfile",			TRUE, &var_file.Adr_ColumnFileName},
	// POS_STRUCT_WAV = 37
	{"checkbutton_hertz_wav",			TRUE, &var_file_wav.Adr_ColumnFileWavHertz},
	{"checkbutton_newhertz_wav",		TRUE, &var_file_wav.Adr_ColumnFileWavNewHertz},
	{"checkbutton_voie_wav",			TRUE, &var_file_wav.Adr_ColumnFileWavVoie},
	{"checkbutton_newvoie_wav",			TRUE, &var_file_wav.Adr_ColumnFileWavNewVoie},
	{"checkbutton_bits_wav",			TRUE, &var_file_wav.Adr_ColumnFileWavBits},
	{"checkbutton_newbits_wav",			TRUE, &var_file_wav.Adr_ColumnFileWavNewBits},
	{"checkbutton_nom_wav",				TRUE, &var_file_wav.Adr_ColumnFileWavName},
	// POS_STRUCT_MP3OGG = 44
	{"checkbutton_type_mp3ogg",			TRUE, &var_file_mp3ogg.Adr_ColumnFileMp3OggType},
	{"checkbutton_bitrate_mp3ogg",		TRUE, &var_file_mp3ogg.Adr_ColumnFileMp3OggBitrate},
	{"checkbutton_nexbitrate_mp3ogg",	TRUE, &var_file_mp3ogg.Adr_ColumnFileMp3OggNewBitrate},
	{"checkbutton_size_mp3ogg",			TRUE, &var_file_mp3ogg.Adr_ColumnFileMp3OggSize},
	{"checkbutton_time_mp3ogg",			TRUE, &var_file_mp3ogg.Adr_ColumnFileMp3OggTime},
	{"checkbutton_nom_mp3ogg",			TRUE, &var_file_mp3ogg.Adr_ColumnFileMp3OggName},
	// POS_STRUCT_TAGS = 50
	{"checkbutton_type_tags",			TRUE, &var_file_tags.Adr_ColumnFileTagsType},
	{"checkbutton_time_tags",			TRUE, &var_file_tags.Adr_ColumnFileTagsTime},
	{"checkbutton_nom_tags",			TRUE, &var_file_tags.Adr_ColumnFileTagsName}
	};



// SET ETAT FIELDS TO STRING-ETAT FOR SAVE CONFIG
// 
void treeview_get_etat_fields (void)
{
	gint cpt;
	
	VarTreeView.BoolNoEnter = TRUE;
	
	// Remove string
	if (NULL != Config.StringBoolFieldsIsVisible) {
		g_free (Config.StringBoolFieldsIsVisible);
		Config.StringBoolFieldsIsVisible = NULL;
	}
	// Allocate string
	Config.StringBoolFieldsIsVisible = g_strnfill (MAX_DATAS_STRUCT_TREEVIEW +4, '1');

	// Set 1 (TRUE) is visible else set 0 (FALSE)
	for (cpt = POS_STRUCT_DVD; cpt < MAX_DATAS_STRUCT_TREEVIEW; cpt ++) {
		Config.StringBoolFieldsIsVisible [ cpt ] = (TabShowHide [ cpt ].BoolIsVisible == FALSE) ? '0' : '1';
	}
	Config.StringBoolFieldsIsVisible [ cpt ] = '\0';
	
	VarTreeView.BoolNoEnter = FALSE;
}
// CALL FROM MAIN.C
// 
void wintreeview_set_etat_fields (void)
{
	gint cpt;

	if( NULL == GtkBuilderProjet_wind_treeview ) {
		GtkBuilderProjet_wind_treeview = Builder_open( "xcfa_win_treeview.glade", GtkBuilderProjet_wind_treeview );
	}
	if( NULL == WindMain_wind_treeview ) {
		WindMain_wind_treeview = GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "wind_treeview" ));
	}

	if (NULL == Config.StringBoolFieldsIsVisible) {
		treeview_get_etat_fields ();
	}
	
	VarTreeView.BoolNoEnter = TRUE;
	
	// Set 1 (TRUE) is visible else set 0 (FALSE)
	for (cpt = POS_STRUCT_DVD; cpt < MAX_DATAS_STRUCT_TREEVIEW; cpt ++) {
		TabShowHide [ cpt ].BoolIsVisible = (Config.StringBoolFieldsIsVisible [ cpt ] == '0') ? FALSE : TRUE;
		// Place le CheckButton avec cet etat
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabShowHide [ cpt ].Name)), TabShowHide [ cpt ].BoolIsVisible);
		gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (*TabShowHide [ cpt ].AdrTreeViewColumn), TabShowHide [ cpt ].BoolIsVisible);
	}
	
	VarTreeView.BoolNoEnter = FALSE;
}
// 
// 
gboolean on_wind_treeview_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	treeview_get_etat_fields ();
	gtk_widget_destroy( WindMain_wind_treeview );
	WindMain_wind_treeview = NULL;
	GtkBuilderProjet_wind_treeview = NULL;
	return TRUE;
}
// 
// 
gboolean on_wind_treeview_destroy_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	treeview_get_etat_fields ();
	gtk_widget_destroy( WindMain_wind_treeview );
	WindMain_wind_treeview = NULL;
	GtkBuilderProjet_wind_treeview = NULL;
	return TRUE;
}
// 
// 
void on_button_quit_windtreeview_clicked (GtkButton *button, gpointer user_data)
{
	treeview_get_etat_fields ();
	gtk_widget_destroy( WindMain_wind_treeview );
	WindMain_wind_treeview = NULL;
	GtkBuilderProjet_wind_treeview = NULL;
}
// 
// 
void treeview_SetGlobal (gboolean BoolVisible)
{
	gint	cpt;

	VarTreeView.BoolNoEnter = TRUE;

	switch(VarTreeView.PageNotebook) {
	case 0 :
		for (cpt = POS_STRUCT_DVD; cpt < POS_STRUCT_CD; cpt ++) {
			TabShowHide [ cpt ].BoolIsVisible = BoolVisible;
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabShowHide [ cpt ].Name)), TabShowHide [ cpt ].BoolIsVisible);
			gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (*TabShowHide [ cpt ].AdrTreeViewColumn), TabShowHide [ cpt ].BoolIsVisible);
		}
		break;
	case 1 :
		for (cpt = POS_STRUCT_CD; cpt < POS_STRUCT_CONVERSIONS; cpt ++) {
			TabShowHide [ cpt ].BoolIsVisible = BoolVisible;
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabShowHide [ cpt ].Name)), TabShowHide [ cpt ].BoolIsVisible);
			gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (*TabShowHide [ cpt ].AdrTreeViewColumn), TabShowHide [ cpt ].BoolIsVisible);
		}
		break;
	case 2 :
		for (cpt = POS_STRUCT_CONVERSIONS; cpt < POS_STRUCT_WAV; cpt ++) {
			TabShowHide [ cpt ].BoolIsVisible = BoolVisible;
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabShowHide [ cpt ].Name)), TabShowHide [ cpt ].BoolIsVisible);
			gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (*TabShowHide [ cpt ].AdrTreeViewColumn), TabShowHide [ cpt ].BoolIsVisible);
		}
		break;
	case 3 :
		for (cpt = POS_STRUCT_WAV; cpt < POS_STRUCT_MP3OGG; cpt ++) {
			TabShowHide [ cpt ].BoolIsVisible = BoolVisible;
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabShowHide [ cpt ].Name)), TabShowHide [ cpt ].BoolIsVisible);
			gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (*TabShowHide [ cpt ].AdrTreeViewColumn), TabShowHide [ cpt ].BoolIsVisible);
		}
		break;
	case 4 :
		for (cpt = POS_STRUCT_MP3OGG; cpt < MAX_DATAS_STRUCT_TREEVIEW; cpt ++) {
			TabShowHide [ cpt ].BoolIsVisible = BoolVisible;
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabShowHide [ cpt ].Name)), TabShowHide [ cpt ].BoolIsVisible);
			gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (*TabShowHide [ cpt ].AdrTreeViewColumn), TabShowHide [ cpt ].BoolIsVisible);
		}
		break;
	case 5 :
		for (cpt = POS_STRUCT_TAGS; cpt < MAX_DATAS_STRUCT_TREEVIEW; cpt ++) {
			TabShowHide [ cpt ].BoolIsVisible = BoolVisible;
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabShowHide [ cpt ].Name)), TabShowHide [ cpt ].BoolIsVisible);
			gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (*TabShowHide [ cpt ].AdrTreeViewColumn), TabShowHide [ cpt ].BoolIsVisible);
		}
		break;
	}
	treeview_get_etat_fields ();
	VarTreeView.BoolNoEnter = FALSE;
}
// 
// 
void on_button_deselect_global_windtreeview_clicked (GtkButton *button, gpointer user_data)
{
	treeview_SetGlobal (FALSE);
}
// 
// 
void on_button_select_global_windtreeview_clicked (GtkButton *button, gpointer user_data)
{
	treeview_SetGlobal (TRUE);
}
// 
// 
void treeview_set_pos_fields_name (void)
{
	if (NULL != Config.StringPosFieldsName && *Config.StringPosFieldsName == '\0') {	
		g_free (Config.StringPosFieldsName);	Config.StringPosFieldsName = NULL;
	}
	
	if (NULL == Config.StringPosFieldsName) {
		// Allocate string with datas
		Config.StringPosFieldsName = g_strnfill (MAX_DATAS_STRUCT_RADIOBUTTONS +4, '1');
	}
}
// 
// 
void on_radiobutton_modif_treeview_clicked (GtkButton *button, gpointer user_data)
{
	gint cpt;

	if (TRUE == VarTreeView.BoolNoEnter) return;
	
	treeview_set_pos_fields_name ();
	
	for (cpt = POS_STRUCT_DVD; cpt < MAX_DATAS_STRUCT_RADIOBUTTONS; cpt ++) {
		if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabPosName [ cpt ].NameLeft)))) {
			Config.StringPosFieldsName [ cpt ] = '0';
			TabPosName [ cpt ].Pos = 0;
			gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (*TabPosName [ cpt ].AdrTreeViewColumn), 0.0);
			g_object_set (*TabPosName [ cpt ].AdrRenderer, "xalign", 0.0, NULL);
			break;
		}
		if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabPosName [ cpt ].NameCenter)))) {
			Config.StringPosFieldsName [ cpt ] = '1';
			TabPosName [ cpt ].Pos = 1;
			gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (*TabPosName [ cpt ].AdrTreeViewColumn), 0.5);
			g_object_set (*TabPosName [ cpt ].AdrRenderer, "xalign", 0.5, NULL);
			break;
		}
		if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabPosName [ cpt ].NameRight)))) {
			Config.StringPosFieldsName [ cpt ] = '2';
			TabPosName [ cpt ].Pos = 2;
			gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (*TabPosName [ cpt ].AdrTreeViewColumn), 1.0);
			g_object_set (*TabPosName [ cpt ].AdrRenderer, "xalign", 1.0, NULL);
			break;
		}
	}
}
// 
// 
void wintreeview_radiobutton_set_PosFieldsName (void)
{
	gint cpt;

	treeview_set_pos_fields_name ();
	
	VarTreeView.BoolNoEnter = TRUE;
	
	for (cpt = POS_STRUCT_DVD; cpt < MAX_DATAS_STRUCT_RADIOBUTTONS; cpt ++) {
		if (Config.StringPosFieldsName [ cpt ] == '0') {
			TabPosName [ cpt ].Pos = 0;
			gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (*TabPosName [ cpt ].AdrTreeViewColumn), 0.0);
			g_object_set (*TabPosName [ cpt ].AdrRenderer, "xalign", 0.0, NULL);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabPosName [ cpt ].NameLeft)), TRUE);
		}
		else if (Config.StringPosFieldsName [ cpt ] == '1') {
			TabPosName [ cpt ].Pos = 1;
			gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (*TabPosName [ cpt ].AdrTreeViewColumn), 0.5);
			g_object_set (*TabPosName [ cpt ].AdrRenderer, "xalign", 0.5, NULL);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabPosName [ cpt ].NameCenter)), TRUE);
		}
		else if (Config.StringPosFieldsName [ cpt ] == '2') {
			TabPosName [ cpt ].Pos = 2;
			gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (*TabPosName [ cpt ].AdrTreeViewColumn), 1.0);
			g_object_set (*TabPosName [ cpt ].AdrRenderer, "xalign", 1.0, NULL);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabPosName [ cpt ].NameRight)), TRUE);
		}
	}
	VarTreeView.BoolNoEnter = FALSE;

	gtk_widget_destroy( WindMain_wind_treeview );
	WindMain_wind_treeview = NULL;
	GtkBuilderProjet_wind_treeview = NULL;
}
// ACTIVE OU DESACTIVE LA VISIBILITE DU CHAMPS SUITE A CLICK BUTTON
// 
void on_checkbutton_modif_treeview_clicked (GtkButton *button, gpointer user_data)
{
	gint cpt;
	
	if (TRUE == VarTreeView.BoolNoEnter) return;
	
	for (cpt = POS_STRUCT_DVD; cpt < MAX_DATAS_STRUCT_TREEVIEW; cpt ++) {
		if (GTK_BUTTON (button) == GTK_BUTTON (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, TabShowHide [ cpt ].Name))) {
			// g_print("cpt = %d\n", cpt);
			TabShowHide [ cpt ].BoolIsVisible = (TabShowHide [ cpt ].BoolIsVisible == TRUE) ? FALSE : TRUE;
			// Show / Hide la colonne du TreeView
			gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (*TabShowHide [ cpt ].AdrTreeViewColumn), TabShowHide [ cpt ].BoolIsVisible);
			// sortie
			break;
		}
	}
}
// 
// 
void on_notebook_windtreeview_switch_page( GtkNotebook *notebook, gpointer page, guint page_num, gpointer user_data )
{
	VarTreeView.PageNotebook = page_num;

	switch(VarTreeView.PageNotebook) {
	case 0 :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_general")), NOTEBOOK_DVD_AUDIO);
		break;
	case 1 :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_general")), NOTEBOOK_CD_AUDIO);
		break;
	case 2 :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_general")), NOTEBOOK_FICHIERS);
			gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_in_file")), NOTEBOOK_FICHIERS_CONVERSION);
		break;
	case 3 :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_general")), NOTEBOOK_FICHIERS);
			gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_in_file")), NOTEBOOK_FICHIERS_WAV);
		break;
	case 4 :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_general")), NOTEBOOK_FICHIERS);
			gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_in_file")), NOTEBOOK_FICHIERS_MP3OGG);
		break;
	case 5 :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_general")), NOTEBOOK_FICHIERS);
			gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkXcfaProjet, "notebook_in_file")), NOTEBOOK_FICHIERS_TAGS);
		
		break;
	}
}
// 
// 
gboolean wintreeview_init( GtkWidget *FromWindMain )
{
	if( NULL == (GtkBuilderProjet_wind_treeview = Builder_open( "xcfa_win_treeview.glade", GtkBuilderProjet_wind_treeview ))) {
		return( EXIT_FAILURE );
	}
	// Add language support from glade file
	gtk_builder_set_translation_domain(  GtkBuilderProjet_wind_treeview, "wind_treeview" );
	gtk_builder_connect_signals( GtkBuilderProjet_wind_treeview, NULL );

	WindMain_wind_treeview = GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "wind_treeview" ));

	if( NULL != FromWindMain ) {
		gtk_window_set_transient_for (GTK_WINDOW(WindMain_wind_treeview), GTK_WINDOW(FromWindMain));
		gtk_window_set_modal (GTK_WINDOW(WindMain_wind_treeview), TRUE);
	}
	libutils_set_default_icone_to_win( WindMain_wind_treeview );

	gtk_window_set_title( GTK_WINDOW( WindMain_wind_treeview ), "Modification des champs" );

	gtk_widget_show_all( WindMain_wind_treeview );

	wintreeview_set_etat_fields ();
		
	switch (Config.NotebookGeneral) {
	case NOTEBOOK_DVD_AUDIO :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "notebook_windtreeview")), 0);
		break;
	case NOTEBOOK_CD_AUDIO :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "notebook_windtreeview")), 1);
		break;
	case NOTEBOOK_FICHIERS :
		switch (Config.NotebookFile) {
		case NOTEBOOK_FICHIERS_CONVERSION :
			gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "notebook_windtreeview")), 2);
			break;
		case NOTEBOOK_FICHIERS_WAV :
			gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "notebook_windtreeview")), 3);
			break;
		case NOTEBOOK_FICHIERS_MP3OGG :	
			gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "notebook_windtreeview")), 4);
			break;
		case NOTEBOOK_FICHIERS_TAGS :
			gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "notebook_windtreeview")), 5);
			break;
		}
		break;
	default :
		gtk_notebook_set_current_page (GTK_NOTEBOOK (gtk_builder_get_object( GtkBuilderProjet_wind_treeview, "notebook_general")), NOTEBOOK_DVD_AUDIO);
	}
	
	return( TRUE );
}
	
	
	
	
	
	
	
	
	
	
	
	

