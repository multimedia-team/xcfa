 /*
 *  file      : wind_about.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */

 
 
#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <stdlib.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "get_info.h"
#include "web.h"
#include "win_about.h"



GtkBuilder	*GtkBuilderProjet_wind_about = NULL;
GtkWidget	*WindMain_wind_about = NULL;



// 
// 
gboolean on_wind_about_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gtk_widget_destroy( WindMain_wind_about );
	WindMain_wind_about = NULL;
	GtkBuilderProjet_wind_about = NULL;
	return (TRUE);
}
// 
// 
gboolean on_wind_about_destroy_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gtk_widget_destroy( WindMain_wind_about );
	WindMain_wind_about = NULL;
	GtkBuilderProjet_wind_about = NULL;
	return (TRUE);
}
// 
// 
void on_button_ok_about_clicked (GtkButton *button, gpointer user_data)
{
	gtk_widget_destroy( WindMain_wind_about );
	WindMain_wind_about = NULL;
	GtkBuilderProjet_wind_about = NULL;
}
// 
// 
void on_button_contrib_about_clicked (GtkButton *button, gpointer user_data)
{
	typedef struct {
		gchar  *NameWidget;
		gchar  *Url;
	} TYPE_BUTTON;
	#define MAX_TYPE_BUTTON_PAGE_WEB 21
	TYPE_BUTTON TypeButton [ MAX_TYPE_BUTTON_PAGE_WEB ] = {
	{"button_pagexcfa_about",		"http://www.xcfa.tuxfamily.org"},
	{"button01_contrib_about",		"http://www.road2mayotte.org/blook/"},						/* TODO */
	{"button02_contrib_about",		"http://e17blog.tuxfamily.org/index.php"},					/* TODO ok */
	{"button03_contrib_about",		"http://ubunteros.tuxfamily.org/spip.php?page=plan"},
	{"button04_contrib_about",		"http://www.jesuislibre.org"},
	{"button05_contrib_about",		"http://audiobezon.dlinkddns.com/"},						/* TODO ok */
	{"button06_contrib_about",		"http://www.tuxfamily.org"},
	{"button07_contrib_about",		"http://www.framasoft.net/article4492.html"},
	{"button08_contrib_about",		"http://ekd.tuxfamily.org"},
	{"button09_contrib_about",		"http://defis-libristes.tuxfamily.org/viewforum.php?id=2"},	/* TODO */
	{"button10_contrib_about",		"http://doc.ubuntu-fr.org/xcfa"},
	{"button11_contrib_about",		"http://unm.arcis.free.fr/spip.php?article109"},
	{"button12_contrib_about",		"http://ubunteros.tuxfamily.org/spip.php?article168"},
	{"button13_contrib_about",		"http://ubunteros.tuxfamily.org/spip.php?article159"},
	{"button14_contrib_about",		"http://ubunteros.tuxfamily.org/spip.php?article30"},
	{"button15_contrib_about",		"http://linuxerie.midiblogs.com/"},
	{"button16_contrib_about",		"http://www.deb-multimedia.org/"},
	{"button17_contrib_about",		"http://frugalware.org/"},
	{"button20_contrib_about",		"http://opera-info.de/forum/thread.php?threadid=20506"},	/* TODO ok*/
	{"button21_contrib_about",		"http://linuxforen.de/forums/showthread.php?p=1693658"},
	{"button22_contrib_about",		"http://www.alessiotreglia.com/"}
	};

	gint cpt;

	for (cpt = 0; cpt < MAX_TYPE_BUTTON_PAGE_WEB; cpt ++) {
		if (GTK_BUTTON (button) == GTK_BUTTON (GTK_WIDGET (gtk_builder_get_object( GtkBuilderProjet_wind_about, TypeButton [ cpt ].NameWidget)))) {
			web_goto_url (TypeButton [ cpt ].Url);
			break;
		}
	}
}
// 
// 
void on_label_button_pagexcfa_about_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_label_set_markup (GTK_LABEL (widget), _("<span foreground=\"#0000FF\">  Xcfa on the web</span>"));
}
// 
// 
void on_textview_label_license_about_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_text_view_set_editable (GTK_TEXT_VIEW (widget), FALSE);
	gtk_text_view_set_justification (GTK_TEXT_VIEW (widget), GTK_JUSTIFY_CENTER);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (widget), FALSE);
	gtk_text_buffer_set_text (gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget)),
	"\n\nCopyright (C) 2003 - 2015\n\n\n"
	"XCFA is free software; you can redistribute it and/or modify\n"
	"it under the terms of the GNU General Public License as published by\n"
	"the Free Software Foundation; either version 3 of the License, or\n"
	"(at your option) any later version.\n\n"
	"XCFA is distributed in the hope that it will be useful,\n"
	"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n"
	"GNU General Public License for more details.\n\n"
	"You should have received a copy of the GNU General Public License\n"
	"along with this program; if not, see\n"
	"http://www.gnu.org/licenses\n"
	"or write to\n"
	"the Free Software Foundation,Inc.\n"
	"51 Franklin Street\n"
	"Fifth Floor\n"
	"Boston, MA 02110-1301 USA\n\n------------\n\n"
	
	"XCFA est un logiciel libre, vous pouvez le redistribuer\n"
	"et / ou le modifier selon les termes de la Licence Publique Générale GNU\n"
	"telle que publiée par la Free Software Foundation; soit la version 3 de\n"
	"la Licence, ou (à votre choix) toute version ultérieure.\n\n"
	"XCFA est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,\n"
	"sans même la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN\n"
	"USAGE PARTICULIER. Voir la Licence Publique Générale GNU pour plus de détails.\n\n"
	"Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec\n"
	"ce programme, sinon, voir\n"
	"http://www.gnu.org/licenses\n"
	"ou écrivez à la\n"
	"Free Software Foundation, Inc,\n"
	"51 Franklin Street\n"
	"Fifth Floor\n"
	"Boston, Massachusetts, USA 02110-1301\n"
	
	, -1);
}
// 
// 
void on_label_contrib_about_realize (GtkWidget *widget, gpointer user_data)
{
#define TEXTE _("\n<span foreground=\"#00000F\" weight=\"normal\"><i>" \
		"<b>Au tout debut:</b>\n\n" \
		"Serres Patrick, Hamann Regis, Fabien Gregis, @Dzef, @alteo_gange, \n" \
		"@Leonux, @zarer, @Shankarius, @bishop, @Ludo, @Patachonf,\n" \
		"...  ;-)\n\n" \
		"</i></span>")

	gtk_label_set_use_markup (GTK_LABEL (widget), TRUE);
	gtk_label_set_justify (GTK_LABEL (widget), GTK_JUSTIFY_CENTER);
	gtk_label_set_markup (GTK_LABEL (widget), TEXTE);
}
// 
// 
void AboutXcfa_info_machine (GtkWidget *widget, gchar *text)
{
	gchar *save = NULL;
	
	save = g_strdup_printf ("%s%s", gtk_label_get_label (GTK_LABEL(widget)), text);
	
	gtk_label_set_use_markup (GTK_LABEL (widget), TRUE);
	gtk_label_set_justify (GTK_LABEL (widget), GTK_JUSTIFY_LEFT);
	gtk_label_set_markup (GTK_LABEL (widget), save);
	gtk_widget_show (widget);
	
	g_free (save);
	save = NULL;
}
// 
// 
void on_label_about_machine_realize (GtkWidget *widget, gpointer user_data)
{
	gchar *ptr = NULL;
	
	ptr = GetInfo_cpu_str ();
	AboutXcfa_info_machine (widget, "\n");
	AboutXcfa_info_machine (widget, ptr);
	g_free (ptr);
	ptr = NULL;
}
// 
// 
void on_label_author_about_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_label_set_justify (GTK_LABEL(widget), GTK_JUSTIFY_CENTER);
	gtk_label_set_markup (GTK_LABEL(widget), "\nCopyright © 2003-2015  -  Bulin Claude  -  fr\n");
}
// 
// 
void on_label_xcfa_about_realize (GtkWidget *widget, gpointer user_data)
{
	gchar	*str = NULL;
	gchar	*ModelName = GetInfo_cpu ();

	str = g_strdup_printf (
		"\n"
		"GTK+ implementation of the GNU shell command.\n"
		"version %s\n\n"
		"<i>%d CPU %d bits\n%s</i>\n",
		VERSION,
		HostConf.NbCpu,HostConf.TypeCpu,ModelName);
	
	gtk_label_set_justify (GTK_LABEL(widget), GTK_JUSTIFY_CENTER);
	gtk_label_set_markup (GTK_LABEL(widget), str);
	g_free (str);		str = NULL;
	g_free (ModelName);	ModelName = NULL;
}
// 
// 
void on_image_gpl_xcfa_about_realize (GtkWidget *widget, gpointer user_data)
{
	gchar     *LineCommand = NULL;

	if( NULL != (LineCommand = xdg_search_data_xdg( "gplv3.png" ))) {
		gtk_image_set_from_file (GTK_IMAGE (widget), LineCommand);
		g_free (LineCommand);
		LineCommand = NULL;
	}
}
// 
// 
void on_image_about_realize (GtkWidget *widget, gpointer user_data)
{
	gchar     *LineCommand = NULL;

	if( NULL != (LineCommand = xdg_search_data_xdg( "about.png" ))) {
		gtk_image_set_from_file (GTK_IMAGE (widget), LineCommand);
		g_free (LineCommand);
		LineCommand = NULL;
	}
}
// 
// 
gboolean wind_about_init( GtkWidget *FromWindMain )
{
	gchar	*PackageVersion = NULL;
	
	if( NULL == (GtkBuilderProjet_wind_about = Builder_open( "xcfa_win_about.glade", GtkBuilderProjet_wind_about ))) {
		return( EXIT_FAILURE );
	}
	// Add language support from glade file
	gtk_builder_set_translation_domain(  GtkBuilderProjet_wind_about, "wind_about" );
	gtk_builder_connect_signals( GtkBuilderProjet_wind_about, NULL );

	WindMain_wind_about = GTK_WIDGET( gtk_builder_get_object( GtkBuilderProjet_wind_about, "wind_about" ));
	if( NULL != FromWindMain ) {
		gtk_window_set_transient_for (GTK_WINDOW(WindMain_wind_about), GTK_WINDOW(FromWindMain));
		gtk_window_set_modal (GTK_WINDOW(WindMain_wind_about), TRUE);
	}
	libutils_set_default_icone_to_win( WindMain_wind_about );

	PackageVersion = g_strdup_printf( "%s  -  %s", PACKAGE_NAME, VERSION );
	gtk_window_set_title( GTK_WINDOW( WindMain_wind_about ), PackageVersion );
	g_free( PackageVersion );
	PackageVersion = NULL;

	gtk_widget_show_all( WindMain_wind_about );

	return( TRUE );
}

