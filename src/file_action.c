 /*
 *  file      : file_action.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
    Salut Claude

    Le ReplayGain s'applique toujours en dernier, une fois toutes les
    conversions effectuées.
    Sauf bien sûr pour nettoyer mais en principe quand on nettoie les « tags
    » ReplayGain c'est la seule opération que l'on fait sur le/les fichier(s).

    Maxime

    xcfaudio a écrit :
    > Bonjour à tous,
    >
    > L'intégration du ReplayGain est en bonne voie ;)
    >
    > ? Juste une question d'ordre technique ?
    >
    > L'ordre de prise en compte par rapport à une sélection (globale) doit
    > bien être:
    > 1 - Clear ReplayGain
    > 2 - Normalisation Individuelle
    > 3 - Normalisation Collective
    > 4 - Conversions
    > 5 - ReplayGain
    >
    > Xavier, Maxime, Patrick ... ???
    >
*/


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <pthread.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "file.h"
#include "replaygain.h"
#include "win_scan.h"
#include "normalise.h"
#include "notify_send.h"


/*
*---------------------------------------------------------------------------
* VARIABLES
*---------------------------------------------------------------------------
*/

typedef struct {

	gboolean   dedans;
	guint      handler_timeout;
	gboolean   bool_ReplaygainClear;
	gboolean   bool_normalize;
	gboolean   bool_conversion;
	gboolean   bool_ReplaygainApply;
	gboolean   bool_trash;

	gint       NbrFileRemove;
	
} FILE_ACTION;

FILE_ACTION FileAction;



// 
// 
void fileaction_set_end (FILEACTION_TYPE FileActionType)
{
	FileAction.dedans = FALSE;
	
	switch (FileActionType) {
	case TYPE_REPLAYGAINCLEAR :
		FileAction.bool_ReplaygainClear =  FALSE;
		break;
	case TYPE_NORMALISE :
		FileAction.bool_normalize       =  FALSE;
		break;
	case TYPE_CONVERSION :
		FileAction.bool_conversion      =  FALSE;
		break;
	case TYPE_REPLAYGAINAPPLY :
		FileAction.bool_ReplaygainApply =  FALSE;
		break;
	case TYPE_TRASH :
		FileAction.bool_trash		=  FALSE;
		break;
	}
}
// 
// 
static gint fileaction_timeout (gpointer data)
{
	if (FileAction.dedans == TRUE) return (TRUE);
	
	if (TRUE == conv.bool_stop) {
		FileAction.bool_ReplaygainClear =  FALSE;
		FileAction.bool_normalize       =  FALSE;
		FileAction.bool_conversion      =  FALSE;
		FileAction.bool_ReplaygainApply =  FALSE;
		FileAction.bool_trash		=  FALSE;
	}
	
	if (FileAction.bool_ReplaygainClear == TRUE) {
		FileAction.dedans = TRUE;
		replaygain_action (_CLEAR_REPLAYGAIN_);
		// FileAction.bool_ReplaygainClear = FALSE;
		return (TRUE);
	}
	if (FileAction.bool_normalize == TRUE) {
		FileAction.dedans = TRUE;
		normalise_action ();
		// FileAction.bool_normalize = FALSE;
		return (TRUE);
	}
	if (FileAction.bool_conversion == TRUE) {
		FileAction.dedans = TRUE;
		fileconv_action ();
		// FileAction.bool_conversion = FALSE;
		return (TRUE);
	}	
	if (FileAction.bool_ReplaygainApply == TRUE) {
		FileAction.dedans = TRUE;
		replaygain_action (_APPLY_REPLAYGAIN_);
		// FileAction.bool_ReplaygainApply = FALSE;
		return (TRUE);
	}
	if (FileAction.bool_trash == TRUE) {
		FileAction.dedans = TRUE;
		filetrash_action ();
		// FileAction.bool_trash = FALSE;
		return (TRUE);
	}
	if (FALSE == FileAction.bool_ReplaygainClear &&
	    FALSE == FileAction.bool_normalize &&
	    FALSE == FileAction.bool_conversion &&
	    FALSE == FileAction.bool_ReplaygainApply &&
	    FALSE == FileAction.bool_trash) {
		
		WindScan_close ();
		g_source_remove (FileAction.handler_timeout);
		
		if (FALSE == conv.bool_stop) {
			NotifySend_msg (_("XCFA: Processing files"), _("Ok"), conv.bool_stop);
		} else {
			NotifySend_msg (_("XCFA: Processing files"), _("Stop by user"), conv.bool_stop);
		}
	}
	
	return (TRUE);
}
// 
// 
gboolean fileaction_get_bool_Replaygain_Is_Clear (void)
{
	GList   *list = NULL;
	DETAIL  *detail = NULL;

	list = g_list_first (entetefile);
	while (list) {
		if (NULL != (detail = (DETAIL *)list->data)) {
			if (detail->type_infosong_file_is == FILE_IS_OGG ||
				detail->type_infosong_file_is == FILE_IS_MP3 ||
				detail->type_infosong_file_is == FILE_IS_FLAC ||
				detail->type_infosong_file_is == FILE_IS_WAVPACK) {
				
				if (detail->Etat_ReplayGain == RPG_EFFACER) return (TRUE);
			}
		}
		list = g_list_next (list);
	}
	return (FALSE);
}
// 
// 
void fileaction_choice (void)
{
	FileAction.dedans = FALSE;
	FileAction.NbrFileRemove = 0;
	conv.bool_stop = FALSE;
	
	// PRINT_FUNC_LF();
	
	// VERIFICATION DE LA PRESENCE DES FICHIERS SUR LA MEMOIRE DE MASSE
	file_verify_before_conversion ();
		
	FileAction.bool_ReplaygainClear    = fileaction_get_bool_Replaygain_Is_Clear ();
	FileAction.bool_normalize          = normalise_is_individuel ();
	FileAction.bool_conversion         = file_get_bool_is_conversion ();
	FileAction.bool_ReplaygainApply    = file_get_bool_ReplaygainApply ();
	FileAction.bool_trash              = filetrash_ok ();
		
	FileAction.handler_timeout = g_timeout_add (150, fileaction_timeout, 0);
}









