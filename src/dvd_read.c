 /*
 *  file      : dvd_read.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 *
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 *
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "conv.h"
#include "dvd.h"

/*
NAME_DVD
	TITLE 01
		AUDIO_TITLE 1
		CHAPTER 01
			AUDIO 1
				Stream id: 0x80, 0x81, 0x82, ...
				Length: hh:mm:ss.ccc
				Format: ac3, dts
				Frequency: 48000 (ac3 par defaut) pas dts
				Quantization: drc
				Channels:

			AUDIO 2
		CHAPTER 02
			AUDIO 1
			AUDIO 2
	TITLE 02
		CHAPTER 01
			AUDIO 1
			AUDIO 2



*/


void dvdread_remove_list( void )
{
	GList	*list = NULL;
	VAR		*var = NULL;
	
	if( NULL != (list = g_list_first( GlistDvd ))) {
		while( list ) {
			if( NULL != (var = (VAR *)list->data )) {
				if( var->def )					{ g_free( var->def );					var->def = NULL;					}
				if( var->time )					{ g_free( var->time );					var->time = NULL;					}
				var->GUINT_TempsTotal = 0;
				if( var->name_file )			{ g_free( var->name_file );				var->name_file = NULL;				}
				if( var->StaticNameFile )		{ g_free( var->StaticNameFile );		var->StaticNameFile = NULL;			}
				if( var->StrNumerateTitle )		{ g_free( var->StrNumerateTitle );		var->StrNumerateTitle = NULL;		}
				if( var->StrNumerateChapter )	{ g_free( var->StrNumerateChapter );	var->StrNumerateChapter = NULL;		}
				if( var->StrNumberChannel )		{ g_free( var->StrNumberChannel );		var->StrNumberChannel = NULL;		}
				if( var->StrNumerateStreamId )	{ g_free( var->StrNumerateStreamId );	var->StrNumerateStreamId = NULL;	}
				g_free (var);
				var = list->data = NULL;
			}
			list = g_list_next( list );
		}
		g_list_free( GlistDvd );
		GlistDvd = NULL;
	}
}

void dvdread_create_list( TYPE_LIST_DVD type_list_dvd,
			  gchar *def,
			  gchar *time,
			  gchar *name_file,
			  gint   format_id,
			  gint   NumerateTitle,
			  gint   NumerateChapter,
			  gint   NumberChannel,
			  gchar *StrNumerateStreamId )
{
	VAR *var = g_malloc0( sizeof(VAR) );
	var->type_list_dvd       = type_list_dvd;
	var->def                 = g_strdup (def ? def : "");
	var->time                = g_strdup (time ? time : "");
	var->GUINT_TempsTotal    = 0;

	if (NULL != time) {
		guint H = 0;
		guint M = 0;
		guint S = 0;
		gchar *Ptr = time;
		/*
		TIME=00:08:36
		*/
		H = atoi (Ptr);
		Ptr += 3;
		M = atoi (Ptr);
		Ptr += 3;
		S = atoi (Ptr);

		var->GUINT_TempsTotal = (H * 60) * 60;
		var->GUINT_TempsTotal += M * 60;
		var->GUINT_TempsTotal += S;
	}
	var->name_file           = g_strdup (name_file ? name_file : "");
	var->StaticNameFile      = g_strdup (var->name_file);
	var->format_id           = format_id;
	var->StrNumerateTitle    = g_strdup_printf ("%d", NumerateTitle);
	var->NumerateTitle       = NumerateTitle;
	var->StrNumerateChapter  = g_strdup_printf ("%d", NumerateChapter);
	var->NumerateChapter     = NumerateChapter;
	var->StrNumberChannel    = g_strdup_printf ("%d", NumberChannel);
	var->StrNumerateStreamId = g_strdup (StrNumerateStreamId ? StrNumerateStreamId : "");
	var->EtatChoix           = _COCHE_;
	var->DebutLecture        = 0;
	var->EtatNormalise       = FALSE;

	GlistDvd = g_list_append (GlistDvd, var);
}

gboolean dvdread_dvd_read( void )
{
	gchar	**Larrbuf = NULL;
	gchar	*Ptr = NULL;
	gint	line;
	gchar	*Str = NULL;
	gchar	*buf = NULL;
	gchar	**PtrTabArgs = NULL;
	gint	cpt_chapter;
	gint	cpt_data;
	gint	LongestTrack = 0;
	gint	max_line_chapter;
	
	gint	Frequency = 0;
	gchar	*StrTime = NULL;
	gchar	*StrNameFile = NULL;
	gint	format_id = _INCONNU_;
	gint	NumerateTitle = -1;
	gint	NumerateChapter;
	gint	NumberChannel = 2;
	gchar	*StrNumerateStreamId = NULL;
	gchar	*StrLanguage = NULL;
	gint	NumerateAudio = 0;

	if( FALSE == g_file_test( "/tmp/data_xcfa.txt", G_FILE_TEST_IS_REGULAR )) {
		printf("Lecture du contenu de: %s\n",var_dvd.PathDvd );
		PtrTabArgs = conv_lsdvd_read_get_param( var_dvd.PathDvd );
		conv_to_convert( PtrTabArgs, FALSE, LSDVD, "NEW2:-> LSDVD" );
		PtrTabArgs = conv_RemoveTab( PtrTabArgs );
	}
	
	if( NULL == (buf = libutils_get_datas_on_disk() )) {
		return FALSE;
	}
	else if( NULL != buf  ) {
		Larrbuf = g_strsplit( buf, "\n", 0 );
		g_free( buf );
		buf = NULL;
	}

	// Search dvd title 
	for( line = 0; Larrbuf && Larrbuf[ line ]; line++ )
		if( NULL != (Ptr = strstr( Larrbuf[ line ], "Disc Title: " ))) {
			Ptr += 12;
			dvdread_create_list( _DVD_NAME, Ptr, NULL, NULL, 0, 0, 0, 0, NULL );
			break;
		}
	
	// Search Longest track
	for( line = 0; Larrbuf && Larrbuf[ line ]; line++ )
		if( NULL != (Ptr = strstr( Larrbuf[ line ], "Longest track: " ))) {
			Ptr += 15;
			LongestTrack = atoi( Ptr );
			break;
		}

	// List Building
	for( line = 1; Larrbuf && Larrbuf[ line ]; line ++ ) {

		// Title: 15, Length: 02:05:00.600 Chapters: 21, Cells: 22, Audio streams: 04, Subpictures: 06
		Ptr = Larrbuf[ line ];
		if( *(Ptr +0) == 'T' &&
			*(Ptr +1) == 'i' &&
			*(Ptr +2) == 't' &&
			*(Ptr +3) == 'l' &&
			*(Ptr +4) == 'e' &&
			*(Ptr +5) == ':' &&
			*(Ptr +6) == ' ' ) {

			NumerateTitle = atoi( Ptr +7 );
			if( NumerateTitle == LongestTrack )
					Str = g_strdup_printf( _("Title [%02d <span color=\"red\"><b>**</b></span> ]"), NumerateTitle );
			else 	Str = g_strdup_printf( _("Title [%02d]"), NumerateTitle );
			dvdread_create_list( _DVD_TITLE, Str, NULL, NULL, 0, 0, 0, 0, NULL );
			g_free( Str );
			Str = NULL;
			
			// Seeking the number of lines per chapter for: max_line_chapter
			for( max_line_chapter = line +1; Larrbuf && Larrbuf[ max_line_chapter ]; max_line_chapter ++ ) {
				Ptr = Larrbuf[ max_line_chapter ];
				if( *(Ptr +0) == 'T' &&
					*(Ptr +1) == 'i' &&
					*(Ptr +2) == 't' &&
					*(Ptr +3) == 'l' &&
					*(Ptr +4) == 'e' &&
					*(Ptr +5) == ':' &&
					*(Ptr +6) == ' ' ) {
					break;
				}
			}
			
			// Search all "Chapter: " in: (line +1) .. max_line_chapter
			// Chapter: 01, Length: 00:07:26.640, Start Cell: 01
			for( cpt_chapter = line +1; cpt_chapter < max_line_chapter; cpt_chapter ++ ) {
				if( NULL != (Ptr = strstr( Larrbuf[ cpt_chapter ], "Chapter: ")) && strstr( Larrbuf[ cpt_chapter ], ", Start Cell: " )) {
					Ptr += 9;
					NumerateChapter = atoi( Ptr );
					Str = g_strdup_printf( _("Chapter: [%02d]"), NumerateChapter );
					dvdread_create_list( _DVD_CHAPTER, Str, NULL, NULL, 0, 0, 0, 0, NULL );
					g_free( Str );
					Str = NULL;
					
					if( StrTime )				{ g_free( StrTime );				StrTime = NULL;				}
					Ptr = strstr( Larrbuf[ cpt_chapter ], "Length: ");
					Ptr += 8;
					StrTime = g_strdup( Ptr );
					Ptr = StrTime;
					*(Ptr +8 ) = '\0';
					
					// Search all "Audio: " in: (line +1) .. max_line_chapter
					// Audio: 1, Language: en - English, Format: ac3, Frequency: 48000, Quantization: drc, Channels: 6, AP: 0, Content: Undefined, Stream id: 0x80
					for( cpt_data = line +1; cpt_data < max_line_chapter; cpt_data ++ ) {
						if( NULL != (Ptr = strstr( Larrbuf[ cpt_data ], "Audio: " ))) {
							
							Ptr += 7;
							NumerateAudio = atoi( Ptr );
							
							if( NULL != (Ptr = strstr( Larrbuf[ cpt_data ], "Frequency: " ))) {
								Ptr += 11;
								Frequency = atoi( Ptr ) / 1000;
							}
							if( NULL != (Ptr = strstr( Larrbuf[ cpt_data ], "Format: " ))) {
								Ptr += 8;
								if( *Ptr == 'a' && *(Ptr +1) == 'c' && *(Ptr +2) == '3' )
									format_id = _AC3_;
								else if( *Ptr == 'd' && *(Ptr +1) == 't' && *(Ptr +2) == 's' )
									format_id = _DTS_;
								else
									format_id = _INCONNU_;
							}
							if( NULL != (Ptr = strstr( Larrbuf[ cpt_data ], "Channels: " ))) {
								Ptr += 10;
								NumberChannel = atoi( Ptr );
							}
							if( NULL != (Ptr = strstr( Larrbuf[ cpt_data ], "Stream id: " ))) {
								Ptr += 11;
								StrNumerateStreamId = g_strdup( Ptr );
							}
							if( NULL != (Ptr = strstr( Larrbuf[ cpt_data ], "Language: " ))) {
								Ptr += 10;
								StrLanguage = g_strdup( Ptr );
								for( Ptr = StrLanguage; *Ptr != ','; Ptr ++ );
								*Ptr = '\0';
							}
							
							StrNameFile = g_strdup_printf( "Title_%d_chapter_%d_audio_%d_lang_%c%c",
														NumerateTitle,
														NumerateChapter,
														NumerateAudio,
														*(StrLanguage +0),
														*(StrLanguage +1)
														);
														
							Str = g_strdup_printf( "%s      %s      %d KHz",
												StrLanguage,
												format_id == _AC3_ ? "AC3" : format_id == _DTS_ ? "DTS" : "???",
												Frequency
												);
												
							dvdread_create_list (_DVD_DATA,		// TYPE_LIST_DVD type_list_dv
										Str,					// gchar *def
										StrTime,				// gchar *time
										StrNameFile,			// gchar *name_file
										format_id,				// gint   format_id
										NumerateTitle,			// gint   NumerateTitle
										NumerateChapter,		// gint   NumerateChapter
										NumberChannel,			// gint   NumberChannel
										StrNumerateStreamId		// gchar *StrNumerateStreamId
										);
										
							if( Str ) 					{ g_free( Str );					Str = NULL;					}
							if( StrNameFile ) 			{ g_free( StrNameFile );			StrNameFile = NULL;			}
							if( StrNumerateStreamId )	{ g_free( StrNumerateStreamId );	StrNumerateStreamId = NULL;	}
							if( StrLanguage )			{ g_free( StrLanguage );			StrLanguage = NULL;			}
							
						}
					}
				}
			}
		}
	}
	g_strfreev( Larrbuf );

	return (TRUE);
}













