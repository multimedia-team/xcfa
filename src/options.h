 /*
 *  file      : options.h
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef options_h
#define options_h 1


typedef enum {
	COLOR_INIT = 0,												// 
	COLOR_NONE,													// 
	COLOR_LAME_DEBIT,											// 
	COLOR_LAME_MODE,											// 
	COLOR_OGGENC_DEBIT,											// 
	COLOR_OGGENC_MANAGED,										// 
	COLOR_OGGENC_DOWNMIX,										// 
	COLOR_FLAC_TAUX_COMPRESSION,								// 
	COLOR_MAC_QUALITE,											// 
	COLOR_WAVPACK_MODE_HYBRIDE,									// 
	COLOR_WAVPACK_COMPRESSION,									// 
	COLOR_WAVPACK_SOUND,										// 
	COLOR_WAVPACK_FICHIER_CORRECTION,							// 
	COLOR_WAVPACK_COMPRESSION_MAXIMUM,							// 
	COLOR_WAVPACK_SIGNATURE_MD5,								// 
	COLOR_WAVPACK_EXTRA_ENCODING,								// 
	COLOR_MUSEPACK_QUALITE,										// 
	COLOR_FAAC_CONTENEUR,										// 
	COLOR_FAAC_SET_CHOICE_VBR_ABR,								// 
	COLOR_TAG_ARTIST,											// 
	COLOR_TAG_TITLE,											// 
	COLOR_TAG_ALBUM,											// 
	COLOR_TAG_NUMERATE,											// 
	COLOR_TAG_GENRE,											// 
	COLOR_TAG_YEAR,												// 
	COLOR_TAG_COMMENT											// 
} COLOR_LINE_COMMAND;

typedef enum {
	_NONE_ = 0,													// 
	_DEBIAN_,													// 
	_TGZ_,														// 
	_RPM_,														// 
	_FPM_,														// 
	_ARCHLINUX_,												// 
	_PACKAGE_NOT_FOUND_											// 
} TYPE_PACKAGE;

typedef struct {
	GtkWidget		*Adr_button_music_file_end_of_convert;		// 
	GtkWidget		*Adr_checkbutton_end_of_convert;			// 
	GtkComboBox		*Adr_Widget_Nice;							// 
	GtkComboBox		*Adr_Widget_Lame_bitrate;					// 
	GtkComboBox		*Adr_Widget_Lame_abr_cbr_vbr;				// 
	GtkComboBox		*Adr_Widget_Lame_Mode;						// 
	GtkComboBox		*Adr_Widget_Oggenc_bitrate;					// 
	GtkComboBox		*Adr_Widget_Oggenc_managed;					// 
	GtkComboBox		*Adr_Widget_Oggenc_downmix;					// 
	GtkComboBox		*Adr_Widget_Mppenc;							// 
	GtkComboBox		*Adr_Widget_wavpack;						// 
	GtkComboBox		*Adr_Widget_wavpack_sound;					// 
	GtkComboBox		*Adr_Widget_wavpack_mode_hybride;			// 
	GtkComboBox		*Adr_Widget_wavpack_correction_file;		// 
	GtkComboBox		*Adr_Widget_wavpack_maximum_compression;	// 
	GtkComboBox		*Adr_Widget_wavpack_signature_md5;			// 
	GtkComboBox		*Adr_Widget_wavpack_extra_encoding;			// 
	GtkComboBox		*Adr_Widget_flac_compression;				// 
	GtkComboBox		*Adr_Widget_ape_compression;				// 
	GtkWidget		*Adr_radiobutton_cdparanoia;				// 
	GtkWidget		*Adr_radiobutton_libcddb;					// 
	GtkWidget		*Adr_radiobutton_extract_with_cdparanoia;	// 
	GtkWidget		*Adr_radiobutton_cdparanoia_mode_2;			// 
	GtkWidget		*Adr_radiobutton_cdparanoia_mode_expert;	// 
	GtkWidget		*Adr_radiobutton_extract_with_cdda2wav;		// 
	GtkWidget		*Adr_label_type_paquege;					// 
	TYPE_PACKAGE		Type_Package;							// _NONE_ = 0, _DEBIAN_, _TGZ_, _RPM_, _FPM_, _PACKAGE_NOT_FOUND_
	GtkWidget		*Adr_entry_lame_mp3;						// 
	GtkWidget		*Adr_label_lame_mp3;						// 
	GtkWidget		*Adr_entry_oggenc_ogg;						// 
	GtkWidget		*Adr_label_oggenc_ogg;						// 
	GtkWidget		*Adr_entry_flac_flac;						// 
	GtkWidget		*Adr_label_flac_flac;						// 
	GtkWidget		*Adr_entry_mac_ape;							// 
	GtkWidget		*Adr_label_mac_ape;							// 
	GtkWidget		*Adr_entry_wavpack_wv;						// 
	GtkWidget		*Adr_label_wavpack_wv;						// 
	GtkWidget		*Adr_entry_musepack_mpc;					// 
	GtkWidget		*Adr_label_musepack_mpc;					// 
	GtkWidget		*Adr_entry_faac_m4a;						// 
	GtkWidget		*Adr_label_faac_m4a;						// 
	GtkWidget		*Adr_checkbutton_artist_tag;				// 
	GtkWidget		*Adr_checkbutton_title_tag;					// 
	GtkWidget		*Adr_checkbutton_album_tag;					// 
	GtkWidget		*Adr_checkbutton_numerate_tag;				// 
	GtkWidget		*Adr_checkbutton_genre_tag;					// 
	GtkWidget		*Adr_checkbutton_year_tag;					// 
	GtkWidget		*Adr_checkbutton_comment_tag;				// 
	COLOR_LINE_COMMAND	ColorLineCommand;						// 
	GtkWidget		*Adr_path_temp;								// 
	GtkComboBox		*Adr_Widget_faac_conteneur;					// 
	GtkComboBox		*Adr_Widget_faac_choice_vbr_abr;			// 
	GtkComboBox		*Adr_Widget_faac_set_choice_vbr_abr;		// 
	GtkComboBox		*Adr_Widget_aacplusenc_mono;				// 
	GtkComboBox		*Adr_Widget_aacplusenc_stereo;				// 
} VAR_OPTIONS;

// 
// ---------------------------------------------------------------------------
//  OPTIONS.C
// ---------------------------------------------------------------------------
// 
extern VAR_OPTIONS var_options;

#include "conv.h"
#include "file.h"

// 
// ---------------------------------------------------------------------------
//  OPTIONS.C
// ---------------------------------------------------------------------------
// 
void		options_set_all_interne (void);
gboolean	options_get_entry_is_valid (TYPE_CONV p_verif_conv);
gchar          *options_get_params( TYPE_CONV TypeConv );

// 
// ---------------------------------------------------------------------------
//  OPTIONS_CD.C
// ---------------------------------------------------------------------------
// 
void		OptionsCd_set_entry_and_label( void );
gboolean	OptionsCd_get_save_log_mode_expert( void );


// 
// ---------------------------------------------------------------------------
//  OPTIONS_INTERNAL.C
// ---------------------------------------------------------------------------
// 
void		OptionsInternal_set_datas_interne (COLOR_LINE_COMMAND ColorLineCommand, GtkWidget *widget, TYPE_CONV type_conv);

// 
// ---------------------------------------------------------------------------
//  OPTIONS_MP3.C 
//  mp3
// ---------------------------------------------------------------------------
// 
gchar		*optionsLame_get_str_val_bitrate_abr_vbr_lame (void);
gchar		*optionsLame_get_str_val_mode_lame (void);
gchar		*optionsLame_get_param( void );

// 
// ---------------------------------------------------------------------------
//  OPTIONS_MUSEPACK.C
//  mpc
// ---------------------------------------------------------------------------
// 
gchar		*optionsMusepack_get_quality_mppenc (void);

// 
// ---------------------------------------------------------------------------
//  OPTIONS_OGGENC.C
//  ogg
// ---------------------------------------------------------------------------
// 
gchar		*optionsOggenc_get_val_bitrate_oggenc (void);
gboolean	optionsOggenc_get_bool_managed_oggenc (void);
gboolean	optionsOggenc_get_bool_downmix_oggenc (void);
gchar		*optionsOggenc_get_param( void );

// 
// ---------------------------------------------------------------------------
//  OPTIONS_WAVPACK.C
//  wv
// ---------------------------------------------------------------------------
// 
gchar		*optionsWavpack_get_wavpack_compression (void);
gchar		*optionsWavpack_get_wavpack_sound (void);
gchar		*optionsWavpac_get_wavpack_hybride (void);
gchar		*optionsWavpack_get_wavpack_correction_file (void);
gchar		*optionsWavpack_get_wavpack_maximum_compression (void);
gchar		*optionsWavpack_get_wavpack_signature_md5 (void);
gchar		*optionsWavpack_get_wavpack_extra_encoding (void);

// 
// ---------------------------------------------------------------------------
//  OPTIONS_FLAC.C
//  flac
// ---------------------------------------------------------------------------
// 
gchar		*optionsFlac_get_compression_level_flac (void);

// 
// ---------------------------------------------------------------------------
//  OPTIONS_MAC.C
//  ape
// ---------------------------------------------------------------------------
// 
gchar		*optionsApe_get_compression_level_ape (void);

// 
// ---------------------------------------------------------------------------
//  OPTIONS_FAAC.C
//  m4a
// ---------------------------------------------------------------------------
// 
gchar		*OptionsFaac_get_faac_conteneur (void);
gchar		*OptionsFaac_get_faac_set_choice_vbr_abr (void);

// 
// ---------------------------------------------------------------------------
//  OPTIONS_AACPLUSENC.C
//  aac
// ---------------------------------------------------------------------------
// 
gint		optionsaacplusenc_get_bitrate_mono (void);
gint		optionsaacplusenc_get_bitrate_stereo (void);


#endif

