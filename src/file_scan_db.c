 /*
 *  file      : file_scan_db.c
 *  project   : xcfa
 *  with      : Gtk-3
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <pthread.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "get_info.h"
#include "win_scan.h"
#include "file.h"



/*
*---------------------------------------------------------------------------
* VARIABLES: GESTION THREAD & TIMEOUT FOR LEVEL
*---------------------------------------------------------------------------
*/

typedef struct {
	gboolean         bool_etat;			// 
	gchar		*MessUser;			// 
	
	gint             NbrElementsInList;		// 
	gint             ElementActif;			// 
	double           total_percent;			// 
	
	pthread_t        nmr_tid;			// 
	guint            handler_timeout;		// 
	gboolean         bool_thread_end;		// 

	gboolean         bool_compteur;			// 
	gint             pass_conv;			// 
	
	gboolean         bool_update;			// 
	
	gboolean	 BoolWindScanUsed;		// 
	
} VAR_FILE_SCAN_DB;

VAR_FILE_SCAN_DB VarFileScanDB;





// 
// 
static void FileScanDB_thread (void *arg)
{
	DETAIL	*detail = NULL;
	GList	*list = NULL;
	gint	FicLevelMix = (gint)gtk_spin_button_get_value (var_file.AdrSpinbuttonNormalise);
	
 	VarFileScanDB.bool_thread_end = FALSE;

	list = g_list_first (entetefile);
	while (list) {
		
		if (NULL != (detail = (DETAIL *)list->data) &&
		    detail->Etat_Scan == ETAT_SCAN_DEMANDE &&
		    (detail->type_infosong_file_is == FILE_IS_WAV ||
		     detail->type_infosong_file_is == FILE_IS_OGG ||
		     detail->type_infosong_file_is == FILE_IS_MP3)) {
		
			if (detail->type_infosong_file_is == FILE_IS_WAV) {
				
				INFO_WAV *info = (INFO_WAV *)detail->info;

				VarFileScanDB.bool_compteur = TRUE;
				
				detail->LevelMix         = FicLevelMix;
				info->LevelDbfs.NewLevel = FicLevelMix;
				info->LevelDbfs.level    = GetInfo_level_get_from (FILE_IS_WAV, detail->namefile);
				
				detail->Etat_Scan = ETAT_SCAN_OK;
				VarFileScanDB.ElementActif ++;
				VarFileScanDB.bool_compteur = FALSE;
				VarFileScanDB.bool_etat = TRUE;
				VarFileScanDB.bool_update = TRUE;
			}
			else if (detail->type_infosong_file_is == FILE_IS_OGG) {
				
				INFO_OGG *info = (INFO_OGG *)detail->info;
				
				VarFileScanDB.bool_compteur = TRUE;
			
				detail->LevelMix         = FicLevelMix;
				info->LevelDbfs.NewLevel = FicLevelMix;
				info->LevelDbfs.level    = GetInfo_level_get_from (FILE_IS_OGG, detail->namefile);
				
				detail->Etat_Scan = ETAT_SCAN_OK;
				VarFileScanDB.ElementActif ++;
				VarFileScanDB.bool_compteur = FALSE;
				VarFileScanDB.bool_etat = TRUE;
				VarFileScanDB.bool_update = TRUE;
			}
			else if (detail->type_infosong_file_is == FILE_IS_MP3) {
				
				INFO_MP3 *info = (INFO_MP3 *)detail->info;
				
				VarFileScanDB.bool_compteur = TRUE;
			
				detail->LevelMix         = FicLevelMix;
				info->LevelDbfs.NewLevel = FicLevelMix;
				info->LevelDbfs.level    = GetInfo_level_get_from (FILE_IS_MP3, detail->namefile);
				
				detail->Etat_Scan = ETAT_SCAN_OK;
				VarFileScanDB.ElementActif ++;
				VarFileScanDB.bool_compteur = FALSE;
				VarFileScanDB.bool_etat = TRUE;
				VarFileScanDB.bool_update = TRUE;
			}
		}
		list = g_list_next (list);
	} 
	
	VarFileScanDB.bool_thread_end = TRUE;
	pthread_exit(0);
}
// 
// 
static gint FileScanDB_timeout (gpointer data)
{
	if (VarFileScanDB.bool_etat == TRUE) {
		if( NULL != VarFileScanDB.MessUser ) {
			g_free (VarFileScanDB.MessUser);
			VarFileScanDB.MessUser = NULL;
		}
		VarFileScanDB.bool_etat = FALSE;
	}
	
	if (FALSE == VarFileScanDB.BoolWindScanUsed && VarFileScanDB.bool_compteur == TRUE) {
		
		gchar *str = NULL;
    		gchar *spinner="|/-\\";
		gchar  foo [ 2 ];
		
		foo [ 0 ] = spinner[VarFileScanDB.pass_conv++%4];
		foo [ 1 ] = '\0';
		
		str = g_strdup_printf ("<b>En cours: %d sur %d   %s</b>",
					VarFileScanDB.ElementActif +1,
					VarFileScanDB.NbrElementsInList,
					foo
					);
		WindScan_set_label (str);
		g_free (str);
		str = NULL;
		VarFileScanDB.total_percent = (double)(VarFileScanDB.ElementActif +1) / (double)VarFileScanDB.NbrElementsInList;
		str = g_strdup_printf ("%d%%", (int)(VarFileScanDB.total_percent*100));
		WindScan_set_progress (str, VarFileScanDB.total_percent);
		g_free (str);
		str = NULL;
	}
	
	if (VarFileScanDB.bool_update == TRUE) {
		VarFileScanDB.bool_update = FALSE;
		file_pixbuf_update_glist ();
		return (TRUE);
	}
	
	if (VarFileScanDB.bool_thread_end == TRUE) {
		if (FALSE == VarFileScanDB.BoolWindScanUsed) WindScan_close ();
		g_source_remove (VarFileScanDB.handler_timeout);
	}
	
	return (TRUE);
}
// 
// 
void FileScanDB_action (gboolean BoolAllScan)
{
	PRINT_FUNC_LF();
	
	// SI DES FICHIERS wav, mp3 et ogg PRET POUR LE SCAN
	if ((VarFileScanDB.NbrElementsInList =  file_get_scan ()) > 0) {
		VarFileScanDB.bool_thread_end = FALSE;
		VarFileScanDB.bool_etat = FALSE;
		VarFileScanDB.ElementActif = 0;
		VarFileScanDB.bool_compteur = FALSE;
		VarFileScanDB.pass_conv = -1;
		VarFileScanDB.bool_update = FALSE;
		VarFileScanDB.MessUser = NULL;
		
		// VarFileScanDB.BoolWindScanUsed = WindScan_open ("Scan dBFS", WINDSCAN_PULSE);
		VarFileScanDB.BoolWindScanUsed =
			wind_scan_init(
				WindMain,
				"Scan dBFS",
				WINDSCAN_PULSE
				);
		VarFileScanDB.handler_timeout = g_timeout_add (50, FileScanDB_timeout, 0);
		pthread_create (&VarFileScanDB.nmr_tid, NULL ,(void *)FileScanDB_thread, (void *)NULL);
	}
}










