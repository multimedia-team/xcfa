 /*
 *  file      : options_aacplusenc.c
 *  project   : xcfa
 *  with      : Gtk-2
 *
 *  copyright : (C) 2003 - 2015 by Claude Bulin
 *
 *  xcfa - GTK+ implementation of the GNU shell command
 *  GNU General Public License
 *
 *  This file is part of XCFA.
 * 
 *  XCFA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  at your option) any later version.
 * 
 *  XCFA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with XCFA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
	#include "../config.h"
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gstdio.h>

#ifdef ENABLE_STATIC_LIBRARY
	#include "../lib/lib.h"
#endif

#include "global.h"
#include "configuser.h"
#include "options.h"



static 	gint Tab_bitrate_mono [] = { 10, 12, 14, 18, 20, 24, 32, 40 };
static 	gint Tab_bitrate_stereo [] = { 18, 24, 32, 40, 48 };
// 
// 
gint optionsaacplusenc_get_bitrate_mono (void)
{
	return (Tab_bitrate_mono [ gtk_combo_box_get_active (var_options.Adr_Widget_aacplusenc_mono) ]);
}
// 
// 
gint optionsaacplusenc_get_bitrate_stereo (void)
{
	return (Tab_bitrate_stereo [ gtk_combo_box_get_active (var_options.Adr_Widget_aacplusenc_stereo) ]);
}
// 
// 
void on_combobox_accplusenc_mono_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "10");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "12");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "14");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "18");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "20");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "24");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "32");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "40");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.ChoiceMonoAacplusenc);
	var_options.Adr_Widget_aacplusenc_mono = GTK_COMBO_BOX (widget);
}
// 
// 
void on_combobox_accplusenc_mono_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_aacplusenc_mono) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_aacplusenc_mono))) >= 0)
			Config.ChoiceMonoAacplusenc = ind;
	}
}
// 
// 
void on_combobox_accplusenc_stereo_realize (GtkWidget *widget, gpointer user_data)
{
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "18");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "24");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "32");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "40");
	gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), "48");
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget), Config.ChoiceStereoAacplusenc);
	var_options.Adr_Widget_aacplusenc_stereo = GTK_COMBO_BOX (widget);
}
// 
// 
void on_combobox_accplusenc_stereo_changed (GtkComboBox *combobox, gpointer user_data)
{
	if (NULL != var_options.Adr_Widget_aacplusenc_stereo) {
		gint ind;
		if ((ind = gtk_combo_box_get_active (GTK_COMBO_BOX (var_options.Adr_Widget_aacplusenc_stereo))) >= 0)
			Config.ChoiceStereoAacplusenc = ind;
	}
}








